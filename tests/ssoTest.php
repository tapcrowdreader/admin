<?php
class ssoTest extends PHPUnit_Framework_TestCase
{
	public function testDefault()
	{
		$this->assertTrue(true);
	}

	public function testHappy()
	{
		# data
		$hostname = 'admin.axa.tapcrowd.com';
		$shared_salt = '6pi2DeEGn0zMR25ZrwUXwVrnoYKmf9dw';
		$email = 'tom@tapcrowd.com';
		$external_id = 'Fs1h395g3';
		$fullname = 'Pol de Crossmol';
		$timestamp = time();

		# Build url
		$querystring = http_build_query(compact('email','external_id','fullname','timestamp'));
		$hash = hash_hmac('sha1', $querystring, $shared_salt);
		$url = sprintf('http://%s/auth/sso?%s&hash=%s', $hostname, $querystring, $hash);

		# Execute
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		$data = curl_exec($ch);
		$headers = explode("\r\n", substr($data, 0, strpos($data, "\r\n\r\n")));

		# Assert
		$this->assertContains('HTTP/1.1 303 See Other', $headers);
		$this->assertContains('Location: /', $headers);
		$this->assertRegExp('/Set-Cookie: tc_sess2=/', $data);

		preg_match('/Set-Cookie: tc_sess2=([a-f0-9]+);/', $data, $m);
		$sessId = $m[1];

		# Test: Requesting homepage should not redirect to auth/login
		curl_setopt($ch, CURLOPT_URL, "http://$hostname/");
		curl_setopt($ch, CURLOPT_COOKIE, "tc_sess2=$sessId;");
		$data = curl_exec($ch);

		$headers = explode("\r\n", substr($data, 0, strpos($data, "\r\n\r\n")));
		$this->assertNotContains('Location: /auth/login', $headers);
	}

	public function testBadTimestamp()
	{
		# data
		$hostname = 'admin.axa.tapcrowd.com';
		$shared_salt = '6pi2DeEGn0zMR25ZrwUXwVrnoYKmf9dw';
		$email = 'tom@tapcrowd.com';
		$external_id = 'Fs1h395g3';
		$fullname = 'Pol de Crossmol';
		$timestamp = time() - 3601; # More then 1 hour ago

		# Build url
		$querystring = http_build_query(compact('email','external_id','fullname','timestamp'));
		$hash = hash_hmac('sha1', $querystring, $shared_salt);
		$url = sprintf('http://%s/auth/sso?%s&hash=%s', $hostname, $querystring, $hash);

		# Execute
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
		$data = curl_exec($ch);
		$p = strpos($data, "\r\n\r\n");
		$headers = explode("\r\n", substr($data, 0, $p));
		$body = substr($data, $p+4);

		# Assert
		$this->assertContains('HTTP/1.1 400 Bad Request', $headers);
		$json = json_encode((object)array('errors' => array('Invalid timestamp')));
		$this->assertEquals($json, $body);
	}

	public function testBadHash_change_in_data()
	{
		# data
		$hostname = 'admin.axa.tapcrowd.com';
		$shared_salt = '6pi2DeEGn0zMR25ZrwUXwVrnoYKmf9dw';
		$email = 'tom@tapcrowd.com';
		$external_id = 'Fs1h395g3';
		$fullname = 'Pol de Crossmol';
		$timestamp = time(); # More then 1 hour ago

		# Build url
		$querystring_good = http_build_query(compact('email','external_id','fullname','timestamp'));
		$email = 'tom@tabcrowd.com';
		$querystring_bad = http_build_query(compact('email','external_id','fullname','timestamp'));
		$hash = hash_hmac('sha1', $querystring_bad, $shared_salt);
		$url = sprintf('http://%s/auth/sso?%s&hash=%s', $hostname, $querystring_good, $hash);

		# Execute
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/xml'));
		$data = curl_exec($ch);
		$p = strpos($data, "\r\n\r\n");
		$headers = explode("\r\n", substr($data, 0, $p));
		$body = substr($data, $p+4);

		# Assert
		$this->assertContains('HTTP/1.1 400 Bad Request', $headers);
		$xml = "<?xml version=\"1.0\"?>\n<response><errors><error>Invalid Hash</error></errors></response>";
		$this->assertEquals($xml, trim($body));
	}

	public function testUnsupportedAcceptContentType()
	{
		# data
		$hostname = 'admin.axa.tapcrowd.com';
		$shared_salt = '6pi2DeEGn0zMR25ZrwUXwVrnoYKmf9dw';

		# Build url
		$hash = hash_hmac('sha1', '', $shared_salt);
		$url = sprintf('http://%s/auth/sso?%s&hash=%s', $hostname, '', $hash);

		# Execute
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/rss'));
		$data = curl_exec($ch);
		$p = strpos($data, "\r\n\r\n");
		$headers = explode("\r\n", substr($data, 0, $p));
		$body = substr($data, $p+4);

		# Assert
		$this->assertContains('HTTP/1.1 406 Not Acceptable', $headers);
	}

	/**
	 * @group onelogin
	 */
	public function testOnelogin()
	{
		# data
		$hostname = 'admin.axa.tapcrowd.com';
		$shared_salt = '6pi2DeEGn0zMR25ZrwUXwVrnoYKmf9dw';
		$token =  hash_hmac('sha1','one_login_sso', $shared_salt);
		// '4275597a85ac374394786dfd062ba9969c79d0ad';

		$email = 'test@tapcrowd.com';
		$external_id = 'Fs1h395g3';
		$firstname = 'Pol';
		$lastname = 'de Crossmol';
		$timestamp = time();

		# Build url
		$signature = sha1(implode('', compact('firstname','lastname','email','timestamp','token')));
		$querystring = http_build_query(compact('firstname','lastname','email','timestamp'));
		$url = sprintf('http://%s/auth/onelogin?%s&signature=%s', $hostname, $querystring, $signature);

		# Execute
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		$data = curl_exec($ch);
		$headers = explode("\r\n", substr($data, 0, strpos($data, "\r\n\r\n")));

		# Assert
		$this->assertContains('HTTP/1.1 303 See Other', $headers);
		$this->assertContains('Location: /', $headers);
		$this->assertRegExp('/Set-Cookie: tc_sess2=/', $data);

		preg_match('/Set-Cookie: tc_sess2=([a-f0-9]+);/', $data, $m);
		$sessId = $m[1];

		# Test: Requesting homepage should not redirect to auth/login
		curl_setopt($ch, CURLOPT_URL, "http://$hostname/");
		curl_setopt($ch, CURLOPT_COOKIE, "tc_sess2=$sessId;");
		$data = curl_exec($ch);

		$headers = explode("\r\n", substr($data, 0, strpos($data, "\r\n\r\n")));
		$this->assertNotContains('Location: /auth/login', $headers);
	}
}
