<?php

$path = dirname(dirname(__DIR__));

#
# NOTE: ideal case requires us to mock the database
#
require $path . '/src/application/config/database.php';
require $path . '/src/application/models/abstractpdomodel_model.php';

# Account Model
require $path . '/src/application/models/account_model.php';

class AccountTest extends PHPUnit_Framework_TestCase
{
	public $model;

	public function setUp()
	{
		$this->model = \Tapcrowd\Model\Account::getInstance();
	}

	public function testGetUserRoles()
	{
		$roles = $this->model->getAccountRoles();
		$this->assertInternalType('array', $roles);
		$this->assertContainsOnly('object', $statuses);

		# Check default roles
		$this->assertContains('admin', $roles);
		$this->assertContains('user', $roles);

		$this->assertContains('bootstrap-label', $roles['user']);
	}

	public function testGetUserStatuses()
	{
		$statuses = $this->model->getAccountStatuses();
		$this->assertInternalType('array', $statuses);
		$this->assertContainsOnly('object', $statuses);

		# Check default statuses
		$this->assertContains('pending', $statuses);
		$this->assertContains('active', $statuses);

		$this->assertContains('bootstrap-label', $statuses['active']);
	}
}
