<?php

$path = dirname(dirname(__DIR__));

#
# NOTE: ideal case requires us to mock the database
#
require $path . '/src/application/config/database.php';
require $path . '/src/application/models/abstractpdomodel_model.php';

# Channel requirements
require $path . '/src/application/structs/Basestruct.php';
require $path . '/src/application/structs/Theme.php';
require $path . '/src/application/structs/Flavor.php';
require $path . '/src/application/structs/Channel.php';
require $path . '/src/application/models/channel_model.php';


class ChannelTest extends PHPUnit_Framework_TestCase
{
	private $channel_model;

	public function setUp()
	{
		$this->model = \Tapcrowd\Model\Channel::getInstance();
		$this->channel = new \Tapcrowd\Structs\Channel();
		$this->channel->channelId = 16;

		// delete from tc_channel_settings where channelId=16
	}

	public function testGetInstance()
	{
		$model = \Tapcrowd\Model\Channel::getInstance();
		$this->assertInstanceOf('\Tapcrowd\Model\AbstractPDOModel', $this->model);
		$this->assertInstanceOf('\Tapcrowd\Model\Channel', $this->model);
		return $model;
	}

	/**
	 * @dataProvider dataProvider_testGetCurrentChannel
	 * @runInSeparateProcess
	 */
	public function testGetCurrentChannel( $hostname, $channelId )
	{
		$_SERVER['HTTP_HOST'] = $hostname;
		$channel = $this->model->getCurrentChannel();
		$this->assertInstanceOf('\Tapcrowd\Structs\Channel', $channel);
		$this->assertEquals($channelId, $channel->channelId);
	}
	public function dataProvider_testGetCurrentChannel()
	{
		return array(
			array('admin.test.tapcrowd.com', 1),
			array('admin.staging.tapcrowd.com', 1),
			array('admin.ferdau.tapcrowd.com', 1),
			array('admin.axa.tapcrowd.com', 15),
		);
	}

// 	/**
// 	 * @dataProvider dataProvider_testGetCurrentChannel_error
// 	 * @expectedException \Tapcrowd\Model\InvalidChannelException
// 	 * @runInSeparateProcess
// 	 * @backupGlobals false
// 	 * @covers getCurrentChannel
// 	 */
// 	public function testGetCurrentChannel_error( $hostname )
// 	{
// 		$_SERVER['HTTP_HOST'] = $hostname;
// 		$this->model->getCurrentChannel();
// 	}
// 	public function dataProvider_testGetCurrentChannel_error()
// 	{
// 		return array(
// 			array('test.com'),
// 			array('tapcrowd.com'),
// 			array('domain.nx'),
// 		);
// 	}

	public function testGetChannelSettings()
	{
		$settings = $this->model->getChannelSettings($this->channel);
		$this->assertInstanceOf('ArrayObject', $settings);
		$this->assertNotEmpty($settings);
		foreach($settings as $name => $setting) {
			$this->assertInternalType('object', $setting);
			$this->assertObjectHasAttribute('value', $setting);
			$this->assertObjectHasAttribute('default', $setting);
			$this->assertEquals($setting->value, $setting->default);
		}
	}

	public function testSetChannelSetting()
	{
		$setting = (object)array('name'=>'dummy', 'value'=>'a random value', 'default'=>'dumbo');

		# Set
		$this->model->setChannelSetting($setting->name, $setting->value, $this->channel);
		$settings = $this->model->getChannelSettings($this->channel);
		$this->assertArrayHasKey($setting->name, (array)$settings);
		$this->assertEquals($setting->value, $settings[$setting->name]->value);

		# Unset
		$this->model->setChannelSetting($setting->name, null, $this->channel);
		$settings = $this->model->getChannelSettings($this->channel);
		$this->assertArrayHasKey($setting->name, (array)$settings);
		$this->assertEquals($setting->default, $settings[$setting->name]->value);
	}

	/**
	 * @expectedException \Tapcrowd\Model\InvalidChannelSettingException
	 */
	public function testSetChannelSetting_exception()
	{
		$setting = (object)array('name' => 'nonexisting', 'value' => 'a random value');
		$this->model->setChannelSetting($setting->name, $setting->value, $this->channel);
	}

	/**
	 * @covers \Tapcrowd\Model\Channel::getChannelSupport
	 */
	public function testGetChannelSupport_themes()
	{
		$themes = $this->model->getChannelSupport( 'theme', $this->channel);
		$this->assertInstanceOf('ArrayObject', $themes);
		$this->assertCount(0, $themes);
	}

	/**
	 * @covers \Tapcrowd\Model\Channel::getChannelSupport
	 */
	public function testGetChannelSupport_flavors()
	{
		$themes = $this->model->getChannelSupport( 'flavor', $this->channel);
		$this->assertInstanceOf('ArrayObject', $themes);
		$this->assertCount(0, $themes);
	}

	/**
	 * @dataProvider dataProvider_testSetChannelSupport
	 * @covers \Tapcrowd\Model\Channel::enableChannelSupport
	 * @covers \Tapcrowd\Model\Channel::disableChannelSupport
	 * @covers \Tapcrowd\Model\Channel::getChannelSupport
	 */
	public function testChannelSupport( $object )
	{
		$type = get_class();
		$r = $this->model->enableChannelSupport( $object, $this->channel);
		$this->assertTrue($r);

		$dataset = $this->model->getChannelSupport($type, $this->channel);
		$this->assertInstanceOf('ArrayObject', $dataset);
		$this->assertContains($object, $dataset);
		$class = '\Tapcrowd\Structs\\' . ucfirst($type);
		$this->assertContainsOnlyInstancesOf($class, $dataset);

		$r = $this->model->disableChannelSupport( $object, $this->channel);
		$this->assertTrue($r);

		$dataset = $this->model->getChannelSupport($type, $this->channel);
		$this->assertInstanceOf('ArrayObject', $dataset);
		$this->assertNotContains($object, $dataset);
	}
	public function dataProvider_testSetChannelSupport()
	{
		# Theme
		$theme = new \Tapcrowd\Structs\Theme;
		$theme->id = 17;
		$theme->name = 'Sweet';

		# Flavor
		$flavor = new \Tapcrowd\Structs\Flavor;
		$flavor->id = 3;
		$flavor->name = 'Pro Event Flavor';

		return array(array($theme),array($flavor));
	}
}
