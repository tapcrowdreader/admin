<?php
class GeocodingDeamonTest extends PHPUnit_Framework_TestCase
{
	protected $service;

	public function __construct()
	{
		ini_set('xdebug.cli_color', true);
		ini_set('xdebug.var_display_max_children', 100);
		ini_set('xdebug.var_display_max_depth', 100);
		$this->service = GeocodingDeamon::getInstance();
	}

	public function testCompleteFlow()
	{
		$this->service->getAddressAndSetLocation();
	}

	public function testGetLocationUsingGoogleMapsAPI( $address, $country )
	{
		$service = new GeocodingDeamon;
		$res = $service->getLocationUsingGoogleMapsAPI($address, $country);
		if(!is_array($res) || !is_numeric($res[0]) || !is_numeric($res[1])) {
			printf('Google Failed: '. json_encode($res) . "\n");
		} else {
			printf('Google Success: '. json_encode($res) . "\n");
		}
	}

	public function testGetLocationUsingMapQuestAPI( $address, $country )
	{
		$service = new GeocodingDeamon;
		$res = $service->getLocationUsingMapQuestAPI($address, $country);
		if(!is_array($res) || !is_numeric($res[0]) || !is_numeric($res[1])) {
			printf('MapQuest Failed: '. json_encode($res) . "\n");
		} else {
			printf('MapQuest Success: '. json_encode($res) . "\n");
		}
	}

	public function testGetLocationUsingBingAPI( $address, $country )
	{
		$service = new GeocodingDeamon;
		$res = $service->getLocationUsingBingAPI($address, $country);
		if(!is_array($res) || !is_numeric($res[0]) || !is_numeric($res[1])) {
			printf('Bing Failed: '. json_encode($res) . "\n");
		} else {
			printf('Bing Success: '. json_encode($res) . "\n");
		}
	}

	public function testGetLocationAPIs()
	{
		$addrs = array(
// 			array('Avenue de Maire 101, 7500 Doornik', 'BE'),
// 			array('Steendam 66, Gent', 'BE'),
// 			array('Grauwpoort 1 9000 Gent', 'BE'),
// 			array('Dudleystreet 4 Grimsby', 'UK'),
			array('Brussel , Belgium', 'BE'),
		);
		while(list($address, $country) = array_pop($addrs)) {
			printf("\n%s (%s)\n", $address, $country);
			$this->testGetLocationUsingGoogleMapsAPI($address, $country);
			$this->testGetLocationUsingMapQuestAPI($address, $country);
			$this->testGetLocationUsingBingAPI($address, $country);
		}
	}

	public function testGetLocation()
	{
		$api = null;
		$addrs = array(
// 			array('Avenue de Maire 101, 7500 Doornik', 'BE'),
// 			array('Steendam 66, Gent', 'BE'),
// 			array('Grauwpoort 1 9000 Gent', 'BE'),
// 			array('Dudleystreet 4 Grimsby', 'UK'),
			array('Brussel , Belgium', 'BE'),
			array('Anspachlaan 110 1000 Brussel, Brussel , Belgium', 'BE'),
		);
		while(list($address, $country) = array_pop($addrs)) {
			printf("\n%s (%s)\n", $address, $country);
			$res = $this->service->getLocation( $address, $country, $api );
			if(!is_array($res) || !is_numeric($res[0]) || !is_numeric($res[1])) {
				printf('Bing Failed: '. json_encode($res) . "\n");
			} else {
				printf('Success: '. json_encode($res) . "\n");
			}
		}
	}
}
