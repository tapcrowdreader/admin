// $(document).ready(function() {
// 	$('.tc-tooltip').tooltip();
// });
/* WIZARD */
$(".flavorselect").click(function(){
	step2(this.id);
});

$(".btnStep3").click(function(){
	step3();
});

$(".btnStep4").click(function(){
	step4();
});

$(".btnFinish").click(function(){
    
});

//js for step 2
$('#date1Event').datepicker( {
    'weekstart' : 1,
    format: 'dd-mm-yyyy'
});
$('#date2Event').datepicker( {
    'weekstart' : 1,
    format: 'dd-mm-yyyy'
});

$('#sameAsAppEvent').mousedown(function() {
    if(!$(this).is(':checked')) {
        $("#appNameEventDiv").val('');
        $("#appNameEventDiv").css('display','none');
    } else {
        $("#appNameEventDiv").css('display','block');
    }
});
$('#sameAsAppFestival').mousedown(function() {
    if(!$(this).is(':checked')) {
        $("#appNameFestivalDiv").val('');
        $("#appNameFestivalDiv").css('display','none');
    } else {
        $("#appNameFestivalDiv").css('display','block');
    }
});
$('#sameAsAppShop').mousedown(function() {
    if(!$(this).is(':checked')) {
        $("#appNameShopDiv").val('');
        $("#appNameShopDiv").css('display','none');
    } else {
        $("#appNameShopDiv").css('display','block');
    }
});
$('#sameAsAppRestaurant').mousedown(function() {
    if(!$(this).is(':checked')) {
        $("#appNameRestaurantDiv").val('');
        $("#appNameRestaurantDiv").css('display','none');
    } else {
        $("#appNameRestaurantDiv").css('display','block');
    }
});
// end of js for step 2

function step2(flavorid){
    $('#selectedFlavorid').val(flavorid);
    switch(flavorid) {
        case '3':
            $('#step2event').css('display', 'block');
            break;
        case '7':
            $('#step2shop').css('display', 'block');
            break;
        case '8':
            $('#step2resto').css('display', 'block');
            break;
        case '10':
            $('#step2festival').css('display', 'block');
            break;
    }
	$('.bar').animate({
        width: '34.3%'
    }, 700, function() {
        // Animation complete.
    });
    
    $('#wizardStep1').fadeOut('fast', function() {
        $('#wizardStep2').fadeIn('fast');
    });
}

$(".btnStep3Event").click(function(){
    var fields = $('#step2FormEvent').serializeArray();
    jQuery.each(fields, function(i, field){
        switch(field.name) {
            case 'appNameEvent':
                 $('#selectedAppName').val(field.value);
                 break;
            case 'eventName':
                 $('#selectedEventName').val(field.value);
                 break;
            case 'addressEvent':
                 $('#selectedAddress').val(field.value);
                 break;
            case 'date1Event':
                 $('#selectedStartdate').val(field.value);
                 break;
            case 'date2Event':
                 $('#selectedEnddate').val(field.value);
                 break;
        }
    });
});
$(".btnStep3Festival").click(function(){
    var fields = $('#step2FormFestival').serializeArray();
    jQuery.each(fields, function(i, field){
        switch(field.name) {
            case 'appNameFestival':
                 $('#selectedAppName').val(field.value);
                 break;
            case 'festivalName':
                 $('#selectedEventName').val(field.value);
                 break;
            case 'addressFestival':
                 $('#selectedAddress').val(field.value);
                 break;
            case 'date1Festival':
                 $('#selectedStartdate').val(field.value);
                 break;
            case 'date2Festival':
                 $('#selectedEnddate').val(field.value);
                 break;
        }
    });
});
$(".btnStep3Restaurant").click(function(){
    var fields = $('#step2FormRestaurant').serializeArray();
    jQuery.each(fields, function(i, field){
        switch(field.name) {
            case 'appNameRestaurant':
                 $('#selectedAppName').val(field.value);
                 break;
            case 'restaurantName':
                 $('#selectedVenueName').val(field.value);
                 break;
            case 'addressRestaurant':
                 $('#selectedAddress').val(field.value);
                 break;
        }
    });
});
$(".btnStep3Shop").click(function(){
    var fields = $('#step2FormShop').serializeArray();
    jQuery.each(fields, function(i, field){
        switch(field.name) {
            case 'appNameShop':
                 $('#selectedAppName').val(field.value);
                 break;
            case 'shopName':
                 $('#selectedVenueName').val(field.value);
                 break;
            case 'addressShop':
                 $('#selectedAddress').val(field.value);
                 break;
        }
    });
});

function step3(){
	$('.bar').animate({
        width: '68.6%'
    }, 700, function() {
        // Animation complete.
    });
    
    $('#wizardStep2').fadeOut('fast', function() {
        $('#wizardStep3').fadeIn('fast');
    });
}

function step4(){
    var fields = $('#step3Form').serializeArray();
    var languages = [];
    jQuery.each(fields, function(i, field){
        switch(field.name) {
            case 'mobileurl':
                 $('#selectedMobileurl').val(field.value);
                 break;
            case 'language[]':
                 languages.push(field.value);
                 break;
            case 'defaultlang':
                 $('#selectedDefaultLanguage').val(field.value);
                 break;
        }
    });
    $('#selectedLanguages').val(languages.join(','));

    $.post("wizard", $("#creatorform").serialize(), function(data) {
        $('#btnFinish').attr('href', 'http://'+location.hostname+'/apps/view/'+data);
     });

	$('.bar').animate({
        width: '100%'
    }, 700, function() {
        // Animation complete.
    });
    
    $('.progress').addClass('progress-success');
    
    $('#wizardStep3').fadeOut('fast', function() {
        $('#wizardStep4').fadeIn('fast');
    });
}

var geocoder;
var map;
var bounds;
var marker;
function codeAddress() {
    var address = document.getElementById('address').value;
    if(address !== '') {
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if(marker !== null){
                    marker.setMap(null);
                }
                map.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });

                $('#lat').val(results[0].geometry.location.lat());
                $('#lon').val(results[0].geometry.location.lng());

                marker.draggable = true;
                google.maps.event.addListener(marker, 'drag', function() {
                    $('#lat').val(marker.getPosition().lat());
                    $('#lon').val(marker.getPosition().lng());
                });

                bounds = new google.maps.LatLngBounds();
                bounds.extend(results[0].geometry.location);
                map.fitBounds(bounds);
                map.setZoom(map.getZoom()-1);
            } else {
                if(status === 'ZERO_RESULTS'){
                    alert("<?= __('No results found on the address ...')?>");
                } else {
                    alert("<?= __('Geocode was not successful for the following reason: ')?> + status");
                }
            }
        });
    }
}

$(".languageCheckbox").click(function(event) {
    var value = $(this).attr('value');
    var text = $(this).next('span').text();
    if($(this).is(':checked')) {
        changeDefaultLanguages(value,text, true);
    } else {
        changeDefaultLanguages(value,text, false);
    }
    
});
function changeDefaultLanguages(value,text, check) {
    if(check) {
        $('#defaultlang').append(
            $('<option></option>').val(value).html(text)
        );
    } else {
        $("#defaultlang option[value='"+value+"']").remove();
    }
}


//OVERALL CMS FUNCTIONALITY


/* ### INIT DATATABLE ### */
var icon_columns = new Array();

if($('#listview')){
    var tab = $('#listview');
    for(var i = 0; i < (tab.find('th').length - tab.find('th.data').length); i++){
        icon_columns.push({ "bSortable": false });
    }
}
$('#listview').dataTable({
    "bPaginate": true,
    "iDisplayLength" : 25,
    "bLengthChange": true,
    "bFilter": true,
    "bSort": true,
    "bInfo": true,
    "bAutoWidth": false,
    "oLanguage": {
        "sProcessing":   "Loading...",
        "sLengthMenu":   "Show _MENU_ rows",
        "sZeroRecords":  "No results...",
        "sInfo":         "Row _START_ to _END_ of _TOTAL_ rows",
        "sInfoEmpty":    "Row 0 to 0 of 0 rows",
        "sInfoFiltered": "(filtered from _MAX_ rows in total)",
        "sSearch":       "Search:",
        "oPaginate": {
            "sFirst":    "First",
            "sPrevious": "Previous",
            "sNext":     "Next",
            "sLast":     "Last"
        }
    },
    "bStateSave": true,
    "iCookieDuration": 600,
    "aoColumns": $.merge($('#listview').find('th.data').get(), icon_columns)
});

$('#datefrom').datepicker( {
    'weekstart' : 1,
    format: 'dd-mm-yyyy'
});
$('#dateto').datepicker( {
    'weekstart' : 1,
    format: 'dd-mm-yyyy'
});

