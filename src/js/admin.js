jQuery(document).ready(function(){

$('.typeahead').each(function()
{
	var input = $(this).focus();
	var select = $(this.form).find('select');
	var accountId_input = this.form.account_id;
	var submit = $(this.form).find('button').get(0);
	var renderTimeout;

	// Options are kept in a haystack because of how eg. Chrome ignores jQuery.hide() calls
	// on options elements.
	var options = $('<select />');

	/**
	 * Display Accountlist callback
	 */
	var hideAccountList = function()
	{
		select.hide();
		input.focus();
	}

	/**
	 * Display Accountlist callback
	 */
	var displayAccountList = function( focus )
	{
		var pos = input.position();
		select.css({
			position: 'absolute',
			top:  (pos.top + (input.outerHeight())),
			left: pos.left,
		}).show();

		// If focus isset, select first visible option
		if(focus === true) {
			var opt = select.focus().children();
			if(opt.length > 0) opt.get(0).selected = true;
			else hideAccountList();
		}
	};

	/**
	 * Select account
	 */
	var selectAccount = function()
	{
		var opt = select.find('option:selected');
		input.val(opt.text());
		accountId_input.value = opt.val();
		submit.disabled = false;
		hideAccountList();
	}

	/**
	 * (De)Select account
	 */
	var deselectAccount = function()
	{
		accountId_input.value = '';
		submit.disabled = 'disabled';
	}

	/**
	 * Search callback
	 */
	var renderCallback = function()
	{
		var value = input.val().toLowerCase();

		if(value.length == 0) {
			hideAccountList();
			deselectAccount();
			return;
		}

		select.html('');
		for(var i in options.get(0).options) {
			var n = options.get(0).options[i];
			if(typeof n.text != "string") continue;

			if(n.text.toLowerCase().indexOf(value) != -1) {
				$(n).clone().appendTo(select);
			}
		}

		deselectAccount();
		displayAccountList();
	};

	/**
	 * Generate account list
	 */
	(function(options, input)
	{
		var d = JSON.parse($(input).attr('data-source'));
		var i, key, value;
		for(i in d) {
			key, value = d[i];
			if(d[i].indexOf('|') > 0) {
				var a = d[i].split('|');
				key = a[0];
				value = a[1];
			}
			$('<option value="'+key+'">'+value+'</option>').appendTo(options);
		}

		// Clone options into select by default
		options.children().clone().appendTo(select);
	}(options, input));

	// Input Events
	input.keyup(function(event)
	{
		if(event.keyCode == 40) {           // Down Arrow
			displayAccountList(true);
		}

		// Don't update on arrows (37, 38, 40, 39)
		if(event.keyCode >= 37 && event.keyCode <= 40) return;

		clearTimeout(renderTimeout);
		renderTimeout = setTimeout(renderCallback, 300);
	});

	// Select Events
	select.keyup(function(event)
	{
		if(event.keyCode == 13) {          // Enter
			selectAccount();
		}
		else if(event.keyCode == 27) {    // Escape
			hideAccountList();
		}
	}).dblclick(selectAccount).blur(hideAccountList).keydown(function(event)
	{
		if(event.keyCode == 38) {         // Up Arrow
			if(select.find('option:visible:first').is(select.find('option:selected'))) {
				hideAccountList();
			}
		}
	});
});
});
