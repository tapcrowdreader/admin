$(document).ready(function() {
	
	// Fadeout
	//$('.fadeout').delay(500).slideUp();
	$('.infobar').delay(7500).slideUp();
	$('.infobar').click(function(e){
		$('.infobar').clearQueue().slideUp();
	});
	
	// Labelify
	$("#frmregister input, #frmlogin input").labelify();
	$('#frmregister input.password, #frmlogin input.password').focus(function(e){
		$(this).attr("type", "password");
	}).blur(function(e){
		if($(this).val() == '') {
			$(this).attr("type", "text");
		}
	});
	
});
