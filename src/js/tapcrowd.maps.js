/**
 * Tapcrowd Javascript Floorplan + Google Maps Library
 * @author Tom Van de Putte
 * @date 2013-02-28
 *
 *
 */
(function(){


var MarkerMaps = {};

/**
 * Parses Google Map Options from the given url
 * @see https://developers.google.com/maps/
 */
MarkerMaps.parseGoogleMapOptionsFromUrl = function( url )
{
	// Default Map Options
	var mapOptions = {
		zoom: 2,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: new google.maps.LatLng(0,0),
	};

	// Parse url into map options
	var search = $('<a href="'+url+'">').get(0).search.substring(1).split('&');
	for(var i in search) {
		var p = search[i].split('=');
		switch(p[0]) {
			case 'z': 					// Zoom level
				mapOptions.zoom = parseInt(p[1]);
				break;
			case 'll': 					// Center point
				var ll = p[1].split(',');
				mapOptions.center = new google.maps.LatLng(ll[0],ll[1]);
				break;
			case 't': 					// Map type
				var mtypes = {m:'ROADMAP',s:'SATELLITE',h:'HYBRID',t:'TERRAIN'};
				mapOptions.mapTypeId = google.maps.MapTypeId[ mtypes[p[1]] ];
				break;
		}
	}

	return mapOptions;
}

/**
 * Renders a Map
 *
 * Either a Floorplan Map using the iViewer plugin
 * @see https://github.com/can3p/iviewer
 *
 * Or a Google Map from the given url
 * @see https://developers.google.com/maps/
 */
MarkerMaps.createMapFromContainer = function( container )
{
	// Parse url into model
	var url = $(container).find('img').attr('src');
	$(container).find('img').remove();

	if(url.indexOf('maps.google.com') != -1) {
		var mapOptions = MarkerMaps.parseGoogleMapOptionsFromUrl(url);
		var model = new google.maps.Map( container, mapOptions);
	} else {
		var model = $(container).iviewer({ src: url, });
	}

	// Create map instance
	var map = new MarkerMaps.Map(model);

	return map;
}

MarkerMaps.Map = function( model )
{
	this.model = model;

	this.markers = [];

// 	this.addMarkerFromAddress = function( address )
// 	{
// 		this.codeAddress(address);
// 	}

	this.init = function()
	{
		//
	}
}

MarkerMaps.Map.prototype = {

	/**
	 * codeAddress: Translates (string) address to Location object
	 */
	codeAddress : function( address )
	{
		var map = this;
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode( {'address': address}, function(results, status)
		{
			if(status == google.maps.GeocoderStatus.OK) {
				var loc = results[0].geometry.location;
				map.setMarker(loc);
			} else {
				if(status == 'ZERO_RESULTS') {
					alert('Could not find any results for given address');
				} else {
					alert('Geocode was not successful for the following reason: ' + status);
				}
			}
		});
	},

	setMarker : function( location, marker )
	{
		var marker;
		if(this.model instanceof google.maps.Map) {
			marker = new google.maps.Marker({map: this.model, draggable: true});
		} else {
			marker = $('<a href="#"><i class="icon-map-marker"></i></a>');
		}

		console.log(marker, location);
		marker.setPosition(location);

	},
}

/**
 * Create jQuery Plugin
 */
$.fn.markerMap = function( method )
{
	var method = method || 'init';
	var args = Array.prototype.slice.call( arguments, 1);

	return this.each(function()
	{
		// Get/Set map
		var map = $(this).data('markermap');
		if(typeof map == 'undefined') {
			map = MarkerMaps.createMapFromContainer(this)
			$(this).data('markermap', map);
		}

		// Add public callbacks
		if( typeof map[method] == 'function' ) {
			return map[method].apply( map, args);
		} else if ( typeof method === 'object' || ! method ) {
			return map.init.apply( map, args );
		} else {
			$.error( 'Method ' +  method + ' does not exist' );
		}
	});
}

$(document).ready(function(){
	$('div.tc_markermap').markerMap();
});


// Onload
// $(document).ready(function(){
// 	$('div.tc_markermap').each(function()
// 	{
// 		// Get Url and remove img tag
// 		var url = $(this).find('img').attr('src');
// 		$(this).find('img').remove();
//
// 		// Render Map
// 		MarkerMaps.createMapFromUrl(url, this);
// 	});
// });


/**
 * Google Maps integration
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.8/reference
 */
// (function(){
// 	var map, geocoder, marker, input_lat, input_lng, input_addr;

	/**
	 * Initiate map, geocoder and marker
	 */
// 	function initialize()
// 	{
// 		var mapOptions = {
// 			zoom: 8,
// 			center: new google.maps.LatLng( 50.753887, 4.269936),
// 			mapTypeId: google.maps.MapTypeId.ROADMAP
// 		};
// 		map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
// 		geocoder = new google.maps.Geocoder();
// 		marker = new google.maps.Marker({map: map, draggable: true});
// 		google.maps.event.addListener(marker, 'dragend', updateInputFields);
//
// 		// Set DOMNodes
// 		input_lat = document.getElementById('input_lat');
// 		input_lng = document.getElementById('input_lng');
// 		input_addr = document.getElementById('input_addr');
//
// 		// Update map if any input fields are already set
// 		if(parseFloat(input_lat.value) != 0 && parseFloat(input_lng.value) != 0) {
// 			updateMarker( new google.maps.LatLng(input_lat.value, input_lng.value) );
// 		}
// 		else if(input_addr.value != '') {
// 			codeAddress();
// 		}
// 	}

	/**
	 * Show address on map
	 * @param string address
	 */
// 	function codeAddress( address )
// 	{
// 		var address = input_addr.value;
// 		geocoder.geocode( {'address': address}, function(results, status)
// 		{
// 			if(status == google.maps.GeocoderStatus.OK) {
// 				var loc = results[0].geometry.location;
// 				updateMarker(loc)
// 				updateInputFields(loc);
// 			} else {
// 				if(status == 'ZERO_RESULTS') {
// // 					alert('<?=__('Could not find any results for given address')?>');
// 				} else {
// // 					alert('<?=__('Geocode was not successful for the following reason: ')?>' + status);
// 				}
// 			}
// 		});
// 	}

	/**
	 * Update Marker
	 * @param google.maps.LatLng location
	 */
// 	function updateMarker( location )
// 	{
// 		map.setCenter(location);
// 		marker.setPosition(location);
// 		map.setZoom(16);
// 	}

	/**
	 * Method to update form input fields
	 */
// 	function updateInputFields()
// 	{
// 		if(typeof(marker) != 'object') return;
// 		var location = marker.getPosition();
// 		document.getElementById('input_lat').value = location.lat();
// 		document.getElementById('input_lng').value = location.lng();
// 	}

	// Append event listeners
// 	google.maps.event.addDomListener(window, 'load', initialize);
// 	document.getElementById('show_location_button').onclick = codeAddress;
// })();


})();
