$(document).ready(function() {

/**
 * Google Maps integration
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.8/reference
 */
	var map, geocoder, marker, input_lat, input_lng, input_addr;

	/**
	 * Initiate map, geocoder and marker
	 */
	function initialize()
	{
		var mapOptions = {
			zoom: 8,
			center: new google.maps.LatLng( 50.753887, 4.269936),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		geocoder = new google.maps.Geocoder();
		marker = new google.maps.Marker({map: map, draggable: true});
		google.maps.event.addListener(marker, 'dragend', updateInputFields);

		// Set DOMNodes
		input_lat = document.getElementById('input_lat');
		input_lng = document.getElementById('input_lng');
		input_addr = document.getElementById('input_addr');

		// Update map if any input fields are already set
		if(parseFloat(input_lat.value) != 0 && parseFloat(input_lng.value) != 0) {
			updateMarker( new google.maps.LatLng(input_lat.value, input_lng.value) );
		}
		else if(input_addr.value != '') {
			codeAddress();
		}
	}

	/**
	 * Show address on map
	 * @param string address
	 */
	function codeAddress( address )
	{
		var address = input_addr.value;
		geocoder.geocode( {'address': address}, function(results, status)
		{
			if(status == google.maps.GeocoderStatus.OK) {
				var loc = results[0].geometry.location;
				updateMarker(loc)
				updateInputFields(loc);
			} else {
				if(status == 'ZERO_RESULTS') {
					alert('Could not find any results for given address');
				} else {
					alert('Geocode was not successful for the following reason: ' + status);
				}
			}
		});
	}

	/**
	 * Update Marker
	 * @param google.maps.LatLng location
	 */
	function updateMarker( location )
	{
		map.setCenter(location);
		marker.setPosition(location);
		map.setZoom(16);
	}

	/**
	 * Method to update form input fields
	 */
	function updateInputFields()
	{
		if(typeof(marker) != 'object') return;
		var location = marker.getPosition();
		document.getElementById('input_lat').value = location.lat();
		document.getElementById('input_lng').value = location.lng();
	}

	// Append event listeners
	google.maps.event.addDomListener(window, 'load', initialize);
	if($("#show_location_button").is('*')) {
		document.getElementById('show_location_button').onclick = codeAddress;
	}
	
});