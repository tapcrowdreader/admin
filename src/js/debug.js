var debug = {
	info: function(msg) {
		debug.log(msg, 'info');
	},
	
	error: function(msg) {
		debug.log(msg, 'error');
	},
	
	warn: function(msg) {
		debug.log(msg, 'warning');
	},
	
	log: function(msg, level) {
		if (console && console.log) {
			console.log("[" + level + "] " + msg);
		}
		$("#pqp-console tbody").append("<tr class=\"log-" + level + "\">" +
				"<td class=\"type\">" + level + "</td>" +
				"<td class=\"details\">" + "" + "</td>" +
				"<td>" + msg + "</td>" +
				"<td class=\"details\">" + "" + "</td>" +
				"</tr>");
		$("#debugCount").html(parseInt($("#debugCount").html()) + 1);
	}
};


$(document).ready(function(){
	$("#pqp-metrics").click(function(){
		$("#pqp-console").toggle();
	});
});