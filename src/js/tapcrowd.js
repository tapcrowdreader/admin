$(document).ready(function() {
	jQuery.expr[':'].iContains = function(a, i, m) {
	  return jQuery(a).text().toUpperCase()
	      .indexOf(m[3].toUpperCase()) >= 0;
	};
	/* ### SIDEBAR ### */
	/*$('#contentwrapper').css({"height": $("#wrapper").height() - $('#header').height() - ($('#footer').height() +20)});*/
	/*$('#sidebar').css({"height": $("#contentwrapper").height() + ($('#footer').height() +20)});*/
// 	$(window).resize(function() {
		/*$('#contentwrapper').css({"height": $("#wrapper").height() - $('#header').height() - ($('#footer').height() + 20)});*/
		/*$('#sidebar').css({"height": $("#contentwrapper").height() + ($('#footer').height() +20)});*/
// 	});

	var sidebarToe = false;

	/* ### GLOBAL FUNCTIONS ### */

	$('.topbar').dropdown();
	$('.dropdown-toggle').dropdown();

	$('.fadeout').delay(5000).slideUp(400);
	$('.datepicker').datepicker({ firstDay: 1, dateFormat: 'dd-mm-yy' });
	$('.timepicker').timepicker({ timeFormat: 'hh:mm' });
	$('.datetimepicker').datetimepicker({ dateFormat: 'dd-mm-yy' });

	/* ### INIT DATATABLE ### */
	var icon_columns = new Array();

	if($('#listview').length){
		var tab = $('#listview');
		for(var i = 0; i < (tab.find('th').length - tab.find('th.data').length); i++){
			icon_columns.push({ "bSortable": false });
		}
	}

// 	if($('#listview_translation')){
// 		var tab = $('#listview_translation');
// 		for(var i = 0; i < (tab.find('th').length - tab.find('th.data').length); i++){
// 			icon_columns.push({ "bSortable": false });
// 		}
// 	}

// 	if($('#listview_analytics')){
// 		var tab = $('#listview_analytics');
// 		for(var i = 0; i < (tab.find('th').length - tab.find('th.data').length); i++){
// 			icon_columns.push({ "bSortable": false });
// 		}
// 	}

	var opts = {
	  lines: 12, // The number of lines to draw
	  length: 7, // The length of each line
	  width: 4, // The line thickness
	  radius: 10, // The radius of the inner circle
	  color: '#fff', // #rgb or #rrggbb
	  speed: 1, // Rounds per second
	  trail: 60, // Afterglow percentage
	  shadow: false // Whether to render a shadow
	};
	var target = document.getElementById('preloader');
	var spinner = new Spinner(opts).spin(target);

	$('#listview').dataTable({
		"bPaginate": true,
		"iDisplayLength" : 25,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"oLanguage": {
			"sProcessing":   "Loading...",
			"sLengthMenu":   "Show _MENU_ rows",
			"sZeroRecords":  "No results...",
			"sInfo":         "Row _START_ to _END_ of _TOTAL_ rows",
			"sInfoEmpty":    "Row 0 to 0 of 0 rows",
			"sInfoFiltered": "(filtered from _MAX_ rows in total)",
			"sSearch":       "Search:",
			"oPaginate": {
				"sFirst":    "First",
				"sPrevious": "Previous",
				"sNext":     "Next",
				"sLast":     "Last"
			}
		},
		"bStateSave": true,
		"iCookieDuration": 600,
		"aoColumns": $.merge($('#listview').find('th.data').get(), icon_columns)
	});

// 	$('#listview_translation').dataTable({
// 		"bPaginate": true,
// 		"iDisplayLength" : 25,
// 		"bLengthChange": true,
// 		"bFilter": true,
// 		"bSort": true,
// 		"bInfo": true,
// 		"bAutoWidth": false,
// 		"oLanguage": {
// 			"sProcessing":   "Loading...",
// 			"sLengthMenu":   "Show _MENU_ rows",
// 			"sZeroRecords":  "No results...",
// 			"sInfo":         "Row _START_ to _END_ of _TOTAL_ rows",
// 			"sInfoEmpty":    "Row 0 to 0 of 0 rows",
// 			"sInfoFiltered": "(filtered from _MAX_ rows in total)",
// 			"sSearch":       "Search:",
// 			"oPaginate": {
// 				"sFirst":    "First",
// 				"sPrevious": "Previous",
// 				"sNext":     "Next",
// 				"sLast":     "Last"
// 			}
// 		},
// 		"bStateSave": true,
// 		"iCookieDuration": 600,
// 		"aaSorting": [[5,"asc"]],
// 		"aoColumns": $.merge($('#listview_translation').find('th.data').get(), icon_columns)
// 	});

// 	$('#listview_analytics').dataTable({
// 		"aaSorting": [[ 0, "desc" ]],
// 		"bPaginate": true,
// 		"iDisplayLength" : 15,
// 		"bLengthChange": true,
// 		"bFilter": true,
// 		"bSort": true,
// 		"bInfo": true,
// 		"bAutoWidth": false,
// 		"oLanguage": {
// 			"sProcessing":   "Loading...",
// 			"sLengthMenu":   "Show _MENU_ rows",
// 			"sZeroRecords":  "No results...",
// 			"sInfo":         "Row _START_ to _END_ of _TOTAL_ rows",
// 			"sInfoEmpty":    "Row 0 to 0 of 0 rows",
// 			"sInfoFiltered": "(filtered from _MAX_ rows in total)",
// 			"sSearch":       "Search:",
// 			"oPaginate": {
// 				"sFirst":    "First",
// 				"sPrevious": "Previous",
// 				"sNext":     "Next",
// 				"sLast":     "Last"
// 			}
// 		},
// 		"bStateSave": true,
// 		"iCookieDuration": 600,
// 		//"aoColumns": $.merge($('#listview_analytics').find('th.data').get(), icon_columns)
// 		"aoColumns": [
// 			null,
// 			{ "sType": 'numeric' }
// 		]
// 	});

	$('#listview_speakers').dataTable({
		"bPaginate": true,
		"iDisplayLength" : 25,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"oLanguage": {
			"sProcessing":   "Loading...",
			"sLengthMenu":   "Show _MENU_ rows",
			"sZeroRecords":  "No results...",
			"sInfo":         "Row _START_ to _END_ of _TOTAL_ rows",
			"sInfoEmpty":    "Row 0 to 0 of 0 rows",
			"sInfoFiltered": "(filtered from _MAX_ rows in total)",
			"sSearch":       "Search:",
			"oPaginate": {
				"sFirst":    "First",
				"sPrevious": "Previous",
				"sNext":     "Next",
				"sLast":     "Last"
			}
		},
		"bStateSave": true,
		"iCookieDuration": 600,
		"aoColumns": $.merge($('#listview').find('th.data').get(), icon_columns)
	});

	/* ### EVENT FUNCTIONS ### */

	// ### MODULE INSTALL ### //
	$('body').on("click", ".module.inactive a.activate", function(e){
		var id = $(this).attr('id');
		var url = $(this).attr('href');
		e.preventDefault();
		if(id != 'activatePush' && id != 'activateForm' && id != 'form' && id != 'activateMeetMe') {
			$.ajax({
				type: "POST",
				url: url
			}).done(function( data ) {
				var content = $( data ).find( '.modules' );
				$('.modules').html(content);
				var menu = $( data ).find( '.modules-list');
				$('.modules-list').html(menu);
				$('#iframe').attr('src', $('#iframe').attr('src'));
			});
		} else if(id == 'activateForm') {
			window.location = url;
			return true;
		} else if(id == 'activateMeetMe') {
			jConfirm('This module is used in combination with the "Attendee" and "Messaging" and "User" module. Would you like to activate these 4 modules?', 'Meet Me Module', function(r) {
				if(r === true) {
					$.ajax({
						type: "POST",
						url: url
					}).done(function( data ) {
						var content = $( data ).find( '.modules' );
						$('.modules').html(content);
						var menu = $( data ).find( '.modules-list');
						$('.modules-list').html(menu);
						$('#iframe').attr('src', $('#iframe').attr('src'));
					});
				} else {
					return false;
				}
			});
		} else {
			alert('Your certificate to enable push notifications will be created within 24hours on business days.');
			$.ajax({
				type: "POST",
				url: url
			}).done(function( data ) {
				var content = $( data ).find( '.modules' );
				$('.modules').html(content);
				var menu = $( data ).find( '.modules-list');
				$('.modules-list').html(menu);
				$('#iframe').attr('src', $('#iframe').attr('src'));
			});
			return true;
		}
	});

	// ### MODULE DEINSTALL ### //
	$('body').on("click", ".module.active a.activate.delete", function(e){
		var id = $(this).attr('id');
		if(id != 'form') {
			var url = $(this).attr('href');
			e.preventDefault();

			var delurl = $(this).attr('href');
			jConfirm('If you disable this module, the content will no longer appear in your app. Are you sure?', 'Deactivate Module', function(r) {
				if(r === true) {
					$.ajax({
						type: "POST",
						url: delurl
					}).done(function( data ) {
						var content = $( data ).find( '.modules' );
						$('.modules').html(content);
						var menu = $( data ).find( '.modules-list');
						$('.modules-list').html(menu);
						$('#iframe').attr('src', $('#iframe').attr('src'));
					});
				} else {
					return false;
				}
			});
		}
	});

	//sidebar height
	// $("#sidebar").height($('#content').innerHeight());

	//prevent double submit of forms
	// $('.frmsessions').submit(function() {
	// 	var $submitButton = $(this, "button[type='submit']");
	// 	$submitButton.attr("disabled", "true");
	// });


	//premium checkboxes
	if($('.premiumCheckbox').is(':checked')) {
		$("#extralinep").css('display','block');
	}
	$('.premiumCheckbox').mousedown(function() {
        if (!$(this).is(':checked')) {
            $("#extralinep").css('display','block');
        } else {
			$("#extralinep").css('display','none');
        }
    });

	var sidebar = $("#sidebar");
	if(sidebar.length) {
		var previewheight = $("#sidebar").height();
		var contentheight = $("#contentwrapper").height();
		if(previewheight < contentheight) {
			$("#sidebar").css('height', contentheight + 50);
		}
	}

    $('.tooltip2').tooltip({'placement':'right'});


	/**
	 * @see http://stackoverflow.com/a/901144
	 */
	window.getParameterByName = function(name)
	{
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.search);
		if(results == null)
			return "";
		else
			return decodeURIComponent(results[1].replace(/\+/g, " "));
	}

});


/**
 * Translation table functions
 */
$(document).ready(function(){

/**
 * Tapcrowd Table Engine constructor
 */
function tcTable( table )
{
	this.jqtable = $(table);
	this.tablerows = this.jqtable.find('tbody tr');
	this.index = {};

	this.num_rows = 25;
	this.compactIndex = true;

	/**
	 * Display loader
	 */
	// this.loader = $('\
	// <div class="alert alert-info span8" style="position:absolute;top:200px;left:300px;">\
	// 	<div class="progress"><div class="bar" style="width:30%;"></div></div>\
	// 	<p>Loading table data<p>\
	// </div>').insertAfter(this.jqtable);
// 	this.loader.css(this.jqtable.position());
// 	this.jqtable.hide();
// 	console.timeEnd('table-engine mid');
	/**
	 * Current page index
	 * @var int
	 */
	this.currentPage = 1;

	this.searchcallback = (function(table){return function(e)
	{
		e.preventDefault();
		table.filter(this.filter.value, '~', this.q.value);
		var value = $('<div/>').text(this.q.value).html();
		table.showhide(0, (value == '')? '' : 'Search: <b>'+value+'</b>; ');
		table.pageindex();
		return false;
	}})(this);

	this.filtercallback = (function(table){return function(key, value)
	{
		table.filter(key, '=', value);
		table.showhide(0, 'Filter '+key+':<b>'+value+'</b> ');
		table.pageindex();
	}})(this);

	var formsearches = this.jqtable.find('thead form.form-search');
	if(!formsearches.length) {
		formsearches = this.jqtable.find('thead form.formsearch');
	}
	if(formsearches.length) {
		formsearches.submit(this.searchcallback);
	}

	// Sort callback
	this.jqtable.find('thead th.sort-num, thead th.sort-alfa').each((function(tctable){return function()
	{
		$(this).css({cursor:'pointer','text-decoration':'underline'});
		$(this).click((
			function(header, tctable)
			{
				return function()
				{
					tctable.sortColumn(header);
				}
			})(this,tctable)
		);
	}})(this));

	// this.loader.find('.bar').css({width:'10%'});

	this.showhide(0);
	// this.loader.find('.bar').css({width:'50%'});

	this.pageindex();
	// this.loader.find('.bar').css({width:'90%'});

	this.createindex();
	// this.loader.find('.bar').css({width:'100%'});

	// this.loader.fadeOut('slow');
}

tcTable.prototype = {

	/**
	 * Create showhide function
	 */
	showhide: function( offset, infotext )
	{
		if(isNaN(offset)) return false;
		var row_count = this.tablerows.length;
		var end = parseInt(offset) + this.num_rows;
		if(end > row_count) end = row_count;

		var selection = this.tablerows.hide().slice(offset,end).show();
		this.jqtable.find('.infobox').html((infotext || '') + 'Results '+(offset+1)+'-'+end+' of ' + row_count);

		// Update even/odd classes
		var _class = [['even','odd'],['odd','even']];
		for(var i = 0; i < selection.length; i++) {
			$(selection[i]).removeClass(_class[i%2][0]).addClass(_class[i%2][1]);
		}
	},

	sortColumn: function( header )
	{
		var cellIndex = header.cellIndex;
		var body = this.jqtable.children('tbody');

		var dir = (!!(header.className.indexOf('sort-asc')+1))? 'sort-desc' : 'sort-asc';
		$(header).removeClass('sort-asc').removeClass('sort-desc').addClass(dir);

		//console.log(this.tablerows.slice(0,5));
		if($(header).hasClass('sort-num')) {
			this.tablerows.sort(function(a,b)
			{
				var a = $(a.cells[cellIndex]).text(), b = $(b.cells[cellIndex]).text();

				if(isNaN(a)) a = a.length; if(isNaN(b)) b = b.length;
	// 			console.log(a,b);

	// 			if(isNaN(a)) a = 0; if(isNaN(b)) b = 0;
				return (dir=='sort-desc')? b-a : a-b;
			});
		} else if($(header).hasClass('sort-alfa')) {
			this.tablerows.sort(function(a,b)
			{
				var a = $(a.cells[cellIndex]).text(), b = $(b.cells[cellIndex]).text();

				// if(isNaN(a)) a = a; if(isNaN(b)) b = b;
	// 			console.log(a,b);

	// 			if(isNaN(a)) a = 0; if(isNaN(b)) b = 0;
				// return (dir=='sort-desc')? b-a : a-b;
				var x = a.toLowerCase();
				var y = b.toLowerCase();
				if(dir=='sort-desc') {
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				} else {
					return ((x > y) ? -1 : ((x < y) ? 1 : 0));
				}

			});
		}
		//console.log(this.tablerows.slice(0,5));
		$.each(this.tablerows, function(index, row){body.append(row);});
		this.showhide(0);
		this.pageindex();
	},

	/**
	 * Create filter function
	 */
	filter: function( key, mode, query )
	{
		if(query == '') this.tablerows = this.jqtable.find('tbody tr').show();
		else if(!this.index[key]) {
			this.tablerows.hide();
			this.tablerows = this.jqtable.find('tbody tr:iContains(' + query + ')').show();	// Fallback when index is not available
		} else {
			var mode = mode || '~';
			query = query.toLowerCase();
			this.tablerows.hide();
			this.tablerows = $([]);

			switch(mode) {
				case '~':
					for(var v in this.index[key]) {
						if(v.indexOf(query) != -1) {
							this.tablerows = this.tablerows.add( this.index[key][v] );
						}
					}
					break;
				case '=':
					this.tablerows = $(this.index[key][query]);
					break;
			}
			this.tablerows.show();
		}
	},

	createindex: function()
	{
		this.jqtable.find('tbody tr').each((function(tctable){return function()
		{
			var keys = $(this).find('form.table-filter input');
			var name, value;
			for(var i = 0; i < keys.length; i++) {
				name = keys[i].name.toLowerCase();
				value = keys[i].value.toLowerCase();
				if(!tctable.index[name]) tctable.index[name] = {};
				if(!tctable.index[name][value]) tctable.index[name][value] = [];
				tctable.index[name][value].push(this);
			}
		}})(this));
	},

	/**
	 * Add page index
	 */
	pageindex: function()
	{
		var pageIndex = $('<ul />').appendTo(this.jqtable.find('div.pagination').html(''));
		var row_count = this.tablerows.length;

		if(row_count <= this.num_rows) return;		// No need to draw index

// 		pageIndex.append('<li class="disabled first"><a href="#">«</a></li>');
		var p = 1,  selected;
		for(var c = 0; c < row_count; c++) {
			if((c % this.num_rows) == 0) {
				selected = (p == this.currentPage)? ' class="active"' : '';
				pageIndex.append('<li'+selected+'><a href="#">' + (p++) + '</a></li>');
			}
		}
// 		pageIndex.append('<li class="last"><a href="#">»</a></li>');

		// Compact Index
		var page_count = p-1;
		var max_visible_pages = 12;
// 		if(this.compactIndex && (page_count > max_visible_pages)) {
// 			var start = (max_visible_pages/2)+1;
// 			var stop = (page_count-start)+2;
//
// 			// Hide midrange, add midrange space
// 			$(pageIndex.children().slice(start,stop)).hide();
// 			$(pageIndex.children()[start-1]).after('<li class="disabled"><a href="#">...</a></li>');
// 		}

		//
		// Onclick callback
		//
		pageIndex.find('a').click(
			(function(tctable){return function(e)
			{
				e.preventDefault();

				// Skip disabled and active
				var parent = $(this).parent();
				if(parent.hasClass('disabled') || parent.hasClass('active')) return;

				// Get requested page
				var page = $(this).text();
				if(isNaN(page)) return;
				tctable.currentPage = page;		// Update current page index

				// Update
				tctable.showhide( (page-1) * tctable.num_rows );
				parent.siblings('li.active').removeClass('active');
				parent.addClass('active');
			}})(this)
		);
	}
}

/**
 * Add table engines
 */
// console.time('table-engine loaded');
$('table.table-engine').each(function()
{
// 	console.time('table-engine mid');
	this.tctable = new tcTable(this);
});
// console.timeEnd('table-engine loaded');
/**
 * Add confirm on destructive buttons
 */
// $('.btn-danger').click(function(e)
// {
// 	if(!confirm('Are you sure? This can\'t be undone!')) return false;
// });

/**
 * Accesability / Usability guides
 */
$('.tc_bilities a.btn').click(function()
{
	// Destructive button
	if($(this).hasClass('btn-danger')) {
		if(!confirm('Are you sure? This can\'t be undone!')) return false;
	}

	//console.log(this.href);

	// Get modal box
	var tc_bilities_box = $('#tc_bilities_box');

	// Get url + Add Frame parameter
	var href = this.href + '?frame=1';

	// Perform request
	$.get(href, function(response)
	{
		tc_bilities_box.find('.modal-body').html(response);
		tc_bilities_box.removeClass('fade').show();

		// modal hide fade
	});

	return false;
});

$(".confirm").click(function(e) {
	e.preventDefault();

	var delurl = $(this).attr('href');
	jConfirm('Are you sure?', 'Delete item', function(r) {
		if(r === true) {
			$.ajax({
				type: "GET",
				url: delurl
			}).done(function( data ) {
				location.reload();
			});
		} else {
			return false;
		}
	});
});

$(".deletechecked").click(function(e) {
	e.preventDefault();

	var selected = new Array();
	$('tbody input:checked').each(function() {
		if($(this).attr('class') == 'deletecheckbox') {
			selected.push($(this).attr('name'));
		}
	});

	var delurl = $(this).attr('href');
	jConfirm('Are you sure?', 'Delete items', function(r) {
		if(r === true) {
			$.ajax({
				type: "POST",
				url: delurl,
				data: { selectedids: selected }
			}).done(function( data ) {
				location.reload();
			});
		} else {
			return false;
		}
	});
});

$('.selectallcheckbox').mousedown(function() {
    if (!$(this).is(':checked')) {
        $(".deletecheckbox:visible").attr('checked', true);
    } else {
		$(".deletecheckbox").attr('checked', false);
    }
});

if($(".btn-groupitem-delete").is('*')) {
	$("#groupitem-delete-dialog").dialog({
	  autoOpen: false,
	  modal: true
	});

	$(".btn-groupitem-delete").click(function(e) {
	e.preventDefault();
	var targetUrl = $(this).attr("href");
	var targetUrl2 = targetUrl.replace('/delete/', '/deleteall/');

	$("#groupitem-delete-dialog").dialog({
	  buttons : {
	    "Remove all" : function() {
	      window.location.href = targetUrl2;
	    },
	    "Remove from this group" : function() {
	      window.location.href = targetUrl;
	    },
	    "Cancel" : function() {
	      $(this).dialog("close");
	    }
	  }
	});

	$("#groupitem-delete-dialog").dialog("open");
	});
}

$(".deletemetaimage").click(function(e) {
	e.preventDefault();
	var delurl = $(this).attr('href');
	$.ajax({
		type: "GET",
		url: delurl,
		data: { }
	}).done(function( data ) {
		location.reload();
	});
});

});



