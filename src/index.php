<?php
date_default_timezone_set('Europe/Brussels');

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
// 	define('ENVIRONMENT', 'development');
$server = $_SERVER['SERVER_NAME'];
switch($server) {
	case 'admin.tapcrowd.com':
	case 'admin.live.tapcrowd.com':
	case 'mijnevent.admin.tapcrowd.com':
	case 'myupcoming.admin.tapcrowd.com':
	case 'nousorganisons.admin.tapcrowd.com':
	case 'mijnevenement.admin.tapcrowd.com':
	case 'myapp.bmapps.be':
	case 'admin.events.tapcrowd.com':
	case 'admin.axa.tapcrowd.com':
		$environment = 'production';
		break;
	case 'admin.staging.tabcrowd.com':
	case 'admin.staging.tapcrowd.com':
		$environment = 'staging';
		break;
	case 'admin.test.tabcrowd.com':
	case 'admin.test.tapcrowd.com':
	case 'admin.tabcrowd.com':
		$environment = 'testing';
		break;
	default:
		$environment = 'development';
}

if(strrpos($server, 'admin.tapcrowd.com') !== false) {
	$environment = 'production';
}
define('ENVIRONMENT', $environment);

define('SANDBOX_MODE', false);

function lg($msg)
{
	file_put_contents( __DIR__ . '/tmp/query_log.sql', "\n\n" . $msg, FILE_APPEND);
}

/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */

if(defined('ENVIRONMENT')) {
	switch (ENVIRONMENT) {
		case 'development':
		case 'testing':
			error_reporting(E_ALL);
			if(function_exists('xdebug_enable')) {
				xdebug_enable();
			}
			break;
		case 'staging':
			error_reporting(E_ALL);
			if(function_exists('xdebug_enable')) {
				xdebug_enable();
			}
			break;
		case 'production':
			error_reporting(0);
			break;
		default:
			exit('The application environment is not set correctly.');

	}
}

if(ENVIRONMENT == 'development') {
	function _get_caller() {
		$trace = debug_backtrace();
		$caller = $trace[2];
		return isset($caller['class'])? $caller['class'] .'::'.$caller['function'] : $caller['function'] ;
	}

}


/**
 * Enable SPL Autoloading
 */
spl_autoload_register(function( $classname )
{
	if(strpos($classname, 'Tapcrowd') === 0) {

		$basepath = __DIR__ . '/application/';
		$filepath = str_replace('\\','/', lcfirst(substr($classname,9))) . '.php';
		if(file_exists($basepath.$filepath)) {
			require $basepath.$filepath;
			return;
		}

		# Load classes
		if(strpos($classname, 'Tapcrowd\Model') === 0) {
			$class = strtolower(substr($classname, 15)) . '_model';
			require __DIR__ . '/application/models/' . $class . '.php';
		}
	}
}, true, true);



/*
 *---------------------------------------------------------------
 * SYSTEM FOLDER NAME
 *---------------------------------------------------------------
 *
 * This variable must contain the name of your "system" folder.
 * Include the path if the folder is not in the same  directory
 * as this file.
 *
 */
	$system_path = 'system';

/*
 *---------------------------------------------------------------
 * APPLICATION FOLDER NAME
 *---------------------------------------------------------------
 *
 * If you want this front controller to use a different "application"
 * folder then the default one you can set its name here. The folder
 * can also be renamed or relocated anywhere on your server.  If
 * you do, use a full server path. For more info please see the user guide:
 * http://codeigniter.com/user_guide/general/managing_apps.html
 *
 * NO TRAILING SLASH!
 *
 */
	$application_folder = 'application';

/*
 * --------------------------------------------------------------------
 * DEFAULT CONTROLLER
 * --------------------------------------------------------------------
 *
 * Normally you will set your default controller in the routes.php file.
 * You can, however, force a custom routing by hard-coding a
 * specific controller class/function here.  For most applications, you
 * WILL NOT set your routing here, but it's an option for those
 * special instances where you might want to override the standard
 * routing in a specific front controller that shares a common CI installation.
 *
 * IMPORTANT:  If you set the routing here, NO OTHER controller will be
 * callable. In essence, this preference limits your application to ONE
 * specific controller.  Leave the function name blank if you need
 * to call functions dynamically via the URI.
 *
 * Un-comment the $routing array below to use this feature
 *
 */
	// The directory name, relative to the "controllers" folder.  Leave blank
	// if your controller is not in a sub-folder within the "controllers" folder
	// $routing['directory'] = '';

	// The controller class file name.  Example:  Mycontroller
	// $routing['controller'] = '';

	// The controller function you wish to be called.
	// $routing['function']	= '';


/*
 * -------------------------------------------------------------------
 *  CUSTOM CONFIG VALUES
 * -------------------------------------------------------------------
 *
 * The $assign_to_config array below will be passed dynamically to the
 * config class when initialized. This allows you to set custom config
 * items or override any default config values found in the config.php file.
 * This can be handy as it permits you to share one application between
 * multiple front controller files, with each file containing different
 * config values.
 *
 * Un-comment the $assign_to_config array below to use this feature
 *
 */
	// $assign_to_config['name_of_config_item'] = 'value of config item';



// --------------------------------------------------------------------
// END OF USER CONFIGURABLE SETTINGS.  DO NOT EDIT BELOW THIS LINE
// --------------------------------------------------------------------

/*
 * ---------------------------------------------------------------
 *  Resolve the system path for increased reliability
 * ---------------------------------------------------------------
 */

	// Set the current directory correctly for CLI requests
	if (defined('STDIN'))
	{
		chdir(dirname(__FILE__));
	}

	if (realpath($system_path) !== FALSE)
	{
		$system_path = realpath($system_path).'/';
	}

	// ensure there's a trailing slash
	$system_path = rtrim($system_path, '/').'/';

	// Is the system path correct?
	if ( ! is_dir($system_path))
	{
		exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}

/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

	// The PHP file extension
	// this global constant is deprecated.
	define('EXT', '.php');

	// Path to the system folder
	define('BASEPATH', str_replace("\\", "/", $system_path));

	// Path to the front controller (this file)
	define('FCPATH', str_replace(SELF, '', __FILE__));

// 	// Name of the "system folder"
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));


	// The path to the "application" folder
	if (is_dir($application_folder))
	{
		define('APPPATH', $application_folder.'/');
	}
	else
	{
		if ( ! is_dir(BASEPATH.$application_folder.'/'))
		{
			exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
		}

		define('APPPATH', BASEPATH.$application_folder.'/');
	}

	if(isset($_GET['lang'])) {
		$lang = $_GET['lang'];
		if($lang == 'nl' || $lang == 'en' || $lang == 'pt') {
			setcookie('language', $lang,time()+3600*24*365, "/", 'admin.tapcrowd'.substr($_SERVER['SERVER_NAME'], strrpos($_SERVER['SERVER_NAME'], '.')));
			setcookie('language', $lang,time()+3600*24*365, "/", 'tapcrowd'.substr($_SERVER['SERVER_NAME'], strrpos($_SERVER['SERVER_NAME'], '.')));
			header('Location: http://'. str_replace('admin', 'account', $_SERVER['SERVER_NAME']));
			exit;
		}
	}

/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 *
 */
require_once BASEPATH.'core/CodeIgniter.php';

/* End of file index.php */
/* Location: ./index.php */
