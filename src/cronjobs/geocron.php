<?php
namespace Tapcrowd\Deamons;

class GeocodingDeamon
{
	/**
	 * PDO Statements
	 * @var array
	 */
	private $_statements;

	/**
	 * cUrl resource
	 * @var resource
	 */
	private $_curl;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->_statements = array();

		# Setup cUrl
		$this->_curl = curl_init();
		curl_setopt_array($this->_curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => false,
			CURLOPT_CONNECTTIMEOUT => 1,
			CURLOPT_TIMEOUT => 1,
		));
	}

	/**
	 * Private, log function
	 *
	 * @param string $msg
	 */
	private function _log($msg)
	{
		call_user_func_array('printf', func_get_args());
	}

	/**
	 * Add geocoding support to the given table
	 *
	 * @param string $tablename
	 * @param \PDOStatement $select Prepared statement, should return id, address and country
	 * @param \PDOStatement $update Prepared statement, should accept lat, lng and id
	 * @param \PDOStatement $lookup Prepared statement, should accept address and return lat/lng
	 * @return void
	 */
	public function addTableSupport( \PDOStatement $select, \PDOStatement $update, \PDOStatement $lookup )
	{
		$this->_statements[] = (object)compact('select','update','lookup');
	}

	/**
	 * Performs Google Maps API request for the given address
	 * return array with lng/lat pair or null if no data is found. Returns false
	 * when things go bad like OVER_QUERY_LIMIT
	 *
	 * @param string $address
	 * @param string $country 2 char country code (ISO 3166-1 alpha-2)
	 * @return false|null|array [lat,lng]
	 * @see https://developers.google.com/maps/documentation/geocoding/
	 */
	public function getLocationUsingGoogleMapsAPI( $address, $country )
	{
		# Validate / Sanitize
		$address = filter_var($address, FILTER_SANITIZE_ENCODED, FILTER_FLAG_ENCODE_LOW | FILTER_FLAG_ENCODE_HIGH);
		if(empty($address) || !preg_match('/[A-Z]{2}/', strtoupper($country))) {
			return null;
			//throw new \InvalidArgumentException('Invalid/Empty Address : '.$address);
		}

		# Request
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=%s&country=%s';
		curl_setopt($this->_curl, CURLOPT_URL, sprintf($url, $address, $country));
		usleep(400000);
		$output = curl_exec($this->_curl);

		# If cUrl failed give some feedback
		if($output === false) {
			$errno = curl_errno($this->_curl);
			$error = curl_error($this->_curl);
			echo "\nGEOCODING :: Something bad happened: #$errno | $error";
			var_dump(curl_getinfo($this->_curl));
			return null;
		}

		$response = json_decode($output);

		# Unable to decode response
		if($response === null) {
			var_dump(sprintf($url, $address, $country), $output);
			return null;
		}

		# Return
		switch($response->status) {
			case 'OK':
				$lat = $response->results[0]->geometry->location->lat;
				$lng = $response->results[0]->geometry->location->lng;
				$result = array($lat, $lng);
				break;
			case 'ZERO_RESULTS':
				$result = null;
				break;
			default:
				var_dump($response);
				$result = false;
				break;
		}
		return $result;
	}

	/**
	 * Performs MapQuest API request for the given address
	 * return array with lng/lat pair or null if no data is found. Returns false
	 * when things go bad.
	 *
	 * @param string $address
	 * @param string $country 2 char country code (ISO 3166-1 alpha-2)
	 * @return false|null|array [lat,lng]
	 * @see http://www.mapquestapi.com/geocoding/
	 */
	public function getLocationUsingMapQuestAPI( $address, $country )
	{
		# Validate / Sanitize
		$address = filter_var($address, FILTER_SANITIZE_ENCODED, FILTER_FLAG_ENCODE_LOW | FILTER_FLAG_ENCODE_HIGH);
		if(empty($address) || !preg_match('/[A-Z]{2}/', strtoupper($country))) return null;

		# Batch request url
		# http://www.mapquestapi.com/geocoding/v1/batch?key=YOUR_KEY_HERE&callback=renderBatch
		# &location=York,PA&location=Red Lion&location=19036&location=300 Granite Run Dr, Lancaster, PA

		# Setup request
		$key = 'Fmjtd%7Cluub2qa72l%2C8l%3Do5-9u7006';
		$url = 'http://www.mapquestapi.com/geocoding/v1/address?key=%s&location=%s,%s&maxResults=1&thumbMaps=false';
		curl_setopt($this->_curl, CURLOPT_URL, sprintf($url, $key, $address, $country));
		$output = curl_exec($this->_curl);
		$response = json_decode($output);
		if($response === null || !isset($response->info) || $response->info->statuscode != 0) {
			var_dump(sprintf($url, $key, $address, $country), $output);
			return false;
		}

		# Parse response
		if(empty($response->results) || empty($response->results[0]->locations)) {
			$result = null;
		} else {
			$lat = $response->results[0]->locations[0]->latLng->lat;
			$lng = $response->results[0]->locations[0]->latLng->lng;
			$result = array($lat, $lng);
		}
		return $result;
	}

	/**
	 * Performs Bing API request for the given address
	 * return array with lng/lat pair or null if no data is found. Returns false
	 * when things go bad.
	 *
	 * @param string $address
	 * @param string $country 2 char country code (ISO 3166-1 alpha-2)
	 * @return false|null|array [lat,lng]
	 * @see http://msdn.microsoft.com/en-us/library/ff701714.aspx
	 */
	public function getLocationUsingBingAPI( $address, $country )
	{
		# Validate / Sanitize
		$address = filter_var($address, FILTER_SANITIZE_ENCODED, FILTER_FLAG_ENCODE_LOW | FILTER_FLAG_ENCODE_HIGH);
		if(empty($address) || !preg_match('/[A-Z]{2}/', strtoupper($country))) return null;

		# Setup request
		$key = 'AghJ88MGlKeFIPFVSM-ypDbYVffkWbq2Yg3dU32DiZFkybz5vbHxzM41fYcCVRGV';
		$url = 'http://dev.virtualearth.net/REST/v1/Locations/?key=%s&addressLine=%s&countryRegion=%s&maxResults=1';
		curl_setopt($this->_curl, CURLOPT_URL, sprintf($url, $key, $address, $country));
		usleep(400000);
		$output = curl_exec($this->_curl);
		$response = json_decode($output);

		# Parse response
		if($response === null || !isset($response->statusCode) || $response->statusCode != 200) {
			$result = null;
		} else {
			if(empty($response->resourceSets) || empty($response->resourceSets[0]->resources)) {
				$result = null;
			} else {
				$result = $response->resourceSets[0]->resources[0]->point->coordinates;
			}
		}
		return $result;
	}

	/**
	 * Performs lookup in internal database for the given address
	 * return array with lng/lat pair or null if no data is found. Returns false
	 * when things go bad.
	 *
	 * @param string $address
	 * @param string $country 2 char country code (ISO 3166-1 alpha-2)
	 * @return false|null|array [lat,lng]
	 * @see http://msdn.microsoft.com/en-us/library/ff701714.aspx
	 */
	public function getLocationUsingInternalAPI( $address, $country )
	{
		# Validate / Sanitize
		if(empty($address) || !is_string($address) || !preg_match('/[A-Z]{2}/', strtoupper($country))) return null;

		$result = null;
		foreach($this->_statements as &$stats) {
			if($stats->lookup->execute(array($address, $country))) {
				$result = $stats->lookup->fetch();
				if(!empty($result)) break;
			} else {
				$msg = $stats->select->errorInfo();
				throw new \RuntimeException('Database issue '.$msg[2]);
			}
		}
		return $result;
	}

	/**
	 * Performs API request(s) for the given address
	 * return array with lng/lat pair or null if no data is found. Returns false
	 * when things go bad. This function will switch between API's if none if given
	 * as parameter.
	 *
	 * @param string $address
	 * @param string $country 2 char country code (ISO 3166-1 alpha-2)
	 * @param string $api API of choice. i.e.: GoogleMaps or Bing
	 * @return false|null|array [lat,lng]
	 */
	public function getLocation( $address, $country, $api = null )
	{
		$try_another_api_on_failure = ($api === null);

		# Empty address? return NULL coordinates
		if(empty($address)) return array(0,0);

		# Always try Internal API first
		if($try_another_api_on_failure) {
			$this->_log("Using Internal API\n");
			$res = $this->getLocationUsingInternalAPI($address, $country);
			if(is_array($res)) return $res;
		}

		# Get and call external API
		$apis = array('GoogleMaps','Bing');
		if($api === null || !in_array($api, $apis)) {
			$api = $apis[array_rand($apis)];
		}
		$this->_log("Using $api API\n");
		$method = 'getLocationUsing'.$api.'API';
		$res = $this->$method($address, $country);

		# Handle success
		if($res === null || is_array($res)) {

			# Use other API
			if($try_another_api_on_failure && ($res === null || ($res[0] == 0 && $res[1] == 0))) {
				$other_api = ($api == 'Bing')? 'GoogleMaps' : 'Bing';
				$res = $this->getLocation($address, $country, $other_api);
			}
			return is_array($res)? $res : array(0,0);
		}

		# Handle failure
		else {
			// TODO
			throw new \Exception('Unknown GeoLocation Exception. Api was '.$api.', response was '.gettype($res));
		}
	}

	/**
	 * Gets an address and tries to fetch and set the geo location for it
	 * using Geolocation APIs. Returns true on success, false on failure
	 *
	 * TODO: This disables further processing of empty tables
	 * using a late static binding ($proccessed);
	 *
	 * @return bool
	 */
	public function getAddressAndSetLocation()
	{
		static $proccessed = array();

		#
		# Get address
		#
		# TODO: This disables further processing of empty tables
		# using the late static $proccessed;
		#
		$i = array_rand( array_diff_key( array_keys($this->_statements), $proccessed));
		if($i === null) return false;
		$stats =& $this->_statements[$i];

		if($stats->select->execute()) {
			list($id, $address, $country) = $stats->select->fetch();

			# Nothing left do to
			if($id === null) {
				$proccessed[] = $i;
				return true;
			}
		} else {
			$msg = $stats->select->errorInfo();
			throw new \RuntimeException('Database issue '.$msg[2]);
		}

		# Get location
		list($lat, $lng) = $this->getLocation( $address, $country );

		# Update
		$res = $stats->update->execute(array($lat, $lng, $id));
		if(empty($res)) {
			$msg = $stats->update->errorInfo();
			if(!empty($msg[0])) {
				throw new \RuntimeException('Database issue '.$msg[2]);
			} else {
				// TODO Nothing seems to be updated, strange
				$this->_log("\nNothing done for \"%s\"", $address);
			}
		} else {
			$this->_log("Updated location (%f %f) for \"%s\"\n", $lat, $lng, $address);
		}

		return true;
	}

	/**
	 * Returns loaded service
	 *
	 * @return \Tapcrowd\Deamons\GeocodingDeamon
	 */
	public static function getInstance()
	{
		static $service;
		if(isset($service)) return $service;

		$service = new GeocodingDeamon;

		# Setup PDO
		$dsn = 'mysql:dbname=tapcrowd;host=localhost';
		$driver_opts = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"');
		$pdo = new \PDO($dsn, 'tcAdminDB', '2012?tapcrowdadmin', $driver_opts);
		if(!($pdo instanceof \PDO)) {
			$msg = $pdo->errorInfo();
			throw new \RuntimeException('Database failure : '.$msg[2]);
		}
		$pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_NUM);

		# Places Support
		$select = $pdo->prepare('SELECT id, addr, country FROM tc_places WHERE isgeocoded=0 AND addr != "" AND (lat=0 OR lng=0) LIMIT 1');
		$update = $pdo->prepare('UPDATE tc_places SET lat=?, lng=?, isgeocoded=1 WHERE id=?');
		$lookup = $pdo->prepare('SELECT lat, lng FROM tc_places WHERE isgeocoded=1 AND addr=? AND country=? LIMIT 1');
		$service->addTableSupport($select, $update, $lookup);

		# Venue Support
		$select = $pdo->prepare('SELECT id, address, country FROM venue
					WHERE active=1 AND deleted=0 AND isgeocoded=0 AND address != "" AND (lat=0 OR lon=0) LIMIT 1');
		$update = $pdo->prepare('UPDATE venue SET lat=?, lon=?, isgeocoded=1 WHERE id=?');
		$lookup = $pdo->prepare('SELECT lat, lon AS lng FROM venue WHERE isgeocoded=1 AND address=? AND country=? LIMIT 1');
		$service->addTableSupport($select, $update, $lookup);

		return $service;
	}

	/**
	 * Keep running for at least 100 addresses
	 */
	public static function start()
	{
		$service = GeocodingDeamon::getInstance();

		# Run
		for($c = 500;($c && $service->getAddressAndSetLocation());$c--);
	}
}

GeocodingDeamon::start();
