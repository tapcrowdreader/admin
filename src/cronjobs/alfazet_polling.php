<?php
	libxml_use_internal_errors(true);
	poll();

	function poll() {
		// alfazet order states 
		$stateMessages = array(
				0 => 'Order in polling queue',
				1 => 'Order polled',
				2 => 'Accepted by kassa',
				3 => 'Denied by kassa',
				4 => 'Order on hold in kassa'
			);

		// database connection
		$con = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin", "tapcrowd");
		if (!$con)
		  {
		die(sprintf('Connect Error (%s) %s', mysqli_connect_errno(), mysqli_connect_error()));
		  }
		mysqli_set_charset($con, "utf8");

		// get venues to call
		$venues = array();
		$venuesQuery =  mysqli_query($con, 
			"SELECT b.externalvenueid, b.venueid, a.id as appid, a.bundle FROM basket b 
			INNER JOIN appvenue ON appvenue.venueid = b.venueid 
			INNER JOIN app a ON appvenue.appid = a.id 
			WHERE b.send_order_to_api = 'alfazet' AND a.id <> 2");
		while($venue = mysqli_fetch_assoc($venuesQuery)) {
			if(empty($venue['bundle'])) {
				$venue['bundle'] = 'com.tapcrowd.demoapp';
			}
			
			$venues[$venue['externalvenueid']] = (object) $venue;
		}
		

		// fetch orders that are already in our database
		$alfazet_ordersQuery =  mysqli_query($con, "SELECT * FROM alfazet_push");
		while($alfazet_order = mysqli_fetch_assoc($alfazet_ordersQuery)) {
			$alfazet_orders[$alfazet_order['orderid']][$alfazet_order['deviceid']][$alfazet_order['status']] = (object) $alfazet_order;
		}

		// switch to profiling database
		mysqli_select_db($con, "profiling");

		$insert = array();
		foreach($venues as $externalvenueid => $venue) {
			// fetch orders from alfazet
			$url = 'http://79.174.135.14/tapcrowd-1.0/status/'.$externalvenueid.'/60';
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$content = curl_exec($curl);
			curl_close($curl);

			if ($content !== false) {
				$content = json_decode($content);
				// $content->orders[] = (object) array('orderid' => 42, 'venueid' => 121212, 'state' => 1, 'deviceid' => '1234567dfg8');
				if(isset($content->orders) && !empty($content->orders)) {
					foreach($content->orders as $order) {
						//check if a push notification was already sent for the current status of the current order, if not send push.
						if(!isset($alfazet_orders[$order->orderid][$order->deviceid][$order->state]) && !empty($order->deviceid)) {
							// query profiling db to check device for current bundle, if no found, send using demoapp
							$deviceQuery =  mysqli_query($con, "SELECT * FROM push WHERE bundle ='$venue->bundle' AND deviceid='$order->deviceid'");
							if($deviceQuery == false || mysqli_num_rows($deviceQuery) == 0) {
								$venue->bundle = 'com.tapcrowd.demoapp';
							}

							// send push
							$ch = curl_init('http://analytics.tapcrowd.com/0.1/pushservice/sendPushForBundle');
							curl_setopt($ch, CURLOPT_POST, true);                                                                     
							curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$venue->bundle.'&deviceid='.$order->deviceid.'&msg='.urlencode($stateMessages[$order->state]));  
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							$result = stristr($result, 'ok');
							if($result) {
								// log push notfication
								$insert[] = "($order->orderid, $order->state, '$order->deviceid')";
							}
							curl_close($ch);
						}
					} 
				}
			}
		}

		// switch to tapcrowd database
		mysqli_select_db($con, "tapcrowd");
		$insert = implode(',',$insert);
		mysqli_query($con, "INSERT INTO alfazet_push (orderid, status, deviceid) VALUES $insert");
		mysqli_close($con);
	}
