<?php 
synch();
	function synch() {
		$salt = 'wvcV23efGead!(va$43';
		$appid = 3342;
		$eventid = 5772;
		$mysqli = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin","tapcrowd");
		$mysqli->set_charset("utf8");
		if ($mysqli->connect_error) {
		    die('Connect Error (' . $mysqli->connect_errno . ') '
		            . $mysqli->connect_error);
		}

		$url = 'http://api.eventdrive.be/AttendeeContactInfos/?%24inlinecount=allpages&%24format=json&Authorization=C362FFF7-4031-4ABB-AEE3-F4C0F6D88F5F&%24top=100000&%24orderby=cust_lname&%24filter=EventID%20eq%204012%20and%20cust_aanwezig%20eq%20true';
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Accept: application/json'
	    ));
		$data=curl_exec($curl);
		curl_close($curl);

		if($data) {
			$data = jsonRemoveUnicodeSequences($data);
			$data = json_decode($data);
		}

		$attendeelauncheridExists = $mysqli->query("SELECT id FROM launcher WHERE eventid = $eventid AND moduletypeid = 14 LIMIT 1");
		if($attendeelauncheridExists->num_rows > 0) {
			$attendeelauncherid = $attendeelauncheridExists->fetch_assoc();
			$attendeelauncherid = $attendeelauncherid['id'];
		} else {
			$stmt = $mysqli->prepare("INSERT INTO launcher (eventid, moduletypeid, module, title, icon, active) VALUES (?, ?, ?, ?, ?, ?)");
			$stmt->bind_param("iisssi", $eventid, 14, 'Attendee list', 'Attendee list', 'l_attendees', 1);
			$stmt->execute();
			$attendeelauncherid = $stmt->insert_id;
			$stmt->close();
		}
		
		if($data && isset($data->d->results)) {
			foreach($data->d->results as $attendee) {
				$newattendee->external_id = ($attendee->cust_id != null) ? $attendee->cust_id : '';
				$newattendee->firstname = ($attendee->cust_fname != null) ? $attendee->cust_fname : '';
				$newattendee->name = ($attendee->cust_lname != null) ? $attendee->cust_lname : '';
				$newattendee->email = '';
				$newattendee->country = ($attendee->cust_country != null) ? $attendee->cust_country : '';
				$newattendee->company = ($attendee->cust_company != null) ? $attendee->cust_company : '';
				$newattendee->function = ($attendee->cust_functie != null) ? $attendee->cust_functie : '';
				$newattendee->phonenr = '';

				$attendeeExists = $mysqli->query("SELECT id FROM attendees WHERE eventid = $eventid AND external_id = '".$newattendee->external_id."' LIMIT 1");
				if($attendeeExists->num_rows > 0) {
					$newattendee->id = $attendeeExists->fetch_assoc();
					$newattendee->id = $newattendee->id['id'];

					$stmt = $mysqli->prepare("UPDATE attendees SET external_id = ?, eventid = ?, firstname = ?, name = ?, email = ?, country = ?, company = ?, function = ?, phonenr = ? WHERE id = ?");
					$stmt->bind_param("sisssssssi", $newattendee->external_id, $eventid, $newattendee->firstname, $newattendee->name, $newattendee->email, $newattendee->country, $newattendee->company, $newattendee->function, $newattendee->phonenr, $newattendee->id);
					$stmt->execute();
					$stmt->close();
				} else {
					$stmt = $mysqli->prepare("INSERT INTO attendees (external_id, eventid, firstname, name, email, country, company, function, phonenr) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
					$stmt->bind_param("sisssssss", $newattendee->external_id, $eventid, $newattendee->firstname, $newattendee->name, $newattendee->email, $newattendee->country, $newattendee->company, $newattendee->function, $newattendee->phonenr);
					$stmt->execute();
					$newattendee->id = $stmt->insert_id;
					$stmt->close();
				}

				$user->external_id = $newattendee->external_id;
				$user->login = $newattendee->email;
				$user->email = $newattendee->email;
				$user->name = $newattendee->firstname . ' ' . $newattendee->name;
				$user->password = md5($attendee->cust_id.$salt);
				$user->attendeeid = $newattendee->id;

				$userExists = $mysqli->query("SELECT u.id FROM user u INNER JOIN appuser au ON au.userid = u.id WHERE au.appid = $appid AND u.external_id = '".$user->external_id."' LIMIT 1");
				if($userExists->num_rows > 0) {
					$user->id = $userExists->fetch_assoc();
					$user->id = $user->id['id'];

					$stmt = $mysqli->prepare("UPDATE user SET external_id = ?, login = ?, email = ?, name = ?, password = ?, attendeeid = ? WHERE id = ?");
					$stmt->bind_param("sssssii", $user->external_id, $user->login, $user->email, $user->name, $user->password, $user->attendeeid, $user->id);
					$stmt->execute();
					$stmt->close();
				} else {
					$stmt = $mysqli->prepare("INSERT INTO user (external_id, login, email, name, password, attendeeid) VALUES (?, ?, ?, ?, ?, ?)");
					$stmt->bind_param("sssssi", $user->external_id, $user->login, $user->email, $user->name, $user->password, $user->attendeeid);
					$stmt->execute();
					$user->id = $stmt->insert_id;
					$stmt->close();
				}

				$appuserExists = $mysqli->query("SELECT au.id FROM appuser au WHERE au.appid = $appid AND au.userid = '$user->id' AND au.parentType = 'launcher' AND au.parentId = $attendeelauncherid LIMIT 1");
				if($appuserExists->num_rows == 0) {
					$parentType = 'launcher';
					$stmt = $mysqli->prepare("INSERT INTO appuser (appid, userid, parentType, parentId) VALUES (?, ?, ?, ?)");
					$stmt->bind_param("iisi", $appid, $user->id, $parentType, $attendeelauncherid);
					$stmt->execute();
					$appuseruser->id = $stmt->insert_id;
					$stmt->close();
				}
			}
		}

		$mysqli->close();
		echo "imported\n\n";
	}

	function jsonRemoveUnicodeSequences($struct) {
	   return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $struct);
	}

?>