<?php 
notimmo();
function notimmo() {
	// $appid = 1994;
	$mysqli = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin","notimmo");
	if ($mysqli->connect_error) {
	    die('Connect Error (' . $mysqli->connect_errno . ') '
	            . $mysqli->connect_error);
	}
	// $mysqli->set_charset("utf8");

	$currentids = array();
	$externalids = array();
	$lats = array();
	$lons = array();
	$addresses = array();
	$venues = array();
	$venuesQuery = $mysqli->query("SELECT id, externalid, latitude, longitude, address FROM venues_b2c");
	if($venuesQuery->num_rows > 0) {
		while ($data = $venuesQuery->fetch_assoc())
		{
		   $venues[] = $data;
		}
		foreach($venues as $v) {
			if(isset($v['externalid'])) {
				$externalids[$v['externalid']] = $v['id'];
				$lats[$v['externalid']] = $v['latitude'];
				$lons[$v['externalid']] = $v['longitude'];
				$addresses[$v['externalid']] = $v['address'];
			}
		}
	}


	$url = "ftp://FTPCLASS:iC1AssIM!@ftpservices.corelio.be/EXPORT/CLASSIIMMO/IpadfeedImmoNot.xml";
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

	$data=curl_exec($curl);

	// $filename = "../IpadfeedImmoNot.xml";
	// if (($handle = fopen($filename, "r")) !== FALSE) {
	// 	$data = fread($handle, filesize($filename));
	// }

	$categories = array('74988' => '01', '74990' => '06', '74991' => '06', '74992' => '05', '74993' => '03', '74995' => '03', '74997' => '07');


	$rss = new SimpleXmlElement($data);
	foreach($rss->Zook as $item) {
		$attributes = $item->attributes();
		$venue->externalid = (string) $attributes['AdvertentieID'];
		$currentids[$venue->externalid] = $venue->externalid;
		$venue->name = utf8_decode((string)$item->Data->Title);
		$venue->description = utf8_decode((string)$item->Data->ContentOnline);
		$venue->address = '';
		$venue->street = '';
		$venue->number = '';
		$venue->zipcode = '';
		$venue->city = '';
		if(isset($item->Data->ObjectLocationStreet)) {
			$venue->street = utf8_decode((string)$item->Data->ObjectLocationStreet);
			$venue->address .= $venue->street . ' ';
		} 
		if(isset($item->Data->ObjectLocationNumber)) {
			$venue->number .= utf8_decode((string)$item->Data->ObjectLocationNumber);
			$venue->address .= $venue->number . ', ';
		}
		if(isset($item->Data->ObjectLocationZipCode)) {
			$venue->zipcode .= utf8_decode((string)$item->Data->ObjectLocationZipCode);
			$venue->address .= $venue->zipcode . ' ';
		}
		if(isset($item->Data->ObjectLocationCity)) {
			$venue->city .= utf8_decode((string)$item->Data->ObjectLocationCity);
			$venue->address .= $venue->city;
		}

		$venue->country = utf8_decode((string)$item->Data->ObjectLocationCountry);

		// $iImages = 0;
		// while($iImages < 5) {
		// 	if(isset($item->Images->Image[$iImages]) && !empty($item->Images->Image[$iImages])) {
		// 		$venue->{'image'.($iImages+1)} = utf8_decode((string)$item->Images->Image[$iImages]);
		// 	}
		// 	$iImages++;
		// }
		$venue->urltophoto = utf8_decode((string)$item->Image);
		$venue->price = (int)utf8_decode((string)$item->Data->DemandPrice);
		$venue->urltopage = utf8_decode((string)$item->Data->ObjectContentExternURL);
		$venue->categoryalias_original = (string) $attributes['category'];
		$venue->categoryalias = '';
		if(isset($categories[$venue->categoryalias_original])) {
			$venue->categoryalias = (string)$categories[$venue->categoryalias_original];
		}
		$venue->dateofselling = '2013-01-01';
		

		// $venue->active = 1;
		// $venue->timestamp = time();

		if(isset($externalids[$venue->externalid])) {
			if(isset($lats[$venue->externalid])) {
				$venue->lat = $lats[$venue->externalid];
			}
			if(isset($lons[$venue->externalid])) {
				$venue->lon = $lons[$venue->externalid];
			}
			if(!(isset($addresses[$venue->externalid]) && $addresses[$venue->externalid] == $venue->address) && !empty($venue->address)) {
				$string = str_replace (" ", "+", utf8_encode($venue->address));
				$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $details_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = json_decode(curl_exec($ch), true);
				if ($response['status'] == 'OK') {
					$geometry = $response['results'][0]['geometry'];

				    $venue->lat = $geometry['location']['lat'];
				    $venue->lon = $geometry['location']['lng'];
				}

				curl_close($ch);
				sleep(2);
			}
			$venue->id = $externalids[$venue->externalid];
			$stmt = $mysqli->prepare("UPDATE venues_b2c SET externalid = ?, name = ?, latitude = ?, longitude = ?, country = ?, city = ?, street = ?, `number` = ?, zipcode = ?, price = ?, description = ?, categoryalias = ?, categoryalias_original = ?, urltopage = ?, urltophoto = ?, address = ?, dateofselling = ? WHERE id = ?");
			$stmt->bind_param("ssddsssssdsssssssi",$venue->externalid, $venue->name, $venue->lat, $venue->lon, $venue->country, $venue->city, $venue->street, $venue->number, $venue->zipcode, $venue->price, $venue->description, $venue->categoryalias, $venue->categoryalias_original, $venue->urltopage, $venue->urltophoto, $venue->address, $venue->dateofselling, $externalids[$venue->externalid]);
			$stmt->execute();
			$stmt->close();
		} else {
			$venue->lat = 0;
			$venue->lon = 0;
			if(!empty($venue->address)) {
				$string = str_replace (" ", "+", utf8_encode($venue->address));
				$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $details_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = json_decode(curl_exec($ch), true);
				if ($response['status'] == 'OK') {
					$geometry = $response['results'][0]['geometry'];

				    $venue->lat = $geometry['location']['lat'];
				    $venue->lon = $geometry['location']['lng'];
				}

				curl_close($ch);
				sleep(2);
			}

			$stmt = $mysqli->prepare("INSERT INTO venues_b2c (externalid, name, latitude, longitude, country, city, street, `number`, zipcode, price, description, categoryalias, categoryalias_original, urltopage, urltophoto, address, dateofselling) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param("ssddsssssdsssssss", $venue->externalid, $venue->name, $venue->lat, $venue->lon, $venue->country, $venue->city, $venue->street, $venue->number, $venue->zipcode, $venue->price, $venue->description, $venue->categoryalias, $venue->categoryalias_original, $venue->urltopage, $venue->urltophoto, $venue->address, $venue->dateofselling);
			$stmt->execute();
			$venue->id = $stmt->insert_id;
			$stmt->close();
			$externalids[$v['externalid']] = $venue->id;
		}

		// translations
		$stmt = $mysqli->prepare("DELETE FROM b2c_translations WHERE tableid = $venue->id");
		$stmt->execute();
		$stmt->close();

		if(isset($item->Data->TitleFR)) {
			$stmt = $mysqli->prepare("INSERT INTO b2c_translations (tableid, fieldname, language, translation) VALUES (?, 'name', 'fr', ?)");
			$stmt->bind_param("ss", $venue->id, utf8_decode((string)$item->Data->TitleFR));
			$stmt->execute();
			$stmt->close();
		}
		if(isset($item->Data->TitleNL)) {
			$stmt = $mysqli->prepare("INSERT INTO b2c_translations (tableid, fieldname, language, translation) VALUES (?, 'name', 'nl', ?)");
			$stmt->bind_param("ss", $venue->id, utf8_decode((string)$item->Data->TitleNL));
			$stmt->execute();
			$stmt->close();
		}
		if(isset($item->Data->ContentOnlineFR)) {
			$stmt = $mysqli->prepare("INSERT INTO b2c_translations (tableid, fieldname, language, translation) VALUES (?, 'description', 'fr', ?)");
			$stmt->bind_param("ss", $venue->id, utf8_decode((string)$item->Data->ContentOnlineFR));
			$stmt->execute();
			$stmt->close();
		}
		if(isset($item->Data->ContentOnlineNL)) {
			$stmt = $mysqli->prepare("INSERT INTO b2c_translations (tableid, fieldname, language, translation) VALUES (?, 'description', 'nl', ?)");
			$stmt->bind_param("ss", $venue->id, utf8_decode((string)$item->Data->ContentOnlineNL));
			$stmt->execute();
			$stmt->close();
		}
	}

	foreach($externalids as $k => $v) {
		if(!isset($currentids[$k])) {
			$stmt = $mysqli->prepare("DELETE FROM venues_b2c WHERE id = ?");
			$stmt->bind_param("i",$v);
			$stmt->execute();
			$stmt->close();
		}
	}

	echo "imported\n\n";
	curl_close ($curl);
	$mysqli->close();
}