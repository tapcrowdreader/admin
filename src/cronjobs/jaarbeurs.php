<?php 
ftptest();
function ftptest() {
	$mysqli = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin","tapcrowd");
	if ($mysqli->connect_error) {
	    die('Connect Error (' . $mysqli->connect_errno . ') '
	            . $mysqli->connect_error);
	}

	$url = "ftp://vps.ijv.be/";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_USERPWD, "tapcrowd:eF4SdRd2");
	curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1) ;
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'NLST');

	$ftp=curl_exec($curl);
	$directory=split("[\n|\r]",$ftp);

	foreach($directory as $key=>$value) {
	    if($value=='' || !stripos($value, 'csv')) {
	    	unset($directory[$key]);
	    } 
	}

	foreach($directory as $k => $v) {
		// $path = '/Users/JensLovesMobileJuice/Downloads/'.$v;
		$ch = curl_init($url.$v);
		// curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_USERPWD, "tapcrowd:eF4SdRd2");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1) ;
		$filedata = curl_exec($ch);
		// file_put_contents($path, $data, FILE_APPEND);

		$externalids = array();
		
		$appid = 0;
		$eventid = 0;

		if(stristr($v, 'estetika')) {
			$eventid = 5488;
			$appid = 3093;
		} elseif(stristr($v, 'jaarbeurs')) {
			$eventid = 5339;
			$appid = 2945;
		} elseif(stristr($v, 'artgent')) {
			$eventid = 5516;
			$appid = 3128;
		}

		if(!empty($appid) && !empty($eventid)) {
			$mainparentid = $mysqli->query("SELECT id FROM `group` WHERE eventid = $eventid AND name = 'exhibitorcategories' LIMIT 1");
			$mainparentid = $mainparentid->fetch_assoc();
			$mainparentid = $mainparentid['id'];
			$headers = true;
			$heading = array();
			$categories = array();

			if(($data2 = str_getcsv($filedata, "\n")) !== false) {
				foreach($data2 as $data) {
					$data = explode(';', $data);
					$num = count($data);
					$newrow = array();
					for ($c=0; $c < $num; $c++) {
						if($headers) {
							if(!empty($data[$c]) && stripos($data[$c], 'category') !== false) {
								$categories[$data[$c]] = $data[$c];
								$heading[] = $data[$c];
							} elseif(!empty($data[$c])) {
								$heading[] = $data[$c];
							}
						} elseif(isset($data[$c]) && !empty($data[$c]) && isset($heading[$c])) {
							$newrow[$heading[$c]] = $mysqli->real_escape_string($data[$c]);
							if($appid == 3128) {
								// test without escaping
								$newrow[$heading[$c]] = $data[$c];
							}
						}
					}

					if(!$headers && $newrow['name'] != '') {
						$externalids[$newrow['external_id']] = $newrow['external_id'];
						//insert exhibitor
						foreach($heading as $k) {
							if(!isset($newrow[$k])) {
								$newrow[$k] = '';
							}
						}

						//insert exhibitor
						$exhibitorExists = $mysqli->query("SELECT id FROM exhibitor WHERE eventid = $eventid AND external_id = '".$newrow['external_id']."' LIMIT 1");
						if($exhibitorExists->num_rows == 0) {
							$stmt = $mysqli->prepare("INSERT INTO exhibitor (external_id, eventid, name, booth, description, address, tel, email, web, imageurl) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
							$stmt->bind_param("sissssssss", $newrow['external_id'], $eventid, $newrow['name'], $newrow['booth'], $newrow['description'], $newrow['address'], $newrow['tel'], $newrow['email'], $newrow['web'], $newrow['image1']);
							$stmt->execute();
							$exhibitorid = $stmt->insert_id;
							$stmt->close();
						} else {
							$exhibitorid = $exhibitorExists->fetch_assoc();
							$exhibitorid = $exhibitorid['id'];
							$stmt = $mysqli->prepare("UPDATE exhibitor SET external_id = ?, eventid = ?, name = ?, booth = ?, description = ?, address = ?, tel = ?, email = ?, web = ?, imageurl = ? WHERE id = ?");
							$stmt->bind_param("sissssssssi",$newrow['external_id'], $eventid, $newrow['name'], $newrow['booth'], $newrow['description'], $newrow['address'], $newrow['tel'], $newrow['email'], $newrow['web'], $newrow['image1'], $exhibitorid);
							$stmt->execute();
							$stmt->close();
						}

						if(!empty($exhibitorid)) {
							$stmt = $mysqli->prepare("DELETE FROM groupitem WHERE itemid = ? AND itemtable = 'exhibitor' AND eventid = ?");
							$stmt->bind_param("ii",$exhibitorid, $eventid);
							$stmt->execute();
							$stmt->close();

							foreach($categories as $catname) {
								if(isset($newrow[$catname]) && !empty($newrow[$catname])) {
									// if(!stripos($catname, '_fr')) {
										$catExists = $mysqli->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = '".$mysqli->real_escape_string($newrow[$catname])."' LIMIT 1");
										if($catExists->num_rows == 0) {
											$stmt = $mysqli->prepare("INSERT INTO `group` (appid, eventid, name, parentid) VALUES (?, ?, ?, ?)");
											$stmt->bind_param("iisi",$appid, $eventid, $newrow[$catname], $mainparentid);
											$stmt->execute();
											$catid = $stmt->insert_id;
											$stmt->close();
										} else {							
											$catid = $catExists->fetch_assoc();
											$catid = $catid['id'];
										}

										$stmt = $mysqli->prepare("INSERT INTO groupitem (itemid, itemtable, appid, eventid, groupid) VALUES (?, 'exhibitor', ?, ?, ?)");
										$stmt->bind_param("iiii",$exhibitorid, $appid, $eventid, $catid);
										$stmt->execute();
										$stmt->close();
									// } else {
									// 	$dutchname = $newrow[str_replace('_fr', '_nl', $catname)];
									// 	if(!empty($dutchname)) {
									// 		$catExists = $mysqli->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = '".mysql_escape_string($dutchname)."' LIMIT 1");
									// 		$catExists = $catExists->fetch_assoc();
									// 		$catExists = $catExists['id'];
									// 		$translationExists = $mysqli->query("SELECT id FROM translation WHERE `table` = 'group' AND 'tableid' = $catExists AND `language` = 'fr' LIMIT 1");
									// 		if($translationExists->num_rows == 0) {
									// 			$stmt = $mysqli->prepare("INSERT INTO `translation` (`table`, tableid, language, translation) VALUES ('group', ?, ?, ?)");
									// 			$stmt->bind_param("siss", $catExists, 'fr', $newrow[$catname]);
									// 			$stmt->execute();
									// 			$stmt->close();
									// 		}
									// 	}
									// }
								}
							}
						}
					}

					$headers = FALSE;
				}
			}

			if(!empty($externalids)) {
				$externalidsSql = implode(',', $externalids);
				$$externalidsSql = $mysqli->real_escape_string(utf8_encode($externalidsSql));
				$stmt = $mysqli->prepare("DELETE FROM exhibitor WHERE eventid = ? AND external_id NOT IN (".$externalidsSql.") AND external_id <> ''");
				$stmt->bind_param("i",$eventid);
				$stmt->execute();
				$stmt->close();
			}
		}

		curl_close($ch);
	}

	echo "imported\n\n";
	// echo("<pre>".print_r($directory,true)."</pre>");
	curl_close ($curl);
	$mysqli->close();
}

?>