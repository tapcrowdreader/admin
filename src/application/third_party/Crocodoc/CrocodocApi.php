<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
/**
 * Provides access to the Crocodoc API. This is a class that can be used
 * internally by the other Crocodoc API clients for generic methods
 * including error and request.
 */

abstract class CrocodocApi {
	
	protected $apiToken, $protocol, $host, $basePath, $path, $curlDefaultOptions, $response;
	
	public function __construct(){
		$this->init();
	}
	
	public function CrocodocApi(){
		$this->init();		
	}
	
	private function init(){
		$this->apiToken = 'twu48iPMsUjqICa0KWpBkT9h';
		$this->protocol = 'https';
		$this->host = 'crocodoc.com';
		$this->basePath = '/api/v2';
		$this->path = '/';
		$this->response = array();
		
		$this->curlDefaultOptions = array(
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_USERAGENT => 'crocodoc-php'
		);
	}
	
	protected function _request($method, $getParams, $postParams, $isJson = true) {
		$ch = curl_init();
		$url = $this->protocol . '://' . $this->host . $this->basePath . $this->path . $method;
		$options = $this->curlDefaultOptions;
		
		// if $getParams is a string, turn it into an array
		if (!empty($getParams) && is_string($getParams)) {
			$getParams = parse_str($getParams);
		}
		
		// add the API token to $getParams
		if (empty($getParams)) $getParams = array();
		$getParams['token'] = $this->apiToken;
		
		// turn $getParams into a query string and append it to $url
		if (!empty($getParams)) {
			$getParams = http_build_query($getParams, null, '&');
			$url .= (strpos($url, '?')) ? '&' : '?';
			$url .= $getParams;
		}
		
		// turn $postParams into a query string and add it to the curl $options
		if (!empty($postParams)) {
			// add the API token to $postParams
			if (is_string($postParams)) $postParams = parse_str($postParams);
			$postParams['token'] = $this->apiToken;
			
			foreach ($postParams as $key => $value) {
				if (is_resource($value)) {
					$metadata = stream_get_meta_data($value);
					
					if (empty($metadata) || empty($metadata['uri'])) {
						return $this->response = array('status' => 'FAILED', 'message' => 'INVALID FILE UPLOAD');
					}
					
					$postParams[$key] = '@' . $metadata['uri'];
				}
			}
			
			$options[CURLOPT_POST] = 1;
			$options[CURLOPT_POSTFIELDS] = $postParams;
		}
		
		$options[CURLOPT_URL] = $url;
		
		if (empty($options[CURLOPT_HTTPHEADER])) $options[CURLOPT_HTTPHEADER] = array();
		
		$options[CURLOPT_HTTPHEADER][] = 'Expect:';
		
		curl_setopt_array($ch, $options);
		$result = curl_exec($ch);
		
		$curlErrno = curl_errno($ch);
		
		if (!empty($curlErrno)) {
			$curlError = curl_error($ch);
			curl_close($ch);
			return $this->response = array('status' => 'FAILED', 'message' => 'CURL EXCEPTION');			
		}
		
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		if ($isJson) {
			$jsonDecoded = @json_decode($result, true);
	
			if ($jsonDecoded === false || $jsonDecoded === null) {
				return $this->response = array('status' => 'FAILED', 'message' => 'SERVER RESPONSE NOT VALID - JSON');				
			}
			
			if (is_array($jsonDecoded) && !empty($jsonDecoded['error'])) {
				return $this->response = array('status' => 'FAILED', 'message' => 'INVALID JSON DECODE');
			}
			
			$result = $jsonDecoded;
		}
		
		$http4xxErrorCodes = array(
			400 => 'BAD REQUEST',
			401 => 'UNAUTHORIZED',
			404 => 'NOT FOUND',
			405 => 'METHOD NOT ALLOWED',
		);
		
		if (isset($http4xxErrorCodes[$httpCode])) {
			return $this->response = array('status' => 'FAILED', 'message' => 'SERVER ERROR - ' . $httpCode . ' ' . $http4xxErrorCodes[$httpCode]);
		}
		
		if ($httpCode >= 500 && $httpCode < 600) {
			return $this->response = array('status' => 'FAILED', 'message' => 'SERVER ERROR - ' . $httpCode . ' UNKNOWN');
		}
		
		return $result;
	}
	
	abstract public function upload($urlOrFile);
	
	abstract public function create($uuid);
	
	abstract public function thumbnail($uuid, $width = null, $height = null);
	
	abstract public function delete($uuid);
	
}