<?php
namespace Tapcrowd\Structs;

/**
 * TapCrowd Flavor Struct
 */
class Flavor extends Basestruct
{
	/**
	 * Identifier
	 * @var int
	 */
	public $id;

	/**
	 * Qualified name
	 * @var string
	 */
	public $name;

	/**
	 * Title
	 * @var string
	 */
	public $title;

	/**
	 * Short description
	 * @var string
	 */
	public $description;

	/**
	 * Path (relative to channel theme folder) with device screenshot
	 * @var string
	 */
	public $image_phone;

	/**
	 * Flavor specific htmlform name (used in app_add forms)
	 * @var string
	 */
	public $formname;

	/**
	 * Whether or not the flavor can be used on live environment
	 * @var int
	 */
	public $available;

	/**
	 * Sort order
	 * @var int
	 */
	public $sortorder;

	/**
	 * Flavor Family
	 * 1. single_event
	 * 2. single_venue
	 * 3. multiple_event_multiple_venue
	 * 4. content
	 * 5. custom
	 * @var int
	 */
	public $familyid;
}
