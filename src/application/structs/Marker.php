<?php
/**
 * Tapcrowd Core
 * @version 0.1
 */
namespace Tapcrowd\Structs;

/**
 * Marker Struct
 */
class Marker
{
	public $mapId;
	public $parentType;
	public $parentId;

	public $xpos = 0;
	public $ypos = 0;

	public $lat = 0;
	public $lng = 0;

	public $icon;

	public $title;
	public $id = 0;
	public $status = 'created';
	public $cdate = 0;
	public $mdate = 0;
}
