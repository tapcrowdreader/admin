<?php
namespace Tapcrowd\Structs;

/**
 * Theme; Used for both native / mobile apps
 */
class Theme extends Basestruct
{
	/**
	 * Identifier
	 * @var int
	 */
	public $id;

	/**
	 * Qualified name
	 * @var string
	 */
	public $name;

	/**
	 * Path to a screenshot relative to the uploads.tapcrowd.com DOCUMENT ROOT
	 * @var string
	 */
	public $screenshot;

	/**
	 * Sort order
	 * @var int
	 */
	public $sortorder;

	/**
	 * Whether or not the theme can be used on live environment
	 * @var int
	 */
	public $islive;
}
