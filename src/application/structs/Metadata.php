<?php
/**
 * Tapcrowd Core
 * @version 0.1
 */
namespace Tapcrowd\Structs;

/**
 * Metadata Struct
 */
class Metadata
{
	const OPT_LOCALISE = 1;

	public $id;
	public $parent;
	public $sortorder;
	public $status;
	public $type;
	public $opts;
	public $qname;
	public $name;
	public $value;
}
