<?php
/**
 * Tapcrowd Core
 * @version 0.1
 */
namespace Tapcrowd\Structs;

/**
 * Map Struct
 *
 * Urls can be local and remote, images and google maps are supported
 * @see http://www.seomoz.org/ugc/everything-you-never-wanted-to-know-about-google-maps-parameters
 * for more information on google maps url parameters.
 */
class Map
{
	const SHOW_NAMES_ON_MAP = 1;

	/**
	 * MapType property
	 *
	 * There a @ types of markers: Those with x/y coordinates and those whose parent objects
	 * supply the latitute/longitude coordinates. This is set using the Map::mapType property:
	 * Type 'cartesian' implements x/y pairs, type 'geographic' implements coordinates
	 *
	 * @var string
	 */
	public $mapType = 'cartesian';

	public $appId;
	public $parentType;
	public $parentId;

	/**
	 * Url Parameter
	 *
	 * Bitmaps and google maps are supported.
	 * Google maps support uses maps,google.com urls with parameters for zoom, map type, center point
	 * eg: https://maps.google.com/?z=8&t=m&ll=50.753887,4.269936 Shows a normal map centered on belgium (zoom level 8).
	 *
	 * 'z' sets the zoom level, 'll' (two lowercase L characters) set the decimal longitude/latidute.
	 * 't' selects the maps type; supported are 'm' for normal map, 'k' for satellite, 'h' for hybrid and 'p' for terrain.
	 *
	 * Relative urls are mapped to the respective uploads hostname (eg uploads.tapcrowd.com)
	 *
	 * @see http://www.seomoz.org/ugc/everything-you-never-wanted-to-know-about-google-maps-parameters
	 * @var string
	 */
	public $url;

	public $title;

	/**
	 * Settings Bitmask
	 *
	 * A selection of class constants
	 * @var int
	 */
	public $settings = 0;

	/**
	 * Collection of Markers
	 * @var ArrayObject
	 */
	public $markers;

	public $id;
	public $status = 'created';
	public $cdate = 0;
	public $mdate = 0;
}
