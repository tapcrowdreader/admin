<?php
/**
 * Tapcrowd Core
 * @version 0.1
 */
namespace Tapcrowd\Structs;

/**
 * Metavalue Struct
 */
class Metavalue
{
	public $metaId;
	public $parent;
	public $sortorder;
	public $name;
	public $type;
	public $value;
	public $locale;
}
