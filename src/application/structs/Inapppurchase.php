<?php
/**
 * Tapcrowd Core
 * @version 0.1
 */
namespace Tapcrowd\Structs;

/**
 * Marker Struct
 */
class Inapppurchase
{
	public $id;
	public $appid;
	public $productid;
	public $productvalue;
	public $type = 'non-consumable';
}
