<?php
/**
 * Tapcrowd Core
 * @version 0.1
 */
namespace Tapcrowd\Structs;

/**
 * Channel Struct
 */
class Channel
{
	public $channelId = 1;
	public $name = 'Tapcrowd';
	public $logo = '/img/header_logo.png';

	public $support;
}
