<?php if(!defined('BASEPATH')) exit('No direct script access');

class Creator_model extends CI_Model {
	
	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
    
	function listFlavors() {
		$res = $this->db->query("SELECT * FROM apptype");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
    }
}
?>
