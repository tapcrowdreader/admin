<?php if(!defined('BASEPATH')) exit('No direct script access');

class Attendee_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Attendee_model() {
		parent::__construct();
	}
	
	function getAttendeeByID($id) {
		$this->db->where('id', $id);
		$res = $this->db->get('attendees');
		if($res->num_rows() == 0) return FALSE;
		
		return $res->row();
	}
	
	function getAttendeesByEventID($id, $apptypefield = 'eventid') {
		$this->db->where($apptypefield, $id);
		$this->db->order_by('name', 'ASC');
		$res = $this->db->get('attendees');
		
		if($res->num_rows() == 0)
			return FALSE;
		
		return $res->result();
	}

	function removePremium($id) {
		if(is_numeric($id)) {
			$this->db->query("DELETE FROM premium WHERE tablename = 'attendees' AND tableId  = $id");
		}
	}

	function getMainCategorieGroup($eventid) {
		$res = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'attendeecategories' LIMIT 1 ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row()->id;
	}

    var $htmlarray = array();
    var $groupids = '';
	function display_children($parent, $level, $eventid, $attendeeid = 0) {  
	    // retrieve all children of $parent <br>  
	    $result = $this->db->query('SELECT * FROM `group` WHERE parentid = '.$parent.' ORDER BY id DESC');  

	    // display each child <br>  
	    if($level == 0 && !empty($parent)) {
		    $parentname = $this->db->query("SELECT name FROM `group` WHERE id = $parent LIMIT 1");
		    if($parentname->num_rows() > 0) {
		    	$parentname = $parentname->row()->name;
			    $html = '<input type="checkbox" class="checkbox" name="groups[]" value="'.$parent.'" />&nbsp'.$parentname."<br />";
		    	if($attendeeid != 0) {
		    		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $parent AND itemid = $attendeeid AND itemtable = 'attendees' ");
		    		if($res->num_rows() != 0) {
		    			$html = '<input type="checkbox" class="checkbox" name="groups[]" value="'.$parent.'" checked>&nbsp'.$parentname."<br />";  
		    		}
		    	}
			    $this->htmlarray[] = $html;
			    $level++;
		    }
	    }
	    foreach($result->result() as $row) {  
	    	// indent and display the title of this child <br>
	    	$this->groupids .= $row->id . '/';
	    	$html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).'<input type="checkbox" class="checkbox" name="groups[]" value="'.$row->id.'" />&nbsp'.$row->name."<br />";  
	    	if($attendeeid != 0) {
	    		$id = $row->id;
	    		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $id AND itemid = $attendeeid AND itemtable = 'attendees' ");
	    		if($res->num_rows() != 0) {
	    			$html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).'<input type="checkbox" class="checkbox" name="groups[]" value="'.$row->id.'" checked>&nbsp'.$row->name."<br />";  
	    		}
	    	}
	        
	        $this->htmlarray[] = $html;
	        // call this function again to display this <br>  
	        // child's children <br>  
	        $this->display_children($row->id, $level+1, $eventid, $attendeeid); 
	        
	    } 

	    return $this->htmlarray;
	} 

}
