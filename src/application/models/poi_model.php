<?php if(!defined('BASEPATH')) exit('No direct script access');

class Poi_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Poi_model() {
		parent::__construct();
	}
	
	function getPOIsByEventID($eventid) {
		// $res = $this->db->query("SELECT * FROM poi WHERE eventid='$eventid'");
		// if($res->num_rows() == 0) return FALSE;
		// return $res->result();
	}
	
	function getPoiById($id) {
		// $this->db->where('id', $id);
		// $res = $this->db->get('poi');
		// if($res->num_rows() == 0) return FALSE;
		// return $res->row();
	}
	
	function getPoisByMapId($mapid) {
		// $res = $this->db->query("SELECT * FROM map WHERE id='$mapid'");
		// if($res->num_rows() == 0) return FALSE;
		// $ret = $res->row();
		
		// $res = $this->db->query("SELECT p.*, pt.name as poitypename, pt.icon as poitypeicon FROM poi p, poitype pt WHERE p.eventid = '$ret->eventid' AND pt.id = p.poitypeid");
		// if($res->num_rows() == 0) return FALSE;
		// return $res->result();
	}
	
	function getPoisByMapIdForVenue($mapid) {
		// $res = $this->db->query("SELECT * FROM map WHERE id='$mapid'");
		// if($res->num_rows() == 0) return FALSE;
		// $ret = $res->row();
		
		// $res = $this->db->query("SELECT p.*, pt.name as poitypename, pt.icon as poitypeicon FROM poi p, poitype pt WHERE p.venueid = '$ret->venueid' AND pt.id = p.poitypeid");
		// if($res->num_rows() == 0) return FALSE;
		// return $res->result();
	}
	
	function getPoiTypes() {
		// $res = $this->db->get('poitype');
		// if($res->num_rows() == 0) return FALSE;
		// return $res->result();
	}
	
	function getPOIsByVenue($venueid) {
		// $res = $this->db->query("SELECT * FROM poi WHERE venueid='$venueid'");
		// if($res->num_rows() == 0) return FALSE;
		// return $res->result();
	}
	
	function getPoiTypesService($type = 'event', $id) {
		// if($type == 'venue'){
		// 	$pois = $this->db->query("SELECT DISTINCT poitypeid FROM poi WHERE venueid='$id'");
		// } else {
		// 	$pois = $this->db->query("SELECT DISTINCT poitypeid FROM poi WHERE eventid='$id'");
		// }
		// if($pois->num_rows() == 0) return FALSE;
		
		// $first = TRUE;
		// foreach($pois->result() as $poi) {
		// 	if(!$first) { $where .= " OR "; }
		// 	$where .= "id = ".$poi->poitypeid;
		// 	$first = FALSE;
		// }
		// $this->db->orderby('name');
		// $res = $this->db->query("SELECT * FROM poitype WHERE $where ORDER BY name");
		// if($res->num_rows() == 0) return FALSE;
		// $data = array();
		// foreach($res->result() as $row) {
		// 	$data += array($row->id => $row);
		// }
		// return $data;
	}

}