<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Notes_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}

	function getActiveNoteModules($app, $type, $typeid) {
		$typefield = $type.'id';
		$res = $this->db->query("SELECT n.launcherid, l.title FROM notes n INNER JOIN launcher l ON l.id = n.launcherid WHERE n.appid = $app->id AND n.".$typefield." = $typeid");
		if($res->num_rows > 0) {
			return $res->result();
		}

		return array();
	}

	function saveNoteModules($app, $post, $type, $typeid) {
		$typefield = $type.'id';
		$this->db->query("DELETE FROM notes WHERE appid = $app->id AND $typefield = $typeid");
		foreach($post as $post) {
			$data = array(
				'appid' => $app->id,
				'launcherid' => $post
				);

			$data[$typefield] = $typeid;
			$this->general_model->insert('notes', $data);
		}
	}
	
}