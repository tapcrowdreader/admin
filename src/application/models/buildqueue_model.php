<?php if(!defined('BASEPATH')) exit('No direct script access');

class Buildqueue_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function insert($app, $env, $email) {
		$buildexists = $this->db->query("SELECT * FROM buildqueue WHERE appid = $app->id AND env = '$env' LIMIT 1");
		$buildservice = 'base';
		if($app->apptypeid == 9 || $app->apptypeid == 12) {
			$buildservice = 'blank';
		}
		$language = filter_input(INPUT_COOKIE, 'language');

		if(!in_array($language, getAvailableLanguages())) {
			$language = 'en';
		}
		if($buildexists->num_rows() == 0) {
			$this->general_model->insert('buildqueue', array(
					'appid' => $app->id,
					'env'	=> $env, 
					'email' => $email,
					'buildservice' => $buildservice,
					'notification_lang' => $language
				));
		}
	}

	function getStatusios($appid, $env) {
		$buildexists = $this->db->query("SELECT * FROM buildqueue WHERE appid = $appid AND env = '$env' LIMIT 1");
		if($buildexists->num_rows() == 0) {
			return 'nothing';
		} else {
			$row = $buildexists->row();
			if($row->buildended == 1) {
				return 'ended';
			} elseif($row->buildbusy == 1) {
				return 'busy';
			} else {
				return 'queue';
			}
		}
	}

	function getStatusAndroid($appid, $env) {
		$buildexists = $this->db->query("SELECT * FROM buildqueue WHERE appid = $appid AND env = '$env' LIMIT 1");
		if($buildexists->num_rows() == 0) {
			return 'nothing';
		} else {
			$row = $buildexists->row();
			if($row->buildended == 1) {
				return 'ended';
			} elseif($row->buildbusy == 1) {
				return 'busy';
			} else {
				return 'queue';
			}
		}
	}

	function rebuild($id, $platform) {
		// $buildqueueid = $this->db->query("SELECT id FROM buildqueue WHERE appid = $id AND env = '$platform' LIMIT 1");
		// if($buildqueueid->num_rows() > 0) {
		// 	$buildqueueid = $buildqueueid->row()->id;
		// 	$this->general_model->update("buildqueue", $buildqueueid, array('buildended' => 0, 'buildbusy' => 0));
		// }

		$this->db->query("DELETE FROM buildqueue WHERE appid = $id AND env = '$platform'");
	}
}