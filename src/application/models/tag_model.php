<?php if(!defined('BASEPATH')) exit('No direct script access');

class Tag_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getOldImage($appid, $tagname) {
		$image = '';
		$oldimage = $this->db->query("SELECT * FROM tag WHERE appid = $appid AND tag = '$tagname' AND image <> '' LIMIT 1");
		if($oldimage->row() != false) {
			$oldimage = $oldimage->row();
			$image = $oldimage->image;
		}
		return $image;
	}
	
	function getTagIds($appid, $tagname) {
		$tagids = $this->db->query("SELECT id FROM tag WHERE appid = $appid AND tag = '$tagname'");
		$tagids = $tagids->result();

		$tagidsArray = array();
		if($tagids != false) {
			foreach($tagids as $id) {
				$tagidsArray[] = $id->id;
			}
		}
		
		return $tagidsArray;
	}
	
	function getTagIdbyFormID($formid) {
		$res = $this->db->query("SELECT id FROM tag WHERE formid = $formid");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->row();
	}
	
	function getTagIdsOfObject($type, $itemid) {
		if($type == 'catalog') {
			$type = 'catalogitem';
		}
		$table = $type.'id';
		$tagids = $this->db->query("SELECT id FROM tag WHERE $table = $itemid");
		$tagids = $tagids->result();
		$tagidsArray = array();
		if($tagids != false) {
			foreach($tagids as $id) {
				$tagidsArray[] = $id->id;
			}
		}
		
		return $tagidsArray;
	}

	function getTagsOfObject($type, $itemid) {
		if($type == 'catalog') {
			$type = 'catalogitem';
		}
		$table = $type.'id';
		$tags = $this->db->query("SELECT id, tag FROM tag WHERE $table = $itemid");
		return $tags->result();
	}

	function getById($id) {
		$tag = $this->db->query("SELECT * FROM tag WHERE id = $id");
		return $tag->row();
	}

	function removeTagsOfObject($field, $fieldid) {
		if(!empty($field) && !empty($fieldid)) {
			$this->db->query("DELETE FROM tag WHERE $field = $fieldid");
		}
		
	}

	function checkTag($type, $id, $tag) {
		$column = $type.'id';
		$res = $this->db->query("SELECT id FROM tag WHERE $column = $id AND tag = '$tag' LIMIT 1");
		if($res->num_rows() == 0) {
			return false;
		}

		return true;
	}

	/**
	 * Append the respective tags and tagNames arrays to each object in the passed objects array
	 *
	 * @param array $objects
	 * @param string $objectType One of venue, event, citycontent...
	 * @param int $appId
	 */
	public function appendTags( Array &$objects, $objectType, $appId )
	{
		# Validate; Supported object types
		$types = array(
			'session','catalogitem','newsitem','event','venue','exhibitor','sponsor','cataloggroup',
			'artist','citycontent','sessiongroup','section','contentmodule','sectioncontent','form',
		);
		if(!is_array($objects) || !in_array($objectType, $types)) {
			throw new \InvalidArgumentException;
		}

		# Get PDO
		$dsn = 'mysql:dbname='.$this->db->database.';host='.$this->db->hostname;
		$driver_opts = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"');
		$pdo = new \PDO($dsn, $this->db->username, $this->db->password, $driver_opts);
		if(!$pdo instanceOf \PDO) throw new \Exception('Unable to create PDO object');
		$pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

		# Create tag Index
		$column = $objectType . 'id';
		$sql = 'SELECT id, tag, color, image, venueid, eventid, citycontentid FROM tag WHERE appid=%d AND %s != 0';
		if($objectType == 'citycontent') $sql .= ' AND formid = 0'; # TODO why?
		$index = array();
		$stat = $pdo->query(sprintf($sql, $appId, $column));
		while(($tag = $stat->fetch()) !== false) {
			if(!isset($index[$tag->$column])) {
				$index[$tag->$column] = array('tags' => array($tag), 'tagNames' => array($tag->tag));
			} else {
				$index[$tag->$column]['tags'][] = $tag;
				$index[$tag->$column]['tagNames'][] = $tag->tag;
			}
		}

		# Iterate over objects, querying / appending tags.
		$c = count($objects);
		while($c--) {
			$object =& $objects[$c];
			if(!is_object($object) || empty($object->id) || !isset($index[$object->id])) {
				$object->tags = array();
				$object->tagNames = array();
			} else {
				$object->tags = $index[$object->id]['tags'];
				$object->tagNames = $index[$object->id]['tagNames'];
			}
		}
	}
}
