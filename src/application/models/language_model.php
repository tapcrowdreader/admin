<?php if(!defined('BASEPATH')) exit('No direct script access');

class Language_model extends CI_Model {
	
	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Language_model() {
		parent::__construct();
	}
    
	function getAllLanguages() {
		$res = $this->db->query("SELECT * FROM language");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
    function removeLanguageFromApp($appid, $language) {
        $res = $this->db->delete('applanguage', array('appid' => $appid, 'language' => $language)); 
    }
    
    function getLanguageByKey($key) {
        $res = $this->db->query("SELECT * FROM language WHERE `key` = '$key'");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
    }
    
    function getLanguagesOfApp($appid) {
		$res = $this->db->query("SELECT `language` FROM applanguage WHERE appid = $appid");
		if($res->num_rows() == 0) return FALSE;
        $languages = array();

        foreach($res->result() as $row) {
            $languages[] = $this->getLanguageByKey($row->language);
        }

        //make sure the default language is the first one in array so that default language field has to be filled in first.
        $defaultlanguage = $this->db->query("SELECT defaultlanguage FROM app WHERE id = $appid LIMIT 1")->row()->defaultlanguage;
        $sortedlanguages = array();
        foreach($languages as $l) {
            if($l->key == $defaultlanguage) {
                $sortedlanguages[] = $l;
            }
        }
        foreach($languages as $l) {
            if($l->key != $defaultlanguage) {
                $sortedlanguages[] = $l;
            }
        }
        
		return $sortedlanguages;
    }
    
    function addTranslation($table, $tableid, $fieldname, $language, $translation) {
        $this->db->delete('translation', array('`table`' => $table, 'tableid'=>$tableid, 'fieldname'=>$fieldname, 'language'=>$language)); 
		$data = array(
			'table'         => $table,
            'tableid'       => $tableid,
            'fieldname'     => $fieldname,
            'language'      => $language,
            'translation'   => $translation
		);
		$this->db->insert('translation', $data);
		return $this->db->insert_id();
    }
    
    function updateTranslation($table, $tableid, $fieldname, $language, $translation) {
        $res = $this->db->query("SELECT id FROM translation WHERE `table` = '".$table."' AND `tableid` = '".$tableid."' AND `fieldname` = '".$fieldname."' AND `language` = '".$language."' LIMIT 1");
        if($res->num_rows() == 0) return FALSE;
        $id = $res->row()->id;
        
		$data = array(
			'table'         => $table,
            'tableid'       => $tableid,
            'fieldname'     => $fieldname,
            'language'      => $language,
            'translation'   => $translation
		);
        $this->db->where('id', $id);
		$this->db->update('translation', $data);
		return $id;
    }
    
    function getTranslation($table, $tableid, $fieldname, $language) {
        $res = $this->db->query("SELECT translation FROM translation WHERE `table` = '".$table."' AND `tableid` = '".$tableid."' AND `fieldname` = '".$fieldname."' AND `language` = '".$language."' LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
    }
    
    function getTranslationsOfLanguage($table, $tableid, $language) {
        $res = $this->db->query("SELECT * FROM translation WHERE `table` = '".$table."' AND `tableid` = '".$tableid."' AND `language` = '".$language."'");
		if($res->num_rows() == 0) return FALSE;

        $result = array();
        foreach($res->result() as $row) {
            $result[$row->fieldname.'_'.$language] = $row->translation;
        }
        return $result;
    }
    
    function removeTranslations($table, $tableid) {
        $this->db->delete('translation', array('`table`' => $table, '`tableid`' => $tableid));
    }

    function getLanguagesAsArray() {
        $res = $this->db->query("SELECT * FROM language");
        $array = array();
        foreach($res->result() as $lang) {
            $array[$lang->key] = $lang->name;
        }

        return $array;
    }
	
	function removeTranslationByFieldName($table, $tableid, $key, $val) {
		$this->db->delete('translation', array('`table`' => $table, '`tableid`' => $tableid, '`language`' => $key, '`fieldname`' => $val));
	}
}
?>