<?php if(!defined('BASEPATH')) exit('No direct script access');

class Event_model extends CI_Model {
	
	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Event_model() {
		parent::__construct();
	}
	
	function add($name, $organizerid, $venueid, $eventtypeid, $datefrom, $dateto, $active, $eventlogo, $description, $phonenr, $website, $ticketlink, $email, $order) {
		$data = array(
			'name' => $name,
			'organizerid' => $organizerid,
			'venueid' => $venueid,
			'eventtypeid' => $eventtypeid,
			'datefrom' => $datefrom,
			'dateto' => $dateto,
			'active' => $active,
			'eventlogo' => $eventlogo,
			'timestamp' => time(),
			'description' => $description,
            'phonenr' => $phonenr,
            'website' => $website,
            'ticketlink' => $ticketlink,
			'email'	=> $email,
			'order'	=> $order
		);
		$this->db->insert('event', $data);
		return $this->db->insert_id();
		//No need to call updatetimestamp, allready set when creating this record
	}
	
	function edit($eventid, $name, $organizerid, $venueid, $eventtypeid, $datefrom, $dateto, $active, $eventlogo, $description, $phonenr, $website, $ticketlink, $email, $order) {
		$data = array(
			'name' => $name,
			'organizerid' => $organizerid,
			'venueid' => $venueid,
			'eventtypeid' => $eventtypeid,
			'datefrom' => $datefrom,
			'dateto' => $dateto,
			'active' => $active,
			'timestamp' => time(),
			'description' => $description,
            'phonenr' => $phonenr,
            'website' => $website,
            'ticketlink' => $ticketlink,
			'email'	=> $email,
			'order'	=> $order
		);
		if($eventlogo != '') {
			$data['eventlogo'] = $eventlogo;
		}
		$this->db->where('id', $eventid);
		$this->db->update('event', $data);
		//No need to call updatetimestamp, allready set when creating this record
	}
	
	function allfromorganizer($id) {
		$res = $this->db->query("SELECT e.*, t.name as typename FROM event e, eventtype t WHERE e.organizerid = '$id' AND e.eventtypeid = t.id AND e.deleted = 0 ORDER BY id DESC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function allFromApp($id) {
		$res = $this->db->query("SELECT e.*, t.name as typename FROM appevent ae, event e, eventtype t WHERE ae.appid = '$id' AND e.id = ae.eventid AND e.eventtypeid = t.id AND e.deleted = 0 ORDER BY e.order ASC, id DESC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getbyid($id) {
		$res = $this->db->query("SELECT e.*, t.name as typename FROM event e, eventtype t WHERE e.id = '$id' AND e.eventtypeid = t.id AND e.deleted = 0 LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
    function getEventTypeId($id) {
		$res = $this->db->query("SELECT eventtypeid FROM event WHERE id = '$id' LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
        
	function getbyNameAndDatefrom($name, $datefrom) {
		$res = $this->db->query("SELECT e.* FROM event e WHERE e.name = '$name' AND e.datefrom = '$datefrom' LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function setActive($id, $status) {
		$data = array(
				'id' => $id,
				'active' => $status
			);
		$this->db->where('id', $id);
		$this->db->update('event', $data);
		$this->updateTimeStamp($id);
	}
	
	function updateTimeStamp($id) {
		$data = array(
				'timestamp' => time()
			);
		$this->db->where('id', $id);
		$this->db->update('event', $data);
		//$this->updateTimeStamp($id);
	}
	
	function getEventtypes() {
		$this->db->order_by('order', 'asc');
		$res = $this->db->get('eventtype');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getLocation($address) {
		$this->load->library('curl');
		$address = str_replace(" ","+",$address);
		$xmlstring = $this->curl->simple_get('http://maps.google.com/maps/api/geocode/xml?address='.$address.'&sensor=true');
		try {
			$sXML = new SimpleXMLElement($xmlstring);
			if(isset($sXML->result)) {
				return $sXML->result->geometry->location;
			} else return ;
		} catch (Exception $e) {
			return;
		}
	}
	
	function updateAllTimeStamps() {
		$data = array(
				'timestamp' => time()
			);
		$this->db->update('event', $data);
	}

	function getEventtype($id) {
		$res = $this->db->query("SELECT * FROM eventtype WHERE id='$id' LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
    
    function getMetadata($id) {
		$res = $this->db->query("SELECT * FROM metadata WHERE `identifier`='$id' AND `table` = 'event'");
		if($res->num_rows() == 0) return FALSE;
        $result = array();
		foreach($res->result() as $row) {
            $result[$row->key] = $row->value;
        }
        return $result;
    }
	
	function countLauncherFromEventModule($eventid, $moduletypeid) {
		$res = $this->db->query("SELECT l.* FROM launcher l WHERE l.eventid = '$eventid' AND l.moduletypeid = '$moduletypeid' ");
		if($res->num_rows() == 0) return FALSE;
		return $res->num_rows();
	}
	
    function insertKXpoLaunchers($eventid, $categoriegroupid, $brandgroupid) {
    	$launcherdata = array(
    			'eventid'	=> $eventid,
    			'moduletypeid'	=> 2,
    			'module'	=> 'exhibitors',
    			'icon'		=> 'upload/appimages/1/customlaunchers/exhibitors.png',
    			'title'		=> 'Exhibitor Catalog',
    			'order'		=> 2
	    	);
	    $this->general_model->insert('launcher',$launcherdata);

    	$launcherdata = array(
    			'eventid'	=> $eventid,
    			'moduletypeid'	=> 5,
    			'module'	=> 'map',
    			'icon'		=> 'upload/appimages/1/customlaunchers/map.png',
    			'title'		=> 'Floorplan',
    			'order'		=> 3
	    	);
	    $this->general_model->insert('launcher',$launcherdata);

    	$launcherdata = array(
    			'eventid'	=> $eventid,
    			'moduletypeid'	=> 33,
    			'module'	=> 'groups',
    			'icon'		=> 'upload/appimages/1/customlaunchers/categories.png',
    			'title'		=> 'Brands',
    			'order'		=> 4,
    			'groupid'	=> $categoriegroupid
	    	);
	    $this->general_model->insert('launcher',$launcherdata);

    	$launcherdata = array(
    			'eventid'	=> $eventid,
    			'moduletypeid'	=> 33,
    			'module'	=> 'groups',
    			'icon'		=> 'upload/appimages/1/customlaunchers/brands.png',
    			'title'		=> 'Categories',
    			'order'		=> 5,
    			'groupid'	=> $brandgroupid
	    	);
	    $this->general_model->insert('launcher',$launcherdata);
    }

	function changelauncher($eventid, $oldlauncherid, $newlauncherid, $appid) {
		$oldlauncher = $this->module_mdl->getLauncherById($oldlauncherid);
		$newlauncher = $this->module_mdl->getLauncherById($newlauncherid);

		if(!empty($appid) && !empty($eventid) && !empty($oldlauncher->tag)) {
			$this->db->query("DELETE FROM tag WHERE appid = $appid AND tag = '$oldlauncher->tag' AND eventid = $eventid");
			$this->general_model->insert('tag', array(
					'appid' => $appid,
					'tag' => $newlauncher->tag,
					'eventid' => $eventid
				));
		}


		return true;
	}

	function remove($eventid) {
		$this->db->query("UPDATE event SET deleted = 1 WHERE id = $eventid");
	}
	
	/*
	function updateTimeStamp($eid) {
		$currenttimestamp = time();
		$data = array(
				'timestamp' => $currenttimestamp
			);
		$this->db->where('id', $eid)
		$this->db->update('event', $data);
	}
	*/
/*
	function update($id, $data) {
		$this->db->where('id', $id);
		if($this->db->update('event', $data)){
			return true;
		} else {
			return false;
		}
	}
	
	function all() {
		$q = $this->db->get('event');
		if($q->num_rows>0) {
			foreach($q->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
	}
	
	function edit($id, $eventtypeid, $datefrom, $dateto) {
	    $data = array(
	           'id' => $id,
	           'eventtypeid' => $eventtypeid,
	           'datefrom' => $datefrom,
	           'dateto' => $dateto
	        );
	    $this->db->where('id', $id);
	    $this->db->update('event', $data);
	    $this->updateTimeStamp($id);
	}
	
	function replaceLogo($id, $eventlogo) {
	    $path = 'upload/eventlogos/';
	    $data = array(
	           'eventlogo' => $eventlogo
	        );
	    $this->db->where('id', $id);
	    $this->db->update('event', $data);
	    $this->updateTimeStamp($id);
	}
	
	function updateAllTimeStamps() {
	    $currenttimestamp = time();
	    $data = array(
	           'timestamp' => $currenttimestamp
	        );
	    $this->db->update('event', $data);
	}
	
	function remove($id) {
	    $this->db->delete('event', array('id' => $id));
	}
*/
}

?>
