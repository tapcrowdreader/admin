<?php if(!defined('BASEPATH')) exit('No direct script access');

class Report_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Report_model() {
		parent::__construct();
	}

	function getNewUsers(){
        $res = $this->db->query('SELECT * FROM tc_accounts ORDER BY cdate DESC');
		return $res->result();
	}

	function getChannelName($id){
        $res = $this->db->query('SELECT name FROM channel WHERE id='.$id.'');
		return $res->row()->name;
	}

	function getUserDates(){
		$res = $this->db->query('SELECT DISTINCT(DATE_FORMAT(cdate,"%Y-%m-%d")) AS crdate, DATE_FORMAT(cdate,"%Y-%m-%d") AS creation FROM `tc_accounts` ORDER BY cdate DESC');
		return $res->result();
	}

	function getAppDates(){
		$res = $this->db->query('SELECT DISTINCT(DATE_FORMAT(creation,"%Y-%m-%d")) AS creation, id FROM `app` ORDER BY creation DESC');
		return $res->result();
	}

	function getRevenueDates(){
		$res = $this->db->query('SELECT DISTINCT(DATE_FORMAT(startdate,"%Y-%m-%d")) AS creation FROM `order` ORDER BY startdate DESC');
		return $res->result();
	}	

	function getTotalUsersUptoDate(){
		$userCount = array();
		$i=0;
		$cdates = $this->getUserDates();
		foreach ($cdates as $value) {
			$cdate = $value->crdate;
			$userCount[$i]['total'] = $this->db->query('SELECT * FROM `tc_accounts` WHERE cdate LIKE "%'.$cdate.'%" ORDER BY cdate DESC')->num_rows();
			$userCount[$i]['crdate'] = $cdate;
			$i++;
		}
		return $userCount;
	}

	function getNewApps($month,$year){
		$res = $this->db->query('SELECT a.apptypeid, a.name AS appname,a.accountId AS ownerid, a.creation, atc.apptypeid, atc.channelid, c.id, c.name AS channel, at.id, at.name AS flavor FROM app a, apptype_channel atc, channel c, apptype at WHERE a.apptypeid = atc.apptypeid AND atc.apptypeid = c.id AND a.apptypeid = at.id AND  a.creation LIKE "%'.$year.'-'.$month.'-%" GROUP BY a.creation ORDER BY a.creation DESC');
		return $res->result();		
	}

	function getOwnerName($id){
        $res = $this->db->query('SELECT fullname FROM tc_accounts WHERE accountId='.$id.'');
		return $res->row()->fullname;
	}

	function getTotalAppsUptoDate(){
		$appCount = array();
		$i=0;
		$cdates = $this->getAppDates();
		
		foreach ($cdates as $value) {
			$creation = $value->creation;
			$appCount[$i]['total'] = $this->db->query('SELECT * FROM `app` WHERE creation LIKE "%'.$creation.'%" ORDER BY creation DESC')->num_rows();
			$appCount[$i]['creation'] = $creation;
			$i++;
		}
		$unique = array_map("unserialize", array_unique(array_map("serialize", $appCount)));
		//echo '<pre>';print_r($unique);echo '</pre>';exit;
		return $unique;
	}

	function getTotalAppsByDate($creationDate){
		$appCount = $this->db->query('SELECT * FROM `app` WHERE creation LIKE "%'.$creationDate.'%"')->result();
		//echo '<pre>';echo count($appCount);echo '</pre>';exit;
		return $appCount;
	}	

	/*function getPaidAppsByDate(){
		$paidAppCount = $this->db->query('SELECT * FROM `order` WHERE orderstatus = "paid"')->num_rows();
		return $paidAppCount;
	}*/	

	function getPaidAppsByDate($startdate){
		$paidAppCount = $this->db->query('SELECT * FROM `order` WHERE orderstatus = "paid" AND DATE_FORMAT(startdate,"%Y-%m-%d") BETWEEN "2008-01-01" AND "'.$startdate.'"')->num_rows();
		return $paidAppCount;
	}	

	function getTotalUsersByDate($creationDate){
		$userCount = $this->db->query('SELECT * FROM `tc_accounts` WHERE cdate LIKE "%'.$creationDate.'%"')->num_rows();
		return $userCount;
	}		

	function getAppUsersByAppId($appId){
		//echo 'SELECT accountId FROM `app` WHERE id = "'.$appId.'"';
		$userCount = $this->db->query('SELECT accountId FROM `app` WHERE id = "'.$appId.'"')->num_rows();
		return $userCount;
	}

	function getUserCountByDate($cdate){
		$userCount = $this->db->query('SELECT * FROM tc_accounts WHERE DATE_FORMAT(cdate,"%Y-%m-%d") BETWEEN "2008-01-01" AND "'.$cdate.'"')->num_rows();		
		return $userCount;
	}

	function getRevenueByAppDate($cdate){
		$revCount = $this->db->query('SELECT startdate, SUM(`totalamount`) AS amt FROM `order` WHERE DATE_FORMAT(startdate,"%Y-%m-%d") BETWEEN "2008-01-01" AND "'.$cdate.'"')->result();		
		return $revCount[0]->amt;
	}

	function getRevenueByAppId($appId){
		$revenue = $this->db->query('SELECT SUM(`totalamount`) AS amt FROM `order` WHERE `appid` = '.$appId)->result();
		$amount = $revenue[0]->amt;
		return $amount;	
	}

	function revenueToDate(){
		$dates = $this->db->query('SELECT DISTINCT(startdate) FROM `order` ORDER BY startdate DESC')->result();
		$revenue = array();		
		foreach($dates as $date){
			$revenue[] = $this->db->query('SELECT startdate, SUM(totalamount) as totalamount FROM `order` WHERE startdate = "'.$date->startdate.'" ORDER BY startdate DESC')->result();
		}

		return $revenue;			
	}

	function allDates(){
		$result = array_merge($this->getAppDates(), $this->getUserDates(), $this->getRevenueDates());
		$allDates =  array();
		$i=0;
		foreach ($result as $value) {
			$allDates[$i]['creation'] = $value->creation;
			if(isset($value->id)){
				$allDates[$i]['id'] = $value->id;	
			}
			$i++;
		}
		
		arsort($allDates);
		$unique = array_map("unserialize", array_unique(array_map("serialize", $allDates)));
		return $unique;

	}	

	function getRevnueByDate($revdate){
		$revenue = $this->db->query('SELECT SUM(totalamount) AS totalamount FROM `order` WHERE startdate LIKE "%'.$revdate.'%"')->result();
		return $revenue[0]->totalamount;

	}

	function getFunnelPaidAppsByDate($startdate){
		$paidAppCount = $this->db->query('SELECT * FROM `order` WHERE orderstatus = "paid" AND startdate LIKE "%'.$startdate.'%"')->num_rows();
		return $paidAppCount;
	}	


}