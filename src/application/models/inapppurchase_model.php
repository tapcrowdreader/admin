<?php
/**
 * Account model
 */
namespace Tapcrowd\Model;

/**
 * InAppPurchase model
 */
class Inapppurchase extends AbstractPDOModel
{
	const STRUCT = '\Tapcrowd\Structs\Inapppurchase';

	/**
	 * Select Inapppurchases
	 *
	 * @param array $query
	 * @return array
	 */
	public function select( Array $query )
	{
		$this->_validateQuery($query);

		$sql = 'SELECT * FROM inapppurchase WHERE 1';
		foreach($query as $column => $value) $sql .= ' AND ' . $column . " = " . (int)$value;
		$data = $this->query($sql)->fetchAll(\PDO::FETCH_CLASS, self::STRUCT);
		return $data;
	}

	/**
	 * Select Inapppurchase
	 *
	 * @param array $query
	 * @return object|null
	 */
	public function selectFirst( Array $query )
	{
		$this->_validateQuery($query);

		$sql = 'SELECT * FROM inapppurchase WHERE 1';
		foreach($query as $column => $value) $sql .= ' AND ' . $column . " = " . (int)$value;
		$sql .= ' LIMIT 1';
		$data = $this->query($sql)->fetchAll(\PDO::FETCH_CLASS, self::STRUCT);
		return empty($data)? null : $data[0];
	}

	/**
	 * Returns empty Inapppurchase
	 *
	 * @return object
	 */
	public function getEmpty()
	{
		$class = self::STRUCT;
		$data = new $class;
		return $data;
	}

	/**
	 * Save (add/update) Inapppurchase
	 *
	 * @param array $query
	 * @return bool
	 * @throws \RuntimeException
	 * @throws \BadMethodCallException
	 */
	public function save( Array $input )
	{
		if(empty($input) || empty($input['productid']) || empty($input['productvalue'])) {
			throw new \BadMethodCallException('Missing productid and/or productvalue values');
		}

		if(empty($input['id'])) {
			$sql = 'INSERT INTO inapppurchase (appid,productid,productvalue,type) VALUES (%d,%s,%s,%s)';
		} else {
			$sql = 'UPDATE inapppurchase SET appid=%d, productid=%s, productvalue=%s, type=%s WHERE id=%d';
		}
		$data = $this->execute($sql, $input['appid'], $input['productid'], $input['productvalue'], $input['type'], $input['id']);

		if(!is_int($data)) {
			throw new \RuntimeException('Could not add/update; System exception.');
		}

		return ($data > 0);
	}

	/**
	 * Delete Inapppurchase(s)
	 *
	 * @param array $query
	 * @return bool
	 */
	public function delete( Array $query )
	{
		$this->_validateQuery($query);

		$sql = 'DELETE FROM inapppurchase WHERE 1';
		foreach($query as $column => $value) $sql .= ' AND ' . $column . " = " . (int)$value;
		$data = $this->execute($sql);
		return !!$data;
	}

	/**
	 * Validate Inapppurchase query
	 *
	 * @param array $input
	 * @return void
	 * @throws \BadMethodCallException
	 * @throws \InvalidArgumentException
	 */
	protected function _validateQuery( Array $query )
	{
		if(empty($query)) {
			throw new \BadMethodCallException('Missing query');
		}

		if(!empty($query['appid']) && (!is_numeric($query['appid']) || $query['appid'] != _currentApp()->id)) {
			throw new \InvalidArgumentException('Missing or Invalid appId');
		}

		if(!empty($query['id']) && !is_numeric($query['id'])) {
			throw new \InvalidArgumentException('Missing or Invalid Id');
		}
	}
}
