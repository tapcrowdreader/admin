<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Sponsor_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getSponsorsByEventID($eventid) {
		$res = $this->db->query("SELECT * FROM sponsor WHERE eventid = $eventid ORDER BY `order` DESC, name ASC");
		if($res->num_rows() == 0) return FALSE;
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = base_url() . $row->image;
			}
		}
		return $res->result();
	}
    
	function getSponsorsByVenueID($venueid) {
		$res = $this->db->query("SELECT * FROM sponsor WHERE venueid = $venueid ORDER BY `order` DESC, name ASC");
		if($res->num_rows() == 0) return FALSE;
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = base_url() . $row->image;
			}
		}
		return $res->result();
	}
    
	function getSponsorById($id) {
		$res = $this->db->query("SELECT * FROM sponsor WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		$row = $res->row();
		//$row->image = base_url() . $row->image;
		return $row;
	}
	
	function getSponsorgroupsByEventID($eventid) {
		$res = $this->db->query("SELECT * FROM sponsorgroup WHERE eventid = $eventid ORDER BY sponsorgroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getSponsorgroupsByVenueID($venueid) {
		$res = $this->db->query("SELECT * FROM sponsorgroup WHERE venueid = $venueid ORDER BY sponsorgroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getSponsorPerGroupByEventID($eventid) {
		$res = $this->db->query("SELECT sg.* FROM sponsorgroup sg WHERE sg.eventid = $eventid ORDER BY sg.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		// sessions per group ophalen
		foreach ($res->result() as $group) {
			$sponsors = $this->db->query("SELECT * FROM sponsor WHERE sponsorgroupid = $group->id ORDER BY sponsor.order ASC");
			$group->sponsors = $sponsors->result();
		}
		return $res->result();
	}
	
	function getSponsorPerGroupByVenueID($venueid) {
		$res = $this->db->query("SELECT sg.* FROM sponsorgroup sg WHERE sg.venueid = $venueid ORDER BY sg.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		// sessions per group ophalen
		foreach ($res->result() as $group) {
			$sponsors = $this->db->query("SELECT * FROM sponsor WHERE sponsorgroupid = $group->id ORDER BY sponsor.order ASC");
			$group->sponsors = $sponsors->result();
		}
		return $res->result();
	}
	
	function getSponsorgroupByID($id) {
		$res = $this->db->query("SELECT * FROM sponsorgroup WHERE id = $id ORDER BY sponsorgroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	function edit_sponsorgroup($id, $data) {
		$this->db->where('id', $id);
		if($this->db->update('sponsorgroup', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function getSponsorGroupsByEvent($eventid) {
		$res = $this->db->query("SELECT * FROM sponsorgroup WHERE eventid = $eventid ORDER BY sponsorgroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getSponsorGroupsByVenue($venueid) {
		$res = $this->db->query("SELECT * FROM sponsorgroup WHERE venueid = $venueid ORDER BY sponsorgroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getSponsorByGroups($groupid) {
		$res = $this->db->query("SELECT * FROM sponsor WHERE sponsorgroupid = $groupid ORDER BY sponsor.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function removePremium($id) {
		if(is_numeric($id)) {
			$this->db->query("DELETE FROM premium WHERE tablename = 'sponsor' AND tableId  = $id");
		}
	}
}
