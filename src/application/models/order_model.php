<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Order_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}

	function createOrderForPrices($priceids, $appid, $paymentinfo) {
		$this->load->model("price_model", "price");
		//var_dump($priceids);
		$prices = array();
		$totalAmount = 0;
		$isforSubflavor = TRUE;
		foreach ($priceids as $priceid) {
			$cpr = $this->price->get_entry($priceid);
			$totalamount = (int)$cpr->price_setup+(int)$cpr->price_permonth*$cpr->billingperiod+(int)$totalamount;
			if($cpr->moduleid == "0") {
				$isforSubflavor = FALSE;
			}
			//var_dump($cpr);
			if($cpr->priceoptionid == 0) {
				//No priceoption, use for 
				$mainOrderPrice = $cpr;
			}
			$prices[]  = $cpr;
		}
		$user = \Tapcrowd\API::getCurrentUser();

		$ordertype = 'new';
		$paidsubflavors = $this->getPaidSubflavorForApp($appid);
		foreach ($paidsubflavors as $paidsubflavor) {
			// - /price_setup
			$paid = $paid + (float)$paidsubflavor->price_setup;
			$ordertype = 'update';
		}

		if($ordertype == 'update') {
			$discountFromPreviousOrders = (float)$this->alreadyPaidForAppTotal($appid)->paid;	
		}
		if($isforSubflavor) {
			$totalamount = $totalamount-(float)$paid+$discountFromPreviousOrders;	
			$discountFromPreviousOrders = 0;
		}
		
		$userid = $user->accountId;
		//echo "total: ".$totalamount;
		$vatval = $paymentinfo->vatAmount / 100;
		$totalex = $totalamount;
		$totalin = $totalamount * (1 + $vatval);
		$totalvat = $totalin-$totalex;

		$data = array(
			'appid' => $appid,
			'subflavorid' => $mainOrderPrice->subflavorid,
			'userid' => $userid,
			'totalamount' => $totalamount,
			'amountexcludingVAT' => $totalex,
			'amountVAT' => $totalvat,
			'VATpercentage' => $vatval,
			'amountincludingVAT' => $totalin,
			'startdate' => date("Y-m-d H:i:s"),
			'enddate' => date("Y-m-d H:i:s"),
			'contractduration' => $mainOrderPrice->minimumcontractduration,
            'billingperiod' => $mainOrderPrice->billingperiod,
            'timestamp' => date("Y-m-d H:i:s"),
            'currencyid' => $mainOrderPrice->currencyid,
            'ordertype' => $ordertype,
            'orderstatus' => 'open',
            'alreadypaid' => $paid,
		);
		//var_dump($data);
		$this->db->insert('order', $data);
		$orderid = $this->db->insert_id();
		foreach ($prices as $price) {
			$this->linkOrderPrice($orderid,$price->id);
		}
		return $this->get_entry($orderid);
	}

	function get_entry($id) {
		$res = $this->db->query("SELECT * FROM `order` WHERE id = $id LIMIT 1");
		return $res->row();
	}

	function linkOrderPrice($orderid, $priceid) {
		$data = array(
			'orderid' => $orderid,
			'priceid' => $priceid
		);
		$this->db->insert('orderprice', $data);
	}

	function getOrderPrices($orderid) {
		$res = $this->db->query("SELECT priceid FROM `orderprice` WHERE orderid = '$orderid'");
		$priceids = $res->result();
		$arrP = array();
		foreach ($priceids as $priceid) {
			$arrP[] = $priceid->priceid;
		}
		return $arrP;

	}

	function getPaidOrdersForApp($appid) {
		$res = $this->db->query("SELECT * FROM `order` WHERE appid = '$appid' AND orderstatus = 'paid'");
		return $res->result();
	}

	function getPaidSubflavorForApp($appid) {
		$res = $this->db->query("SELECT price.* FROM `order` LEFT JOIN `orderprice` ON order.id = orderprice.orderid LEFT JOIN price on orderprice.priceid = price.id  WHERE order.orderstatus = 'paid' AND price.priceoptionid = 0 AND order.appid = '$appid'");
		return $res->result();
	}

	function alreadyPaidForAppTotal($appid) {
		$res = $this->db->query("SELECT sum(alreadypaid) as paid FROM `order` WHERE orderstatus = 'paid' AND appid = '$appid'");
		return $res->row();
	}

	//True: everything paid!
	//False: no payments found
	//Array: list of items to pay for

	function isAppPaid($appid, $priceids) {
		//var_dump($priceids);
		$paidOrdersForApp = $this->getPaidOrdersForApp($appid);
		$orderedPrices = array();
		$paidPrices = array(); 
		if(count($paidOrdersForApp) > 0) {
			foreach ($paidOrdersForApp as $paidOrder) {
				$arr = $this->getOrderPrices($paidOrder->id);
				$paidPrices =  array_merge($arr, $paidPrices);
			}	
			$orderedPrices = array_diff($priceids, $paidPrices);

			if(count($orderedPrices) > 0) {
				return $orderedPrices;
			}else {
				return true;
			}
		}else {
			return false;
		}
		
		//$this->load->model("")
		//Function to determine whether the current app status is paid

	}
}
