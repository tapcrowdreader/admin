<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Module_mdl extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Module_mdl() {
		parent::__construct();
	}
	
	function getModules() {
		
	}
	
	function getVenueModules($venueid, $apptypeid = '') {
		$activeModuleIds = array();
		$activeModules = $this->db->query("SELECT moduletype FROM module WHERE venue = $venueid");
		$activeModules = $activeModules->result();
		foreach($activeModules as $activeModule) {
			$activeModuleIds[] = $activeModule->moduletype;
		}
		
		if($apptypeid == 5) {
			$res = $this->db->query("SELECT m.* FROM eventtypemoduletype em, moduletype m, defaultlauncher l, moduletype_subflavor ms, subflavor s WHERE em.venueid = 1 AND em.moduletype = m.id AND l.moduletypeid = m.id AND (ms.moduletypeid = m.id AND s.id = ms.subflavorid AND s.apptypeid =$apptypeid) AND m.id <> 0 ORDER BY l.`order`");
		} else {
			$res = $this->db->query("SELECT m.*, s.name as package FROM eventtypemoduletype em, moduletype m, moduletype_subflavor ms, subflavor s, defaultlauncher l WHERE em.venueid = 1 AND em.moduletype = m.id AND (ms.moduletypeid = m.id AND s.id = ms.subflavorid AND s.apptypeid =$apptypeid) AND l.moduletypeid = m.id AND m.id <> 0 ORDER BY l.`order`");
		}
        if($res->num_rows() == 0) return FALSE;
		foreach ($res->result() as $row) {
			if(in_array($row->id, $activeModuleIds)) {
				$row->active = 1;
			} else {
				$row->active = 0;
			}
		}
		return $res->result();
	}
	
	function getModulesByType($eventtype, $eventid, $apptypeid = '') {
		$appid = $this->db->query("SELECT * FROM appevent WHERE eventid = $eventid AND appid <> 2")->row()->appid;
		$app = $this->db->query("SELECT * FROM app WHERE id = $appid")->row();
		
		$activeModuleIds = array();
		$activeModules = $this->db->query("SELECT moduletype FROM module WHERE event = $eventid");
		$activeModules = $activeModules->result();
		foreach($activeModules as $activeModule) {
			$activeModuleIds[] = $activeModule->moduletype;
		}
		
		if($apptypeid == 5) {
			$sql = "SELECT mt.* FROM eventtypemoduletype em, moduletype mt, defaultlauncher l WHERE (em.eventtype = $eventtype OR em.appid = $appid) AND em.moduletype = mt.id AND l.moduletypeid = mt.id ORDER BY l.`order`";
		} else {
			$sql = "SELECT mt.*, s.name as package FROM eventtypemoduletype em, moduletype mt, moduletype_subflavor ms, subflavor s, defaultlauncher l WHERE (em.eventtype = $eventtype OR em.appid = $appid) AND em.moduletype = mt.id AND (ms.moduletypeid = mt.id AND s.id = ms.subflavorid AND s.apptypeid = $apptypeid) AND l.moduletypeid = mt.id ORDER BY l.`order`";
		}
		
		$eventModuleIds = array();
		$res = $this->db->query($sql);
		if($res->num_rows() == 0) return FALSE;
		foreach ($res->result() as $row) {
			if(in_array($row->id, $activeModuleIds)) {
				$row->active = 1;
			} else {
				$row->active = 0;
			}
			$eventModuleIds[$row->id] = $row->id;
		}
		$eventModules = $res->result();

		//modules from other flavors which are attached to this event.
		foreach($activeModuleIds as $activeModuleId) {
			if(!isset($eventModuleIds[$activeModuleId])) {
				
			}
		}
		
		return $eventModules;
	}
	
	function getModuleForEvent($eventid) {
		
	}
	
	function addModuleToEvent($eventid, $moduleid, $groupid = 0) {
		$data = array(
			'moduletype' => $moduleid,
			'event' => $eventid
		);
		$this->db->insert('module', $data);
		$resultid = $this->db->insert_id();

		$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = $moduleid LIMIT 1")->row();

		$appid = $this->app_model->getRealAppOfEvent($eventid);
		$app = $this->app_model->get($appid->appid);
		if($app->themeid != 0) {
			//use theme icon
			$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = $moduleid LIMIT 1");
			if($icon->num_rows() != 0) {
				$icon = $icon->row();
				$icon = $icon->icon;
				$image = $this->config->item('imagespath') . $icon;
				$newpath = 'upload/appimages/'.$appid->appid .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
				if(file_exists($image)) {
					if(!is_dir($this->config->item('imagespath') . 'upload/appimages/'.$appid->appid)){
						mkdir($this->config->item('imagespath') . 'upload/appimages/'.$appid->appid, 0775,true);
					}

					if(copy($image, $this->config->item('imagespath') . $newpath)) {
						$defaultlauncher->icon = $newpath;
					}
				}
			}
		}

		$launcherexists = $this->db->query("SELECT * FROM launcher WHERE eventid = $eventid AND moduletypeid = $moduleid LIMIT 1");
		if($launcherexists->num_rows() == 0) {
			// $defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = $moduleid LIMIT 1")->row();
			$launcherdata = array(
				'moduletypeid'	=> $moduleid,
				'eventid'		=> $eventid,
				'module'		=> $defaultlauncher->module,
				'title'			=> $defaultlauncher->title,
				'icon'			=> $defaultlauncher->icon,
				'order'			=> $defaultlauncher->order,
				'displaytype' 	=> $defaultlauncher->displaytype,
				'active'		=> 1,
				'groupid'		=> $groupid,
				'url'			=> $defaultlauncher->url
				);

			$this->general_model->insert('launcher',$launcherdata);
		} elseif($launcherexists->num_rows() == 1) {
			$row = $launcherexists->row();
			$this->general_model->update('launcher', $row->id, array('active' => 1, 'groupid' => $groupid));
		}
		$this->calcAndWriteToDB($app);

		if($moduleid == 64) {
			$attendees = false;
			$messages = false;
			$users = false;
			$eventmodules = $this->db->query("SELECT moduletype FROM module WHERE event = $eventid")->result();
			foreach($eventmodules as $m) {
				if($m->moduletype == 14) {
					$attendees = true;
				} elseif($m->moduletype == 65) {
					$messages = true;
				} elseif($m->moduletype == 49) {
					$users = true;
				}
			}

			if(!$attendees) {
				$this->addModuleToEvent($eventid, 14);
			}
			if(!$messages) {
				$this->addModuleToEvent($eventid, 65);
			}
			if(!$users) {
				$this->addModuleToEvent($eventid, 49);
			}
		}

		return $resultid;
	}
	
	function removeModuleFromEvent($eventid, $moduleid) {
		// Remove data from module
		$this->removeModuleData($eventid, 0, $moduleid);
		$this->db->where('moduletype', $moduleid);
		$this->db->where('event', $eventid);
		if($this->db->delete('module')){
			$launcherexists = $this->db->query("SELECT id FROM launcher WHERE eventid = $eventid AND moduletypeid = $moduleid LIMIT 1");
			if($launcherexists->num_rows() != 0) {
				$launcherdata = array(
						'active'		=> 0
					);

				$this->general_model->update('launcher',$launcherexists->row()->id, $launcherdata);
			}
		} else {
			return FALSE;
		}
		$appid = $this->app_model->getRealAppOfEvent($eventid);
		$app = $this->app_model->get($appid->appid);
		$this->calcAndWriteToDB($app);
	}
	
	function countModuleFromEventModule($eventid, $moduletypeid) {
		$res = $this->db->query("SELECT m.* FROM module m WHERE m.event = '$eventid' AND m.moduletype = '$moduletypeid' ");
		if($res->num_rows() == 0) return FALSE;
		return $res->num_rows();
	}
	
	function countModuleFromVenueModule($venueid, $moduletypeid) {
		$res = $this->db->query("SELECT m.* FROM module m WHERE m.venue = '$venueid' AND m.moduletype = '$moduletypeid' ");
		if($res->num_rows() == 0) return FALSE;
		return $res->num_rows();
	}
	

	function countModuleFromAppModule($appid, $moduletypeid) {
		$res = $this->db->query("SELECT m.* FROM module m WHERE m.app = '$appid' AND m.moduletype = '$moduletypeid' ");
		if($res->num_rows() == 0) return FALSE;
		return $res->num_rows();
	}
	function addModuleToVenue($venueid, $moduleid, $app = '', $groupid = '') {
		$moduleExists = $this->db->query("SELECT id FROM module WHERE venue = $venueid AND moduletype = $moduleid LIMIT 1");
		if($moduleExists->num_rows() == 0) {
		$data = array(
			'moduletype' => $moduleid,
			'venue' => $venueid
		);
		$this->db->insert('module', $data);
		$resultid = $this->db->insert_id();
		} else {
			$resultid = $moduleExists->row()->id;
		}

		$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = $moduleid LIMIT 1")->row();

		$appid = $this->app_model->getRealAppOfVenue($venueid);
		$app = $this->app_model->get($appid->appid);
		if($app->themeid != 0) {
			//use theme icon
			$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = $moduleid LIMIT 1");
			if($icon->num_rows() != 0) {
				$icon = $icon->row();
				$icon = $icon->icon;
				$image = $this->config->item('imagespath') . $icon;
				$newpath = 'upload/appimages/'.$appid->appid .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
				if(file_exists($image)) {
					if(copy($image, $this->config->item('imagespath') . $newpath)) {
						$defaultlauncher->icon = $newpath;
					}
				}
			}
		}

		$launcherexists = $this->db->query("SELECT * FROM launcher WHERE venueid = $venueid AND moduletypeid = $moduleid LIMIT 1");
		if($launcherexists->num_rows() == 0) {
			// $defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = $moduleid LIMIT 1")->row();

			//for business apps insert team instead artists
			$appid = $this->app_model->getRealAppOfVenue($venueid);
			$app = $this->app_model->get($appid->appid);
			if(($app->apptypeid == 4 || $app->apptypeid == 11) && $moduleid == 18) {
				$defaultlauncher->title = 'Team';
			}

			$launcherdata = array(
				'moduletypeid'	=> $moduleid,
				'venueid'		=> $venueid,
				'module'		=> $defaultlauncher->module,
				'title'			=> $defaultlauncher->title,
				'displaytype' 	=> $defaultlauncher->displaytype,
				'icon'			=> $defaultlauncher->icon,
				'order'			=> $defaultlauncher->order,
				'active'		=> 1,
				'url'			=> $defaultlauncher->url
				);

			if($moduleid != 50 && $moduleid != 51) {
				//don't update groupid for menu, gallery modules
				$launcherdata['groupid'] = $groupid;
			}

			if(($app->apptypeid == 7 || $app->apptypeid == 8) && $moduleid == 15) {
				$cataloggroupid = $this->db->query("SELECT * FROM `group` WHERE venueid = $venueid AND `name` = 'catalogcategories'")->row()->id;
				$launcherdata['groupid'] = $cataloggroupid;
			}

			$this->db->insert('launcher',$launcherdata);
		} else {
			if($moduleid == 50 || $moduleid == 51) {
				//don't update groupid for menu, gallery modules
				$this->general_model->update('launcher', $launcherexists->row()->id, array('active'=>1));
			} else {
				$this->general_model->update('launcher', $launcherexists->row()->id, array('active'=>1, 'groupid' => $groupid));
			}
		}
		$this->calcAndWriteToDB($app);
		return $resultid;
	}
	
	function removeModuleFromVenue($venueid, $moduleid) {
		//launcher non active
		$launcher = $this->db->query("SELECT id from launcher WHERE venueid = $venueid AND moduletypeid = $moduleid LIMIT 1");
		if($launcher->num_rows() != 0) {
			$launcher = $launcher->row();
			$this->general_model->update('launcher', $launcher->id, array('active'=>0));
		}
		// Remove data from module
		$this->db->where('moduletype', $moduleid);
		$this->db->where('venue', $venueid);
		if($this->db->delete('module')){
			return TRUE;
		} else {
			return FALSE;
		}
		$appid = $this->app_model->getRealAppOfVenue($venueid);
		$app = $this->app_model->get($appid->appid);
		$this->calcAndWriteToDB($app);
	}
	
	function addModuleToApp($appid, $moduleid) {
		if($moduleid == 18) {
			$res = $this->db->query("SELECT * FROM launcher WHERE appid = $appid AND moduletypeid = 18");
			if($res->num_rows() != 0) {
			$this->db->query("UPDATE launcher SET active = 1 WHERE appid = $appid AND moduletypeid = 18");
			}
		}
		$data = array(
			'moduletype' => $moduleid,
			'app' => $appid
		);
		$this->db->insert('module', $data);
		$resultid = $this->db->insert_id();
		$app = $this->app_model->get($appid);
		$launcherexists = $this->db->query("SELECT * FROM launcher WHERE appid = $appid AND moduletypeid = $moduleid LIMIT 1");
		if($launcherexists->num_rows() == 0) {
			$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = $moduleid LIMIT 1")->row();

			
			if(($app->apptypeid == 4 || $app->apptypeid == 11) && $moduleid == 18) {
				$defaultlauncher->title = 'Team';
			}

			if($app->themeid != 0) {
				$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = $moduleid LIMIT 1");
				if($icon->num_rows() != 0) {
					$icon = $icon->row();
					$icon = $icon->icon;
					$image = $this->config->item('imagespath') . $icon;
					$newpath = 'upload/appimages/'.$appid .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
					if(file_exists($image)) {
						if(copy($image, $this->config->item('imagespath') . $newpath)) {
							$defaultlauncher->icon = $newpath;
						}
					}
				}
			}

			$launcherdata = array(
				'moduletypeid'	=> $moduleid,
				'appid'			=> $appid,
				'module'		=> $defaultlauncher->module,
				'title'			=> $defaultlauncher->title,
				'displaytype' 	=> $defaultlauncher->displaytype,
				'icon'			=> $defaultlauncher->icon,
				'order'			=> $defaultlauncher->order,
				'active'		=> 1,
				'url'			=> $defaultlauncher->url
				);

			$this->db->insert('launcher',$launcherdata);
		}
		$this->calcAndWriteToDB($app);
		return $resultid;
	}
	
	function removeModuleFromApp($appid, $moduleid) {
		if($moduleid == 18) {
			$res = $this->db->query("SELECT * FROM launcher WHERE appid = $appid AND moduletypeid = 18");
			if($res->num_rows() != 0) {
			$this->db->query("UPDATE launcher SET active = 0 WHERE appid = $appid AND moduletypeid = 18");
			}
		}
		$this->db->where('moduletype', $moduleid);
		$this->db->where('app', $appid);
		if($this->db->delete('module')){
			return TRUE;
		} else {
			return FALSE;
		}
		$app = $this->app_model->get($appid);
		$this->calcAndWriteToDB($app);
	}
	
	function removeModuleData($eventid = 0, $venueid = 0, $moduleid) {
		// get module params
		$this->db->where('moduletype', $moduleid);
		$this->db->where('event', $eventid);
		if($eventid != 0){
			$res = $this->db->query("SELECT m.*, mt.name FROM module m, moduletype mt WHERE m.moduletype = $moduleid AND m.event = $eventid AND m.moduletype = mt.id");
		} else if($venueid != 0) {
			$res = $this->db->query("SELECT m.*, mt.name FROM module m, moduletype mt WHERE m.moduletype = $moduleid AND m.venue = $venueid AND m.moduletype = mt.id");
		}
		
		if($res->num_rows() == 0) return FALSE;
		
		switch($res->row()->name) {
			case "News feeds" :
				return TRUE;
				break;
			case "Exhibitor catalog" :
				return TRUE;
				break;
			case "Products, brands & services catalog" :
				return TRUE;
				break;
			case "POI's" :
				return TRUE;
				break;
			case "Interactive map" :
				return TRUE;
				break;
			case "Favourites" :
				return TRUE;
				break;
			case "Agenda" :
				return TRUE;
				break;
			case "Lead generation" :
				return TRUE;
				break;
			case "News flash" :
				return TRUE;
				break;
			case "Line-up & playlist" :
				return TRUE;
				break;
			default:
				return TRUE;
				break;
		}
		return FALSE;
	}
	
	function getById($id) {
		$res = $this->db->query("SELECT * FROM moduletype WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getLauncher($id, $type, $typeid) {
		//get launcher of venue or event
		if($type == 'venue') {
			$res = $this->db->query("SELECT * FROM launcher WHERE venueid = $typeid AND moduletypeid = $id");
			if($res->num_rows() != 0){
				$result = $res->row();
				$result->newLauncherItem = false;
			}
		} elseif($type == 'event') {
			$res = $this->db->query("SELECT * FROM launcher WHERE eventid = $typeid AND moduletypeid = $id");
			if($res->num_rows() != 0){
				$result = $res->row();
				$result->newLauncherItem = false;
			}
		} elseif($type == 'app') {
			$res = $this->db->query("SELECT * FROM launcher WHERE appid = $typeid AND moduletypeid = $id");
			if($res->num_rows() != 0){
				$result = $res->row();
				$result->newLauncherItem = false;
			}
		}
		
		//if no custom launcher is found get default launcher for this module
		if($res->num_rows() == 0){ 
			$result = $this->getDefaultLauncher($id);
		}

		$moduletype = $this->db->query("SELECT controller FROM moduletype WHERE id = $result->moduletypeid LIMIT 1");
		if($moduletype->num_rows() > 0) {
			$result->controller = $moduletype->row()->controller;
		}
		
		return $result;
	}
	
	function getLauncherById($id) {
		$result = false;
		$res = $this->db->query("SELECT * FROM launcher WHERE id = $id");
		if($res->num_rows() != 0){
			$result = $res->row();
		}
		return $result;
		
	}
	
	function getAllLaunchersOfEvent($eventid) {
		$moduletypeids = array();
		$eventLaunchers = $this->db->query("SELECT l.*, m.controller FROM launcher l INNER JOIN moduletype m ON m.id = l.moduletypeid WHERE eventid = $eventid");
		if($eventLaunchers->num_rows() != 0) {
			$eventLaunchers = $eventLaunchers->result();
			foreach($eventLaunchers as $eventLauncher) {
				$moduletypeids[] = $eventLauncher->moduletypeid;
			}
		} else {
			$eventLaunchers = array();
		}
		
		$defaultLaunchers = $this->db->query("SELECT l.*, m.controller FROM defaultlauncher l INNER JOIN moduletype m ON m.id = l.moduletypeid ");
		$defaultLaunchers = $defaultLaunchers->result();
		foreach($defaultLaunchers as $defLauncher) {
			if(!in_array($defLauncher->moduletypeid, $moduletypeids)) {
				$eventLaunchers[] = $defLauncher;
			}
		}

		foreach($eventLaunchers as $l) {
			if($l->controller == 'forms') {
				$template = $this->db->query("SELECT * FROM templateform WHERE moduletypeid = $l->moduletypeid LIMIT 1");
				if($template->num_rows > 0) {
					$template = $template->row();
					$l->emailsendresult = $template->emailsendresult;
				}
			}
		}
		return $eventLaunchers;
	}
	
	function getAllLaunchersOfVenue($venueid) {
		$moduletypeids = array();
		$venueLaunchers = $this->db->query("SELECT l.*, m.controller FROM launcher l INNER JOIN moduletype m ON m.id = l.moduletypeid WHERE venueid = $venueid");
		if($venueLaunchers->num_rows() != 0) {
			$venueLaunchers = $venueLaunchers->result();
			foreach($venueLaunchers as $venueLauncher) {
				$moduletypeids[] = $venueLauncher->moduletypeid;
			}
		} else {
			$venueLaunchers = array();
		}
		
		$defaultLaunchers = $this->db->query("SELECT l.*, m.controller FROM defaultlauncher l INNER JOIN moduletype m ON m.id = l.moduletypeid");
		$defaultLaunchers = $defaultLaunchers->result();
		foreach($defaultLaunchers as $defLauncher) {
			if(!in_array($defLauncher->moduletypeid, $moduletypeids)) {
				$venueLaunchers[] = $defLauncher;
			}
		}

		foreach($venueLaunchers as $l) {
			if($l->controller == 'forms') {
				$template = $this->db->query("SELECT * FROM templateform WHERE moduletypeid = $l->moduletypeid LIMIT 1");
				if($template->num_rows > 0) {
					$template = $template->row();
					$l->emailsendresult = $template->emailsendresult;
				}
			}
		}
		return $venueLaunchers;
	}
	
	function getAllLaunchersOfApp($appid) {
		$moduletypeids = array();
		$applaunchers = $this->db->query("SELECT * FROM launcher WHERE appid = $appid");
		if($applaunchers->num_rows() != 0) {
			$applaunchers = $applaunchers->result();
			foreach($applaunchers as $applauncher) {
				$moduletypeids[] = $applauncher->moduletypeid;
			}
		} else {
			$applaunchers = array();
		}
		


		return $applaunchers;
	}
	
	function getDefaultLauncher($id) { 
		$res = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = $id");
		if($res->num_rows() != 0){
			$result = $res->row();
			$result->newLauncherItem = true;
		} else {
			return false;
		}
		return $result;
	}
	
	function getSubflavor($appid) {
		$res = $this->db->query("SELECT s.* FROM subflavor s, appsubflavor a WHERE s.id = a.subflavorid AND a.appid = $appid");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getSubflavorsOfFlavor($id) {
		$res = $this->db->query("SELECT * FROM subflavor WHERE apptypeid = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	//check if artist module is activated for current app
	function artistsActive($appid) {
		$qry = $this->db->query("SELECT * FROM module WHERE moduletype = 18 AND app = $appid");
		if($qry->num_rows() > 0){
			return true;
		} else {
			return false;
		}
	}
	
	//check if artist module is activated for current app
	function socialActive($appid) {
		$qry = $this->db->query("SELECT * FROM module WHERE moduletype = 28 AND app = $appid");
		if($qry->num_rows() > 0){
			return true;
		} else {
			return false;
		}
	}
	
	function getArtistLauncher($appid, $apptypeid) {
		$res = $this->db->query("SELECT * FROM launcher WHERE appid = $appid AND moduletypeid = 18");
		if($res->num_rows() == 0) {
			$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = 18");
			$defaultlauncher = $defaultlauncher->row();

			if($apptypeid == '4' || $apptypeid == '11') {
				$defaultlauncher->title = 'Team';
			}
			return $defaultlauncher;
		}
		return $res->row();
	}
	
	function getDynamicModules($id, $type) {
		if($type == 'event') {
			$res = $this->db->query("SELECT * FROM launcher WHERE eventid = $id AND moduletypeid = 0 AND module = 'dynamic'");
			if($res->num_rows() == 0) {
				return array();
			} else {
				return $res->result();
			}
		} elseif($type == 'venue') {
			$res = $this->db->query("SELECT * FROM launcher WHERE venueid = $id AND moduletypeid = 0 AND module = 'dynamic'");
			if($res->num_rows() == 0) {
				return array();
			} else {
				return $res->result();
			}
		}
	}

	function getGroupModules($id, $type) {
		if($type == 'event') {
			$res = $this->db->query("SELECT * FROM launcher WHERE eventid = $id AND moduletypeid = 33");
			if($res->num_rows() == 0) {
				return array();
			} else {
				return $res->result();
			}
		} elseif($type == 'venue') {
			$res = $this->db->query("SELECT * FROM launcher WHERE venueid = $id AND (moduletypeid = 33 OR moduletypeid = 41)");
			if($res->num_rows() == 0) {
				return array();
			} else {
				return $res->result();
			}
		}
	}

	//create launchers for city app, like wine & dine, ...
	function createCityLaunchers($appid, $subflavorid) {
		$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = 1")->row();

		$news = array(
			'moduletypeid'	=> 1,
			'appid'			=> $appid,
			'module'		=> $defaultlauncher->module,
			'title'			=> $defaultlauncher->title,
			'icon'			=> $defaultlauncher->icon,
			'order'			=> 11,
			'active'		=> 1
			);

		$this->general_model->insert('launcher',$news);
		$this->general_model->insert('module', array('moduletype' => 1, 'app' => $appid));

		$eventcalendar = array(
			'moduletypeid'	=> 30,
			'appid'			=> $appid,
			'module'		=> 'eventTags',
			'title'			=> 'Event Calendar',
			'icon'			=> 'l_events',
			'order'			=> 12,
			'active'		=> 0,
			'tag'			=> 'EventCalendar'
			);

		$this->general_model->insert('launcher',$eventcalendar);

		$this->general_model->insert('module', array('moduletype' => 21, 'app' => $appid));

		$social = array(
			'moduletypeid'	=> 28,
			'appid'			=> $appid,
			'module'		=> 'social',
			'title'			=> "Social Media",
			'icon'			=> 'l_social',
			'order'			=> 19,
			'active'		=> 0
			);

		$this->general_model->insert('launcher',$social);

		$push = array(
			'moduletypeid'	=> 20,
			'appid'			=> $appid,
			'module'		=> 'push',
			'title'			=> "Push Notifications",
			'icon'			=> 'l_push',
			'order'			=> 20,
			'active'		=> 0
			);

		$this->general_model->insert('launcher',$push);
		
		$forms = array(
			'moduletypeid'	=> 44,
			'appid'			=> $appid,
			'module'		=> 'forms',
			'title'			=> 'Form',
			'icon'			=> 'l_form',
			'order'			=> 21,
			'active'		=> 0,
			'tag'			=> 'Form'
			);

		$this->general_model->insert('launcher',$forms);

		$ads = array(
			'moduletypeid'	=> 29,
			'appid'			=> $appid,
			'module'		=> 'ads',
			'title'			=> 'Ads',
			'icon'			=> 'l_ads',
			'order'			=> 30,
			'active'		=> 0,
			'displaytype' 	=> 'none',
			);

		$this->general_model->insert('launcher',$ads);
	}

	function insertFestivalLaunchers($eventid) {
		$launcher = $this->db->query("SELECT * FROM launcher l WHERE eventid = $eventid AND moduletypeid = 10 LIMIT 1");
		if($launcher->num_rows() == 0) {
			$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = 10")->row();
			$appid = $this->app_model->getRealAppOfEvent($eventid);
			$app = $this->app_model->get($appid->appid);
			if($app->themeid != 0) {
				//use theme icon
				$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = 10 LIMIT 1");
				if($icon->num_rows() != 0) {
					$icon = $icon->row();
					$icon = $icon->icon;
					$image = $this->config->item('imagespath') . $icon;
					$newpath = 'upload/appimages/'.$appid->appid .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
					if(file_exists($image)) {
						if(!is_dir($this->config->item('imagespath') . 'upload/appimages/'.$appid->appid)){
							mkdir($this->config->item('imagespath') . 'upload/appimages/'.$appid->appid, 0775,true);
						}

						if(copy($image, $this->config->item('imagespath') . $newpath)) {
							$defaultlauncher->icon = $newpath;
						}
					}
				}
			}
			$sessions = array(
				'moduletypeid'	=> 10,
				'eventid'		=> $eventid,
				'module'		=> $defaultlauncher->module,
				'title'			=> 'Dates & Line-up',
				'displaytype'	=> 'lineupv2',
				'icon'			=> $defaultlauncher->icon,
				'order'			=> 11,
				'active'		=> 1,
				'url'			=> $defaultlauncher->url
				);

			$this->general_model->insert('launcher',$sessions);
		}
	}

	function getLauncherOfGroupAndVenue($groupid, $venueid) {
		return $this->db->query("SELECT * FROM launcher WHERE venueid = $venueid AND groupid = $groupid LIMIT 1")->row();
	}

	function getLauncherOfGroupAndEvent($groupid, $eventid) {
		return $this->db->query("SELECT * FROM launcher WHERE eventid = $eventid AND groupid = $groupid LIMIT 1")->row();
	}

	function getLauncherOfGroupAndApp($groupid, $appid) {
		return $this->db->query("SELECT * FROM launcher WHERE appid = $appid AND groupid = $groupid LIMIT 1")->row();
	}

	function createContentLaunchers($id, $apptypeid) {
		$this->general_model->insert('contentmodule', array(
				'appid' => $id, 
				'title' => 'Home', 
				'icon' => '',
				'homemodule' => 1
			));
	}

	function getLauncherByTitle($type, $typeid, $title) {
		if($type == 'event') {
			$res = $this->db->query("SELECT * from launcher WHERE eventid = $typeid AND title = '$title' LIMIT 1")->row();
		}

		return $res;
	}

	function getModuletypes() {
		$res = $this->db->query("SELECT * FROM moduletype")->result();
		return $res;
	}

	function getModuletypeByComponent($component) {
		$res = $this->db->query("SELECT * FROM moduletype WHERE component = '$component' LIMIT 1")->row();
		return $res;
	}

	function calcAndWriteToDB($app) {
		$subflavor = $this->calculateSubflavor($app);
		$this->writeSubflavorForAppToDb($app, $subflavor);
	}

	function writeSubflavorForAppToDb($app, $subflavor) {
		$data = array(
           'subflavorid' => $subflavor->id,
           'subflavortype' => $subflavor->subflavortype
        );
	
		$this->db->where('id', $app->id);
		$this->db->update('app', $data);
	}


	function calculateSubflavor($app) {
		if($app->familyid == 1) {
			$subflavorids = array();
			$subflavors = $this->db->query("SELECT * FROM subflavor WHERE apptypeid = $app->apptypeid")->result();
			foreach($subflavors as $s) {
				$subflavorids[] = $s->id;
			}
			$subflavorids = implode(',', $subflavorids);

			$eventid = $this->db->query("SELECT eventid FROM appevent WHERE appid = $app->id LIMIT 1")->row()->eventid;

			//check for dynamic module
			$launchers = $this->db->query("SELECT * FROM launcher WHERE eventid = $eventid AND active = 1")->result();
			foreach($launchers as $l) {
				if($l->moduletypeid == 0) {
					$subflavor = $this->db->query("SELECT s.* FROM subflavor s INNER JOIN moduletype_subflavor ms ON s.id = ms.subflavorid WHERE ms.moduletypeid = 0 AND ms.subflavorid IN ($subflavorids) LIMIT 1")->row();
					return $subflavor;
				}
			}

			//no dynamic modules => check subflavor
			$event = $this->db->query("SELECT * FROM event WHERE id = $eventid LIMIT 1")->row();
			$modules = $this->db->query("SELECT ms.subflavorid FROM module m 
										INNER JOIN moduletype mt ON mt.id = m.moduletype 
										INNER JOIN moduletype_subflavor ms ON ms.moduletypeid = m.moduletype
										WHERE m.event = $event->id
										AND ms.subflavorid IN ($subflavorids)")->result();
			$highestSubflavorid = 0;
			foreach($modules as $m) {
				if($m->subflavorid > $highestSubflavorid) {
					$highestSubflavorid = $m->subflavorid;
				}
			}

			foreach($subflavors as $s) {
				if($s->id == $highestSubflavorid) {
					return $s;
				}
			}
		} elseif($app->familyid == 2) {
			$subflavorids = array();
			$subflavors = $this->db->query("SELECT * FROM subflavor WHERE apptypeid = $app->apptypeid")->result();
			foreach($subflavors as $s) {
				$subflavorids[] = $s->id;
			}
			$subflavorids = implode(',', $subflavorids);

			$venueid = $this->db->query("SELECT venueid FROM appvenue WHERE appid = $app->id LIMIT 1")->row()->venueid;

			//check for dynamic module
			$launchers = $this->db->query("SELECT * FROM launcher WHERE venueid = $venueid AND active = 1")->result();
			foreach($launchers as $l) {
				if($l->moduletypeid == 0) {
					$subflavor = $this->db->query("SELECT s.* FROM subflavor s INNER JOIN moduletype_subflavor ms ON s.id = ms.subflavorid WHERE ms.moduletypeid = 0 AND ms.subflavorid IN ($subflavorids) LIMIT 1")->row();
					return $subflavor;
				}
			}

			//no dynamic modules => check subflavor
			$venue = $this->db->query("SELECT * FROM venue WHERE id = $venueid LIMIT 1")->row();
			$modules = $this->db->query("SELECT ms.subflavorid FROM module m 
										INNER JOIN moduletype mt ON mt.id = m.moduletype 
										INNER JOIN moduletype_subflavor ms ON ms.moduletypeid = m.moduletype
										WHERE m.venue = $venue->id
										AND ms.subflavorid IN ($subflavorids)")->result();
			$highestSubflavorid = 0;
			foreach($modules as $m) {
				if($m->subflavorid > $highestSubflavorid) {
					$highestSubflavorid = $m->subflavorid;
				}
			}

			foreach($subflavors as $s) {
				if($s->id == $highestSubflavorid) {
					return $s;
				}
			}
		} elseif($app->familyid == 3) {
			$subflavorids = array();
			$subflavors = $this->db->query("SELECT * FROM subflavor WHERE apptypeid = $app->apptypeid")->result();
			foreach($subflavors as $s) {
				$subflavorids[] = $s->id;
			}
			$subflavorids = implode(',', $subflavorids);

			//get launchers
			$moduletypeids = array();
			$launchers = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND active = 1")->result();
			foreach($launchers as $l) {
				//check dynamic module
				if($l->moduletypeid == 0) {
					$subflavor = $this->db->query("SELECT s.* FROM subflavor s INNER JOIN moduletype_subflavor ms ON s.id = ms.subflavorid WHERE ms.moduletypeid = 0 AND ms.subflavorid IN ($subflavorids) LIMIT 1")->row();
					return $subflavor;
				}
				$moduletypeids[] = $l->moduletypeid;
			}

			$moduletypeidsString = implode(',', $moduletypeids);

			$modules = $this->db->query("SELECT ms.subflavorid FROM moduletype_subflavor ms 
										WHERE ms.moduletypeid IN ($moduletypeidsString)
										AND ms.subflavorid IN ($subflavorids)")->result();
			$highestSubflavorid = 0;
			foreach($modules as $m) {
				if($m->subflavorid > $highestSubflavorid) {
					$highestSubflavorid = $m->subflavorid;
				}
			}

			foreach($subflavors as $s) {
				if($s->id == $highestSubflavorid) {
					return $s;
				}
			}
		}
	}

	//this function displayed all modules can be of use for later so commented instead of deleted
	// function getModulesForMenu($type, $typeid, $app, $current = '') {
	// 	$array = array();
	// 	if($type == 'event') {
	// 		//get all default launchers (seperate because we need them all for orders)
	// 		$defaultlaunchersResult = $this->db->query("SELECT id, moduletypeid, title, `order` FROM defaultlauncher")->result();
	// 		$defaultLaunchers = array();
	// 		foreach($defaultlaunchersResult as $d) {
	// 			$defaultLaunchers[$d->moduletypeid] = $d;
	// 		}

	// 		//get all active launchers for this event
	// 		$launchers = $this->db->query("SELECT mt.id, mt.name, mt.controller, l.id as launcherid, l.eventid, l.moduletypeid, l.title, l.active FROM launcher l INNER JOIN moduletype mt ON l.moduletypeid = mt.id WHERE l.eventid = $typeid AND l.active = 1")->result();
	// 		$launchermoduletypeids = array();
	// 		foreach($launchers as $l) {
	// 			if(isset($defaultLaunchers[$l->moduletypeid])) {
	// 				$l->order = $defaultLaunchers[$l->moduletypeid]->order;
	// 			}
	// 			$launchermoduletypeids[] = $l->moduletypeid;
	// 		}
	// 		$launchermoduletypeids = implode(',', $launchermoduletypeids);

	// 		//get all modules for this event in right flavor with defaultlauncher information
	// 		$modules = $this->db->query("SELECT mt.id, mt.name, mt.controller, dl.moduletypeid, dl.module, dl.title, dl.`order` FROM eventtypemoduletype em 
	// 									INNER JOIN moduletype mt ON mt.id = em.moduletype
	// 									INNER JOIN defaultlauncher dl ON dl.moduletypeid = em.moduletype
	// 									INNER JOIN moduletype_subflavor ms ON ms.moduletypeid = em.moduletype 
	// 									INNER JOIN subflavor s ON s.id = ms.subflavorid
	// 									WHERE em.eventtype = 3 AND s.apptypeid = $app->apptypeid AND mt.id NOT IN ($launchermoduletypeids)
	// 									ORDER BY dl.`order`")->result();
			
	// 		//add launchers to modules
	// 		foreach($launchers as $l) {
	// 			$modules[] = $l;
	// 		}

	// 		//sort modules
	// 		uasort($modules,array($this, 'cmp'));

	// 		//add extra information
	// 		foreach($modules as $m) {
	// 			$m->typeid = $typeid;
	// 			$m->type = $type;
	// 			if($m->controller == $current) {
	// 				$m->selected = 1;
	// 			} else {
	// 				$m->selected = 0;
	// 			}
	// 		}
			
	// 		$array[] = $modules;
			
	// 		//get dynamic modules
	// 		$dynamic = $this->getDynamicModules($typeid, 'event');
	// 		foreach($dynamic as $m) {
	// 			$m->typeid = $typeid;
	// 			$m->type = $type;
	// 		}
	// 		$array[] = $dynamic;
	// 	}
	// 	return $array;
	// }

	function getModulesForMenu($type, $typeid, $app, $current = '', $launcherid = 0) {
		//get all default launchers (seperate because we need them all for orders)
		$defaultlaunchersResult = $this->db->query("SELECT id, moduletypeid, title, `order` FROM defaultlauncher")->result();
		$defaultLaunchers = array();
		foreach($defaultlaunchersResult as $d) {
			$defaultLaunchers[$d->moduletypeid] = $d;
		}

		$launchers = $this->db->query("SELECT mt.id, mt.name, mt.controller, mt.component, l.id as launcherid, l.".$type.'id'.", l.moduletypeid, l.title, l.active, l.groupid FROM launcher l INNER JOIN moduletype mt ON l.moduletypeid = mt.id WHERE l.".$type.'id'." = $typeid AND l.active = 1 AND mt.id <> 0")->result();
		foreach($launchers as $l) {
			if(isset($defaultLaunchers[$l->moduletypeid])) {
				$l->order = $defaultLaunchers[$l->moduletypeid]->order;
			}
		}

		//get dynamic modules
		$dynamic = $this->getDynamicModules($typeid, $type);
		foreach($dynamic as $m) {
			if($m->active == 1) {
				$launchers[] = $m;
			}
		}

		//sort modules
		uasort($launchers,array($this, 'cmp'));

		//add extra information
		foreach($launchers as $m) {
			$m->typeid = $typeid;
			$m->type = $type;
			$m->selected = 0;
			if($current != '' && $m->controller == $current && $current != 'groups' && $m->component != 'projects' && $m->component != 'services' && $m->component != 'careers') {
				if($launcherid != 0) {
					if($m->launcherid == $launcherid) {
						$m->selected = 1;
					}
				} elseif($current != 'forms') {
					$m->selected = 1;
				}
			} elseif($current == 'menu' && $m->id == 50) {
				$m->selected = 1;
			} elseif(($current == 'gallery' || $current == 'pictures') && $m->id == 51) {
				$m->selected = 1;
			} elseif($current == 'team' && $m->id == 52) {
				$m->selected = 1;
			} elseif($m->controller == null && $current == 'dynamiclauncher' && $launcherid == $m->id) {
				$m->selected = 1;
			} elseif(strtolower($m->component) == strtolower($current) && $m->component != '') {
				$m->selected = 1;
			}


			if($app->apptypeid == 4 && $m->id == 21 || $m->id == 22 || $m->id == 23) {
				$m->selected = 0;
				if($m->id == 22 && $current == 'location') {
					$m->selected = 1;
				} elseif($m->id == 23 && $current == 'contact') {
					$m->selected = 1;
				} elseif($m->id == 21 && $current == 'about') {
					$m->selected = 1;
				}
			}
		}
		
		return $launchers;
	}

	// Comparison function
	static function cmp($a, $b) {
		if(!isset($b->order)) {
			return 1;
		}
		if($b->order == 0) {
			return 1;
		}
		if(isset($a->order) && isset($b->order)) {
			$a->order = (int) $a->order;
			$b->order = (int) $b->order;
			if ($a->order == $b->order) {
				return 1;
			}
			return ($a->order < $b->order) ? -1 : 1;
		} else {
			return 1;
		}
	}


	function removeLauncher($launcher) {
		if(!empty($launcher) && !empty($launcher->tag) && !empty($launcher->appid)) {
			$this->db->query("DELETE FROM tag WHERE tag = '$launcher->tag' AND appid = $launcher->appid");
			$this->db->query("DELETE FROM launcher WHERE id = $launcher->id");
		}

		return true;
	}

	function checkPlacesCats($typeid, $type, $app) {
		$tableid = $type.'id';
		$res = $this->db->query("SELECT id FROM `group` WHERE $tableid = $typeid AND name = 'placescategories' LIMIT 1");
		if($res->num_rows() > 0) {
			return $res->row()->id;
		}

		$groupdata = array(
				'appid'	=> $app->id,
				$tableid	=> $typeid,
				'name'	=> 'placescategories',
				'parentid'	=> 0
			);
		return $this->general_model->insert('group', $groupdata);
	}

	function checkTeamCats($typeid, $type, $app) {
		$tableid = $type.'id';
		$res = $this->db->query("SELECT id FROM `group` WHERE $tableid = $typeid AND name = 'Team' LIMIT 1");
		if($res->num_rows() > 0) {
			return $res->row()->id;
		}
		$groupdata = array(
				'appid'	=> $app->id,
				$tableid	=> $typeid,
				'name'	=> 'Team',
				'parentid'	=> 0
			);
		return $this->general_model->insert('group', $groupdata);
	}

	function checkAttendeesCats($typeid, $type, $app) {
		$tableid = $type.'id';
		$res = $this->db->query("SELECT id FROM `group` WHERE $tableid = $typeid AND name = 'attendeecategories' LIMIT 1");
		if($res->num_rows() > 0) {
			return $res->row()->id;
		}

		$groupdata = array(
				'appid'	=> $app->id,
				$tableid	=> $typeid,
				'name'	=> 'attendeecategories',
				'parentid'	=> 0
			);
		return $this->general_model->insert('group', $groupdata);
	}

	function checkSpeakersCats($typeid, $type, $app) {
		$tableid = $type.'id';
		$res = $this->db->query("SELECT id FROM `group` WHERE $tableid = $typeid AND name = 'speakercategories' LIMIT 1");
		if($res->num_rows() > 0) {
			return $res->row()->id;
		}

		$groupdata = array(
				'appid'	=> $app->id,
				$tableid	=> $typeid,
				'name'	=> 'speakercategories',
				'parentid'	=> 0
			);
		return $this->general_model->insert('group', $groupdata);
	}

	function checkCatalogCats($typeid, $type, $app) {
		$tableid = $type.'id';
		$res = $this->db->query("SELECT id FROM `group` WHERE $tableid = $typeid AND name = 'catalogcategories' LIMIT 1");
		if($res->num_rows() > 0) {
			return $res->row()->id;
		}
		$groupdata = array(
				'appid'	=> $app->id,
				$tableid	=> $typeid,
				'name'	=> 'catalogcategories',
				'parentid'	=> 0
			);
		return $this->general_model->insert('group', $groupdata);
	}

	function getLauncheridsOfObjects($type, $objects) {
		$launcherids = array();
		if(!empty($objects)) {
			$field = $type.'id';
			$objectids = array();
			foreach($objects as $object) {
				$objectids[] = $object->id;
			}

			$objectids = implode(',', $objectids);
			
			if(!empty($objectids)) {
				$launchers = $this->db->query("SELECT id FROM launcher WHERE $field IN ($objectids)")->result();
				foreach($launchers as $l) {
					$launcherids[] = $l->id;
				}
			}
		}

		return $launcherids;
	}

	/**
	 * Return listing of active launchers supported by the user module.
	 *
	 * @param object $object (venue, app or event object)
	 * @param string $objectType (venue, app or event)
	 * @return array
	 */
	public function getActiveLaunchers( $object, $objectType, $usermodule = 'user' )
	{
		if($usermodule == 'user') {
			$supportedModuleTypes = array(
				52,	# Team
				25,	# Projects
				24,	# Services
				15,	# Catalog
				54,	# Places
				27,	# Careers
				1,	# News
				14,	# Attendees
				39,	# Postpicture, Photo Sharing,
				43  # Personal programme
			);
			$sql = 'SELECT * FROM launcher WHERE %s=%d AND active=1 AND moduletypeid IN("%s") ORDER BY `order`';
			$res = $this->db->query(sprintf($sql, $objectType.'id', $object->id, implode('","',$supportedModuleTypes)));
		} elseif($usermodule == 'notes') {
			$supportedModuleTypes = array(
				10, #sessions
				2, #exhibitors
				19, #sponsors
				40 #speakers
			);
			$sql = 'SELECT * FROM launcher WHERE %s=%d AND active=1 AND moduletypeid IN("%s") ORDER BY `order`';
			$res = $this->db->query(sprintf($sql, $objectType.'id', $object->id, implode('","',$supportedModuleTypes)));
		}  elseif($usermodule == 'socialshare') {
			$supportedModuleTypes = array(
				10, #sessions
				2, #exhibitors
				19, #sponsors
				1,	# News
				14,	# Attendees
				54,	# Places
			);
			$sql = 'SELECT * FROM launcher WHERE %s=%d AND active=1 AND moduletypeid IN("%s") ORDER BY `order`';
			$res = $this->db->query(sprintf($sql, $objectType.'id', $object->id, implode('","',$supportedModuleTypes)));
		} else {
			$sql = 'SELECT * FROM launcher WHERE %s=%d AND active=1 ORDER BY `order`';
			$res = $this->db->query(sprintf($sql, $objectType.'id', $object->id));
		}
		
		return $res->result();
	}

	function getAvailableCityVenueModules($excluded = array()) {
		$ids = array(21, 1, 28, 29, 44, 0);
		if(!empty($excluded)) {
			$ids = array_diff($ids, $excluded);
		}
		$ids = implode(',', $ids);
		$modules = $this->db->query("SELECT dl.* FROM defaultlauncher dl WHERE moduletypeid IN ($ids)");

		return $modules->result();
	}

	function getAvailableCityEventModules($excluded = array()) {
		$ids = array(21, 1, 28, 29, 44, 0, 2, 5, 10, 14);
		if(!empty($excluded)) {
			$ids = array_diff($ids, $excluded);
		}
		$ids = implode(',', $ids);
		$modules = $this->db->query("SELECT dl.* FROM defaultlauncher dl WHERE moduletypeid IN ($ids)");

		return $modules->result();
	}

	function checkUserModuleSettings($appid) {
		$exists = $this->db->query("SELECT id FROM usermodule WHERE appid = $appid LIMIT 1");
		if($exists->num_rows() == 0) {
			$this->general_model->insert('usermodule', array(
					'appid' => $appid,
					'stayloggedin' => 1
				));
		}
	}

	function getEventsCityLaunchers($app) {
		$res = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND moduletypeid = 30");
		return $res->result();
	}

	function getVenuesCityLaunchers($app) {
		$res = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND moduletypeid = 31");
		return $res->result();
	}
}
