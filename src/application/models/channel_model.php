<?php
/**
 * Tapcrowd Platform | Channel Model
 *
 * @author Tom Van de Putte
 */
namespace Tapcrowd\Model;

use \Tapcrowd\Structs\Channel as ChannelStruct;

class ChannelPermissionException extends \RuntimeException {}
class InvalidChannelSettingException extends \InvalidArgumentException {}
class UnsupportedStructException extends \InvalidArgumentException {}

class InvalidChannelException extends \InvalidArgumentException
{
	public function __construct()
	{
		$this->message = filter_var($_SERVER['HTTP_HOST'], FILTER_SANITIZE_URL);
	}
}


/**
 * Channel Model
 */
class Channel extends AbstractPDOModel
{
	/**
	 * Namespace for structs
	 * @var string
	 */
	const STRUCT_NAMESPACE = '\Tapcrowd\Struct\\';

	/**
	 * Current Channel settings
	 * @var \Tapcrowd\Structs\Channel
	 */
	protected $channel;

	/**
	 * Current Channel settings
	 * @var array
	 */
	protected $channel_settings;

	/**
	 * Get Current Channel
	 *
	 * Returns the current channel, based on the HTTP Host
	 *
	 * @return \Tapcrowd\Structs\Channel
	 * @throws \Tapcrowd\Model\InvalidChannelException
	 */
	public function getCurrentChannel()
	{
		if(!isset($this->channel)) {
			$domain = filter_var($_SERVER['HTTP_HOST'], FILTER_SANITIZE_URL);
			if(empty($domain)) {
				throw new InvalidChannelException();
			}

			$sql = 'SELECT channel.* FROM channel, channelurl
				WHERE channelurl.channelid = channel.id AND channelurl.url = %s
				LIMIT 1';
			$stat = $this->query($sql, $domain);
			$this->channel = $this->_parseChannelData($stat);
		}
		return $this->channel;
	}

	/**
	 * Returns channels
	 *
	 * If current user not is admin, only return current channel.
	 *
	 * @return \ArrayObject
	 */
	public function listChannels()
	{
		$list = new \ArrayObject();

		if(\_isAdmin() && ($this->getCurrentChannel()->channelId == 1)) {
			$sql = 'SELECT channel.* FROM channel, channelurl WHERE channelurl.channelid = channel.id';
			$stat = $this->query($sql);
			while(($row = $stat->fetch()) !== false) {

				# Create channel
				$channel = new \Tapcrowd\Channel;
				foreach($row as $k => $v) $channel->$k = $v;
				$channel->channelId = (int)$channel->id;

				# Add to list
				$list->offsetSet( $channel->name, $channel );
				$list->offsetSet( $channel->id, $channel );
			}
		} else {
			$channel = $this->getCurrentChannel();
			$list->offsetSet( $channel->name, $channel );
			$list->offsetSet( $channel->id, $channel );
		}

		return $list;
	}

	/**
	 * Get Channel
	 *
	 * Returns the channel
	 *
	 * @param int $channelId
	 * @return \Tapcrowd\Structs\Channel
	 * @throws \Tapcrowd\Model\InvalidChannelException
	 */
	public function getChannel( $channelId )
	{
		$sql = 'SELECT channel.* FROM channel WHERE id=%d LIMIT 1';
		$stat = $this->query($sql, (int)$channelId);
		return  $this->_parseChannelData($stat);
	}



	/**
	 * Returns the hashed string (channel specific)
	 *
	 * @param string $string
	 * @param string $algorithm sha1, md5 etc..
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	public function getHash( $string, $algorithm = 'sha1' )
	{
		if(!in_array($algorithm, hash_algos())) {
			throw new \InvalidArgumentException('Invalid Algorithm');
		}

		switch($this->getCurrentChannel()->channelId) {
			case 1: # TapCrowd Channel
				$shared_salt = 'qWM527PvdQF16cEsvxwi5hc6HVcHTGrf';
				break;
			case 15: # AXA Channel
				$shared_salt = '6pi2DeEGn0zMR25ZrwUXwVrnoYKmf9dw';
				break;
			default:
				$shared_salt = 'CkMtqWM527PvdQF16cEsvxwi5hc6HVcH';
				break;
		}

		return hash_hmac($algorithm, $string, $shared_salt);
	}

	/**
	 * Returns all Settings for the given (or current) channel
	 *
	 * @param \Tapcrowd\Structs\Channel $channel Optional, defaults to current channel
	 * @return ArrayObject
	 */
	public function getChannelSettings(  \Tapcrowd\Structs\Channel &$channel = null )
	{
		# If channel === Current channel
		if($channel === null) {
			$channel = $this->getCurrentChannel();
			$settings =& $this->channel_settings;
			if(!empty($settings)) return $settings;
		}

		$settings = new \ArrayObject();

		$sql = 'SELECT def.n AS name, IF(setting.v IS NULL, def.v, setting.v) AS `value`, def.v AS `default`
			FROM tc_channel_settings def
			LEFT OUTER JOIN tc_channel_settings setting ON setting.channelId=%d AND setting.n=def.n
			WHERE def.channelId=1';
		$stat = $this->query($sql, $channel->channelId);
		while(($setting = $stat->fetch()) !== false) {
			$name = array_shift($setting);
			$settings->offsetSet( $name, (object)$setting );
		}

		return $settings;
	}

	/**
	 * Change a channel setting
	 * @param string $name
	 * @param string $value Setting to NULL removes setting
	 * @param \Tapcrowd\Structs\Channel $channel Optional, defaults to current channel.
	 * @return void
	 * @throws \Tapcrowd\Model\InvalidChannelSettingException
	 */
	public function setChannelSetting( $name, $value, \Tapcrowd\Structs\Channel &$channel = null )
	{
		if($channel === null) $channel = $this->getCurrentChannel();

		if(!is_string($name) || (!is_string($value) && $value !== null)) {
			throw new \Tapcrowd\Model\InvalidChannelSettingException($name);
		}

		# Validate setting name
		$sql = 'SELECT channelId FROM tc_channel_settings WHERE channelId=1 AND n=%s LIMIT 1';
		$stat = $this->query($sql, $name);
		if($stat->rowCount() !== 1) {
			throw new \Tapcrowd\Model\InvalidChannelSettingException($name);
		}

		if($value === null) {
			$sql = 'DELETE FROM tc_channel_settings WHERE channelId=%d AND n=%s';
		} else {
			$sql = 'INSERT INTO tc_channel_settings (channelId, n, v) VALUES(%d, %s, %s)
				ON DUPLICATE KEY UPDATE v=VALUES(v)';
		}
		$this->execute($sql, $channel->channelId, $name, $value);
	}

	/**
	 * Returns the objects for the given type, supported by the channel
	 * @param string $type (eg.: theme, flavor, etc...)
	 * @param \Tapcrowd\Structs\Channel $channel
	 * @return \ArrayObject
	 * @throws \Tapcrowd\Model\UnsupportedStructException
	 */
	public function getChannelSupport( $type, \Tapcrowd\Structs\Channel &$channel)
	{
		if(!is_string($type)) throw new UnsupportedStructException($type);
		switch($type) {
			case 'theme':
				$sql = 'SELECT theme.* FROM theme INNER JOIN tc_channel_theme
					ON theme.id=tc_channel_theme.themeid AND tc_channel_theme.channelId=%d
					ORDER BY sortOrder ASC';
				break;
			case 'flavor':
				$sql = 'SELECT apptype.* FROM apptype INNER JOIN apptype_channel xref
					ON apptype.id=xref.apptypeid AND xref.channelid=%d';
				break;
			default:
				throw new UnsupportedStructException($type);
		}

		$stat = $this->query($sql, $channel->channelId);
		$stat->setFetchMode(\PDO::FETCH_CLASS, self::STRUCT_NAMESPACE.ucfirst($type));
		$structs = new \ArrayObject;
		while(($struct = $stat->fetch()) !== false) {
			$structs->offsetSet( $struct->id, $struct );
			$structs->offsetSet( $struct->name, $struct );
		}
		return $structs;
	}

	/**
	 * Enable channel support for the given object
	 * @param \Tapcrowd\Structs\Basestruct $object
	 * @param \Tapcrowd\Structs\Channel $channel
	 * @return bool
	 * @throws \Tapcrowd\Model\UnsupportedStructException
	 */
	public function enableChannelSupport( \Tapcrowd\Structs\Basestruct &$object, \Tapcrowd\Structs\Channel &$channel)
	{
		switch(basename(get_class($object))) {
			case 'Theme':
				$sql = 'INSERT IGNORE INTO tc_channel_theme (themeId, channelId) VALUES(%d, %d)';
				break;
			case 'Flavor':
				$sql = 'INSERT IGNORE INTO apptype_channel (apptypeid, channelid) VALUES(%d, %d)';
				break;
			default:
				throw new UnsupportedStructException($type);
		}
		return !!$this->execute($sql, $object->id, $channel->channelId);
	}

	/**
	 * Disable channel support for the given object
	 * @param \Tapcrowd\Structs\Basestruct $object
	 * @param \Tapcrowd\Structs\Channel $channel
	 * @return bool
	 * @throws \Tapcrowd\Model\UnsupportedStructException
	 */
	public function disableChannelSupport( \Tapcrowd\Structs\Basestruct &$object, \Tapcrowd\Structs\Channel &$channel)
	{
		switch(basename(get_class($object))) {
			case 'Theme':
				$sql = 'DELETE FROM tc_channel_theme WHERE themeId=%d AND channelId=%d';
				break;
			case 'Flavor':
				$sql = 'DELETE FROM apptype_channel WHERE apptypeid=%d AND channelid=%d';
				break;
			default:
				throw new UnsupportedStructException($type);
		}
		return !!$this->execute($sql, $object->id, $channel->channelId);
	}

	/**
	 * Internal method to create a Channel object fron a PDOStatement
	 * @param \PDOStatement $stat
	 * @return \Tapcrowd\Structs\Channel
	 * @throws \Tapcrowd\Model\InvalidChannelException
	 */
	private function _parseChannelData( \PDOStatement $stat )
	{
		$stat->setFetchMode(\PDO::FETCH_CLASS, '\Tapcrowd\Structs\Channel');

		if($stat->rowCount() == 0 || ($channel = $stat->fetch()) === false) {
			throw new InvalidChannelException();
		}
		$channel->channelId = (int)$channel->id;

		# Setup Logo
		if(!empty($channel->templatefolder)) {
			$filepath = "/templates/{$channel->templatefolder}{$channel->logo}";
			$finfo = new \SplFileInfo(dirname(dirname(__DIR__)) . $filepath);
			if($finfo->isReadable()) $channel->logo = $filepath;
		}

		# Setup Support Contact
		$channel->support = (object)array(
			'email' => $channel->support,
		);

		# Tapcrowd Information
		if($channel->channelId == 1) {
			$channel->information = '<div style="float:left;">
					<p>
						<strong>TapCrowd NV</strong><br />
						Grauwpoort 1<br />
						9000 Gent<br />
						BELGIUM<br /><br />
					</p>
					<p>
						<strong>TapCrowd Brazil</strong><br />
						Av. Eng. Luis Carlos Berrini 1500<br/>
						Brooklin Novo, São Paulo, SP<br />
						CEP 04571-000<br />
						BRAZIL<br />
					</p>
				</div>
				<div style="float:right;">
					<p>
						E: info@tapcrowd.com<br />
						T: +32 9 298 01 92<br />
						VAT: 0818.826.191<br />
						Acc. Nr.: 738-0281377-02<br /><br />
					</p>
					<p>
						E: contato@tapcrowd.com<br />
						T: +55 11 3588 0400<br />
					</p>
				</div>';
		}

		return $channel;
	}

	/**
	 * Validate input
	 * @param mixed $value
	 * @param string|array $filter
	 * @param string $message defaults to empty string
	 * @throws \InvalidArgumentException
	 * @throws RuntimeException
	 */
	private function _validate( $value, $filter, $message = '' )
	{
		$invalid = true;

		# String Filters
		if(is_string($filter)) {
			switch($filter) {
				case 'is_numeric':
					if(is_numeric($value)) $invalid = false;
					break;
				case 'not_empty':
					if(!empty($value)) $invalid = false;
					break;
				case 'is_bool':
					if(is_bool($value)) $invalid = false;
					break;
				case 'is_string':
					if(is_string($value)) $invalid = false;
					break;
				default:
					throw new \RuntimeException('Invalid Filter');
					break;
			}
		}

		# Set filter (array)
		elseif(is_array($filter) && in_array($value, $filter)) {
			$invalid = false;
		}

		if($invalid) {
			throw new \InvalidArgumentException($message);
		}
	}
}
