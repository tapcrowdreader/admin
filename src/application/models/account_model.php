<?php
/**
 * Account model
 */
namespace Tapcrowd\Model;

/*
accountId	int(10) unsigned	NO	PRI		auto_increment
parentId	int(10) unsigned	NO	MUL
clientId	int(11)	NO
channelId	int(10) unsigned	NO	0
role		enum('admin','user')	NO	user
mdate		timestamp		NO	CURRENT_TIMESTAMP	on update CURRENT_TIMESTAMP
cdate		timestamp		NO	0000-00-00 00:00:00
fullname	varchar(64)		NO
email		varchar(255)		NO
*/

class Account extends AbstractPDOModel
{
	/**
	 * User Roles
	 * @var private
	 */
	private $user_roles = array(
		'admin' => array(
			'name' => 'admin',
			'desc' => 'Channel Administrator',
		),
		'user' => array(
			'name' => 'user',
			'desc' => 'Default user account',
		),
		'appadmin' => array(
			'name' => 'appadmin',
			'desc' => 'Can submit apps',
		),
		'editor' => array(
			'name' => 'editor',
			'desc' => 'Can edit content',
		),
	);

	/**
	 * Returns the account for the given identifier (if the current user has access to it)
	 *
	 * Normal users will get all subusers, Chansession_modelnel admins will get all accounts within their channel
	 * Tapcrowd admins will get ALL accounts
	 *
	 * @param int $accountId
	 * @return \Tapcrowd\Account
	 * @throws \InvalidArgumentException
	 * @throws \Tapcrowd\AccessViolation
	 */
	public function getAccount( $accountId )
	{
		if(!is_numeric($accountId)) {
			throw new \InvalidArgumentException('Invalid Account Identifier');
		}

		$sql = 'SELECT tc_accounts.*, tc_accounts.accountId AS id, tc_accounts.clientId AS organizerId, tc_logins.login AS login
			FROM tc_accounts LEFT OUTER JOIN tc_logins USING(accountId) WHERE accountId=%d LIMIT 1';
		$stat = $this->query($sql, $accountId);
		$stat->setFetchMode(\PDO::FETCH_CLASS, '\Tapcrowd\Account');
		$account = $stat->fetch();
		if($account === false) {
			$msg = $stat->errorInfo();
			throw new \InvalidArgumentException('Could not find Account; '.$msg[2]);
		}

		$ca = _currentUser();
		if(
			($account->accountId == $ca->accountId) ||
			($account->parentId == $ca->accountId) ||
			($ca->role == 'admin' && ($ca->channelId == 1 || $ca->channelId == $account->channelId))
		) {
			return $account;
		} else {
			throw new \Tapcrowd\AccessViolation();
		}
	}

	/**
	 * Set Account role
	 * @param int $accountId
	 * @param string $role
	 * @return void
	 */
	public function setAccountRole( $accountId, $role )
	{
		# Validate
		$account = $this->getAccount( $accountId );
		$user_roles = $this->getAccountRoles();

		if(!array_key_exists($role, $user_roles)) {
			throw new \InvalidArgumentException('Invalid User Role');
		}

		# Update
		$sql = 'UPDATE tc_accounts SET role=%s WHERE accountId=%d LIMIT 1';
		if( $this->execute($sql, $role, $account->accountId) === false ) {
			throw new \RuntimeException('Could not update account:' . $this->lastErrorMessage());
		}
	}

	/**
	 * Set Account status
	 * @param int $accountId
	 * @param string $status
	 * @return void
	 */
	public function setAccountStatus( $accountId, $status )
	{
		# Validate
		$account = $this->getAccount( $accountId );
		$user_statuses = $this->getAccountStatuses();
		if(!in_array($status, $user_statuses)) {
			throw new \InvalidArgumentException('Invalid User Status');
		}

		# Update
		$sql = 'UPDATE tc_accounts SET status=%s WHERE accountId=%d LIMIT 1';
		if( $this->execute($sql, $status, $account->accountId) === false ) {
			throw new \RuntimeException('Could not update account');
		}
	}

	/**
	 * Removes the given account
	 *
	 * @param int $accountId
	 * @return void
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 * @throws \Tapcrowd\AccessViolation
	 */
	public function removeAccount( $accountId )
	{
		# Make shure we can access this account
		$account = $this->getAccount( $accountId );

		$sql = 'UPDATE tc_accounts SET status="deleted" WHERE accountId=%d LIMIT 1';
		if( $this->execute($sql, $account->accountId) === false ) {
			throw new \RuntimeException('Could not remove account');
		}
	}

	/**
	 * Returns the accounts the given account can manage
	 *
	 * Normal users will get all subusers, Channel admins will get all accounts within their channel
	 * Tapcrowd admins will get ALL accounts
	 *
	 * @param int $accountId
	 * @return \Tapcrowd\Account[]
	 */
	public function getAccounts( $accountId )
	{
		# Make shure we can access this account
		$account = $this->getAccount( $accountId );

		$sql = 'SELECT *,tc_accounts.accountId AS id, channel.name AS channel
			FROM tc_accounts LEFT JOIN channel ON channel.id = tc_accounts.channelId
			WHERE accountId != %d AND status != "deleted"';
		if($account->role != 'admin') $sql.= ' AND parentId = %1$d';
		$sql.= ' AND channelId = %d ORDER BY tc_accounts.mdate DESC, tc_accounts.fullname ASC';

		$stat = $this->query($sql, $account->accountId, $account->channelId);
		$stat->setFetchMode(\PDO::FETCH_CLASS, '\Tapcrowd\Account');
		$data = $stat->fetchAll();
		return $data;
	}

	/**
	 * Returns the accounts within the given channel
	 *
	 * This method will not return system accounts.
	 *
	 * @param \Tapcrowd\Structs\Channel $channel
	 * @return \Tapcrowd\Account[]
	 */
	public function getAccountsForChannel( \Tapcrowd\Structs\Channel $channel )
	{
		# Make shure we can access this channel (current use must be channel or tacrowd admin)
		$account = \Tapcrowd\API::getCurrentUser();
		if($account->role != 'admin' || $account->channelId != 1 && $channel->id != $account->channelId) {
			throw new \Tapcrowd\AccessViolation();
		}

		$sql = 'SELECT tc_accounts.*,tc_accounts.accountId AS id, channel.name AS channel
			FROM tc_accounts INNER JOIN channel ON channel.id = tc_accounts.channelId
			WHERE channel.id = %d AND status != "deleted" AND tc_accounts.accountId NOT IN(1,2,3,4)
			ORDER BY tc_accounts.mdate DESC, tc_accounts.fullname ASC';
		$stat = $this->query($sql, $channel->id);
		$stat->setFetchMode(\PDO::FETCH_CLASS, '\Tapcrowd\Account');
		return $stat->fetchAll();
	}

	/**
	 * Returns the balance for the given account
	 *
	 * @param \Tapcrowd\Account $account
	 * @return \Tapcrowd\Balance
	 */
	public function getAccountBalance( \Tapcrowd\Account $account )
	{
		# Make shure we can access this account
		$account = $this->getAccount( $account->accountId );

		$balance = new \Tapcrowd\Balance();
		$balance->account = $account;

		# Add apps
		$sql = 'SELECT app.id AS appId, app.name, LOWER(subflavor.name) AS subflavor
			FROM app INNER JOIN subflavor ON app.subflavorid = subflavor.id
			WHERE accountId=1094 AND isdeleted = 0';
		$stat = $this->query($sql,$account->accountId);
		$balance->apps = $stat->fetchAll(\PDO::FETCH_OBJ);

		#
		# TODO : Add/Calc credits
		#
		$balance->credits = (object)array('basic' => 0,'plus' => 0,'pro' => 0);

		return $balance;
	}

	/**
	 * Updates the balance for the given account
	 *
	 * @param \Tapcrowd\Balance $balance
	 * @return true|false
	 */
	public function setAccountBalance( \Tapcrowd\Balance $balance )
	{
		# Make shure we can access this account
		$account = $this->getAccount( $balance->account->accountId );

		#
		# TODO : Update credits in the database
		#

		return true;
	}

	/**
	 * Returns the applications for the given account
	 *
	 * @param \Tapcrowd\Account $account
	 * @return \Tapcrowd\Application[]
	 * @throws \InvalidArgumentException
	 * @throws \Tapcrowd\AccessViolation
	 */
	public function getApplications( \Tapcrowd\Account $account )
	{
		# Make shure we can access this account
		$account = $this->getAccount( $account->accountId );

		# Get applications
		$sql = 'SELECT app.* FROM app
			WHERE app.isdeleted=0 AND (app.accountId = %d OR app.id IN(
			  SELECT entityId FROM tc_entityrights WHERE entity = "application" AND accountId = %1$d
			))';
		$stat = $this->query($sql, $account->accountId);
		$stat->setFetchMode(\PDO::FETCH_CLASS, '\Tapcrowd\Application');
		$data = $stat->fetchAll();
		return $data;
	}

	/**
	 * Returns the entity rights for the given account
	 *
	 * @param \Tapcrowd\Account $account
	 * @param string $entity (application)
	 * @return array with appId's
	 * @throws \InvalidArgumentException
	 * @throws \Tapcrowd\AccessViolation
	 */
	public function getEntityRights( \Tapcrowd\Account $account, $entity = 'application' )
	{
		# Make shure we can access this account
		$account = $this->getAccount( $account->accountId );

		if($entity !== 'application') {
			throw new \InvalidArgumentException('Invalid entity');
		}

		$sql = 'SELECT id FROM app WHERE accountId = %2$d UNION
			SELECT entityId FROM tc_entityrights WHERE entity=%s AND accountId = %2$d';
		$stat = $this->query($sql, $entity, $account->accountId);
		$stat->setFetchMode(\PDO::FETCH_NUM);
		$data = array(); while(($row = $stat->fetch()) !== false) $data[] = (int)$row[0];
		return $data;
	}

	/**
	 * Sets the entity rights for the given account
	 *
	 * @param \Tapcrowd\Account $account
	 * @param string $entity (application)
	 * @param array $entityIds entity ids
	 * @return void
	 * @throws \InvalidArgumentException
	 * @throws \Tapcrowd\AccessViolation
	 */
	public function setEntityRights( \Tapcrowd\Account $account, $entity, Array $entityIds)
	{
		# Make shure we can access this account
		$account = $this->getAccount( $account->accountId );
		$ca_rights = $this->getEntityRights( _currentUser() );

		if($entity !== 'application') {
			throw new \InvalidArgumentException('Invalid entity');
		}

		# Create insert / delete rights queries
		$values = array();
		foreach($entityIds as $appId) {
			if(!is_numeric($appId) || !in_array($appId, $ca_rights)) {
				throw new \Tapcrowd\AccessViolation('Application #'.$appId);
			}
			$values[] = "({$account->accountId},'$entity',$appId)";
		}
		$insert_sql = 'INSERT IGNORE INTO tc_entityrights VALUES' . implode(',',$values);
		$delete_sql = 'DELETE FROM tc_entityrights WHERE entity = %s AND accountId = %d AND entityId NOT IN(%s)';

		# Apply changes
		try {
			$this->startTransaction();
			$this->execute($delete_sql, $entity, $account->accountId, $entityIds);
			$this->execute($insert_sql);
			$this->commitTransaction();
		} catch(\Exception $e) {
			$this->rollbackTransaction();
		}
	}

	/**
	 * Returns Roles
	 * @return array
	 */
	public function getAccountRoles()
	{
		$roles = $this->_parseTableEnum('tc_accounts','role');
		$default_role = (object)array('name' => 'Unset', 'desc' => 'No description');
		return array_merge(array_fill_keys($roles, $default_role), $this->user_roles);
	}

	/**
	 * Returns Statuses
	 * @return array
	 */
	public function getAccountStatuses()
	{
		return $this->_parseTableEnum('tc_accounts','status');
	}

	/**
	 * Verify if the given account has the required permission
	 *
	 * @param string $permission
	 * @param \Tapcrowd\Account $account
	 * @return bool
	 */
	public function hasPermission( $permission, \Tapcrowd\Account $account )
	{
		# Roles: admin, user, appadmin, editor
		switch($permission) {
			case 'app.submit':
			case 'app.create':
			case 'app.manage':
				$r = in_array($account->role, array('admin','user','appadmin'));
				break;
			default:
				$r = false;
				break;
		}
		return $r;
	}

	/**
	 * Parse and return a list of enum entries
	 * @param string $tablename
	 * @param string $filedname
	 * @return array|false results
	 */
	private function _parseTableEnum( $tablename, $fieldname )
	{
		$stat = $this->query('SHOW COLUMNS IN '.$tablename.' WHERE Field="'.$fieldname.'"');
		$row = $stat->fetch();
		if(preg_match("/^enum\('(.*)'\)$/", $row['Type'], $entries) && !empty($entries)) {
			$entries = explode("','", $entries[1]);
			return $entries;
		}
		return false;
	}
}
