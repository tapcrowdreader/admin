<?php if(!defined('BASEPATH')) exit('No direct script access');

class Venue_model extends CI_Model {
	
	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Venue_model() {
		parent::__construct();
	}
	
	function add($name, $address, $latlon) {
		if(isset($latlon)) {
			$data = array(
				'name' => $name,
				'address' => $address,
				'lat' => $latlon->lat,
				'lon' => $latlon->lng
			);
		} else {
			$data = array(
				'name' => $name,
				'address' => $address
			);
		}
		$this->db->insert('venue', $data);
		return $this->db->insert_id();
	}
	
	function edit($venueid, $name, $address, $latlon) {
		if(isset($latlon)) {
			$data = array(
				'name' => $name,
				'address' => $address,
				'lat' => $latlon->lat,
				'lon' => $latlon->lng
			);
		} else {
			$data = array(
				'name' => $name,
				'address' => $address
			);
		}
		$this->db->where('id', $venueid);
		$this->db->update('venue', $data);
		return $venueid;
	}
	
	function remove($venueid) {
		$this->db->delete('venue', array('id' => $venueid));
	}
	
	function getById($id) {
		$res = $this->db->query("SELECT * FROM venue WHERE id='$id' AND deleted = 0 LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function allfromorganizer($id) {
		$res = $this->db->query("SELECT v.* FROM app a, appvenue av, venue v WHERE a.organizerid = '$id' AND av.appid = a.id AND v.id = av.venueid AND v.deleted = 0 ORDER BY v.id DESC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function isVenueApp($id) {
		$res = $this->db->query("SELECT a.* FROM app a WHERE a.id = '$id'");
		if($res->num_rows() == 0) return FALSE;
		if($res->row()->apptypeid == '2'){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function getAppVenue($appid) {
		$res = $this->db->query("SELECT v.* FROM appvenue av, venue v WHERE av.appid = $appid AND v.id = av.venueid AND v.deleted = 0 ORDER BY v.order ASC, v.id DESC");
		$countvenues = $this->db->query("SELECT id FROM appvenue WHERE appid = ?", array($appid));
		if($countvenues->num_rows() > 2000) {
			$res = $this->db->query("SELECT v.id, v.name, v.address, v.order, v.active FROM appvenue av, venue v WHERE av.appid = $appid AND v.id = av.venueid AND v.deleted = 0 ORDER BY v.order ASC, v.id DESC");
		}
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function setActive($id, $status) {
		$data = array( 'active' => $status );
		$this->db->where('id', $id);
		$this->db->update('venue', $data);
	}
	
	function countLauncherFromVenueModule($venueid, $moduletypeid) {
		$res = $this->db->query("SELECT l.* FROM launcher l WHERE l.venueid = '$venueid' AND l.moduletypeid = '$moduletypeid' ");
		if($res->num_rows() == 0) return FALSE;
		return $res->num_rows();
	}

	function insertRestoLaunchers($appid, $venueid) {
		$parentid = $this->db->query("SELECT id FROM `group` WHERE name = 'catalogcategories' AND venueid = $venueid LIMIT 1");
		$parentid = $parentid->row()->id;
		$menugroup = array(
				'appid' => $appid,
				'venueid'	=> $venueid,
				'name'		=> 'Menu',
				'parentid'	=> $parentid,
				'displaytype'	=> 'list'

			);
		$menugroupid = $this->general_model->insert('group',$menugroup);

		$menu = array(
			'moduletypeid'	=> 50,
			'venueid'		=> $venueid,
			'module'		=> 'Menu',
			'title'			=> "Menu",
			'icon'			=> 'l_menu',
			'order'			=> 3,
			'active'		=> 0,
			'displaytype'	=> 'list',
			'groupid'		=> $menugroupid
			);

		$this->general_model->insert('launcher',$menu);


		$galleryroup = array(
				'appid' => $appid,
				'venueid'	=> $venueid,
				'name'		=> 'Gallery',
				'parentid'	=> $parentid,
				'displaytype'	=> 'thumb'
			);
		$gallerygroupid = $this->general_model->insert('group',$galleryroup);

		$gallery = array(
			'moduletypeid'	=> 51,
			'venueid'		=> $venueid,
			'module'		=> 'Gallery',
			'title'			=> "Gallery",
			'icon'			=> 'l_pictures',
			'order'			=> 4,
			'active'		=> 0,
			'displaytype'	=> 'thumb',
			'groupid'		=> $gallerygroupid
			);

		$this->general_model->insert('launcher',$gallery);


		$teamgroup = array(
				'appid' => $appid,
				'venueid'	=> $venueid,
				'name'		=> 'Team',
				'parentid'	=> $parentid,
				'displaytype'	=> 'list'

			);
		$teamgroupid = $this->general_model->insert('group',$teamgroup);

		$team = array(
			'moduletypeid'	=> 52,
			'venueid'		=> $venueid,
			'module'		=> 'Team',
			'title'			=> "Team",
			'icon'			=> 'l_team',
			'order'			=> 4,
			'active'		=> 0,
			'displaytype'	=> 'list',
			'tag'			=> 'restoteam',
			'groupid'		=> $teamgroupid
			);

		$this->general_model->insert('launcher',$team);
	}

	function insertShopLaunchers($appid, $venueid) {
		// $parentid = $this->db->query("SELECT id FROM `group` WHERE name = 'catalogcategories' AND venueid = $venueid LIMIT 1");
		$parentid = 0;
		$galleryroup = array(
				'appid' => $appid,
				'venueid'	=> $venueid,
				'name'		=> 'Pictures',
				'parentid'	=> $parentid,
				'displaytype'	=> 'thumb'
			);
		$gallerygroupid = $this->general_model->insert('group',$galleryroup);

		$gallery = array(
			'moduletypeid'	=> 51,
			'venueid'		=> $venueid,
			'module'		=> 'Gallery',
			'title'			=> "Pictures",
			'icon'			=> 'l_pictures',
			'order'			=> 4,
			'active'		=> 0,
			'displaytype'	=> 'thumb',
			'groupid'		=> $gallerygroupid
			);

		$this->general_model->insert('launcher',$gallery);

		$parentid = $this->db->query("SELECT id FROM `group` WHERE name = 'catalogcategories' AND venueid = $venueid LIMIT 1")->row()->id;
		$cataloggroup = array(
				'appid' => $appid,
				'venueid'	=> $venueid,
				'name'		=> 'Catalog',
				'parentid'	=> $parentid
			);
		$cataloggroupid = $this->general_model->insert('group',$cataloggroup);

		$catalog = array(
			'moduletypeid'	=> 15,
			'venueid'		=> $venueid,
			'module'		=> 'catalog',
			'title'			=> "Catalog",
			'icon'			=> 'l_catalog',
			'displaytype'	=> 'list',
			'order'			=> 3,
			'active'		=> 0,
			'groupid'		=> $cataloggroupid
			);

		$this->general_model->insert('launcher',$catalog);
	}

	function insertBusinessLaunchers($appid, $venueid) {
		$parentid = $this->db->query("SELECT id FROM `group` WHERE name = 'catalogcategories' AND venueid = $venueid LIMIT 1")->row()->id;
		$cataloggroup = array(
				'appid' => $appid,
				'venueid'	=> $venueid,
				'name'		=> 'Catalog',
				'parentid'	=> $parentid
			);
		$cataloggroupid = $this->general_model->insert('group',$cataloggroup);

		$catalog = array(
			'moduletypeid'	=> 15,
			'venueid'		=> $venueid,
			'module'		=> 'catalog',
			'title'			=> "Catalog",
			'icon'			=> 'l_catalog',
			'displaytype'	=> 'list',
			'order'			=> 3,
			'active'		=> 1,
			'groupid'		=> $cataloggroupid
			);

		$this->general_model->insert('launcher',$catalog);
	}

	function changelauncher($venueid, $oldlauncherid, $newlauncherid, $appid) {
		$oldlauncher = $this->module_mdl->getLauncherById($oldlauncherid);
		$newlauncher = $this->module_mdl->getLauncherById($newlauncherid);

		if(!empty($appid) && !empty($oldlauncher->tag) && !empty($venueid)) {
			$this->db->query("DELETE FROM tag WHERE appid = $appid AND tag = '$oldlauncher->tag' AND venueid = $venueid");
			$this->general_model->insert('tag', array(
					'appid' => $appid,
					'tag' => $newlauncher->tag,
					'venueid' => $venueid
				));
		}


		return true;
	}


	function getVenuesByTag($appid, $tag) {
		$res = $this->db->query("SELECT vnu.*, group_concat(t.tag) as tags
			FROM venue vnu
			LEFT JOIN appvenue av ON (vnu.id = av.venueid)
			INNER JOIN tag t ON t.venueid = av.venueid
			WHERE av.appid = ? AND vnu.deleted = 0 AND t.tag = ?
			GROUP BY vnu.id
			ORDER BY vnu.name ASC, vnu.id DESC
			", array($appid, $tag));
		$countvenues = $this->db->query("SELECT id FROM appvenue WHERE appid = ?", array($appid));
		if($countvenues->num_rows() > 2000) {
			$res = $this->db->query("SELECT vnu.id, vnu.name, vnu.address, vnu.active, vnu.lat, vnu.lon, group_concat(t.tag) as tags, vnu.order
			FROM venue vnu
			LEFT JOIN appvenue av ON (vnu.id = av.venueid)
			INNER JOIN tag t ON t.venueid = av.venueid
			WHERE av.appid = ? AND vnu.deleted = 0 AND t.tag = ?
			GROUP BY vnu.id
			ORDER BY vnu.name ASC, vnu.id DESC
			", array($appid, $tag));
		}

		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
/*
	
	function all() {
		$res = $this->db->get('venue');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function edit($id, $name, $address, $latlon) {
		if(isset($latlon)) {
			$data = array(
					'id' => $id,
					'name' => $name,
					'address' => $address,
					'lat' => $latlon->lat,
					'lon' => $latlon->lng
				);
		} else {
			$data = array(
				'id' => $id,
				'name' => $name,
				'address' => $address
			);
		}
		$this->db->where('id', $id);
		$this->db->update('venue', $data);
	}
	
	function remove($id) {
		//Delete this venue
		$this->db->delete('venue', array('id' => $id));
	}
	
	function updateTimestamp($eventid) {
		$CI =& get_instance();
		$CI->load->model('event_model');
		$event = $CI->event_model;
		$event->updateTimeStamp($eventid);
	}
*/
}
?>
