<?php if(!defined('BASEPATH')) exit('No direct script access');

class Catalog_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Catalog_model() {
		parent::__construct();
	}
	function getCatalogByID($catid) {
		$this->db->where('id', $catid);
		$res = $this->db->get('catalog');
		if($res->num_rows() == 0) return FALSE;
		$catalog = $res->row();
		
		// get brands
		$brands = $this->db->query("SELECT eb.id FROM catebrand b, catalogbrand eb WHERE b.catalogid = $catid AND eb.id = b.catalogbrandid");
		$brandids = array();
		foreach ($brands->result() as $bitem) {
			array_push($brandids, $bitem->id);
		}
		$catalog->brands = $brandids;
		
		// get categories
		$categories = $this->db->query("SELECT ec.id FROM catecat c, catalogcategory ec WHERE c.catalogid = $catid AND ec.id = c.catalogcategoryid");
		$catids = array();
		foreach ($categories->result() as $citem) {
			array_push($catids, $citem->id);
		}
		$catalog->categories = $catids;
		
		return $catalog;
	}
    
	function getcatalogsByEventID($eventid) {
		$this->db->where('eventid', $eventid);
		$this->db->order_by('name', "asc");
		$res = $this->db->get('catalog');
		if($res->num_rows() == 0) return FALSE;
		foreach ($res->result() as $row) {
			$row->name = htmlspecialchars_decode($row->name, ENT_NOQUOTES);
		}
		return $res->result();
	}
    
	function getCatalogsByVenueID($venueid, $catalogtype = '') {
		$this->db->where('venueid', $venueid);
		//for services/projects
		$this->db->where('type', $catalogtype);
		$this->db->order_by('order', "desc");
		$this->db->order_by('name', "asc");
		$res = $this->db->get('catalog');
		if($res->num_rows() == 0) return FALSE;
		foreach ($res->result() as $row) {
			$row->name = htmlspecialchars_decode($row->name, ENT_NOQUOTES);
		}
		return $res->result();
	}
	
	function getBrandsByEventID($eventid) {
		$this->db->where('eventid', $eventid);
		$this->db->order_by('name', 'ASC');
		$res = $this->db->get('catalogbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCategoriesByEventID($eventid) {
		$this->db->where('eventid', $eventid);
		$this->db->order_by('name', 'ASC');
		$res = $this->db->get('catalogcategory');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getBrandsByVenueID($venueid) {
		$this->db->where('venueid', $venueid);
		$this->db->order_by('name', 'ASC');
		$res = $this->db->get('catalogbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCategoriesByVenueID($venueid) {
		$this->db->where('venueid', $venueid);
		$this->db->order_by('name', 'ASC');
		$res = $this->db->get('catalogcategory');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
    
	function saveBrands($catalogid, $brands) {
		if($this->db->delete('catebrand', array('catalogid' => $catalogid))){
			if(is_array($brands) && count($brands) > 0){
				foreach($brands as $brand) {
					$this->db->insert('catebrand', array('catalogid' => $catalogid, 'catalogbrandid' => $brand));
				}
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function saveCategories($catalogid, $categories) {
		if($this->db->delete('catecat', array('catalogid' => $catalogid))){
			if(is_array($categories) && count($categories) > 0){
				foreach($categories as $category) {
					$this->db->insert('catecat', array('catalogid' => $catalogid, 'catalogcategoryid' => $category));
				}
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}
    
	function getCataloggroupsByEventID($eventid) {
		$res = $this->db->query("SELECT * FROM cataloggroup WHERE eventid = $eventid ORDER BY cataloggroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCataloggroupsByVenueID($venueid) {
		$res = $this->db->query("SELECT * FROM cataloggroup WHERE venueid = $venueid ORDER BY cataloggroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCatalogsByGroups($groupid, $catalogtype = '') {
		$res = $this->db->query("SELECT * FROM catalog WHERE cataloggroupid = $groupid AND type = '$catalogtype' ORDER BY catalog.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	function getCataloggroupByID($id) {
		$res = $this->db->query("SELECT * FROM cataloggroup WHERE id = $id ORDER BY cataloggroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	function edit_cataloggroup($id, $data) {
		$this->db->where('id', $id);
		if($this->db->update('cataloggroup', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function getCatalogsBySourceId($sourceid) {
		$res = $this->db->query("SELECT * FROM catalog WHERE sourceid = $sourceid");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
    
    function getRssOfEvent($eventid) {
		$res = $this->db->query("SELECT * FROM catalogsource WHERE eventid = $eventid AND type = 'rss'");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
    }
    
    function getRssOfVenue($venueid) {
		$res = $this->db->query("SELECT * FROM catalogsource WHERE venueid = $venueid AND type = 'rss'");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
    }
    
    function getSourceOfId($id) {
		$res = $this->db->query("SELECT * FROM catalogsource WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
    }
    
    function removeCatalogsOfSourceId($sourceid) {
        $catalogs = $this->getCatalogsBySourceId($sourceid);   
        foreach($catalogs as $catalog) {
            //remove translation
            $this->language_model->removeTranslations('catalog', $catalog->id);
        }
        $this->db->where('sourceid', $sourceid);
        $this->db->delete('catalog'); 
    }

    function getMainCategorieGroup($venueid) {
		$res = $this->db->query("SELECT * FROM `group` WHERE venueid = $venueid AND name = 'catalogcategories' LIMIT 1 ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row()->id;
    }

    function getCatalogGroup($venueid) {
		$res = $this->db->query("SELECT * FROM `group` WHERE venueid = $venueid AND name = 'Catalog' LIMIT 1 ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row()->id;
    }

  	//for new system of brands and catalogs in groups with groupitems
    var $htmlarray = array();
    var $groupids = '';
	// function display_children($parent, $level, $venueid, $catalogid = 0) {  
	//     // retrieve all children of $parent <br>  
	//     $result = mysql_query('SELECT * FROM `group` WHERE parentid = '.$parent.' ORDER BY id DESC;');  

	//     // display each child <br>  
	//     while ($row = mysql_fetch_array($result)) {  
	//     	// indent and display the title of this child <br>

	//     	$html = '<option name="brands[]" value="'.$row['id'].'">&nbsp' . str_repeat('--',$level).'&nbsp'.$row['name']."\n".'</option>';  
	//     	if($catalogid != 0) {
	//     		$id = $row['id'];
	//     		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $id AND itemid = $catalogid AND itemtable = 'catalog' ");
	//     		if($res->num_rows() != 0) {
	//     			$this->groupids .= $row['id'] . '/';
	//     			$html = '<option name="brands[]" selected="checked" value="'.$row['id'].'">&nbsp' . str_repeat('--',$level).'&nbsp'.$row['name']."\n".'</option>';  
	//     		}
	//     	}
	          
	        
	//         $this->htmlarray[] = $html;
	//         // call this function again to display this <br>  
	//         // child's children <br>  
	//         $this->display_children($row['id'], $level+1, $venueid, $catalogid); 
	        
	//     } 

	//     return $this->htmlarray;
	// } 

	function display_children($parent, $level, $venueid, $catalogid = 0) {  
	    // retrieve all children of $parent <br>  
	    $result = $this->db->query('SELECT * FROM `group` WHERE parentid = '.$parent.' ORDER BY id DESC');  

	    // display each child <br>  
	    if($level == 0 && !empty($parent)) {
		    $parentname = $this->db->query("SELECT name FROM `group` WHERE id = $parent LIMIT 1");
		    if($parentname->num_rows() > 0) {
		    	$parentname = $parentname->row()->name;
			    $html = '<input type="checkbox" class="checkbox" name="groups[]" value="'.$parent.'" />&nbsp'.$parentname."<br />";
		    	if($catalogid != 0) {
		    		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $parent AND itemid = $catalogid AND itemtable = 'catalog' ");
		    		if($res->num_rows() != 0) {
		    			$html = '<input type="checkbox" class="checkbox" name="groups[]" value="'.$parent.'" checked>&nbsp'.$parentname."<br />";  
		    		}
		    	}
			    $this->htmlarray[] = $html;
			    $level++;
		    }
	    }
	    foreach($result->result() as $row) {  
	    	// indent and display the title of this child <br>
	    	$this->groupids .= $row->id . '/';
	    	$html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).'<input type="checkbox" class="checkbox" name="groups[]" value="'.$row->id.'" />&nbsp'.$row->name."<br />";  
	    	if($catalogid != 0) {
	    		$id = $row->id;
	    		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $id AND itemid = $catalogid AND itemtable = 'catalog' ");
	    		if($res->num_rows() != 0) {
	    			$html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).'<input type="checkbox" class="checkbox" name="groups[]" value="'.$row->id.'" checked>&nbsp'.$row->name."<br />";  
	    		}
	    	}
	          
	        
	        $this->htmlarray[] = $html;
	        // call this function again to display this <br>  
	        // child's children <br>  
	        $this->display_children($row->id, $level+1, $venueid, $catalogid); 
	        
	    } 

	    return $this->htmlarray;
	} 

	function deleteGroupItemsFromCatalog($id) {
		$this->db->where('itemtable', 'catalog');
		$this->db->where('itemid', $id);
		$this->db->delete('groupitem'); 
	}

	function removePremium($id) {
		if(is_numeric($id)) {
			$this->db->query("DELETE FROM premium WHERE tablename = 'catalog' AND tableId  = $id");
		}
	}

	function getPremiumCatalogsByVenueID($venueid) {
		$res = $this->db->query("SELECT c.* FROM catalog c INNER JOIN premium p ON p.tableId = c.id WHERE c.venueid = $venueid AND p.tablename = 'catalog' ORDER BY p.sortorder");
		return $res->result();
	}
}