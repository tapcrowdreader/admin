<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Postpicture_model extends CI_Model {

	//php 5 constructor
	private $con;
	function __construct() {
		parent::__construct();
		$this->con = mysqli_connect("live001.tapcrowd.com", "tcAdminDB", "2012?tapcrowdadmin", "postpicture");
		if (!$this->con)
		  {
		  die('Could not connect: ' . mysqli_error());
		  }
		mysqli_set_charset($this->con, "utf8");
	}
	
	function getByAppId($appid) {
		$pictures = array();
		$picturesQuery = mysqli_query($this->con, "SELECT * FROM picture WHERE appid = $appid");
		if($picturesQuery != false) {
			while($picture = mysqli_fetch_assoc($picturesQuery)) {
				$picture = (object)$picture;
				$picture->imageurl = 'http://tools.tapcrowd.com/PostPicture/upload/'.$appid.'/'.$picture->imageurl;
				$pictures[] = $picture;
			}
		}
		return $pictures;
	}

	function get($id) {
		$picturesQuery = mysqli_query($this->con, "SELECT * FROM picture WHERE id = $id LIMIT 1");
		if($picturesQuery != false) {
			while($picture = mysqli_fetch_assoc($picturesQuery)) {
				$picture = (object)$picture;
				$picture->imageurl = 'http://tools.tapcrowd.com/PostPicture/upload/'.$picture->appid.'/'.$picture->imageurl;
				return $picture;
			}
		}
		return false;
	}

	function delete($picture) {
		if($picture->id != null && is_numeric($picture->id)) {
			$picturesQuery = mysqli_query($this->con, "DELETE FROM picture WHERE id = $picture->id");
			if($picturesQuery != false) {
				$file = "/var/www/tools.tapcrowd.com/PostPicture/upload/".$picture->appid.'/'.substr($picture->imageurl);
				if(file_exists($file)) {
					unlink($file);
				}
				return true;
			}
		}
		return false;
	}

	function deleteMany($selectedids) {
		if(!empty($selectedids)) {
			$selectedids = implode(',', $selectedids);
			$query = mysqli_query($this->con, "SELECT * FROM picture WHERE id IN ($selectedids)");
			if($query != false) {
				while($picture = mysqli_fetch_assoc($query)) {
					$picture = (object)$picture;
					$file = "/var/www/tools.tapcrowd.com/PostPicture/upload/".$picture->appid.'/'.substr($picture->imageurl);
					if(file_exists($file)) {
						unlink($file);
					}
				}
			}

			$picturesQuery = mysqli_query($this->con, "DELETE FROM picture WHERE id IN ($selectedids)");
		}
		return true;
	}
}
