<?php
/**
 * Tapcrowd Session Model
 * @author Tom Van de Putte
 */
namespace Tapcrowd\Model;

if(!defined('BASEPATH')) exit('No direct script access');

/**
 * Basic PDO Implementation
 */
abstract class AbstractPDOModel
{
	private static $_pdo;

	private $_statement;

	public $__DRYRUN__ = false;

	/**
	 * Singleton Implementation
	 */
	public static function getInstance()
	{
		static $instance;
		if(!isset($instance)) {
			$class = get_called_class();
			$instance = new $class;
		}
		return $instance;
	}

	/**
	 * Constructor
	 */
	protected function __construct()
	{
		# Create PDO Connection
		if(!isset($this->_pdo)) {
// 			$db = get_instance()->db;

			include dirname(__DIR__) . '/config/database.php';
			$db = (object)$db['default'];

			$dsn = 'mysql:dbname='.$db->database.';host='.$db->hostname;
			$driver_opts = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"');
			$this->_pdo = new \PDO($dsn, $db->username, $db->password, $driver_opts);
			if(!$this->_pdo instanceOf \PDO) {
				throw new \Exception('Unable to create PDO object for ' . __CLASS__);
			}

			# PDO Options
			$this->_pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
		}
	}

	/**
	 * lastInsertId
	 *
	 * @param int|false
	 */
	final protected function lastErrorMessage()
	{
		if($this->_statement instanceOf \PDOStatement) {
			$info = $this->_statement->errorInfo();
		} else {
			$info = $this->_pdo->errorInfo();
		}
		if($info[1] === null) return null;
		else return sprintf("SQLSTATE [%s|%s] %s", $info[0], $info[1], $info[2]);
	}

	/**
	 * Return the last insert id
	 *
	 * @return array
	 */
	final protected function lastInsertId()
	{
		return $this->_pdo->lastInsertId();
	}

	/**
	 * Starts a new transaction
	 *
	 * Remember that some statements cause implicit commits:
	 * @see http://dev.mysql.com/doc/refman/5.0/en/implicit-commit.html
	 *
	 * @return void
	 */
	final protected function startTransaction()
	{
		# Make shure PDO throws exceptions when things go wrong
		$this->_pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$this->_pdo->beginTransaction();
	}

	/**
	 * Commits the current transaction
	 *
	 * @return void
	 */
	final protected function commitTransaction()
	{
		$this->_pdo->commit();
	}

	/**
	 * Undo changes for current transaction
	 *
	 * @return void
	 */
	final protected function rollbackTransaction()
	{
		$this->_pdo->rollBack();
	}

	/**
	 * Internal Query method
	 * @param string $query The query
	 * @param mixed $param1 Optional extra parameters
	 * @return \PDOStatement
	 */
	final protected function query( $query, $param1 = null )
	{
		if(func_num_args() > 1) {
			$args = func_get_args();
			foreach($args as $k => &$v) {
// 				if($k && !is_numeric($v)) $v = $this->_pdo->quote((string)$v);
				if($k === 0) continue;
				elseif(is_array($v)) {
					$v = array_values($v); $c = count($v);
					while($c-- && $a =& $v[$c]) $a = is_numeric($a)? (int)$a : $this->_pdo->quote($a);
					$v = implode(',',$v);
				}
				elseif(!is_numeric($v)) $v = $this->_pdo->quote((string)$v);
			}
			$query = call_user_func_array('sprintf', $args);
		}

		if($this->__DRYRUN__) { var_dump($query); return new \PDOStatement; }
// var_dump($query); //exit;

		$stat =& $this->_statement;
		$stat = $this->_pdo->query($query);
		if(!$stat instanceOf \PDOStatement) {
			$i = $this->_pdo->errorInfo();
			throw new \RuntimeException("[$i[0]|$i[1]]: $i[2]");
		}
		return $stat;
	}

	/**
	 * Internal Execute method
	 * @param string $query The query
	 * @param mixed $param1 Optional extra parameters
	 * @return boolean
	 */
	final protected function execute( $query, $param1 = null )
	{
		if(func_num_args() > 1) {
			$_query = $query;
			$args = func_get_args();
			foreach($args as $k => &$v) {
				if($k === 0) continue;
				elseif(is_array($v)) {
					$v = array_values($v); $c = count($v);
					while($c-- && $a =& $v[$c]) $a = is_numeric($a)? (int)$a : $this->_pdo->quote($a);
					$v = implode(',',$v);
				}
				elseif(!is_numeric($v)) $v = $this->_pdo->quote((string)$v);
			}

			// Uncatchable errors
			try { $query = call_user_func_array('sprintf', $args); } catch(\Exception $e) {}
			if($query === false) {
				throw new \RuntimeException(__('Invalid query syntax "%s"',$_query));
			}
		}

		if($this->__DRYRUN__) { var_dump($query); return 1; }
// var_dump($query); //exit;

		try {
			$r = $this->_pdo->exec($query);
			if($r === false) $error = $this->_pdo->errorInfo();
		} catch(\PDOException $e) {
			$error = $e->errorInfo;
		}

		if(!empty($error)) {
			throw new \RuntimeException("[$error[0]|$error[1]]: $error[2]");
		} else {
			return $r;
		}
	}
}
