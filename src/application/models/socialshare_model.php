<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Socialshare_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}

	function getActiveNoteModules($app, $type, $typeid) {
		$typefield = $type.'id';
		$res = $this->db->query("SELECT s.launcherid, l.title FROM socialshare s INNER JOIN launcher l ON l.id = s.launcherid WHERE s.appid = $app->id AND s.".$typefield." = $typeid");
		if($res->num_rows > 0) {
			return $res->result();
		}

		return array();
	}

	function saveSocialshareModules($app, $post, $type, $typeid) {
		$typefield = $type.'id';
		$this->db->query("DELETE FROM socialshare WHERE appid = $app->id AND $typefield = $typeid");
		foreach($post as $post) {
			$data = array(
				'appid' => $app->id,
				'launcherid' => $post
				);

			$data[$typefield] = $typeid;
			$this->general_model->insert('socialshare', $data);
		}
	}
	
}