<?php if(!defined('BASEPATH')) exit('No direct script access');

class Group_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}

	function getById($id) {
		$res = $this->db->query("SELECT * FROM `group` WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) {
			return false;
		}
		return $res->row();
	}

	function getGroupsByEvent($eventid) {
		$res = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid");
		if($res->num_rows() == 0) {
			return array();
		}
		return $res->result();
	}

	function getChildren($id) {
		$res = $this->db->query("SELECT * FROM `group` WHERE parentid = $id");
		if($res->num_rows() == 0) {
			return array();
		}
		return $res->result();
	}

	function getParents($id) {
		$current = $this->db->query("SELECT * FROM `group` WHERE id = $id LIMIT 1");
		if($current->num_rows() == 0) {
			return array();
		} else {
			$current = $current->row();
		}
		$parents = array();
		for($i = 0;$i < 100;$i++) {
			$res = $this->db->query("SELECT * FROM `group` WHERE id = $current->parentid LIMIT 1 ");
			if($res->num_rows() != 0) {
				
				$current = $res->row();
				array_unshift($parents,$current);
			} else {
				$i = 100;
				return $parents;
			}
		}

		return $parents;
	}

	function getItemsOfGroup($id) {
	// $res = $this->db->query("SELECT groupitem.*, groupitem.itemtable into @tmp FROM `groupitem` INNER JOIN @tmp ON @tmp.id = groupitem.itemid WHERE groupitem.groupid = $id");
		$res = $this->db->query("SELECT * FROM `groupitem` WHERE groupid = $id");
		if($res->num_rows() == 0) {
			return array();
		}
		$groupitems = array();
		foreach($res->result() as $groupitem) {
			if(strtolower($groupitem->itemtable) == 'catalog') {
				$groupitem->itemtable = 'catalog';
			}
			$object = $this->db->query("SELECT * FROM $groupitem->itemtable WHERE id = $groupitem->itemid LIMIT 1");
			if($object->num_rows() != 0) {
				$object = $object->row();
				//koppel titel enzo
				if($object->title != null) {
					$groupitem->title = $object->title;
				} elseif($object->firstname != null) {
					$groupitem->title = $object->name . ' ' . $object->firstname;
				} elseif($object->name != null) {
					$groupitem->title = $object->name;
				}

				if(isset($object->imageurl) && $object->imageurl != null) {
					$groupitem->imageurl = $object->imageurl;
				} elseif(isset($object->image) && $object->image != null) {
					$groupitem->imageurl = $object->image;
				}

				if(isset($groupitem->description) && $groupitem->description != null) {
					$groupitem->subtitle = $object->description;
				}

				if(isset($object->order) && $object->order != null) {
					$groupitem->order = $object->order;
				}

				if(isset($object->type)) {
					$groupitem->type = $object->type;
				}

				if($groupitem->itemtable == 'tc_places') {
					$groupitem->type = 'places';
				}

				$groupitem->groupitem = true;
				
				$groupitems[] = $groupitem;
			}
		}
		return $groupitems;
	}

	function removeGroupsOfEvent($eventid) {
		$this->db->where('eventid', $eventid);
		$this->db->delete('group'); 
	}

	// Comparison function
	static function cmp($a, $b) {
		if(!isset($b->order)) {
			return 1;
		}
		if($b->order == 0) {
			return 1;
		}
		if(isset($a->order) && isset($b->order)) {
			$a->order = (int) $a->order;
			$b->order = (int) $b->order;
			if ($a->order == $b->order) {
				return 1;
			}
			return ($a->order < $b->order) ? -1 : 1;
		} else {
			return 1;
		}

	}

	function sortChildren($array) {
		uasort($array,array($this, 'cmp'));
		
		return $array;
	}

	function getMainCategorieGroup($parentId, $parentType, $controller) {
		$parentType2 = $parentType.'id';
		if($controller == 'places') {
			$res = $this->db->query("SELECT * FROM `group` WHERE name = 'placescategories' AND $parentType2 = $parentId LIMIT 1");
			if($res->num_rows() > 0) {
				return $res->row()->id;
			}
		}

		return false;
	}

    var $htmlarray = array();
    var $groupids = '';
	function display_children($parent, $level, $parentId, $objectid = 0, $parentType = '', $objecttype = '') {  
	    // retrieve all children of $parent <br>  
	    $result = $this->db->query('SELECT * FROM `group` WHERE parentid = '.$parent.' ORDER BY id DESC;');  

	    // display each child <br>  
	    if($level == 0 && !empty($parent)) {
		    $parentname = $this->db->query("SELECT name FROM `group` WHERE id = $parent LIMIT 1");
		    if($parentname->num_rows() > 0) {
		    	$parentname = $parentname->row()->name;
			    $html = '<input type="checkbox" class="checkbox" name="groups[]" value="'.$parent.'" />&nbsp'.$parentname."<br />";
		    	if($objectid != 0) {
		    		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $parent AND itemid = $objectid AND itemtable = '$objecttype' ");
		    		if($res->num_rows() != 0) {
		    			$html = '<input type="checkbox" class="checkbox" name="groups[]" value="'.$parent.'" checked>&nbsp;'.$parentname."<br />";  
		    		}
		    	}
			    $this->htmlarray[] = $html;
			    $level++;
		    }
	    }
	    foreach($result->result() as $row) {  
	    	// indent and display the title of this child <br>
	    	$this->groupids .= $row->id . '/';
	    	$html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).'<input type="checkbox" class="checkbox" name="groups[]" value="'.$row->id.'" />&nbsp;'.$row->name."<br />";  
	    	if($objectid != 0) {
	    		$id = $row->id;
	    		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $id AND itemid = $objectid AND itemtable = '$objecttype' ");
	    		if($res->num_rows() != 0) {
	    			$html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).'<input type="checkbox" class="checkbox" name="groups[]" value="'.$row->id.'" checked>&nbsp;'.$row->name."<br />";  
	    		}
	    	}
	          
	        
	        $this->htmlarray[] = $html;
	        // call this function again to display this <br>  
	        // child's children <br>  
	        $this->display_children($row->id, $level+1, $parentId, $objectid, $parentType, $objecttype); 
	        
	    } 

	    return $this->htmlarray;
	}

	function deleteGroupItemsFromObject($id, $table) {
		$this->db->where('itemtable', $table);
		$this->db->where('itemid', $id);
		$this->db->delete('groupitem'); 
	}

	function getByName($name, $type, $typeid) {
		$type = $type.'id';
		$res = $this->db->query("SELECT * FROM `group` WHERE name = ? AND $type = ? LIMIT 1", array($name, $typeid));
		if($res->num_rows() == 0) {
			return false;
		}
		return $res->row();
	}
}