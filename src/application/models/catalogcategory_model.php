<?php if(!defined('BASEPATH')) exit('No direct script access');

class Catalogcategory_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Catalogategory_model() {
		parent::__construct();
	}
	
	function getCatalogCategoriesByEventID($eventid) {
		$this->db->where('eventid', $eventid);
		$res = $this->db->get('catalogcategory');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCatalogCategoriesByVenueID($venueid) {
		$this->db->where('venueid', $venueid);
		$res = $this->db->get('catalogcategory');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCatalogCategoryByID($categoryid) {
		$this->db->where('id', $categoryid);
		$res = $this->db->get('catalogcategory');
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	// ### Webservice
	
	function getCatalogCategoriesByEventIDService ($eventid) {
		$res = $this->db->query("SELECT * FROM catalogcategory WHERE eventid='$eventid' ORDER BY name asc");
		if($res->num_rows() == 0) return FALSE;
		$data = array();
		foreach($res->result() as $row) {
			$data += array($row->id => $row);
		}
		return $data;
	}
	
	function getCatalogCategoriesByVenueIDService ($venueid) {
		$res = $this->db->query("SELECT * FROM catalogcategory WHERE venueid='$venueid' ORDER BY name asc");
		if($res->num_rows() == 0) return FALSE;
		$data = array();
		foreach($res->result() as $row) {
			$data += array($row->id => $row);
		}
		return $data;
	}
	
	/*function import($id, $eventid, $name) {
		if($this->getbyid($id) == null) {
			$data = array(
				'id' => $id,
				'name' => $name,
				'eventid' => $eventid
			);
			
			$this->db->insert('exhibitorcategory', $data);
			return $id;
		} else {
			$data = array(
				'name' => $name,
				'eventid' => $eventid
			);
			
			$this->db->insert('exhibitorcategory', $data);
			return $this->db->insert_id();
		}
	}*/
	
}