<?php if(!defined('BASEPATH')) exit('No direct script access');

class User_mdl extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function User_mdl() {
		parent::__construct();
	}
	
	function getUserByLogin($login) {
		$this->db->where('login', $login);
		$res = $this->db->get('organizer');
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function edit_user($id, $data) {
		$this->db->where('id', $id);
		if($this->db->update('organizer', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function check_user($email) {
		$this->db->where('email', $email);
		$res = $this->db->get('organizer');
		if($res->num_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function check_useremail($login, $email) {
		$this->db->where('email', $email);
		$this->db->where('login !=', $login);
		$this->db->limit(1);
		$res = $this->db->get('organizer');
		if($res->num_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function reset_password($email, $data) {
		$this->db->where('email', $email);
		if($this->db->update('organizer', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function user_availability($username) {
		$this->db->where('login', $username);
		$res = $this->db->get('organizer');
		if($res->num_rows() == 0) return TRUE;
		return FALSE;
	}
	
	function email_availability($email) {
		if(substr($email,-12) == 'tapcrowd.com' || substr($email,-14) == 'mobilejuice.be' ) {
			return true;
		}
		$this->db->where('email', $email);
		$res = $this->db->get('organizer');
		if($res->num_rows() == 0) return TRUE;
		return FALSE;
	}
	
	function activateUser($id) {
		$this->db->where('md5(id)', $id);
		if($this->db->update('organizer', array('activation' => 'active'))){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function getOrganizerById($id) {
		$this->db->where('id', $id);
		$this->db->limit(1);
		$res = $this->db->get('organizer');
		return $res->row();
	}

}