<?php
namespace Tapcrowd\models;

/**
 * GeoLocationService Model
*/
class GeoLocationService implements \Tapcrowd\interfaces\iGeoLocationService
{
	public function getCoordinatesForLocations( Array &$locations, $addressCol, $latCol, $lngCol, $swap = 20 )
	{
		$bing_geo_url = 'http://dev.virtualearth.net/REST/v1/Locations/';
		$bing_key = 'key=AghJ88MGlKeFIPFVSM-ypDbYVffkWbq2Yg3dU32DiZFkybz5vbHxzM41fYcCVRGV';
		
		$google_geo_url = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=';		
		
		$options = array(
			CURLOPT_RETURNTRANSFER	=> TRUE,
			CURLOPT_HEADER			=> 0,
			CURLOPT_ENCODING		=> '',
			CURLOPT_CONNECTTIMEOUT	=> 10,
			CURLOPT_TIMEOUT			=> 10
		);
		
		$ch = curl_init();
				
		$counter = 0;
		$swapOn = $swap * 2; // 4
		$total = count( $locations );
		$checkGoogle = TRUE;
		
		for( $i=1; $i<$total; $i++ ){
			if( $counter > $swap ){
				$checkGoogle = FALSE;
				$addr = str_ireplace( " ", "%20", $locations[$i][$addressCol] );
				$options[CURLOPT_URL] = $bing_geo_url . $addr . '?' . $bing_key;
				curl_setopt_array( $ch, $options );	
				$response = curl_exec( $ch );
				$response = json_decode($response);
				
				$locations[$i][$latCol] = $response->resourceSets[0]->resources[0]->point->coordinates[0];
				$locations[$i][$lngCol] = $response->resourceSets[0]->resources[0]->point->coordinates[1];
				
				if( !$locations[$i][$latCol] && !$locations[$i][$lngCol] ) $checkGoogle = TRUE;
			}
			if( $checkGoogle ){
				$options[CURLOPT_URL] = $google_geo_url .  urlencode( $locations[$i][$addressCol] );
				curl_setopt_array( $ch, $options );	
				$response = curl_exec( $ch );
				$response = json_decode($response);
				
				$locations[$i][$latCol] = $response->results[0]->geometry->location->lat;
				$locations[$i][$lngCol] = $response->results[0]->geometry->location->lng;
			
			}
			if( $counter == $swapOn ):
				$counter = 0;
				$checkGoogle = TRUE;					
			endif;
			$counter++;
		}
		
		curl_close( $ch );
		return $rtnArray;
	}
}

?>