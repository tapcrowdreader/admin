<?php 

if (! defined('BASEPATH')) exit(__('No direct script access'));

class loyalty_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}

	function getById($id) {
		$res = $this->db->query("SELECT * FROM loyalty WHERE loyaltyid = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
}