<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Theme_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	/**
	 * Returns all themes (within the channel)
	 * @return false|array
	 */
	function getAll( $only_live = false )
	{
		$channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();

		$sql = '
		SELECT * FROM theme, tc_channel_theme xref
		WHERE xref.channelid = %d AND xref.themeId = theme.id';
		if($only_live) $sql.= ' AND islive > 0';

		$sql.= ' ORDER BY theme.sortorder ASC';
		$res = $this->db->query(sprintf($sql, $channel->channelId));

// 		$res = $this->db->query("SELECT * FROM theme ORDER BY sortorder ASC");
		if($res->num_rows() == 0) return FALSE;
		foreach($res->result() as $row){
			if($row->screenshot != ''){
				$row->screenshot = base_url() . $row->screenshot;
			}
		}
		return $res->result();
	}

	/**
	 * Returns all live themes for the current channel
	 * (unless channel is TapCrowd - then it return all)
	 * @return false|array
	 */
	function getAllLive()
	{
		return $this->getAll(true);

// 		$channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();
// 		$sql = sprintf('
// 		SELECT * FROM theme, tc_channel_theme xref
// 		WHERE xref.channelid = %d AND xref.themeId = theme.id
// 		AND islive > 0 ORDER BY theme.sortorder ASC', $channel->channelId);
//
// 		$res = $this->db->query($sql);
// 		if($res->num_rows() == 0) return false;
// 		foreach($res->result() as $row){
// 			if($row->screenshot != ''){
// 				$row->screenshot = base_url() . $row->screenshot;
// 			}
// 		}
// 		return $res->result();
	}

	function getById($id) {
		$res = $this->db->query("SELECT * FROM theme WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		$row = $res->row();
		$row->screenshot = base_url() . $row->screenshot;
		return $row;
	}

	function removeValuesOfTheme($id) {
		$this->db->query("DELETE FROM themeappearance WHERE themeid = $id");
	}

	function apply($themeid, $appid) {
		$appearance = $this->db->query("SELECT * FROM themeappearance WHERE themeid = $themeid")->result();
		$appappearance = $this->db->query("SELECT * FROM appearance WHERE appid = $appid")->result();
		foreach($appappearance as $a) {
			if(stristr($a->value, 'upload/')) {
				if(file_exists($this->config->item('imagespath') . $a->value)) {
					unlink($this->config->item('imagespath') . $a->value);
				}
				if(file_exists($this->config->item('imagespath') . 'cache/' . $a->value)) {
					unlink($this->config->item('imagespath') . 'cache/' . $a->value);
				}
			}
		}
		$this->db->query("DELETE FROM appearance WHERE appid = $appid");
		if(!is_dir($this->config->item('imagespath') . "upload/appimages/".$appid)){
			mkdir($this->config->item('imagespath') . "upload/appimages/".$appid, 0775, true);
		}
		foreach($appearance as $a) {
			//copy the images to app specific path
			if(strlen($a->value) != 6) {
				$image = $this->config->item('imagespath') . $a->value;
				$newpath = 'upload/appimages/'.$appid .'/'. substr($a->value, strrpos($a->value , "/") + 1);
				if(file_exists($image)) {
					if(copy($image, $this->config->item('imagespath') . $newpath)) {
						$a->value = $newpath;
					}
				}
			}
			$this->general_model->insert('appearance', array('appid' => $appid, 'controlid' => $a->controlid, 'value' => $a->value));
		}

		//icons of launchers
		$icons = array();
		$iconsres = $this->db->query("SELECT icon, moduletypeid FROM themeicon WHERE themeid = $themeid")->result();
		foreach($iconsres as $i) {
			$icons[$i->moduletypeid] = $i->icon;
		}
		$eventids = array();
		$eventidsres = $this->db->query("SELECT eventid FROM appevent WHERE appid = $appid")->result();
		foreach($eventidsres as $e) {
			$eventids[] = $e->eventid;
		}
		$venueids = array();
		$venueidsres = $this->db->query("SELECT venueid FROM appvenue WHERE appid = $appid")->result();
		foreach($venueidsres as $v) {
			$venueids[] = $v->venueid;
		}

		$eventidssql = implode(',', $eventids);
		$venueidssql = implode(',', $venueids);
		$sql = "SELECT * FROM launcher WHERE appid = $appid";
		if(!empty($eventids)) {
			$sql .= " OR eventid IN ($eventidssql)";
		}
		if(!empty($venueids)) {
			$sql .= " OR venueid IN ($venueidssql)";
		} 
		$launchers = $this->db->query($sql)->result();
		foreach($launchers as $l) {
			if(isset($icons[$l->moduletypeid])) {
				$icon = $icons[$l->moduletypeid];
				$image = $this->config->item('imagespath') . $icon;
				$newpath = 'upload/appimages/'.$appid .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
				if(file_exists($image)) {
					if(!is_dir($this->config->item('imagespath') . 'upload/appimages/'.$appid)){
						mkdir($this->config->item('imagespath') . 'upload/appimages/'.$appid, 0775,true);
					}
					if(copy($image, $this->config->item('imagespath') . $newpath)) {
						$this->general_model->update('launcher', $l->id, array('icon' => $newpath));
					}
				}
			}
		}

		$this->general_model->update('app', $appid, array('themeid' => $themeid));
	}

	function getAllIcons() {
		$moduletypes = $this->db->query("SELECT moduletypeid FROM moduletype_subflavor GROUP BY moduletypeid")->result();
		$moduletypeids = array();
		foreach($moduletypes as $m) {
			$moduletypeids[] = $m->moduletypeid;
		}
		// $moduletypeids[] = 21;
		$moduletypeids = implode(',', $moduletypeids);
		$res = $this->db->query("SELECT id, moduletypeid, icon, title FROM defaultlauncher WHERE moduletypeid IN($moduletypeids) AND icon <> ''")->result();
		return $res;
	}

	function getThemeIcons($id) {
		$res = $this->db->query("SELECT * FROM themeicon WHERE themeid = $id");
		return $res->result();
	}

	function deleteIcons($id) {
		$this->db->query("DELETE FROM themeicon WHERE themeid = $id");
	}

	function getAppearance($themeid) {
		$res = $this->db->query("SELECT * FROM themeappearance WHERE themeid = $themeid");
		$appearance = array();
		foreach($res->result() as $row) {
			$appearance[$row->controlid] = $row;
		}
		return $appearance;
	}

	function remove($id) {
		if($id > 0) {
			$this->db->query("DELETE FROM themeappearance WHERE themeid = $id");
			$this->db->query("DELETE FROM themeicon WHERE themeid = $id");
			$this->general_model->remove('theme', $id);
		}
	}

	function getAppearanceOfThemeControl($themeid, $controlid) {
		$res = $this->db->query("SELECT * FROM themeappearance WHERE themeid = $themeid AND controlid = $controlid LIMIT 1");
		return $res->row();
	}

	function getAppearanceIconOfThemeControl($themeid, $moduletypeid) {
		$res = $this->db->query("SELECT * FROM themeicon WHERE themeid = $themeid AND moduletypeid = $moduletypeid LIMIT 1");
		return $res->row();
	}

	function getThemeFormIcon($themeid) {
		$res = $this->db->query("SELECT * FROM themeicon WHERE themeid = $themeid AND moduletypeid = 44 LIMIT 1");
		if($res->num_rows() > 0) {
			return $res->row();
		}

		return '';		
	}
}
