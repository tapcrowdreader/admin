<?php if(!defined('BASEPATH')) exit('No direct script access');

class Brands_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Brands_model() {
		parent::__construct();
	}
	
	function getBrandsByEventID($eventid) {
		$this->db->where('eventid', $eventid);
		$res = $this->db->get('exhibitorbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getBrandsByVenueID($venueid) {
		$this->db->where('venueid', $venueid);
		$res = $this->db->get('exhibitorbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getBrandByID($brandid) {
		$this->db->where('id', $brandid);
		$res = $this->db->get('exhibitorbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	// ### Webservice
	
	function getBrandsByEventIDService ($eventid) {
		$res = $this->db->query("SELECT * FROM exhibitorbrand WHERE eventid='$eventid' ORDER BY name asc");
		if($res->num_rows() == 0) return FALSE;
		$data = array();
		foreach($res->result() as $row) {
			$data += array($row->id => $row);
		}
		return $data;
	}
	
	function getBrandsByVenueIDService ($venueid) {
		$res = $this->db->query("SELECT * FROM exhibitorbrand WHERE venueid='$venueid' ORDER BY name asc");
		if($res->num_rows() == 0) return FALSE;
		$data = array();
		foreach($res->result() as $row) {
			$data += array($row->id => $row);
		}
		return $data;
	}
}