<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Users_model extends CI_Model {

	private $salt;
	
	//php 5 constructor
	function __construct() {
		$this->salt = 'wvcV23efGead!(va$43'; // also used in API -> checkLogin
		parent::__construct();
	}
	
	//php 4 constructor
	function Users_model() {
		$this->salt = 'wvcV23efGead!(va$43'; // also used in API -> checkLogin
		parent::__construct();
	}
	
	function getUserByAppId($appId){
		$res = $this->db->query('SELECT user.* FROM user, appuser WHERE user.id = appuser.userid AND appuser.appid = '.$appId.' GROUP BY user.login ORDER BY user.name');
		
		if($res->num_rows() == 0) return array();
		
		return $res->result();
	}
	
	function getUserById($userId){
		$this->db->select('*');
		$this->db->where('id', $userId);
		
		return $this->db->get('user')->row();
	}

	function getAppUserIds($appId) {
		$res = $this->db->query('SELECT user.id FROM user, appuser WHERE user.id = appuser.userid AND appuser.appid = '.$appId.' GROUP BY user.login');
		
		if($res->num_rows() == 0) return array();
		
		$ids = array();
		foreach($res->result() as $user) {
			$ids[$user->id] = $user->id;
		}

		return $ids;
	}
	
	function checkUser($appId, $loginName, $type, $typeId){
		$res = $this->db->query('SELECT id FROM user WHERE id IN(SELECT userid FROM appuser WHERE appid = ' . $appId . ' AND parentType = "'.$type.'" AND parentId = "'.$typeId.'") AND login = \'' . $loginName . '\'');
		if($res->num_rows() == 0) return TRUE;
		
		return FALSE;
	}
	
	function removeUser($appId, $userId){
		$appId 	= intval( $appId );
		$userId = intval( $userId );
		
		$this->db->where('id', $userId);
		if($this->db->delete('user')){
			$this->db->where('userid', $userId);
			$this->db->where('appid', $appId);
			$this->db->delete('appuser');
			
			return TRUE;			
		}
		
		return FALSE;		
	}

	function stayloggedin($type, $typeId) {
		if($type == 'event') {
			$app = $this->app_model->getRealAppOfEvent($typeId);
		} elseif($type == 'venue') {
			$app = $this->app_model->getRealAppOfVenue($typeId);
		} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
		}

		$res = $this->db->query("SELECT * FROM usermodule WHERE appid = $app->appid LIMIT 1");
		if($res->num_rows() == 0) return false;

		$res = $res->row();
		if($res->stayloggedin == 1) {
			return true;
		} 

		return false;
	}

	function saveStayLoggedIn($appid, $value) {
		$check = $this->db->query("SELECT id FROM usermodule WHERE appid = $appid LIMIT 1");
		if($value) {
			if($check->num_rows() == 0) {
				$this->general_model->insert('usermodule', array('appid' => $appid, 'stayloggedin' => 1));
			} else {
				$this->general_model->update('usermodule', $check->row()->id, array('stayloggedin' => 1));
			}
		} else {
			if($check->num_rows() == 0) {
				$this->general_model->insert('usermodule', array('appid' => $appid, 'stayloggedin' => 0));
			} else {
				$this->general_model->update('usermodule', $check->row()->id, array('stayloggedin' => 0));
			}
		}
	}

	function pincodes($type, $typeId) {
		if($type == 'event') {
			$app = $this->app_model->getRealAppOfEvent($typeId);
		} elseif($type == 'venue') {
			$app = $this->app_model->getRealAppOfVenue($typeId);
		} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
		}

		$res = $this->db->query("SELECT * FROM usermodule WHERE appid = $app->appid LIMIT 1");
		if($res->num_rows() == 0) return false;

		$res = $res->row();
		if($res->pincodes == 1) {
			return true;
		} 

		return false;
	}

	function savePincodes($appid, $value) {
		$check = $this->db->query("SELECT id FROM usermodule WHERE appid = $appid LIMIT 1");
		if($value) {
			if($check->num_rows() == 0) {
				$this->general_model->insert('usermodule', array('appid' => $appid, 'pincodes' => 1));
			} else {
				$this->general_model->update('usermodule', $check->row()->id, array('pincodes' => 1));
			}
		} else {
			if($check->num_rows() == 0) {
				$this->general_model->insert('usermodule', array('appid' => $appid, 'pincodes' => 0));
			} else {
				$this->general_model->update('usermodule', $check->row()->id, array('pincodes' => 0));
			}
		}
	}


	function securedmodules($app) {
		$res = $this->db->query("SELECT s.launcherid, l.title FROM securedmodules s INNER JOIN launcher l ON l.id = s.launcherid WHERE s.appid = $app->id");
		if($res->num_rows > 0) {
			return $res->result();
		}

		if($app->familyid == 1) {
			return array('event');
		} elseif($app->familyid == 2) {
			return array('venue');
		} else {
			return array('app');
		}
	}

	function saveUsermodule($app, $post) {
		$this->db->query("DELETE FROM securedmodules WHERE appid = $app->id");
		foreach($post as $post) {
			$data = array(
				'appid' => $app->id,
				'launcherid' => $post
				);
			$this->general_model->insert('securedmodules', $data);
		}
	}

	function deleteAppuser($userid) {
		$this->db->query("DELETE FROM appuser WHERE userid = $userid");
	}

	function getUserSecuredModuleIds($user) {
		$res = $this->db->query("SELECT parentId FROM appuser WHERE userid = $user->id AND parentType = 'launcher'");
		$ids = array();
		if($res->num_rows() > 0) {
			foreach($res->result() as $r) {
				$ids[] = $r->parentId;
			}
		}

		return $ids;
	}
	
	function CheckMeetMe($object, $type) {
		$res = $this->db->query("SELECT id FROM module WHERE moduletype = 64 AND $type = $object->id");
		if($res->num_rows() > 0) {
			return true;
		} 

		return false;
	}
	
	function insertUser($appid, $userData, $parentType, $parentId, $modules = ''){
		$userData['password'] = md5( $userData['password'] . $this->salt );
		
		if($this->db->insert('user', $userData)){
			$userId = $this->db->insert_id();
			if( !empty( $modules ) ) {
				foreach( $modules as $launcherid ) {
					$this->general_model->insert('appuser', array('appid' => $appid, 'userid' => $userId, 'parentType' => 'launcher', 'parentId' => $launcherid));
				}
			} else {
				$this->general_model->insert('appuser', array('appid' => $appid, 'userid' => $userId, 'parentType' => $parentType, 'parentId' => $parentId));
			}
			
			return $userId;
		} else {
			return FALSE;
		}
	}
	
	function updateUser($userId, $appid, $userData, $parentType, $parentId, $modules = ''){
		if( $userData['password'] )
			$userData['password'] = md5( $userData['password'] . $this->salt );
		
		$this->db->where('id', $userId);
		if($this->db->update('user', $userData)){
			
			if( !empty( $modules ) ) {
				$this->deleteAppuser($userId);
				
				foreach( $modules as $launcherid ) {
					$this->general_model->insert('appuser', array('appid' => $appid, 'userid' => $userId, 'parentType' => 'launcher', 'parentId' => $launcherid));
				}
			} else {
				$this->deleteAppuser($userId);
				$this->general_model->insert('appuser', array('appid' => $appid, 'userid' => $userId, 'parentType' => $parentType, 'parentId' => $parentId));
			}
			
			return $userId;
		} else {
			return FALSE;
		}
	}
	
	function checkUserExists($appId, $loginName, $type, $typeId){
		$res = $this->db->query("SELECT id FROM user WHERE id IN
			(SELECT userid FROM appuser WHERE appid = ? AND parentType = ? AND parentId = ?) AND login = ?", array($appId, $type, $typeId, $loginName));
		if($res->num_rows() == 0) return FALSE;
		else return $res->row()->id;
	}
	
	function applySecuredModules($launcherid, $userid, $appid) {
		$exists = $this->db->query("SELECT id FROM appuser WHERE userid = $userid AND parentType = 'launcher' AND parentId = $launcherid LIMIT 1");
		if($exists->num_rows() == 0) {
			$this->general_model->insert('appuser', array(
					'appid' => $appid,
					'userid' => $userid,
					'parentType' => 'launcher',
					'parentId' => $launcherid
				));
		}
	}

}
