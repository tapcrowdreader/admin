<?php if(!defined('BASEPATH')) exit('No direct script access');

class Metadata_model extends CI_Model
{
	function get($appid, $table, $tableid) {
		$res = $this->db->query("SELECT * FROM metadata WHERE appid = $appid AND `table` = '$table' AND identifier = $tableid");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function fetchAssoc($appid, $table, $tableid) {
		$res = $this->db->query("SELECT * FROM metadata WHERE appid = $appid AND `table` = '$table' AND identifier = $tableid");
		if($res->num_rows() == 0) return FALSE;
		$array = array();
		foreach($res->result() as $row) {
			$array[$row->key] = $row;
		}

		return $array;
	}

	function getById($id) {
		$res = $this->db->query("SELECT * FROM metadata WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

	function insertArray($array, $appid, $table, $identifier) {
		foreach($array as $key => $value) {
			$delete = $this->db->query("DELETE FROM metadata WHERE appid = $appid AND `table` = '".$table."' AND identifier = '".$identifier."' AND `key` = '".$key."'");
			$sql = $this->db->query("INSERT INTO metadata (appid, `table`, identifier, `key`, `value`) VALUES ($appid, '".$table."', $identifier, '".$key."', '".$value."')");
		}
	}

	function removeKey($key, $table, $identifier) {
		$this->db->query("DELETE FROM metadata WHERE `table`='$table' AND identifier=$identifier AND `key`='$key'");
	}

	function removeFromObject($table, $id) {
		$this->db->query("DELETE FROM metadata WHERE `table`='$table' AND identifier=$id");
	}



	#
	# new metadata functions
	#
	function getAllTypes() {
		$query = $this->db->query("SHOW COLUMNS FROM tc_metadata LIKE 'type'");
		$row = $query->row();
		$regex = "/'(.*?)'/";
		preg_match_all( $regex , $row->Type, $enum_array );
		$enum_fields = $enum_array[1];
		return( $enum_fields );
	}

	function getFieldsByParent($type, $id) {
		$res = $this->db->query("SELECT * FROM tc_metadata WHERE parentType = '$type' AND parentId = $id ORDER BY sortorder DESC");
		return $res->result();
	}

	function getFieldsByParentMulti($type, $id, $app, $eventvenue, $moduletypeid) {
		if($app->apptypeid == 4) {
			$venueid = $this->db->query("SELECT venueid FROM appvenue WHERE appid = $app->id LIMIT 1")->row()->venueid;
			$sql = "SELECT id FROM launcher WHERE moduletypeid = 26 AND launcher.venueid = $venueid LIMIT 1";
		} elseif($eventvenue == 'venue') {
			$sql ="
			SELECT id FROM launcher WHERE tag = (
				SELECT tag.tag FROM launcher
				INNER JOIN tag ON launcher.venueid = tag.venueid
				WHERE launcher.id=$id LIMIT 1
			) AND launcher.appid = $app->id LIMIT 1";
		} elseif($eventvenue == 'event') {
			$sql ="
			SELECT id FROM launcher WHERE tag = (
				SELECT tag.tag FROM launcher
				INNER JOIN tag ON launcher.eventid = tag.eventid
				WHERE launcher.id=$id LIMIT 1
			) AND launcher.appid = $app->id LIMIT 1";
		}

		$parentLauncher = $this->db->query($sql);
		if($parentLauncher->num_rows() > 0) {
			$parentLauncher = $parentLauncher->row();
			$res = $this->db->query("SELECT * FROM tc_metadata WHERE parentType = '$type' AND parentId = $parentLauncher->id AND moduletypeid = $moduletypeid ORDER BY sortorder DESC");
			return $res->result();
		}
		$res = $this->db->query("SELECT * FROM tc_metadata WHERE parentType = '$type' AND parentId = $id AND moduletypeid = $moduletypeid ORDER BY sortorder DESC");
		return $res->result();
	}

	function getFieldById($id) {
		$res = $this->db->query("SELECT * FROM tc_metadata WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) {
			return false;
		}
		return $res->row();
	}

	function removeFieldsFromObject($type, $id) {
		return $this->db->query("DELETE FROM tc_metavalues WHERE parentType = '$type' AND parentId = $id");
	}

	function removeFieldFromObject($type, $id, $fieldid, $language) {
		return $this->db->query("DELETE FROM tc_metavalues WHERE parentType = '$type' AND parentId = $id AND metaId = $fieldid AND language = '$language'");
	}

	function removeFieldsFromManyObjects($type, $ids) {
		if(!empty($ids) && !empty($type)) {
			$ids = implode(',', $ids);
			$this->db->query("DELETE FROM tc_metavalues WHERE parentType = '$type' AND parentid IN ($ids)");
		}
	}

	function getValuesByParent($type, $id, $app) {
		return $this->db->query("SELECT * FROM tc_metavalues WHERE parentType = '$type' AND parentId = $id AND language = '$app->defaultlanguage'")->result();
	}

	function getMetadata($launcher, $type, $typeid, $app)
	{
		if($app->familyid == 3 || $app->familyid == 2) {
			if($launcher->venueid != 0 && $app->familyid != 2) {
				$metadataRes = $this->metadata_model->getFieldsByParentMulti('launcher', $launcher->id, $app, 'venue', $launcher->moduletypeid);
			} elseif($launcher->eventid != 0) {
				$metadataRes = $this->metadata_model->getFieldsByParentMulti('launcher', $launcher->id, $app, 'event', $launcher->moduletypeid);
			} else {
				$metadataRes = $this->metadata_model->getFieldsByParent('launcher', $launcher->id);
			}
		} else {
			$metadataRes = $this->metadata_model->getFieldsByParent('launcher', $launcher->id);
		}
		
		$metadata = array();
		foreach($metadataRes as $m) {
			$metadata[$m->name] = $m;
		}
		if(!empty($type) && !empty($typeid)) {
			$metavalues = $this->metadata_model->getValuesByParent($type, $typeid, $app);
			foreach($metavalues as $v) {
				if(isset($metadata[$v->name])) {
					$metadata[$v->name]->value = $v->value;
				}
			}
		}
		return $metadata;
	}

	function getMetadataNamesOfLaunchers($launcherids) {
		if(!empty($launcherids)) {
			$launcherids = implode(',', $launcherids);
			$res = $this->db->query("SELECT parentId, name FROM tc_metadata WHERE parentType = 'launcher' AND parentId IN ($launcherids)")->result();

			$metadata = array();
			foreach($res as $r) {
				$metadata[$r->parentId][] = $r;
			}

			return $metadata;
		}

		return array();
	}
	
	function updateMetaDataValues($data, $whr){
		foreach( $whr as $key=>$val )
			$this->db->where($key, $val);
		
		if($this->db->update('tc_metavalues', $data)) return TRUE;
		
		return FALSE;
	}

}