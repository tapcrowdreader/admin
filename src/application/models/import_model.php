<?php if(!defined('BASEPATH')) exit('No direct script access');

class Import_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Import_model() {
		parent::__construct();
	}
	
	function getXpoList() {
		$client = new soapclient($this->config->item('kxpo_url'));
		$result = $client->getBeurzen(array("sPaswoord" => $this->config->item('kxpo_password')));
		$xml = simplexml_load_string($result->getBeurzenResult);
		
		foreach($xml as $item) {
			if($item->NaamNL[0] != "") {
				$event->id = $item->ID[0];
				$event->name = $item->NaamNL[0];
			}else if($item->NaamFR[0] != "") {
				$event->id = $item->ID[0];
				$event->name = $item->NaamFR[0];
			}else if($item->NaamEN[0] != "") {
				$event->id = $item->ID[0];
				$event->name = $item->NaamEN[0];
			}
			$returnlist[] = $event;
			$event = null;
		}
		if(isset($returnlist)) {
			return $returnlist;
		}else {
			return FALSE;
		}
	}
	
	function getXpoBeurs($beursid) {
		ini_set('max_execution_time', 0);
		$client = new soapclient($this->config->item('kxpo_url'));
		$result = $client->getBeurs(array("sPaswoord" => $this->config->item('kxpo_password'), 'iBeursID'=> $beursid));
		$xml = simplexml_load_string($result->getBeursResult);
		$xml = $xml->beurs;
		return $xml;
	}
    
    function readIbbtXml($name, $eventid, $appid) {
        $xml = simplexml_load_file("upload/import/ibbt/".$name);
        $sessiongroupOrder = 0;
        $date = '12 September 2011';
        $sessionid = 1;
        $sessionOrder = 0;
        $sessiongroupid = 712;
        
        $months = array(
        'jan' => 1,
        'feb' => 2,
        'mar' => 3,
        'apr' => 4,
        'may' => 5,
        'jun' => 6,
        'jul' => 7,
        'aug' => 8,
        'sep' => 9,
        'oct' => 10,
        'nov' => 11,
        'dec' => 12
        );

        foreach($xml->children() as $session)
        {            
            $paperOrder = 0;
            //create sessiongroup (dag) wanneer datum verschilt van vorige sessie
            if(((string) $session->date) != $date) { 
                $sessiongroupid = $sessiongroupid + 1;
                $sessiongroupOrder += 10;
                $date = (string) $session->date;
            }
            
            //create session (effectieve sessie)
            $chairsString = "";
            foreach($session->chairs as $chairs) {
                foreach($chairs as $chair) {
                    if($chairsString != "") {
                        $chairsString .= ', ';
                    }
                    $chairsString .= (string) $chair->name;
                }
                
            }
            $timeArray = explode('-',$session->range);
            $starttime = date("Y-m-d H:i:s",strtotime($timeArray[0]));
            $endtime = date("Y-m-d H:i:s",strtotime($timeArray[1]));

            $sessionToAdd = array(
                'name' => (string) $session->sessiontitle,
                'location'    => (string) $session->room,
                'speaker'   => $chairsString,
                'order' => $sessionOrder,
                'sessiongroupid' => $sessiongroupid,
                'starttime'   => $starttime,
                'endtime'     => $endtime
            );
            
            $sessionToAddId = $this->general_model->insert('session', $sessionToAdd);

            $sessionOrder += 10;
            //var_dump($sessionToAdd);
            
            //create session (lezing/paper)
            foreach($session->papers->paper as $paper) {
                $authorsOfPaper = array();
                foreach($paper->authors as $authors) {
                    foreach($authors as $author) {
                        $authorsOfPaper[] = $author;
                    }
                }
                
                $speakers = "";
                foreach($authorsOfPaper as $authorOfPaper) {
                    if($speakers != "") {
                        $speakers .= ', ';
                    }
                    $speakers .= (string) $authorOfPaper->name . ' (' . $authorOfPaper->affiliation . ')';
                }

                $dateArray = explode(" ", (string) $session->date);
                $day = $dateArray[0];
                $month = $dateArray[1];
                $monthnumber = $months[strtolower(substr($month,0,3))];
                $year = $dateArray[2];
                
                $starttime = explode(":", (string) $paper->starttime);
                $hour = $starttime[0];
                $minutes = $starttime[1];

                $fullStarttime = date("Y-m-d H:i:s", mktime($hour, $minutes, '0', $monthnumber, $day, $year));
                $fullEndtime = date("Y-m-d H:i:s", mktime($hour, $minutes + 20, '0', $monthnumber, $day, $year));

                $paperToAdd = array(
                  'name'        => (string) $paper->papertitle,
                  'description' => (string) $paper->abstract,
                  'starttime'   => $fullStarttime,
                  'endtime'     => $fullEndtime,
                  'speaker'     => (string) $speakers, 
                  'location'    => (string) $session->room,
                  'order'       => $paperOrder,
                  'sessiongroupid' => $sessiongroupid
                );
                
                $paperToAddId = $this->general_model->insert('session', $paperToAdd);
                
                $tag1 = array(
                    'appid' => $appid,
                    'sessionid' => $paperToAddId,
                    'tag'   => $sessionToAddId
                );
                $this->general_model->insert('tag', $tag1);
                
                
                $paperOrder += 10;
            }
            
            $sessionid++;
        }
    }

}