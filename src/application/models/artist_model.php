<?php if(!defined('BASEPATH')) exit('No direct script access');

class Artist_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Artist_model() {
		parent::__construct();
	}
	
	function getArtistsByAppID($appid) {
		$res = $this->db->query("SELECT * FROM artist WHERE appid = $appid ORDER BY `order` ASC");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	function getArtistByID($id) {
		$res = $this->db->query("SELECT * FROM artist WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	function getArtistsByEventId($id) {
		$res = $this->db->query("SELECT * FROM artist WHERE eventid = $id ORDER BY `order` ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	function getArtistsByVenueId($id) {
		$res = $this->db->query("SELECT * FROM artist WHERE venueid = $id ORDER BY `order` ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
    
	function edit($id, $data) {
		$this->db->where('id', $id);
		if($this->db->update('artist', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
    
    function getArtistSessionOfSession($sessionid) {
		$res = $this->db->query("SELECT * FROM artistsessions WHERE sessionid = $sessionid");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
    }
    
    function getArtistsOfEvent($eventid) {
        $sessions = $this->db->query("SELECT s.* FROM sessiongroup sg, session s WHERE sg.id = s.sessiongroupid AND sg.eventid = $eventid ORDER BY sg.order ASC");
        $sessions = $sessions->result();
        
        
        $sessionids = array();
        foreach($sessions as $session) {
            $sessionids[] = $session->id;
        }
        
        $artistids = $this->db->query("SELECT artistid FROM artistsessions WHERE sessionid IN (". implode(',', $sessionids) .")");
        $artistids = $artistids->result();

        $artists = array();
        foreach($artistids as $artistid) {
            if($artistid->artistid != null && $artistid->artistid != 0) {
            $artist = $this->db->query("SELECT * FROM artist WHERE id = $artistid->artistid");
            $artists[] = $artist->row();
            }
        }
        
        return $artists;
    }
}