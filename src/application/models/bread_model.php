<?php
namespace Tapcrowd;

if(!defined('BASEPATH')) exit('No direct script access');
/**
 * Tapcrowd structures and default  model
 * @author Tom Van de Putte
 */

/**
 * Default struct object
 */
abstract class Struct
{
	public $id;
	public $appId;
	public $parentType;
	public $parentId;
	public $sortorder = 0;
	public $status;
	public $imageurl;

	public $tags;
	public $metavalues = array();

	/**
	 * Constructor, sets the identifier
	 */
	public function __construct( $id = null )
	{
		if($id !== null) $this->id = $id;
	}
}

class Resource extends \Tapcrowd\Struct
{
	public $path;
	public $title;
	public $info;
}

class Query
{
	public $appId;
	public $parentType;
	public $parentId;
	public $groupId = null;
	public $tags = null;
	public $query = null;
	public $offset = 0;
	public $limit = 500;

	/**
	 * Construct Query object
	 *
	 * @param int $appId The application identifier
	 * @param string $parentType The parentType
	 * @param int $parentId The parent identifier
	 * @param int $groupId Optional group identifier
	 * @param string $tags Optional comma separated lsits of tags
	 * @param string $query Optional search query
	 * @param int $offset Optional offset in the resultset, defaults to 0 (zero)
	 * @param int $limit Optional max. number of results, defaults to 500
	 */
	public function __construct( $appId, $parentType, $parentId, $groupId = null, $tags = null, $query = null, $offset = 0, $limit = 500 )
	{
		$params = get_class_vars(__CLASS__);
		while(list($p,) = each($params)) $this->$p = $$p;
	}
}

/**
 * UnknownStructException
 */
class UnknownStructException extends \InvalidArgumentException {}


class Bread_model
{
	/**
	 * Database Tablename
	 * @var string
	 */
	protected $_table;

	/**
	 * Database connection
	 * @var \PDO
	 */
	protected $_pdo;

	/**
	 * Classname (struct extension)
	 * @var string
	 */
	protected $_structclass;

	/**
	 * Model Constructor
	 */
	public function __construct( $tablename, \PDO &$pdo, $structclass )
	{
		# Validate table name
		if(!isset($tablename)) {
			throw new \Exception(__('No tablename given for %s', __CLASS__));
		}

		# Validate classname
		if(!class_exists($structclass)) {
			throw new \Exception(__('Struct "%s" not found in %s', $structclass, __CLASS__));
		}

		$this->_pdo = $pdo;
		$this->_table = $tablename;
		$this->_structclass = $structclass;
	}

	/**
	 * Internal Query method
	 * @param string $query The query
	 * @param mixed $param1 Optional extra parameters
	 * @return \PDOStatement
	 */
	final protected function query( $query, $param1 = null )
	{
		if(func_num_args() > 1) {
			$args = func_get_args();
			foreach($args as $k => &$v) {
// 				if($k && !is_numeric($v)) $v = $this->_pdo->quote((string)$v);
				if($k === 0) continue;
				elseif(is_array($v)) {
					$v = array_values($v); $c = count($v);
					while($c-- && $a =& $v[$c]) $a = is_numeric($a)? (int)$a : $this->_pdo->quote($a);
					$v = implode(',',$v);
				}
				elseif(!is_numeric($v)) $v = $this->_pdo->quote((string)$v);
			}
			$query = call_user_func_array('sprintf', $args);
		}
// var_dump($query); //exit;
		$stat =& $this->_statement;
		$stat = $this->_pdo->query($query);
		if(!$stat instanceOf \PDOStatement) {
			$i = $this->_pdo->errorInfo();
			throw new \RuntimeException("[$i[0]|$i[1]]: $i[2]");
		}
		return $stat;
	}

	/**
	 * Internal Execute method
	 * @param string $query The query
	 * @param mixed $param1 Optional extra parameters
	 * @return boolean
	 */
	final protected function execute( $query, $param1 = null )
	{
		if(func_num_args() > 1) {
			$_query = $query;
			$args = func_get_args();
			
			foreach($args as $k => &$v) {
				if($k === 0) continue;
				elseif(is_array($v)) {
					$v = array_values($v); 
					$c = count($v);
					while($c--) {
						$a =& $v[$c];
						if($a == null) {
							$a = '';
						}
						if(is_numeric($a)) {
							$a = (int)$a;
						} else {
							$a = $this->_pdo->quote($a);
						}
					}
					$v = implode(',',$v);
				}
				elseif(!is_numeric($v)) $v = $this->_pdo->quote((string)$v);
			}

			// Uncatchable errors
			try { $query = call_user_func_array('sprintf', $args); } catch(\Exception $e) {}
			if($query === false) {
				throw new \RuntimeException(__('Invalid query syntax "%s"',$_query));
			}
		}
// var_dump($query); exit;
		try {
			$r = $this->_pdo->exec($query);
			if($r === false) $error = $this->_pdo->errorInfo();
		} catch(\PDOException $e) {
			$error = $e->errorInfo;
		}

		if(!empty($error)) {
			throw new \RuntimeException("[$error[0]|$error[1]]: $error[2]");
		} else {
			return $r;
		}
	}

	/**
	 * Lists structures from database
	 *
	 * @param \Tapcrowd\Query $query Query instance
	 * @return \Tapcrowd\Struct[]
	 * @throws \Tapcrowd\UnknownStructException
	 */
	public function browse( \Tapcrowd\Query $query )
	{
		$sql = 'SELECT * FROM '.$this->_table.' WHERE appId=%d AND parentType=%s AND parentId=%d';
		$stat = self::query($sql, $query->appId, $query->parentType, $query->parentId);
		$stat->setFetchMode(\PDO::FETCH_CLASS, $this->_structclass);

		$list = array();
		while(($obj = $stat->fetch()) !== false) {
			$list[] = $obj;
		}
		return $list;
	}

	/**
	 * Get structure
	 *
	 * @param \Tapcrowd\Struct $struct The struct (id must be set)
	 * @return boolean
	 * @throws \Tapcrowd\UnknownStructException
	 */
	public function read( \Tapcrowd\Struct &$struct )
	{
		if(!isset($struct->id)) { throw new UnknownStructException; }
		$sql = 'SELECT * FROM '. $this->_table .' WHERE id=%d LIMIT 1';
		$stat = self::query($sql, (int)$struct->id);
		$stat->setFetchMode(\PDO::FETCH_INTO, $struct);
		$obj = $stat->fetch();

		# TODO Select tags

		return ($obj === $struct);
	}

	/**
	 * Edit structure
	 *
	 * @param \Tapcrowd\Struct $struct The struct to edit
	 * @return boolean
	 * @throws \Tapcrowd\UnknownStructException
	 */
	public function edit( \Tapcrowd\Struct &$struct )
	{
		if(!isset($struct->id)) { throw new UnknownStructException; }
		$column_values = array();

		# Parse values
		$values = array_diff_key(get_object_vars($struct), get_class_vars('\Tapcrowd\Struct'));
		unset($values['parentType']);
		unset($values['parentId']);
		$values['status'] = $struct->status; # Add status
		while(list($k, $v) = each($values)) {
			$column_values[] = is_numeric($v)? "$k=$v" : "$k=".$this->_pdo->quote($v);
		}

		# TODO Update tags

		$sql = 'UPDATE '.$this->_table.' SET '.implode(', ',$column_values).' WHERE id=%d';
		return self::execute($sql, (int)$struct->id);
	}

	/**
	 * Add structure
	 *
	 * @param \Tapcrowd\Struct $struct The struct to add
	 * @return boolean
	 * @throws \Tapcrowd\UnknownStructException
	 */
	public function add( \Tapcrowd\Struct &$struct )
	{
		$values = get_object_vars($struct);
		unset($values['id']);
		unset($values['tags']);
		unset($values['metavalues']);
		$columns = implode(',', array_keys($values));
		$sql = 'INSERT INTO '.$this->_table.' ('.$columns.') VALUES(%s)';

		if(self::execute($sql, $values) == 1) {
			$struct->id = (int)$this->_pdo->lastInsertId();
		} else {
			throw new \RuntimeException(__('Could not create new '.get_class($struct)));
		}
	}

	/**
	 * Delete structure
	 *
	 * @param \Tapcrowd\Struct $struct The struct to delete
	 * @return boolean
	 * @throws \Tapcrowd\UnknownStructException
	 */
	public function delete( \Tapcrowd\Struct &$struct )
	{
		if(!isset($struct->id)) { throw new UnknownStructException; }

		# TODO Delete tags

		$sql = 'DELETE FROM '.$this->_table.' WHERE id=%d';
		return self::execute($sql, (int)$struct->id);
	}
}
