<?php if(!defined('BASEPATH')) exit('No direct script access');

class General_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function General_model() {
		parent::__construct();
	}
	
	function all($table) {
		$res = $this->db->get($table);
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function one($table, $id) {
		$this->db->where('id', $id);
		$res = $this->db->get($table);
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function insert($table, $data) {
		if($this->db->insert($table, $data)){
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}
	
	function update($table, $id, $data, $returnid = 0) {
		$this->db->where('id', $id);
		if($this->db->update($table, $data)){
			if($returnid) {
				return TRUE;
			} else {
				return $id;
			}
		} else {
			return FALSE;
		}
	}

	function insert_or_update($table, $data, $updatefield) {
		foreach($data as $key => $value) {
			if($key != $updatefield) {
				$this->db->where($key,$value);
			}
		}
		$res = $this->db->get($table);
		if($res->num_rows() == 0) {
			return $this->insert($table,$data);
		} else {
			return $this->update($table,$res->row()->id,$data);
		}
	}
	
	function updateMany($table, $ids, $data) {
		$this->db->where_in('id', $ids);
		if($this->db->update($table, $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function remove($table, $id) {
		$this->db->where('id', $id);
		if($this->db->delete($table)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function get_meta_data($table = "", $id = "") {
		$this->db->where("table", $table);
		$this->db->where("identifier", $id);
		$res = $this->db->get("metadata");
		return $res->result();
	}
    
	function get_meta_data_by_key($table = "", $id = "", $key = "") {
		$this->db->where("`table`", $table);
		$this->db->where("`identifier`", $id);
        $this->db->where("`key`", $key);
		$res = $this->db->get("metadata");

		return $res->row();
	}
	
	function update_meta_data($table, $id, $key, $data) {
		$this->db->where('`table`', $table);
		$this->db->where('identifier', $id);
		$this->db->where('`key`', $key);
		if($this->db->update('metadata', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function get_default_meta_data($table = "", $apptypeid = "") {
		$this->db->where("table", $table);
		$this->db->where("apptypeid", $apptypeid);
		$res = $this->db->get("defaultmetadata");
		return $res->result();
	}
	
	function get_meta_data_temp($table = "", $appid = "") {
		$this->db->where("table", $table);
		$this->db->where("appid", $appid);
		$res = $this->db->get("metadatatemp");
		return $res->result();
	}

	function getUsedMetadata($appid, $table) {
		$res = $this->db->query("SELECT * FROM metadata WHERE appid = $appid AND `table`= '$table' GROUP BY `key`");

		return $res->result();
	}
        
	function recordExists($table, $field, $value, $eventid){
		$this->db->select('*');
		$this->db->where('eventid',  $eventid);
		$this->db->where($field,  $value); 
		$res = $this->db->get($table)->row()->id;
		return $res;
	}
	
	function speakerExists($name, $eventid){
		$this->db->select('id');
		$this->db->where('name', $name);
		$this->db->where('eventid',  $eventid);
		$res = $this->db->get('speaker')->row()->id;
		return $res;
	}
	
	function removeByParentId($parentFieldName, $parentFieldVal, $table){
		$this->db->where($parentFieldName, $parentFieldVal);
		if($this->db->delete($table)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function rowExists($table, $whrArr, $getRow=0){
		if($getRow)
        	$this->db->select('*');
		else
        	$this->db->select('id');
            
		if(count($whrArr)>0):
        	foreach($whrArr as $field => $value)
            	$this->db->where($field,  $value); 
		else:
        	return false;
		endif;
            
        if($getRow)
        	return $this->db->get($table)->row();
		else
        	return $this->db->get($table)->row()->id;
	}
	
	function PoisExists($appId, $checkField, $checkValue){
        $res = $this->db->query('SELECT DISTINCT(venue.id) FROM venue, appvenue WHERE venue.'.$checkField.' = "'.$checkValue.'" AND appvenue.appid = '.$appId.' AND venue.id IN(appvenue.venueid)');
		return $res->result();
	}

	function removeMany($ids, $table) {
		if(!empty($ids) && !empty($table)) {
			$ids = implode(',', $ids);
			if($table == 'catalog' || $table == 'exhibitor') {
				$this->db->query("DELETE FROM groupitem WHERE itemtable = '$table' AND itemid IN ($ids)");
			}
			$this->db->query("DELETE FROM $table WHERE id IN ($ids)");
		}
	}

	function insert_unique($table, $data, $uniquefield, $uniquevalue) {
		if(is_array($uniquefield) && is_array($uniquevalue)) {
			$where = '';
			$i = 0;
			foreach($uniquefield as $f) {
				$where .= $f . " = '" . $uniquevalue[$i] . "'";
				$i++;
				if($i < count($uniquefield)) {
					$where .= ' AND ';
				}
			}
			$res = $this->db->query("SELECT id FROM $table WHERE " . $where . " LIMIT 1");
		} else {
			$res = $this->db->query("SELECT id FROM $table WHERE $uniquefield = $uniquevalue LIMIT 1");
		}
		
		if($res->num_rows() == 0) {
			return $this->insert($table, $data);
		} else {
			return $this->update($table, $res->row()->id, $data, 0);
		}
	}

	function get($table, $field, $fieldid) {
		$res = $this->db->query("SELECT * FROM $table WHERE $field = $fieldid LIMIT 1");
		if($res->num_rows() > 0) {
			return $res->row();
		}

		return false;
	}
}