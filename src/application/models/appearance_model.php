<?php if(!defined('BASEPATH')) exit('No direct script access');

class Appearance_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getAppearanceByAppId($id) {
		$res = $this->db->query("SELECT a.*, c.name as controlname FROM appearance a INNER JOIN appearancecontrol c ON a.controlid=c.id WHERE a.appid = $id");
		$settings = array();
		if($res->num_rows() != 0) {
			foreach($res->result() as $setting) {
				$settings[$setting->controlid] = $setting;
			}
		}
		return $settings;
	}
	
	function getAppearanceById($id) {
		$res = $this->db->query("SELECT * FROM appearance WHERE id = $id");
		return $res->row();
	}
	
	function deleteAppearanceFromAppId($id) {
		$this->db->query("DELETE FROM appearance WHERE appid = $id");
	}
	
	function getAllControls($appid = '') {
		if($appid == '') {
			$res = $this->db->query("SELECT * FROM appearancecontrol");
			if($res->num_rows() == 0) return array();
			return $res->result();
		} else {
			$res = $this->db->query("SELECT c.id as controlid, c.name as controlname, c.appearancetype as typeid, c.image_width as image_width, c.image_height as image_height, c.image_name as image_name, c.title as controltitle, c.defaultcolor as defaultcolor, a.id as appearanceid, a.value as value, a.appid as appid FROM appearancecontrol c LEFT JOIN appearance a ON (c.id = a.controlid AND a.appid = $appid) ORDER BY c.order");
			if($res->num_rows() == 0) return array();
			return $res->result();
		}
	}

	function getControlsForApp($app, $contentflavor = false) {
		if($app->apptypeid == 12) {
			$res = $this->db->query("SELECT * FROM appearance WHERE appid = $app->id")->result();
			return $res;
		}
		if(($app->apptypeid == 9 && $contentflavor == false)) {
			return array();
		}
		//get all active modules in this app
		$modulesInApp = array();
		$eventids = $this->db->query("SELECT eventid FROM appevent WHERE appid = $app->id")->result();
		foreach($eventids as $eventid) {
			$eventmodules = $this->db->query("SELECT moduletype FROM module WHERE event = $eventid->eventid")->result();
			foreach($eventmodules as $em) {
				$modulesInApp[] = $em->moduletype;
			}
			$eventDynamicLaunchers = $this->db->query("SELECT moduletypeid FROM launcher WHERE eventid = $eventid->eventid AND moduletypeid = 0");
			if($eventDynamicLaunchers->num_rows() > 0) {
				$modulesInApp[] = 0;
			}
		}

		$venueids = $this->db->query("SELECT venueid FROM appvenue WHERE appid = $app->id")->result();
		foreach($venueids as $venueid) {
			$venuemodules = $this->db->query("SELECT moduletype FROM module WHERE venue = $venueid->venueid")->result();
			foreach($venuemodules as $vm) {
				$modulesInApp[] = $vm->moduletype;
			}

			$venueDynamicLaunchers = $this->db->query("SELECT moduletypeid FROM launcher WHERE venueid = $venueid->venueid AND moduletypeid = 0");
			if($venueDynamicLaunchers->num_rows() > 0) {
				$modulesInApp[] = 0;
			}
		}


		$appmodules = $this->db->query("SELECT moduletype FROM module WHERE app = $app->id")->result();
		foreach($appmodules as $am) {
			$modulesInApp[] = $am->moduletype;
		}

		$appDynamicLaunchers = $this->db->query("SELECT moduletypeid FROM launcher WHERE appid = $app->id AND moduletypeid = 0");
		if($appDynamicLaunchers->num_rows() > 0) {
			$modulesInApp[] = 0;
		}

		$modulesSql = '';
		$modulesInApp = array_unique($modulesInApp);
		$modulesInApp = implode(',', $modulesInApp);
		if(!empty($modulesInApp)) {
			$modulesSql = "AND afm.moduletypeid IN (".$modulesInApp.")";
		}

		//get all the controls with their values for this app
		//slow query because of left join on appearance table. Possible to use union?
		$res = $this->db->query("SELECT c.id as controlid, c.name as controlname, c.appearancetype as typeid, c.image_width as image_width, c.image_height as image_height, c.image_name as image_name, c.title as controltitle, c.defaultcolor as defaultcolor, a.id as appearanceid, a.value as value, a.appid as appid FROM appearancecontrol c INNER JOIN appearanceflavormodule afm ON c.id = afm.appearancecontrolid LEFT JOIN appearance a ON (c.id = a.controlid AND a.appid = $app->id) WHERE (afm.flavorid = $app->apptypeid AND afm.moduletypeid = 0) OR (afm.flavorid = 0 AND afm.moduletypeid = 0) OR ((afm.flavorid = $app->apptypeid OR afm.flavorid = 0) ".$modulesSql.") ORDER BY c.order");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
	function getControlsOfTheme($themeid) {
		$res = $this->db->query("SELECT * FROM themeappearance WHERE themeid = $themeid");
		return $res->result();
	}

	function getScreens($appid) {
		$res = $this->db->query("SELECT * FROM appscreenshots WHERE appid = $appid");
		return $res->result();
	}

	function getLauncherBackgroundColor($appid) {
		$res = $this->db->query("SELECT value FROM appearance WHERE appid = $appid AND controlid = 25 LIMIT 1");
		if($res->num_rows() == 0) {
			return 'transparent';
		} else {
			return '#'.$res->row()->value;
		}
	}
}