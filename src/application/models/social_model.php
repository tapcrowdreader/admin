<?php if(!defined('BASEPATH')) exit('No direct script access');

class Social_model extends CI_Model {
	
	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Social_model() {
		parent::__construct();
	}
	
	function getByEventId($id) {
		$res = $this->db->query("SELECT * FROM socialmedia WHERE eventid='$id' LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getByVenueId($id) {
		$res = $this->db->query("SELECT * FROM socialmedia WHERE venueid='$id' LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getByAppId($id) {
		$res = $this->db->query("SELECT * FROM socialmedia WHERE appid='$id' LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getById($id) {
		$res = $this->db->query("SELECT * FROM socialmedia WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function removeFromEvent($eventid) {
		$res = $this->db->query("DELETE FROM socialmedia WHERE eventid = $eventid");
	}

	function removeFromVenue($venueid) {
		$res = $this->db->query("DELETE FROM socialmedia WHERE venueid = $venueid");
	}
	
	function socialiframe($social) {
		if($social->eventid != 0) {
			if($social->facebookid != '' && $social->twitter == '' && $social->twithash != '#') {
				return 'social/eventfacebook/'.$social->eventid;
			} elseif($social->facebookid == '' && ($social->twitter != '' || $social->twithash != '#')) {
				return 'social/eventtwitter/'.$social->eventid;
			}
			return 'news/event/'.$social->eventid;
		} elseif($social->venueid != 0) {
			if($social->facebookid != '' && $social->twitter == '' && $social->twithash != '#') {
				return 'social/venuefacebook/'.$social->venueid;
			} elseif($social->facebookid == '' && ($social->twitter != '' || $social->twithash != '#')) {
				return 'social/venuetwitter/'.$social->venueid;
			}
			return 'news/venue/'.$social->venueid;
		} elseif($social->appid != 0) {
			if($social->facebookid != '' && $social->twitter == '' && $social->twithash != '#') {
				return 'social/appfacebook/'.$social->appid;
			} elseif($social->facebookid == '' && ($social->twitter != '' || $social->twithash != '#')) {
				return 'social/apptwitter/'.$social->appid;
			}
			return 'news/app/'.$social->appid;
		}
	}
}
?>
