<?php if(!defined('BASEPATH')) exit('No direct script access');

class Askaquestion_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}

    function getQuestionsOfEvent($eventid, $launcherid) {
    	$formsubmissionsResult = $this->db->query("SELECT f.id as formid, f.eventid, f.title, fsf.value, ff.label, ff.formfieldtypeid, fs.id as formsubmissionid, fs.opts as formsubmissionopts, ff.order, fs.visible FROM form f 
    		LEFT JOIN formsubmission fs ON fs.formid = f.id 
    		LEFT JOIN formsubmissionfield fsf ON fsf.formsubmissionid = fs.id
    		LEFT JOIN formfield ff ON ff.id = fsf.formfieldid
    		WHERE launcherid = $launcherid
    		ORDER BY ff.order ASC, fs.id DESC");
    	$formsubmissionsResult = $formsubmissionsResult->result();
    	$formsubmissions = array();
    	foreach($formsubmissionsResult as $s) {
    		if($s->formfieldtypeid == 17 && !empty($s->value)) {
    			$s->speakername = $this->db->query("SELECT name FROM speaker WHERE id = $s->value LIMIT 1")->row()->name;
                $s->speakerid = $s->value;
    		}
            if($s->formsubmissionid != null) {
                $formsubmissions[$s->formsubmissionid][] = $s;
            }
    	}
    	return $formsubmissions;
    }

    function getQuestionFormOfEvent($eventid, $launcherid) {
        $form = $this->db->query("SELECT * FROM form WHERE launcherid = $launcherid LIMIT 1");
        return $form->row();
    }

    function removeMany($ids) {
        $table = 'formsubmission';
        if(!empty($ids) && !empty($table)) {
            $ids = implode(',', $ids);
            $this->db->query("DELETE FROM $table WHERE id IN ($ids)");
            $this->db->query("DELETE FROM formsubmissionfield WHERE formsubmissionid IN ($ids)");
        }
    }

    function getPageLayout($appid, $purpose) {
        $res = $this->db->query("SELECT * FROM customhtml WHERE appid = $appid AND purpose = '$purpose' LIMIT 1");
        if($res->num_rows() > 0) {
            return $res->row();
        }

        return array();
    }
}