<?php

if (!defined('BASEPATH'))
    exit('No direct script access');

class Appclone_model extends CI_Model {

    function getAppNames() {

        $this->db->select('*');
        if(_isAdmin()== FALSE){
            $this->db->where('organizerid',_currentUser()->organizerId);
        }
        $this->db->where('isdeleted',0);
        $this->db->order_by("name", "asc");
        $records = $this->db->get('app')->result();

        $data = array();

        foreach ($records as $rec) {
            $data[$rec->id] = $rec->name;
        }

        return ($data);
    }

    function getOrganizers() {

        $this->db->select('*');
        $this->db->where('activation','active');
        $this->db->order_by("login", "asc");
        $records = $this->db->get('organizer')->result();

        $data = array();

        foreach ($records as $rec) {
            $data[$rec->id] = $rec->login;
        }

        return ($data);
    }


    function getAppData($appId) {
        $this->db->select('*');
        $this->db->where('id', $appId);
        $app = $this->db->get('app')->row();
        return ($app);
    }

    function getLastAppId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('app')->row();
        return $maxid->id;
    }

    function getAppTypeId($appId) {
        $this->db->select('apptypeid');
        $this->db->where('id', $appId);
        $apptypeid = $this->db->get('app')->row();
        return $apptypeid->id;
    }

    function getLastEventId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('event')->row();
        return $maxid->id;
    }

    function getLastExhibitorId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('exhibitor')->row();
        return $maxid->id;
    }

    function getLastSessionId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('session')->row();
        return $maxid->id;
    }

    function getLastNewsId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('newsitem')->row();
        return $maxid->id;
    }

    function getLastRssId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('newssource')->row();
        return $maxid->id;
    }

    function getLastCatalogId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('catalog')->row();
        return $maxid->id;
    }

    function getLastSponsorId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('sponsor')->row();
        return $maxid->id;
    }

    function getLastLauncherId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('launcher')->row();
        return $maxid->id;
    }

    function getLastAdId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('ad')->row();
        return $maxid->id;
    }

    function getLastGroupId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('group')->row();
        return $maxid->id;
    }

    function getLastParentGroupId() {
        $this->db->select_max('id');
        $this->db->select('parentid');
        $maxid = $this->db->get('group')->row();
        return $maxid->parentid;
    }

    function getBrandLastParentGroupId() {
        $this->db->select_max('id');
        $this->db->where('name', 'exhibitorbrands');
        $maxid = $this->db->get('group')->row();
        return $maxid->id;
    }

    function getCategoryLastParentGroupId() {
        $this->db->select_max('id');
        $this->db->where('name', 'exhibitorcategories');
        $maxid = $this->db->get('group')->row();
        return $maxid->id;
    }

    function getLastAttendeeId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('attendees')->row();
        return $maxid->id;
    }

    function getLastSessiongroupId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('sessiongroup')->row();
        return $maxid->id;
    }

    function getLastMapId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('map')->row();
        return $maxid->id;
    }

    function getLastArtistId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('artist')->row();
        return $maxid->id;
    }

    function getLastVenueId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('venue')->row();
        return $maxid->id;
    }

    function getLastGroupSessionId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('sessiongroup')->row();
        return $maxid->id;
    }

    function getOrganizerId($id = NULL) {
        $this->db->select('organizerid');
        $this->db->where('id', $id);
        $organizer = $this->db->get('app')->row();
        return $organizer->organizerid;
    }

    function getEvents($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('appevent')->result();

        $events = array();

        foreach ($records as $rec) {
            $events[$rec->id] = $rec->eventid;
        }
        return ($events);
    }

    function getEventDetail($eventId){
        $this->db->select('*');
        $this->db->where('id', $eventId);
        $eventDetail = $this->db->get('event')->row();
        return $eventDetail;
    }

    function getTags($tagId) {
        $this->db->select('*');
        $this->db->where('id', $tagId);
        $records = $this->db->get('tag')->result();

        $events = array();

        foreach ($records as $rec) {
            $events[] = $rec->id;
        }
        return ($events);
    }

    function getTagDetail($tagId){
        $this->db->select('*');
        $this->db->where('id', $tagId);
        $tagDetail = $this->db->get('tag')->row();
        return $tagDetail;
    }

    function getVenueDetail($venueId){
        $this->db->select('*');
        $this->db->where('id', $venueId);
        $venueDetail = $this->db->get('venue')->row();
        return $venueDetail;
    }

    function getModuleDetail($eventId){
        $this->db->select('*');
        $this->db->where('event', $eventId);
        $moduleDetail = $this->db->get('module')->result();
        return $moduleDetail;
    }

    function getVenueModuleDetail($venueId){
        $this->db->select('*');
        $this->db->where('venue', $venueId);
        $moduleDetail = $this->db->get('module')->result();
        return $moduleDetail;
    }

    function getMapDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $mapDetail = $this->db->get('map')->row();
        return $mapDetail;
    }

    function getVenueMapDetail($id){
        $this->db->select('*');
        $this->db->where('venueid', $id);
        $mapDetail = $this->db->get('map')->row();
        return $mapDetail;
    }

    function getExhibitorDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $exhibitorDetail = $this->db->get('exhibitor')->result();
        return $exhibitorDetail;
    }

    function getSessionDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $sessionDetail = $this->db->get('session')->row();
        return $sessionDetail;
    }

    function getSessions($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $sessionDetail = $this->db->get('session')->result();
        return $sessionDetail;
    }

    function getNewsDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $newsDetail = $this->db->get('newsitem')->result();
        return $newsDetail;
    }

    function getCatalogDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $newsDetail = $this->db->get('catalog')->result();
        return $newsDetail;
    }

    function getVenueCatalogDetail($venueId){
        $this->db->select('*');
        $this->db->where('venueid', $venueId);
        $newsDetail = $this->db->get('catalog')->result();
        return $newsDetail;
    }

    function getEventCatalogDetail($venueId){
        $this->db->select('*');
        $this->db->where('eventid', $venueId);
        $newsDetail = $this->db->get('catalog')->result();
        return $newsDetail;
    }

    function getVenueNewsDetail($venueId){
        $this->db->select('*');
        $this->db->where('venueid', $venueId);
        $newsDetail = $this->db->get('newsitem')->result();
        return $newsDetail;
    }

    function getAppNewsDetail($newsId){
        $this->db->select('*');
        $this->db->where('id', $newsId);
        $newsDetail = $this->db->get('newsitem')->result();
        return $newsDetail;
    }

    function getSponsorDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $SponsorDetail = $this->db->get('sponsor')->result();
        return $SponsorDetail;
    }

    function getVenueSponsorDetail($venueId,$sponsorgroupid){
        $this->db->select('*');
        $this->db->where('venueid', $venueId);
        $this->db->where('sponsorgroupid', $sponsorgroupid);
        $SponsorDetail = $this->db->get('sponsor')->result();
        return $SponsorDetail;
    }

    function getEventSponsorDetail($eventId,$sponsorgroupid){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $this->db->where('sponsorgroupid', $sponsorgroupid);
        $SponsorDetail = $this->db->get('sponsor')->result();
        return $SponsorDetail;
    }

    function getPoiDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $poiDetail = $this->db->get('poi')->result();
        return $poiDetail;
    }

    function getVenueLaunchers($venueId){
        $this->db->select('*');
        $this->db->where('venueid', $venueId);
        $this->db->where('moduletypeid !=', '44');
        $Launchers = $this->db->get('launcher')->result();
        return $Launchers;
    }
    function getEventLaunchers($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $this->db->where('moduletypeid !=', '44');
        $Launchers = $this->db->get('launcher')->result();
        return $Launchers;
    }

    function getLauncherDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $LauncherDetail = $this->db->get('launcher')->result();
        return $LauncherDetail;
    }

    function getAppLauncherDetail($launcherId){
        $this->db->select('*');
        $this->db->where('id', $launcherId);
        $appLauncherDetail = $this->db->get('launcher')->result();
        return $appLauncherDetail;
    }

    function getAppLaunchers($appId){
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $this->db->where('moduletypeid !=', '44');
        $records = $this->db->get('launcher')->result();

        $launchers = array();

        foreach ($records as $rec) {
            $launchers[] = $rec->id;
        }
        return ($records);
    }

    function getVenueNews($appId){
        $this->db->select('*');
        $this->db->where('venueid', $appId);
        $records = $this->db->get('newsitem')->result();

        $news = array();

        foreach ($records as $rec) {
            $news[] = $rec->id;
        }
        return ($records);
    }

    function getFacebookDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $facebookDetail = $this->db->get('facebook')->result();
        return $facebbokDetail;
    }

    function getFavoritesDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $favoritesDetail = $this->db->get('favorites')->result();
        return $favoritesDetail;
    }

    function getAdDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $adDetail = $this->db->get('ad')->result();
        return $adDetail;
    }

    function getGroupDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $groupDetail = $this->db->get('group')->result();
        return $groupDetail;
    }

    function getBrandGroupDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $this->db->where('name', 'exhibitorbrands');
        $group = $this->db->get('group')->row();
        $groupDetail=array();
        foreach($group as $key2 => $value2){
            $groupDetail[$key2]=$value2;
        }
        return $groupDetail;
    }

    function getBrandGroupSizeDetail($eventId, $groupId){
        $group = $this->db->query("SELECT tree FROM `group` WHERE `tree` LIKE '%".$groupId."%' AND `eventid` =".$eventId." ")->result();
        return $group;
    }

    function getCategoryGroupDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $this->db->where('name', 'exhibitorcategories');
        $group = $this->db->get('group')->row();
        $groupDetail=array();
        foreach($group as $key2 => $value2){
            $groupDetail[$key2]=$value2;
        }
        return $groupDetail;
    }

    function getVenueCategoryGroupDetail($venueId){
        $this->db->select('*');
        $this->db->where('venueid', $venueId);
        $this->db->where('name', 'catalogcategories');
        $group = $this->db->get('group')->row();
        $groupDetail=array();
        foreach($group as $key2 => $value2){
            $groupDetail[$key2]=$value2;
        }
        return $groupDetail;
    }

    function getEventSocialDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $socialDetail = $this->db->get('socialmedia')->result();
        return $socialDetail;
    }

    function getVenueSocialDetail($venueId){
        $this->db->select('*');
        $this->db->where('venueid', $venueId);
        $socialDetail = $this->db->get('socialmedia')->result();
        return $socialDetail;
    }

    function getAppSocialDetail($venueId){
        $this->db->select('*');
        $this->db->where('appid', $venueId);
        $socialDetail = $this->db->get('socialmedia')->result();
        return $socialDetail;
    }

    function getAttendeeDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $attendeeDetail = $this->db->get('attendees')->result();
        return $attendeeDetail;
    }

    function getArtists($id){
        $this->db->select('*');
        $this->db->where('appid', $id);
        $artistDetail = $this->db->get('artist')->result();
        return $artistDetail;
    }

    function getArtistDetail($appId){
        $this->db->select('*');
        $this->db->where('id', $appId);
        $artistDetail = $this->db->get('artist')->result();
        return $artistDetail;
    }

    function getVenueArtistDetail($venueId){
        $this->db->select('*');
        $this->db->where('venueid', $venueId);
        $artistDetail = $this->db->get('artist')->result();
        return $artistDetail;
    }

    function getScheduleDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $scheduleDetail = $this->db->get('schedule')->result();
        return $scheduleDetail;
    }

    function getVenueScheduleDetail($venueId){
        $this->db->select('*');
        $this->db->where('venueid', $venueId);
        $scheduleDetail = $this->db->get('schedule')->result();
        return $scheduleDetail;
    }

    function getTranslationDetail($id){
        $this->db->select('*');
        $this->db->where('id', $id);
        $translationDetail = $this->db->get('translation')->row();
        return $translationDetail;
    }

    function getSessionGroups($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $sessionGroups = $this->db->get('sessiongroup')->result();
        return $sessionGroups;
    }

    function getSessionGroupDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $sessionGroupDetail = $this->db->get('sessiongroup')->result();
        return $sessionGroupDetail;
    }

    function getAppVenues($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('appvenue')->result();

        $venues = array();

        foreach ($records as $rec) {
            $venues[] = $rec->venueid;
        }
        return ($venues);
    }

    function getAppNews($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('newsitem')->result();

        $venues = array();

        foreach ($records as $rec) {
            $venues[] = $rec->id;
        }
        return ($venues);
    }

    function getSubFlavors($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('appsubflavor')->result();

        $subflavors = array();

        foreach ($records as $rec) {
            $subflavors[] = $rec->subflavorid;
        }
        return ($subflavors);
    }

    function getAppApperance($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('appearance')->result();

        $apperance = array();

        foreach ($records as $rec) {
            $apperance['controlid'] = $rec->controlid;
            $apperance['value'] = $rec->value;
        }
        return ($records);
    }

    function getAppStores($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('appstores')->result();

        $stores = array();

        foreach ($records as $rec) {
            $stores['appid'] = $rec->appid;
            $stores['name'] = $rec->name;
            $stores['appleappstore'] = $rec->appleappstore;
            $stores['androidmarket'] = $rec->androidmarket;
            $stores['blackberry'] = $rec->blackberry;
            $stores['mobwebsite'] = $rec->mobwebsite;
        }
        return ($stores);
    }

    function getAppPushNotifications($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('push')->result();

        $notifications = array();

        foreach ($records as $rec) {
            $notifications['appid'] = $rec->appid;
            $notifications['token'] = $rec->name;
        }
        return ($records);
    }

    function getAppLanguages($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('applanguage')->result();

        $notifications = array();

        foreach ($records as $rec) {
            $notifications['appid'] = $rec->appid;
            $notifications['token'] = $rec->name;
        }
        return ($records);
    }

    function getAppTags($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getEventTags($id) {
        $this->db->select('*');
        $this->db->where('eventid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getSessionTags($id) {
        $this->db->select('*');
        $this->db->where('sessionid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }


    function getVenueTags($id) {
        $this->db->select('*');
        $this->db->where('venueid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getSponsorTags($id) {
        $this->db->select('*');
        $this->db->where('sponsorid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getExhibitorTags($id) {
        $this->db->select('*');
        $this->db->where('exhibitorid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getContentModuleTags($id) {
        $this->db->select('*');
        $this->db->where('contentmoduleid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getSectionTags($id) {
        $this->db->select('*');
        $this->db->where('sectionid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getSectionContentTags($id) {
        $this->db->select('*');
        $this->db->where('sectioncontentid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getNewsTags($id) {
        $this->db->select('*');
        $this->db->where('catalogitemid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getCatalogTags($id) {
        $this->db->select('*');
        $this->db->where('newsitemid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getSessionGroupTags($id) {
        $this->db->select('*');
        $this->db->where('sessiongroupid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getArtistTags($id) {
        $this->db->select('*');
        $this->db->where('artistid', $id);
        $records = $this->db->get('tag')->result();
        return ($records);
    }

    function getTranslation($table, $id) {
        $this->db->select('*');
        $this->db->where('tableid', $id);
        $this->db->where('table', $table);
        $records = $this->db->get('translation')->result();
        return ($records);
    }

    function getVenueSponsorGroup($id) {
        $this->db->select('*');
        $this->db->where('venueid', $id);
        $records = $this->db->get('sponsorgroup')->result();
        return ($records);
    }

    function getEventSponsorGroup($id) {
        $this->db->select('*');
        $this->db->where('eventid', $id);
        $records = $this->db->get('sponsorgroup')->result();
        return ($records);
    }

    function getLastSponsorGroupId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('sponsorgroup')->row();
        return $maxid->id;
    }

    function getVenueCatalogGroup($id) {
        $this->db->select('*');
        $this->db->where('venueid', $id);
        $records = $this->db->get('cataloggroup')->result();
        return ($records);
    }

    function getEventCatalogGroup($id) {
        $this->db->select('*');
        $this->db->where('eventid', $id);
        $records = $this->db->get('cataloggroup')->result();
        return ($records);
    }

    function getLastCatalogGroupId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('cataloggroup')->row();
        return $maxid->id;
    }

    function getEventExhibitorBrands($eventid){
        $this->db->select('*');
        $this->db->where('eventid', $eventid);
        $records = $this->db->get('exhibitorbrand')->result();
        return ($records);
    }


    function getSpeakersOfSession($sessionid) {
        $speakers = $this->db->query("SELECT speaker.* FROM speaker INNER JOIN speaker_session ON speaker.id = speaker_session.speakerid WHERE speaker_session.sessionid = $sessionid ")->result();
        return ($speakers);
    }

    function insertSpeaker($speaker, $newsessionid, $eventid) {
        $speakerData=array(
                    'eventid' => $eventid,
                    'sessionid' =>$newsessionid,
                    'order' => $speaker->order,
                    'name' => $speaker->name,
                    'description' => $speaker->description,
                    'imageurl' => $speaker->imageurl,
                    'company' => $speaker->company,
                    'function'  => $speaker->function
        );

        $speakerid = $this->general_model->insert('speaker', $speakerData);

        $this->general_model->insert('speaker_session', array('eventid' => $eventid, 'speakerid' => $speakerid, 'sessionid' => $newsessionid));
    }

    function getEventExhibitorCategory($eventid){
        $this->db->select('*');
        $this->db->where('eventid', $eventid);
        $records = $this->db->get('exhibitorcategory')->result();
        return ($records);
    }

    function getVenueExhibitorBrands($venueid){
        $this->db->select('*');
        $this->db->where('venueid', $venueid);
        $records = $this->db->get('exhibitorbrand')->result();
        return ($records);
    }

    function getEventExhibitorBrand($eventid){
        $this->db->select('*');
        $this->db->where('eventid', $eventid);
        $records = $this->db->get('exhibitorcategory')->result();
        return ($records);
    }

    function getExhibitorCategoryId($id) {
        $this->db->select('*');
        $this->db->where('exhibitorid', $id);
        $maxid = $this->db->get('exhicat')->result();
        return $maxid->id;
    }

    function getExhibitorCategoryById($id){
        $this->db->select('*');
        $this->db->where('id', $id);
        $records = $this->db->get('exhibitorcategory')->result();
        return ($records);
    }

    function getLastExhibitorCategoryId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('exhibitorcategory')->row();
        return $maxid->id;
    }
    function getExhibitorBrandId($id) {
        $this->db->select('*');
        $this->db->where('exhibitorid', $id);
        $maxid = $this->db->get('exhibrand')->result();
        return $maxid->id;
    }

    function getExhibitorBrandById($id){
        $this->db->select('*');
        $this->db->where('id', $id);
        $records = $this->db->get('exhibitorbrand')->result();
        return ($records);
    }

    function getLastExhibitorBrandId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('exhibitorbrand')->row();
        return $maxid->id;
    }

    function getGroupItem($id){
        $this->db->select('*');
        $this->db->where('itemid', $id);
        $groupItem = $this->db->get('groupitem')->result();
        return $groupItem;
    }

    function getGroupName($id){
        $this->db->select('name');
        $this->db->where('id', $id);
        $maxid = $this->db->get('group')->row();
        return $maxid->name;
    }

    function getOldParentName($id){
        $this->db->select('name');
        $this->db->where('id', $id);
        $maxid = $this->db->get('group')->row();
        return $maxid->name;
    }

    function getLastExhibitorGroupId($groupname){
        $this->db->select_max('id');
        $this->db->where('name', $groupname);
        $maxid = $this->db->get('group')->row();
        return $maxid->id;
    }

    function getGroupChilds($parentId){
        $this->db->select('*');
        $this->db->where('parentid', $parentId);
        $childItem = $this->db->get('group')->result();
        return $childItem;
    }

    function get_list($parent) {
        $this->db->select('*');
        $this->db->where('parentid', $parent);
        $children = $this->db->get('group')->result();
        return $children;
    }

    function getChildList($parent) {
        $this->db->select('*');
        $this->db->where('parentid', $parent);
        $children = $this->db->get('group')->result();
        return $children;
    }

    function getRssData($id){
        $this->db->select('*');
        $this->db->where('eventid', $id);
        $rss = $this->db->get('newssource')->result();
        return $rss;
    }

    function getVenueRssData($id){
        $this->db->select('*');
        $this->db->where('venueid', $id);
        $rss = $this->db->get('newssource')->result();
        return $rss;
    }

    function getAppRssData($id){
        $this->db->select('*');
        $this->db->where('appid', $id);
        $rss = $this->db->get('newssource')->result();
        return $rss;
    }

    function getContentModule($appId) {
        $this->db->select('*');
        $this->db->where('appid', $appId);
        $records = $this->db->get('contentmodule')->result();
        return ($records);
    }

    function getLastContentModuleId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('contentmodule')->row();
        return $maxid->id;
    }

    function getModuleSections($id) {
        $this->db->select('*');
        $this->db->where('contentmoduleid', $id);
        $records = $this->db->get('section')->result();
        return ($records);
    }

    function getLastSectionId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('section')->row();
        return $maxid->id;
    }

    function getSectionContent($id) {
        $this->db->select('*');
        $this->db->where('contentmoduleid', $id);
        $records = $this->db->get('sectioncontent')->result();
        return ($records);
    }

    function getLastSectionContentId() {
        $this->db->select_max('id');
        $maxid = $this->db->get('sectioncontent')->row();
        return $maxid->id;
    }

    function getFormDetail($eventId){
        $this->db->select('*');
        $this->db->where('eventid', $eventId);
        $formDetail = $this->db->get('form')->result();
        return $formDetail;
    }
	
	function getAccountId($clientId){
		$this->db->select('accountId');
		$this->db->where('clientId', $clientId);
		$res = $this->db->get('tc_accounts');
		if( $res->num_rows() > 0 ) return $res->row()->accountId;
		
		return FALSE;
	}
	
	function getCouponsByVenueId($venueid){
		$this->db->select('*');
		$this->db->where('venueid', $venueid);
		$res = $this->db->get('coupons');
		if( $res->num_rows() > 0 ) return $res->result();
		
		return FALSE;
	}
	
	function getLoyaltyByVenueId($venueid){
		$this->db->select('*');
		$this->db->where('venueid', $venueid);
		$res = $this->db->get('loyalty');
		if( $res->num_rows() > 0 ) return $res->result();
		
		return FALSE;
	}
}
