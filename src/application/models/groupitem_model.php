<?php if(!defined('BASEPATH')) exit('No direct script access');

class Groupitem_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}

	function getById($id) {
		$res = $this->db->query("SELECT * FROM `groupitem` WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) {
			return false;
		}
		return $res->row();
	}

	function removeGroupitemsOfObject($table, $id) {
		$this->db->where('itemtable', $table);
		$this->db->where('itemid', $id);
		$this->db->delete('groupitem'); 
	}

	function removeGroupitemsOfEvent($eventid) {
		$this->db->where('eventid', $eventid);
		$this->db->delete('groupitem'); 
	}

	function getByItemIdAndTypeAndGroup($itemid, $type, $groupid) {
		$res = $this->db->query("SELECT * FROM `groupitem` WHERE itemid = $itemid AND groupid = $groupid AND itemtable = '$type' LIMIT 1");
		if($res->num_rows() == 0) {
			return false;
		}
		return $res->row();
	}
}