<?php if(!defined('BASEPATH')) exit('No direct script access');

class Speaker_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getById($id) {
		$res = $this->db->query("SELECT * FROM speaker WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getByEventId($eventid) {
		$res = $this->db->query("SELECT * FROM speaker WHERE eventid = $eventid ORDER BY `order`DESC, name ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function insertSpeakers($speakers, $eventid, $sessionid) {
		//remove speakers of session
		$this->db->query("DELETE FROM speaker_session WHERE sessionid = $sessionid");

		foreach($speakers as $speaker) {
			//get speakerid if exists, else insert speaker
			$speakerexists = $this->db->query("SELECT id FROM speaker WHERE eventid = $eventid AND name = ? LIMIT 1", array($speaker));
			if($speakerexists->num_rows() == 0) {
				$speakerid = $this->general_model->insert('speaker', array(
						'eventid' => $eventid,
						'sessionid'	=> $sessionid,
						'name' => $speaker
					));
			} else {
				$speakerid = $speakerexists->row()->id;
			}

			//attach speaker to session
			$this->general_model->insert('speaker_session', array(
					'eventid' => $eventid,
					'speakerid' => $speakerid,
					'sessionid' => $sessionid
				));
		}
	}
	
	function getBySessionId($sessionid) {
		$res = $this->db->query("SELECT speaker.* FROM speaker INNER JOIN speaker_session ON speaker_session.speakerid = speaker.id WHERE speaker_session.sessionid = $sessionid ");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}

    function getMainCategorieGroup($eventid) {
		$res = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'speakercategories' LIMIT 1 ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row()->id;
    }

    var $htmlarray = array();
    var $groupids = '';
	function display_children($parent, $level, $eventid, $speakerid = 0) {  
	    // retrieve all children of $parent <br>  
	    $result = $this->db->query('SELECT * FROM `group` WHERE parentid = '.$parent.' ORDER BY id DESC;');  

	    // display each child <br>  
	    if($level == 0 && !empty($parent)) {
		    $parentname = $this->db->query("SELECT name FROM `group` WHERE id = $parent LIMIT 1");
		    if($parentname->num_rows() > 0) {
		    	$parentname = $parentname->row()->name;
			    $html = '<input type="checkbox" class="checkbox" name="groups[]" value="'.$parent.'" />&nbsp'.$parentname."<br />";
		    	if($speakerid != 0) {
		    		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $parent AND itemid = $speakerid AND itemtable = 'speaker' ");
		    		if($res->num_rows() != 0) {
		    			$html = '<input type="checkbox" class="checkbox" name="groups[]" value="'.$parent.'" checked>&nbsp'.$parentname."<br />";  
		    		}
		    	}
			    $this->htmlarray[] = $html;
			    $level++;
		    }
	    }
	    foreach($result->result() as $row) {  
	    	// indent and display the title of this child <br>
	    	$this->groupids .= $row->id . '/';
	    	$html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).'<input type="checkbox" class="checkbox" name="groups[]" value="'.$row->id.'" />&nbsp'.$row->name."<br />";  
	    	if($speakerid != 0) {
	    		$id = $row->id;
	    		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $id AND itemid = $speakerid AND itemtable = 'speaker' ");
	    		if($res->num_rows() != 0) {
	    			$html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).'<input type="checkbox" class="checkbox" name="groups[]" value="'.$row->id.'" checked>&nbsp'.$row->name."<br />";  
	    		}
	    	}
	        
	        $this->htmlarray[] = $html;
	        // call this function again to display this <br>  
	        // child's children <br>  
	        $this->display_children($row->id, $level+1, $eventid, $speakerid); 
	        
	    } 

	    return $this->htmlarray;
	} 

	function deleteGroupItemsFromSpeaker($id) {
		$this->db->where('itemtable', 'speaker');
		$this->db->where('itemid', $id);
		$this->db->delete('groupitem'); 
	}
}