<?php if(!defined('BASEPATH')) exit('No direct script access');

class Catalogbrands_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Catalogbrands_model() {
		parent::__construct();
	}
	
	function getCatalogbrandsByEventID($eventid) {
		$this->db->where('eventid', $eventid);
		$res = $this->db->get('catalogbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCatalogbrandsByVenueID($venueid) {
		$this->db->where('venueid', $venueid);
		$res = $this->db->get('catalogbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCatalogbrandByID($brandid) {
		$this->db->where('id', $brandid);
		$res = $this->db->get('catalogbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	// ### Webservice
	
	function getCatalogbrandsByEventIDService ($eventid) {
		$res = $this->db->query("SELECT * FROM catalogbrand WHERE eventid='$eventid' ORDER BY name asc");
		if($res->num_rows() == 0) return FALSE;
		$data = array();
		foreach($res->result() as $row) {
			$data += array($row->id => $row);
		}
		return $data;
	}
	
	function getCatalogbrandsByVenueIDService ($venueid) {
		$res = $this->db->query("SELECT * FROM catalogbrand WHERE venueid='$venueid' ORDER BY name asc");
		if($res->num_rows() == 0) return FALSE;
		$data = array();
		foreach($res->result() as $row) {
			$data += array($row->id => $row);
		}
		return $data;
	}
}