<?php
if (! defined('BASEPATH')) exit('No direct script access');

use \Tapcrowd\API;

class Translation_model extends CI_Model
{
	/**
	 * Returns the supported sources
	 *
	 * @return array
	 */
	public function getSourceTypes()
	{
		return array('cms', 'ios', 'android');
	}

	/**
	 * Returns the requested localization data
	 *
	 * @param array $arrLanguages
	 * @param string $source (cms, ios or android)
	 * @param array $checksum Optional array of checksums
	 * @return array
	 */
	public function getLocalization( $arrLanguages, $source = 'cms', $checksums = null )
	{
		if(!in_array($source, $this->getSourceTypes())) {
			throw new \InvalidArgumentException('Invalid source '.$source);
		}
		if(is_array($checksums) && !filter_var_array($checksums, FILTER_VALIDATE_INT)) {
			throw new \InvalidArgumentException('Invalid checkums');
		}

		$channelId = \Tapcrowd\API::getCurrentChannel()->channelId;

		$sql = "SELECT loc.id, loc.k as k, loc.source, ";

		# Languages
		$joins = '';
		$_queue_in = array();
		$join_sql = ' LEFT JOIN localization %s ON %1$s.language = "%1$s" AND %1$s.id = loc.id';
		$join_sql.= ' AND %1$s.source = loc.source AND %1$s.channel = loc.channel';
		foreach($arrLanguages as $l) {
			$sql .= sprintf(' %s.v AS %1$s, ', $l->language);
			$joins .= sprintf($join_sql, $l->language);
			$_queue_in[] = "'{$l->language}'";
		}

		$sql .= " 0 as lineCount FROM localization loc ". $joins;	# Add joins
		$sql .= " WHERE loc.id != '2147483647' ";			# Exlude default example strings
		$sql .= " AND loc.source = '$source' ";				# Set requested source
		$sql .= " AND loc.channel = " . $channelId . ' ';

		# Add optional checksums, locales
		$checksum_sql = '';
		if(is_array($checksums)) {
			$sql .= ' AND loc.id IN('. implode(',',$checksums) .') ';
			$checksum_sql = ' AND id IN('. implode(',',$checksums) .') ';
		}
		$locale_sql = empty($_queue_in)? '' : 'AND language IN(' . implode(', ',$_queue_in) . ')';
		$nulls_sql = empty($arrLanguages)? '' : implode(', ',array_fill(0,count($arrLanguages), 'NULL')) . ',';

		# Add Queue
		$queue_sql = 'UNION
			SELECT id, k, source, %s SUM(cnt) as lineCount FROM localization_queue queue
			WHERE id NOT IN(SELECT DISTINCT id FROM localization) AND source = "%s"
			%s %s AND channel = %d GROUP BY id ORDER BY lineCount DESC';
		$sql .= sprintf($queue_sql, $nulls_sql, $source, $locale_sql, $checksum_sql, $channelId);

		return $this->db->query($sql)->result();
	}

	/**
	 * Saves the given translation into the database
	 *
	 * @param string $source cms, ios or android
	 * @param int $checksum The checksum (crc32) of the translation
	 * @param array $translations A locale (or language) indexed array of translations
	 * @return void
	 * @throws InvalidArgumentException
	 */
	public function saveTranslation( $source, $checksum, Array $translations )
	{
		$localisations = $this->getLocalization( array(), $source, array($checksum) );
		if(empty($localisations)) {
			throw new \InvalidArgumentException('Could not find Translation');
		}

		# Gather data
		$key = $localisations[0]->k;
		$channelId = \Tapcrowd\API::getCurrentChannel()->channelId;

		# Remove data from queue (lineCount == 0 indicates non-queue origin)
		if($localisations[0]->lineCount != 0) {
			$this->db->query('DELETE FROM localization_queue WHERE id=?', $checksum);
		}

		# Insert / update into database
		$sql = 'INSERT INTO localization (id, channel, source, k, language, country, v) VALUES (%d,%d,"%s","%s",?,?,?)'.
			'ON DUPLICATE KEY UPDATE v=VALUES(v)';
		$sql = sprintf($sql, $checksum, $channelId, $source, $key);

		foreach($translations as $locale => $translation) {
			if($translation === null) continue;
			list($language, $country) = explode('_',$locale);
			if($country == null) $country = '';
			$r = $this->db->query($sql, array($language, $country, $translation));
		}
	}



// 	function getTranslationForKey($key) {
// 		$arrLanguages = $this->getLanguages();
// /*		SELECT nl.k, nl.v As nltext, en.v as entext, pt.v as pttext
// FROM localization nl
// LEFT JOIN localization en ON en.language = 'en' AND en.k = nl.k
// LEFT JOIN localization pt ON pt.language = 'pt' AND pt.k = nl.k
// WHERE nl.language = 'nl'*/
// 		$i = 0;
//
// 		$sqlStatement = "";
// 		foreach($arrLanguages as $lang) {
// 			if ($i == 0) {
// 				$sqlStatement = "SELECT loc.id, loc.k as blabla, loc.v as " . $lang->language . ", loc.country as " . $lang->language . "country, loc.channel as " . $lang->language . "channel, ";
// 			} else if ($i+1 == count($arrLanguages)) {
// 								$sqlStatement .= $lang->language . ".v as " . $lang->language . ", " . $lang->language . ".country as " . $lang->language . "country, " . $lang->language . ".channel as " . $lang->language ."channel ";
// 			} else {
// 				$sqlStatement .= $lang->language . ".v as " . $lang->language . ", " . $lang->language . ".country as " . $lang->language . "country, " . $lang->language . ".channel as " . $lang->language ."channel, ";
// 			}
// 			$i++;
// 		}
// 		$i = 0;
// 		$sqlStatement .= " FROM localization loc ";
// 		foreach ($arrLanguages as $lang) {
// 			if ($i == 0) {
// 				$sqlStatement .+ " FROM localization " . $lang->language;
// 			} else {
// 				$sqlStatement .= " LEFT JOIN localization " . $lang->language . " ON " . $lang->language . ".language = '" . $lang->language . "' AND " . $lang->language . ".id = loc.id";
// 			}
// 			$i++;
// 		}
// 		$sqlStatement .= " WHERE loc.language = '" . $arrLanguages[0]->language . "' And loc.id = '" . $key . "'";
// 		#add Queue
// 		$sqlStatement .= " UNION SELECT id, k, " . implode(', ',array_fill(0,count($arrLanguages)*3, 'NULL')) . " FROM localization_queue WHERE id = '" . $key . "'";
// 		//var_dump($sqlStatement);
// 		//exit();
// 		$result = $this->db->query($sqlStatement)->result();
// // var_dump($result);
// 		return $result;
// 	}


	function getLanguages() {
		return $this->db->query("SELECT DISTINCT language FROM localization")->result();
	}

	function import($fileName){

		if( ! $_FILES[$fileName]['tmp_name']) return FALSE;

		$channel 	= (int)\Tapcrowd\API::getCurrentChannel()->channelId;
		$country	= 'UK';
		$language 	= 'en';

		$file_type = $_FILES[$fileName]['type'];
		$tmpfile  = $_FILES[$fileName]['tmp_name'];
		$source = ($file_type == 'text/xml' || $file_type == 'application/xml') ? 'android' : 'ios';

		$rawContents = file_get_contents($tmpfile);
		$skipKeys = array('APPNAME', 'app_name', 'fb_appid', 'lang', 'basecallurl', 'baseimgurl', 'gmapskey', 'mobilewebsite');

		$sql = 'INSERT INTO localization (language, country, channel, source, k, v, id) VALUES (?,?,?,?,?,?,?)';

		if( $source == 'ios' ){
			$rawContents = preg_replace('!/\*.*?\*/!s', '', $rawContents); // Remove multiline comments
			$rawContents = preg_replace('/\n\s*\n/', "\n", $rawContents); // Remove empty lines
			$linesArray = explode(";\n", $rawContents);
			foreach($linesArray as $line){
				$line = str_replace('"', '', $line); // Removing ( " )
				$tmpArr = explode('=', $line);
				$key = trim($tmpArr[0]);
				$val = trim($tmpArr[1]);
				if( ! in_array($key, $skipKeys) ){
					$this->db->query($sql, array($language, $country, $channel, $source, $key, $val, crc32($key)));
				} // if( ! in_array($key, $skipKeys) )
			}
		}else{
			$this->load->library('simplexml');
            $xmlData = $this->simplexml->xml_parse($rawContents);

			foreach($xmlData['string'] as $row){
				$key =  $row['@attributes']['name'];
				$val = $row['@content'];
				if( ! in_array($key, $skipKeys) ){
					$this->db->query($sql, array($language, $country, $channel, $source, $key, $val, crc32($key)));
				} // if( ! in_array($key, $skipKeys) )
			} // foreach($xmlData['string'] as $row)
		}

		return TRUE;
	}

	function exportToZip($source){
		$source = ( trim($source) == 'ios' ) ? 'ios' : 'android';

		$channel 	= (int)\Tapcrowd\API::getCurrentChannel()->channelId;
		$qr = "SELECT en.k, en.v AS en, nl.v AS nl, fr.v AS fr, pt.v AS pt, tr.v AS tr
			FROM tapcrowd.localization en
			INNER JOIN tapcrowd.localization nl USING(id,source,channel)
			INNER JOIN tapcrowd.localization fr USING(id,source,channel)
			INNER JOIN tapcrowd.localization pt USING(id,source,channel)
			INNER JOIN tapcrowd.localization tr USING(id,source,channel)
			WHERE en.source = '" . $source . "' AND en.channel = " . $channel . " AND en.language = 'en'
			AND nl.language='nl' AND fr.language='fr' AND pt.language='pt' AND tr.language='tr'";
		echo $qr; exit;
		return $this->db->query($qr)->result();
	}

	/**
	 * Returns ArrayObject over all translation for given source
	 * with missing translations defaulted to en.
	 *
	 * This method is used for complete exports
	 *
	 * @param string $source (cms, ios or android)
	 * @return ArrayObject
	 */
	public function getTranslations( $source )
	{
		if(!in_array($source, array('cms', 'android', 'ios'))) {
			throw new \InvalidArgumentException('Invalid Source');
		}
		$translations = new ArrayObject();

		# Get languages
		$languages = array();
		foreach($this->getLanguages() as $l) $languages[] = $l->language;

		# Create defaults
		$sql = 'SELECT id AS crc, k, v FROM tapcrowd.localization WHERE source=? AND language="en"';
		$res = $this->db->query($sql, $source);
		$c = $res->num_rows();
		while($c--) {
			list($crc, $k, $v) = array_values($res->row_array($c));
			$translations[$crc] = (object)array_fill_keys($languages, $v);
			$translations[$crc]->k = $k;
			$translations[$crc]->crc = $crc;
		}

		# Overwrite translated strings
		$sql = 'SELECT id AS crc, CONCAT( language, "_", country) AS locale, language, v
			FROM tapcrowd.localization WHERE source=? AND language!="en"';
		$res = $this->db->query($sql, $source);
		$c = $res->num_rows();
		while($c--) {
			list($crc, $locale, $language, $v) = array_values($res->row_array($c));
			$translations[$crc]->$language = $v;
		}

		return $translations;
	}
}
