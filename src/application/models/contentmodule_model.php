<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Contentmodule_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getOfApp($appid) {
		$res = $this->db->query("SELECT * FROM contentmodule WHERE appid = $appid ORDER BY `order` ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function getById($id) {
		$res = $this->db->query("SELECT * FROM contentmodule WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		$row = $res->row();
		return $row;
	}

	function getSectionsOfModule($id) {
		$res = $this->db->query("SELECT * FROM section WHERE contentmoduleid = $id ORDER BY `order` ASC");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}

	function getSectionsOfModuleWithTypes($id) {
		$res = $this->db->query("SELECT section.*, sectiontype.name as sectiontype FROM section INNER JOIN sectiontype ON section.sectiontypeid = sectiontype.id WHERE contentmoduleid = $id ORDER BY `order` ASC");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}

	function getSectiontypes() {
		$res = $this->db->query("SELECT * FROM sectiontype");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}

	function getSectionById($id) {
		$res = $this->db->query("SELECT * FROM section WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		$row = $res->row();
		return $row;
	}

	function getSectionByIdWithSectionType($id) {
		$res = $this->db->query("SELECT section.*, sectiontype.name as sectiontype FROM section INNER JOIN sectiontype ON section.sectiontypeid = sectiontype.id  WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		$row = $res->row();
		return $row;
	}

	function getSectioncontentBySectionId($sectionid) {
		$res = $this->db->query("SELECT * FROM sectioncontent WHERE sectionid = $sectionid");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}

	function getSectioncontentById($id) {
		$res = $this->db->query("SELECT * FROM sectioncontent WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		$row = $res->row();
		return $row;
	}

	function getContentModules($appid) {
		$res = $this->db->query("SELECT * FROM contentmodule WHERE appid = $appid ORDER BY `order` ASC");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}

	function removeHomeModule($appid) {
		$this->db->query("UPDATE contentmodule SET homemodule = 0 WHERE homemodule = 1 AND appid = $appid");
	}

	function getSectionTags($sectionid) {
		$res = $this->db->query("SELECT id, tag FROM tag WHERE sectionid = $sectionid");
		if($res->num_rows() == 0) return false;
		return $res->result();
	}
        
	function urlExists($appid,$url) {
		$res = $this->db->query("SELECT * FROM newssource WHERE appid = $appid AND url LIKE '%$url%' LIMIT 1");
		return $res->num_rows();
	}
        
	function newsItemExists($appid,$url) {
		$res = $this->db->query("SELECT * FROM newsitem WHERE appid = $appid AND url = $url LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		$row = $res->row();
		return $row;
	}
        
}
