<?php if(!defined('BASEPATH')) exit('No direct script access');

class formtemplate_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Module_mdl() {
		parent::__construct();
	}
	
        function getFormTemplateIdByAppId($apptypeid, $appid, $ownerid, $ownertype = 'event'){
            $templateForms = array();
            
            $inClz = 'AND tf.id NOT IN (SELECT templateformid FROM launcher WHERE eventid = '.$ownerid.' AND appid ='.$appid.')';
            if($ownertype == 'venue')
                $inClz = 'AND tf.id NOT IN (SELECT templateformid FROM launcher WHERE venueid = '.$ownerid.' AND appid ='.$appid.')';
            elseif ($ownertype == 'city')
                    $inClz = 'AND tf.id NOT IN (SELECT templateformid FROM launcher WHERE appid ='.$appid.')';
            
            $templateForms = $this->db->query("SELECT tf.id, tf.title, s.name as package FROM templateform tf, templateformapp ta, subflavor s WHERE ta.apptypeid = ".$apptypeid." AND tf.id = ta.templateformid AND s.id = ta.subflavorid ".$inClz);
            
            return $templateForms->result();
        }
        
        function getFormTemplate($formid){
            $templateForms = array();
            $templateForms = $this->db->query("SELECT * FROM templateform WHERE moduletypeid = ".$formid);
            
            return $templateForms->result();
        }
        
        function getFormTemplateScreen($formid){
            $templateFormsScreens = array();
            $templateFormsScreens = $this->db->query("SELECT * FROM templateformscreen WHERE formid = ".$formid.' ORDER BY id ASC');
            
            return $templateFormsScreens->result();
        }
        
        function getFormTemplateControls($formid){
            $templateControls = array();
            $templateControls = $this->db->query("SELECT * FROM  templateformfield WHERE formscreenid IN(SELECT id FROM templateformscreen WHERE formid = ".$formid.")");
            
            return $templateControls->result();
        }
	
}