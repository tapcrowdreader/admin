<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Ad_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getAdsByEventID($eventid) {
		$res = $this->db->query("SELECT * FROM ad WHERE eventid = $eventid ORDER BY `order` DESC, name ASC");
		if($res->num_rows() == 0) return FALSE;
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = base_url() . $row->image;
			}
		}
		return $res->result();
	}
    
	function getAdsByVenueID($venueid) {
		$res = $this->db->query("SELECT * FROM ad WHERE venueid = $venueid ORDER BY `order` DESC, name ASC");
		if($res->num_rows() == 0) return FALSE;
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = base_url() . $row->image;
			}
		}
		return $res->result();
	}

	function getAdsByAppID($appid) {
		$res = $this->db->query("SELECT * FROM ad WHERE appid = $appid ORDER BY `order` DESC, name ASC");
		if($res->num_rows() == 0) return FALSE;
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = base_url() . $row->image;
			}
		}
		return $res->result();
	}
    
	function getAdById($id) {
		$res = $this->db->query("SELECT * FROM ad WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		$row = $res->row();
		//$row->image = base_url() . $row->image;
		return $row;
	}
}
