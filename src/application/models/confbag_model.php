<?php if(!defined('BASEPATH')) exit('No direct script access');

class Confbag_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getFromObject($table, $id) {
		$res = $this->db->query("SELECT * FROM confbag WHERE itemtable = '$table' AND tableid = $id ");
		if($res->num_rows() == 0) return false;
		return $res->result();
	}

	function checkActive($eventid, $type = 'event') {
		if($type == 'venue') {
			$res = $this->db->query("SELECT * FROM module WHERE event = $eventid AND moduletype = 42 ");
		} else {
			$res = $this->db->query("SELECT * FROM module WHERE event = $eventid AND moduletype = 42 ");
		}
		
		if($res->num_rows() == 0) return false;
		return $res->result();
	}

	function getById($id) {
		$res = $this->db->query("SELECT * FROM confbag WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return false;
		return $res->row();
	}
	
	function getConfbagdata($eventid, $type = 'event') {
		$typeid = $eventid;
		if($type == 'venue') {
			$res = $this->db->query(
			"SELECT * FROM (
			    SELECT p.email, 'catalog' as object, e.id, e.name, GROUP_CONCAT(c.documentlink) as documentlink, e.url as url, e.pdf as pdf, e.urltitle as urltitle FROM catalog e
			    INNER JOIN personal p ON `table`='catalog' AND e.id=p.tableid AND p.venueid = $eventid AND p.type = 'confbag'
			    LEFT JOIN confbag c ON c.itemtable='catalog' AND c.tableid = p.tableid
			    GROUP BY p.email, e.id
				UNION
			    SELECT p.email, 'other' as object, '' as id, '' as name, '' as firstname, '' as starttime, p.extra as documentlink, '' as url, '' as pdf, '' as urltitle FROM personal p WHERE p.venueid = $eventid AND p.type = 'confbag' AND p.table = 'other'
			    GROUP BY p.email
		    ) as t ORDER BY email, object, starttime");
		} else {
			// $res = $this->db->query(
			// "SELECT * FROM ( 
			// 	SELECT p.email, 'exhibitor' as object, e.id, e.name, '' as firstname, '' as starttime, GROUP_CONCAT(c.documentlink) as documentlink, '' as company, '' as function, '' as emailatt, '' as linkedin FROM exhibitor e INNER JOIN personal p ON `table`='exhibitors' AND e.id=p.tableid AND p.eventid = $eventid AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='exhibitor' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION 
			// 	SELECT p.email, 'session' as object, e.id, e.name, '' as firstname, e.starttime as starttime, GROUP_CONCAT(c.documentlink) as documentlink, '' as company, '' as function, '' as emailatt, '' as linkedin FROM session e INNER JOIN personal p ON `table`='sessions' AND e.id=p.tableid AND p.eventid = $eventid AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='session' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION 
			// 	SELECT p.email, 'attendee' as object, e.id, e.name, e.firstname, '' as starttime, GROUP_CONCAT(c.documentlink) as documentlink, e.company as company, e.function as function, e.email as emailatt, e.linkedin as linkedin FROM attendees e INNER JOIN personal p ON `table`='attendees' AND e.id=p.tableid AND p.eventid = $eventid AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='attendees' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION 
			// 	SELECT p.email, 'other' as object, '' as id, '' as name, '' as firstname, '' as starttime, p.extra as documentlink, '' as company, '' as function, '' as emailatt, '' as linkedin FROM personal p WHERE p.eventid = $eventid AND p.type = 'confbag' AND p.table = 'other' GROUP BY p.email ) as t ORDER BY email, object, starttime");

			$res = $this->db->query(
			"SELECT * FROM ( 
				SELECT p.email, 'exhibitor' as object, e.id, e.name, '' as firstname, '' as starttime, '' as endtime, GROUP_CONCAT(c.documentlink) as documentlink, '' as company, '' as function, e.email as emailatt, '' as linkedin, e.description as description, e.booth as booth, e.tel as phone, '' as country, e.address as address, '' as location, e.imageurl as imageurl, e.web as website FROM exhibitor e INNER JOIN personal p ON `table`='exhibitors' AND e.id=p.tableid AND p.eventid = $typeid AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='exhibitor' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION
				SELECT p.email, 'session' as object, e.id, e.name, '' as firstname, e.starttime as starttime, e.endtime as endtime, GROUP_CONCAT(c.documentlink) as documentlink, '' as company, '' as function, '' as emailatt, '' as linkedin, e.description as description, '' as booth, '' as phone, '' as country, '' as address, e.location as location, e.imageurl as imageurl, '' as website FROM session e INNER JOIN personal p ON `table`='sessions' AND e.id=p.tableid AND p.eventid = $typeid AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='session' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION 
				SELECT p.email, 'attendee' as object, e.id, e.name, e.firstname, '' as starttime, '' as endtime, GROUP_CONCAT(c.documentlink) as documentlink, e.company as company, e.function as function, e.email as emailatt, e.linkedin as linkedin, '' as description, '' as booth, e.phonenr as phone, e.country as country, '' as address, '' as location, e.imageurl as imageurl, '' as website FROM attendees e INNER JOIN personal p ON `table`='attendees' AND e.id=p.tableid AND p.eventid = $typeid AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='attendees' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION 
				SELECT p.email, 'speaker' as object, e.id, e.name, '' as firstname, '' as starttime, '' as endtime, GROUP_CONCAT(c.documentlink) as documentlink, e.company as company, e.function as function, '' as emailatt, '' as linkedin, e.description as description, '' as booth, '' as phone, '' as country, '' as address, '' as location, e.imageurl as imageurl, '' as website FROM speaker e INNER JOIN personal p ON `table`='speaker' AND e.id=p.tableid AND p.eventid = $typeid AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='speaker' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION
				SELECT p.email, 'other' as object, '' as id, '' as name, '' as firstname, '' as starttime, '' as endtime, GROUP_CONCAT(p.extra) as documentlink, '' as company, '' as function, '' as emailatt, '' as linkedin, '' as description, '' as booth, '' as phone, '' as country, '' as address, '' as location, '' as imageurl, '' as website FROM personal p WHERE p.eventid = $typeid AND p.type = 'confbag' AND p.table = 'other' GROUP BY p.email ) as t ORDER BY email, object, starttime");
		}

		if($res->num_rows() == 0) return false;
		return $res->result();
	}

	function getConfbagTestData($email, $typeid, $type = 'event') {
		$res = $this->db->query(
			"SELECT * FROM ( 
				SELECT p.email, 'exhibitor' as object, e.id, e.name, '' as firstname, '' as starttime, '' as endtime, GROUP_CONCAT(c.documentlink) as documentlink, '' as company, '' as function, e.email as emailatt, '' as linkedin, e.description as description, e.booth as booth, e.tel as phone, '' as country, e.address as address, '' as location, e.imageurl as imageurl, e.web as website FROM exhibitor e INNER JOIN personal p ON `table`='exhibitors' AND e.id=p.tableid AND p.eventid = $typeid AND p.email = '$email' AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='exhibitor' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION 
				SELECT p.email, 'session' as object, e.id, e.name, '' as firstname, e.starttime as starttime, e.endtime as endtime, GROUP_CONCAT(c.documentlink) as documentlink, '' as company, '' as function, '' as emailatt, '' as linkedin, e.description as description, '' as booth, '' as phone, '' as country, '' as address, e.location as location, e.imageurl as imageurl, '' as website FROM session e INNER JOIN personal p ON `table`='sessions' AND e.id=p.tableid AND p.eventid = $typeid AND p.email = '$email' AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='session' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION 
				SELECT p.email, 'attendee' as object, e.id, e.name, e.firstname, '' as starttime, '' as endtime, GROUP_CONCAT(c.documentlink) as documentlink, e.company as company, e.function as function, e.email as emailatt, e.linkedin as linkedin, '' as description, '' as booth, e.phonenr as phone, e.country as country, '' as address, '' as location, e.imageurl as imageurl, '' as website FROM attendees e INNER JOIN personal p ON `table`='attendees' AND e.id=p.tableid AND p.eventid = $typeid AND p.email = '$email' AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='attendees' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION 
				SELECT p.email, 'speaker' as object, e.id, e.name, '' as firstname, '' as starttime, '' as endtime, GROUP_CONCAT(c.documentlink) as documentlink, e.company as company, e.function as function, '' as emailatt, '' as linkedin, e.description as description, '' as booth, '' as phone, '' as country, '' as address, '' as location, e.imageurl as imageurl, '' as website FROM speaker e INNER JOIN personal p ON `table`='speaker' AND e.id=p.tableid AND p.eventid = $typeid AND p.email = '$email' AND p.type = 'confbag' LEFT JOIN confbag c ON c.itemtable='speaker' AND c.tableid = p.tableid GROUP BY p.email, e.id UNION
				SELECT p.email, 'other' as object, '' as id, '' as name, '' as firstname, '' as starttime, '' as endtime, GROUP_CONCAT(p.extra) as documentlink, '' as company, '' as function, '' as emailatt, '' as linkedin, '' as description, '' as booth, '' as phone, '' as country, '' as address, '' as location, '' as imageurl, '' as website FROM personal p WHERE p.eventid = $typeid AND p.email = '$email' AND p.type = 'confbag' AND p.table = 'other' GROUP BY p.email ) as t ORDER BY email, object, starttime");
		if($res->num_rows() == 0) return false;
		return $res->result();
	}

	function getMailData($appid) {
		$res = $this->db->query("SELECT * FROM confbagmail WHERE appid = $appid LIMIT 1");
		if($res->num_rows() == 0) return false;
		return $res->row();
	}
}