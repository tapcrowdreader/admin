<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Price_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getPriceForApp($app) {
		//check if price is defined for this app
		$channel = \Tapcrowd\API::getCurrentChannel();

		$subflavor = $this->module_mdl->calculateSubflavor($app);

		if($subflavor) {
			$apprice = $this->db->query("SELECT * FROM price WHERE appid = $app->id AND moduleid = 0 AND priceoptionid = 0 LIMIT 1");
			if($apprice->num_rows() > 0) {
				return $apprice->row();
			} else {
				$apprice = $this->db->query("SELECT * FROM price WHERE channelid = $channel->channelId AND apptypeid = $app->apptypeid AND subflavorid = $subflavor->id AND moduleid = 0 AND priceoptionid = 0 LIMIT 1");
				if($apprice->num_rows() > 0) {
					return $apprice->row();
				} else {
					$apprice = $this->db->query("SELECT * FROM price WHERE channelid = 1 AND apptypeid = $app->apptypeid AND subflavorid = $subflavor->id AND moduleid = 0 AND priceoptionid = 0 LIMIT 1");
					if($apprice->num_rows() > 0) {
						return $apprice->row();
					} 
				}
			}
		}else {
			return;
		}

				
	}

	function getOptions($app) {
		//check if price is defined for this app
		$options = $this->db->query("SELECT * FROM priceoption");
		if($options->num_rows() == 0) {
			return array();
		}

		$options = $options->result();
		$channel = \Tapcrowd\API::getCurrentChannel();

		foreach($options as $o) {
			$o->price_setup = 0;
			$o->price_permonth = 0;
			$price = $this->db->query("SELECT * FROM price WHERE priceoptionid = $o->id AND appid = $app->id LIMIT 1");
			if($price->num_rows() > 0) {
				$o->min_contract = $price->row()->minimumcontractduration;
				$o->price_id = $price->row()->id;
				$o->price_setup = $price->row()->price_setup;
				$o->price_permonth = $price->row()->price_permonth;
			} else {
				$price = $this->db->query("SELECT * FROM price WHERE channelid = $channel->channelId AND priceoptionid = $o->id AND apptypeid = $app->apptypeid LIMIT 1");
				if($price->num_rows() > 0) {
					$o->min_contract = $price->row()->minimumcontractduration;
					$o->price_id = $price->row()->id;
					$o->price_setup = $price->row()->price_setup;
					$o->price_permonth = $price->row()->price_permonth;
				} else {
					$price = $this->db->query("SELECT * FROM price WHERE channelid = 1 AND priceoptionid = $o->id AND apptypeid = $app->apptypeid LIMIT 1");
					if($price->num_rows() > 0) {
						$o->min_contract = $price->row()->minimumcontractduration;
						$o->price_id = $price->row()->id;
						$o->price_setup = $price->row()->price_setup;
						$o->price_permonth = $price->row()->price_permonth;
					} else {
						$price = $this->db->query("SELECT * FROM price WHERE priceoptionid = $o->id LIMIT 1");
						if($price->num_rows() > 0) {
							$o->min_contract = $price->row()->minimumcontractduration;
							$o->price_id = $price->row()->id;
							$o->price_setup = $price->row()->price_setup;
							$o->price_permonth = $price->row()->price_permonth;
						}
					}
				}
			}
		}
		return $options;
	}

	function getOptionPrice($optionid, $app) {
		$channel = \Tapcrowd\API::getCurrentChannel();
		$o = $this->db->query("SELECT * FROM priceoption WHERE id = $optionid")->row();
		$o->price_setup = 0;
		$o->price_permonth = 0;
		$price = $this->db->query("SELECT * FROM price WHERE priceoptionid = $o->id AND appid = $app->id LIMIT 1");
		if($price->num_rows() > 0) {
			$o->price_id = $price->row()->id;
			$o->price_setup = $price->row()->price_setup;
			$o->price_permonth = $price->row()->price_permonth;
		} else {
			$price = $this->db->query("SELECT * FROM price WHERE channelid = $channel->channelId AND priceoptionid = $o->id AND apptypeid = $app->apptypeid LIMIT 1");
			if($price->num_rows() > 0) {
				$o->price_id = $price->row()->id;
				$o->price_setup = $price->row()->price_setup;
				$o->price_permonth = $price->row()->price_permonth;
			} else {
				$price = $this->db->query("SELECT * FROM price WHERE channelid = 1 AND priceoptionid = $o->id AND apptypeid = $app->apptypeid LIMIT 1");
				if($price->num_rows() > 0) {
					$o->price_id = $price->row()->id;
					$o->price_setup = $price->row()->price_setup;
					$o->price_permonth = $price->row()->price_permonth;
				} else {
					$price = $this->db->query("SELECT * FROM price WHERE priceoptionid = $o->id LIMIT 1");
					if($price->num_rows() > 0) {
						$o->price_id = $price->row()->id;
						$o->price_setup = $price->row()->price_setup;
						$o->price_permonth = $price->row()->price_permonth;
					}
				}
			}
		}
		return $o;
	}

	function get_entry($id) {
		$res = $this->db->query("SELECT * FROM price WHERE id = $id LIMIT 1");
		return $res->row();
	}

	function addPriceOfApp($appid, $price, $priceid) {
		$res = $this->db->query("SELECT id FROM appprice WHERE appid = $appid LIMIT 1");
		if($res->num_rows() > 0) {
			$res = $res->row();
			$this->general_model->update("appprice", $res->id, array(
					'priceid' => $priceid,
					'amount_due' => $price
				));
		} else {
			$this->general_model->insert('appprice', array(
					'appid' => $appid,
					'priceid' => $priceid,
					'amount_due' => $price
				));	
		}
	}

	function getBalanceForApp($appid) {
		$res = $this->db->query("SELECT amount_due, amount_paid FROM appprice WHERE appid = $appid LIMIT 1");
		if($res->num_rows() > 0) {
			//Found
			return $res->row();
		} else {
			return false;
		}	
	}

	function getAppPaymentInfo($appid) {
		$eu = array(
			'AL', 'AD', 'AM', 'AT', 'BY', 'BE', 'BA', 'BG', 'CH', 'CY', 'CZ', 'DE',
			'DK', 'EE', 'ES', 'FO', 'FI', 'FR', 'GB', 'GE', 'GI', 'GR', 'HU', 'HR',
			'IE', 'IS', 'IT', 'LT', 'LU', 'LV', 'MC', 'MK', 'MT', 'NO', 'NL', 'PO',
			'PT', 'RO', 'RU', 'SE', 'SI', 'SK', 'SM', 'TR', 'UA', 'VA',
		);
		$res = $this->db->query("SELECT * FROM apppaymentinfo WHERE appid = $appid LIMIT 1");
		$result = $res->row();
		if($result->country == 'BE') {
			$result->vatAmount = 21;
		} elseif(in_array($result->country, $eu)) {
			if($result->vat == 1) {
				$result->vatAmount = 0;
			} else {
				$result->vatAmount = 21;
			}
		} else {
			$result->vatAmount = 0;
		}
		return $result;
	}

	function allowPayment() {
		$channel = \Tapcrowd\API::getCurrentChannel();

		return $this->db->query("SELECT allowpayment FROM channel WHERE id = $channel->channelId LIMIT 1")->row()->allowpayment;
	}
}
