<?php if(!defined('BASEPATH')) exit('No direct script access');

class Citycontent_model extends CI_Model {
	
	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Citycontent_model() {
		parent::__construct();
	}
	
	function add($title, $appid, $image, $text) {
		$data = array(
			'appid' => $appid,
			'image' => $image,
			'text' => $text,
			'title' => $title
		);
		$this->db->insert('citycontent', $data);
		return $this->db->insert_id();
		//No need to call updatetimestamp, allready set when creating this record
	}
	
	function edit($citycontentid, $title, $appid, $image, $text) {
		$data = array(
			'image' => $image,
			'text' => $text,
			'title' => $title
		);

		$this->db->where('id', $citycontentid);
		$this->db->update('citycontent', $data);
		//No need to call updatetimestamp, allready set when creating this record
	}
	
	function allFromApp($id) {
		$res = $this->db->query("SELECT * FROM citycontent WHERE appid = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getById($id) {
		$res = $this->db->query("SELECT * FROM citycontent WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
}

?>
