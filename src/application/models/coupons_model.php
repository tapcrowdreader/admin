<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Coupons_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getOfApp($appid) {
		$res = $this->db->query("SELECT * FROM coupons WHERE appid = $appid ORDER BY `order` ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function getById($id) {
		$res = $this->db->query("SELECT * FROM coupons WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		$row = $res->row();
		return $row;
	}
}