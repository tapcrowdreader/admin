<?php 

if (! defined('BASEPATH')) exit('No direct script access');

class Favourites_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Favourites_model() {
		parent::__construct();
	}
	
	function getFavouriteById($id) {
		$res = $this->db->query("SELECT * FROM favorites WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getFavouritesByEvent($eventid) {
		//$res = $this->db->query("SELECT DISTINCT f.useremail, f.id, f.eventid, IF(f.exhibitorid, e.name, '/') as ename, IF(f.sessionid, s.name, '/') as sname FROM favorites f, exhibitor e, session s WHERE f.eventid = $eventid AND (f.sessionid = s.id OR f.exhibitorid = e.id)");
		$res = $this->db->query("SELECT DISTINCT f.useremail, f.id, f.eventid, s.name as sname FROM favorites f, session s WHERE f.eventid = $eventid AND f.sessionid = s.id");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getUsersFavourites($eventid) {
		//$res = $this->db->query("SELECT DISTINCT f.useremail, f.id, f.eventid, IF(f.exhibitorid, e.name, '/') as ename, IF(f.sessionid, s.name, '/') as sname, IF(f.sessionid, sg.name, '/') as sgroupname, s.presentation as presentation FROM favorites f, exhibitor e, session s, sessiongroup sg WHERE f.eventid = $eventid AND ((f.sessionid = s.id AND s.sessiongroupid = sg.id) OR f.exhibitorid = e.id) AND f.notified = '0000-00-00 00:00:00' ORDER BY f.useremail ASC, sg.order ASC");
		$res = $this->db->query("SELECT DISTINCT f.useremail, f.id, f.eventid, f.extra, s.name as sname, sg.name as sgroupname, s.presentation as presentation FROM favorites f, session s LEFT JOIN sessiongroup sg ON (s.sessiongroupid = sg.id) WHERE f.eventid = $eventid AND f.sessionid = s.id AND f.notified = '0000-00-00 00:00:00' ORDER BY f.useremail ASC, sg.order ASC");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
	function getUsersFavouritesExtra($eventid, $useremail) {
		//$res = $this->db->query("SELECT DISTINCT f.useremail, f.id, f.eventid, IF(f.exhibitorid, e.name, '/') as ename, IF(f.sessionid, s.name, '/') as sname, IF(f.sessionid, sg.name, '/') as sgroupname, s.presentation as presentation FROM favorites f, exhibitor e, session s, sessiongroup sg WHERE f.eventid = $eventid AND ((f.sessionid = s.id AND s.sessiongroupid = sg.id) OR f.exhibitorid = e.id) AND f.notified = '0000-00-00 00:00:00' ORDER BY f.useremail ASC, sg.order ASC");
		$res = $this->db->query("SELECT DISTINCT f.useremail, f.id, f.eventid, f.extra FROM favorites f WHERE f.eventid = $eventid AND f.notified = '0000-00-00 00:00:00' AND f.useremail = '$useremail'");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
	function getEventFavouriteStats($eventid) {
		$totalfavs = $this->db->query("SELECT COUNT(*) as totaal FROM favorites WHERE eventid = $eventid AND notified = '0000-00-00 00:00:00'");
		$usertotals = $this->db->query("SELECT DISTINCT useremail, COUNT(*) as aantal FROM favorites WHERE eventid = $eventid AND notified = '0000-00-00 00:00:00' GROUP BY useremail ORDER BY useremail");
		
		$res->total = $totalfavs->result();
		$res->users = $usertotals->result();
		
		return $res;
	}
	
}