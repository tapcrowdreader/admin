<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Premium_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
    
	function get($table, $id) {
		$res = $this->db->query("SELECT * FROM premium WHERE tablename = '$table' AND tableid = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

	function save($postpremium, $table, $tableid, $extraline, $premium, $type, $typeid, $order = 0, $title = '') {
		if($postpremium) {
			if($premium == false) {
				$this->insert($table, $tableid, $extraline, $type, $typeid, $order, $title);
			} else {
				$this->update($premium->id, $extraline, $order, $title, $type, $typeid, $table);
			}
		} else {
			if($premium != false) {
				$this->remove($premium->id);
			}
		}
	}

	function insert($table, $id, $extraline, $type, $typeid, $order, $title) {
		$data = array(
				'tablename' => $table,
				'tableid' => $id,
				'extraline' => $extraline,
				'sortorder' => $order,
				'title' => $title
			);
		switch($type) {
			case 'event' : 
				$data['eventid'] = $typeid;
				break;
			case 'venue' :
				$data['venueid'] = $typeid;
				break;
			case 'app' :
				$data['appid'] = $typeid;
				break;
		}
		$this->general_model->insert('premium', $data);

		if(!empty($title)) {
			if($type == 'event') {
				$this->db->query("UPDATE premium SET title = '$title' WHERE tablename = '$table' AND eventid = $typeid");
			} elseif($type == 'venue') {
				$this->db->query("UPDATE premium SET title = '$title' WHERE tablename = '$table' AND venueid = $typeid");
			}
		}
	}

	function update($id, $extraline, $order, $title, $type, $typeid, $table) {
		$this->general_model->update('premium', $id, array('extraline' => $extraline, 'sortorder' => $order));

		if(!empty($title)) {
			if($type == 'event') {
				$this->db->query("UPDATE premium SET title = '$title' WHERE tablename = '$table' AND eventid = $typeid");
			} elseif($type == 'venue') {
				$this->db->query("UPDATE premium SET title = '$title' WHERE tablename = '$table' AND venueid = $typeid");
			}
		}
	}

	function remove($id) {
		$this->general_model->remove('premium', $id);
	}

	function removeMany($ids, $table) {
		if(!empty($ids) && !empty($table)) {
			$ids = implode(',', $ids);
			$this->db->query("DELETE FROM premium WHERE tableId IN ($ids) AND tablename = '$table'");
		}
	}

	function getTitle($table, $type, $typeid) {
		if($type == 'event') {
			$res = $this->db->query("SELECT title FROM premium WHERE tablename = '$table' AND eventid = $typeid LIMIT 1");
		} elseif($type == 'venue') {
			$res = $this->db->query("SELECT title FROM premium WHERE tablename = '$table' AND venueid = $typeid LIMIT 1");
		}

		if($res->num_rows() == 0) {
			return 'Premium';
		}

		return $res->row()->title;
	}
}
