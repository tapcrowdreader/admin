<?php if(!defined('BASEPATH')) exit('No direct script access');

class Forms_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Forms_model() {
		parent::__construct();
	}
	
	//Get forms for a event
	function getFormsByEventID($eventid) {
		$res = $this->db->query("SELECT f.* FROM form f WHERE f.eventid = $eventid ORDER BY f.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	//Get forms for a venue
	function getFormsByVenueID($venueid) {
		$res = $this->db->query("SELECT f.* FROM form f WHERE f.venueid = $venueid ORDER BY f.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	//Get forms fields for a form
	function getFormScreensByFormID($formid) {
		$res = $this->db->query("SELECT fs.* FROM formscreen fs WHERE fs.formid = $formid ORDER BY fs.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	//Get forms fields for a form
	function getFormFieldsByFormScreenID($formscreenid) {
		$res = $this->db->query("SELECT fl.* FROM formfield fl WHERE fl.formscreenid = $formscreenid ORDER BY fl.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}

	function getFormFieldsByForm($formid, $hiddenfields = true) {
		if(!$hiddenfields) {
			$res = $this->db->query("SELECT fl.* FROM formfield fl INNER JOIN formscreen fs ON fs.id = fl.formscreenid WHERE fs.formid = $formid AND fl.formfieldtypeid <> 16 ORDER BY fl.order ASC");
		} else {
			$res = $this->db->query("SELECT fl.* FROM formfield fl INNER JOIN formscreen fs ON fs.id = fl.formscreenid WHERE fs.formid = $formid ORDER BY fl.order ASC");
		}
		
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	//Get Field Max Order By Screen
	function getFormFieldsMaxOrderByFormScreenID($formscreenid) {
		$res = $this->db->query("SELECT MAX( fl.`order` ) AS `order` FROM formfield fl WHERE fl.formscreenid = $formscreenid");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	//Get forms field types
	function getFormsFieldTypes() {
		$res = $this->db->query("SELECT ft.* FROM formfieldtype ft ORDER BY ft.name ASC");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	//get form by id
	function getFormByLauncherID($id) {
		$res = $this->db->query("SELECT f.* FROM form f WHERE f.launcherid = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	//get form by id
	function getFormsByLauncherID($id) {
		$res = $this->db->query("SELECT f.* FROM form f WHERE f.launcherid = $id ");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}
	
	//get form by id
	function getFormByID($id) {
		$res = $this->db->query("SELECT f.* FROM form f WHERE f.id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	//get form by id
	function getFormScreenByID($id) {
		$res = $this->db->query("SELECT fs.* FROM formscreen fs WHERE fs.id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	//get form field by id
	function getFormFieldByID($id) {
		$res = $this->db->query("SELECT fl.* FROM formfield fl WHERE fl.id = $id LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	//get form field by screenid
	function getFormFieldByScreenID($id) {
                $this->db->select('*');
                $this->db->where('formscreenid', $id);
				$this->db->where('formfieldtypeid 	!=', 10);
				$this->db->order_by('order', 'ASC');
                $app = $this->db->get('formfield')->result();
                
                $data = array();
                
                foreach ($app as $key=>$rec) {
                    $data[$key] = $rec->label;
                }

                return $data;
	}

	function edit_form($id, $data) {
		$this->db->where('id', $id);
		if($this->db->update('form', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function edit_table($id, $data, $table) {
		$this->db->where('id', $id);
		if($this->db->update($table, $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
        
	function getFormSubmissions($formId){
		$res = $this->db->query("SELECT * FROM formsubmissionfield WHERE formsubmissionid = $formId");
		$records = $res->result();
		$formResult = array();

		foreach ($records as $key=>$rec) {
		    $formResult[$key] = $rec->value;
		}

		return ($formResult);                            
	}

	function getFormSubmissionsIds($formId){
		$this->db->select('*');
		$this->db->where('formid', $formId);
		$app = $this->db->get('formsubmission')->result();

		$formIds = array();
		foreach ($app as $rec) {
		    $formIds[] = $this->getFormSubmissions($rec->id);
		}

		return ($formIds);
	}     

	function getAppIdOfFormField($id) {
		$field = $this->getFormFieldByID($id);
		$screen = $this->getFormScreenByID($field->formscreenid);
		$form = $this->getFormByID($screen->formid);
		return $form->appid;
	}   

	function removeFieldsOfScreen($id) {
		$this->db->query("DELETE FROM formfield WHERE formscreenid = $id");
	}
	
	function getActiveForms($fieldName, $fieldValue, $active = 1, $moduletypeid = 44){
		$this->db->select('count(id) as id');
		$this->db->where($fieldName, $fieldValue);
		$this->db->where('moduletypeid', $moduletypeid);
		$this->db->where('active', $active);
		$row = $this->db->get('launcher')->row();
	    if(count( $row ) > 0)
			return $row->id;
		
		return FALSE;
	}

	function getFormSubmissionById($id) {
		$res = $this->db->query("SELECT * FROM formsubmission WHERE id = $id LIMIT 1");
		return $res->row();
	}

	function showSubmission($id) {
		$this->db->query("UPDATE formsubmission SET visible = 1 WHERE id = $id");
	}
	function hideSubmission($id) {
		$this->db->query("UPDATE formsubmission SET visible = 0 WHERE id = $id");
	}

	function getResultsOfScreen($screenid, $formid) {
		$submissions = array();
		$res = $this->db->query(
			"SELECT fs.id as formsubmissionid, fs.formid as formid, ff.id as formfieldid, ff.label as label, fsf.value as value, ff.possiblevalues as possiblevalues FROM formsubmission fs 
			INNER JOIN formsubmissionfield fsf ON fsf.formsubmissionid = fs.id
			INNER JOIN formfield ff ON ff.id = fsf.formfieldid
			WHERE formid = $formid AND ff.formscreenid = $screenid");
		foreach($res->result() as $r) {
			$formfieldoptions = $this->db->query("SELECT * FROM formfieldoption WHERE formfieldid = $r->formfieldid");
			if($formfieldoptions->num_rows() > 0) {
				$possiblevalues = array();
				foreach($formfieldoptions->result() as $option) {
					$possiblevalues[] = $option->value;
				}

				$r->possiblevalues = implode(',', $possiblevalues);
			}
			$submissions[$r->formfieldid][] = $r;
		}

		return $submissions;
	}

	function getResultsOfForm($formid, $hiddenfields = true) {
		$submissions = array();
		$res = $this->db->query(
			"SELECT fs.id as formsubmissionid, fs.formid as formid, ff.id as formfieldid, ff.label as label, ff.formfieldtypeid as formfieldtypeid, fsf.value as value, ff.possiblevalues as possiblevalues FROM formsubmission fs 
			INNER JOIN formsubmissionfield fsf ON fsf.formsubmissionid = fs.id
			INNER JOIN formfield ff ON ff.id = fsf.formfieldid
			WHERE fs.formid = $formid
			ORDER BY fs.id");
		foreach($res->result() as $r) {
			if($hiddenfields == false && $r->formfieldtypeid == 16) {

			} else {
				$submissions[$r->formsubmissionid][] = $r;
			}
			
		}

		return $submissions;
	}

	function getTopFormScreenByFormID($id, $order, $order_by = "`order` ASC") {
		if($order == 0)
			$res = $this->db->query("SELECT fs.* FROM formscreen fs WHERE fs.formid = $id AND visible = 1 ORDER BY $order_by LIMIT 0,1");
		else 
			$res = $this->db->query("SELECT fs.* FROM formscreen fs WHERE fs.formid = $id AND visible = 1 AND `order`>$order ORDER BY $order_by LIMIT 0,1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

    function getPageLayout($appid, $purpose) {
        $res = $this->db->query("SELECT * FROM customhtml WHERE appid = $appid AND purpose = '$purpose' LIMIT 1");
        if($res->num_rows() > 0) {
            return $res->row();
        }

        return array();
    }

    function getCustomColors($appid, $purpose) {
    	$res = $this->db->query("SELECT * FROM customhtml WHERE appid = $appid AND purpose = '$purpose' LIMIT 1");
    	if($res->num_rows() == 0) {
    		return array('#4BB2C5', '#EAA228', '#C5B47F', '#579575', '#839557', '#958C12', '#953579', '#4B5DE4', '#D8B83F', '#FF5800', '#0085CC', '#C747A3', '#CDDF54', '#FBD178', '#26B4E3', '#BD70C7'
							, '#4BB2C5', '#EAA228', '#C5B47F', '#579575', '#839557');
    	}

    	$colors = explode(',', $res->row()->colors);
    	return $colors;
    }
	
	function getFieldOptions($fieldid){
		$res = $this->db->query("SELECT id, formfieldid, value, isdefault, formflow_nextscreenid as formscreenid FROM formfieldoption WHERE formfieldid = $fieldid ORDER BY id ASC");
        
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getFieldOptionById($id){
		$res = $this->db->query("SELECT id, formfieldid, value, isdefault, formflow_nextscreenid as formscreenid FROM formfieldoption WHERE id = $id LIMIT 1");
        
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getFormScreensByFormIdExcludeOne($formid, $excludescreenid) {
		$res = $this->db->query("SELECT fs.* FROM formscreen fs WHERE fs.formid = $formid AND fs.id != $excludescreenid ORDER BY fs.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}

	function getFormsByAppId($appid, $extraparam) {
		$forms = $this->db->query("SELECT * FROM form WHERE appid = $appid");
		return $forms->result();
	}
}