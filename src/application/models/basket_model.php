<?php if(!defined('BASEPATH')) exit('No direct script access');

class Basket_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function active($venueid)
	{
		$res = false;
		
		if($this->db->query("SELECT * FROM basket WHERE venueid=$venueid")->row()->active == 1)
			$res = true;
		else
			$res = false;
		return $res;
	}
	
	function getModuleStatus($venueid, $module)
	{
		$array = (array)$this->db->query("SELECT * FROM basket WHERE venueid=$venueid")->row();
		$status = $array[$module."active"];
		
		return $status;
	}
	
	function getBasketId($venueid)
	{
		$result = $this->db->query("SELECT * FROM basket WHERE venueid=$venueid");
		if($result->num_rows > 0) {
			return $result->row()->id;
		}
		
		return false;
	}

	function getBasket($venueid)
	{
		$result = $this->db->query("SELECT * FROM basket WHERE venueid=$venueid");
		if($result->num_rows > 0) {
			return $result->row();
		}
		
		return false;
	}
	
	function getCode($venueid)
	{
		$res = NULL;
		
		$res = $this->db->query("SELECT codeactive, code FROM basket WHERE venueid=$venueid")->row();
		
		return $res;
	}
	
	function getTableOptions($id)
	{
		$result = (array)$this->db->query("SELECT * FROM table_options WHERE id=$id")->row();
		$arraykeys = array_keys($result);
		$array = array();
		
		foreach($arraykeys as $key)
		{
			$array[$key] = $result[$key];
		}
		
		return $array;
	}
	
	function setCode($venueid, $code)
	{
		$this->db->query("UPDATE basket SET code=$code WHERE venueid=$venueid");
		
		return 'ok';
	}
	
	function setTableOption($id, $type, $status)
	{
		$status = ($status == 0) ? 1 : 0;
		
		$ok = $this->db->query("UPDATE table_options SET $type=$status WHERE id=$id");
		
		return $ok;
	}
	
	function activateModule($venueid, $module)
	{
		switch($module)
		{
			case "code": $this->activateCode($venueid); break;
			case "table": $this->activateTableOptions($venueid); break;
		}
	}
	
	function deactivateModule($venueid, $module)
	{
		switch($module)
		{
			case "code": $this->deactivateCode($venueid); break;
			case "table": $this->deactivateTableOptions($venueid); break;
		}
	}
	
	function activateCode($venueid)
	{
		$row = $this->db->query("SELECT * FROM basket WHERE venueid=$venueid")->row();
		$codeint = $row->code;
		
		if($codeint == 0)
		{
			$codestr = ''.rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).'';
			$codeint = intval($codestr);
		}
		
		$this->db->query("UPDATE basket SET codeactive=1, code=$codeint WHERE venueid=$venueid");
	}
	
	function deactivateCode($venueid)
	{
		$this->db->query("UPDATE basket SET codeactive=0 WHERE venueid=$venueid");
	}
	
	
	function activateTableOptions($venueid)
	{
		$row = $this->db->query("SELECT * FROM basket WHERE venueid=$venueid")->row();
		$basketid = $row->id;
		$tableopts = $row->tableopts;
		
		$this->db->query("UPDATE basket SET tableactive=1 WHERE id=$basketid");
		if($this->db->query("SELECT * FROM table_options WHERE id=$basketid")->num_rows() == 0)
			$this->db->query("INSERT INTO table_options (id) VALUES ($basketid)");
	}
	
	function deactivateTableOptions($venueid)
	{
		$this->db->query("UPDATE basket SET tableactive=0 WHERE venueid=$venueid");
	}

	function apichange($api, $externalvenueid, $venueid) {
		$res = $this->db->query("SELECT id FROM basket WHERE venueid = $venueid LIMIT 1");
		if($res->num_rows > 0) {
			$this->general_model->update('basket', $res->row()->id, array('send_order_to_api' => $api, 'externalvenueid' => $externalvenueid));
		} else {
			$this->general_model->insert('basket', array('send_order_to_api' => $api, 'externalvenueid' => $externalvenueid, 'venueid' => $venueid, 'active' => 1));
		}
	}

	function optionschange($orderattable, $ordertakeaway, $orderreservation, $venueid, $popupperorder, $popupperitem) {
		if($orderattable !== false) {
			$orderattable = 1;
		} else {
			$orderattable = 0;
		}

		if($ordertakeaway !== false) {
			$ordertakeaway = 1;
		} else {
			$ordertakeaway = 0;
		}

		if($orderreservation !== false) {
			$orderreservation = 1;
		} else {
			$orderreservation = 0;
		}

		if($popupperorder !== false) {
			$popupperorder = 1;
		} else {
			$popupperorder = 0;
		}

		if($popupperitem !== false) {
			$popupperitem = 1;
		} else {
			$popupperitem = 0;
		}
		$res = $this->db->query("SELECT id FROM basket WHERE venueid = $venueid LIMIT 1");
		if($res->num_rows > 0) {
			$this->general_model->update('basket', $res->row()->id, array('orderattable' => $orderattable, 'ordertakeaway' => $ordertakeaway, 'orderreservation' => $orderreservation, 'popupperorder' => $popupperorder, 'popupperitem' => $popupperitem));
		} else {
			$this->general_model->insert('basket', array('orderattable' => $orderattable, 'ordertakeaway' => $ordertakeaway, 'orderreservation' => $orderreservation, 'venueid' => $venueid, 'active' => 1, 'popupperorder' => $popupperorder, 'popupperitem' => $popupperitem));
		}
	}
}