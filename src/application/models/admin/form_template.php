<?php if(!defined('BASEPATH')) exit('No direct script access');

class Form_template extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Form_template() {
		parent::__construct();
	}
	
	//Get App Types
	function getAppTypes() {
		$res = $this->db->query( ' SELECT * FROM apptype WHERE id != 9 AND title != \'\' ' );

		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

    // Get Sub Flavors
	function getSubFlavorsByAppTypeId($appTypeId) {
		$this->db->select('id, name');
		$this->db->where('apptypeid', $appTypeId);
		$this->db->order_by("name", "asc");
		$res = $this->db->get('subflavor');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

    // Get Form Templates
	function getFormTemplates($formId = 0){
		if($formId)
			$this->db->where("id", $formId);

		$this->db->order_by("id", "desc");
		$res = $this->db->get('templateform');

		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function getFormTemplateApps($formId){
		$rtnArray = array();
		$sql = "SELECT apptypeid, subflavorid FROM templateformapp WHERE templateformid=". $formId;
		$qry = $this->db->query($sql);
		if ($qry->num_rows() > 0)
			foreach ($qry->result() as $row){
				$rtnArray[$row->apptypeid]= $row->subflavorid;
			}

		return $rtnArray;
	}

    //get form sreens by from id
	function getFormScreenByID($id) {
		$res = $this->db->query("SELECT fs.* FROM templateformscreen fs WHERE fs.id = $id ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

    //get form by id
	function getFormByID($id) {
		$res = $this->db->query("SELECT f.* from templateform f WHERE f.id = $id ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

   //Get forms fields for a form
	function getFormFieldsByFormScreenID($formscreenid) {
		$res = $this->db->query("SELECT fl.* FROM templateformfield fl WHERE fl.formscreenid = $formscreenid ORDER BY fl.order ASC");
		if($res->num_rows() == 0) return FALSE;

		return $res->result();
	}

    //Get Field Max Order By Screen
	function getFormFieldsMaxOrderByFormScreenID($formscreenid) {
		$res = $this->db->query("SELECT MAX( fl.`order` ) AS `order` FROM templateformfield fl WHERE fl.formscreenid = $formscreenid");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

    //get form field by id
	function getFormFieldByID($id) {
		$res = $this->db->query("SELECT fl.* FROM templateformfield fl WHERE fl.id = $id ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

    //Get form screens for a form
	function getFormScreensByFormID($formid) {
		$res = $this->db->query("SELECT fs.* FROM templateformscreen fs WHERE fs.formid = $formid ORDER BY fs.order ASC");
		if($res->num_rows() == 0) return FALSE;

		return $res->result();
	}

	function removeFormTemplate($id) {
		$templateform = $this->db->query("SELECT * FROM templateform WHERE id = $id LIMIT 1")->row();
		$this->db->query("DELETE FROM templateform WHERE id = $id");
		$this->db->query("DELETE FROM templateformapp WHERE templateformid = $id");
		$screenidsResult = $this->db->query("SELECT * FROM templateformscreen WHERE formid = $id")->result();
		$screenids = array();
		foreach($screenidsResult as $s) {
			
		}
		if(!empty($screenids)) {
			$screenids = implode(',', $screenids);
			$this->db->query("DELETE FROM templateformfield WHERE formscreenid IN ($screenids)");
			$this->db->query("DELETE FROM templateformscreen WHERE formid = $id");
		}

		if($templateform->moduletypeid != 0) {
			$this->db->query("DELETE FROM moduletype WHERE id = $templateform->moduletypeid");
			$this->db->query("DELETE FROM eventtypemoduletype WHERE moduletype = $templateform->moduletypeid");
			$this->db->query("DELETE FROM moduletype_subflavor WHERE moduletypeid = $templateform->moduletypeid");
			$this->db->query("DELETE FROM defaultlauncher WHERE moduletypeid = $templateform->moduletypeid");
			$this->db->query("DELETE FROM themeicon WHERE moduletypeid = $templateform->moduletypeid");

			//remove module from existing apps? otherwise when new moduletypeid is added, id will be the same, different module
			$this->db->query("DELETE FROM module WHERE moduletypeid = $templateform->moduletypeid");
			$this->db->query("DELETE FROM launcher WHERE moduletypeid = $templateform->moduletypeid");
		}
		
	}
}