<?php if(!defined('BASEPATH')) exit('No direct script access');

class Module_template extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getModuleTemplates() {
		return $this->db->query("SELECT moduletype.*, defaultlauncher.*, moduletype.id as id FROM moduletype INNER JOIN defaultlauncher ON moduletype.id = defaultlauncher.moduletypeid WHERE moduletype.template = 1")->result();
	}

	function getModuleTemplateApps($moduletypeid){
		$rtnArray = array();
		$sql = "SELECT subflavor.apptypeid, moduletype_subflavor.subflavorid FROM moduletype_subflavor INNER JOIN subflavor ON subflavor.id = moduletype_subflavor.subflavorid WHERE moduletype_subflavor.moduletypeid=". $moduletypeid;
		$qry = $this->db->query($sql);
		if ($qry->num_rows() > 0)
			foreach ($qry->result() as $row){
				$rtnArray[$row->apptypeid]= $row->subflavorid;
			}

		return $rtnArray;
	}

	function getModule($id) {
		$res = $this->db->query("SELECT moduletype.*, defaultlauncher.*, moduletype.id as id FROM moduletype INNER JOIN defaultlauncher ON defaultlauncher.moduletypeid = moduletype.id WHERE moduletype.id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

	function removeModuleTemplate($id) {
		if(!empty($id) && is_numeric($id)) {
			$this->db->query("DELETE FROM moduletype WHERE id = $id");
			$this->db->query("DELETE FROM moduletype_subflavor WHERE moduletypeid = $id");
			$this->db->query("DELETE FROM defaultlauncher WHERE moduletypeid = $id");
			$this->db->query("DELETE FROM moduletype_channel WHERE moduletypeid = $id");
		}
	}

	function getChannels() {
		return $this->db->query("SELECT * FROM channel")->result();
	}

	function getChannelidsOfModule($id) {
		$channelids = array();
		$res = $this->db->query("SELECT channelid FROM moduletype_channel WHERE moduletypeid = $id")->result();
		foreach($res as $row) {
			$channelids[] = $row->channelid;
		}

		return $channelids;
	}

	function addModuleToChannels($channelids, $moduletypeid) {
		foreach($channelids as $channelid) {
			$exists = $this->db->query("SELECT id FROM moduletype_channel WHERE moduletypeid = $moduletypeid AND channelid = $channelid LIMIT 1");
			if($exists->num_rows() == 0) {
				$this->general_model->insert('moduletype_channel', array('moduletypeid' => $moduletypeid, 'channelid' => $channelid));
			}
		}
	}
}