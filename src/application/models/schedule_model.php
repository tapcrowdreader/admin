<?php if(!defined('BASEPATH')) exit('No direct script access');

class Schedule_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Schedule_model() {
		parent::__construct();
	}
	
	function getScheduleByEvent($eventid) {
		$this->db->where('eventid', $eventid);
		$res = $this->db->get('schedule');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getScheduleByVenue($venueid) {
		$this->db->where('venueid', $venueid);
		$res = $this->db->get('schedule');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function getById($id) {
		$this->db->where('id', $id);
		$res = $this->db->get('schedule');
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
}