<?php if(!defined('BASEPATH')) exit('No direct script access');

class Exhibitor_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Exhibitor_model() {
		parent::__construct();
	}
	
	function getExhibitorByID($exhid) {
		$this->db->where('id', $exhid);
		$res = $this->db->get('exhibitor');
		if($res->num_rows() == 0) return FALSE;
		$exhibitor = $res->row();
		
		// get brands
		$brands = $this->db->query("SELECT eb.id FROM exhibrand b, exhibitorbrand eb WHERE b.exhibitorid = $exhid AND eb.id = b.exhibitorbrandid");
		$brandids = array();
		foreach ($brands->result() as $bitem) {
			array_push($brandids, $bitem->id);
		}
		$exhibitor->brands = $brandids;
		
		// get categories
		$categories = $this->db->query("SELECT ec.id FROM exhicat c, exhibitorcategory ec WHERE c.exhibitorid = $exhid AND ec.id = c.exhibitorcategoryid");
		$catids = array();
		foreach ($categories->result() as $citem) {
			array_push($catids, $citem->id);
		}
		$exhibitor->categories = $catids;
		
		return $exhibitor;
	}

	function getById($id) {
		$this->db->where('id', $id);
		$res = $this->db->get('exhibitor');
		if($res->num_rows() == 0) return FALSE;
		$exhibitor = $res->row();
		return $exhibitor;
	}
	
	function getExhibitorsByEventID($eventid) {
		$this->db->where('eventid', $eventid);
		$this->db->order_by('order', "desc");
		$this->db->order_by('name', "asc");
		$res = $this->db->get('exhibitor');
		if($res->num_rows() == 0) return FALSE;
		foreach ($res->result() as $row) {
			$row->name = htmlspecialchars_decode($row->name, ENT_NOQUOTES);
		}
		return $res->result();
	}
	
	function getExhibitorsByVenueID($venueid) {
		$this->db->where('venueid', $venueid);
		$this->db->order_by('name', "asc");
		$res = $this->db->get('exhibitor');
		if($res->num_rows() == 0) return FALSE;
		foreach ($res->result() as $row) {
			$row->name = htmlspecialchars_decode($row->name, ENT_NOQUOTES);
		}
		return $res->result();
	}
	
	function getBrandsByEventID($eventid) {
		$this->db->where('eventid', $eventid);
		$this->db->order_by('name', 'ASC');
		$res = $this->db->get('exhibitorbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCategoriesByEventID($eventid) {
		$this->db->where('eventid', $eventid);
		$this->db->order_by('name', 'ASC');
		$res = $this->db->get('exhibitorcategory');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getBrandsByVenueID($venueid) {
		$this->db->where('venueid', $venueid);
		$this->db->order_by('name', 'ASC');
		$res = $this->db->get('exhibitorbrand');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getCategoriesByVenueID($venueid) {
		$this->db->where('venueid', $venueid);
		$this->db->order_by('name', 'ASC');
		$res = $this->db->get('exhibitorcategory');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function saveBrands($exhid, $brands) {
		if($this->db->delete('exhibrand', array('exhibitorid' => $exhid))){
			if(is_array($brands) && count($brands) > 0){
				foreach($brands as $brand) {
					$this->db->insert('exhibrand', array('exhibitorid' => $exhid, 'exhibitorbrandid' => $brand));
				}
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function saveCategories($exhid, $categories) {
		if($this->db->delete('exhicat', array('exhibitorid' => $exhid))){
			if(is_array($categories) && count($categories) > 0){
				foreach($categories as $category) {
					$this->db->insert('exhicat', array('exhibitorid' => $exhid, 'exhibitorcategoryid' => $category));
				}
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function getUnmappedByEvent($eventid) {
		$this->db->where('x1', 0);
		$this->db->where('y1', 0);
		$this->db->where('x2', 0);
		$this->db->where('y2', 0);
		$this->db->where('eventid', $eventid);
		$this->db->order_by('booth');
		$res = $this->db->get('exhibitor');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getUnmappedByVenue($venueid) {
		$this->db->where('x1', 0);
		$this->db->where('y1', 0);
		$this->db->where('x2', 0);
		$this->db->where('y2', 0);
		$this->db->where('venueid', $venueid);
		$this->db->order_by('booth');
		$res = $this->db->get('exhibitor');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getExhibitorBrands($exhibitorid) {
		$res = $this->db->query("SELECT eb.*, br.name FROM exhibrand eb LEFT JOIN exhibitorbrand br ON eb.exhibitorbrandid = br.id WHERE eb.exhibitorid='$exhibitorid' ORDER BY br.name asc");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getExhibitorCats($exhibitorid) {
		$res = $this->db->query("SELECT eb.*, br.name FROM exhicat eb LEFT JOIN exhibitorcategory br ON eb.exhibitorcategoryid = br.id WHERE eb.exhibitorid='$exhibitorid' ORDER BY br.name asc");
		if($res->num_rows()  == 0) return FALSE;
		return $res->result();
    }

    function getMainCategorieGroup($eventid) {
		$res = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'exhibitorcategories' LIMIT 1 ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row()->id;
    }

    function getMainBrandGroup($eventid) {
		$res = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'exhibitorbrands' LIMIT 1 ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row()->id;
    }

    function getMainCatGroup($eventid) {
		$res = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'exhibitorcategories' LIMIT 1 ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row()->id;
    }

    function removeExhibitorsOfGroup($groupid) {
		$res = $this->db->query("SELECT itemid FROM groupitem WHERE groupid = $groupid AND itemtable = 'exhibitor' ");
		if($res->num_rows() == 0) return FALSE;
		$ids = array();
		foreach($res->result() as $row) {
			$ids[] = $row->itemid;
		}
		$ids = implode(',',$ids);
		$this->db->query("DELETE FROM exhibitor WHERE id IN ($ids)");
    }

  //   function getBrandGroups($eventid) {
  //   	$mainbrandid = $this->getMainBrandGroup($eventid);
  //   	$value = '0/'.$mainbrandid;
		// $res = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND tree LIKE '$value%' ");
		// if($res->num_rows() == 0) return FALSE;

		// $children = $res->result();

		// $array = array($mainbrandid => array());
		// foreach($children as $child) {
		// 	if($child->parentid == $mainbrandid) {
		// 		$array[$mainbrandid][$child->id] = array(); 
		// 	} else {
		// 		$treeArray = explode('/', $child->tree);
		// 		$len = count($treeArray) - 1;

		// 		$array[$treeArray[2]][$child->id] = array();
		// 	}
		// }

		// return $array;
  //   }

  	//for new system of brands and catalogs in groups with groupitems
    var $htmlarray = array();
    var $groupids = '';
	function display_children($parent, $level, $eventid, $exid = 0) {  
	    // retrieve all children of $parent <br>  
	    $result = $this->db->query('SELECT * FROM `group` WHERE parentid = '.$parent.' ORDER BY id DESC;');  

	    // display each child <br>  
	    foreach($result->result() as $row) {  
	    	// indent and display the title of this child <br>

	    	$html = '<option name="brands[]" value="'.$row->id.'">&nbsp' . str_repeat('--',$level).'&nbsp'.$row->name."\n".'</option>';  
	    	if($exid != 0) {
	    		$id = $row->id;
	    		$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $id AND itemid = $exid AND itemtable = 'exhibitor' ");
	    		if($res->num_rows() != 0) {
	    			$this->groupids .= $row->id . '/';
	    			$html = '<option name="brands[]" selected="checked" value="'.$row->id.'">&nbsp' . str_repeat('--',$level).'&nbsp'.$row->name."\n".'</option>';  
	    		}
	    	}
	          
	        
	        $this->htmlarray[] = $html;
	        // call this function again to display this <br>  
	        // child's children <br>  
	        $this->display_children($row->id, $level+1, $eventid, $exid); 
	        
	    } 

	    return $this->htmlarray;
	} 

	function deleteGroupItemsFromExhibitor($exid) {
		$this->db->where('itemtable', 'exhibitor');
		$this->db->where('itemid', $exid);
		$this->db->delete('groupitem'); 
	}

	function removePremium($id) {
		if(is_numeric($id)) {
			$this->db->query("DELETE FROM premium WHERE tablename = 'exhibitor' AND tableId  = $id");
		}
	}

	function getPremiumExhibitorsByEventID($eventid) {
		$res = $this->db->query("SELECT e.* FROM exhibitor e INNER JOIN premium p ON p.tableId = e.id WHERE e.eventid = $eventid AND p.tablename = 'exhibitor' ORDER BY p.sortorder");
		return $res->result();
	}
	
	function getPlacesMainCategorieGroup($typeid, $type='event') {
		$whrClzCol = ( $type == 'event' ) ? 'eventid' : 'venueid';
		
		$res = $this->db->query("SELECT * FROM `group` WHERE $whrClzCol = $typeid AND name = 'placescategories' LIMIT 1 ");
		if($res->num_rows() == 0) return FALSE;
		return $res->row()->id;
    }
	
}