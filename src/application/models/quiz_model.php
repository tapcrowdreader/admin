<?php if(!defined('BASEPATH')) exit('No direct script access');

class Quiz_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}

	function getByLauncherId($launcherid) {
		$res = $this->db->query("SELECT * FROM quiz WHERE launcherid = $launcherid LIMIT 1");
		if($res->num_rows() == 0) return false;
		return $res->row();
	}

	function getById($id) {
		$res = $this->db->query("SELECT * FROM quiz WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) return false;
		return $res->row();
	}

	function saveQuizData($appid, $launcherid, $quiz, $numberofquestionspergame, $facebooksharescore, $defaultscorepercorrectanswer, $sendresultsbyemail_formid) {
		if($facebooksharescore == 'yes') {
			$facebooksharescore = 1;
		} else {
			$facebooksharescore = 0;
		}
		if($quiz) {
			$this->general_model->update('quiz', $quiz->id, array(
					'numberofquestionspergame' => $numberofquestionspergame,
					'defaultscorepercorrectanswer' => $defaultscorepercorrectanswer,
					'facebooksharescore' => $facebooksharescore,
					'sendresultsbyemail_formid' => $sendresultsbyemail_formid
				));
		} else {
			$this->general_model->insert('quiz', array(
					'appid' => $appid,
					'launcherid' => $launcherid,
					'numberofquestionspergame' => $numberofquestionspergame,
					'defaultscorepercorrectanswer' => $defaultscorepercorrectanswer,
					'facebooksharescore' => $facebooksharescore,
					'sendresultsbyemail_formid' => $sendresultsbyemail_formid
				));
		}
	}

	function checkModule($typeid, $type, $app) {
		$column = $type.'id';
		$launcher = $this->db->query("SELECT id FROM launcher WHERE $column = $typeid AND moduletypeid = 69 LIMIT 1");
		if($launcher->num_rows() > 0) {
			$launcher = $launcher->row();
			$res = $this->db->query("SELECT id FROM quiz WHERE launcherid = $launcher->id LIMIT 1");
			if($res->num_rows() == 0) {
				$this->general_model->insert('quiz', array(
						'appid' => $app->id,
						'launcherid' => $launcher->id,
						'numberofquestionspergame' => 10,
						'defaultscorepercorrectanswer' => 10
					));
			}
		}
	}

	function getQuestionTypes($quizid) {
		$questiontypes = $this->db->query("SELECT * FROM quizquestiontype")->result();
		return $questiontypes;
	}

	function getQuestionsOfQuiz($quizid) {
		$res = $this->db->query("SELECT * FROM quizquestion WHERE quizid = $quizid");
		return $res->result();
	}

	function getQuestionById($questionid) {
		$res = $this->db->query("SELECT * FROM quizquestion WHERE id = $questionid LIMIT 1");
		if($res->num_rows() == 0) {
			return false;
		}

		return $res->row();
	}

	function getQuestionOptions($questionid) {
		$res = $this->db->query("SELECT * FROM quizquestionoption WHERE quizquestionid = $questionid");
		return $res->result();
	}

	function getQuestionOptionById($optionid) {
		$res = $this->db->query("SELECT * FROM quizquestionoption WHERE id = $optionid LIMIT 1");
		if($res->num_rows() == 0) {
			return false;
		}

		return $res->row();
	}

	function addOptionsToQuestion($questionid, $quizid) {
		$this->db->query("UPDATE quizquestionoption SET quizquestionid = $questionid WHERE quizquestionid = 0 AND quizid = $quizid");
	}

	function getReviewsOfQuiz($quizid) {
		$res = $this->db->query("SELECT * FROM quizreview WHERE quizid = $quizid")->result();
		foreach($res as $review) {
			$review->title = substr($review->reviewtext, 0, 50);
		}

		return $res;
	}

	function getReviewById($id) {
		$res = $this->db->query("SELECT * FROM quizreview WHERE id = $id LIMIT 1");
		if($res->num_rows() == 0) {
			return false;
		}

		return $res->row();
	}

	function getSubmissionsOfQuiz($quizid) {
		$res = $this->db->query("SELECT * FROM quizsubmission WHERE quizid = $quizid")->result();
		foreach($res as $submission) {
			$questions = $this->db->query("SELECT sq.*, q.questiontext as question, qo.optiontext FROM quizsubmissionquestion sq
											INNER JOIN quizquestion q ON q.id = sq.quizquestionid
											LEFT JOIN quizquestionoption qo ON qo.id = sq.quizquestionoptionid
											WHERE quizsubmissionid = $submission->id")->result();
			$submission->questions = $questions;
		}

		return $res;
	}

	function removeMany($ids, $table) {
		if(!empty($ids) && !empty($table)) {
			$ids = implode(',', $ids);
			$this->db->query("DELETE FROM quizquestionoption WHERE quizquestionid IN($ids)");
			$this->db->query("DELETE FROM $table WHERE id IN ($ids)");
		}
	}
}
