<?php
/**
 * Map model
 */
namespace Tapcrowd\Models;

use \Tapcrowd\Structs\Map as MapObject;
use \Tapcrowd\Structs\Marker as MarkerObject;

/**
 *
 */
class Map extends \Tapcrowd\Model\AbstractPDOModel
{
	protected $_supported_parents = array('app','event','venue','launcher');

	/**
	 * Return collection of Map structs
	 * @return ArrayObject
	 */
	public function getMaps( $parentType, $parentId, $query = null, $limit = null, $offset = null )
	{
// 		$map1 = new MapObject;
// 		$map1->parentType = $parentType;
// 		$map1->parentId = $parentId;
// 		$map1->url = 'https://maps.google.com/?z=8&t=t&ll=50.753887,4.269936';
//
// 		$map2 = new MapObject;
// 		$map2->parentType = $parentType;
// 		$map2->parentId = $parentId;
// 		$map2->url = 'http://upload.tapcrowd.com/upload/maps/2010_Expo_Floorplan_07151.jpg';
//
// 		$map3 = new MapObject;
// 		$map3->parentType = $parentType;
// 		$map3->parentId = $parentId;
// 		$map3->url = 'http://upload.tapcrowd.com/upload/maps/plattegrondleffingenleuren.jpg';
//
// 		$maps = new \ArrayObject(array($map1, $map2, $map3));
// 		return $maps;

		# Validate input
		if(!is_numeric($parentId) || !in_array($parentType, $this->_supported_parents)) {
			throw new \InvalidArgumentException();
		}

		# Get map for given parent
		# Setting App as parentType select all maps for that app
		$idname = $parentType.'id';
		$sql = "
		SELECT
		  IF(eventid = 0, IF(venueid = 0, 'launcher', 'venue'), 'event') AS parentType,
		  IF(eventid = 0, IF(venueid = 0, launcherid, venueid), eventid) AS parentId,
		  imageurl AS url, title, appid AS appId, mapType, id, FROM_UNIXTIME(timestamp) AS mdate
		FROM map WHERE $idname=%d";

		$stat = $this->query($sql, $parentId);
		$maps = $stat->fetchAll(\PDO::FETCH_CLASS, '\Tapcrowd\Structs\Map');

		$arr = new \ArrayObject();
		foreach($maps as &$map) {
			$map->markers = $this->getMarkers($map);	# Append Markers
			$arr[$map->id] = $map;
		}

		return $maps;
	}

	/**
	 * Returns the Marker structs for the give map
	 *
	 * @param \Tapcrowd\Structs\Map $map
	 * @return ArrayObject
	 */
	public function getMarkers( \Tapcrowd\Structs\Map $map )
	{
		$arr = array();
		if($map->mapType == 'geographic') {

			if($map->parentType != 'launcher') {
				throw new \InvalidArgumentException('Geographic maps are only supported on launcher level');
			}

			# Markers are: all instances of the launcher's module controller type
			# within the same event/venue as the launcher

			# Get launcher (and controller type)
			$sql = 'SELECT controller,
			  IF(eventid = 0, "venue", "event") AS parentType,
			  IF(eventid = 0, venueid, eventid) AS parentId
			FROM launcher, moduletype
			WHERE launcher.moduletypeid=moduletype.id AND launcher.id=%d LIMIT 1';
			$stat = $this->query($sql, $map->parentId);
			$dat = $stat->fetchAll(\PDO::FETCH_ASSOC);
			extract($dat[0]); # $controller, $parentType, $parentId

			# Create database query
			switch($controller) {
				case 'places':
					$sql = "
					SELECT {$map->id} AS mapId, p.id, p.id AS parentId, p.lat, p.lng, p.title, 'place' AS parentType,
					  m.value AS markerIcon
					FROM tc_places p
					LEFT OUTER JOIN tc_metavalues m ON m.type='markericon' AND m.parentType = 'place'
					AND m.parentId = p.id
					WHERE p.parentType=%s AND p.parentId=%d AND p.status != 'inactive'";
					break;
				default:
					throw new \RuntimeException('Geographic maps unsupported controller '.$controller);
					break;
			}
			$stat = $this->query($sql, $parentType, $parentId);
		} else {

			# Get cartesian markers
			$sql = 'SELECT * FROM tc_markers WHERE id IN(%d)';
			$stat = $this->query($sql, array_keys($arr));
		}

		# Fetch Markers
		$stat->setFetchMode(\PDO::FETCH_CLASS, '\Tapcrowd\Structs\Marker');
		while(($marker = $stat->fetch()) !== false) {
			$arr[$marker->id] = $marker;
		}

		return $arr;
	}

	/**
	 * Constructs and returns a new Map struct
	 * @return \Tapcrowd\Structs\Map
	 */
	public function createMap( $parentType, $parentId, $url, $title, $mapType )
	{
		# Validate
		// TODO

// 		# Validate
// 		$map->url = filter_var($map->url, FILTER_VALIDATE_URL);
// 		if(!is_numeric($map->parentId) || !in_array($map->parentType, array('event','venue','launcher','map')) || $map->url === false) {
// 			throw new \InvalidArgumentException();
// 		}
// 		$map->title = filter_var($map->title, FILTER_SANITIZE_SPECIAL_CHARS);
//
// 		if(!in_array($map->mapType, array('cartesian','geographic'))) {
// 			throw new \InvalidArgumentException('Invalid Map type : '.$map->mapType);
// 		}

		# Create struct
		$map = new MapObject;
		$map->parentType = $parentType;
		$map->parentId = $parentId;
		$map->url = $url;
		$map->title = $title;
		$map->mapType = $mapType;
		$map->cdate = time();
		return $map;
	}

	/**
	 * Creates and return a new Marker struct
	 *
	 * @param
	 * @return \Tapcrowd\Structs\Marker
	 */
	public function createMarker( \Tapcrowd\Structs\Map $map, $parentType, $parentId )
	{
		# Validate TODO

		# Create
		$marker = new MarkerObject;
		$marker->mapId = $map->id;
		$marker->parentType = $parentType;
		$marker->parentId = $parentId;
		$marker->cdate = time();
		return $marker;
	}

	/**
	 * Saves the given Map struct
	 *
	 * @param \Tapcrowd\Structs\Map &$map
	 * @return void
	 */
	public function saveMap( \Tapcrowd\Structs\Map &$map )
	{
// 		$this->__DRYRUN__ = true;

		# Insert
		# -> get idname (parentid, eventid, venueid, launcherid)
		$idname = ($map->parentType == 'map')? 'parentid' : $map->parentType.'id';
		$sql = 'INSERT INTO map ('.$idname.', imageurl, title, mapType, timestamp) VALUES(%d,%s,%s,%s,CURRENT_TIMESTAMP)';
		$stat = $this->execute($sql, $map->parentId, $map->url, $map->title, $map->mapType);
		if(!$stat) {
			throw new \RuntimeException( $this->lastErrorMessage() );
		}
		$map->id = $this->lastInsertId();

// 		$this->__DRYRUN__ = false;
	}

	/**
	 * Add marker
	 *
	 * @param \Tapcrowd\Structs\Marker $marker
	 * @return void
	 */
	public function saveMarker( \Tapcrowd\Structs\Marker $marker )
	{
		$sql = 'INSERT INTO tc_markers (mapId,parentType,parentId,xpos,ypos,cdate) VALUES ()';
	}

	public function setMarkerPosition( \Tapcrowd\Structs\Marker $marker, $x, $y )
	{
		//
	}

// 	public function renderMap( MapObject $map )
// 	{
// 		$html = '<img src="'.$map->url.'">';
// 		return $html;
// 	}
}
