<?php if(!defined('BASEPATH')) exit('No direct script access');

class Sessions_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Sessions_model() {
		parent::__construct();
	}
	
	function getUnmappedSessionsByEvent($eventid) {
		$this->db->where('eventid', $eventid);
		$this->db->order_by('order');
		$groups = $this->db->get('sessiongroup');
		if($groups->num_rows() == 0) return FALSE;
		$sessions = array();
		foreach ($groups->result() as $group) {
			$this->db->where('xpos', 0.000000);
			$this->db->where('ypos', 0.000000);
			$this->db->where('sessiongroupid', $group->id);
			$this->db->order_by('order');
			$res = $this->db->get('session');
			if($res->num_rows() > 0) {
				foreach ($res->result() as $session) {
					$sessions[] = $session;
				}
			}
		}
		return $sessions;
	}
	
	function getSessiongroupsByEventID($eventid) {
		$res = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid ORDER BY sessiongroup.order DESC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getSessiongroupsByVenueID($venueid) {
		$res = $this->db->query("SELECT * FROM sessiongroup WHERE venueid = $venueid ORDER BY sessiongroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getSessionByEventID($eventid) {
		$res = $this->db->query("SELECT s.* FROM sessiongroup sg, session s WHERE sg.id = s.sessiongroupid AND sg.eventid = $eventid AND s.eventid = $eventid ORDER BY s.order DESC, s.name ASC");
		if($res->num_rows() == 0) return FALSE;
		
		// sessions per group ophalen
		/*foreach ($res->result() as $group) {
			$sess = $this->db->query("SELECT * FROM session WHERE sessiongroupid = $group->id ORDER BY session.order ASC");
			$group->sessions = $sess->result();
		}*/
		
		return $res->result();
	}
	
	function getSessionByVenueID($venueid) {
		$res = $this->db->query("SELECT s.* FROM sessiongroup sg, session s WHERE sg.id = s.sessiongroupid AND sg.venueid = $venueid ORDER BY sg.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		// sessions per group ophalen
		/*foreach ($res->result() as $group) {
			$sess = $this->db->query("SELECT * FROM session WHERE sessiongroupid = $group->id ORDER BY session.order ASC");
			$group->sessions = $sess->result();
		}*/
		
		return $res->result();
	}
	
	function getSessionPerGroupByEventID($eventid) {
		$res = $this->db->query("SELECT sg.* FROM sessiongroup sg WHERE sg.eventid = $eventid ORDER BY sg.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		// sessions per group ophalen
		foreach ($res->result() as $group) {
			$sess = $this->db->query("SELECT * FROM session WHERE sessiongroupid = $group->id AND allowAddToFavorites = 1 AND eventid = $eventid ORDER BY session.order ASC");
			$group->sessions = $sess->result();
		}
		return $res->result();
	}
	
	function getSessionPerGroupByVenueID($venueid) {
		$res = $this->db->query("SELECT sg.* FROM sessiongroup sg WHERE sg.venueid = $venueid ORDER BY sg.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		// sessions per group ophalen
		foreach ($res->result() as $group) {
			$sess = $this->db->query("SELECT * FROM session WHERE sessiongroupid = $group->id AND allowAddToFavorites = 1 ORDER BY session.order ASC");
			$group->sessions = $sess->result();
		}
		return $res->result();
	}
	
	function getSessiongroupByID($id) {
		$res = $this->db->query("SELECT * FROM sessiongroup WHERE id = $id ORDER BY sessiongroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

	function getSessionByID($id) {
		$res = $this->db->query("SELECT s.*, sg.eventid, sg.venueid, sg.name as sessiongroupname FROM session s, sessiongroup sg WHERE s.id = $id AND sg.id = s.sessiongroupid");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

	function getById($id) {
		$res = $this->db->query("SELECT s.*, sg.eventid, sg.venueid, sg.name as sessiongroupname FROM session s, sessiongroup sg WHERE s.id = $id AND sg.id = s.sessiongroupid");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getSessionGroupsByEvent($eventid) {
		$res = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid ORDER BY sessiongroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getSessionGroupsByVenue($venueid) {
		$res = $this->db->query("SELECT * FROM sessiongroup WHERE venueid = $venueid ORDER BY sessiongroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getSessionByGroups($groupid) {
		$res = $this->db->query("SELECT * FROM session WHERE sessiongroupid = $groupid ORDER BY session.order ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function edit_session($id, $data) {
		$this->db->where('id', $id);
		if($this->db->update('session', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function delete_session($id) {
		
	}
	
	function edit_sessiongroup($id, $data) {
		$this->db->where('id', $id);
		if($this->db->update('sessiongroup', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	//### WEBSERVICE 
	function getSessionByEventIDService($eventid) {
		$res = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid ORDER BY sessiongroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		// sessions per group ophalen
		foreach ($res->result() as $group) {
			$sess = $this->db->query("SELECT * FROM session WHERE sessiongroupid = $group->id ORDER BY session.order ASC");
			// UTF8-stuff
			$group->name = htmlspecialchars_decode($group->name, ENT_NOQUOTES);
			foreach($sess->result() as $sessie){
				$sessie->date = date('d/m/Y', strtotime($sessie->starttime));
				$sessie->starttime = date('H', strtotime($sessie->starttime)) . 'h' . date('i', strtotime($sessie->starttime));
				$sessie->endtime = date('H', strtotime($sessie->endtime)) . 'h' . date('i', strtotime($sessie->endtime));
				
				// UTF8-stuff
				$sessie->name = htmlspecialchars_decode($sessie->name, ENT_NOQUOTES);
				
				
				$tags = $this->db->query("SELECT tag FROM tag WHERE sessionid = $sessie->id");
				if($tags->num_rows() == 0) {
					$sessie->tags = array();
				} else {
					$tagz = array();
					$tagstring = '';
					foreach ($tags->result() as $tag) {
						$tagz[] = $tag->tag;
						$tagstring .= $tag->tag . ', ';
					}
					$tagstring = substr($tagstring,0,-2);
					$sessie->tags = $tagz;
				}
				if($eventid == 269) {
					$sessie->description = htmlspecialchars_decode($sessie->description . "\n\n" . $tagstring, ENT_NOQUOTES);
				} else {
					$sessie->description = htmlspecialchars_decode($sessie->description, ENT_NOQUOTES);
				}
				
				/*
				$sessie->name			= "Name:/:".$sessie->name;
				$sessie->date			= "Date:/:".$sessie->date;
				$sessie->time			= "Time:/:".$sessie->starttime." - " . $sessie->endtime;
				$sessie->speaker		= "Speaker:/:".$sessie->speaker;
				$sessie->location		= "Location:/:".$sessie->location;
				$sessie->description	= "Description:/:".$sessie->description;
				*/
			}
			$group->sessions = $sess->result();
		}
		
		return $res->result();
	}
	
	function getSessionByVenueIDService($venueid) {
		$res = $this->db->query("SELECT * FROM sessiongroup WHERE venueid = $venueid ORDER BY sessiongroup.order ASC");
		if($res->num_rows() == 0) return FALSE;
		
		// sessions per group ophalen
		foreach ($res->result() as $group) {
			$sess = $this->db->query("SELECT * FROM session WHERE sessiongroupid = $group->id ORDER BY session.order ASC");
			// UTF8-stuff
			$group->name = htmlspecialchars_decode($group->name, ENT_NOQUOTES);
			foreach($sess->result() as $sessie){
				$sessie->date = date('d/m/Y', strtotime($sessie->starttime));
				$sessie->starttime = date('H', strtotime($sessie->starttime)) . 'h' . date('i', strtotime($sessie->starttime));
				$sessie->endtime = date('H', strtotime($sessie->endtime)) . 'h' . date('i', strtotime($sessie->endtime));
				
				// UTF8-stuff
				$sessie->name = htmlspecialchars_decode($sessie->name, ENT_NOQUOTES);
				$sessie->description = htmlspecialchars_decode($sessie->description, ENT_NOQUOTES);
				
				$tags = $this->db->query("SELECT tag FROM tag WHERE sessionid = $sessie->id");
				if($tags->num_rows() == 0) {
					$sessie->tags = array();
				} else {
					$tagz = array();
					foreach ($tags->result() as $tag) {
						$tagz[] = $tag->tag;
					}
					$sessie->tags = $tagz;
				}
				
				/*
				$sessie->name			= "Name:/:".$sessie->name;
				$sessie->date			= "Date:/:".$sessie->date;
				$sessie->time			= "Time:/:".$sessie->starttime." - " . $sessie->endtime;
				$sessie->speaker		= "Speaker:/:".$sessie->speaker;
				$sessie->location		= "Location:/:".$sessie->location;
				$sessie->description	= "Description:/:".$sessie->description;
				*/
			}
			$group->sessions = $sess->result();
		}
		
		return $res->result();
	}
    
    function getVotesOfEvent($eventid) {
        $sessions = $this->db->query("SELECT * FROM session INNER JOIN sessiongroup ON session.sessiongroupid = sessiongroup.id WHERE sessiongroup.eventid='$eventid'");
        
        $votes = 0;
        foreach($sessions->result() as $session) {
            $votes = $votes + $session->votes;
        }
		
		return $votes;
    }

    function getSessiongroupByName($eventid, $name) {
		$res = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid AND name = ? LIMIT 1", array($name));
		if($res->num_rows() == 0) {
			$sgdata = array( 
				"name" => $name,
				"eventid" => $eventid,
				"order" => 0
			);
			return $this->general_model->insert('sessiongroup', $sgdata);
		} else {
			return $res->row()->id;
		}
    }
	
	function getSessionLocationByEventId($eventid) {
		$res = $this->db->query("SELECT s.* FROM sessiongroup sg, session s WHERE sg.id = s.sessiongroupid AND sg.eventid = $eventid AND location <> '' AND xpos = '0.000000' GROUP BY location ORDER BY s.location ASC");
		if($res->num_rows() == 0) return FALSE;
		
		return $res->result();
	}

	function mapLocations($x, $y, $sessionid, $mapid) {
		$session = $this->db->query("SELECT * FROM session WHERE id = $sessionid LIMIT 1")->row();
		$this->db->query("UPDATE session SET mapid = $mapid, xpos = $x, ypos = $y WHERE location = '$session->location' AND eventid = $session->eventid");
	}

	function getIFrameUrl($id, $app) {
		$res = $this->db->query("SELECT * FROM launcher WHERE eventid = $id AND moduletypeid = 10 LIMIT 1");
		if($res->num_rows() == 0) {
			if($app->apptypeid == 10) {
				return 'sessions/timeline/'.$id;
			} else {
				return 'sessions/event/'.$id;
			}
		}
			
		$launcher = $res->row();
		
		if($launcher->displaytype == "location"){
			return "sessions/location/".$id;
		} elseif ($launcher->displaytype == "sessionoverview" || $launcher->displaytype == "overview") {
			return "sessions/overview/".$id;
		} elseif ($launcher->displaytype == "lineupv2") {
			return "sessions/overview/".$id;
		} elseif ($launcher->displaytype == "tracks") {
			return "sessions/tracks/".$id;
		} elseif ($launcher->displaytype == "alfa") {
			return "sessions/alfa/".$id;
		} elseif ($launcher->displaytype == "lineup") {
			return "sessions/lineup/".$id;
		} elseif ($launcher->displaytype == "timeline") {
			return "sessions/timeline/".$id;
		} else {
			if($app->apptypeid == 10 && $launcher->displaytype == '') {
				return 'sessions/timeline/'.$id;
			}
			return 'sessions/event/'.$id;
		}
	}

	function removeSessionsFromGroup($groupid) {
		$this->db->query("UPDATE session SET sessiongroupid = 0 WHERE sessiongroupid = $groupid");
	}

	function removePremium($id) {
		if(is_numeric($id)) {
			$this->db->query("DELETE FROM premium WHERE tablename = 'session' AND tableId  = $id");
		}
	}

	function getPremiumSessionsByEventID($eventid) {
		$res = $this->db->query("SELECT s.* FROM session s INNER JOIN premium p ON p.tableId = s.id WHERE s.eventid = $eventid AND p.tablename = 'session' ORDER BY p.sortorder");
		return $res->result();
	}

	function getSessionsByParentId($eventid, $parentid, $allow0 = false) {
		if($allow0 == false && $parentid == 0) {
			$sessions = $this->db->query("SELECT * FROM session WHERE eventid = $eventid")->result();
		} else {
			$sessions = $this->db->query("SELECT * FROM session WHERE eventid = $eventid AND parentid = $parentid")->result();
		}
		
		return $sessions;
	}

	function getByExternalId($external_parentid, $eventid) {
		$res = $this->db->query("SELECT * FROM session WHERE external_id = ? AND eventid = ? LIMIT 1", array($external_parentid, $eventid));
		if($res->num_rows == 0) {
			return false;
		}

		return $res->row();
	}
}