<?php
/**
 * Tapcrowd Session Model
 * @author Tom Van de Putte
 */
namespace Tapcrowd\Model;

if(!defined('BASEPATH')) exit('No direct script access');

require dirname(__DIR__) . '/models/pdo_model.php';

/**
 * Session Model
 */
class Session extends AbstractPDOModel
{
	private static $_sessId;
	private static $_sessName;
	private static $_sessExpire;

	/**
	 * Salt; Used for token generation
	 * @var string
	 */
	private static $salt = 'did_cow_tap_joe?';

	/**
	 * Constructor
	 */
	protected function __construct()
	{
		parent::__construct();

		# Set seesion name (used for session cookies)
		$this->_sessName = 'tc_sess2';

		# Set session timeout (sec)
		if(ENVIRONMENT == 'development') {
			$this->_sessExpire = 240*60; // 4 hours
		} else {
			$this->_sessExpire = 120*60; // 2 hours
		}
	}

	/**
	 * Authenticates the given credentials
	 *
	 * @param string $login
	 * @param string $password
	 * @return bool
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 *
	 * @todo Write Timeout checks
	 */
	public function authenticate( $login, $password )
	{
		$sql1 = 'SELECT accountId, status FROM tc_logins WHERE login=%s AND pass=%s AND status != "inactive" LIMIT 1';
		$sql2 = 'UPDATE tc_logins SET status = "loggedin" WHERE login=%s';
		$sql3 = 'UPDATE tc_sessions SET accountId=%d WHERE sessId=%d';
		$sql4 = 'UPDATE tc_logins SET pass=%s WHERE login=%s';
		$sql5 = 'SELECT channelId, role FROM tc_accounts WHERE accountId=%d AND status IN("pending","active")';

		#
		# TODO : Timeouts
		#
		# We should put a timed lock on current (username - ip address) tuple
		# If login is succesful the lock is released, else subsequent authentication
		# requests are postponed for:
		#
		# One failed attempt: At least 5 seconds
		# Two failed attempts: At least 15 seconds
		# Three failed attempts: At least 45 seconds
		#
		# After 3 failed attempts the account is blocked until:
		#
		# - Manual status change is done in the database.
		# - Users succesfully performs 'forgot password procedure'
		#

		# Login
		$channel_model = \Tapcrowd\Model\Channel::getInstance();
		$hmac_pass = $channel_model->getHash($password);

// 		$stat = $this->query($sql1, $login, sha1($password . self::$salt) );
		$stat = $this->query($sql1, $login, $hmac_pass);
		if($stat->rowCount() == 0) {

			# Try old sha1 password
			$stat = $this->query($sql1, $login, sha1($password . self::$salt) );
			if($stat->rowCount() == 0) {
				throw new \InvalidArgumentException(__('Unmatched credentials'));
			} else {

				# User still has old sha1 password : update it with channel salt.
				if( $this->execute($sql4, $hmac_pass, $login) === false ) {
					throw new \RuntimeException(__('Could not update password for "%s"', $login));
				}
			}

			# Try old md5 password
			/*
			$stat = $this->query($sql1, $login, sha1(md5($password)) );
			if($stat->rowCount() == 0) {
				throw new \InvalidArgumentException(__('Unmatched credentials'));
			} else {

				# User still has old (unsalted) password : update it with salt.
				if( $this->execute($sql4, sha1($password . self::$salt), $login) === false ) {
					throw new \RuntimeException(__('Could not update password for "%s"', $login));
				}
			} */
		}

		$row = $stat->fetch();
		$accountId = (int)$row['accountId'];
		$status = $row['status'];

		# Verify Account status, channel
		$stat = $this->query($sql5, $accountId);
		if($stat->rowCount() == 0 || ($row = $stat->fetch()) === false) {
			throw new \Tapcrowd\AccountException('Account is disabled');
		}

		#
		# Verify Channel: Accounts can only login into there respective channel (except tapcrowd admins)
		#
		$channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();
		if( !($row['channelId'] == 1 && $row['role'] == 'admin') && ($row['channelId'] != $channel->id) ) {
			throw new \InvalidArgumentException(__('Unmatched channel'));
		}

		# If Already loggedin, return
		// if($status == 'loggedin') return true;

		# Update login and authenticate session
		if( $this->execute($sql2, $login) === false ) {
			throw new \RuntimeException(__('Could not log in "%s"', $login));
		}
		if( $this->execute($sql3, $accountId, $this->_sessId) === false ) {
			throw new \RuntimeException(__('Could not log in "%s"', $login));
		}

		return true;
	}

	/**
	 * Returns the account for the given session or false if none
	 * -> Creates new session, authenticates user and return session token
	 *
	 * @param string $login
	 * @param string $password
	 * @return string HTTP GET query (sess_name=token)
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 */
	public function sso($login, $pass)
	{
		if($this->_createSession() === false) {
			throw new \RuntimeException('Could not create session');
		}
		$this->authenticate( $login, $pass);
		$token = $this->generateToken(60, null, $this->_sessId);
		return "{$this->_sessName}=$token";
	}

	/**
	 * Returns the account for the given session or false if none
	 *
	 * @return object|false
	 */
	public function getCurrentAccount()
	{
		static $account = false;
		if(is_object($account)) return $account;

		if(!isset($this->_sessId)) {
			$this->startSession();			// BUG : This should not happen
		}

		$sql = 'SELECT tc_accounts.*, tc_logins.status FROM tc_sessions INNER JOIN tc_logins USING(accountId) ';
		$sql.= 'INNER JOIN tc_accounts USING(accountId) ';
		$sql.= 'WHERE tc_sessions.sessId=%d AND tc_logins.status="loggedin" LIMIT 1';
		$stat = $this->query($sql, $this->_sessId);

		if($stat->rowCount() == 0) {
			return false;
		} else {
			$account = (object)$stat->fetch();
			$account->organizerId = $account->clientId;		# Set organizerid
			return $account;
		}
	}

	/**
	 * Verify the given token
	 *
	 * @param string $token
	 * @return bool
	 * @throws InvalidArgumentException
	 */
	public function verifyToken( $token )
	{
		$sql = 'SELECT 1 FROM tc_tokens WHERE token=%s AND timer > UNIX_TIMESTAMP() LIMIT 1';
		$stat = $this->query($sql, $token);
		if($stat->rowCount() == 0) {
			throw new \InvalidArgumentException(__('Invalid token "%s"', $token));
		}
		$stat->fetch();
		return true;
	}

	/**
	 * Remove the given token (if it exists)
	 *
	 * @param string $token
	 * @return void
	 */
	public function removeToken( $token )
	{
		$sql = 'DELETE FROM tc_tokens WHERE token=%s';
		$this->execute($sql, $token);
		return;
	}

	/**
	 * Remove the given token (if it exists)
	 *
	 * @param string $oldLogin
	 * @param string $newLogin
	 * @return void
	 */
	public function renameLogin( $oldLogin, $newLogin )
	{
		# If new login exists throw Exception
		$accountId = $this->getAccountForLogin($newLogin);
		if($accountId !== null) {
			$error = 'Could not rename login "%s", login "%s" already used';
			throw new \InvalidArgumentException(sprintf($error, $oldLogin, $newLogin));
		}

		# Get accountId for old login
		$accountId = $this->getAccountForLogin($oldLogin);
		if($accountId === null) {
			$error = 'Could not rename login "%s": Login does not exist';
			throw new  \InvalidArgumentException(sprintf($error, $oldLogin));
		}

		# Update login
		$sql = 'UPDATE tc_logins SET login = %s WHERE accountId = %d';
		if($this->execute($sql, $newLogin, $accountId) === false) {
			$error = 'Could not rename login "%s" to "%s": Database error';
			throw new \RuntimeException(sprintf($error, $oldLogin, $newLogin));
		}
	}

	/**
	 * Saves the given credentials, Updates if login already exists
	 *
	 * @param string $login
	 * @param string $pass
	 * @param string $fullname Optional full name
	 * @param string $email Optional email address
	 * @param int $ispartner is account partner
	 * @return bool
	 */
	public function saveLogin( $login, $pass = null, $fullname = null, $email = null, $ispartner = 0, $company = '' )
	{
		# Get
		$accountId = $this->getAccountForLogin($login);

		# Start transaction
		$this->startTransaction();

		# Make shure not to create a new account without password
		if(!$accountId && empty($pass)) {
			throw new \InvalidArgumentException('Password is required for new accounts');
		}

		if(!empty($fullname) || !empty($email)) {

			#
			# Create account
			#
			if(!$accountId) {

				# Account defaults
				$ca = $this->getCurrentAccount();
				$parentId = ($ca === false)? 1 : $ca->accountId;

				$channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();
				// $channelId = 1;
				// $channel = \Tapcrowd\API::getCurrentChannel();
				// if(isset($channel) && is_numeric($channel->id)) {
				// 	$channelId = $channel->id;
				// }
				// $channelId = $this->channel->id;

				# Create organizer
				$sql = 'INSERT INTO organizer(name,login,email,channelid,activation) VALUES(%s,%s,%s,%d,"active")';
				if($this->execute($sql, $fullname, $login, $email, $channel->id) === false) {
					$this->rollbackTransaction();
					throw new \RuntimeException(__('Could not create organizer "%s"', $login));
				}
				$clientId = (int)$this->lastInsertId();

				# Create Account
				$sql = 'INSERT INTO tc_accounts (parentId,channelId,fullname,email,clientId,cdate,ispartner,company) ';
				$sql.= 'VALUES(%d,%d,%s,%s,%d,NOW(),%d,%s)';
				if($this->execute($sql, $parentId, $channel->id, $fullname, $email, $clientId, $ispartner, $company) === false) {
					$this->rollbackTransaction();
					throw new \RuntimeException(__('Could not create account "%s"', $login));
				}
				$accountId = $this->lastInsertId();

			#
			# Update Account
			#
			} else {
				# Verify email (status).
				# When a user enters a new email address to the system, the account should
				# be set to status 'pending' if it was active before.
				$update_status = false;
				if($email !== null && $this->emailStatus($email) != 'active') {
					 $update_status = true;
				}

				$sql = 'UPDATE tc_accounts SET fullname=%s, email=%s, ispartner=%d WHERE accountId=%d';
				if($this->execute($sql, $fullname, $email, $ispartner, $accountId) === false) {
					$this->rollbackTransaction();
					throw new \RuntimeException(__('Could not update account "%s"', $login));
				}

				# Update account status
				$sql = 'UPDATE tc_accounts SET status="pending" WHERE accountId=%d AND status="active" LIMIT 1';
				if($update_status) {
					if($this->execute($sql, $accountId) === false) {
						$this->rollbackTransaction();
						throw new \RuntimeException(__('Could not update status for account "%s"', $login));
					}
				}
			}
		}

		# if pass is given, insert/update login
		if($pass !== null) {
			$sql = 'INSERT INTO tc_logins (login, pass, accountId, status) VALUES(%s, %s, %d,"active") ';
			$sql.= 'ON DUPLICATE KEY UPDATE pass=VALUES(pass)';

			$channel_model = \Tapcrowd\Model\Channel::getInstance();
			$hmac_pass = $channel_model->getHash($pass);

// 			$password = sha1($pass . self::$salt)
			if( $this->execute($sql, $login, $hmac_pass, $accountId) === false ) {
				$this->rollbackTransaction();
				throw new \RuntimeException(__('Could not create/update login "%s"', $login));
			}
		}

		# Commit transaction
		$this->commitTransaction();
	}

	/**
	 * Generates and returns token (inserts it into the token database)
	 *
	 * @param int $timer Optional; Timeout for the token
	 * @param string $email Optional email address
	 * @param int $sessId Optional; Restricts token to Session
	 * @return string
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 */
	public function generateToken( $timer = 86400, $email = null, $sessId = null )
	{
		$accountId = 0;
		if(!empty($email)) {
			$accountId = $this->getAccountForEmail($email);
			if(!$accountId) {
				throw new \InvalidArgumentException(__('There is no account with given email'));
			}
		}

		# Default the timer
		if(!is_numeric($timer) || $timer < 1) $timer = time() + 86400;
		elseif($timer < time()) $timer = time() + $timer;

		# Verify the session identifier
		if($sessId !== null && !is_numeric($sessId)) {
			throw new \InvalidArgumentException('Invalid session given');
		}

		# Generate token
		static $token;
		$token = sha1( uniqid() . self::$salt . $token);

		$sql = 'INSERT INTO tc_tokens (token, timer, accountId, sessId) VALUES(%s,%d,%d,%d)';
		if( $this->execute($sql, $token, (int)$timer, $accountId, (int)$sessId) === false ) {
			throw new \RuntimeException(__('Could not save token "%s"', $token));
		}
		return $token;
	}

	/**
	 * Close Session; Logout and stop session timing
	 *
	 * @return void
	 */
	public function closeSession()
	{
		session_destroy();
		setcookie( session_name(), '', 1);

		if(empty($this->_sessId)) return;

		# Check if this an authenticated session
		$accountId = $this->getAccountForSession( $this->_sessId );
		if(!$accountId || $accountId == 3) return;

		$sql = 'UPDATE tc_logins SET status="active" WHERE accountId=%d';
		if($this->execute($sql, (int)$accountId) === false) {
			throw new \RuntimeException(__('Could not set status for account #%d', $accountId));
		}
	}

	/**
	 * Starts Session; Set cookie etc... Or updates existing session
	 *
	 * Sessions are started using account #3 (anonymous) by default
	 *
	 * @return void
	 */
	public function startSession()
	{
		static $started;
		if($started === true) return;

		$sql3 = 'SELECT sessId, accountId, UNIX_TIMESTAMP(end_time) as end_time FROM tc_sessions WHERE sessId=%d LIMIT 1';
		$sql4 = 'UPDATE tc_logins SET status = "active" WHERE accountId=%d';
		$sql5 = 'UPDATE tc_sessions SET end_time = NOW() WHERE sessId=%d';

		# Get session status
		if(PHP_VERSION >= '5.4.0') {
			session_register_shutdown();
			if( session_status() === PHP_SESSION_ACTIVE ) {
				//
			} else {
				//
			}
		} else {
			//
		}

		# Set cache headers
		# @see http://be1.php.net/session_cache_limiter
		session_cache_limiter('nocache');

		# Set session configs
		session_set_cookie_params( 86400, '/', $_SERVER['HTTP_HOST'], false, true);
		session_name( $this->_sessName );

		#
		# Get Session using GET token or COOKIE
		#
		$sessToken = filter_input(INPUT_GET, $this->_sessName);
		$sessCookie = filter_input(INPUT_COOKIE, $this->_sessName);
		if($sessToken !== null) {
			$this->verifyToken($sessToken);

			$sql = 'SELECT sessId FROM tc_tokens WHERE token=%s LIMIT 1';
			$stat = $this->query($sql, $sessToken);
			if($stat->rowCount()) {
				$row = $stat->fetch();
				$sessId = (int)$row['sessId'];
			}

			$this->removeToken($sessToken);
		}
		elseif($sessCookie !== null) {
			$sessId = hexdec($sessCookie);
		}

		# Get && Verify session
		if(is_numeric($sessId)) {
			$stat = $this->query($sql3, $sessId);
			if($stat->rowCount()) {
				$sess = (object)$stat->fetch();

				#
				# TODO : Verify the sessions owner (using IP, user-agent etc...)
				#

				# If session is expired and user != anonymous: logout user.
				if(((time() - $sess->end_time) > $this->_sessExpire) && $sess->accountId != 3) {
					$this->execute($sql4, (int)$sess->accountId);
				}

				# Revamp Session (update end_time) if not older then 1 day
				if($sess->end_time > (time() - 86400)) {
					$this->execute($sql5, (int)$sess->sessId);
					$this->_sessId = (int)$sess->sessId;
				}
			}
		}

		# Else : Create new session
		if(empty($this->_sessId) && $this->_createSession() === false) {
			throw new \RuntimeException(__('Could not create new session'));
		}

		# Start PHP session
		session_id( dechex( $this->_sessId ) );
		session_start();
		$started = true;
	}

	/**
	 * Returns the accountId for the given id or false
	 *
	 * @param string $login
	 * @return int|false
	 */
	public function getAccountForLogin( $login )
	{
		$channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();
		$sql = '
			SELECT accountId FROM tc_logins
			INNER JOIN tc_accounts USING(accountId)
			WHERE login=%s AND channelId=%d LIMIT 1';
		$stat = $this->query($sql, $login, $channel->channelId);
		if($stat->rowCount() == 0) {
			return null;
		}
		$row = $stat->fetch();
		return (int)$row['accountId'];
	}

	/**
	 * Returns the accountId for the given id or false
	 *
	 * @param string $email
	 * @return int|false
	 */
	public function getAccountForEmail( $email )
	{
		$sql = 'SELECT accountId FROM tc_accounts WHERE email=%s LIMIT 1';
		$stat = $this->query($sql, $email);
		if($stat->rowCount() == 0) {
			return null;
		}
		$row = $stat->fetch();
		return (int)$row['accountId'];
	}

	/**
	 * Returns the accountId for the given session or false
	 *
	 * @param int $sessId
	 * @return int|false
	 */
	public function getAccountForSession( $sessId )
	{
		$sql = 'SELECT accountId FROM tc_sessions WHERE sessId=%s LIMIT 1';
		$stat = $this->query($sql, $sessId);
		if($stat->rowCount() == 0) {
			return null;
		}
		$row = $stat->fetch();
		return (int)$row['accountId'];
	}

	/**
	 * Returns the email for the given token or false
	 * Remove the token
	 *
	 * @param string $token
	 * @return string|false
	 */
	public function getEmailForToken( $token )
	{
		$sql = 'SELECT email FROM tc_accounts INNER JOIN tc_tokens USING(accountId) WHERE token=%s LIMIT 1';
		$stat = $this->query($sql, $token);
		if($stat->rowCount() == 0) {
			return null;
		}
		$row = $stat->fetch();
		$this->removeToken($token);
		return $row['email'];
	}

	/**
	 * Returns the accountId for the given id or false
	 *
	 * @param string $email
	 * @param string $status Optional, If set, updates the status
	 * @return boolean
	 */
	public function emailStatus( $email, $status = null )
	{
		# Update
		if($status == 'active') {
			$sql = 'UPDATE tc_accounts SET status="active" WHERE email=%s';
			if(($r = $this->execute($sql, $email)) === false) {
				throw new \RuntimeException('Could not update email status');
			}
			return $r;
		}

		# Select
		$sql = 'SELECT status FROM tc_accounts WHERE email=%s LIMIT 1';
		$stat = $this->query($sql, $email);
		if($stat->rowCount() == 0) {
			return null;
		} else {
			$row = $stat->fetch();
			return $row['status'];
		}
	}

	/**
	 * Returns the logins attached to given token
	 * Removes the token
	 *
	 * @param string $token
	 * @return array login strings
	 */
	public function getLoginsForToken( $token )
	{
		$logins = array();
		$sql = '
		SELECT tc_logins.login
		FROM tc_tokens
		INNER JOIN tc_accounts account USING(accountId)
		INNER JOIN tc_accounts USING(email)
		INNER JOIN tc_logins ON tc_logins.accountid = tc_accounts.accountId
		WHERE tc_tokens.token=%s';
		$stat = $this->query($sql, $token);
		while(($c = $stat->fetch()) !== false) {
			$logins[] = $c['login'];
		}
		$this->removeToken($token);
		return $logins;
	}


	/**
	 * Create new session; Sets id to internal _sessId param
	 * By default, user is Anonymous (3)
	 *
	 * @return bool
	 */
	private function _createSession()
	{
		#
		# TODO: Session Ids should be generated, not autoincremented
		# eg. by using ini_set('session.hash_function', 'hash_algorithm_name');
		#

		$sql = 'INSERT INTO tc_sessions (accountId,end_time) VALUES( 3, NOW() )';
		if($this->execute($sql) === false) {
			return false;
		}
		$this->_sessId = (int)$this->lastInsertId();
		return true;
	}
}


/* DATABASE TABLES

show columns from tc_accounts

accountId	int(10) unsigned
parentId	int(10) unsigned
clientId	int(11)
channelId	int(10) unsigned
role		enum('admin','user')
mdate		timestamp
cdate		timestamp
fullname	varchar(64)
email		varchar(255)


show columns from tc_logins

accountId	int(10) unsigned
status		enum('pending','active','loggedin','inactive','api')
login		varchar(32)
pass		char(40)
token		char(40)
mdate		timestamp


show columns from tc_sessions

sessId		int(10) unsigned
accountId	int(10) unsigned
start_time	timestamp
end_time	timestamp


tc_tokens

token		char(40)
timer		timestamp
accountId	int(10) unsigned
*/
