<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class Analytics_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function getAnalytics($appid, $filterDate) {
		if($filterDate != '') {
			$dateMin = $filterDate.'-01';
			$dateMax = $filterDate.'-31';
			$res = $this->db->query("SELECT * FROM analytics WHERE appid = $appid AND version = 1 AND (date BETWEEN '$dateMin' AND '$dateMax') ORDER BY date DESC");
		} else {
			$res = $this->db->query("SELECT * FROM analytics WHERE appid = $appid AND version = 1 ORDER BY date DESC");
		}
		
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getMonths($appid) {
		$res = $this->db->query("SELECT appid, max(date) as max_date, min(date) as min_date FROM analytics WHERE appid = $appid GROUP BY appid")->row();
		
		if($res) {
		$min = strtotime($res->min_date);
		$max = strtotime($res->max_date);
		
		$months = array();
		while($min <= $max) {
			$months[] = date('Y-m', $min);
			$min = (int)$min + 2629744;
		}
		
		if(!in_array((string)substr($res->max_date, 0, 7),$months)) {
			$months[] = (string)substr($res->max_date, 0, 7);
		}
		
		return $months;
		} else {
			return array();
		}
	}
}
