<?php if(!defined('BASEPATH')) exit('No direct script access');

class Map_model extends CI_Model {
	
	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Map_model() {
		parent::__construct();
	}
	
	function getMapsByEvent($eventid) {
		$res = $this->db->query("SELECT * FROM map WHERE eventid=$eventid ORDER BY parentid");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getMapsByVenue($venueid) {
		$res = $this->db->query("SELECT * FROM map WHERE venueid='$venueid' ORDER BY parentid");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getMapsById($mapid) {
		$res = $this->db->query("SELECT * FROM map WHERE id='$mapid'");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getMarkersByMapId($mapid, $table = 'exhibitor', $eventid = 0) {
		$res = $this->db->query("SELECT * FROM map WHERE id=$mapid");
		if($res->num_rows() == 0) return FALSE;
		$ret = $res->row();

		if ($table == "session") {
			if($mapid < 595) {
				$res = $this->db->query("SELECT session.* FROM session LEFT JOIN sessiongroup ON (session.sessiongroupid = sessiongroup.id) WHERE sessiongroup.eventid = '$ret->eventid' AND session.xpos <> '0' AND session.ypos <> '0' AND (mapid = $mapid OR mapid = 0)");
			} else {
				$res = $this->db->query("SELECT session.* FROM session LEFT JOIN sessiongroup ON (session.sessiongroupid = sessiongroup.id) WHERE sessiongroup.eventid = '$ret->eventid' AND session.xpos <> '0' AND session.ypos <> '0' AND mapid = $mapid");
			}
		} elseif($table == 'indoorrouting') {
			$points = $this->db->query("SELECT * FROM map_routingpoint WHERE mapid = ?", array($mapid))->result();
			$paths = $this->db->query("SELECT map_routingpath.id, map_routingpath.mapid, map_routingpoint.x as x1, map_routingpoint.y as y1, map_routingpoint2.x as x2, map_routingpoint2.y as y2 FROM map_routingpath 
										INNER JOIN map_routingpoint ON map_routingpoint.id = map_routingpath.map_routingpointid_start 
										INNER JOIN map_routingpoint as map_routingpoint2 ON map_routingpoint2.id = map_routingpath.map_routingpointid_end
										WHERE map_routingpath.mapid = ?", array($mapid))->result();
			return array('points' => $points, 'paths' => $paths);
		} else {
			if($mapid < 595) {
				//for old apps with only 1 map find mapid 0 because it wasn't filled in.
				$res = $this->db->query("SELECT * FROM exhibitor WHERE (eventid='$ret->eventid' OR eventid = $eventid) AND x1 <> '0' AND y1 <> '0' AND (mapid = $mapid OR mapid = 0)");
			} else {
				$res = $this->db->query("SELECT * FROM exhibitor WHERE (eventid='$ret->eventid' OR eventid = $eventid) AND x1 <> '0' AND y1 <> '0' AND mapid = $mapid");
			}
			
		}
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getMarkersByMapIdForVenue($mapid) {
		$res = $this->db->query("SELECT * FROM map WHERE id='$mapid'");
		if($res->num_rows() == 0) return FALSE;
		$ret = $res->row();
		
		$res = $this->db->query("SELECT * FROM exhibitor WHERE venueid='$ret->venueid' AND x1 <> '0' AND y1 <> '0'");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getTablesByMapIdForVenue($mapid)
	{
		$res = $this->db->query("SELECT * FROM tables_in_venue WHERE mapid=$mapid");
	
		return $res->result();
		
	}
	
	function getTableForVenue($mapid, $venueid, $tableid)
	{
		$res = $this->db->query("SELECT * FROM tables_in_venue WHERE mapid=$mapid AND venueid=$venueid AND tableid=$tableid");
		if($res->num_rows() == 0)
			return FALSE;
		else
			return $res->result();
		
	}
	
	function all() {
		$res = $this->db->get('map');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
	function getbyeventid($id) {
		$res = $this->db->query("SELECT * FROM map WHERE eventid='$id' LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function getbyvenueid($id) {
		$res = $this->db->query("SELECT * FROM map WHERE venueid='$id' LIMIT 1");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
	
	function add($eventid, $imageurl, $width = 0, $height = 0, $name = '') {
		if($name == '0') {
			$name = '';
		}
		$currenttimestamp = time();
		$data = array(
			'eventid' => $eventid,
			'imageurl' => $imageurl,
			'timestamp' => $currenttimestamp,
			'width'		=> $width,
			'height'	=> $height,
			'title'		=> $name
		);
		$this->db->insert('map', $data);
    }
	
	function edit($mapid, $imageurl, $width = 0, $height = 0, $name = '') {
		if($name == '0') {
			$name = '';
		}
		$this->db->where('id', $mapid);
		$currenttimestamp = time();
		$data = array(
			'imageurl' => $imageurl,
			'timestamp' => $currenttimestamp,
			'width'		=> $width,
			'height'	=> $height
		);
		$this->db->update('map', $data);
	}
	
	function addVenueMap($venueid, $imageurl, $width = 0, $height = 0, $name = '') {
		if($name == '0') {
			$name = '';
		}
		$currenttimestamp = time();
		$data = array(
			'venueid' => $venueid,
			'imageurl' => $imageurl,
			'timestamp' => $currenttimestamp,
			'width'		=> $width,
			'height'	=> $height
		);
		$this->db->insert('map', $data);
	}
	
	function editVenueMap($mapid, $imageurl, $width = 0, $height = 0, $name = '') {
		if($name == '0') {
			$name = '';
		}
		$this->db->where('id', $mapid);
		$currenttimestamp = time();
		$data = array(
			'imageurl' => $imageurl,
			'timestamp' => $currenttimestamp,
			'width'		=> $width,
			'height'	=> $height
		);
		$this->db->update('map', $data);
	}

	function getSubfloorplans($parentid) {
		$maps = $this->db->query("SELECT * FROM map WHERE parentId = $parentid")->result();
		return $maps;
	}

	function removeItemsOfMap($mapid, $type, $typeid) {
		$array = array();
		$parentArray = array();
		if($type == 'event') {
			$maps = $this->db->query("SELECT id, parentId FROM map WHERE eventid = $typeid ORDER BY parentId")->result();
		} elseif($type == 'venue') {
			$maps = $this->db->query("SELECT id, parentId FROM map WHERE venueid = $typeid ORDER BY parentId")->result();
		}

		foreach($maps as $map) {
			if(isset($parentArray[$map->parentId]) || $map->parentId == $mapid) {
				$parentArray[$map->parentId] = 1;
				$parentArray[$map->id] = 1;
				$array[] = $map->id;
			}
		}

		$array[] = $mapid;
		$ids = implode(',', $array);
		$this->db->query("UPDATE session SET mapid = 0, xpos = 0, ypos = 0 WHERE mapid IN ($ids)");
		$this->db->query("UPDATE exhibitor SET mapid = 0, x1 = 0, y1 = 0 WHERE mapid IN ($ids)");
		$this->db->query("DELETE FROM map WHERE parentId IN ($ids)");
	}

	function getZoomMaps($mapid) {
		$res = $this->db->query("SELECT * FROM map_zoomlevel WHERE mapid = ?", array($mapid));
		return $res->result();
	}

	function getIndoorRoutingPoint($id) {
		$res = $this->db->query("SELECT * FROM map_routingpoint WHERE id = ? LIMIT 1", array($id));
		return $res->row();
	}

	function removeIndoorRoutingPoint($id) {
		$this->db->query("DELETE FROM map_routingpoint WHERE id = ?", array($id));
		$this->db->query("DELETE FROM map_routingpath WHERE map_routingpointid_start = ? OR map_routingpointid_end = ?", array($id, $id));
	}

	function getIndoorRoutingPath($id) {
		$res = $this->db->query("SELECT map_routingpath.*, map.eventid, map.venueid FROM map_routingpath INNER JOIN map ON map.id = map_routingpath.mapid WHERE map_routingpath.id = ? LIMIT 1", array($id));
		return $res->row();
	}

	function removeIndoorRoutingPath($id) {
		$this->db->query("DELETE FROM map_routingpath WHERE id = ?", array($id));
	}

	function getIndoorRoutingPointByXY($mapid, $x, $y) {
		$res = $this->db->query("SELECT * FROM map_routingpoint WHERE mapid = ? AND x = ? AND y = ? LIMIT 1", array($mapid, $x, $y));
		if($res->num_rows() == 0) {
			return $false;
		}

		return $res->row();
	}
}

?>
