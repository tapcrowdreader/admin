<?php if(!defined('BASEPATH')) exit('No direct script access');

use \Tapcrowd\API;
use \Tapcrowd\AccessViolation;

/**
 * Tapcrowd Application Model
 */
class App_model extends CI_Model
{

	function all() {
		$res = $this->db->get('event');
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function get($id) {
		// $res = $this->db->get_where('app', array('id' => $id));
		$res = $this->db->query("SELECT app.*, apptypefamily.id as familyid FROM app INNER JOIN apptype ON apptype.id = app.apptypeid INNER JOIN apptypefamily ON apptype.familyid = apptypefamily.id WHERE app.id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

	function getbyid($id) {
		// $res = $this->db->get_where('app', array('id' => $id));
		$res = $this->db->query("SELECT app.*, apptypefamily.id as familyid FROM app INNER JOIN apptype ON apptype.id = app.apptypeid INNER JOIN apptypefamily ON apptype.familyid = apptypefamily.id WHERE app.id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}

	function addEventToApp($appid, $eventid) {
		$data = array(
			'appid' => $appid,
			'eventid' => $eventid
		);
		$this->db->insert('appevent', $data);
	}

	function appsByOrganizer($organizerid) {
		$res = $this->db->query("SELECT * FROM app WHERE organizerid ='$organizerid' AND isdeleted = 0");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function getAppsByEvent($eventid) {
		$res = $this->db->query("SELECT * FROM appevent WHERE eventid='$eventid'");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

    function getRealAppOfEvent($eventid) {
		$res = $this->db->query("SELECT appid FROM appevent WHERE eventid='$eventid' AND appid <> 2");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
    }

    function getRealAppOfVenue($venueid) {
		$res = $this->db->query("SELECT appid FROM appvenue WHERE venueid='$venueid' AND appid <> 2");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
    }

    function getSubflavor($id) {
		$res = $this->db->query("SELECT * FROM subflavor WHERE id='$id'");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
    }

	function getAppsByVenue($venueid) {
		$res = $this->db->query("SELECT * FROM appvenue WHERE venueid='$venueid'");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function getEventsByApp($appid) {
		$res = $this->db->query("SELECT a.*, e.datefrom FROM appevent a, event e WHERE a.appid = $appid AND e.id = a.eventid ORDER BY e.datefrom ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function getVenuesByApp($appid) {
		$res = $this->db->query("SELECT v.* FROM appvenue a, venue v WHERE a.appid = $appid AND v.id = a.venueid");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function getActiveVenuesByApp($appid) {
		$res = $this->db->query("SELECT v.* FROM appvenue a, venue v WHERE a.appid = $appid AND v.id = a.venueid AND v.active = 1");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}

	function getAppTypes() {
		$channelid = $this->channel->id;

		if(_isAdmin() && $channelid == 1) {
			$res = $this->db->query("SELECT * FROM apptype WHERE id <> 2 AND id <> 6 AND id <> 11 ORDER BY sortorder");
		} else {
			$res = $this->db->query("SELECT a.* FROM apptype a INNER JOIN apptype_channel c ON a.id = c.apptypeid WHERE c.channelid = $channelid AND a.available = 1 ORDER BY a.sortorder");
		}

		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}

	function checkToken($id = 0, $token = 0) {
		if($token == 0) return FALSE;
		$app = $this->get($id);
		if(isset($app)) {
			if($app->token == $token) {
				return true;
			}
		}
		return FALSE;
	}

    function getFlavorOfId($id) {
		$res = $this->db->get_where('apptype', array('id' => $id));
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
    }

    /**
	 * @deprecated
	 */
    function checkDomainAvailability($subdomain, $currentId = 0) {
    	if($currentId != 0) {
    		$res = $this->db->query("SELECT id FROM app WHERE isdeleted = 0 AND subdomain = '$subdomain' AND id <> $currentId");
    	} else {
    		$res = $this->db->query("SELECT id FROM app WHERE isdeleted = 0 AND subdomain = '$subdomain'");
    	}

		if($res->num_rows() == 0) return true;
		return false;
    }

    function getPopupScript($app) {
    	$androidstore = '';
    	$applestore = '';
    	$stores = $this->db->query("SELECT * FROM appstores WHERE appid = $app->id LIMIT 1");
    	if($stores->num_rows() > 0) {
    		$androidstore = $stores->row()->androidmarket;
    		$androidstore = substr($androidstore, strrpos($androidstore, '=') + 1);
    		$applestore = $stores->row()->appleappstore;
    		$applestore = substr($applestore, strrpos($applestore, '/') + 3);
    		$applestore = substr($applestore, 0, strrpos($applestore, '?'));
    	}

    	$script = array();
    	$script['meta'] = '<link rel="stylesheet" href="http://clients.tapcrowd1.netdna-cdn.com/smartbanner/jquery.smartbanner.css" type="text/css" media="screen">
<meta name="apple-itunes-app" content="app-id='.$applestore.'">
<meta name="google-play-app" content="app-id='.$androidstore.'">';

    	$script['js'] = '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<script src="http://clients.tapcrowd1.netdna-cdn.com/smartbanner/jquery.smartbanner.js"></script>
<script type="text/javascript">
	var appname = "'.$app->name.'";
	var appicon = "http://upload.tapcrowd1.netdna-cdn.com/'.$app->app_icon.'";
    var android = location.href.match(/#android$/) || navigator.userAgent.match(/Android/i) != null;
    if (android) {
	    $.smartbanner({
	        title: appname,
	        author: "TapCrowd",
	        icon: appicon,
	        force: android ? "android" : "ios",
	        daysHidden: 15,
	        daysReminder: 0
	    });

	    $(".theme-toggle").on("click", function(e) {
	        e.preventDefault();
	        $.smartbanner("switchType")
	        $(this).text($(this).text() == "Preview Android" ? "Preview iOS" : "Preview Android")
	    })

	    if (android) $(".theme-toggle").text("Preview iOS");
	}
</script>';

	return $script;
    }

    function getRedirectScript($scriptlanguage, $app) {
    	if($scriptlanguage == 'php') {
			  	$script = '<?php
$mobile_browser = 0;

if (preg_match("/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i", strtolower($_SERVER["HTTP_USER_AGENT"]))) {
    $mobile_browser++;
}

if ((strpos(strtolower($_SERVER["HTTP_ACCEPT"]),"application/vnd.wap.xhtml+xml") > 0) or ((isset($_SERVER["HTTP_X_WAP_PROFILE"]) or isset($_SERVER["HTTP_PROFILE"])))) {
    $mobile_browser++;
}

$cookie = false;
if(isset($_GET["setpreference"])) {
  setcookie("fullsitepreference", "fullsite", time()+3600*24*7, "/");
  $cookie = true;
}

if (isset($_COOKIE["fullsitepreference"])) {
  $cookie = true;
}
if ($mobile_browser > 0 && $cookie == false) {
  header("Location: http://'.$app->subdomain.'.m.tap.cr");
}
?>';
		} elseif($scriptlanguage == 'js') {
			$script = '<script type="text/javascript">
var subdomain = "'.$app->subdomain.'";
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i) ? true : false;
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i) ? true : false;
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) ? true : false;
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
    }
};

function getParam(name)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=?([^&#]*)?";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return false;
  else if(typeof(results[1]) == "undefined") {
  	return null;
  }
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function Set_Cookie() {
	var expires = new Date();
	expires.setMonth(expires.getMonth() + 1);
	document.cookie = "fullsitepreference=fullsite;expires="+expires+";path=/";
}

function getCookie(c_name) {
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
	  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
	  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
	  x=x.replace(/^\s+|\s+$/g,"");
	  if (x==c_name)
	    {
	    return unescape(y);
	    }
	  }
}

var cookie = false;
if(getParam("setpreference") !== false) {
	Set_Cookie();
	cookie = true;
}

if(getCookie("fullsitepreference")) {
	cookie = true;
}

if( isMobile.any() && cookie == false) {
	location.href ="http://"+subdomain+".m.tap.cr";
}
</script>';
		}

		return $script;
    }

	function countLauncherFromAppModule($appid, $moduletypeid) {
		$res = $this->db->query("SELECT l.* FROM launcher l WHERE l.appid = '$appid' AND l.moduletypeid = '$moduletypeid' ");
		if($res->num_rows() == 0) return FALSE;
		return $res->num_rows();
	}
/*
    function allfromorganizer($organizerid) {
        $q = $this->db->query("SELECT * FROM app WHERE organizerid='$organizerid'");
        if($q->num_rows>0) {
            foreach($q->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }


    function add($name, $apptypeid, $organizerid) {
        $token = substr(md5(rand()+time()),0,10);
        $data = array(
                'name' => $name,
                'apptypeid' => $apptypeid,
                'organizerid' => $organizerid,
                'token' => $token
            );
        $this->db->insert('app', $data);
    }
    function delete($id) {
        //TODO: Add code to delete an app!
    }
    function checkToken($id = 0, $token = 0) {
        if($token > 0) {
            $app = $this->get($id);
            if(isset($app)) {
                if($app->token == $token) {
                    return true;
                }

            }

        }return false;
    }
*/

	/**
	 * Returns wether the current account has permissions on the app
	 *
	 * @param \Tapcrowd\Application $app
	 * @return void
	 * @throws \Tapcrowd\AccessViolation
	 */
	public function checkPermissions(  \Tapcrowd\Application $app )
	{
		$account = \Tapcrowd\API::getCurrentUser();
		if(!\Tapcrowd\API::hasEntityAccess( $account, 'app', $app->id)) {
			throw new \Tapcrowd\AccessViolation();
		}
	}

	/**
	 * Returns the permissions set for the given app
	 *
	 * These are found in the table tc_entityrights. Admins always have
	 * rights to all apps within their channel, so they are excluded.
	 *
	 * @param \Tapcrowd\Application $app
	 * @return ArrayObject
	 */
	public function getPermissions( \Tapcrowd\Application $app )
	{
		$this->checkPermissions($app);
		$sql = 'SELECT tc_entityrights.*, tc_accounts.fullname AS accountname
			FROM tc_entityrights
			INNER JOIN tc_accounts USING(accountId)
			WHERE entity="application" AND entityId=?';
		return $this->db->query($sql, $app->id)->result();
	}

	/**
	 * Gives the account permissions for the given app
	 *
	 * @param \Tapcrowd\Application $app
	 * @param \Tapcrowd\Account $account
	 * @return void
	 */
	public function addPermissions( \Tapcrowd\Application $app, \Tapcrowd\Account $account )
	{
		$this->checkPermissions($app);
		$sql = 'INSERT INTO tc_entityrights (accountId,entity,entityId) VALUES(?,"application",?)';
		$sql.= ' ON DUPLICATE KEY UPDATE entityId=VALUES(entityId)';
		$res = $this->db->query($sql, array((int)$account->accountId, (int)$app->id));
	}

	/**
	 * Removes the accounts permissions for the given app
	 *
	 * @param \Tapcrowd\Application $app
	 * @param \Tapcrowd\Account $account
	 * @return void
	 */
	public function removePermissions( \Tapcrowd\Application $app, \Tapcrowd\Account $account )
	{
		$this->checkPermissions($app);
		$sql = 'DELETE FROM tc_entityrights WHERE entity="application" AND accountId=? AND entityId=?';
		$res = $this->db->query($sql, array((int)$account->accountId, (int)$app->id));
	}

	function getBundles() {
		$bundles = $this->db->query("SELECT bundle FROM app")->result();
		$array = array();
		foreach($bundles as $b) {
			$array[$b->bundle] = $b->bundle;
		}

		return $array;
	}

	/**
	 * Validates the given subdomain
	 *
	 * @param string $subdomain
	 * @param int $appId The app id for this subdomain
	 * @return bool
	 * @throws \InvalidArgumentException
	 */
	public function validateSubdomain( $subdomain, $appId = 0 )
	{
		if(empty($subdomain) || !is_string($subdomain)) {
			throw new \InvalidArgumentException(__('Subdomain should be non-empty string'));
		}

		if(!is_numeric($appId)) {
			throw new \InvalidArgumentException(__('Invalid Application "%s"', $appId));
		}

		# Validate against RFC 2396 (URI)
		if(!filter_var('http://' . $subdomain . '.m.tap.cr', FILTER_VALIDATE_URL)) {
			throw new \InvalidArgumentException(__('Subdomain should be valid DNS name (%s)', $subdomain));
		}

		# Check for duplicate in database
		$sql = 'SELECT COUNT(*) AS cnt FROM tapcrowd.app WHERE subdomain = ? AND id != ?';
		$res = $this->db->query($sql, array($subdomain, $appId))->result_array();
		if(isset($res[0]) && !empty($res[0]['cnt'])) {
			throw new \InvalidArgumentException(__('Subdomain (%s) already used by another Application', $subdomain));
		}

		return true;
	}
}
