<?php 

if (! defined('BASEPATH')) exit('No direct script access');

class News_model extends CI_Model {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function News_model() {
		parent::__construct();
        
	}
	
	function getNewsByEventID($eventid) {
		$res = $this->db->query("SELECT * FROM newsitem WHERE eventid = $eventid ORDER BY datum DESC, title ASC");
		if($res->num_rows() == 0) return FALSE;
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = $this->config->item('publicupload') . $row->image;
			}
		}
		return $res->result();
	}
	
	function getNewsByVenueID($venueid) {
		$res = $this->db->query("SELECT * FROM newsitem WHERE venueid = $venueid ORDER BY datum DESC, title ASC");
		if($res->num_rows() == 0) return FALSE;
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = $this->config->item('publicupload') . $row->image;
			}
		}
		return $res->result();
	}
	
	function getNewsById($id) {
		$res = $this->db->query("SELECT * FROM newsitem WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
	}
    
	function getNewsBySourceId($sourceid) {
		$res = $this->db->query("SELECT * FROM newsitem WHERE sourceid = $sourceid");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
    
    function getRssOfEvent($eventid) {
		$res = $this->db->query("SELECT * FROM newssource WHERE eventid = $eventid AND type = 'rss'");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
    }
    
    function getRssOfVenue($venueid) {
		$res = $this->db->query("SELECT * FROM newssource WHERE venueid = $venueid AND type = 'rss'");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
    }

    function getRssOfApp($appid) {
		$res = $this->db->query("SELECT * FROM newssource WHERE appid = $appid AND type = 'rss'");
		if($res->num_rows() == 0) return FALSE;
                //echo '<pre>';print_r($res->result());echo '</pre>';
		return $res->result();
    }
    
    function getSourceOfId($id) {
		$res = $this->db->query("SELECT * FROM newssource WHERE id = $id");
		if($res->num_rows() == 0) return FALSE;
		return $res->row();
    }
    
    function removeNewsOfSourceId($sourceid) {
        $news = $this->getNewsBySourceId($sourceid);  
        if(isset($news) && $news != false && $news != null && !empty($news)) {
	        foreach($news as $newsitem) {
	            //remove translation
	            $this->language_model->removeTranslations('newsitem', $newsitem->id);
	        }
        } 
        $this->db->where('sourceid', $sourceid);
        $this->db->delete('newsitem'); 
    }

	function getNewsByAppId($appid) {
		$res = $this->db->query("SELECT * FROM newsitem WHERE appid = $appid ORDER BY datum DESC, id ASC");
		if($res->num_rows() == 0) return FALSE;
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = $this->config->item('publicupload') . $row->image;
			}
		}
		return $res->result();
	}

	function getTagsOfNewsByApp($appid) {
		$res = $this->db->query("SELECT id FROM newsitem WHERE appid = $appid");
		if($res->num_rows() == 0) return FALSE;
		$newsids = array();
		foreach($res->result() as $n) {
			$newsids[] = $n->id;
		}
		
		$newsidsstring = implode(',', $newsids);
		$tags = $this->db->query("SELECT * FROM tag WHERE newsitemid IN ($newsidsstring) GROUP BY tag");
		return $tags->result();
	}

	function getNewsByTagId($tagid) {
		$tag = $this->db->query("SELECT * FROM tag WHERE id = $tagid LIMIT 1");
		if($tag->num_rows() == 0) {
			return array();
		} 

		$tag = $tag->row();
		$tagname = $tag->tag;
		$appid = $tag->appid;

		$newsitemidsquery = $this->db->query("SELECT newsitemid FROM tag WHERE tag = '$tagname' AND appid = $appid");
		if($newsitemidsquery->num_rows() == 0) {
			return array();
		}

		$newsitemids = array();
		foreach($newsitemidsquery->result() as $nid) {
			$newsitemids[] = $nid->newsitemid;
		}

		$newsitemidsstring = implode(',', $newsitemids);
		
		$newsitems = $this->db->query("SELECT * FROM newsitem WHERE id IN ($newsitemidsstring)");
		return $newsitems->result();
	}
        
	function urlExists($appid,$url) {
		$res = $this->db->query("SELECT * FROM newssource WHERE appid = $appid AND url LIKE '%$url%' LIMIT 1");
		return $res->num_rows();
	}
        
	function newsItemExists($sourceid,$url) {
		$res = $this->db->query("SELECT * FROM newsitem WHERE sourceid = $sourceid AND url LIKE '%$url%' LIMIT 1");
                return $res->num_rows();
	}
	function getContentRssFeeds(){
		$newssources = $this->db->query("SELECT a.apptypeid, a.id AS appid, at.id AS typeid, n.appid, n.id AS newsid, n.url FROM app a, apptype at, newssource n WHERE a.apptypeid = at.id AND at.id = 9 AND n.appid = a.id ");
		return $newssources->result();
	}

	function refreshrss($source, $parent, $parentType = 'event') {
		// $today = date("Y-m-d H:i:s",time());

		// $deletetime = time() - $source->deleteafterdays * 3600 * 24;
		// $deletedate = date("Y-m-d H:i:s",$deletetime);
		// if($source->timestamp >= $deletedate || $source->deleteafterdays == 0) {
		// 	$insert = array();
		// 	$rss_url = $source->url;
		// 	$rssfeed = $this->rss_to_array($rss_url);
		// 	if($rssfeed) {
		// 		// $sourcehash = $this->sourcehash($rssfeed);
		// 		$this->db->query("UPDATE newssource SET hash = '$sourcehash', `timestamp` = '$today' WHERE id = $source->id");
		// 		// if($sourcehash != $source->hash) {
		// 			foreach($rssfeed as $item) {
		// 				$hash = $item['hash'];
		// 				$exists = 0;
		// 				$existsQuery = $this->db->query("SELECT id, hash FROM newsitem WHERE hash='$hash' AND sourceid = $source->id LIMIT 1");
		// 				if($existsQuery->num_rows() == 0) {
		// 					$title = $item['title'];
		// 					$txt = $item['description'];
		// 					$url = $item['link'];
		// 					if($item['pubDate']!= null && !empty($item['pubDate'])) {
		// 						$date = date("Y-m-d H:i:s", strtotime($item['pubDate']));
		// 					} else {
		// 						$date = date("Y-m-d H:i:s",time());
		// 					}
		// 					$videourl = $item['videourl'];
		// 					$image = '';
		// 					$imgpos = stripos($txt, '<img');
		// 					if($imgpos !== false) {
		// 						// var_dump($item['description']);
		// 						$imgend = stripos($txt, '</img>', $imgpos) + 6;
		// 						if($imgend === false || $imgend == 6) {
		// 							$imgend = stripos($txt, '/>', $imgpos) + 3;
		// 						}
		// 						if($imgend !== false && $imgpos !== false) {
		// 							$imglength = $imgend - $imgpos;
		// 							$image = substr($txt, $imgpos, $imglength);
		// 							preg_match('@src="([^"]+)"@' , $image , $imagematch);
		// 							if(isset($imagematch[1])) {
		// 								$image = $imagematch[1];
		// 							} else {
		// 								$image = '';
		// 							}
		// 							// $txt = substr($txt, 0, $imgpos) . substr($txt, $imgend - 1);
		// 						}
		// 					}
							
		// 					if(empty($image)) {
		// 						preg_match('@enclosure url="([^"]+)"@' , $txt , $imagematch);
		// 						if(isset($imagematch[1])) {
		// 							$image = $imagematch[1];
		// 						} else {
		// 							preg_match('@media:content url="([^"]+)"@' , $txt , $imagematch);
		// 							if(isset($imagematch[1])) {
		// 								$image = $imagematch[1];
		// 							} else {
		// 								$image = '';
		// 							}
		// 						}
		// 					}

		// 					if(isset($item['image'])) {
		// 						$image = $item['image'];
		// 					}
		// 					if($source->deleteafterdays == 0 || strtotime($date) >= $deletetime) {
		// 						$insert[] = "('".$parent->id."','".mysql_real_escape_string($title)."','".mysql_real_escape_string($txt)."','".$image."','".$url."','".$source->id."','".$date."','".$hash."','".$videourl."')";
		// 					}
		// 				}
		// 			}
		// 			if(!empty($insert)) {
		// 				$insert = implode(',',$insert);
		// 				if($parentType == 'event') {
		// 					$this->db->query("INSERT INTO newsitem (eventid, title, txt, image, url, sourceid, datum, hash, videourl) VALUES $insert");
		// 				} elseif($parentType == 'venue') {
		// 					$this->db->query("INSERT INTO newsitem (venueid, title, txt, image, url, sourceid, datum, hash, videourl) VALUES $insert");
		// 				} elseif($parentType == 'app') {
		// 					$this->db->query("INSERT INTO newsitem (appid, title, txt, image, url, sourceid, datum, hash, videourl) VALUES $insert");
		// 				}
		// 			}
		// 		// }
		// 	}
		// }
		// if($source->deleteafterdays != 0) {
		// 	$this->db->query("DELETE FROM newsitem WHERE sourceid = $source->id AND datum < '$deletedate'");
		// }
	}

	function rss_to_array($url) {
		if(!empty($url)) {
			//check lastmodified with curl
			$curl = curl_init($url);
			//don't fetch the actual page, you only want headers
			curl_setopt($curl, CURLOPT_NOBODY, true);
			//stop it from outputting stuff to stdout
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			// attempt to retrieve the modification date
			curl_setopt($curl, CURLOPT_FILETIME, true);
			$result = curl_exec($curl);
			if ($result === false) {
			    return false;
			}
			// $timestamp = curl_getinfo($curl, CURLINFO_FILETIME);
			// if ($timestamp != -1) { //otherwise unknown
			//     if($timestamp < time() - 3600 * 48) {
			//     	//last modified older than 2 days ago -> don't import
			//     	return false;
			//     }
			// } 


			$curl = curl_init($url);
			//stop it from outputting stuff to stdout
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$content = curl_exec($curl);
			if ($content === false) {
			    return false;
			}

			try {
				$rss = new SimpleXmlElement($content);
				if(!$rss) {
					return array();
				}
			} catch (Exception $e) {
				$rss = null;
			}

			$rss_array = array();
			$countitems = 0;
			foreach($rss->channel->item as $post) {
				$description = (string)$post->description;
		    	$namespace = $post->children('http://purl.org/rss/1.0/modules/content/');
		    	$content = (string)$namespace->encoded;
		    	if($content != null) {
		    		$description = $content;
		    	}   

		    	$videourl = '';
		    	$image = '';
				$media = $post->children('http://search.yahoo.com/mrss/');
				if($media) {
					$media = $media->attributes();
					if(stristr($media['type'], 'video')) {
						$videourl = (string)$media['url'];
					} elseif(stristr($media['type'], 'image')) {
						$image = (string)$media['url'];
					}
				}

				$guid = '';
				if(isset($post->guid)) {
					$guid = (string) $post->guid;
				}

				$extra = array();
				$notExtraParams = array('title', 'description', 'link', 'pubDate', 'guid', 'image');
				foreach($post as $key => $value) {
					if(!in_array($key, $notExtraParams)) {
						$extra[$key] = (string) $value;
					}
				}

				$categories = array();
				if(isset($post->category)) {
					foreach($post->category as $cat) {
						$cat = (string) $cat;
						if(stripos($cat, ',')) {
							$cats = explode(',', $cat);
							foreach($cats as $c) {
								$categories[] = $c;
							}
						} else {
							$categories[] = $cat;
						}
					}
				}

				$data = array( 
						"title" 		=> (string)$post->title,
						"description" 	=> $description,
						"link"			=> (string)$post->link,
						"pubDate"		=> ((string) $post->pubDate!= null && (string) $post->pubDate != '') ? date("Y-m-d H:i:s",strtotime((string) $post->pubDate)) : date("Y-m-d H:i:s",time()),
						"hash"			=> md5((string)$post->title.$description.(string)$post->link),
						"videourl" 		=> $videourl,
						"guid"			=> $guid,
						"image"			=> $image,
						"extra"			=> json_encode($extra),
						"categories"	=> $categories
					);

				if(isset($post->image)) {
					$data['image'] = (string)$post->image;
				}

				array_push($rss_array, $data);

				// if($countitems >= 20) {
				// 	break;
				// }
				$countitems++;
			}

			if(!empty($rss_array)) {
				return $rss_array;
			}
		}

		return false;
	}

	function sourcehash($array) {
		return md5($array[0]['title'].$array[0]['description'].$array[0]['link']);
	}

    function getRssSourceByTag($appid, $tag) {
		$res = $this->db->query("SELECT * FROM newssource WHERE appid = $appid AND type = 'rss' AND tag LIKE '%$tag%'");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
    }

    function getNewsBySourceTag($tag, $appid) {
    	$news = array();
    	$sources = $this->db->query("SELECT id FROM newssource WHERE appid = $appid AND tag = '$tag'")->result();
    	foreach($sources as $s) {
    		$news2 = $this->db->query("SELECT * FROM newsitem WHERE sourceid = $s->id")->result();
    		foreach($news2 as $n) {
    			$news[] = $n;
    		}
    	}

    	return $news;
    }

}