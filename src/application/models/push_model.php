<?php if(!defined('BASEPATH')) exit('No direct script access');

class Push_model extends CI_Model {
	
	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Push_model() {
		parent::__construct();
	}
	
	function getByAppId($appid) {
		$res = $this->db->get_where('push', array("appid" => $appid));
		return $res->result();
	}
	
	function getSentByAppId($appid) {
		$res = $this->db->query("SELECT * FROM pushcue WHERE appid = $appid ORDER BY sent DESC, id ASC");
		if($res->num_rows() == 0) return FALSE;
		return $res->result();
	}
	
}

?>
