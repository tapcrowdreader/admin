<?php

	class Pagesetup
	{
		function start() {
			$obj =& get_instance();

			// BUG: Set global channel object (should be replace with calls to channel model)
			$obj->channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();

			$obj->templatefolder = "";

// 			$subdomain_arr = explode('.', $_SERVER['HTTP_HOST'], 2);
// 			$subdomain_name = $subdomain_arr[0];
//
// 			$account = \Tapcrowd\API::getCurrentUser();
// 			if($account !== false) {
// 				if($account->channelId == 1) {
// 					//user channelid == 1 => tapcrowd channel
// 					$res = $obj->db->query("SELECT ch.*, cu.url FROM channel ch INNER JOIN channelurl cu ON cu.channelid = ch.id WHERE ch.id = 1 LIMIT 1");
// 					$obj->channel = $res->row();
// 				} elseif($_SERVER['SERVER_NAME'] == 'admin.tapcrowd.com') {
// 					$userchannel = $obj->db->query("SELECT ch.*, cu.url FROM channel ch INNER JOIN channelurl cu ON cu.channelid = ch.id WHERE ch.id = $account->channelId LIMIT 1")->row();
// 					if(!empty($userchannel->url)) {
// 						//tapcrowd channel
// 						$res = $obj->db->query("SELECT ch.*, cu.url FROM channel ch INNER JOIN channelurl cu ON cu.channelid = ch.id WHERE ch.id = 1 LIMIT 1");
// 						$obj->channel = $res->row();
// 					} else {
// 						//user channel
// 						$obj->channel = $userchannel;
// 					}
// 				} elseif($account->channelId != 1 && $_SERVER['SERVER_NAME'] != 'admin.tapcrowd.com') {
// 					//user channel
// 					$userchannel = $obj->db->query("SELECT ch.*, cu.url FROM channel ch INNER JOIN channelurl cu ON cu.channelid = ch.id WHERE ch.id = $account->channelId LIMIT 1")->row();
// 					if($_SERVER['SERVER_NAME'] == $userchannel->url) {
// 						$obj->channel = $userchannel;
// 					}
// 				} else {
// 					//tapcrowd channel
// 					$res = $obj->db->query("SELECT ch.*, cu.url FROM channel ch INNER JOIN channelurl cu ON cu.channelid = ch.id WHERE ch.id = 1 LIMIT 1");
// 					$obj->channel = $res->row();
// 				}
// 			} else {
// 				//old code as backup
// 				$obj->db->from('channelurl')->where('url', $_SERVER['SERVER_NAME']);
// 				$ref = $obj->db->get();
//
// 				if($ref->num_rows() >= 1) {
// 					$row = $ref->row();
// 					$res = $obj->db->query("SELECT ch.* FROM channel ch, channelurl cu WHERE cu.id = $row->id AND ch.id = cu.channelid");
// 					if($res->num_rows() >= 1) {
// 						$obj->templatefolder = $res->row()->templatefolder;
// 						$obj->channel = $res->row();
// 					}
// 				} else {
// 					$res = $obj->db->query("SELECT ch.* FROM channel ch, channelurl cu WHERE cu.id = 1 AND ch.id = cu.channelid");
// 					$obj->channel = $res->row();
// 				}
// 			}

			/*
			$lang = $obj->uri->segment(1);
			if($lang != "" && $lang != FALSE) $obj->config->set_item("language", $lang);
			DEFINE('LANG', $lang);
			*/

			$excl_arr = array('jwebservice', 'webservice', 'download', 'dev', 'cache', 'api', 'giftbag', 'voting', 'voucher', 'app', 'aps');

			if(!in_array($obj->uri->segment(1), $excl_arr)){
				if($obj->uri->segment(1) != 'auth') {
					if(_authed()) {
						if(_currentUser()->login == 'xpo') {
							$obj->templatefolder = 'xpo';
						}
					}
				}
			}

			if(!in_array($obj->uri->segment(1), array('jwebservice', 'webservice', 'auth', 'apps', 'account','submit', 'upload', 'download', 'dev', 'cache', 'api', 'giftbag', 'voting', 'voucher', 'admin', 'app'))){
				$obj->appid = _appID();
			}

			if($obj->uri->segment(1) == FALSE || $obj->uri->segment(1) == '') {
				$obj->session->set_userdata('appid', '');
			}

            $obj->iframeurl = "";
		}

		function finish() {

		}
	}

?>