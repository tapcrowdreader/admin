<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	/**
	 * MY_Controller
	 *
	 * @package Libraries
	 * @author Matthijs Stichelbaut
	 **/
	
	class MY_Controller extends CI_Controller {
		
		
		function MY_Controller() {
			parent::__construct();
            $this->iframeurl = "";
		}
        
		function getParams() {
			
			$method = 'index';
			$ro = new ReflectionObject($this);
			$rm = $ro->getMethod($method);
			
			$params = array();
			$oClassReflect = new ReflectionClass($this);
			$docClass = $oClassReflect->getDocComment();
			if ($docClass == '' || $docClass == false || $docClass == null) {
				$params[] = "No documentation found!";
			} else {
				$sDocComment = preg_replace("/(^[\\s]*\\/\\*\\*)|(^[\\s]\\*\\/)|(^[\\s]*\\*?\\s)|(^[\\s]*)|(^[\\t]*)/ixm", "", $docClass);
				$sDocComment = str_replace("\r", "", $sDocComment);
				$sDocComment = preg_replace("/([\\t])+/", "\t", $sDocComment);
				$sDocComment = explode("\n", $sDocComment);
				if (count($sDocComment) > 1) {
					$params[] = $sDocComment[1];
				} else {
					$params[] = "No description found.";
				}
			}
			
			if ($rm) {
				$param = $rm->getParameters();
				if (count($param) > 0) {
					foreach ($param as $par) {
						$params[] = $par->name;
					}
				}
			}
			
			echo json_encode($params);
		}
		
		function updatedTimestamp($function = '', $type = '', $id = 0, $timestamp = 0) {
			if($function == '' || $timestamp == 0) return FALSE;
			
			$modules = $this->db->query("SELECT * FROM moduletype WHERE apicall = '$function' LIMIT 1");
			if ($modules->num_rows() == 0) return FALSE;
			$module_id = $modules->row()->id;
			$stamp = $this->db->query("SELECT * FROM module WHERE moduletype = $module_id AND $type = $id LIMIT 1");
			if($stamp->row()->timestamp >= $timestamp) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
