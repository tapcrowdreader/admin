<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Auth {
	
	var $CI;
	var $session_prefix				= "auth_";
	var $encryption_key				= "";
	var $use_lastlogin				= FALSE;
	
	var $table 						= "";
	var $user_field 				= "";
	var $password_field				= "";
	var $lastlogin_field			= ""; 
	var $role_field					= ""; //NOT IMPLEMENTED YET
	
	var $cookie_name				= "client_login";
	var $cookie_expire				= 2678400; //31 days;
	var $additional_where_clause	= "";

	function auth()
	{
		$this->CI =& get_instance();
		$this->CI->load->config('auth');
		$this->CI->load->database();
		$this->CI->load->library('session');
		$this->CI->load->library('encrypt');
		$this->CI->load->helper('cookie');
		
		$this->session_prefix		= $this->CI->config->item('auth_session_prefix');
		$this->encryption_key		= $this->CI->config->item('encryption_key');
		$this->use_remembering		= $this->CI->config->item('auth_use_remembering');
		$this->use_lastlogin		= $this->CI->config->item('auth_use_lastlogin');

		$this->table 				= $this->CI->config->item('auth_table');
		$this->user_field 			= $this->CI->config->item('auth_user_field');
		$this->password_field		= $this->CI->config->item('auth_password_field');
		$this->remember_field		= $this->CI->config->item('auth_remember_field');
		$this->lastlogin_field		= $this->CI->config->item('auth_lastlogin_field');
		$this->role_field			= $this->CI->config->item('auth_role_field');
		
		$this->cookie_name			= $this->CI->config->item('auth_cookie_name');
		$this->cookie_expire		= $this->CI->config->item('auth_cookie_expire');
		
		$this->additional_where_clause = $this->CI->config->item('auth_additional_where_clause');
	}

    function login($username, $password, $setCookie = FALSE)
    {
		// ===============
		// = SEARCH USER =
		// ===============
		$sql = "Select * from `" . $this->table . "`
				where `" . $this->user_field . "` = ? 
				AND `" . $this->password_field . "` = ? ";	
		$sql .= $this->additional_where_clause;
		$res = $this->CI->db->query($sql, array($username, $this->_encrypt($password)));

		// ===================
		// = NO VALID RESULT =
		// ===================
		if($res->num_rows() != 1) return FALSE;
		
		// =========================
		// = STORE USER IN SESSION =
		// =========================
		$user = $res->row();
		unset($user->{$this->password_field}); //wachtwoord niet meebewaren in de sessie
		$this->CI->session->set_userdata(array($this->session_prefix . "user" => $user, $this->session_prefix . 'logged' =>$this->CI->encrypt->encode($this->CI->input->ip_address())));
		
		// ================
		// = UPDATE STATS =
		// ================
		if($this->use_lastlogin && $this->lastlogin_field != ""){
			$sql = "UPDATE `" . $this->table . "` SET `" . $this->lastlogin_field . "` = NOW()";
			$this->CI->db->query($sql);
		}
		if($setCookie){
			$this->_setcookie($username, $password);
		} else {
			delete_cookie($this->cookie_name);
		}
		
		// =====================
		// = RETURN USEROBJECT =
		// =====================
		return $user;
    }

	// ===================
	// = GET COOKIE DATA =
	// ===================
	function getCookieValues()
	{
		return $this->_getCookie();
	}
	
	// ===================
	// = INSERT NEW USER =
	// ===================
	function insertUser($user_arr)
	{
		$pass = $user_arr[$this->password_field];
		
		if($this->verifyUserName($user_arr[$this->user_field])){
			$user_arr[$this->password_field] = $this->_encrypt($pass);
			$user_arr[$this->lastlogin_field] = date("Y-m-d H:i:S");
			$this->CI->db->insert($this->table, $user_arr);
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function updateUser($user_arr, $new_password = FALSE)
	{
		$us = $this->user();
		$usname = $us->{$this->user_field};
		if($new_password != FALSE){
			$user_arr[$this->password_field] = $this->_encrypt($user_arr[$this->password_field]);
		} else {
			unset($user_arr[$this->password_field]);
		}
		if(($user_arr[$this->user_field] != $us->{$this->user_field}) && ($user_arr[$this->user_field] != "")){
			$usname = $user_arr[$this->user_field];
			if(! $this->verifyUserName($user_arr[$this->user_field])) return FALSE;
		} else {
			unset($user_arr[$this->user_field]);
		}
		
		$this->CI->db->where($this->user_field, $us->{$this->user_field});
		$this->CI->db->update($this->table, $user_arr);
		
		//nieuwe data in sessie steken
		$sql = "Select * from `" . $this->table . "`
				where `" . $this->user_field . "` = ? ";	
		$res = $this->CI->db->query($sql, array($usname));
		$user = $res->row();
		unset($user->{$this->password_field}); //wachtwoord niet meebewaren in de sessie
		$this->CI->session->set_userdata(array($this->session_prefix . "user" => $user, $this->session_prefix . 'logged' =>$this->CI->encrypt->encode($this->CI->input->ip_address())));
		return $user;
	}
	
	// =========================
	// = CHECK UNIQUE USERNAME =
	// =========================
	function verifyUserName($username)
	{
		$sql = "SELECT * FROM `" . $this->table . "` WHERE `" . $this->user_field . "` = ? ";	
		$res = $this->CI->db->query($sql, array($username));
		return ($res->num_rows() == 0);
	}
	
	// ====================
	// = COMPARE PASSWORD =
	// ====================
	function comparePassword($pass)
	{
		$us = $this->User();
		$sql = "Select * from `" . $this->table . "`
				where `" . $this->user_field . "` = ? ";	
		$res = $this->CI->db->query($sql, array($us->{$this->user_field}));
		$user = $res->row();
		return ($user->{$this->password_field} == $this->_encrypt($pass));
	}
	
	// ======================
	// = LOGOUT =
	// ======================
	function logout()
	{
		$this->CI->session->set_userdata(array($this->session_prefix . 'logged' => "", $this->session_prefix . 'user' => ""));
	}
	
	// ======================
	// = CHECK_STATUS =
	// ======================
	function checkStatus()
	{
		return($this->CI->encrypt->decode($this->CI->session->userdata($this->session_prefix . 'logged')) == $this->CI->input->ip_address());
	}
	
	// ==================
	// = RENEW PASSWORD =
	// ==================
	function updatePassword($username,$password)
	{
		$sql = "UPDATE `" . $this->table . "` SET `" . $this->password_field . "` = ? WHERE  `" . $this->user_field . "` = ? ";
		return $this->CI->db->query($sql, array($this->_encrypt($password), $username));
	}
	
	function makeNewPassword($username)
	{
		$new_pass = substr(md5(time()), 6,6);
		$this->updatePassword($username, $new_pass);
		return $new_pass;
	}
	
	// ====================
	// = GET CURRENT USER =
	// ====================
	function user()
	{
		return $this->CI->session->userdata($this->session_prefix . 'user');
	}
	
	function refreshUser()
	{
		$us = $this->User();
		$sql = "Select * from `" . $this->table . "`
				where `" . $this->user_field . "` = ? ";	
		$res = $this->CI->db->query($sql, array($us->{$this->user_field}));
		$user = $res->row();
		unset($user->{$this->password_field}); //wachtwoord niet meebewaren in de sessie
		$this->CI->session->set_userdata(array($this->session_prefix . "user" => $user, $this->session_prefix . 'logged' =>$this->CI->encrypt->encode($this->CI->input->ip_address())));
		return $user;
	}
	
	// ======================
	// = INTERNAL FUNCTIONS =
	// ======================
	function _encrypt($str)
	{
		return md5($str.$this->encryption_key);
	}
	
	function _setcookie($username, $password){
		$val = "$username]||[$password";
		$val = $this->CI->encrypt->encode($val);
		set_cookie($this->cookie_name, $val, $this->cookie_expire);
	}
	
	function _getcookie(){
		$val = get_cookie($this->cookie_name);
		$val = $this->CI->encrypt->decode($val);
		$pieces = explode("]||[", $val);
		return array("username" => $pieces[0], "password" => $pieces[1]);
	}
}

?>
