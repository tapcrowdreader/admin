<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once('phpass-0.1/PasswordHash.php');

define('PHPASS_HASH_STRENGTH', 4);
define('PHPASS_HASH_PORTABLE', TRUE);

/**
 * SimpleLoginSecure Class
 *
 * Makes authentication simple and secure.
 *
 * Simplelogin expects the following database setup. If you are not using 
 * this setup you may need to do some tweaking.
 *   
 * 
 *   CREATE TABLE `users` (
 *     `user_id` int(10) unsigned NOT NULL auto_increment,
 *     `user_email` varchar(255) NOT NULL default '',
 *     `user_pass` varchar(60) NOT NULL default '',
 *     `user_date` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'Creation date',
 *     `user_modified` datetime NOT NULL default '0000-00-00 00:00:00',
 *     `user_last_login` datetime NULL default NULL,
 *     PRIMARY KEY  (`user_id`),
 *     UNIQUE KEY `user_email` (`user_email`),
 *   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 * 
 * @package   SimpleLoginSecure
 * @version   1.0.1
 * @author    Alex Dunae, Dialect <alex[at]dialect.ca>
 * @copyright Copyright (c) 2008, Alex Dunae
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt
 * @link      http://dialect.ca/code/ci-simple-login-secure/
 */
class SimpleLoginSecure
{
	var $CI;
	var $user_table = 'organizer';

	/**
	 * Create a user account
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function create($login = '', $password = '', $name = '', $email = '', $channelid = 0)
	{
		$this->CI =& get_instance();
		


		//Make sure account info was sent
		if($login == '' OR $password == '') {
			return false;
		}
		
		//Check against user table
		$this->CI->db->where('login', $login);
		$query = $this->CI->db->get_where($this->user_table);
		
		if ($query->num_rows() > 0) { //username already exists
			return 'userexists';
		}

		//Hash user_pass using phpass
		//$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
		$user_pass_hashed = md5($password); //$hasher->HashPassword($password);

		//Insert account into the database
		$data = array(
					'login' => $login,
					'password' => $user_pass_hashed,
					'name' => $name,
					'email' => $email,
					'activation' => 'pending',
					'channelid' => $channelid,
					'admin_channel' => $channelid
				);

		$this->CI->db->set($data); 

		if(!$this->CI->db->insert($this->user_table)) //There was a problem! 
			return false;
		return true;
	}

	/**
	 * Login and sets session variables
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function login($login = '', $password = '')
	{
		$this->CI =& get_instance();

		if($login == '' OR $password == '')
			return false;


		//Check if already logged in
		if($this->CI->session->userdata('login') == $login)
			return true;
		
		//Check against user table
		$this->CI->db->where('login', $login);
		$query = $this->CI->db->get_where($this->user_table);

		if ($query->num_rows() > 0) 
		{
			$user_data = $query->row_array(); 
			
			if(substr($user_data['password'], 0, 4) == '$P$7'){
				// USE HASHER
				$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
				
				if(!$hasher->CheckPassword($password, $user_data['password']))
					return false;
			} else {
				// USE MD5
				if(md5($password) != $user_data['password'])
					return false;
			}


			//Destroy old session
			$this->CI->session->sess_destroy();
			
			//Create a fresh, brand new session
			$this->CI->session->sess_create();

			//$this->CI->db->simple_query('UPDATE ' . $this->user_table  . ' SET user_last_login = NOW() WHERE user_id = ' . $user_data['user_id']);

			//Set session data
			unset($user_data['password']);
			//$user_data['login'] = $user_data['login']; // for compatibility with Simplelogin
			$user_data['logged_in'] = true;
			$this->CI->session->set_userdata($user_data);
			
			return true;
		} 
		else 
		{
			return false;
		}	

	}

	/**
	 * Logout user
	 *
	 * @access	public
	 * @return	void
	 */
	function logout() {
		$this->CI =& get_instance();
		$this->CI->session->sess_destroy();
	}

	/**
	 * Delete user
	 *
	 * @access	public
	 * @param integer
	 * @return	bool
	 */
	function delete($user_id) 
	{
		$this->CI =& get_instance();
		
		if(!is_numeric($user_id))
			return false;
		
		return $this->CI->db->delete($this->user_table, array('user_id' => $user_id));
	}
	
}
?>
