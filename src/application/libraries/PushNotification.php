<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PushNotification {
	
	function send($certif, $phones, $text) {
		
		date_default_timezone_set('Europe/Brussels');
		//error_reporting(-1);
		require_once 'ApnsPHP/Autoload.php';
		
		$push = new ApnsPHP_Push(
			ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
			"_certificates/$certif"
		);
		
		$push->connect();
		
		foreach ($phones as $phone) {
			$message = new ApnsPHP_Message($phone->token);
			$message->setText($text);
			$push->add($message);
		}
		
		$push->send();
		$push->disconnect();
		
		$aErrorQueue = $push->getErrors();
		if (!empty($aErrorQueue)) {
			return FALSE;//var_dump($aErrorQueue);
		} else {
			return TRUE;
		}
		
	}
	
	function testje() {
		date_default_timezone_set('Europe/Rome');
		error_reporting(-1);
		require_once 'ApnsPHP/Autoload.php';
		
		$push = new ApnsPHP_Push(
			ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
			'_certificates/apns-dev.pem'
		);
		$push->connect();
		$message = new ApnsPHP_Message('d7c11236f0698e8bdd86e88c8281930090eb284c');
		$message->setCustomIdentifier("Message-Badge-3");
		$message->setBadge(3);
		$message->setText('Hello APNs-enabled device!');
		$message->setSound();
		$message->setCustomProperty('acme2', array('bang', 'whiz'));
		$message->setCustomProperty('acme3', array('bing', 'bong'));
		$message->setExpiry(30);
		$push->add($message);
		$push->send();
		$push->disconnect();
		$aErrorQueue = $push->getErrors();
		if (!empty($aErrorQueue)) {
			var_dump($aErrorQueue);
		}
	}
	
}
?>