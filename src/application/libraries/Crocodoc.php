<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once APPPATH."/third_party/Crocodoc/CrocodocApi.php"; 
 
class Crocodoc extends CrocodocApi {
	/*
	* PHP 5 Constructor
	*/
    public function __construct() { 
        return parent::__construct();
    }
	
	/*
	* PHP 4 Constructor
	*/
    public function Crocodoc() { 
        return parent::__construct(); 
    } 
	
	public function upload($urlOrFile){
		$postParams = array();
		
		if (is_string($urlOrFile)) {
			$postParams['url'] = $urlOrFile;
		} elseif (is_resource($urlOrFile)) {
			$postParams['file'] = $urlOrFile;
		} else {
			$this->response = array(
					'status' => 'FAILED',
					'message' => 'FILE IS MIISSING OR CURRUPT!'
				);
				
			return $this->response;
		}
		
		$res = $this->_request('document/upload', null, $postParams);
		
		if ( empty( $res['uuid'] ) ) {
			return $this->response = array('status' => 'FAILED', 'message' => 'MISSING UUID');
		}
		
		return $this->response = array('status' => 'SUCCESS', 'message' => 'Congrats!', 'uuid' => $res['uuid']);
	}
	
	public function create($uuid) {
		$postParams = array('uuid' => $uuid);
		$postParams['editable'] = ( isset( $params['isEditable'] ) && $params['isEditable'] ) ? 'true' : 'false';
		
		if (
			!empty($params['user'])
			&& is_array($params['user'])
			&& isset($params['user']['id'])
			&& isset($params['user']['name'])
		) {
			$postParams['user'] = $params['user']['id'] . ',' . $params['user']['name'];
		}
		
		if (!empty($params['filter'])) {
			if (is_array($params['filter'])) {
				$params['filter'] = implode(',', $params['filter']);
			}
			
			$postParams['filter'] = $params['filter'];
		}
		
		if (isset($params['isAdmin'])) {
			$postParams['admin'] = $params['isAdmin'] ? 'true' : 'false';
		}
		
		if (isset($params['isDownloadable'])) {
			$postParams['downloadable'] = $params['isDownloadable'] ? 'true' : 'false';
		}
		
		if (isset($params['isCopyprotected'])) {
			$postParams['copyprotected'] = $params['isCopyprotected'] ? 'true' : 'false';
		}
		
		if (isset($params['isDemo'])) {
			$postParams['demo'] = $params['isDemo'] ? 'true' : 'false';
		}
		
		if (isset($params['sidebar'])) {
			$postParams['sidebar'] = $params['sidebar'];
		}
		
		$session = $this->_request('session/create', null, $postParams);
		
		if (!is_array($session) || empty($session['session'])) return $this->response = array('status' => 'FAILED', 'message' => 'MISSING SESSION KEY!');
		
		return $this->response = array('status' => 'SUCCESS', 'message' => 'SESSION CREATED', 'session' => $session['session']);
	}
	
	public function thumbnail($uuid, $width = null, $height = null){
		$getParams = array(
			'uuid' => $uuid,
		);
		
		if (!empty($width) && !empty($height)) {
			$getParams['size'] = $width . 'x' . $height;
		}
		
		$thumbnail = $this->_request('download/thumbnail', $getParams, null, false);
		
		if ( is_array( $thumbnail ) ) return $this->response = array('status' => 'FAILED', 'message' => 'INVALID UUID!');
		
		$image = imagecreatefromstring($thumbnail);
		// start buffering
		ob_start();
		imagepng($image);
		$contents =  ob_get_contents();
		ob_end_clean();
		
		imagedestroy($image);
		
		return $this->response = array('status' => 'SUCCESS', 'message' => 'THUMBNAIL CREATED', 'thumbnail' => base64_encode($contents));		
	}
	
	public function delete($uuid) {
		$postParams = array(
			'uuid' => $uuid,
		);
		
		return $this->_request('document/delete', null, $postParams);		
	}
}