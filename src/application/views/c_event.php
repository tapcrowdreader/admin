<?php if($app->familyid == 3) : ?>
<div>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
<!-- 	<div class="eventinfo">
		<?php if ($event->eventlogo != '' && file_exists($this->config->item('imagespath') . $event->eventlogo)): ?>
			<div class="eventlogo" style="background:transparent url('<?= image_thumb($event->eventlogo, 50, 50) ?>') no-repeat center center">&nbsp;</div>
		<?php else: ?>
			<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
		<?php endif ?>
		<?php if($event->datefrom == '1970-01-01') : ?>
			<span class="info_title"><?= __('Permanent event') ?></span>
		<?php else: ?>
			<span class="info_title"><?= __('From - To:') ?></span> <?= date('d/m/Y', strtotime($event->datefrom)) ?> - <?= date('d/m/Y', strtotime($event->dateto)) ?>
		<?php endif; ?>
	</div> -->
	<?php if (!$this->session->userdata('mijnevent')): ?>
	<a href="<?= site_url('event/remove/'.$event->id) ?>" class="btn btn-danger" id="deletebtn" style="margin-top:-65px;margin-right:10px;float:right;"><span><?= __('Delete event') ?></span></a>
	<?php if($app->apptypeid != 10) : ?>
		<!-- <a href="<?= site_url('event/travelinfo/'.$event->id) ?>" class="travel btn"><span><?= __('Travel info') ?></span></a> -->
	    <!-- <a href="<?= site_url('schedule/event/'.$event->id) ?>" class="schedule btn"><span><?= __('Schedule') ?></span></a> -->
	<?php endif; ?>
	<!-- <a href="<?= site_url('event/edit/'.$event->id) ?>" class="edit btn"><span><?= __('Edit event') ?></span></a> -->
	<?php endif ?>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#deletebtn').click(function() {
				jConfirm('<?= __('This will <b>remove</b> this event!<br />! This cannot be undone!') ?>', '<?= __('Remove Event') ?>', function(r) {
					if(r == true) {
						window.location = '<?= site_url("event/remove/".$event->id)?>';
						return true;
					} else {
						jAlert('<?= __("Event not removed!") ?>', '<?= __("Info") ?>');
						return false;
					}
				});
				return false;
			});

			// $('.editimage').css('display', 'none');
			// $('.active a').hover(
			// 	function() {
			// 		$(this).parent().find('.editimage').css('display', 'inline');
			// 	},
			// 	function() {
			// 		$(this).parent().find('.editimage').css('display', 'none');
			// 	}
			// );
		});
	</script>
	<!-- <h2><?= __('Menu items') ?></h2> -->
	<a href="dynamiclauncher/add/<?=$event->id?>/event" class="btn primary" style="float:right;margin-bottom:10px;"><?= __('Add Web Module') ?></a>
    <a href="forms/add/<?=$event->id?>/event" class="btn primary" style="float:right; margin-right:5px;"><?= __('Add Form Module') ?></a>
    <?php if($app->id == 1) : ?>
    <a href="dataimport/event/<?=$event->id?>/event" class="btn primary" style="float:right; margin-right:5px;"><?= __('Import Catalog') ?></a>
	<?php endif; ?>
		<?php if(isset($eventlaunchers) && !empty($eventlaunchers)) : ?>
		<select id="launcherselect" style="float:right;margin-right:5px;">
			<?php foreach($eventlaunchers as $l) : ?>
				<option value="<?= $l->id ?>" name="<?= $l->id ?>" <?= $l->id == $launcherid ? 'selected="selected"' : '' ?>><?= $l->title ?></option?>
			<?php endforeach; ?>
		</select>
		<script type="text/javascript">
		$(document).ready(function() {
			$("#launcherselect").change(function(e) {
				var url = "<?= site_url('event/changelauncher/'.$event->id.'/'.$launcherid) ?>" + '/' + $("#launcherselect option:selected").val();
				
				$.ajax({
					type: "GET",
					url: url
				}).done(function( data ) {
					location.href = "<?= site_url('event/view/'.$event->id) ?>" +'/'+$("#launcherselect option:selected").val();
				});
			});
		});
		</script>
		<?php endif; ?>
	<br clear="all" />
	<div class="modules">
		<?php if ($eventmodules != FALSE): ?>
		<?php $activated = 0; ?>
		<?php
			$mapactive = FALSE;
			foreach ($eventmodules as $module):
				if ($module->name == "Interactive map"):
					if ($module->active):
						$mapactive = TRUE;
					endif;
				endif;
			endforeach;
		?>
		<?php foreach ($eventmodules as $module): ?>
			<?php
				$show = false;
			if($app->apptypeid == 5 && $module->id != 18 && $module->id != 19 && $module->id != 20 && $module->id != 30 && $module->id != 31 && $module->id != 35 && $module->id != 36 && $module->id != 37 && $module->id != 38 && $module->id != 40 && $module->id != 41 && $module->id != 11 && $module->id != 43  && $module->id != 44 && $module->id != 45 && $module->id != 39 && $module->id != 49 && $module->id != 54 && $module->id != 60 && $module->id != 61 && $module->id != 62) {
				$show = true;
			}
			if($app->apptypeid != 5) {
				$show = true;
			}
			?>
			<?php if($module->id == 35 ) {
				$show = false;
			} ?>

            <?php if($module->controller == 'forms' ) {
				$show = true;
			} ?>

			<?php if($show) : ?>
			<?php if($launchers != null && $launchers != false) {
				foreach($launchers as $launcher) {
					if($launcher->moduletypeid == $module->id) {
						$module->title = $launcher->title;
						if($module->id == 10 && strtolower($launcher->title) == 'sessions') {
							$module->title = __('Sessions and Speakers');
						}
					}
				}
			}?>

            <?php if($module->controller == 'forms') : ?>
				<?php if($launchers != null && $launchers != false): ?>
                    <?php foreach ($launchers as $launcher): ?>
                        <?php if ($launcher->moduletypeid == $module->id): ?>
                            <?php if (!empty($launcher->active)): ?>
                                <?php $activated++; ?>
                                    <div class="module active">
                                        <a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editlink"><span class="moduletitle"><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
											<span class="module-availabilty">
												<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
											</span>
                                        </a>
                                        <!-- <a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editimage"><i class="icon-pencil"></i></a> -->
                                        <!-- <a href="<?= site_url('forms/editlauncher/'.$launcher->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
                                        <a href="<?= site_url('forms/module/deactivate/'.$event->id.'/'.$launcher->id) ?>" class="activate delete">&nbsp;</a>
                                    </div>
                            <?php elseif($launcher->title == $module->name && $launcher->title == "Form"): ?>
                                    <div class="module inactive">
                                    	<a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editlink">
                                        <span class="moduletitle"><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
										<span class="module-availabilty">
											<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
											<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
											<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
										</span>
										</a>
                                        <a href="<?= site_url('forms/add/'.$event->id.'/event') ?>" id="activateForm" class="activate add white">&nbsp;</a>
                                    </div>
                            <?php elseif(!isset($launcher->topurlevent)): ?>
	                                    <div class="module inactive">
	                                    	<a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editlink">
	                                        <span class="moduletitle"><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
											<span class="module-availabilty">
												<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
											</span>
											</a>
	                                        <a href="<?= site_url('forms/module/activate/'.$event->id.'/'.$launcher->id . '/event') ?>" id="activateForm" class="activate add white">&nbsp;</a>
	                                    </div>
	                            <?php else:?>
                            		<div class="module inactive">
                            			<?php if(isset($launcher->emailsendresult) && $launcher->emailsendresult == 'yes') : ?>
											<a href="javascript:void(0);" onClick="r<eturn getEmailId('<?= site_url('formtemplate/add/'.$event->id.'/'.$launcher->moduletypeid) ?>', 'event');" class="editlink">
										<?php else: ?>
											<a href="<?= site_url('formtemplate/add/'.$event->id.'/'.$launcher->moduletypeid) ?>" class="editlink">
										<?php endif; ?>
                                        <span class="moduletitle"><?= $launcher->title;?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
										<span class="module-availabilty">
											<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
											<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
											<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
										</span>
										</a>
										<?php if(isset($launcher->emailsendresult) && $launcher->emailsendresult == 'yes') : ?>
											<a href="javascript:void(0);" id="activateForm" onClick="return getEmailId('<?= site_url('formtemplate/add/'.$event->id.'/'.$launcher->moduletypeid) ?>', 'event');" class="activate add white">&nbsp;</a>
										<?php else: ?>
											<a href="<?= site_url('formtemplate/add/'.$event->id.'/'.$launcher->moduletypeid) ?>" id="activateForm" class="activate add white">&nbsp;</a>
										<?php endif; ?>
                                    </div>                            
                            <?php endif ?>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php endif ?>

			<?php //no POI's
			 elseif($module->id != '4') : ?>
			<?php if(!($app->apptypeid == 6 && $module->id == 18)) : ?>
			<?php if ($module->active): ?>
			<?php $activated++; ?>
				<?php if(($module->name == "Sessions" && $app->apptypeid == 3 && $event->eventtypeid == 6) || ($module->name == "Sessions" && $app->apptypeid == 10)) : ?>
					<div class="module active">
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink"><span class="moduletitle"><?= __('Dates & Line-up') ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
						<span class="module-availabilty">
							<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
						</span>
						</a>
						<!-- <a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a>
						<a href="<?= site_url('module/edit/'.$module->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php elseif($module->id == 18) : ?>
					<div class="module active">
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink"><span class="moduletitle"><?= __('Artists') ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
						<span class="module-availabilty">
							<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
						</span>
						</a>
<!-- 						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a>
						<a href="<?= site_url('module/edit/'.$module->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php else: ?>
					<div class="module active">
						<?php if($module->id == 21) : ?>
							<a href="<?= site_url('event/edit/'.$event->id.'/') ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty">
								<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
							</span>
							</a>
							<!-- <a href="<?= site_url('event/edit/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a> -->
						<?php elseif($module->controller == 'webmodule') : ?>
							<a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$module->launcherid) ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty">
								<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
							</span>
							</a>
						<?php else: ?>
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
						<span class="module-availabilty">
							<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
						</span>
						</a>
						<!-- <a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a> -->
						<?php endif; ?>
						<?php if($module->id != 28) : ?>
						<!-- <a href="<?= site_url('module/edit/'.$module->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
						<?php endif; ?>
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php endif; ?>
			<?php else: ?>
				<?php if(($module->name == "Sessions" && $app->apptypeid == 3 && $event->eventtypeid == 6) || ($module->name == "Sessions" && $app->apptypeid == 10)) : ?>
						<div class="module inactive">
							<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink">
							<span class="moduletitle"><?=__('Dates & Line-up')?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty">
								<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
							</span>
							</a>
							<a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
						</div>
				<?php elseif ($module->name == "POI's on interactive map"): ?>
					<div class="module inactive activatemap">
						<span class="moduletitle"><?= $module->name ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
						<a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
					</div>
					<?php if (!$mapactive): ?>
					<script type="text/javascript" charset="utf-8">
						$(document).ready(function() {
							$('.module.inactive.activatemap a.activate').click(function() {
								jConfirm('<?= __('Make sure you have installed the interactive map!<br />Do you want to install floorplan first?') ?>', '<?= __('Install POI-module') ?>', function(r) {
									if(r == true) {
										window.location = "<?= site_url('event/module/activate/'.$event->id.'/5/') ?>";
										return true;
									} else {
										jAlert('<?= __('POIs module not installed!') ?>', '<?= __('Info') ?>');
										return false;
									}
								});
								return false;
							});
						});
					</script>
					<?php endif ?>
				<?php else: ?>
					<div class="module inactive">
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink">
						<span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
						<span class="module-availabilty">
							<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
						</span>
						</a>
						<?php if($module->id != 20) : ?>
                        <a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
						<?php else: ?>
						<a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" id="activatePush" class="activate add white">&nbsp;</a>
						<?php endif; ?>
					</div>
				<?php endif ?>
			<?php endif ?>
			<?php endif; ?>
			<?php endif; ?>
			<?php endif; ?>
		<?php endforeach ?>
		<?php foreach($dynamicModules as $dynMod) : ?>
			<?php if ($dynMod->active): ?>
				<div class="module active">
						<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" class="editlink"><span class="moduletitle"><?= $dynMod->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$dynMod->module)?>"> <i class="icon-question-sign"></i></span></span>
						<span class="module-availabilty">
							<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
							<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
							<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
						</span>
						</a>
<!-- 					<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" class="editimage"><i class="icon-pencil"></i></a>
					<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
					<a href="<?= site_url('dynamiclauncher/deactivate/'.$event->id.'/event/'.$dynMod->id) ?>" class="activate delete">&nbsp;</a>
				</div>
			<?php else: ?>
				<div class="module inactive">
					<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" class="editlink">
					<span class="moduletitle"><?= $dynMod->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$dynMod->module)?>"> <i class="icon-question-sign"></i></span></span>
					<span class="module-availabilty">
							<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
							<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
							<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
					</span>
					</a>
					<a href="<?= site_url('dynamiclauncher/activate/'.$event->id.'/event/'.$dynMod->id) ?>" class="activate add white">&nbsp;</a>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
		<?php else: ?>
			<p><?= __('No modules found for this type of event.') ?></p>
		<?php endif ?>
	</div>
</div>







<?php elseif($app->familyid == 4) :?>
<h1 class="eventname"><?= $event->name ?></h1>
<div>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<div class="eventinfo">
		<?php if ($event->eventlogo != '' && file_exists($this->config->item('imagespath') . $event->eventlogo)): ?>
			<div class="eventlogo" style="background:transparent url('<?= image_thumb($event->eventlogo, 50, 50) ?>') no-repeat center center">&nbsp;</div>
		<?php else: ?>
			<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
		<?php endif ?>
		<?php if($event->datefrom == '1970-01-01') : ?>
			<span class="info_title"><?= __('Permanent event') ?></span>
		<?php else: ?>
			<span class="info_title"><?= __('From - To:') ?></span> <?= date('d/m/Y', strtotime($event->datefrom)) ?> - <?= date('d/m/Y', strtotime($event->dateto)) ?>
		<?php endif; ?>
	</div>
	<?php if (!$this->session->userdata('mijnevent')): ?>
	<!-- <a href="<?= site_url('event/remove/'.$event->id) ?>" class="delete btn" id="deletebtn" style="margin-top:-65px;margin-right:10px;float:right;"><span><?= __('Delete event') ?></span></a> -->
	<?php if($app->apptypeid != 10) : ?>
		<!-- <a href="<?= site_url('event/travelinfo/'.$event->id) ?>" class="travel btn"><span><?= __('Travel info') ?></span></a> -->
	    <!-- <a href="<?= site_url('schedule/event/'.$event->id) ?>" class="schedule btn"><span><?= __('Schedule') ?></span></a> -->
	<?php endif; ?>
	<!-- <a href="<?= site_url('event/edit/'.$event->id) ?>" class="edit btn"><span><?= __('Edit event') ?></span></a> -->
	<?php endif ?>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#deletebtn').click(function() {
				jConfirm('<?= __('This will <b>remove</b> this event!<br />! This cannot be undone!') ?>', '<?= __('Remove Event') ?>', function(r) {
					if(r == true) {
						window.location = '<?= site_url("event/remove/".$event->id)?>';
						return true;
					} else {
						jAlert('<?= __("Event not removed!") ?>', '<?= __("Info") ?>');
						return false;
					}
				});
				return false;
			});
		});
	</script>
	<!-- <h2><?= __('Menu items') ?></h2> -->
	<a href="dynamiclauncher/add/<?=$event->id?>/event" class="btn primary" style="float:right;"><?= __('Add Web Module') ?></a>
    <a href="forms/add/<?=$event->id?>/event" class="btn primary" style="float:right; margin-right:5px;"><?= __('Add Form Module') ?></a>
    <?php if($app->id == 1) : ?>
    <a href="dataimport/event/<?=$event->id?>/event" class="btn primary" style="float:right; margin-right:5px;"><?= __('Import Catalog') ?></a>
	<?php endif; ?>
	<br clear="all" />
	<div class="modules">
		<?php if ($eventmodules != FALSE): ?>
		<?php $activated = 0; ?>
		<?php
			$mapactive = FALSE;
			foreach ($eventmodules as $module):
				if ($module->name == "Interactive map"):
					if ($module->active):
						$mapactive = TRUE;
					endif;
				endif;
			endforeach;
		?>
		<?php foreach ($eventmodules as $module): ?>
			<?php
				$show = false;
			if($app->apptypeid == 5 && $module->id != 18 && $module->id != 19 && $module->id != 20 && $module->id != 30 && $module->id != 31 && $module->id != 35 && $module->id != 36 && $module->id != 37 && $module->id != 38 && $module->id != 39 && $module->id != 40 && $module->id != 41 && $module->id != 42 && $module->id != 11 && $module->id != 43  && $module->id != 44 && $module->id != 45) {
				$show = true;
			}
			if($app->apptypeid != 5) {
				$show = true;
			}
			?>
			<?php if($module->id == 35 ) {
				$show = false;
			} ?>

            <?php if($module->controller == 'forms' ) {
				$show = true;
			} ?>

			<?php if($show) : ?>
		<?php if($launchers != null && $launchers != false) {
				foreach($launchers as $launcher) {
					if($launcher->moduletypeid == $module->id) {
						$module->title = $launcher->title;
						if($module->id == 10 && strtolower($launcher->title) == 'sessions') {
							$module->title = __('Sessions and Speakers');
						}
					}
				}
			}?>


            <?php if($module->controller == 'forms') : ?>
				<?php if($launchers != null && $launchers != false): ?>

                    <?php foreach ($launchers as $launcher): ?>

                            <?php if ($launcher->moduletypeid == $module->id): ?>
                                    <?php if ($launcher->active): ?>
                                        <?php $activated++; ?>
                                            <div class="module active">
                                                <a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editlink"><span><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span></a>
                                                <a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editimage"><i class="icon-pencil"></i></a>
                                                <a href="<?= site_url('forms/editlauncher/'.$launcher->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a>
                                                <a href="<?= site_url('forms/module/deactivate/'.$event->id.'/'.$launcher->id) ?>" class="activate delete">&nbsp;</a>
                                            </div>

                                    <?php elseif($launcher->title == $module->name && $launcher->title = "Form"): ?>
                                            <div class="module inactive">
                                            	<a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editlink">
                                                <span><?= $launcher->title ?></span>
                                            	</a>
                                                <a href="<?= site_url('forms/add/'.$event->id.'/event') ?>" id="activateForm" class="activate add white">&nbsp;</a>
                                            </div>

                                    <?php else: ?>
                                            <div class="module inactive">
                                            	<a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editlink">
                                                <span><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
                                            	</a>
                                                <a href="<?= site_url('forms/module/activate/'.$event->id.'/'.$launcher->id) ?>" id="activate" class="activate add white">&nbsp;</a>
                                            </div>

                                    <?php endif ?>

                            <?php endif ?>

                    <?php endforeach ?>

                <?php endif ?>

			<?php //no POI's
			 elseif($module->id != '4') : ?>
			<?php if(!($app->apptypeid == 6 && $module->id == 18)) : ?>
			<?php if ($module->active): ?>
			<?php $activated++; ?>
				<?php if(($module->name == "Sessions" && $app->apptypeid == 3 && $event->eventtypeid == 6) || ($module->name == "Sessions" && $app->apptypeid == 10)) : ?>
					<div class="module active">
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink"><span><?= __('Dates & Line-up') ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span></a>
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a>
						<a href="<?= site_url('module/edit/'.$module->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a>
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php elseif($module->id == 18) : ?>
					<div class="module active">
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink"><span><?= __('Artists') ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span></a>
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a>
						<a href="<?= site_url('module/edit/'.$module->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a>
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php else: ?>
					<div class="module active">
						<?php if($module->id == 21) : ?>
							<a href="<?= site_url('event/edit/'.$event->id.'/') ?>" class="editlink"><span><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span></a>
							<a href="<?= site_url('event/edit/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a>
						<?php else: ?>
							<?php if ($this->session->userdata('mijnevent')): ?><a href="<?= site_url($module->controller.'/event/'.$event->id) ?>"><?php endif ?>
							<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink"><span><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span></a>
							<?php if ($this->session->userdata('mijnevent')): ?></a><?php endif ?>
							<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a>
						<?php endif; ?>
						<?php if($module->id != 28) : ?>
						<a href="<?= site_url('module/edit/'.$module->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a>
						<?php endif; ?>
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php endif; ?>
			<?php else: ?>
				<?php if(($module->name == "Sessions" && $app->apptypeid == 3 && $event->eventtypeid == 6) || ($module->name == "Sessions" && $app->apptypeid == 10)) : ?>
						<div class="module inactive">
							<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink">
							<span><?=__('Dates & Line-up')?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							</a>
							<a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
						</div>
				<?php elseif ($module->name == "POI's on interactive map"): ?>
					<div class="module inactive activatemap">
						<span><?= $module->name ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
						<a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
					</div>
					<?php if (!$mapactive): ?>
					<script type="text/javascript" charset="utf-8">
						$(document).ready(function() {
							$('.module.inactive.activatemap a.activate').click(function() {
								jConfirm('<?= __('Make sure you have installed the interactive map!<br />Do you want to install floorplan first?') ?>', '<?= __('Install POI-module') ?>', function(r) {
									if(r == true) {
										window.location = "<?= site_url('event/module/activate/'.$event->id.'/5/') ?>";
										return true;
									} else {
										jAlert('<?= __('POIs module not installed!') ?>', '<?= __('Info') ?>');
										return false;
									}
								});
								return false;
							});
						});
					</script>
					<?php endif ?>
				<?php else: ?>
					<?php //kortrijk expo mag sponsors niet zien ?>
						<div class="module inactive">
							<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink">
							<span><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							</a>
							<?php if($module->id != 20) : ?>
	                        <a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
							<?php else: ?>
							<a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" id="activatePush" class="activate add white">&nbsp;</a>
							<?php endif; ?>
						</div>
				<?php endif ?>
			<?php endif ?>
			<?php endif; ?>
			<?php endif; ?>
			<?php endif; ?>
		<?php endforeach ?>
		<?php foreach($dynamicModules as $dynMod) : ?>
			<?php if ($dynMod->active): ?>
				<div class="module active">
					<?php if ($this->session->userdata('mijnevent')): ?><a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>"><?php endif ?>
						<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" class="editlink"><span><?= $dynMod->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$dynMod->module)?>"> <i class="icon-question-sign"></i></span></span></a>
					<?php if ($this->session->userdata('mijnevent')): ?></a><?php endif ?>
					<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" class="editimage"><i class="icon-pencil"></i></a>
					<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a>
					<a href="<?= site_url('dynamiclauncher/deactivate/'.$event->id.'/event/'.$dynMod->id) ?>" class="activate delete">&nbsp;</a>
				</div>
			<?php else: ?>
				<div class="module inactive">
					<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" class="editlink">
					<span><?= $dynMod->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$dynMod->module)?>"> <i class="icon-question-sign"></i></span></span>
					</a>
					<a href="<?= site_url('dynamiclauncher/activate/'.$event->id.'/event/'.$dynMod->id) ?>" class="activate add white">&nbsp;</a>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
		<?php else: ?>
			<p><?= __('No modules found for this type of event.') ?></p>
		<?php endif ?>
	</div>
</div>

<?php else: ?>
<div>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
<!-- 	<div class="eventinfo">
		<?php if ($event->eventlogo != '' && file_exists($this->config->item('imagespath') . $event->eventlogo)): ?>
			<div class="eventlogo" style="background:transparent url('<?= image_thumb($event->eventlogo, 50, 50) ?>') no-repeat center center">&nbsp;</div>
		<?php else: ?>
			<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
		<?php endif ?>

		<?php if($event->datefrom == '1970-01-01') : ?>
			<span class="info_title"><?= __('Permanent event') ?></span>
		<?php else: ?>
			<span class="info_title"><?= __('From - To:') ?></span> <?= date('d/m/Y', strtotime($event->datefrom)) ?> - <?= date('d/m/Y', strtotime($event->dateto)) ?>
		<?php endif; ?>
		<br /><br/>
	</div> -->
	<?php if (!$this->session->userdata('mijnevent')): ?><br/>
	<!-- <a href="<?= site_url('event/remove/'.$event->id) ?>" class="delete btn" id="deletebtn" style="margin-top:-65px;margin-right:10px;float:right;"><span><?= __('Delete event'); ?></span></a> -->
	<?php if($app->apptypeid != 10) : ?>
	<!-- <a href="<?= site_url('event/travelinfo/'.$event->id) ?>" class="travel btn"><span><?= __('Travel info'); ?></span></a> -->
    <!-- <a href="<?= site_url('schedule/event/'.$event->id) ?>" class="schedule btn"><span><?= __('Schedule'); ?></span></a> -->
	<?php endif; ?>
	<!-- <a href="<?= site_url('event/edit/'.$event->id) ?>" class="edit btn"><span><?= __('Edit event'); ?></span></a> -->
	<?php endif ?>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#deletebtn').click(function() {
				jConfirm('<?= __('This will <b>remove</b> this event!<br />! This cannot be undone!') ?>', '<?= __('Remove Event') ?>', function(r) {
					if(r == true) {
						window.location = '<?= site_url("event/remove/".$event->id)?>';
						return true;
					} else {
						jAlert('<?= __('Event not removed!') ?>', '<?= __('Info') ?>');
						return false;
					}
				});
				return false;
			});

			// $('.editimage').css('display', 'none');
			// $('.active a').hover(
			// 	function() {
			// 		$(this).parent().find('.editimage').css('display', 'inline');
			// 	},
			// 	function() {
			// 		$(this).parent().find('.editimage').css('display', 'none');
			// 	}
			// );
		});
	</script>
	<!-- <h2><?=__('Menu items')?></h2> -->
	<a href="dynamiclauncher/add/<?=$event->id?>/event" class="btn primary" style="float:right;"><?= __('Add Web Module') ?></a>
	<a href="forms/add/<?=$event->id?>/event" class="btn primary" style="float:right; margin-right:5px;"><?= __('Add Form Module') ?></a>
    <?php if($app->id == 1) : ?>
    <a href="dataimport/event/<?=$event->id?>/event" class="btn primary" style="float:right; margin-right:5px;"><?= __('Import Catalog') ?></a>
	<?php endif; ?>
    <br clear="all" /><br/>
	<div class="modules">
		<?php if ($eventmodules != FALSE): ?>
		<?php $activated = 0; ?>
		<?php
			$mapactive = FALSE;
			foreach ($eventmodules as $module):
				if ($module->name == "Interactive map"):
					if ($module->active):
						$mapactive = TRUE;
					endif;
				endif;
			endforeach;
		?>
		<?php foreach($subflavors as $subflavor): ?>
		<h3 class="packageH2"><?=ucfirst($subflavor->name)?></h3>
		<?php foreach ($eventmodules as $module): ?>
		<?php if($module->package == $subflavor->name) : ?>
			<?php
				$show = false;
			if($app->apptypeid == 5 && $module->id != 18 && $module->id != 19 && $module->id != 20) {
				$show = true;
			}
			if($app->apptypeid != 5) {
				$show = true;
			}
			if(($app->apptypeid == 4 || $app->apptypeid == 11) && $module->id == 18) {
				$show = false;
			}
			?>

            <?php if($module->controller == 'forms' ) {
				$show = true;
			} ?>

			<?php if($show) : ?>
			<?php if($launchers != null && $launchers != false) {
				foreach($launchers as $launcher) {
					if($launcher->moduletypeid == $module->id) {
						$module->title = $launcher->title;

						if($module->id == 10 && strtolower($launcher->title) == 'sessions') {
							$module->title = __('Sessions and Speakers');
						}
						$module->launcherid = $launcher->id;
					}
				}
			}?>

            <?php if($module->controller == 'forms') : ?>
				<?php if($launchers != null && $launchers != false): ?>
                    <?php foreach ($launchers as $launcher): ?>
                        <?php if ($launcher->moduletypeid == $module->id): ?>
                            <?php if (!empty($launcher->active)): ?>
                                <?php $activated++; ?>
                                    <div class="module active">
                                        <a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editlink"><span class="moduletitle"><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
											<span class="module-availabilty">
												<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
											</span>
                                        </a>
                                        <!-- <a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editimage"><i class="icon-pencil"></i></a> -->
                                        <!-- <a href="<?= site_url('forms/editlauncher/'.$launcher->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
                                        <a href="<?= site_url('forms/module/deactivate/'.$event->id.'/'.$launcher->id) ?>" class="activate delete">&nbsp;</a>
                                    </div>
                            <?php elseif($launcher->title == $module->name && $launcher->title == "Form"): ?>
                                    <div class="module inactive">
                                    	<a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editlink">
                                        <span class="moduletitle"><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
										<span class="module-availabilty">
											<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
											<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
											<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
										</span>
										</a>
                                        <a href="<?= site_url('forms/add/'.$event->id.'/event') ?>" id="activateForm" class="activate add white">&nbsp;</a>
                                    </div>
                            <?php elseif(!isset($launcher->topurlevent)): ?>
	                                    <div class="module inactive">
	                                    	<a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$launcher->id) ?>" class="editlink">
	                                        <span class="moduletitle"><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
											<span class="module-availabilty">
												<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
											</span>
											</span>
	                                        <a href="<?= site_url('forms/module/activate/'.$event->id.'/'.$launcher->id . '/event') ?>" id="activateForm" class="activate add white">&nbsp;</a>
	                                    </div>
	                            <?php else:?>
                            		<div class="module inactive">
                            			<?php if(isset($launcher->emailsendresult) && $launcher->emailsendresult == 'yes') : ?>
											<a href="javascript:void(0);" onClick="return getEmailId('<?= site_url('formtemplate/add/'.$event->id.'/'.$launcher->moduletypeid) ?>', 'event');" class="editlink">
										<?php else: ?>
											<a href="<?= site_url('formtemplate/add/'.$event->id.'/'.$launcher->moduletypeid) ?>" class="editlink">
										<?php endif; ?>
                                        <span class="moduletitle"><?= $launcher->title;?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
										<span class="module-availabilty">
											<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
											<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
											<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
										</span>
										</a>
										<?php if(isset($launcher->emailsendresult) && $launcher->emailsendresult == 'yes') : ?>
											<a href="javascript:void(0);" id="activateForm" onClick="return getEmailId('<?= site_url('formtemplate/add/'.$event->id.'/'.$launcher->moduletypeid) ?>', 'event');" class="activate add white">&nbsp;</a>
										<?php else: ?>
											<a href="<?= site_url('formtemplate/add/'.$event->id.'/'.$launcher->moduletypeid) ?>" id="activateForm" class="activate add white">&nbsp;</a>
										<?php endif; ?>
                                    </div>                            
                            <?php endif ?>

                        <?php endif ?>

                    <?php endforeach ?>

                <?php endif ?>

			<?php //no POI's
			 elseif($module->id != '4') : ?>
			<?php if(!($app->apptypeid == 6 && $module->id == 18)) : ?>
			<?php if ($module->active): ?>
			<?php $activated++; ?>
				<?php if(($module->name == "Sessions" && $app->apptypeid == 3 && $event->eventtypeid == 6) || ($module->name == "Sessions" && $app->apptypeid == 10)) : ?>
					<div class="module active">
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink"><span class="moduletitle"><?= __('Dates & Line-up') ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
						<span class="module-availabilty">
							<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
						</span>
						</a>
						<!-- <a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a> -->
						<!-- <a href="<?= site_url('module/edit/'.$module->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php elseif($module->id == 18) : ?>
					<div class="module active">
						<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink"><span class="moduletitle"><?= __('Artists') ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
						<span class="module-availabilty">
							<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
						</span>
						</a>
						<!-- <a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a> -->
						<!-- <a href="<?= site_url('module/edit/'.$module->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php elseif($module->id == 2 && $event->id == 1220) : //quick fix for cavalorv2?>
					<div class="module active">
						<a href="<?= site_url('groups/view/10162/event/1220/exhibitors') ?>" class="editlink"><span class="moduletitle"><?= __('Product Guide') ?></span>
						<span class="module-availabilty">
							<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
							<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
						</span>
						</a>
						<!-- <a href="<?= site_url('groups/view/10162/event/1220/exhibitors') ?>" class="editimage"><i class="icon-pencil"></i></a> -->
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/2') ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php else: ?>
					<div class="module active">
						<?php if($module->id == 21) : ?>
							<a href="<?= site_url('event/edit/'.$event->id.'/'.$module->launcherid) ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
								<span class="module-availabilty">
									<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
									<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
									<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
								</span>
							</a>
							<!-- <a href="<?= site_url('event/edit/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a> -->
						<?php elseif($module->controller == 'webmodule') : ?>
							<a href="<?= site_url($module->controller.'/event/'.$event->id.'/'.$module->launcherid) ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty">
								<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
							</span>
							</a>
						<?php else : ?>
							<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
								<span class="module-availabilty">
									<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
									<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
									<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
								</span>
							</a>
							<!-- <a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editimage"><i class="icon-pencil"></i></a> -->
						<?php endif; ?>
						<?php if($module->id != 28) : ?>
						<!-- <a href="<?= site_url('module/edit/'.$module->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
						<?php endif; ?>
						<a href="<?= site_url('event/module/deactivate/'.$event->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
					</div>
				<?php endif; ?>
			<?php else: ?>
				<?php if(($module->name == "Sessions" && $app->apptypeid == 3 && $event->eventtypeid == 6) || ($module->name == "Sessions" && $app->apptypeid == 10)) : ?>
						<div class="module inactive">
							a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink">
							<span class="moduletitle"><?= __('Dates & Line-up') ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty">
								<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
							</span>
							</a>
							<a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
						</div>
				<?php elseif ($module->name == "POI's on interactive map"): ?>
					<div class="module inactive activatemap">
						<span><?= $module->name ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
						<a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
					</div>
					<?php if (!$mapactive): ?>
					<script type="text/javascript" charset="utf-8">
						$(document).ready(function() {
							$('.module.inactive.activatemap a.activate').click(function() {
								jConfirm('<?=__('Make sure you have installed the interactive map!')?> <br /> <?=__('Do you want to install floorplan first?')?>', '<?=__('Install POI-module')?>', function(r) {
									if(r == true) {
										window.location = "<?= site_url('event/module/activate/'.$event->id.'/5/') ?>";
										return true;
									} else {
										jAlert("<?=__('POI\'s module not installed!')?>', '<?=__('Info')?>");
										return false;
									}
								});
								return false;
							});
						});
					</script>
					<?php endif ?>
				<?php else: ?>
						<div class="module inactive">
							<a href="<?= site_url($module->controller.'/event/'.$event->id.'/') ?>" class="editlink">
							<span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty">
								<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
							</span>
							</a>
							<?php if($module->id != 20) : ?>
	                        <a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
							<?php else: ?>
							<a href="<?= site_url('event/module/activate/'.$event->id.'/'.$module->id) ?>" id="activatePush" class="activate add white">&nbsp;</a>
							<?php endif; ?>
						</div>
				<?php endif ?>
			<?php endif ?>
			<?php endif; ?>
			<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
		<?php endforeach ?>
		<?php endforeach; ?>
		<?php foreach($dynamicModules as $dynMod) : ?>
			<?php if ($dynMod->active): ?>
				<div class="module active">
					<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" class="editlink"><span class="moduletitle"><?= $dynMod->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$dynMod->module)?>"> <i class="icon-question-sign"></i></span></span>
						<span class="module-availabilty">
							<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
							<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
							<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
						</span>
					</a>
					<!-- <a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" class="editimage"><i class="icon-pencil"></i></a> -->
					<!-- <a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" title="<?=__('Module Settings')?>" height="22px" /></a> -->
					<a href="<?= site_url('dynamiclauncher/deactivate/'.$event->id.'/event/'.$dynMod->id) ?>" class="activate delete">&nbsp;</a>
				</div>
			<?php else: ?>
				<div class="module inactive">
					<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/event/'.$event->id)?>" class="editlink">
					<span class="moduletitle"><?= $dynMod->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$dynMod->module)?>"> <i class="icon-question-sign"></i></span></span>
					<span class="module-availabilty">
							<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
							<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
							<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
					</span>
					</a>
					<a href="<?= site_url('dynamiclauncher/activate/'.$event->id.'/event/'.$dynMod->id) ?>" class="activate add white">&nbsp;</a>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
		<?php if($event->id == 1220) : //cavalor ?>
			<div class="module active">
				<a href="<?= site_url('venues') ?>" class="editlink"><span><?= __('Shop Locator') ?></span></a>
				<!-- <a href="<?= site_url('venues') ?>" class="editimage"><i class="icon-pencil"></i></a> -->
				<a href="<?= site_url('event/module/deactivate/'.$event->id.'/31') ?>" class="activate delete">&nbsp;</a>
			</div>
		<?php endif; ?>
		<?php else: ?>
			<p><?= __('No modules found for this type of event.') ?></p>
		<?php endif ?>

	</div>
</div>
<?php endif; ?>
<script>
    function getEmailId(jumpUrl, type)
    {
        var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;

        jPrompt('Enter your email address. You will receive an email for each submission.', '', 'Please enter your email', function(r)
        {
            if(r != null){
                if(!r){jAlert('Please enter valid email.', 'Error'); return false;}

                if(regMail.test(r) == true){
                    window.location = jumpUrl + '/' + r + '/' + type;
                }
                else
                    jAlert('Invalid email.', 'Error');
                }
        });

        return false;
    }
</script>