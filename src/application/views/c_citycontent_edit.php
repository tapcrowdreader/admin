<style type="text/css">
iframe {
    margin-left:15px !important;
}
</style>
<div>
	<h1><?=__('Edit Content')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
            <div class="error"><?= __('Something went wrong with the image upload.') .'<br/>'.__(' Maybe the image size exceeds the maximum width or height') ?></div>
            <?php endif ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
            <?php $trans = _getTranslation('citycontent', $citycontent->id, 'title', $language->key); ?>
                <label for="title"><?=__('Title')?> <?= '(' . $language->name . ')'; ?>:</label>
                <input maxlength="255" type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$language->key, ($trans != null) ? $trans : $citycontent->title), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            <p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
                <label for="image"><?= __('Image:') ?></label>
                <span class="evtlogo" <?php if($citycontent->image != ''){ ?>style="background:transparent url('<?= image_thumb($citycontent->image, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
                <input type="file" name="image" id="image" value="" class="logoupload" />
                <br clear="all"/>
                <?php if($citycontent->image != '' && file_exists($this->config->item('imagespath') . $citycontent->image)){ ?><span><a href="<?= site_url('citycontent/removeimage/'.$citycontent->id) ?>" class="deletemap"><?=__('Remove')?></a></span><?php } ?>
            </p><br />
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("text_".$language->key) != '') ? 'class="error"' : '' ?>>
                <?php $trans = _getTranslation('citycontent', $citycontent->id, 'text', $language->key);
                $data = array(
                        'name'          => 'text_'.$language->key,
                        'id'            => 'text_'.$language->key,
                        'toolbarset'    => 'Travelinfo',
                        'value'         => htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('text_'.$language->key, ($trans != null) ? $trans : $citycontent->text)))))),
                        'width'         => '580px',
                        'height'        => '400'
                    );
                echo '<p style="min-height:0px;margin-top:-30px;"><label>'.__('Text').'('.$language->name.')</label></p>';
                echo form_fckeditor($data);
                ?>
            </p>
            <?php endforeach; ?>
            <?php endif; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('citycontent/app/'.$app->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>