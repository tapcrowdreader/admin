<div>
	<h2><?= __('What would you like to import?') ?></h2>
	<?php if ($this->session->flashdata("import_feedback") != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata("import_feedback") ?></div>
	<?php endif ?>
	<div class="importtype">
		<a href="<?= site_url('excelimport/upload/'.$event->id.'/sessions') ?>"><?=__('Sessions ')?>&rsaquo;&rsaquo;</a>
	</div>
</div>