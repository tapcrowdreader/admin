<?php header ("content-type: text/xml"); ?>
<?php echo'<?xml version="1.0" encoding="UTF-8"?>'; ?>
<expoplus version="1.0">
    <app>
        <eventlist>
            <?php if($events != null) { ?>
            <?php foreach($events as $e) : ?>
                <event>
                    <id><?php  echo $e->id; ?></id>
			<?php $eventname = str_replace(array('&', '&amp;',"'"),array('&amp;','&amp;',""), $e->name); ?>
                    <name><?php echo '<![CDATA[' . $e->name . ']]>';?></name>
                    <logo><?php if($e->eventlogo != '') { echo base_url().$e->eventlogo; } ?></logo>
                    <datefrom><?php echo $e->datefrom; ?></datefrom>
                    <dateto><?php echo $e->dateto; ?></dateto>
                    <eventtype id="<?php echo $e->eventtypeid; ?>"><?php echo $e->eventtypename; ?></eventtype>
			<?php if(isset($e->venue->lat)) { ?>
                    <lat><?php echo $e->venue->lat; ?></lat>
                    <lon><?php echo $e->venue->lon; ?></lon>
			<?php }  ?>
                    <timestamp><?php echo $e->timestamp; ?></timestamp>
					<maptimestamp>1283344489</maptimestamp>
                    <details><?php echo $e->details; ?></details>
                </event>
            <?php endforeach; ?>
            <?php } ?>
        </eventlist>
    </app>
</expoplus>
