<?php header ("content-type: text/xml"); ?>
<?php echo'<?xml version="1.0" encoding="UTF-8"?>'; ?>
<expoplus version="1.0">
    <event id="<?php echo $event->id; ?>">
        <generalinfo>
            <?php $eventname = str_replace(array('&', '&amp;',"'"),array('&amp;','&amp;',""), $event->name); ?>
            <name><?php echo '<![CDATA[' . $event->name . ']]>';?></name>
            <datefrom><?php echo $event->datefrom; ?></datefrom>
            <dateto><?php echo $event->dateto; ?></dateto>
            <logo><?php if($event->eventlogo != '') { echo base_url().$event->eventlogo; } ?></logo>
        </generalinfo>
        <schedule><?php if(isset($schedule)) { ?>
        <?php foreach($schedule as $s) : ?>
            <scheduleitem key="<?php echo $s->key; ?>"><?php echo $s->caption ?></scheduleitem>
        <?php endforeach; ?>
        <?php } ?></schedule> 
       	<?php if(isset($map)) { ?>
			<map>
            	<imageurl><?php echo base_url().''.$map->imageurl; ?></imageurl>
				<red>0</red>
				<green>125</green>
				<blue>0</blue>
			</map>
        <?php }else { ?>
	        <map />
		<?php } ?>

        <?php if(isset($newsitems)) { ?>
        <newsitems>
        <?php foreach($newsitems as $n) : ?>            
            <newsitem>
                <title><?php echo $n->title; ?></title>
                <text><?php echo $n->txt;?> </text>
                <image><?php echo base_url()."newsimages/c_".$n->image; ?></image>
            </newsitem>
        <?php endforeach; ?>
        </newsitems>
        <?php }else echo("<news />") ?>
        <?php if(isset($agendaitems)) { ?>
        <agenda>
        <?php foreach($agendaitems as $a) : ?>
            <agendaitem>
                <title><?php  echo $a->title; ?></title>
                <text><?php echo $a->txt;?></text>
                <place><?php echo $a->place ?></place>
                <start><?php echo $a->starttime ?></start>
                <end><?php echo $a->endtime ?></end>
            </agendaitem>
        <?php endforeach; ?>
        </agenda>
            <?php }else { ?>
        <agenda />
            <?php } ?>
      <pois>
            <?php if(isset($pois)) { ?>
          <poilist>
                <?php foreach($pois as $p) : ?>
                    <poi>
                        <poicategories>
                            <poicategoryid><?php echo $p->poitypeid; ?></poicategoryid>
                        </poicategories>
                        <name><?php  echo $p->name; ?></name>
                        <x1><?php echo $p->x1; ?></x1>
                        <y1><?php echo $p->y1; ?></y1>
                        <width><?php echo $p->x2; ?></width>
                        <height><?php echo $p->y2; ?></height>
                    </poi>
                <?php endforeach; ?>
             </poilist>
            <?php } else { ?>
             <poilist />
             <?php } ?>
         <poicategories>
            <?php if(isset($poitypes) && $poitypes != null && $poitypes != false) { ?>
                <?php foreach($poitypes as $pt) : ?>
                <poicategory id="<?php echo $pt->id; ?>">
                    <name><?php echo $pt->name; ?></name>
                    <icon><?php echo $pt->icon; ?></icon>
                </poicategory>
                <?php endforeach; ?>
            <?php }?>
         </poicategories>
      </pois>
      <exhibitors>
         <exhibitorlist>
         <?php if(isset($exhibitors)) { ?>
            <?php foreach($exhibitors as $e) : ?>
            <exhibitor id="<?php echo $e->id;?>">
		<?php //$exhiname = $e->name; ?>
		<?php //$exhiname = str_replace(array('&', '&amp;'),array('&amp;','&amp;'), $exhiname); ?>
               <name><?php echo '<![CDATA[' . $e->name . ']]>';?></name>
               <boothnumber><?php echo $e->booth; ?></boothnumber>
               <?php if($e->imageurl != "") { ?>
                    <image><?php echo base_url()."exhibitorimages/c_".$e->imageurl; ?></image>
               <?php } else { ?>
                    <image />
               <?php } ?>
               <x1><?php echo $e->x1; ?></x1>
               <y1><?php echo $e->y1; ?></y1>
               <width><?php echo $e->x2; ?></width>
               <height><?php echo $e->y2; ?></height>
               <description><?php echo $e->description; ?></description>
               <?php $website = str_replace(array('&', '&amp;',"'"),array('&amp;','&amp;',""), $e->web); ?>
               <homepage><?php echo $website; ?></homepage>
               <?php if($e->tel != "") {
                    $e->tel = str_replace('(0)', "", $e->tel);
                    $e->tel = str_replace(' ', "", $e->tel);
                    $e->tel = str_replace('+', "00", $e->tel);
               } ?>
               <telephone><?php echo $e->tel; ?></telephone>
               <?php if(isset($e->cats)) { ?>
                    <exhibitorcategories>
                        <?php foreach($e->cats as $cat) { ?>
                            <exhibitorcategoryid><?php echo $cat->exhibitorcategoryid; ?></exhibitorcategoryid>
                        <?php } ?>
                   </exhibitorcategories>
                   <?php } else { ?>
                   <exhibitorcategories />
               <?php } ?>
               <?php if(isset($e->brands)) { ?>
                    <exhibitorbrands>
                        <?php foreach($e->brands as $brand) { ?>
                            <exhibitorbrandid><?php echo $brand->exhibitorbrandid; ?></exhibitorbrandid>
                        <?php } ?>
                   </exhibitorbrands>
                   <?php } else { ?>
                   <exhibitorbrands />
               <?php } ?>

            </exhibitor>
            <?php endforeach; ?>
         <?php }?>
         </exhibitorlist>
         <exhibitorcategories>
            <?php if(isset($exhibitorcategories)) { ?>
<?php foreach($exhibitorcategories as $ec) : ?>
    <exhibitorcategory id="<?php  echo $ec->id; ?>">
        <?php $exhicatname = str_replace(array('&', '&amp;',"'"),array('&amp;','&amp;',""), $ec->name); ?>
       <name><?php echo '<![CDATA[' . $ec->name . ']]>';?></name>
    </exhibitorcategory>
<?php endforeach; ?>
<?php } ?>
         </exhibitorcategories>
         <exhibitorbrands>
            <?php if(isset($exhibitorbrands)) { ?>
<?php foreach($exhibitorbrands as $eb) : ?>
    <exhibitorbrand id="<?php  echo $eb->id; ?>">
        <?php $exhibrandname = str_replace(array('&', '&amp;'),array('&amp;','&amp;'), $eb->name); ?>
       <name><?php echo '<![CDATA[' . $eb->name . ']]>';?></name>
    </exhibitorbrand>
<?php endforeach; ?>
<?php } ?>
         </exhibitorbrands>
      </exhibitors>
   </event>
</expoplus>
