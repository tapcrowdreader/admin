<?php header ("content-type: text/xml"); ?>
<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>
<expoplus version="1.0">
	<schedule>
	<?php if(isset($schedule)) { ?>
	<?php foreach($schedule as $s) : ?>
		<scheduleitem>
			<key><?= $s->key ?></key>
			<caption><?= $s->caption ?></caption>
		</scheduleitem>
	<?php endforeach; ?>
	<?php } ?>
	</schedule> 
	<?php if(isset($map)) { ?>
	<map>
		<imageurl><?= base_url().''.$map->imageurl ?></imageurl>
		<red>0</red>
		<green>125</green>
		<blue>0</blue>
	</map>
	<?php } else { ?>
	<map />
	<?php } ?>
	<newsitems>
		<newsitem>
			<name><?= '<![CDATA[' . $event->name . ']]>' ?></name>
			<datefrom><?= date('d/m/Y', strtotime($event->datefrom)) ?></datefrom>
			<dateto><?= date('d/m/Y', strtotime($event->dateto)) ?></dateto>
			<logo><?= ($event->eventlogo != '') ? base_url().$event->eventlogo : "" ?></logo>
			<title></title>
			<text></text>
			<image></image>
			<type>info</type>
		</newsitem>
	<?php if(isset($newsitems)) { ?>
	<?php foreach($newsitems as $n) : ?>
		<newsitem>
			<name></name>
			<datefrom></datefrom>
			<dateto></dateto>
			<logo></logo>
			<title><?= $n->title ?></title>
			<text><?= $n->txt ?> </text>
			<image><?= $n->image ?></image>
			<type>news</type>
		</newsitem>
	<?php endforeach; ?>
	<?php } ?>
	</newsitems>
	<?php if(isset($agendaitems)) { ?>
	<agenda>
	<?php foreach($agendaitems as $a) : ?>
		<agendaitem>
			<title><?= $a->title ?></title>
			<text><?= $a->txt ?></text>
			<place><?= $a->place ?></place>
			<start><?= date('d/m/Y', strtotime($a->starttime)) ?></start>
			<end><?= date('d/m/Y', strtotime($a->endtime)) ?></end>
		</agendaitem>
	<?php endforeach; ?>
	</agenda>
	<?php } else { ?>
	<agenda />
	<?php } ?>
	<pois>
	<?php if(isset($pois)) { ?>
		<poilist>
		<?php foreach($pois as $p) : ?>
			<poi>
				<poicategories>
					<poicategoryid><?= $p->poitypeid ?></poicategoryid>
				</poicategories>
				<name><?= $p->name ?></name>
				<x1><?= $p->x1 ?></x1>
				<y1><?= $p->y1 ?></y1>
				<width><?= $p->x2 ?></width>
				<height><?= $p->y2 ?></height>
			</poi>
		<?php endforeach; ?>
		</poilist>
	<?php } else { ?>
		<poilist />
	<?php } ?>
		<poicategories>
		<?php if(isset($poitypes)) { ?>
		<?php foreach($poitypes as $pt) : ?>
			<poicategory id="<?= $pt->id ?>">
				<name><?= $pt->name ?></name>
				<icon><?= $pt->icon ?></icon>
			</poicategory>
		<?php endforeach; ?>
		<?php } ?>
		</poicategories>
	</pois>
	<exhibitors>
		<exhibitorlist>
		<?php if(isset($exhibitors)) { ?>
		<?php foreach($exhibitors as $e) : ?>
		<exhibitor>
			<id><?= $e->id ?></id>
			<name><?= '<![CDATA[' . $e->name . ']]>' ?></name>
			<boothnumber><?= $e->booth ?></boothnumber>
			<?php if($e->imageurl != "") { ?>
			<image><?= base_url().$e->imageurl ?></image>
			<?php } else { ?>
			<image />
			<?php } ?>
			<x1><?= $e->x1 ?></x1>
			<y1><?= $e->y1 ?></y1>
			<width><?= $e->x2 ?></width>
			<height><?= $e->y2 ?></height>
			<description><?= $e->description ?></description>
			<homepage><?= str_replace(array('&', '&amp;',"'"),array('&amp;','&amp;',""), $e->web) ?></homepage>
			<?php if($e->tel != "") {
				$e->tel = str_replace('(0)', "", $e->tel);
				$e->tel = str_replace(' ', "", $e->tel);
				$e->tel = str_replace('+', "00", $e->tel);
			} ?>
			<telephone><?= $e->tel ?></telephone>
			<?php if(isset($e->cats)) { ?>
			<exhibitorcategories>
			<?php foreach($e->cats as $cat) { ?>
				<exhibitorcategoryid><?= $cat->exhibitorcategoryid ?></exhibitorcategoryid>
			<?php } ?>
			</exhibitorcategories>
			<?php } else { ?>
			<exhibitorcategories />
			<?php } ?>
			<?php if(isset($e->brands)) { ?>
			<exhibitorbrands>
			<?php foreach($e->brands as $brand) { ?>
				<exhibitorbrandid><?= $brand->exhibitorbrandid ?></exhibitorbrandid>
			<?php } ?>
			</exhibitorbrands>
			<?php } else { ?>
			<exhibitorbrands />
			<?php } ?>
		</exhibitor>
		<?php endforeach; ?>
		<?php }?>
		</exhibitorlist>
		<exhibitorcategories>
		<?php if(isset($exhibitorcategories) && count($exhibitorcategories) > 1) { ?>
		<?php foreach($exhibitorcategories as $ec) : ?>
			<exhibitorcategory>
				<id><?= $ec->id ?></id>
				<name><?= '<![CDATA[' . $ec->name . ']]>' ?></name>
			</exhibitorcategory>
		<?php endforeach; ?>
		<?php } ?>
		</exhibitorcategories>
		<exhibitorbrands>
		<?php if(isset($exhibitorbrands) && count($exhibitorbrands) > 1) { ?>
		<?php foreach($exhibitorbrands as $eb) : ?>
			<exhibitorbrand>
				<id><?= $eb->id ?></id>
				<name><?= '<![CDATA[' . $eb->name . ']]>';?></name>
			</exhibitorbrand>
		<?php endforeach; ?>
		<?php } ?>
		</exhibitorbrands>
	</exhibitors>
	<sessions>
	<?php if (isset($sessions)): ?>
	<?php foreach ($sessions as $sg): ?>
		<session>
			<type>header</type>
			<id></id>
			<sessiongroupid><?= $sg->id ?></sessiongroupid>
			<order><?= $sg->order ?></order>
			<name><?= $sg->name ?></name>
			<description></description>
			<starttime></starttime>
			<endtime></endtime>
			<speaker></speaker>
			<location></location>
			<allowAddToFavorites></allowAddToFavorites>
			<date></date>
		</session>
		<?php foreach ($sg->sessions as $session): ?>
		<session>
			<type>session</type>
			<id><?= $session->id ?></id>
			<sessiongroupid><?= $session->sessiongroupid ?></sessiongroupid>
			<order><?= $session->order ?></order>
			<name><?= '<![CDATA[' . $session->name . ']]>' ?></name>
			<description><?= '<![CDATA[' . $session->description . ']]>' ?></description>
			<starttime><?= $session->starttime ?></starttime>
			<endtime><?= $session->endtime ?></endtime>
			<speaker><?= $session->speaker ?></speaker>
			<location><?= $session->location ?></location>
			<allowAddToFavorites><?= $session->allowAddToFavorites ?></allowAddToFavorites>
			<date><?= $session->date ?></date>     
		</session>
		<?php endforeach ?>
	<?php endforeach ?>
	<?php endif ?>
	</sessions>
	<attendees>
	<?php if (isset($attendees) && count($attendees) > 0): ?>
	<?php foreach ($attendees as $attendee): ?>
	<attendee>
		<id><?= $attendee->id ?></id>
		<eventid><?= $attendee->eventid ?></eventid>
		<venueid><?= $attendee->venueid ?></venueid>
		<businessid><?= $attendee->businessid ?></businessid>
		<name><?= '<![CDATA[' . $attendee->name . ']]>' ?></name>
		<firstname><?= '<![CDATA[' . $attendee->firstname . ']]>' ?></firstname>
		<company><?= '<![CDATA[' . $attendee->company . ']]>' ?></company>
		<email><?= $attendee->email ?></email>
		<linkedin><?= $attendee->linkedin ?></linkedin>
		<phonenr><?= $attendee->phonenr ?></phonenr>
		<photo>http://www.mobilejuice.be/clients/nokia/attendees/<?= $attendee->id ?>.jpg</photo>
	</attendee>
	<?php endforeach ?>
	<?php endif ?>
	</attendees>
</expoplus>
