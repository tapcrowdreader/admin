<a href="<?= site_url('module/edit/23/venue/'.$venue->id) ?>" class="edit btn" style="float:right;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a>
<form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<h3><?= __('Edit Contact information')?></h3>
		<p <?= (form_error("w_tel") != '') ? 'class="error"' : '' ?>>
			<label for="w_tel"><?= __('Telephone:')?></label>
			<input type="text" name="w_tel" id="w_tel" value="<?= set_value('w_tel', $venue->telephone) ?>" />
		</p>
		<p <?= (form_error("w_email") != '') ? 'class="error"' : '' ?>>
			<label for="w_email"><?= __('Email:')?></label>
			<input type="text" name="w_email" id="w_email" value="<?= set_value('w_email', $venue->email) ?>" />
		</p>
		<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
			<label for="website"><?= __('Website:')?></label>
			<input type="text" name="website" id="website" value="<?= set_value('website', $venue->website) ?>" />
		</p>
		<?php foreach($metadata as $m) : ?>
			<?php foreach($languages as $lang) : ?>
				<?php if(_checkMultilang($m, $lang->key, $app)): ?>
					<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
						<?= _getInputField($m, $app, $lang, $languages, 'venue', $venue->id); ?>
					</p>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endforeach; ?>
		<p id="p_address" <?= (form_error("w_address") != '') ? 'class="error"' : '' ?>>
			<label for="w_address"><?= __('Address:')?></label>
			<input type="text" name="w_address" id="w_address" value="<?= htmlspecialchars_decode(set_value('w_address', $venue->address), ENT_NOQUOTES) ?>" onblur="codeAddress()" />
			<a href="#showonmap" class="btn" id="btnshowonmap" onclick="codeAddress(); return false;"><?= __('Show on map')?> &raquo;</a><br clear="all" />
		</p>
		<div id="map_canvas" class="map" style="height:300px; margin-left:0px;"></div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript">
			var geocoder;
			var map;
			var bounds;
			var marker;

			function codeAddress() {
				var address = document.getElementById('w_address').value;
				if(address != '') {
					geocoder.geocode( { 'address': address}, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							if(marker != null){
								marker.setMap(null);
							}
							map.setCenter(results[0].geometry.location);
							marker = new google.maps.Marker({
								map: map,
								position: results[0].geometry.location
							});

							$('#w_lat').val(results[0].geometry.location.lat());
							$('#w_lon').val(results[0].geometry.location.lng());

							marker.draggable = true;
							google.maps.event.addListener(marker, 'drag', function() {
								$('#w_lat').val(marker.getPosition().lat());
								$('#w_lon').val(marker.getPosition().lng());
							});

							bounds = new google.maps.LatLngBounds();
							bounds.extend(results[0].geometry.location);
							map.fitBounds(bounds);
							map.setZoom(map.getZoom()-1);
						} else {
							if(status === 'ZERO_RESULTS'){
								alert('<?= __('No results found on the address ...')?>');
							} else {
								alert("<?= __('Geocode was not successful for the following reason: ')?>" + status);
							}
						}
					});
				}
			}

			$(document).ready(function() {
				geocoder = new google.maps.Geocoder();
				var latlng = new google.maps.LatLng(<?= ($venue->lat != '') ? $venue->lat : '50.753887' ?>, <?= ($venue->lon != '') ? $venue->lon : '4.269936' ?>);
				var myOptions = {
					zoom: 8,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
				bounds = new google.maps.LatLngBounds();

				<?php if($venue->lat != '' && $venue->lon != '') { ?>
				map.setCenter(latlng);
				marker = new google.maps.Marker({
					map: map,
					position: latlng
				});

				marker.draggable = true;
				google.maps.event.addListener(marker, 'drag', function() {
					$('#w_lat').val(marker.getPosition().lat());
					$('#w_lon').val(marker.getPosition().lng());
				});

				bounds.extend(latlng);
				map.setZoom(16);
				<?php } ?>
			});

		</script>

		<p style="display:none;" <?= (form_error("w_lat") != '') ? 'class="error"' : '' ?>>
			<label for="w_lat"><?= __('Latitude:')?></label>
			<input type="text" name="w_lat" id="w_lat" value="<?= set_value('w_lat', $venue->lat) ?>" />
		</p>
		<p style="display:none;" <?= (form_error("w_lon") != '') ? 'class="error"' : '' ?>>
			<label for="w_lon"><?= __('Longitude:')?></label>
			<input type="text" name="w_lon" id="w_lon" value="<?= set_value('w_lon', $venue->lon) ?>" />
		</p>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="btn"><?= __('Cancel')?></a>
		<button type="submit" class="btn primary"><?= __('Save Venue')?></button>
		<br clear="all">
	</div>
	<br clear="all">
</form>