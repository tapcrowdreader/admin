<div>
	<h1><?= __('Add Web Module') ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><p><?= $error ?></p></div>
			<?php endif ?>         
			<?php foreach($languages as $language) : ?>
	        <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
	            <label for="title"><?= __('Title') ?> <?= '(' . $language->name . ')'; ?>:</label>
	            <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$language->key, '' ), ENT_NOQUOTES) ?>" />
	        </p> 
	        <?php endforeach; ?>
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Order:') ?></label>
                <input type="text" name="order" id="order" value="<?= set_value('order', $defaultlauncher->order) ?>" />
            </p>
			<p <?= (form_error("icon") != '') ? 'class="error"' : '' ?>>
				<label for="icon"><?= __('Icon:') ?></label>
				<span class="hintAppearance"><?= __('Image must be in png format, width: %s px, height: %s px',140,140) ?></span>
				<input type="file" name="icon" id="icon" value="" class="logoupload" />
			</p><br clear="all"/>
<!--             <p <?= (form_error("youtube") != '') ? 'class="error"' : '' ?> style="min-height:0px;">
            	<input class="checkboxClass" type="checkbox" style="float:left; display:inline;" name="youtube" id="youtube" />
                <label for="youtube" style="float:left;width:450px;"><?= __('Add youtube channel') ?></label>
                <br clear="all" />
                <label id="channellabel" for="youtube" style="display:none;float:left;width:450px;"><?= __('Channel name:') ?></label>
                <input type="text" name="youtubechannel" id="youtubechannel" value="" style="display:none;"/>
            </p>	 -->
			<?php foreach($languages as $language) : ?>
	        <p <?= (form_error("url_".$language->key) != '') ? 'class="error"' : '' ?> id="urlp">
	            <label for="url"><?= __('Url ')?> <?= '(' . $language->name . ')'; ?>:</label>
	            <input type="text" name="url_<?php echo $language->key; ?>" id="url" value="<?= htmlspecialchars_decode(set_value('url_'.$language->key, '' ), ENT_NOQUOTES) ?>" />
	        </p> 
	        <?php endforeach; ?>
            <p <?= (form_error("extraparams") != '') ? 'class="error"' : '' ?> id="paramsp">
            	<input class="checkboxClass" type="checkbox" style="float:left; display:inline;" name="extraparams" />
                <label for="extraparams" style="float:left;width:450px;"><?= __('Add the appid and deviceid in the querystring of the URL') ?></label>
                <br clear="all" />
            </p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add Module') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
	if($('#youtube').is(':checked')) {
		$("#channellabel").css('display','block');
		$("#youtubechannel").css('display','block');
	}
	$('#youtube').mousedown(function() {
        if (!$(this).is(':checked')) {
            $("#channellabel").css('display','block');
            $("#youtubechannel").css('display','block');
            $("#urlp").css('display','none');
            $("#paramsp").css('display','none');
        } else {
			$("#channellabel").css('display','none');
			$("#youtubechannel").css('display','none');
			$("#urlp").css('display','block');
			$("#paramsp").css('display','block');
        }
    });
</script>
