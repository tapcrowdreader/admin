<h1><?= $contentmodule->title ?></h1>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<div>
	<br />
	<?php if($section->sectiontypeid == 2) : ?>
	<a href="<?= site_url('sectioncontent/add/'.$section->id) ?>" class="add btn primary" style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i>  <?= __('Add Button')?>
	</a>
	<?php endif; ?>
</div>
<br clear="all" />
<div class="modules">
	<?php foreach($sectioncontents as $sc) : ?>
	<div class="module active">
		<a href="<?= site_url('sectioncontent/edit/'.$sc->id) ?>" class="editlink"><span><?=$sc->title?></span></a>
	</div>
	<?php endforeach; ?>
</div>
