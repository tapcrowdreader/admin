<a class="add btn primary" href="<?= site_url('forms/export/'.$form->id)?>" id="downloadreport" style="margin-bottom:10px;">
	<img width="16" height="16" alt="TapCrowd App" src="img/icons/report.png">
	<?= __('Download Report') ?>
</a>
<br clear="all" />

<table class="table table-striped table-condensed table-hover table-engine">
<thead>
<?php
$colspan = 0;
echo '<tr>';
foreach($headings as $heading):
    echo '<th>' . $heading . '</th>';
	$colspan++;
endforeach;
echo '</tr>';
?>
</thead>
<tbody>
<?php
foreach($results as $row):
	echo '<tr>';
	foreach( $row as $key=>$value ):
		$value = ($value == '0') ? '&nbsp;' : $value;
		echo '<td>' . $value . '</td>';
    endforeach;
    echo '</tr>';
endforeach;
?>
</tbody>
<tfoot>
	<tr>
		<td colspan="<?=$colspan?>">
			<div class="pagination"></div>
			<br /><br />
		</td>
	</tr>
</tfoot>
</table>