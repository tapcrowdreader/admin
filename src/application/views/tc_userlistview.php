<?php
/**
 * Tapcrowd list view
 * @author Tom Van de Putte
 */

if(!isset($settings)) {
	echo 'Problem: we need the settings variable';
	return;
}

$headers =& $settings->headers;
$colspan = count($headers)+1;			// # of headers + actions header
if($settings->checkboxes) {
	$colspan++;
}
?>

<?php
#
# Module settings button
#
if(isset($settings->module_url)) { ?>
	<div class="btn-group pull-right">
		<?php if(isset($settings->premium)) : ?>
			<a href="<?= site_url('premium/'.$settings->parentType.'/'.$settings->parentId.'/'.$settings->premium) ?>" class="btn editPremiumButton"><i class="icon-pencil"></i> <?= __('Edit premium items') ?></a>
		<?php endif; ?>
		<a href="<?=$settings->module_url?>" class="btn"><i class="icon-wrench"></i> <?=__('Module Settings')?></a>
	</div><?php
} ?>


<?php
#
# Maps settings button
#
if(isset($settings->enable_maps) && $settings->enable_maps != false) {
	$url = '/maps/launcher/'.$settings->launcher->id; ?>
	<div class="btn-group pull-right">
		<a href="<?=$url?>" class="btn" style="margin-right:10px;"><i class="icon-map-marker"></i> <?=__('Map Setup')?></a>
	</div><?php
} ?>


<?php if(isset($settings->title)) : ?>
<h2><?= ucfirst($settings->title) ?></h2>
<?php else: ?>
<h2><?=(empty($settings->plural)? 'No title set' : ucfirst(__($settings->plural)))?></h2>
<?php endif; ?>

<?php if(isset($settings->premium)) : ?>
<div id="premiuminfo">
	<p><?= __('Extra: Premium listings.<br/>
				This feature allows you to give premium space to certain items by showing them on top of the list
				(regardless of the alphabetic order) and in a different color.
				') ?></p>
	<br clear="all" />
</div>
<?php endif; ?>

<?php
#
# Tabs
#
if(isset($settings->tabs)) : ?>
<div class="tabs-below">
	<ul class="nav nav-tabs">
		<?php foreach ($settings->tabs as $t)  : ?>
		<li <?= $t->active ? 'class="active"' : '' ?>>
			<a href="<?= $t->url ?>"><?= $t->title ?></a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>
<br clear="all" />
<?php endif; ?>
<?php
#
# Button toolbar
#
if(isset($settings->toolbar)) {?>
	<div class="btn-toolbar"><?php
	foreach($settings->toolbar as $btn) {
		if(!empty($btn->btn_class)) $btn->icon_class .= ' icon-white';
		echo '<div class="btn-group"><a href="'.$btn->href.'" class="btn '.$btn->btn_class;
		echo '"><i class="'.$btn->icon_class.'"></i> '.$btn->title.'</a></div>';
	} ?>
	</div><?php
} ?>

<?php
#
# ERROR Notifications
#
if(isset($_SESSION['error'])) { ?>
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<p><strong>Error!</strong> <?=$_SESSION['error']?></p>
	</div><?php
	unset($_SESSION['error']);
}

#
# MESSAGE Notifications
#
if(isset($_SESSION['message'])) { ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<p><strong>Success!</strong> <?=$_SESSION['message']?></p>
	</div><?php
	unset($_SESSION['message']);
}
?>
<?php
#
# extra buttons on top
#
if(isset($settings->extrabuttons) && !empty($settings->extrabuttons)) : ?>
<div style="float:left;">
<?php foreach($settings->extrabuttons as $btn) : ?>
<a class="<?= $btn->btn_class ?>" href="<?= site_url($btn->href) ?>" target="<?=$btn->target ? $btn->target : '_self' ?>" style="margin-bottom:10px;">
	<?php if(stristr($btn->btn_class, 'edit')) { ?><i class="icon-pencil"></i><?php } ?>
	<?= $btn->title ?>
</a>
<?php endforeach; ?>
</div>
<?php endif; ?>
<?php
#
# Filter by group
#
if(isset($settings->filter)) : ?>
	<select id="<?= $settings->filter['id'] ?>" class="<?= $settings->filter['class'] ?>" name="<?= $settings->filter['name'] ?>">
	<option value=""><?= $settings->filter['label'] ?></option>
	<?php foreach($settings->filter['data'] as $d) : ?>
	<option value="<?= $d->id ?>" <?= $d->id == $settings->filter['selected'] ? 'selected="selected"' : ''?>><?= $d->name?></option>
	<?php endforeach; ?>
	</select>
	<?php if(isset($settings->filtermanage)) : ?>
	<a href="<?= $settings->filtermanage['url'] ?>" class="<?= $settings->filtermanage['class'] ?>" style="margin-top:-11px;"><?= $settings->filtermanage['title'] ?></a>
	<?php endif; ?>
	<script charset="utf-8" type="text/javascript">
		$(document).ready(function() {
			$("#<?= $settings->filter['id'] ?>").change(function(){
				window.location = "<?= site_url($settings->filter['url']) ?>/" + $("#<?= $settings->filter['id'] ?>").val();
			});
		});
	</script>
<?php endif; ?>
<?php
#
# user secured modules
#
if(isset($securedmodules) && !empty($securedmodules)) : ?>
	<select id="securedmodules" name="securedmodules">
	<option value=""><?= __('Give users access to modules') ?></option>
	<?php foreach($securedmodules as $d) : ?>
	<option value="<?= $d->launcherid ?>"><?= $d->title?></option>
	<?php endforeach; ?>
	</select>
	<a href="users/linkmodules/<?=$type?>/<?=$typeId?>" id="securedmodulesbutton" style="margin-top:-10px;" class="btn"><?= __('Apply to selected') ?></a> <a href="users/linkmodulesall/<?=$type?>/<?=$typeId?>" id="securedmodulesbuttonall" style="margin-top:-10px;" class="btn"><?= __('Apply to all') ?></a><br clear="all" />
	<script charset="utf-8" type="text/javascript">
		$(document).ready(function() {
			$("#securedmodulesbutton").click(function(e){
				e.preventDefault();
				var selected = new Array();
				$('tbody input:checked').each(function() {
					if($(this).attr('class') == 'securedcheckbox') {
						selected.push($(this).attr('name'));
					}
				});

				var launcherid = $("#securedmodules").find('option:selected').val();

				var url = $("#securedmodulesbutton").attr('href');
				$.ajax({
					type: "POST",
					url: url,
					data: { selectedids: selected, launcherid: launcherid }
				}).done(function( data ) {
					location.reload();
				});
			});

			$("#securedmodulesbuttonall").click(function(e){
				e.preventDefault();
				var launcherid = $("#securedmodules").find('option:selected').val();

				var url = $("#securedmodulesbuttonall").attr('href');
				$.ajax({
					type: "POST",
					url: url,
					data: { launcherid: launcherid }
				}).done(function( data ) {
					location.reload();
				});
			});

			$('.selectallcheckbox').mousedown(function() {
			    if (!$(this).is(':checked')) {
			        $(".securedcheckbox:visible").attr('checked', true);
			    } else {
					$(".securedcheckbox").attr('checked', false);
			    }
			});
		});
	</script>
<?php endif; ?>
<table class="table table-striped table-condensed table-hover table-engine">
<thead>
	<tr>
	<th colspan="<?=$colspan + 1?>" class="row-fluid">
		<div class="pull-right">
			<?php
			#
			# Delete all button
			#
			if($settings->checkboxes) { ?>
				<a href="<?=$settings->deletecheckedurl?>" class="btn btn-danger" id="deletechecked"><i class="icon-remove icon-white"></i> <?=__('Delete checked')?></a>
			<?php
			} ?>
			<?php if(isset($settings->import_url) && !empty($settings->import_url)) : ?>
			<a href="<?=$settings->import_url?>" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> <?=__('Import')?></a>
			<?php endif; ?>
			<?php if(isset($settings->rss_url) && !empty($settings->rss_url)) : ?>
			<a href="<?=$settings->rss_url?>" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> <?=__('RSS')?></a>
			<?php endif; ?>
			<?php if(isset($settings->add_url) && !empty($settings->add_url)) : ?>
			<a href="<?=$settings->add_url?>" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> <?=__('Add Item')?></a>
			<?php endif; ?>
		</div>
		<br clear="all" />
		<form class="formsearch">
			<div class="input-append">
				<input name="q" type="text" class="input-large search-query">
				<input name="filter" type="hidden" value="name"  />
				<button type="submit" class="btn"><i class="icon-search"></i> <?= __('Search')?></button>
			</div>
		</form>
		<div class="infobox searchlist span12"></div>
	</th>
	</tr>
	<tr><?php
	$i = 0;
	foreach($headers as $h => $field) {
		if($i == 0) {
			echo '<th style="max-width:1em !important;"><input type="checkbox" class="selectallcheckbox" /></th>';
		}
			echo "<th class='sort-alfa'>$h</th>";
		$i++;
	} ?>
		<th style="text-align: right;min-width:100px;"><?=__('Actions')?></th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="<?=$colspan?>">
			<div class="pagination"></div>
			<br /><br />
		</td>
	</tr>
</tfoot>
<tbody><?php

# No data
if(empty($data)) { echo '<tr><td colspan="'.$colspan.'">' . __('No data') . '</td></tr></tbody></table>'; return; }

#
# Render rows
#
$row_fmt = '<tr id="%s">' . str_repeat('%s', count($headers)) . '<td><div class="pull-right">';
foreach($settings->actions as $a) {
// 	$row_fmt.= '<a class="btn btn-small '.$a->btn_class.'" href="'.$a->href.'"><i class="'.$a->icon_class.'  icon-white"></i> '.$a->title.'</a>';
	$row_fmt.= ' <a class="btn btn-mini '.$a->btn_class.'" href="'.$a->href.'"><i class="'.$a->icon_class.'  icon-white" title="'.$a->title.'"></i></a>';
}
$row_fmt.= '</div></td></tr>';
while($struct = array_pop($data)) {
	$d = array( $row_fmt, $struct->id );
	reset($headers);
	$i = 0;
	while(list(,$attr) = each($headers)) {
		if($i == 0) {
			if($attr == 'imageurl' && !empty($struct->$attr)) {
				$d[] = '<td><input type="checkbox" class="securedcheckbox" name="'.$struct->id.'" /></td><td><a href="'.$struct->$attr.'" target="_blank"><img src="'.$struct->$attr.'" width="40" /></a></td>';
			} elseif(isset($settings->actions['edit']->href)) {
				$d[] = '<td><input type="checkbox" class="securedcheckbox" name="'.$struct->id.'" /></td><td><a href="'. site_url($settings->actions['edit']->href).'" class="block">'.$struct->$attr . '</a></td>';
			} else {
				$d[] = '<td><input type="checkbox" class="securedcheckbox" name="'.$struct->id.'" /></td><td>'.$struct->$attr.'</td>';
			}
		} else {
			if($attr == 'imageurl' && !empty($struct->$attr)) {
				$d[] = '<td><a href="'.$struct->$attr.'" target="_blank"><img src="'.$struct->$attr.'" width="40" /></a></td>';
			} elseif(isset($settings->actions['edit']->href)) {
				$d[] = empty($struct->$attr) && $struct->$attr !== '0' ? '<td></td>' : '<td><a href="'. site_url($settings->actions['edit']->href).'" class="block">' . $struct->$attr . '</a></td>';
			} else {
				$d[] = empty($struct->$attr) && $struct->$attr !== '0' ? '<td></td>' : '<td>' . $struct->$attr . '</td>';
			}
		}
		$i++;
	}
	$row = call_user_func_array('sprintf', $d);
	$row = str_replace(array('--id--', '--plural--', '--parentType--'), array($struct->id, strtolower($settings->plural), $settings->parentType), $row);
	echo $row;
}
?>
</tbody>
</table>
<script type="text/javascript">
$("#deletechecked").click(function(e) {
	e.preventDefault();

	var selected = new Array();
	$('tbody input:checked').each(function() {
		if($(this).attr('class') == 'securedcheckbox') {
			selected.push($(this).attr('name'));
		}
	});

	var delurl = $(this).attr('href');
	jConfirm('Are you sure?', 'Delete items', function(r) {
		if(r === true) {
			$.ajax({
				type: "POST",
				url: delurl,
				data: { selectedids: selected }
			}).done(function( data ) {
				location.reload();
			});
		} else {
			return false;
		}
	});
});
</script>