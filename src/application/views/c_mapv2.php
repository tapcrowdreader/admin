<script type="text/javascript">
var previousclick = 0;
</script>
<?php if(isset($event)) {
	$type = 'event';
	$object = $event;
	if($map != false) {
		$map->objectid = $map->eventid;
	}
	
} elseif(isset($venue)) {
	$type = 'venue';
	$object = $venue;
	if($map != false) {
		$map->objectid = $map->venueid;
	}
} 
?>
<a href="<?= site_url('module/edit/72/'.$type.'/'.$object->id) ?>" class="edit btn" style="float:right;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a>
<?php
if ($this->uri->segment(4) == 'session') {
	$itemtype = 'session';
} elseif($this->uri->segment(4) == 'poi') {
	$itemtype = 'poi';
} elseif($this->uri->segment(4) == 'subfloorplan') {
	$itemtype = 'subfloorplan';
} elseif($this->uri->segment(4) == 'indoorrouting') {
	$itemtype = 'indoorrouting';
}else {
	$itemtype = 'exhibitor';
}
?>

<h1><?= __('Floorplan')?></h1>
<?php if(isset($_GET['error']) && $_GET['error'] == 'noimage') : ?>
<p style="color:#FF0000;"><?= __('Please select an image to upload.') ?></p>
<?php endif; ?>
<?php if(isset($_GET['error']) && $_GET['error'] == 'image') : ?>
<p style="color:#FF0000;"><?= __('Failed to upload floorplan. Please select an image with maximum filesize 10MB.') ?></p>
<?php endif; ?>
<p><?= __('Image must be in jpg or png format. Minimum width is 800px, minimum height is 600px. Max filesize is 10MB.') ?> <?= __('If you use multiple floorplans for different zoom levels please upload the floorplan with most detail here. This is the floorplan used for the mappings of exhibitors and sessions.') ?></p>
<div class="note">
    <form action="<?= site_url('mapv2/add/'.$object->id.'/'.$type) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
        <fieldset>
		<?php if($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)) : ?>
        <p>
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)){ ?><span class="evtlogo" style="background:transparent url('<?= image_thumb($map->imageurl, 50, 50) ?>') no-repeat top left;"></span><span class="hintAppearance" style="margin-left:60px;"><?= __('If you just replace the floorplan your items will still be mapped on the same locations.')?><br /> <?= __('If you remove the floorplan the mapping will be lost as well.')?></span><?php } ?>
            <input type="file" name="w_map" id="w_map" value="" class="<?= (form_error("w_map") != '') ? 'error' : '' ?> logoupload" /><br clear="all" />
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)){ ?><span><a href="<?= site_url($type.'/removemap/'.$map->id.'/'.$object->id) ?>" class="deletemap" style="margin-left:55px;">Remove</a></span><?php } ?>
        </p>
        <div class="buttonbar">
            <input type="hidden" name="postback" value="postback" id="postback">
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)) : ?>
            <input type="hidden" name="replaceId" value="<?=$map->id?>" />
            <button type="submit" class="btn primary"><?= __('Replace Floorplan') ?></button>
       	 	<?php else: ?>
       	 	<button type="submit" class="btn primary"><?= __('Upload Floorplan') ?></button>
    		<?php endif; ?>
			<br clear="all" />
        </div>
		<br clear="all" />
        <?php else: ?>
        <p>
            <label style="width: 200px;"><?= __('No map found, add a map') ?></label>
            <input type="file" name="w_map" id="w_map" value="" <?= (form_error("w_map") != '') ? 'class="error"' : '' ?> />
        </p>
        <div class="buttonbar">
            <input type="hidden" name="postback" value="postback" id="postback">
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)) : ?>
            <input type="hidden" name="replaceId" value="<?=$map->id?>" />
            <button type="submit" class="btn primary"><?= __('Replace Floorplan') ?></button>
       	 	<?php else: ?>
       	 	<button type="submit" class="btn primary"><?= __('Upload Floorplan') ?></button>
    		<?php endif; ?>
			<br clear="all" />
        </div>
		<br clear="all" />

        <?php endif; ?>
		</fieldset>
    </form>
</div>
<div>
	<p>
		<?= __('You can add multiple floorplans to show when the user zooms on the map. <br /> These floorplans must have the same resolution as your basic floorplan.') ?><br /><br />
		<form action="<?= site_url('mapv2/addzoommap/'.$object->id.'/'.$type) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
			<label for="zoommap_level"><?= __('Zoom level') ?> (in %)</label>
			<input type="text" value="" name="zoommap_level" style="width:25px;"/><br />
			<input type="file" name="zoommap" id="zoommap" value="" class="<?= (form_error("zoommap") != '') ? 'error' : '' ?> logoupload" /><br clear="all" />
			<button type="submit" class="btn primary"><?= __('Add zoom floorplan') ?></button>
		</form>
		<?php foreach($zoommaps as $zoommap) : ?>
		<span><?= __('Zoomlevel') ?>: <?= $zoommap->zoomlevel ?>%</span> <span><?= substr($zoommap->imageurl, strrpos($zoommap->imageurl, '/') + 1) ?></span><a href="<?= site_url('mapv2/removezoommap/'.$zoommap->id) ?>" class="deletezoommap" style="margin-left:5px;padding: 0 6px 20px 15px;"> </a><br />
		<span class="evtlogo" style="background:transparent url('<?= image_thumb($zoommap->imageurl, 50, 50) ?>') no-repeat top left;display: block;height: 50px;width: 50px;"></span><br />		
		<?php endforeach; ?>
	</p>
</div>
<?php if ($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)): ?>
<br clear="all"/>
<div class="switch" style="width: auto;">
	<?php if($app->apptypeid != 10) : ?>
	<a href="<?= site_url('mapv2/'.$type.'/'.$object->id.'/exhibitor/'.$map->id) ?>" class="part <?= ($itemtype == 'exhibitor' ? "active" : "") ?>"><?= __('Add Exhibitor locations') ?></a>
	<a href="<?= site_url('mapv2/'.$type.'/'.$object->id.'/session/'.$map->id) ?>" class="part <?= ($itemtype == 'session' ? "active" : "") ?>"><?= __('Add Session locations') ?></a>
	<!-- <a href="<?= site_url('mapv2/'.$type.'/'.$object->id.'/subfloorplan/'.$map->id) ?>" class="part <?= ($itemtype == 'subfloorplan' ? "active" : "") ?>"><?= __('Add Sub floorplan') ?></a> -->
	<?php else :?>
	<?php if($itemtype != 'subfloorplan' && $itemtype != 'indoorrouting') {
		$itemtype = 'session';
		} ?>
	<a href="<?= site_url('mapv2/'.$type.'/'.$object->id.'/session/'.$map->id) ?>" class="part active"><?= __('Add Session locations') ?></a>
	<!-- <a href="<?= site_url('mapv2/'.$type.'/'.$object->id.'/subfloorplan/'.$map->id) ?>" class="part <?= ($itemtype == 'subfloorplan' ? "active" : "") ?>"><?= __('Add Sub floorplan') ?></a> -->
	<?php endif; ?>
	<a href="<?= site_url('mapv2/'.$type.'/'.$object->id.'/indoorrouting/'.$map->id) ?>" class="part <?= ($itemtype == 'indoorrouting' ? "active" : "") ?>"><?= __('Add Indoor routing') ?></a>
</div>
	<?php if($itemtype == 'indoorrouting'): ?>
    <script type="text/javascript">
    var paper = '';
    $(document).ready(function() {
    	paper = Raphael(document.getElementById("map"), <?= $map->width ?>, <?= $map->height ?>);
    	paper.id = 'paper';
		var img = paper.image("<?= $this->config->item('publicupload').$map->imageurl ?>", 0, 0, <?= $map->width ?>, <?= $map->height ?>);
		// img.attr({ "clip-rect": "20,20,30,30" });
		// rect.attr('background-image', "url('http://upload.tapcrowd.com/upload/maps/events/6124/eventdemoapp_map_detail.jpg')");
        <?php foreach($markers['paths'] as $path) : ?>					
			var path = paper.path("M<?=$path->x1?> <?=$path->y1?>, L<?=$path->x2?> <?=$path->y2?>")
						.click(function (event) {
							event.preventDefault();
							var getPath = paper.getById(this.id);
							// console.log(getPath);
							jConfirm('<?= __("Do you want to remove this path?") ?>', '<?= __("Remove path") ?>', function(r) {
								if(r == true) {
									getPath.remove();
									$.ajax({
										url: "<?= site_url('mapv2/removeroutingpath') ?>/"+getPath.id,
										type: 'GET',
									}).done(function( msg ) {
										// location.reload();
										
									});
								}
							});
							return false;
						});
			path.id = "<?=$path->id?>";
			path.attr("stroke-width", "5");
			path.attr("opacity", 1);
			path.translate(0.5, 0.5);
    	<?php endforeach; ?>
    });
	</script>
	<?php endif; ?>
	<script src="js/mobilymap.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('.map').mobilymap({
				position: 'top left',
				popupClass: 'bubble',
				markerClass: 'point',
				<?php if($itemtype == 'indoorrouting'):  ?>
				popup: false,
				<?php else: ?>
				popup: true,
				<?php endif; ?>
				cookies: true,
				caption: false,
				setCenter: true,
				outsideButtons: null,
				onMarkerClick: function(){
					var pos2 = this.id.split("-");
					x2 = pos2[1];
					y2 = pos2[2];
					if(previousclick != 0) {
						var pos1 = previousclick.split("-");
						x = pos1[1];
						y = pos1[2];

						if(x != x2 || y != y2) { 
							$.ajax({
								url: "<?= site_url('mapv2/addroutingpath/'.$map->id) ?>/"+x+'/'+y+'/'+x2+'/'+y2,
								type: 'GET',
							}).done(function( msg ) {
								previousclick = 0;
								// console.log("M"+x+" "+y+", "+"L"+x2+" "+y2);
								var path = paper.path("M"+pos1[1]+" "+pos1[2]+", "+"L"+pos2[1]+" "+pos2[2])
											.click(function (event) {
												event.preventDefault();
												var getPath = paper.getById(msg);
												jConfirm('<?= __("Do you want to remove this path?") ?>', '<?= __("Remove path") ?>', function(r) {
													if(r == true) {
														$.ajax({
															url: "<?= site_url('mapv2/removeroutingpath') ?>/"+msg,
															type: 'GET',
														}).done(function( msg2 ) {
															// location.reload();
															getPath.remove();
														});
													}
												});
												return false;
											});
								path.id = msg;
								path.attr("stroke-width", "5");
								path.attr("opacity", 1);
								path.translate(0.5, 0.5);
							});
						}
					}
					previousclick = this.id;
					<?php if($itemtype == 'indoorrouting'):  ?>
					var removepointurl = "<?= site_url('mapv2/removeindoorroutingpointbycoord/'.$map->objectid.'/'.$map->id) ?>/"+x2+'/'+y2;
					var $this=$(this),pointw=$this.width(),pointh=$this.height(),pos=$this.position(),py=pos.top,px=pos.left;
					wrap = '<a href="'+removepointurl+'" class="removeIndoorRoutingPoint delete"><?= __("DELETE") ?></a>';
					<?php else: ?>
					var $this=$(this),pointw=$this.width(),pointh=$this.height(),pos=$this.position(),py=pos.top,px=pos.left,wrap=$this.find(".markerContent").html('DELETE');
					<?php endif; ?>
					$("."+sets.popupClass).remove();
					$this.after($("<div />").addClass(sets.popupClass).css({position:"absolute",zIndex:"3"}).html(wrap).append($("<a />").addClass("close")));
					var popup=$this.next("."+sets.popupClass),popupw=popup.innerWidth(),popuph=popup.innerHeight(),y=py,x=px;popup.css({top:y+pointh+"px",left:x+"px",marginLeft:-(popupw/2-pointw/2)+"px"});
				},
				onPopupClose: function(){},
				onMapLoad: function(){
					content.bind({
						dblclick: function(e){
							var position = $(this).position();
							var offset = $(this).offset();
							var x = e.pageX - (offset.left);
							var y = e.pageY - (offset.top);
							if($('#sel_koppel option').size() <= 1){
								$('#saveadd').addClass('disabled');
							}
							$('#posx').val(x); $('#posy').val(y);
							$('#sel_koppel').removeClass('error');
							$('.map .imgContent').append('<div class="point added" id="p-' + x + '-' + y + '"><div class="markerContent"></div></div>');

							<?php if($itemtype != 'indoorrouting') : ?>
								$('.mapform_bg').css({'opacity':0, 'display':'block'}).animate({'opacity':0.6});
								$('.mapform').fadeIn();
							<?php else: ?>
								$.ajax({
								url: "<?= site_url('mapv2/addroutingpoint/'.$map->id) ?>",
								type: 'POST',
								data: {xpos:x, ypos:y}})
								.done(function( msg ) {
									// location.reload();
								});
							<?php endif; ?>
							
							previousclick = 0;
							reinitmap();
						}
					});
				}
			});

			// Cancel NEW MARKER
			$('#canceladd').click(function(e){
				e.preventDefault();
				$('.point.added').remove();
				$('.mapform_bg').fadeOut();
				$('.mapform').fadeOut();
			});

			// SAVE NEW MARKER
			$('#saveadd').click(function(e){
				e.preventDefault();
				if(!$(this).hasClass('disabled')) {
					if($('#sel_koppel').val() != ''){
						$.ajax({
							url: '<?= ($itemtype == "session") ? site_url("mapv2/addSessionMarker") : (($itemtype == "poi") ? site_url('mapv2/addPoi') : ($itemtype == "subfloorplan") ? site_url("mapv2/addSubfloorplan") : site_url("mapv2/addMarker")) ?>',
							type: 'POST',
							data: $('#frmMarkerDetails').serialize(),
							complete: function(xhr, textStatus) {
								$('.point.added .markerContent').html($("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").text() + '<br /><a href="<?= site_url("mapv2/removemarker/".$map->objectid."/".$map->id) ?>/' + $("#sel_koppel").val() + '" class="delete"><?= __('DELETE')?></a>');
								$('.point.added').removeClass('added');

								$('.mapform_bg').fadeOut();
								$('.mapform').fadeOut();

								$("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").remove();
								$('#sel_koppel').val('');
							},
							success: function(data, textStatus, xhr) {

							},
							error: function(xhr, textStatus, errorThrown) {

							}
						});
					} else {
						// no exhibitor chosen
						$('#sel_koppel').addClass('error');
					}
				}
			});

		});

		function reinitmap(){
			var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
			var point=$this.find(".point");

			point.each(function(){
				var $this=$(this),pos=$this.attr("id").split("-");
				x = pos[1] - $this.width()/2, y = pos[2] - $this.height() + 4;
				$this.css({position:"absolute",zIndex:"2",top:y+"px",left:x+"px"});
				<?php if($itemtype == 'exhibitor' || $itemtype == '') : ?>
				$this.css('background-image', "url('../img/marker-exhibitor.png')");
				$this.css('width', '44px');
				<?php elseif($itemtype == 'session') : ?>
				$this.css('background-image', "url('../img/marker-session.png')");
				$this.css('width', '44px');
				<?php elseif($itemtype == 'subfloorplan') : ?>
				$this.css('background-image', "url('../img/marker-map.png')");
				$this.css('width', '44px');
				<?php endif; ?>
			});
			point.click(function(){
				var $this=$(this),pointw=$this.width(),pointh=$this.height(),pos=$this.position(),py=pos.top,px=pos.left,wrap=$this.find(".markerContent").html();
				if(sets.popup){
					$("."+sets.popupClass).remove();
					$this.after($("<div />").addClass(sets.popupClass).css({position:"absolute",zIndex:"3"}).html(wrap).append($("<a />").addClass("close")));
					var popup=$this.next("."+sets.popupClass),popupw=popup.innerWidth(),popuph=popup.innerHeight(),y=py,x=px;popup.css({top:y+pointh+"px",left:x+"px",marginLeft:-(popupw/2-pointw/2)+"px"})
				}else{
					sets.onMarkerClick.call(this)
				}
				return false;
			});
			$this.find(".close").live("click",function(){
				var $this=$(this);
				$this.parents("."+sets.popupClass).remove();
				setTimeout(function(){
					sets.onPopupClose.call(this)
				},100);
				return false;
			});
			if(sets.outsideButtons!=null){
				$(sets.outsideButtons).click(function(){
					var $this=$(this),rel=$this.attr("rel");
					div=content.find("."+sets.markerClass).filter(function(){
						return $(this).attr("id")==rel
					});
					div.click();
					return false;
				});
			}
		}

	</script>
	<?php list($width, $height, $type, $attr) = getimagesize($this->config->item('imagespath') . $map->imageurl); ?>
	<?php if($map->parentId != 0) : ?>
	<a href="<?=site_url('mapv2/'.$type.'/'.$map->objectid.'/subfloorplan/'.$map->parentId);?>" class="btn" style="float:right;margin-top:-3px;"><?= __('Go to parent floorplan') ?></a>
	<?php endif; ?>
	<div class="note">
		<?= __('Double-click the map on the spot you would like to add a marker.') ?><br />
		<?= __('Click a point and choose delete to remove a marker.') ?>
		<?php if($itemtype == 'indoorrouting'): ?>
		<?= __('Click to points to draw a path between them. Click the path if you want too remove it.') ?>
		<?php endif; ?>
	</div>
	<div class="map" id="map">
		<img src="<?= $this->config->item('publicupload') .$map->imageurl ?>" width="<?= ($width) ?>" height="<?= ($height) ?>" style="left:0px !important;position:absolute;z-index:1;" />
		<canvas id="myCanvas" width="<?= ($width) ?>" height="<?= ($height) ?>" style="background:transparent;left:0;top:0;position:absolute;z-index:2;"></canvas>
		<?php if ($markers): ?>
			<?php if ($itemtype == 'session'): ?>
				<?php foreach ($markers as $marker): ?>
					<div class="point" id="p-<?= number_format($marker->xpos, 0, ".", "") ?>-<?= number_format($marker->ypos, 0, ".", "") ?>">
						<div class="markerContent">
							<?= $marker->location ?><br />
							<a href="<?= site_url('mapv2/removeSessionmarker/'.$map->objectid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE') ?></a>
						</div>
					</div>
				<?php endforeach ?>
            <?php elseif($itemtype == 'poi'):  ?>
                <?php foreach ($markers as $marker): ?>
                    <div class="point" id="p-<?= ($marker->x2 != 0) ? $marker->x1+($marker->x2/2) : $marker->x1 ?>-<?= ($marker->y2 != 0) ? $marker->y1+($marker->y2/2) : $marker->y1 ?>">
                        <div class="markerContent">
                            <?= $marker->name . ' (' . $marker->poitypename . ')' ?><br />
                            <a href="<?= site_url('mapv2/removepoi/'.$map->objectid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE') ?></a>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php elseif($itemtype == 'indoorrouting'):  ?>
                <?php foreach ($markers['points'] as $point): ?>
                    <div class="point" id="p-<?= $point->x ?>-<?= $point->y ?>">
                        <div class="markerContent">
                            <a href="<?= site_url('mapv2/removeindoorroutingpoint/'.$map->objectid.'/'.$map->id.'/'.$point->id) ?>" class="removeIndoorRoutingPoint delete"><?= __('DELETE') ?></a>
                        </div>
                    </div>
                <?php endforeach ?>
			<?php else: ?>
				<?php foreach ($markers as $marker): ?>
					<div class="point" id="p-<?= ($marker->x2 != 0) ? $marker->x1+($marker->x2/2) : $marker->x1 ?>-<?= ($marker->y2 != 0) ? $marker->y1+($marker->y2/2) : $marker->y1 ?>">
						<div class="markerContent">
							<?= $marker->name ?><br />
							<a href="<?= site_url('mapv2/removemarker/'.$map->objectid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE') ?></a>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>
		<?php endif ?>
	</div>
	
	<div class="mapform_bg"></div>
	<?php if ($itemtype == 'session'): ?>
		<div class="mapform">
			<form action="<?= site_url('mapv2/marker/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
				<h2><?= __('Choose Location') ?></h2>
				<p>
					<label for="sel_koppel"><?= __('Location')?></label>
					<select name="sel_koppel" id="sel_koppel">
						<option value=""><?= __('Select ...')?></option>
						<?php if ($sessionlocations != FALSE): ?>
						<?php foreach ($sessionlocations as $sl): ?>
							<option value="<?= $sl->id ?>"><?= $sl->location ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="hidden" name="markerid" value="" id="markerid">
					<input type="hidden" name="mapid" value="<?=$map->id?>" id="mapid">
					<input type="hidden" name="posx" value="" id="posx">
					<input type="hidden" name="posy" value="" id="posy">
					<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
					<a href="#" id="saveadd" class="btn primary <?= (isset($sl) && $sl != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
				</div>
			</form>
		</div>
    <?php elseif($itemtype == 'poi') : ?>
	<div class="mapform">
		<form action="<?= site_url('mapv2/marker/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
			<h2><?= __('Choose Poi')?></h2>
			<p>
				<label for="name"><?= __('Name') ?></label>
				<input type="text" name="name" value="" id="name">
			</p>
			<p>
				<label for="sel_koppel"><?= __('Poitype') ?></label>
				<select name="sel_koppel" id="sel_koppel">
					<option value=""><?= __('Select ...') ?></option>
					<?php if ($poitypes != FALSE): ?>
					<?php foreach ($poitypes as $poitype): ?>
						<option value="<?= $poitype->id ?>"><?= $poitype->name ?></option>
					<?php endforeach ?>
					<?php endif ?>
				</select>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<?php if($type == 'event') : ?>
				<input type="hidden" name="eventid" value="<?= $object->id ?>" id="eventid">
				<?php else: ?>
				<input type="hidden" name="venueid" value="<?= $object->id ?>" id="venueid">
				<?php endif; ?>
				<input type="hidden" name="markerid" value="" id="markerid">
				<input type="hidden" name="posx" value="" id="posx">
				<input type="hidden" name="posy" value="" id="posy">
				<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
				<a href="#" id="saveadd" class="btn primary <?= ($poitype != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
			</div>
		</form>
	</div>
	<?php else: ?>
		<div class="mapform">
			<form action="<?= site_url('mapv2/marker/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
				<h2><?= __('Choose Exhibitor') ?></h2>
				<p>
					<label for="sel_koppel"><?= __('Exhibitor') ?></label>
					<select name="sel_koppel" id="sel_koppel">
						<option value=""><?= __('Select ...') ?></option>
						<?php if ($exhibitors != FALSE): ?>
						<?php foreach ($exhibitors as $exhibitor): ?>
							<option value="<?= $exhibitor->id ?>"><?= $exhibitor->booth . ' - ' . $exhibitor->name ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="hidden" name="markerid" value="" id="markerid">
					<input type="hidden" name="mapid" value="<?=$map->id?>" id="mapid">
					<input type="hidden" name="posx" value="" id="posx">
					<input type="hidden" name="posy" value="" id="posy">
					<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
					<a href="#" id="saveadd" class="btn primary <?= ($exhibitors != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
				</div>
			</form>
		</div>
	<?php endif ?>
<?php endif ?>
<?php if($itemtype == 'exhibitor' || $itemtype == '') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/marker-exhibitor.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php elseif($itemtype == 'session') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/marker-session.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php elseif($itemtype == 'subfloorplan') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/marker-map.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function() {
	$('.deletemap').click(function(e) {
		e.preventDefault();
		var delurl = $(this).attr('href');
		jConfirm('If you remove the floorplan all items will be removed from the floorplan as well. Are you sure?', 'Remove floorplan', function(r) {
			if(r === true) {
				location.href = delurl;
			} else {
				return false;
			}
		});
	});
});
</script>
<?php if($itemtype == 'indoorrouting') : ?>
<style type="text/css">
.point {
    width: 10px;
    height: 10px;
    background: #22a8e0;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    border-radius: 50px;
    z-index:20 !important;
}
.bubble {
	width: 60px !important;
	z-index:10 !important;
}

svg {
	z-index:10;
}

a.removeIndoorRoutingPoint {
    background: url("../img/icons/delete.png") no-repeat scroll 5px 1px rgba(0, 0, 0, 0);
    border: medium none;
    color: #FFFFFF;
    display: block;
    float: right;
    margin-left: 5px;
    padding: 3px 6px 3px 25px;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	// $( "a.point" ).live( "dblclick", function() {
	// 	var pos = this.attr("id").split("-");
	// 	x = pos[0];
	// 	y = pos[1];
	// 	$.ajax({
	// 	url: "<?= site_url('mapv2/removeroutingpoint/'.$map->id) ?>",
	// 	type: 'POST',
	// 	data: {xpos:x, ypos:y}})
	// 	.done(function( msg ) {
	// 	});
	// });

	$(".removeIndoorRoutingPoint").live("click", function(event) {
		previousclick = 0;
		event.preventDefault();
		var obj = $(this);
		$.ajax({
			url: $(this).attr("href"),
			type: 'GET'
		}).done(function( msg ) {
			previousclick = 0;
			obj.parent().prev().remove();
			obj.parent().remove();
			return true;
		});
	});
});
</script>
<?php endif; ?>