<div>
	<h1><?= __('Add Item')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>   
			<p <?= (form_error("sponsorgroup") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Sponsorgroup')?></label>
				<select name="sponsorgroup" id="sponsorgroup">
					<option value=""><?= __('Select ...')?></option>
					<?php if(isset($sponsorgroups) && $sponsorgroups != false) : ?>
					<?php foreach ($sponsorgroups as $group): ?>
						<option value="<?= $group->id ?>" <?= set_select('sponsorgroup', $group->id) ?>><?= $group->name ?></option>
					<?php endforeach ?>
					<?php endif; ?>
				</select>
				<a href="<?= site_url('sponsorgroups/add/'.$this->uri->segment(3).'/'.$this->uri->segment(4) . '/') ?>" style="position:absolute; margin:-30px 0 0 210px;"><?= __('Add sponsorgroup')?> &rsaquo;&rsaquo;</a>
			</p>
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p>
                    <label for="name"><?= __('Name '.'(' . $language->name . ')'); ?>:</label>
                    <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?= set_value('name_'.$language->key)?>" />
                </p>
                <?php endforeach; ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("description") != '') ? 'class="error"' : '' ?>>
                    <label for="description"><?= __('Description '. '(' . $language->name . ')'); ?>:</label>
                    <textarea name="description_<?php echo $language->key; ?>" id="description"><?= set_value('description_'.$language->key) ?></textarea>
                </p>
                <?php endforeach; ?>
            <?php else : ?>
                <p <?= (form_error("name_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Name:')?></label>
                    <input type="text" name="name_<?php echo $app->defaultlanguage; ?>" id="name" value="<?=set_value('name_'.$app->defaultlanguage)?>" />
                </p>
                <p <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="description"><?= __('Description:')?></label>
                    <textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description"><?= set_value('description_'.$app->defaultlanguage) ?></textarea>
                </p>
            <?php endif; ?>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Photo:')?></label>
				<input type="file" name="image" id="image" value="<?=set_value('image')?>" class="logoupload" />
				<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
			</p><br />
			<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Website:')?></label>
				<input type="text" name="website" value="<?= set_value('website') ?>" id="website">
			</p>
<!-- 			<p <?= (form_error("premium") != '') ? 'class="error"' : '' ?> class="premiump">
				<input type="checkbox" name="premium" class="checkboxClass premiumCheckbox" id="premium"> <?= __('Premium?')?>
			</p> -->
			<div class="row">
				<label for="tags">Tags:</label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order') ?>"><br/>
<!-- 				<label><?= __('Extra line:')?> </label>
				<input type="text" name="extraline" value="<?= set_value('extraline') ?>" id="order"> -->
			</p>
			<?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<?php if(_checkMultilang($m, $lang->key, $app)): ?>
						<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
							<?= _getInputField($m, $app, $lang, $languages); ?>
						</p>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add Sponsor')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if($postedtags){foreach($postedtags as $val){ echo '"'.$val.'", '; }} ?>],
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = '<?=$tag->tag?>';
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
	});
</script>