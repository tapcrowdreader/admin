<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=7" />

	<title><?= __("Print Me")?></title>
	<base href="<?= base_url() ?>" target="" />
	<!-- CSS -->
	<link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/masters.css" type="text/css" />
	<link rel="stylesheet" href="css/jquery-ui-1.8.5.custom.css" type="text/css">
	<link rel="stylesheet" href="css/jquery.alerts.css" type="text/css" />
	<link rel="stylesheet" href="css/breadcrumb.css" type="text/css" />
	<link rel="stylesheet" href="css/datatables.css" type="text/css" />
	<link rel="stylesheet" href="css/debug.css" type="text/css" />
	<link rel="stylesheet" href="css/jquery.multiselect.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/uploadify.css" type="text/css" />
	<?php if($this->templatefolder != ''){ ?>
		<link rel="stylesheet" href="templates/<?= $this->templatefolder ?>/css/master.css" type="text/css" />
	<?php } ?>
	<!-- IE STYLESHEET -->
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" type="text/css" /><![endif]-->
	<!-- JAVASCRIPT -->
	<script type="text/javascript" charset="utf-8" src="js/jquery-1.7.2.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery-ui-1.8.5.custom.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.inlineedit.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.iphone-switch.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.maskedinput-1.2.2.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.jBreadCrumb.1.1.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.alerts.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery-ui-timepicker.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/tag-it.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/spin.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/bootstrap-dropdown.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/tapcrowd.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.cookie.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.Jcrop.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/debug.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.multiselect.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/bootstrap-tab.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.qrcode.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/qrcode.js"></script>

    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
    <script language="javascript" type="text/javascript" src="js/jquery.flot.js"></script>

  <link rel="Stylesheet" type="text/css" href="css/jpicker-1.1.6.min.css" />
  <link rel="Stylesheet" type="text/css" href="css/jPicker.css" />
  <script src="js/jpicker-1.1.6.min.js" type="text/javascript"></script>
  <script type="text/javascript" charset="utf-8" src="js/uploadify/jquery.uploadify-3.1.js"></script>
		<!-- IE6 UPDATE MESSAGE -->
		<!--[if IE 6]>
			<script type="text/javascript">
				if(typeof jQuery == 'undefined'){ document.write("<script type=\"text/javascript\"   src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\"></"+"script>"); var __noconflict = true; }
				var IE6UPDATE_OPTIONS = {
					icons_path: "http://static.ie6update.com/hosted/ie6update/images/"
				}
			</script>
			<script type="text/javascript" src="http://static.ie6update.com/hosted/ie6update/ie6update.js"></script>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
				  $('<div></div>').html(IE6UPDATE_OPTIONS.message || 'U maakt momenteel gebruik van een verouderde browser I.E.6. Om de site optimaal te bezoeken dient u uw browser te updaten. Download hier een nieuwere versie van Internet Explorer... ').activebar(window.IE6UPDATE_OPTIONS);
				});
			</script>
			<script src="js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
			<script>DD_belatedPNG.fix('.png');</script>
		<![endif]-->

<!-- 		<?php if ($_SERVER['HTTP_HOST'] == "admin.tapcrowd.com"): ?>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-23619195-1']);
			_gaq.push(['_setDomainName', '.tapcrowd.com']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<?php endif ?> -->
	<body>
		<h1 align="center" style="margin-top: 250px"><?= __('My QR Code')?></h1>
		<div id="loyalty">
			<div id="qrcode" align="center"></div>
		</div>
	</body>
</html>

<script type="text/javascript">
function applyqr(){
	if(navigator.appName == "Microsoft Internet Explorer")
	{
		jQuery("#qrcode").qrcode(
		{
			render	: "table",
			text	: "<?=$code?>"
		}
		);
	}
	else{
		jQuery("#qrcode").qrcode({
				text	: "<?=$code?>"
		});
	}
}

$(document).ready(
	function()
	{
		applyqr();
		window.print();
	}
);
</script>