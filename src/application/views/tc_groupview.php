<?php
/**
 * Tapcrowd list view
 * @author Tom Van de Putte
 */

if(!isset($settings)) {
	echo 'Problem: we need the settings variable';
	return;
}

$headers =& $settings->headers;
$colspan = count($headers)+1;			// # of headers + actions header
if($settings->checkboxes) {
	$colspan++;
}
?>

<?php
#
# Module settings button
#
if(isset($settings->module_url)) { ?>
	<div class="btn-group pull-right">
		<?php if(isset($settings->premium)) : ?>
			<a href="<?= site_url('premium/'.$settings->parentType.'/'.$settings->parentId.'/'.$settings->premium) ?>" class="btn editPremiumButton"><i class="icon-pencil"></i> <?= __('Edit premium items') ?></a>
		<?php endif; ?>
		<a href="<?=$settings->module_url?>" class="btn"><i class="icon-wrench"></i> <?=__('Module Settings')?></a>
	</div><?php
} ?>

<h2><?=(isset($title)) ? $title : (empty($settings->plural)? 'No title set' : __($settings->plural))?></h2>

<?php if(isset($settings->premium)) : ?>
<div id="premiuminfo">
	<p><?= __('Extra: Premium listings.<br/>
				This feature allows you to give premium space to certain items by showing them on top of the list
				(regardless of the alphabetic order) and in a different color.
				') ?></p>
	<br clear="all" />
</div>
<?php endif; ?>

<?php
#
# Tabs
#
if(isset($settings->tabs)) : ?>
<div class="tabs-below">
	<ul class="nav nav-tabs">
		<?php foreach ($settings->tabs as $t)  : ?>
		<li <?= $t->active ? 'class="active"' : '' ?>>
			<a href="<?= $t->url ?>"><?= $t->title ?></a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>
<br clear="all" />
<?php endif; ?>
<?php
#
# Button toolbar
#
if(isset($settings->toolbar)) {?>
	<div class="btn-toolbar"><?php
	foreach($settings->toolbar as $btn) {
		if(!empty($btn->btn_class)) $btn->icon_class .= ' icon-white';
		echo '<div class="btn-group"><a href="'.$btn->href.'" class="btn '.$btn->btn_class;
		echo '"><i class="'.$btn->icon_class.'"></i> '.$btn->title.'</a></div>';
	} ?>
	</div><?php
} ?>

<?php
#
# ERROR Notifications
#
if(isset($_SESSION['error'])) { ?>
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<p><strong>Error!</strong> <?=$_SESSION['error']?></p>
	</div><?php
	unset($_SESSION['error']);
}

#
# MESSAGE Notifications
#
if(isset($_SESSION['message'])) { ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<p><strong>Success!</strong> <?=$_SESSION['message']?></p>
	</div><?php
	unset($_SESSION['message']);
}
?>
<?php 
#
# Filter by group
#
if(isset($settings->filter)) : ?>
	<select id="<?= $settings->filter['id'] ?>" class="<?= $settings->filter['class'] ?>" name="<?= $settings->filter['name'] ?>">
	<option value=""><?= $settings->filter['label'] ?></option>
	<?php foreach($settings->filter['data'] as $d) : ?>
	<option value="<?= $d->id ?>" <?= $d->id == $settings->filter['selected'] ? 'selected="selected"' : ''?>><?= $d->name?></option>
	<?php endforeach; ?>
	</select>
	<?php if(isset($settings->filtermanage)) : ?>
	<a href="<?= $settings->filtermanage['url'] ?>" class="<?= $settings->filtermanage['class'] ?>" style="margin-top:-11px;"><?= $settings->filtermanage['title'] ?></a>
	<?php endif; ?>
	<script charset="utf-8" type="text/javascript">
		$(document).ready(function() {
			$("#<?= $settings->filter['id'] ?>").change(function(){
				window.location = "<?= site_url($settings->filter['url']) ?>/" + $("#<?= $settings->filter['id'] ?>").val();
			});
		});
	</script>
<?php endif; ?>
<table class="table table-striped table-condensed table-hover table-engine">
<thead>
	<tr>
	<th colspan="<?=$colspan?>" class="row-fluid">
		<div class="pull-right">
			<?php
			#
			# Delete all button
			#
			if($settings->checkboxes) { ?>
				<a href="<?=$settings->deletecheckedurl?>" class="btn btn-danger deletechecked"><i class="icon-remove icon-white"></i> <?=__('Delete checked')?></a>
			<?php
			} ?>
			<?php if(isset($settings->import_url) && !empty($settings->import_url)) : ?>
			<a href="<?=$settings->import_url?>" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> <?=__('Import')?></a>
			<?php endif; ?>
			<?php if(isset($settings->rss_url) && !empty($settings->rss_url)) : ?>
			<a href="<?=$settings->rss_url?>" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> <?=__('RSS')?></a>
			<?php endif; ?>
			<?php if(isset($settings->add_url) && !empty($settings->add_url)) : ?>
			<a href="<?=$settings->add_url?>" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> <?=__('Add %s',$settings->singular)?></a>
			<?php endif; ?>
			<?php if(isset($settings->extrabuttons) && !empty($settings->extrabuttons)) : ?>
				<?php foreach($settings->extrabuttons as $btn) : ?>
				<a class="btn btn-primary" href="<?= site_url($btn->href) ?>">
					<?php if(stristr($btn->btn_class, 'edit')) { ?><i class="icon-pencil"></i><?php } ?>
					<?php if(stristr($btn->icon_class, 'plus-sign')) { ?><i class="icon-plus-sign icon-white"></i><?php } ?>
					<?= $btn->title ?>
				</a>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<br clear="all" />
		<form class="formsearch">
			<div class="input-append">
				<input name="q" type="text" class="input-large search-query">
				<input name="filter" type="hidden" value="name"  />
				<button type="submit" class="btn"><i class="icon-search"></i> <?= __('Search')?></button>
			</div>
		</form>
		<div class="infobox searchlist span12"></div>
	</th>
	</tr>
	<tr><?php
	foreach($headers as $h => $field) {
		if($settings->checkboxes && $i == 0) {
			echo '<th style="max-width:1em !important;"><input type="checkbox" class="selectallcheckbox" /></th>';
		}
		echo "<th class='sort-alfa'>$h</th>";
	} ?>
		<th style="text-align: right; min-width:100px;"><?=__('Actions')?></th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="<?=$colspan?>">
			<div class="pagination"></div>
			<br /><br />
		</td>
	</tr>
</tfoot>
<tbody><?php

# No data
if(empty($data)) { echo '<tr><td colspan="'.$colspan.'">' . __('No data') . '</td></tr></tbody></table>'; return; }

#
# Render rows
#
$row_fmt = '<tr id="%s">' . str_repeat('%s', count($headers)) . '<td><div class="pull-right">';
foreach($settings->actions as $a) {
// 	$row_fmt.= '<a class="btn btn-small '.$a->btn_class.'" href="'.$a->href.'"><i class="'.$a->icon_class.'  icon-white"></i> '.$a->title.'</a>';
	$row_fmt.= ' <a class="btn btn-mini '.$a->btn_class.'" href="'.$a->href.'"><i class="'.$a->icon_class.'  icon-white" title="'.$a->title.'"></i></a>';
}
$row_fmt.= '</div></td></tr>';

$row_fmt_item = '<tr id="%s">' . str_repeat('%s', count($headers)) . '<td><div class="pull-right">';
foreach($settings->itemactions as $a) {
	$row_fmt_item.= ' <a class="btn btn-mini '.$a->btn_class.'" href="'.$a->href.'"><i class="'.$a->icon_class.'  icon-white" title="'.$a->title.'"></i></a>';
}
$row_fmt_item.= '</div></td></tr>';
while($struct = array_pop($data)) {
	if($struct->groupitem) {
		$d = array( $row_fmt_item, $struct->id );
	} else {
		$d = array( $row_fmt, $struct->id );
	}
	
	reset($headers);
	while(list(,$attr) = each($headers)) {
		if($settings->checkboxes && $i == 0) {
			if($attr == 'imageurl' && !empty($struct->$attr)) {
				$d[] = '<td><input type="checkbox" class="deletecheckbox" name="'.$struct->id.'" /></td> <td><a href="'.$struct->$attr.'" target="_blank"><img src="'.$struct->$attr.'" width="40" /></a></td>';
			} else {
				if($struct->groupitem) {
					$d[] = empty($struct->$attr) && $struct->$attr !== '0' ? __('not set') : '<td><input type="checkbox" class="deletecheckbox" name="item_'.$struct->id.'" /></td> <td><a href="'.$settings->item_view_url.'"><img class="imageInListview" src="img/file.png">' . $struct->$attr .'</a></td>';
				} else {
					$d[] = empty($struct->$attr) && $struct->$attr !== '0' ? __('not set') : '<td><input type="checkbox" class="deletecheckbox" name="'.$struct->id.'" /></td> <td><a href="'.$settings->view_url.'"><img class="imageInListview" src="img/folder.png">' . $struct->$attr . '</a></td>';
				}
			}
		} else {
			if($attr == 'imageurl' && !empty($struct->$attr)) {
				$d[] = '<td><a href="'.$struct->$attr.'" target="_blank"><img src="'.$struct->$attr.'" width="40" /></a></td>';
			} else {
				if($struct->groupitem) {
					$d[] = empty($struct->$attr) && $struct->$attr !== '0' ? __('not set') : '<td><a href="'.$settings->item_view_url.'"><img class="imageInListview" src="img/file.png">' . $struct->$attr .'</a></td>';
				} else {
					$d[] = empty($struct->$attr) && $struct->$attr !== '0' ? __('not set') : '<td><a href="'.$settings->view_url.'"><img class="imageInListview" src="img/folder.png">' . $struct->$attr . '</a></td>';
				}
			}
		}
	}  
	$row = call_user_func_array('sprintf', $d);
	$row = str_replace(array('--id--', '--plural--', '--parentType--', '--itemid--'), array($struct->id, strtolower($settings->plural), $settings->parentType, $struct->itemid), $row);
	echo $row;
}
?>
</tbody>
</table>
<div id="groupitem-delete-dialog" title="Remove item" style="display:none;">
  <?= __('Do you want to remove all occurences of this item in every group or just this one?') ?>
</div>