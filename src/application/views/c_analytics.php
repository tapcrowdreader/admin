<?php if(!$analytics) : ?>
	<!-- Your analytics are not available -->
<?php else : ?>
<!-- <h1>iPhone analytics</h1>
<div id="graph" style="width:600px;height:300px;"></div>
<span id="clickdata"></span><br/>
<span>Total Downloads: <?=$analytics[0]->units_total?></span><br/>
<br clear="all"/>
<select name="filterDate" id="filterDate" style="float:left;margin-bottom: 2px;">
	<option value="Filter by month">Filter by month</option>
	<?php foreach($filterDateArray as $date) : ?>
	<option value="<?=$date?>" <?= ($filterDate == $date) ? 'selected' : '' ?>><?=$date?></option>
	<?php endforeach; ?>
</select>
<div class="listview">
	<table id="listview_analytics" class="display zebra-striped">
		<thead>
			<tr>
				<th class="data order">Date</th>
				<th class="data order">Downloads</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($analytics as $row): ?>
				<tr>
					<td>
						<?=$row->date; ?>
					</td>
					<td>
						<?= (int) $row->units_day?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<p<br clear="all"/></p> -->

<script type="text/javascript">
// $(function () {
// 	var values = new Array();
// 	var i = 0;
// 	<?php foreach($analytics as $row): ?>
// 		<?php $timestamp = strtotime($row->date)  + 7200; ?>
// 		values[i] = [<?=(int)$timestamp * 1000?>,<?=$row->units_day?>];
// 		i++;
// 	<?php endforeach ?>
    
//     var plot = $.plot($("#graph"), [ values], {
// 				xaxis: { mode: "time", timeformat: "%d-%m", tickLength: 5 },
// 				selection: { mode: "x" },
//                series: {
//                    lines: { show: true },
//                    points: { show: true }
//                },
//                grid: { hoverable: true, clickable: true }
// 	});
//     $("#graph").bind("plotclick", function (event, pos, item) {
//         if (item) {
// 			plot.unhighlight();
// 			var date = new Date(values[item.dataIndex][0]);
// 			var month = date.getMonth() + 1;
//             $("#clickdata").text("Downloads op " + date.getFullYear() + "-" + month + "-" + date.getDate() + ': ' + values[item.dataIndex][1]);
// 			plot.highlight(item.series, item.datapoint);
//         }
//     });
	
// 	$('#listview_analytics_length').css('display', 'none');
	
// 	$('#filterDate').change(function() {
// 		if($(this).val() != 'Filter by month') {
// 			var url = "<?= site_url('analytics/app/'.$app->id); ?>";
// 			var value = url + '/' + $(this).val();
// 			window.location.href = value;
// 		}
// 	});
// });
</script>
<?php endif; ?>

<?php if($app->bundle != '') : ?>
	
<?php elseif(isset($pending)) : ?>
	<div style="height:100%;display:block;">
		<p style=""><?=__('We have received your request and will activate analytics soon.')?></p>
	</div>
	<style type="text/css">
		#wrapper {
			background:none;
		}
	</style>
<?php else : ?>
	<div style="height:100%;display:block;">
		<p style=""><?=__('Analytics is not yet enabled for your app. If you want to enable analytics you can request it now.')?></p>
		<p style="margin-top:10px;"><a href="<?=site_url('analytics/activate/'.$app->id)?>" class="btn primary"><?= __('Activate Analytics') ?></a></p>
	</div>
	<style type="text/css">
		#wrapper {
			background:none;
		}
	</style>
<?php endif; ?>
<iframe 
	id="analyticsframe" 
	src="http://<?=str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME'])?>/<?=$controller?>?frame=true&bundle=<?=$app->bundle?>&h=<?=md5('TCAnalytics'.$app->bundle)?>" 
	width="1010px"
	height="auto" 
	style="display: block;
    height: 100%;
    position: absolute;" 
	frameborder="0" 
	scrolling="auto" />
</iframe>
