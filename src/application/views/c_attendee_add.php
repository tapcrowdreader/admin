<h1><?= __('Add Item') ?></h1>
<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
			<label for="name"><?= __('Name:') ?></label>
			<input type="text" name="name" id="name" value="<?= set_value('name') ?>" />
		</p>
		<p <?= (form_error("fname") != '') ? 'class="error"' : '' ?>>
			<label for="fname"><?= __('First Name:') ?></label>
			<input type="text" name="fname" id="fname" value="<?= set_value('fname') ?>" />
		</p>
		<?php if(externalIds()) : ?>
		<p <?= (form_error("external_id") != '') ? 'class="error"' : '' ?>>
			<label for="external_id"><?= __('External id:') ?></label>
			<input type="text" name="external_id" id="external_id" value="<?= set_value('external_id') ?>" />
		</p>
		<?php endif; ?>
		<p <?= (form_error("company") != '') ? 'class="error"' : '' ?>>
			<label for="company"><?= __('Company:') ?></label>
			<input type="text" name="company" id="company" value="<?= set_value('company') ?>" />
		</p>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("function_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="function_<?=$language->key?>"><?= __('Function') ?><?= '(' . $language->name . ')'; ?>:</label>
                <input type="text" name="function_<?=$language->key?>" id="function_<?=$language->key?>" value="<?= set_value('function_'.$language->key) ?>" />
            </p>
            <?php endforeach; ?>
		<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
			<label for="email"><?= __('Email:') ?></label>
			<input type="text" name="email" id="email" value="<?= set_value('email') ?>" />
		</p>
		<p <?= (form_error("linkedin") != '') ? 'class="error"' : '' ?>>
			<label for="linkedin"><?= __('Linkedin:') ?></label>
			<input type="text" name="linkedin" id="linkedin" value="<?= set_value('linkedin') ?>" />
			<span class="note" style="width:500px;">http://www.linkedin.com/in/profilename</span>
		</p>
		<p <?= (form_error("facebook") != '') ? 'class="error"' : '' ?>>
			<label for="facebook"><?= __('Facebook:') ?></label>
			<input type="text" name="facebook" id="facebook" value="<?= set_value('facebook') ?>" />
		</p>
		<p <?= (form_error("twitter") != '') ? 'class="error"' : '' ?>>
			<label for="twitter"><?= __('Twitter:') ?></label>
			<input type="text" name="twitter" id="twitter" value="<?= set_value('twitter') ?>" />
		</p>
		<p <?= (form_error("phone") != '') ? 'class="error"' : '' ?>>
			<label for="phone"><?= __('Phone:') ?></label>
			<input type="text" name="phone" id="phone" value="<?= set_value('phone') ?>" />
		</p>
		<p <?= (form_error("country") != '') ? 'class="error"' : '' ?>>
			<label for="country"><?= __('Country:') ?></label>
			<input type="text" name="country" id="country" value="<?= set_value('country') ?>" />
		</p>
        <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="description"><?= __('Description') ?><?= '(' . $language->name . ')'; ?>:</label>
                <textarea name="description_<?php echo $language->key; ?>" id="description"><?= set_value('description_'.$language->key) ?></textarea>
            </p>
            <?php endforeach; ?>
        <?php else : ?>
            <p <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="description"><?= __('Description:') ?></label>
                <textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description"><?= set_value('description_'.$app->defaultlanguage) ?></textarea>
            </p>
        <?php endif; ?>
		<div class="row">
			<label for="tags">Tags:</label>
			<ul id="mytags" name="mytagsul"></ul>
		</div>
		<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
			<label><?=__('Order:')?></label>
			<input type="text" name="order" value="<?= set_value('order') ?>" id="order"><br/>
		</p>
			<p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl"><?= __('Image') ?>:</label>
				<input type="file" name="imageurl" id="imageurl" value="" /><br />
				<span class="hintImage"><?= __('Maximum size: %sx%spx, jpg or png',2000,2000) ?></span>
				<br clear="all" />
			</p>
        <?php foreach($metadata as $m) : ?>
			<?php foreach($languages as $lang) : ?>
				<?php if(_checkMultilang($m, $lang->key, $app)): ?>
					<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
						<?= _getInputField($m, $app, $lang, $languages); ?>
					</p>
				<?php endif; ?>
			<?php endforeach; ?>
        <?php endforeach; ?>
<!-- 		<p <?= (form_error("premium") != '') ? 'class="error"' : '' ?> class="premiump">
			<input type="checkbox" name="premium" class="checkboxClass premiumCheckbox" id="premium"> <?=__('Premium?')?><br clear="all" />
		</p>
		<p <?= (form_error("order") != '') ? 'class="error"' : '' ?> id="extralinep">
			<label><?=__('Premium order:')?></label>
			<input type="text" name="premiumorder" value="<?= set_value('premiumorder') ?>" id="premiumorder"><br clear="all" />
			<label><?=__('Extra line:')?></label>
			<input type="text" name="extraline" value="<?= set_value('extraline') ?>" id="extraline"><br clear="all" />
		</p> -->
		<?php if($confbagactive) : ?>
		<p <?= (form_error("confbag") != '') ? 'class="error"' : '' ?>>
			<label for="confbag"><?= __('Conference bag content:') ?></label>
			<input type="file" name="confbagcontent" id="confbagcontent" />
			<br clear="all" />
		</p>
		<?php endif; ?>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<button type="submit" class="btn primary"><?= __('Add Attendee') ?></button>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if($postedtags){foreach($postedtags as $val){ echo '"'.$val.'", '; }} ?>],
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = '<?=$tag->tag?>';
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
	});
</script>