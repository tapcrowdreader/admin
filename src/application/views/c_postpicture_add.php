<div>
	<h1><?=__('Add Item')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          
			<p <?= (form_error("description") != '') ? 'class="error"' : '' ?>>
				<label for="description"><?= __('Description:') ?></label>
				<input type="text" name="description" id="description" value="<?= set_value('description') ?>" />
			</p>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:') ?></label>
				<input type="file" name="image" id="image" value="" class="logoupload" />
			</p><br />
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>