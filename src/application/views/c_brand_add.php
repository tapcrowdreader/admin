<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<h3><?= __('Add New Brand') ?></h3>
		<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
			<label for="name"><?= __('Name:') ?></label>
			<input type="text" name="name" id="name" value="<?= set_value('name') ?>" />
		</p>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<button type="submit" class="btn primary"><?= __('Add Brand') ?></button>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>