<div>
	<h1><?=__('Add Info')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="title"><?=__('Title')?> <?= '(' . $language->name . ')'; ?>:</label>
                <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?=set_value('title_'.$language->key)?>" />
            </p>
            <?php endforeach; ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("text_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="text"><?=__('Text')?> <?= '(' . $language->name . ')'; ?>:</label>
                <textarea name="text_<?php echo $language->key; ?>" id="text"><?= set_value('text_'.$language->key) ?></textarea>
            </p>
            <?php endforeach; ?>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add Info') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>