<?php if(isset($launcherid) && $launcherid != 0) : ?>
<a href="<?= site_url('module/editLauncher/'.$launcherid) ?>" class="edit btn" style="float:right;margin-left:10px;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a>
<?php endif; ?>

<div class="events">
	<?php if($events != false && ($app->familyid == 1) && count($events) > 0) : ?>

	<?php else: ?>
		<?php if($launcherid == '') : ?>
		<a href="<?= site_url('event/add') ?>" class="add btn primary">
			<i class="icon-plus-sign icon-white"></i>  <?= __('Add New Event') ?>
		</a>
		<?php else: ?>
		<a href="<?= site_url('event/add/'.$launcherid) ?>" class="add btn primary">
			<i class="icon-plus-sign icon-white"></i>  <?= __('Add New Event') ?>
		</a>
		<?php endif; ?>

        <?php if($app->familyid == 3) : ?>
    	<a href="<?= site_url('import/events/'.$launcherid) ?>" class="add btn primary" style="margin-right:5px;">
			<i class="icon-plus-sign icon-white"></i>  <?= __('Import') ?>
		</a>
		<?php endif; ?>

		<?php if($app->familyid == 3 && isset($launcherid) && $launcherid != 0) : ?>
			<a href="<?= site_url('events/removelauncher/'.$launcherid) ?>" class="btn btn-danger" style="float:right;margin-right:5px;">
				<?=__('Remove module')?>
			</a>
		<?php endif; ?>
		<?php if(_isAdmin()) : ?>
		<a href="<?= site_url('module/fields/'.$launcherid.'/app/'.$app->id) ?>" class="btn" style="float:right;margin-right:5px;">
			<?=__('Custom fields')?>
		</a>
		<?php endif; ?>
		
		<?php if($showButtonMetadata) : ?>
<!-- 			<a href="<?= site_url('metadata/copy/'.$launcherid) ?>" class="btn" style="float:right;margin-right:5px;">
				<?=__('Copy Metadata')?>
			</a> -->
		<?php endif; ?>

		<br><br>
	<?php endif; ?>

	<?php if ($api_events != FALSE): ?>
	<div class='event_import'>
		<select name="sel_events" id="sel_events">
			<option value=""><?= __('Use existing event ...') ?></option>
			<?php foreach ($api_events as $event): ?>
				<option value="<?= site_url('events/import/'.$event->id) ?>"><?= $event->title ?></option>
			<?php endforeach ?>
		</select>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#sel_events').change(function(e){
					var url = $(this).val();
					if($(this).val() != '') {
						e.preventDefault();
						jConfirm('<?= __('Are you sure you want to import this event?') ?>', '<?= __('Import Event') ?>', function(r) {
							if(r == true) {
								window.location = '' + url;
								return true;
							} else {
								jAlert('<?=__('Import cancelled!')?>', '<?=__('Info')?>');
								return false;
							}
						});
						return false;
					}
				});
			});
		</script>
	</div>
	<?php endif ?>
<?php if ($events != FALSE): ?>
	<?php foreach ($events as $event): ?>
		<div class="event_wrapper" id="<?=$event->id?>">
			<a href="<?= site_url('event/view/'.$event->id.'/'.$launcherid) ?>" class="event">
				<?php $CI =& get_instance(); ?>
				<?php if ($event->eventlogo != ''): ?>
					<div class="eventlogo" style="background:transparent url('<?= image_thumb($event->eventlogo, 50, 50) ?>') no-repeat center center">&nbsp;</div>
				<?php else: ?>
					<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
				<?php endif ?>
				<h1><span><?= $event->name ?></span></h1>
				<?php if(date('d-m-Y', strtotime($event->datefrom))== '01-01-1970') : ?>
					<?=__('Permanent event')?>
				<?php else: ?>
					<?= date('d-m-Y', strtotime($event->datefrom)) ?> - <?= date('d-m-Y', strtotime($event->dateto)) ?>
				<?php endif; ?>
			</a>
			<br />
			<?php foreach($event->tags as $tag) : ?>
				<span class="tagInEventList"><?=$tag->tag ?></span>
			<?php endforeach; ?>
			<a href="<?= site_url('event/edit/'.$event->id.'/'.$launcherid) ?>" class="edit btn">
				<i class="icon-pencil"></i>  <?=__('Edit')?>
			</a>
			<?php if ($event->active == 1): ?>
				<a href="<?= site_url('event/active/' . $event->id . '/off') ?>" class="onoff deactivate"><span><?= __('Deactivate event') ?></span></a>
			<?php else: ?>
				<a href="<?= site_url('event/active/' . $event->id . '/on') ?>" class="onoff activate"><span><?= __('Activate event') ?></span></a>
			<?php endif ?>
		</div>
	<?php endforeach ?>
<?php endif; ?>
</div>