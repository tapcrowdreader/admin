<h1><?= __('Push notification') ?></h1>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<div class="account">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" class="data">
		<?php if($error != ''): ?>
		<div class="error"><?= $error ?></div>
		<?php endif ?>
		<p <?= (form_error("pushtext") != '') ? 'class="error"' : '' ?>>
			<label><?= __('Notification:') ?></label>
			<textarea name="pushtext" id="pushtext" rows="8" cols="40" style="height:100px;"><?= set_value('pushtext') ?></textarea>
		</p>
<!--         <p style="line-height: 25px;">
            <label style="margin-bottom:50px;"><?= __('Type:') ?></label>
            <input class="checkboxClass" type="radio" name="type" value="text" checked /> <?= __('Text') ?> <br />
            <input class="checkboxClass" type="radio" name="type" value="number" /><?= __('Number') ?> <br />
        </p> -->
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" class="btn primary"><?= __('SEND') ?></button>
			<br clear="all" />
		</div>
		<br clear="all" />
	</form>
</div>