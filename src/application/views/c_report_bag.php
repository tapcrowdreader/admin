<style type="text/css">	

	table
	{
		margin-top: 1em;
		border: 1px solid #AAAAAA;
	}
	
	table tr
	{
		width: 100%;
	}
	
	table td
	{
		border-bottom: 1px solid #DDDDDD;
	}
	
	table td.userbag
	{
		background-color: whitesmoke;
	}
	
	table th
	{
		background-color: lightgrey;
		border-bottom: 1px solid #AAAAAA;
	}
	
</style>


<?php

function html_show_array($array){
  echo "<table>\n";
  echo "<tr><th>User</th><th>Type</th><th>Name</th><th>Categories</th>";
  show_array($array);
  echo "</table>\n";
}


function show_array($array){
	foreach($array as $user){
		echo '<tr>';
		echo '<td class="userbag">'.$user['user']."</td><td  class=\"userbag\"></td><td  class=\"userbag\"> </td><td  class=\"userbag\"> </td>\n";
		show_element($user['items']);
		echo "</tr>\n";
	}
}

function show_element($array){
	foreach($array as $item){
		echo "<tr>\n";
			echo "<td> </td>\n";
			if($item['type'] == "Exhibitor"){
				echo '<td>'.$item['type']."</td>\n";
				echo '<td>'.$item['element']."</td>\n";
				$categories = $item['categories'];
				if(empty($categories))
					echo "<td> </td>\n";
				else {
					echo '<td>';
					foreach($categories as $category){
						echo "(".$category.")";
					}
					echo '</td>';
				}
			}
			else{
				echo '<td>'.$item['type']."</td>\n";
				echo '<td>'.$item['element']."</td>\n";
				echo "<td> </td>\n";
			}
		echo "<tr>\n";
	}
}

?>

<div id="report">

	<div class="header">
	<h1><?= __('Reports')?></h1>
	

	<input type="hidden" value="" id="name" name="name">
	<button class="add btn primary"><?=__("Download Report as CSV File")?></button>
	</div>
	<div class="listitems">
		<?php html_show_array($report); ?>
	</div>
</div>

<script type="text/javascript">
$("#name").val("<?=$name?>");

$("div.header button.add").click(
	function()
	{
		window.location.href = "reports/personal/<?=$type?>/1/<?=$typeid?>";
	}
)

</script>