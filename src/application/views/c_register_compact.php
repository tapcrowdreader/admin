<!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
	<script src="/js/jquery-1.7.2.js"></script>
	<style>

form {
    background-color: #F7F8FC;
    border: 1px solid #DBDEEB;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1) inset;
    display: block;
    height: 100px;
    margin: 0 20px;
    width: 280px;
}
input {
    background: none repeat scroll 0 0 transparent;
    border: medium none;
    color: #5B6074;
    font-size: 14px;
    height: 50px;
    padding: 0 10px;
    width: 260px;
    font-size: 0.929em;
    margin: 2px 0;
}

button {
    background-color: #26A8E0;
    background-image: linear-gradient(to top, #26A8E0, #3DC1F1);
    border: 1px solid #CFCFCF;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1), 0 -1px rgba(0, 0, 0, 0.5) inset, 0 0 4px rgba(0, 0, 0, 0.35) inset;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 18px;
    font-weight: 800;
    height: 40px;
    margin: 7px 0 0;
    text-shadow: 0 1px #005866;
    width: 280px;
}
	</style>
</head>
<body>
<form method="POST" action="/auth/save" accept-charset="utf-8" id="register_box">

<!-- 	<div class="control-group"> -->
		<input type="text" placeholder="email" name="auth_email" class="" />
<!-- 	</div> -->

<!-- 	<div class="control-group"> -->
		<input type="password" name="auth_pass" class="" />
<!-- 	</div> -->

<!-- 	<div class="control-group"> -->

		<button type="submit" class=""><?=__('Start creating your app')?></button>

		<input type="hidden" name="auth_fn" />
		<input type="hidden" name="auth_login">
		<input type="hidden" name="auth_pass2" />
		<input type="hidden" name="auth_token" value="<?=$auth_token?>" />
<!-- 	</div> -->
</form>
<script>
$(document).ready(function()
{
$('#register_box input[type=password]').click(function(){this.value='';});
$('#register_box input[type=text]').focus(function(){this.select();});
$('#register_box').submit(function()
{
	this.auth_fn.value = this.auth_email.value;
	this.auth_login.value = this.auth_email.value;
	this.auth_pass2.value = this.auth_pass.value;
});
});
</script>
</body>
</html>
