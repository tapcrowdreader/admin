<script type="text/javascript">
$(function(){
	$("#jqueryselectbrands").multiselect({
	   click: function(event, ui){
	   	if(ui.checked) {
	   		$("#brandshidden").val($("#brandshidden").val()+'/'+ui.value);
	   	} else {
	   		var val = $("#brandshidden").val();
	   		var valarray = val.split('/');
	   		var newstring = '';
	   		for(var index in valarray) {
	   			var obj = valarray[index];
	   			if(obj != '' && obj != ui.value) {
	   				newstring = newstring + '/' + obj;
	   			}
	   		}
	   		$("#brandshidden").val(newstring);
	   	}
	   },
		selectedList: 4
	});

	$("#jqueryselectcats").multiselect({
	   click: function(event, ui){
	   	if(ui.checked) {
	   		$("#catshidden").val($("#catshidden").val()+'/'+ui.value);
	   	} else {
	   		var val = $("#catshidden").val();
	   		var valarray = val.split('/');
	   		var newstring = '';
	   		for(var index in valarray) {
	   			var obj = valarray[index];
	   			if(obj != '' && obj != ui.value) {
	   				newstring = newstring + '/' + obj;
	   			}
	   		}
	   		$("#catshidden").val(newstring);
	   	}
	   },
		selectedList: 4
	});
});
</script>
<div>
	<h1><?= __('Edit %s', $exhibitor->name) ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
            <div class="error"><p><?= __('Something went wrong with the image upload. <br/> Maybe the image size exceeds the maximum width or height</p>') ?></div>
            <?php endif ?>
			<?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="name"><?= __('Name') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('exhibitor', $exhibitor->id, 'name', $language->key); ?>
                <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?= htmlspecialchars_decode(set_value('name_'.$language->key, ($trans != null) ? $trans : $exhibitor->name), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            <?php else : ?>
            <p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
                <label for="name"><?= __('Name:') ?></label>
                <input type="text" name="name_<?php echo $app->defaultlanguage; ?>" id="name" value="<?= htmlspecialchars_decode(set_value('name_'.$app->defaultlanguage, $exhibitor->name), ENT_NOQUOTES) ?>"/>
            </p>
            <?php endif; ?>
			<?php if (!isset($venue)): ?>
			<p <?= (form_error("booth") != '') ? 'class="error"' : '' ?>>
				<label for="booth"><?= __('Booth:') ?></label>
				<input type="text" name="booth" value="<?= htmlspecialchars_decode(set_value('booth', $exhibitor->booth), ENT_NOQUOTES) ?>" id="booth">
			</p>
			<?php endif ?>
			<?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p>
                <label for="description"><?= __('Description') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('exhibitor', $exhibitor->id, 'description', $language->key); ?>
                <textarea name="description_<?php echo $language->key; ?>" id="description" <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>><?= set_value('description_'.$language->key, ($trans != null) ? $trans : $exhibitor->description) ?></textarea>
            </p>
            <?php endforeach; ?>
            <?php else : ?>
            <p>
                <label for="description"><?= __('Description:') ?></label>
                <textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description" <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>><?= htmlspecialchars_decode(set_value('description_'. $app->defaultlanguage, $exhibitor->description), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endif; ?>
			<p <?= (form_error("address") != '') ? 'class="error"' : '' ?>>
				<label for="address"><?= __('Address:') ?></label>
				<input type="text" name="address" value="<?= htmlspecialchars_decode(set_value('address', $exhibitor->address), ENT_NOQUOTES) ?>" id="address">
			</p>
			<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
				<label for="email"><?= __('Email:') ?></label>
				<input type="text" name="email" value="<?= htmlspecialchars_decode(set_value('email', $exhibitor->email), ENT_NOQUOTES) ?>" id="email">
			</p>
			<p <?= (form_error("tel") != '') ? 'class="error"' : '' ?>>
				<label for="tel"><?= __('Tel.:') ?></label>
				<input type="text" name="tel" value="<?= htmlspecialchars_decode(set_value('tel', $exhibitor->tel), ENT_NOQUOTES) ?>" id="tel">
			</p>
			<p <?= (form_error("web") != '') ? 'class="error"' : '' ?>>
				<label for="web"><?= __('Web:') ?></label>
				<input type="text" name="web" value="<?= htmlspecialchars_decode(set_value('web', $exhibitor->web), ENT_NOQUOTES) ?>" id="web">
			</p>
			<p <?= (form_error("imageurl1") != '') ? 'class="error"' : '' ?>>
				<label for="image1"><?= (!isset($venue)) ? __('Logo') : __('Image') ?>:</label>
				<span class="evtlogo" <?php if($exhibitor->imageurl != ''){ ?>style="width:50px; height:50px; background:transparent url('<?= image_thumb($exhibitor->imageurl, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="imageurl" id="imageurl" value="" class="logoupload" style="width:360px;" /><br />
				<br clear="all"/>
				<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
				<?php
				$type = 'event';
				if(isset($venue)) {
					$type = 'venue';
				}
				?>
				<?php if($exhibitor->imageurl != '' && file_exists($this->config->item('imagespath') . $exhibitor->imageurl)){ ?><span><a href="<?= site_url('exhibitors/removeimage/'.$exhibitor->id.'/'.$type) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
			</p><br clear="all"/>
			<?php if(!isset($groupid) || $groupid == 0 && $maincat == false) : ?>
				<p>
					<label for="selbrands"><?= __('Brands') ?></label>
					<select name="selbrands[]" id="selbrands" multiple size="15" class="multiple">
						<?php if ($brands != FALSE && count($brands) > 0): ?>
						<?php foreach ($brands as $brand): ?>
							<option value="<?= $brand->id ?>" <?= set_select('selbrands[]', $brand->id, (in_array($brand->id, $exhibitor->brands) ? TRUE : '')) ?>><?= $brand->name ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<p>
					<label for="selcategories"><?= __('Categories') ?></label>
					<select name="selcategories[]" id="selcategories" multiple size="15" class="multiple">
						<?php if ($categories != FALSE && count($categories) > 0): ?>
						<?php foreach ($categories as $category): ?>
							<option value="<?= $category->id ?>" <?= set_select('selcategories[]', $category->id, (in_array($category->id, $exhibitor->categories) ? TRUE : '')) ?>><?= $category->name ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<?php if (!isset($venue)): ?>
				<p><label></label><?= __('Add one or more brands/categories first <a href="%s">here</a>', site_url('brands/event/'.$event->id)) ?></p>
				<?php else: ?>
				<p><label></label><?= __('Add one or more brands/categories first <a href="%s">here</a>', site_url('brands/venue/'.$venue->id)) ?></p>
				<?php endif ?>
				<?php if (!isset($venue)): ?>
<!-- 					<?php for($i=1; $i < 21; $i++){ ?>
						<p>
							<label for="imageurl<?= $i ?>">Image <?= $i ?>:</label>
							<span class="evtlogo" <?php if($exhibitor->{"image".$i} != ''){ ?>style="background:transparent url('<?= image_thumb($exhibitor->{"image".$i}, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
							<input type="file" name="imageurl<?= $i ?>" id="imageurl<?= $i ?>" value="" class="logoupload" />
						</p><br clear="all"/>
					<?php } ?> -->
				<?php endif ?>
			<?php else: ?>
				<?php //we're adding an item to a group so let user choose groups ?>
<!-- 				<p>
					<label><?= __('Add to Brand') ?></label>
					<select title="Basic example" class="jqueryselect" multiple="multiple" name="brands" size="5" id="jqueryselectbrands">
						<?php foreach($brandshtml as $option) : ?>
							<?= $option ?>
						<?php endforeach; ?>
					</select>
				</p> -->
				<p>
					<label><?= __('Add to Category') ?></label>
					<select title="Basic example" class="jqueryselect" multiple="multiple" name="cats" size="5" id="jqueryselectcats">
						<?php foreach($catshtml as $option) : ?>
							<?= $option ?>
						<?php endforeach; ?>
					</select>
				</p>
			<?php endif; ?>
			<?php if($confbagactive) : ?>
			<p <?= (form_error("confbag") != '') ? 'class="error"' : '' ?>>
				<label for="confbag"><?= __('Conference bag content:') ?></label>
				<input type="file" name="confbagcontent" id="confbagcontent" />
				<br clear="all" />
				<?php foreach($exhibitorconfbag as $c) : ?>
					<span style="width:auto;"><?=substr($c->documentlink, strrpos($c->documentlink,'/') +1,strlen($c->documentlink))?><a href="<?=site_url('confbag/removeitem/'.$c->id.'/event/exhibitors');?>"  style="border-bottom:none; margin-left:5px;"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></span><br clear="all"/>
				<?php endforeach;?>
			</p>
			<?php endif; ?>
			<div class="row">
				<label for="tags">Tags:</label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order', $exhibitor->order) ?>" id="order"><br/>
			</p>
			<?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<?php if(_checkMultilang($m, $lang->key, $app)): ?>
						<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
							<?= _getInputField($m, $app, $lang, $languages, 'exhibitor', $exhibitor->id); ?>
						</p>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
			<p <?= (form_error("premium") != '') ? 'class="error"' : '' ?> class="premiump">
				<input type="checkbox" name="premium" class="checkboxClass premiumCheckbox" <?= ($premium) ? 'checked="checked"' : '' ?> id="premium"><?=__(' Premium?')?>
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?> id="extralinep">
				<label><?=__('Premium order:')?></label>
				<input type="text" name="premiumorder" value="<?= set_value('premiumorder', $premium->sortorder) ?>" id="premiumorder">
				<label><?=__('Extra line:')?></label>
				<input type="text" name="extraline" value="<?= set_value('extraline', (isset($premium->extraline)) ? $premium->extraline : '') ?>" id="extraline">
				<label><?=__('Title:')?></label>
				<input type="text" name="premiumtitle" value="<?= set_value('premiumtitle', $premiumtitle) ?>" id="premiumtitle">
			</p>
			<div class="buttonbar">
				<input type="hidden" name="brandshidden" value="<?= $currentBrands ?>" id="brandshidden">
				<input type="hidden" name="catshidden" value="<?= $currentCats ?>" id="catshidden">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= (isset($venue)) ? site_url('exhibitors/venue/'.$venue->id) : site_url('exhibitors/event/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if(isset($postedtags) && $postedtags != false && !empty($postedtags)) { 
				foreach($postedtags as $val){ 
					echo '"'.$val.'", ';
				} 
			} elseif (isset($tags) && $tags != false) {
				foreach($tags as $val){ 
					echo "\"".$val->tag."\"," ;
				}
			} ?>]
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = "<?=$tag->tag?>";
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
	});
</script>
