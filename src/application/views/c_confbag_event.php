<div class="header">
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<h1><?= $title ?></h1>
	<a href="<?= site_url('module/editByController/confbag/event/'.$event->id) ?>" class="edit btn" style="float:right;margin-bottom:5px;margin-top:-35px;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a><br style="margin-bottom:0px;clear:both;"/>
	<a href="reports/view/personal/event"> <button class="add btn primary"><?= __("Reports")?></button></a> <a href="<?= site_url('confbag/maillayout/'.$event->id.'/event') ?>"><button class="add btn primary"><?= __('Mail layout') ?></button></a> <a href="<?= site_url('confbag/mail/'.$event->id)?>"> <button class="add btn primary"><?= __("Send mail to users")?></button></a> <a href="<?= site_url('confbag/testmail/'.$event->id)?>"> <button class="add btn primary"><?= __("Send test mail")?></button></a><br style="margin-bottom:15px;"/>

	<p>
		<?= __('The conference bag is now activated in your app. Users of your app can use the conference bag to keep track of their favorite sessions, exhibitors, attendees and content.
You can add content to individual sessions, such as powerpoint slides. Use the link below to edit sessions and to add content to sessions.
Similar, you can add content to individual exhibitors (e.g. a brochure as a PDF file) and to attendees.') ?><br/>
		<a href="<?=site_url('sessions/event/'.$event->id);?>"><?= __('Edit sessions') ?></a><br/>
		<a href="<?=site_url('exhibitors/event/'.$event->id);?>"><?= __('Edit exhibitors') ?></a><br/>
		<a href="<?=site_url('attendees/event/'.$event->id);?>"><?= __('Edit attendees') ?></a>
	</p>
</div>
