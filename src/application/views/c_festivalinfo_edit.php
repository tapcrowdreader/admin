<div>
	<h1><?=__('Edit info')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
            <div class="error"><p><?= __('Something went wrong with the image upload. <br/> Maybe the image size exceeds the maximum width or height</p>') ?></div>
            <?php endif ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
            <?php $trans = _getTranslation('metadata', $metadata->id, 'key', $language->key); ?>
                <label for="title"><?=__('Title')?> <?= '(' . $language->name . ')'; ?>:</label>
                <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$language->key, ($trans != null) ? $trans : $metadata->key), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
            <?php $trans = _getTranslation('metadata', $metadata->id, 'value', $language->key);?>
                <label for="description"><?=__('Text')?> <?= '(' . $language->name . ')'; ?>:</label>
                <textarea name="text_<?php echo $language->key; ?>" id="text"><?= htmlspecialchars_decode(set_value('text_'.$language->key, ($trans != null) ? $trans : $metadata->value), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endforeach; ?>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('festivalinfo/event/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>