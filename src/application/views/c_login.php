
<form class="form-horizontal" method="POST" action="/auth/authenticate">

	<div class="control-group">
		<label class="control-label" for="inputLogin"><?=__('Login or email')?></label>
		<div class="controls">
			<input type="text" id="inputLogin" placeholder="Login" name="auth_login" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputPassword"><?=__('Password')?></label>
		<div class="controls">
			<input type="password" id="inputPassword" placeholder="Password" name="auth_pass" />
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn btn-primary"><?=__('Sign in')?></button>
		</div>
	</div>
</form>

<div class="center">
	<p><?=__('Lost your password? Retrieve it')?> <a href="/auth/recovery"><strong><?=__('here')?></strong></a></p>
	<p style="padding-bottom:30px;"><?=__('Not yet registered? Register')?> <a href="/auth/register"><strong><?=__('here')?></strong></a></p>
</div>
