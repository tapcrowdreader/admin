<h2><?= __('Please choose an event module') ?></h2>
<?php if(empty($launchers)) : ?>
	<p><?= __("You don't have event modules. ") ?><a href="<?= site_url('apps/view/'.$app->id)?>"><?= __("Please create one first."); ?></a></p>
<?php else: ?>
	<div class="events eventlistapp">
	<?php foreach($launchers as $l) : ?>
	<div class="event_wrapper">
		<a class="event editTagA" href="<?= site_url('events/launcher/'.$l->id)?>">
			<div class="eventlogo">
			<img width="50" height="50" src="<?= $this->config->item('publicupload') . $l->imageurl ?>">
			</div>
			<h1>
				<span><?=$l->title?></span>
			</h1>
		</a>
	</div>
	<?php endforeach; ?>
	</div>
<?php endif; ?>