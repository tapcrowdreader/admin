<style>
.editTagA {
    float: left;
    width: 520px;
}

</style>
<script>
  $(document).ready(function() {
//	$( "#sortable_venues" ).sortable({
//	   stop: function(event, ui) {
//		   var result = $("#sortable_venues").sortable('serialize');
//		   var ids = [];
//		   $(".venue_order").each(function(index) {
//			   ids.push(this.id);
//		   });
//			$.post("venues/sort", { ids: ids } );
//		}
//	});
//
//	$( "#sortable_events" ).sortable({
//	   stop: function(event, ui) {
//		   var result = $("#sortable_events").sortable('serialize');
//		   var ids = [];
//		   $(".event_order").each(function(index) {
//			   ids.push(this.id);
//		   });
//			$.post("events/sort", { ids: ids } );
//		}
//	});

	$('.changeTag').change(function() {
		var id = $(this).val();
		var text = $(this).find("option:selected").text();
		if(id != 'add' && id.substring(0, 3) != 'new') {
			$.post("venue/addtag", { venueid: id, tag: text }, function(data) {
				location.reload();
			} );
		}

		if(id.substring(0, 3) == 'new') {
			$('#divTagsForm').css('display','block');
			$('#venueidForm').val(id.substring(3,id.length));
		}
	});

	$('.changeTagEvent').change(function() {
		var id = $(this).val();
		var text = $(this).find("option:selected").text();
		if(id != 'add' && id.substring(0, 3) != 'new') {
			$.post("event/addtag", { eventid: id, tag: text }, function(data) {
				location.reload();
			} );
		}

		if(id.substring(0, 3) == 'new') {
			$('#divTagsFormEvent').css('display','block');
			$('#eventidForm').val(id.substring(3,id.length));
		}
	});

	$('.changeTagCitycontent').change(function() {
		var id = $(this).val();
		var text = $(this).find("option:selected").text();
		if(id != 'add' && id.substring(0, 3) != 'new') {
			$.post("citycontent/addtag", { citycontentid: id, tag: text }, function(data) {
				location.reload();
			} );
		}

		if(id.substring(0, 3) == 'new') {
			$('#divTagsFormCitycontent').css('display','block');
			$('#citycontentidForm').val(id.substring(3,id.length));
		}
	});

	$('#cancelButton').click(function(event) {
		event.preventDefault();
		$('#divTagsForm').css('display','none');
	});
  });

</script>

<div id="divTagsForm" class="frmsessions frmAddTag">
	<form action="<?= site_url('venue/addtag') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
		<h3 style="color:#FFFFFF">Add new menu item</h3>
		<p style="color: #FFFFFF">
			<label style="color: #FFFFFF">Name:</label>
			<input type="text" name="tag" value="" width="100px" />
		</p>
		<div class="buttonbar">
		<input type="hidden" id="venueidForm" name="venueid"/>
		<a href="<?= site_url('apps/view/'.$app->id)?>" id="cancelButton" class="btn">Cancel</a>
		<button type="submit" class="btn primary" name="saveTag">Save menu item</button>
		</div>
	</form>
</div>
<div id="divTagsFormEvent" class="frmsessions frmAddTag">
	<form action="<?= site_url('event/addtag') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
		<h3 style="color:#FFFFFF">Add new menu item</h3>
		<p style="color: #FFFFFF">
			<label style="color: #FFFFFF">Name:</label>
			<input type="text" name="tag" value="" width="100px" />
		</p>
		<div class="buttonbar">
		<input type="hidden" id="eventidForm" name="eventid"/>
		<a href="<?= site_url('apps/view/'.$app->id)?>" id="cancelButton" class="btn">Cancel</a>
		<button type="submit" class="btn primary" name="saveTag">Save menu item</button>
		</div>
	</form>
</div>
<div id="divTagsFormCitycontent" class="frmsessions frmAddTag">
	<form action="<?= site_url('citycontent/addtag') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
		<h3 style="color:#FFFFFF">Add new menu item</h3>
		<p style="color: #FFFFFF">
			<label style="color: #FFFFFF">Name:</label>
			<input type="text" name="tag" value="" width="100px" />
		</p>
		<div class="buttonbar">
		<input type="hidden" id="citycontentidForm" name="citycontentid"/>
		<a href="<?= site_url('apps/view/'.$app->id)?>" id="cancelButton" class="btn">Cancel</a>
		<button type="submit" class="btn primary" name="saveTag">Save menu item</button>
		</div>
	</form>
</div>
<div>
<?php $CI =& get_instance(); ?>
	<h1><?= $app->name ?> App</h1>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<div class="eventinfo">
		<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
		<span class="info_title"><b>Name:</b></span> <?= $app->name ?><br />
		<?php if(isset($flavor) && $flavor != false) : ?>
        <span class="info_title"><b>Flavor:</b></span> <?= $flavor->name ?><br />
        <?php endif; ?>
        <br/>
        <div class="button_bar">
	        <?php if (!$this->session->userdata('mijnevent')): ?>
	        <a href="<?= site_url('apps/delete/'.$app->id) ?>" class="delete btn btn-danger" id="deletebtn"><i class="icon-remove icon-white"></i> <span> Remove app</span></a>
			<?php if($app->apptypeid == 5) : ?>
			<a href="<?= site_url('events') ?>" class="btn" id="deletebtn"><span>Events</span></a>
			<a href="<?= site_url('venues') ?>" class="btn" id="deletebtn"><span>Venues</span></a>
			<?php endif; ?>
	        <!--<a href="<?= site_url('submit/app/'.$app->id) ?>" class="build btn"><img src="img/icons/email_go.png" width="16" height="16" alt="TapCrowd App"> <span> Submit</span></a>-->
	        <!--<a href="<?= site_url('apps/edit/'.$app->id) ?>" class="edit btn"><img src="img/icons/cog.png" width="16" height="16" alt="TapCrowd App"> <span> Settings</span></a>
	        <a href="<?= site_url('apps/manage/'.$app->id) ?>" class="edit btn"><img src="img/icons/box.png" width="16" height="16" alt="TapCrowd App"> <span> Content</span></a>-->
	        <?php endif ?>
	        <br clear="all"/>
		</div>
	</div>
	<br>
	<?php if($app->apptypeid == 6) : ?>
	<div class="modules">
		<?php if($artistsActive) : ?>
			<div class="module active">
				<?php if ($this->session->userdata('mijnevent')): ?><a href="<?= site_url('artists/app/'.$app->id) ?>"><?php endif ?>
					<a href="<?= site_url('artists/app/'.$app->id) ?>" class="editlink"><span>Artists</span></a>
				<?php if ($this->session->userdata('mijnevent')): ?></a><?php endif ?>
				<a href="<?= site_url('module/edit/8/app/'.$app->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="Module Settings" height="22px" /></a>
				<a href="<?= site_url('apps/module/deactivate/'.$app->id.'/18') ?>" class="activate delete">&nbsp;</a>
			</div>
		<?php else: ?>
			<div class="module inactive">
				<span>Artists</span>
				<a href="<?= site_url('apps/module/activate/'.$app->id.'/18') ?>" class="activate add white">&nbsp;</a>
			</div>
		<?php endif; ?>
		<?php if($socialActive): ?>
			<div class="module active">
				<?php if ($this->session->userdata('mijnevent')): ?><a href="<?= site_url('social/app/'.$app->id) ?>"><?php endif ?>
					<a href="<?= site_url('social/app/'.$app->id) ?>" class="editlink"><span>Social Media</span></a>
				<?php if ($this->session->userdata('mijnevent')): ?></a><?php endif ?>
				<a href="<?= site_url('module/edit/28/app/'.$app->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="Module Settings" height="22px" /></a>
				<a href="<?= site_url('apps/module/deactivate/'.$app->id.'/28') ?>" class="activate delete">&nbsp;</a>
			</div>
		<?php else:?>
			<div class="module inactive">
				<span>Social Media</span>
				<a href="<?= site_url('apps/module/activate/'.$app->id.'/28') ?>" class="activate add white">&nbsp;</a>
			</div>
		<?php endif; ?>
	</div>
	<?php endif; ?>
	<?php //content ?>
	<?php if($app->apptypeid == 5) : ?>
	<h2>Menu items</h2>
<!-- 	<a href="<?= site_url('citycontent/add') ?>" class="add btn primary"style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i>  Add Content
	</a> -->
	<a href="<?= site_url('event/add') ?>" class="add btn primary"style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i>  Add Event
	</a>
	<a href="<?= site_url('venue/add') ?>" class="add btn primary" style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i>  Add POI
	</a>

	<?php //CITY flavor ?>
		<div class="events">
			<div class="event_wrapper">
				<a href="<?= site_url('apps/info/'.$app->id) ?>" class="event editTagA">
					<?php foreach($launchers as $l) : ?>
						<?php if($l->moduletypeid == 21) : ?>
							<?php if($l->icon != 'l_info') : ?>
							<div class="eventlogo"><img src="<?=image_thumb($l->icon, 50, 50) ?>" width="50" height="50"></div>
							<?php else : ?>
							<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
							<?php endif; ?>
							<h1><span><?=$l->title?></span></h1>
						<?php endif; ?>
					<?php endforeach; ?>
				</a>
				<a href="<?= site_url('module/edit/21/app/'.$app->id)?>" style="border-bottom: none;"><img class="editTagImg" src="img/Settings.png" alt="Edit" height="22px" /></a>
			</div>
		</div>
		<?php $lierdrinkdone = false; ?>
		<?php foreach($apptags as $apptag) : ?>
		<?php //custom for lier ?>
		<?php if($app->id == 55 &&$apptag->tag == 'ToDrink' || $apptag->tag == 'ToEat') : ?>
			<?php if($lierdrinkdone == false) : ?>
				<div class="events">
					<div class="event_wrapper">
					<a href="<?= site_url('venues/index/'.$apptag->tag) ?>" class="event editTagA">
							<div class="eventlogo"><img src="<?= image_thumb($apptag->image, 50, 50) ?>" width="50" height="50"></div>
						<h1><span>ToDrink/Eat</span></h1>
					</a>
					<?php if ($apptag->active == 1): ?>
						<a href="<?= site_url('tag/active/' . $apptag->tag . '/' . $app->id . '/off') ?>" class="onoff deactivate" style="margin-top: 17px;"><span>Deactivate venue</span></a>
					<?php else: ?>
						<a href="<?= site_url('tag/active/' . $apptag->tag . '/' . $app->id . '/on') ?>" class="onoff activate" style="margin-top: 17px;"><span>Activate venue</span></a>
					<?php endif ?>
					<a href="<?= site_url('tag/edit/'.$apptag->tag.'/'.$apptag->appid)?>" style="border-bottom: none;"><img class="editTagImg" src="img/Settings.png" alt="Edit" height="22px" /></a>
					<br/>
					</div>
				</div>
				<?php $lierdrinkdone = true; ?>
			<?php endif; ?>
		<?php else: ?>
		<div class="events">
			<div class="event_wrapper">
				<?php if($apptag->eventid != 0) : ?>
					<?php if($apptag->image != '') : ?>
						<a href="<?= site_url('events/index/'.$apptag->tag) ?>" class="event editTagA">
								<div class="eventlogo"><img src="<?= image_thumb($apptag->image, 50, 50) ?>" width="50" height="50"></div>
							<h1><span><?=$apptag->tag?></span></h1>
						</a>
					<?php else : ?>
						<a href="<?= site_url('events/index/'.$apptag->tag) ?>" class="event editTagA">
								<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
							<h1><span><?=$apptag->tag?></span></h1>
						</a>
					<?php endif; ?>
				<?php elseif($apptag->venueid != 0) : ?>
					<?php if($apptag->image != '') : ?>
						<a href="<?= site_url('venues/index/'.$apptag->tag) ?>" class="event editTagA">
								<div class="eventlogo"><img src="<?= image_thumb($apptag->image, 50, 50) ?>" width="50" height="50"></div>
							<h1><span><?= $apptag->tag ?></span></h1>
						</a>
					<?php else : ?>
						<a href="<?= site_url('venues/index/'.$apptag->tag) ?>" class="event editTagA">
								<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
							<h1><span><?= $apptag->tag ?></span></h1>
						</a>
					<?php endif; ?>
				<?php elseif($apptag->citycontentid != 0) : ?>
					<?php if($apptag->image != '') : ?>
						<a href="<?= site_url('citycontent/app/'.$app->id.'/'.$apptag->tag) ?>" class="event editTagA">
								<div class="eventlogo"><img src="<?= image_thumb($apptag->image, 50, 50) ?>" width="50" height="50"></div>
							<h1><span><?= $apptag->tag ?></span></h1>
						</a>
					<?php else : ?>
						<a href="<?= site_url('citycontent/app/'.$app->id.'/'.$apptag->tag) ?>" class="event editTagA">
								<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
							<h1><span><?= $apptag->tag ?></span></h1>
						</a>
					<?php endif; ?>
				<?php endif; ?>
					<?php if ($apptag->active == 1): ?>
						<a href="<?= site_url('tag/active/' . $apptag->tag . '/' . $app->id . '/off') ?>" class="onoff deactivate" style="margin-top: 17px;"><span>Deactivate venue</span></a>
					<?php else: ?>
						<a href="<?= site_url('tag/active/' . $apptag->tag . '/' . $app->id . '/on') ?>" class="onoff activate" style="margin-top: 17px;"><span>Activate venue</span></a>
					<?php endif ?>
				<a href="<?= site_url('tag/edit/'.$apptag->tag.'/'.$apptag->appid)?>" style="border-bottom: none;"><img class="editTagImg" src="img/Settings.png" alt="Edit" height="22px" /></a>
				<br/>
			</div>
		</div>
		<?php endif; ?>
		<?php endforeach; ?>
		<?php if($events != false && count($events) > 0) : ?>
		<?php
		$countedEvents = 0;
		foreach ($events as $event) {
			if(count($event->tagNames) == 0) {
				$countedEvents++;
			}
		}
		?>
		<?php if($countedEvents > 0) : ?>
		<br/><br/>
		<h3><?=($events != FALSE && count($events) != 0) ? 'Events not added to a menu item' : 'Events' ?></h3>
		<br/>
		<div class="events">
			<div id="sortable_events">
				<?php if ($events != FALSE): ?>
					<?php foreach ($events as $event): ?>
					<?php if(count($event->tagNames) == 0): ?>
						<?php $countedEvents++;?>
						<div class="event_wrapper event_order" id="<?=$event->id ?>">
							<a href="<?= site_url('event/view/'.$event->id) ?>" class="event">
								<?php if ($event->eventlogo != ''): ?>
									<div class="eventlogo" style="background:transparent url('<?= image_thumb($event->eventlogo, 50, 50) ?>') no-repeat center center">&nbsp;</div>
								<?php else: ?>
									<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
								<?php endif ?>
								<h1><span><?= $event->name ?></span></h1>
							</a>
							<br/>
							<?php foreach($event->tagNames as $tag) : ?>
								<span class="tagInEventList"><?=$tag ?></span>
							<?php endforeach; ?>
								<select class="changeTagEvent">
									<option value="add">Add Event to menu item</option>
									<?php foreach($apptagsEvents as $tag) : ?>
										<option value="<?=$event->id?>"><?=$tag->tag?></option>
									<?php endforeach; ?>
									<option value="new<?=$event->id?>">New...</option>
								</select>
							<a href="<?= site_url('event/edit/'.$event->id) ?>" class="edit btn">
								<i class="icon-pencil"></i> Edit
							</a>
							<?php if ($event->active == 1): ?>
								<a href="<?= site_url('event/active/' . $event->id . '/off') ?>" class="onoff deactivate"><span>Deactivate event</span></a>
							<?php else: ?>
								<a href="<?= site_url('event/active/' . $event->id . '/on') ?>" class="onoff activate"><span>Activate event</span></a>
							<?php endif ?>
						</div>
					<?php endif; ?>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>
		<?php endif; ?>
		<?php endif; ?>
		<?php if($venues != false && count($venues) > 0) : ?>
		<?php
		$countedVenues = 0;
		foreach ($venues as $venue) {
			if(count($venue->tagNames) == 0) {
				$countedVenues++;
			}
		}
		?>
		<?php if($countedVenues > 0) : ?>
		<br /><br />
		<h3><?=($venues != FALSE && count($venues) != 0) ? "POI's not added to a menu item" : "POI's" ?></h3>
		<br/>
		<div class="events">
			<div id="sortable_venues">
				<?php if ($venues != FALSE): ?>
					<?php foreach ($venues as $venue): ?>
					<?php if(count($venue->tagNames) == 0): ?>
						<div class="event_wrapper venue_order" id="<?=$venue->id ?>">
							<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="event">
								<?php if ($venue->image1 != ''): ?>
									<div class="eventlogo" style="background:transparent url('<?= image_thumb($venue->image1, 50, 50) ?>') no-repeat center center">&nbsp;</div>
								<?php else: ?>
									<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
								<?php endif ?>
								<h1><span><?= $venue->name ?></span></h1>
								<?= $venue->address ?>
							</a>
							<br/>
							<?php foreach($venue->tagNames as $tag) : ?>
								<span class="tagInVenueList"><?=$tag ?></span>
							<?php endforeach; ?>
								<select class="changeTag">
									<option value="add">Add POI to menu item</option>
									<?php foreach($apptagsVenues as $tag) : ?>
										<option value="<?=$venue->id?>"><?=$tag->tag?></option>
									<?php endforeach; ?>
									<option value="new<?=$venue->id?>">New...</option>
								</select>
							<a href="<?= site_url('venue/edit/'.$venue->id) ?>" class="edit btn">
								<i class="icon-pencil"></i> Edit
							</a>
							<?php if ($venue->active == 1): ?>
								<a href="<?= site_url('venue/active/' . $venue->id . '/off') ?>" class="onoff deactivate"><span>Deactivate POI</span></a>
							<?php else: ?>
								<a href="<?= site_url('venue/active/' . $venue->id . '/on') ?>" class="onoff activate"><span>Activate POI</span></a>
							<?php endif ?>
						</div>
					<?php endif; ?>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>
		<?php endif; ?>
		<?php endif; ?>
		<?php if($citycontent != false && count($citycontent) > 0) : ?>
		<?php
		$countedCitycontent = 0;
		foreach ($citycontent as $cc) {
			if(count($cc->tagNames) == 0) {
				$countedCitycontent++;
			}
		}
		?>
		<?php if($countedCitycontent > 0) : ?>
	<!-- 		<br /><br />
			<h3><?=($citycontent != FALSE && count($citycontent) != 0) ? "Content not added to a menu item" : "Extra Content" ?></h3>
			<br/>
			<div class="events">
				<div id="sortable_content">

					<?php if ($citycontent != FALSE): ?>
						<?php foreach ($citycontent as $cc): ?>
						<?php if(count($cc->tagNames) == 0): ?>
							<div class="event_wrapper venue_order" id="<?=$cc->id ?>">
								<a href="<?= site_url('citycontent/edit/'.$cc->id) ?>" class="event">
									<?php if ($cc->image != ''): ?>
										<div class="eventlogo" style="background:transparent url('<?= image_thumb($cc->image, 50, 50) ?>') no-repeat center center">&nbsp;</div>
									<?php else: ?>
										<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
									<?php endif ?>
									<h1><span><?= $cc->title ?></span></h1>
								</a>
								<br/>
								<?php foreach($cc->tagNames as $tag) : ?>
									<span class="tagInVenueList"><?=$tag ?></span>
								<?php endforeach; ?>
									<select class="changeTagCitycontent">
										<option value="add">Add Content to menu item</option>
										<?php foreach($apptagsCitycontent as $tag) : ?>
											<option value="<?=$cc->id?>"><?=$tag->tag?></option>
										<?php endforeach; ?>
										<option value="new<?=$cc->id?>">New...</option>
									</select>
								<a href="<?= site_url('citycontent/edit/'.$cc->id) ?>" class="edit btn">
									<i class="icon-pencil"></i> Edit
								</a>
								<?php if (isset($venue) && $venue != false && $venue->active == 1): ?>
									<a href="<?= site_url('citycontent/active/' . $cc->id . '/off') ?>" class="onoff deactivate"><span>Deactivate Content</span></a>
								<?php else: ?>
									<a href="<?= site_url('citycontent/active/' . $cc->id . '/on') ?>" class="onoff activate"><span>Activate Content</span></a>
								<?php endif ?>
							</div>
						<?php endif; ?>
						<?php endforeach ?>
					<?php endif ?>
				</div>
			</div>  -->
		<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>



	<?php if($app->apptypeid == 2 || $app->apptypeid == 4 || $app->apptypeid == 7 || $app->apptypeid == 8 || $app->apptypeid == 11) : ?>
		<br /><br />
		<h3>Venues in "<?= $app->name ?>"</h3>
		<br>

		<div class="events">
			<div id="sortable_venues">
				<?php if(($app->apptypeid != 4 && $app->apptypeid != 2 && $app->apptypeid != 8) || empty($venues) || _isAdmin()) : ?>
				<a href="<?= site_url('venue/add') ?>" class="add btn primary">
					<i class="icon-plus-sign icon-white"></i>  Add Venue
				</a>
				<br><br>
				<?php endif; ?>
				<?php if ($venues != FALSE): ?>
					<?php foreach ($venues as $venue): ?>
						<div class="event_wrapper venue_order" id="<?=$venue->id ?>">
							<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="event">
								<?php if ($venue->image1 != ''): ?>
									<div class="eventlogo" style="background:transparent url('<?= image_thumb($venue->image1, 50, 50) ?>') no-repeat center center">&nbsp;</div>
								<?php else: ?>
									<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
								<?php endif ?>
								<h1><span><?= $venue->name ?></span></h1>
								<?= $venue->address ?>
							</a>
							<br />
							<?php foreach($venue->tags as $tag) : ?>
								<span class="tagInVenueList"><?=$tag->tag ?></span>
							<?php endforeach; ?>
							<a href="<?= site_url('venue/edit/'.$venue->id) ?>" class="edit btn">
								<i class="icon-pencil"></i> Edit
							</a>
							<?php if ($venue->active == 1): ?>
								<a href="<?= site_url('venue/active/' . $venue->id . '/off') ?>" class="onoff deactivate"><span>Deactivate venue</span></a>
							<?php else: ?>
								<a href="<?= site_url('venue/active/' . $venue->id . '/on') ?>" class="onoff activate"><span>Activate venue</span></a>
							<?php endif ?>
						</div>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if($app->apptypeid == 3 || $app->apptypeid == 6 || $app->apptypeid == 1) : ?>
			<h3>My events in mobile app "<?= $app->name ?>"</h3>
			<br>
			<div class="events">
				<div id="sortable_events">
					<a href="<?= site_url('event/add') ?>" class="add btn primary">
						<i class="icon-plus-sign icon-white"></i>  Add New Event
					</a>
					<br><br>
					<?php if ($events != FALSE): ?>
						<?php foreach ($events as $event): ?>
							<div class="event_wrapper event_order" id="<?=$event->id?>">
								<a href="<?= site_url('event/view/'.$event->id) ?>" class="event">
									<?php if ($event->eventlogo != ''): ?>
										<div class="eventlogo" style="background:transparent url('<?= image_thumb($event->eventlogo, 50, 50) ?>') no-repeat center center">&nbsp;</div>
									<?php else: ?>
										<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
									<?php endif ?>
									<h1><span><?= $event->name ?></span></h1>
									<?php if(date('d-m-Y', strtotime($event->datefrom)) == '01-01-1970') : ?>
										Permanent event
									<?php else: ?>
										<?= date('d-m-Y', strtotime($event->datefrom)) ?> - <?= date('d-m-Y', strtotime($event->dateto)) ?>
									<?php endif; ?>
								</a>
								<br/>
								<a href="<?= site_url('event/edit/'.$event->id) ?>" class="edit btn">
									<i class="icon-pencil"></i> Edit
								</a>
								<?php if ($event->active == 1): ?>
									<a href="<?= site_url('event/active/' . $event->id . '/off') ?>" class="onoff deactivate"><span>Deactivate event</span></a>
								<?php else: ?>
									<a href="<?= site_url('event/active/' . $event->id . '/on') ?>" class="onoff activate"><span>Activate event</span></a>
								<?php endif ?>
							</div>
						<?php endforeach ?>
					<?php endif ?>
				</div>
			</div>
	<?php endif; ?>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#deletebtn').click(function() {
				jConfirm('This will <b>remove</b> this app!<br />! This cannot be undone!', 'Remove App', function(r) {
					if(r == true) {
						window.location = '<?= site_url("apps/delete/".$app->id)?>';
						return true;
					} else {
						jAlert('App not removed!', 'Info');
						return false;
					}
				});
				return false;
			});
		});
	</script>
</div>

