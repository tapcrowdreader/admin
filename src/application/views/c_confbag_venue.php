<div class="header">
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<h1><?= $title ?></h1>
	<a href="<?= site_url('module/editByController/confbag/venue/'.$venue->id) ?>" class="edit btn" style="float:right;margin-bottom:5px;margin-top:-35px;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a><br style="margin-bottom:0px;clear:both;"/>
	<a href="<?= site_url('reports/personal/venue/0/'.$venue->id); ?>"> <button class="add btn primary"><?= __("Reports")?></button></a> <a href="<?= site_url('confbag/maillayout/'.$venue->id.'/venue') ?>"><button class="add btn primary"><?= __('Mail layout') ?></button></a> <a href="<?= site_url('confbag/mail/'.$venue->id.'/venue')?>"> <button class="add btn primary"><?= __("Send mail to users")?></button></a><br style="margin-bottom:15px;"/>

	<p>
		<?= __('The conference bag is now activated in your app. Users of your app can use the conference bag to keep track of their favorite catalog items.
You can add content to individual items, such as powerpoint slides. Use the link below to edit the content.') ?><br/>
		<a href="<?=site_url('catalog/venue/'.$venue->id);?>"><?= __('Edit catalog items') ?></a><br/>
	</p>
</div>
