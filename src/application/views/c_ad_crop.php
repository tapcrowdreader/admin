<link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
<script type="text/javascript">
    jQuery(function($){
      var jcrop_api, boundx, boundy;
      
        function setCoords(c) {
            jQuery('#x').val(c.x);
            jQuery('#y').val(c.y);
            jQuery('#w').val(c.w);
            jQuery('#h').val(c.h);
        }
            <?php if($adpicwidth != null && is_numeric($adpicwidth)) : ?>
                var width = <?php echo $adpicwidth; ?>;
            <?php else : ?>
                var width = 0;
                var ratio = 1;
            <?php endif; ?>
                
            <?php if($adpicheight != null && is_numeric($adpicheight)) : ?>
                var height = <?php echo $adpicheight; ?>;
                var ratio = width/height;
            <?php else : ?>
                var height = 0;
                var ratio = 1;
            <?php endif; ?>
      
        $('#target').Jcrop({
            aspectRatio: ratio,
            onChange: setCoords,
            onSelect: setCoords
        },function(){
        jcrop_api = this;
        jcrop_api.setOptions({
            minSize: [ width, height ],
            allowMove: true,
            allowResize: true,
            allowSelect: true
        });
        jcrop_api.focus();
        });
    });
</script>
<div>
	<h1><?=__('Edit picture')?></h1>
	<div class="frmsessions">
		<form name="newsform" action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <img src="<?= $this->config->item('publicupload').$ad->image ?>" id="target" />
			<div class="buttonbar">
                 <input type="hidden" name="x" id="x" value="" />
                 <input type="hidden" name="y" id="y" value="" />
                 <input type="hidden" name="w" id="w" value="" />
                 <input type="hidden" name="h" id="h" value="" />
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= ("javascript:history.go(-1);"); ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>