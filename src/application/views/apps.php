<?php if(!defined('BASEPATH')) exit('No direct script access');

class Apps extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('app_model');
	}

	//php 4 constructor
	function Apps() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('app_model');
	}

	function index() {
		$this->session->set_userdata('appid', '');
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');
		$apps = $this->app_model->appsByOrganizer(_currentUser()->organizerId);

		$cdata['content'] 		= $this->load->view('c_apps', array('apps' => $apps), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__('My mobile apps') => "apps");
		$this->load->view('master', $cdata);
	}

	function manage($id) {
		$app = $this->app_model->get($id);
		if($app == FALSE) {
			$this->session->set_flashdata('tc_error', __('App not found!'));
			redirect('apps');
		}
		if($app->organizerid != _currentUser()->organizerId) {
			$this->session->set_flashdata('tc_error', __('We would recommend to manage your own apps!'));
			redirect('apps');
		}

		$this->session->set_userdata('appid', $id);
		if($app->apptypeid == 2 || $app->apptypeid == 4) redirect('venues');
		redirect('events');
	}

	function add() {
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');

		$error = '';

		$this->load->library('form_validation');

        $languages = $this->language_model->getAllLanguages();

		// APPTYPE CHOSEN
		if($this->input->post('postback') == 'postback') {
			// lastlogin aanpassen van de user voor volgende login
			$flavorid = $this->input->post('flavorid');
			$flavors = $this->app_model->getAppTypes();
			if(!$flavors) $flavors = array();

			$cdata['content'] = $this->load->view('c_app_add', array('step' => 2, 'error' => $error, 'flavorid' => $flavorid, 'flavors' => $flavors, 'languages' => $languages), TRUE);
		} else {
			$cdata['content'] = $this->load->view('c_app_add', array('step' => 1, 'error' => $error, 'languages' => $languages), TRUE);
		}

		// ADD APP TO DATABASE
		if($this->input->post('postback2') == 'postback2') {
            $flavors = $this->app_model->getAppTypes();
			$this->form_validation->set_rules('flavor', 'flavor', 'trim|required');
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('urlname', 'urlname', 'trim');
			$this->form_validation->set_rules('cname', 'cname', 'trim');
            $this->form_validation->set_rules('defaultlang', 'defaultlang', 'trim|required');

			if($this->form_validation->run() == FALSE){
				$error = "Oops, some fields were not filled in correct.";
			} else {
				$appdata = array(
						'apptypeid'			=> set_value('flavor'),
						'organizerid'		=> _currentUser()->organizerId,
						'name'				=> set_value('name'),
						'token'				=> '123456',
						'certificate'		=> '',
						'subdomain' 		=> set_value('urlname'),
						'cname' 			=> set_value('cname'),
						'advancedFilter'	=> 0,
                        'defaultlanguage'   => $this->input->post('defaultlang')
					);


				$res = $this->general_model->insert('app', $appdata);

                if($this->input->post('language') != "") {
                    $sellanguages = $this->input->post('language');
                    foreach($sellanguages as $lang) {
                        $langdata = array(
                            'appid'     => $res,
                            'language'  => $lang
                        );
                        $this->general_model->insert('applanguage', $langdata);
                    }
                }


				if($res != FALSE){
					// update lastlogin
					$this->load->model('user_mdl');
					$this->user_mdl->edit_user(_currentUser()->organizerId, array('lastlogin' => date('Y-m-d H:i:s', time())));
					$this->session->set_flashdata('event_feedback', __('The app has successfully been created!'));
					redirect("apps/manage/$res");
				} else {
					$error = __("Error, something went wrong, please try again.");
				}
			}
			$cdata['content'] = $this->load->view('c_app_add', array('step' => 2, 'error' => $error, 'flavorid' => set_value('flavor'), 'flavors' => $flavors), TRUE);
		}

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__('My mobile apps') => "apps");
		$this->load->view('master', $cdata);
	}

	function edit($id) {
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');

		$this->load->library('form_validation');
		$error = "";
		$app = $this->app_model->get($id);

        $languages = $this->language_model->getAllLanguages();

		$flavors = $this->app_model->getAppTypes();
		if(!$flavors) $flavors = array();

		if($app->organizerid != _currentUser()->organizerId) redirect('apps');

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('urlname', 'urlname', 'trim');
			$this->form_validation->set_rules('cname', 'cname', 'trim');

			if($this->form_validation->run() == FALSE){
				$error = __("Oops, some fields were not filled in correct.");
			} else {
				$data = array(
						"name" 		=> set_value('name'),
						"subdomain" => set_value('urlname'),
						'cname' 	=> set_value('cname')
					);
                //remove all languages of app
                foreach($languages as $lang) {
                    $this->language_model->removeLanguageFromApp($id, $lang->key);
                }
                //add selected languages to app
                $sellanguages = $this->input->post('language');
                foreach($sellanguages as $lang) {
                    $langdata = array(
                        'appid'     =>  $id,
                        'language'  => $lang
                    );
                    $this->general_model->insert('applanguage', $langdata);
                }
				if($this->general_model->update('app', $id, $data)){
					$this->session->set_flashdata('event_feedback', __('The application has successfully been updated'));
					redirect('apps');
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_app_edit', array('flavors' => $flavors, 'error' => $error, 'languages' => $languages), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__("Edit application") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function delete($id) {
		/*if($this->general_model->remove('app', $id)){
			$this->session->set_flashdata('event_feedback', 'The app has successfully been deleted');
			redirect('apps/');
		} else {
			$this->session->set_flashdata('event_error', 'Oops, something went wrong. Please try again.');
			redirect('apps/');
		}*/

		if($this->general_model->update('app', $id, array('isdeleted' => 1))){
			$this->session->set_flashdata('event_feedback', __('The app has successfully been deleted'));
			redirect('apps/');
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('apps/');
		}

	}

    function view($id) {
        $error = "";
        $flavors = $this->app_model->getAppTypes();
		if(!$flavors) $flavors = array();

		$app = $this->app_model->get($id);

        $languages = $this->language_model->getAllLanguages();

		$cdata['content'] 		= $this->load->view('c_app', array('flavors' => $flavors, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__('View application') => $this->uri->uri_string());
		$this->load->view('master', $cdata);
    }

}