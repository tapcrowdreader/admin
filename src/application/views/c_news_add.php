<div>
	<h1><?= __('Add Item')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          
			<?php if(isset($languages) && !empty($languages)) : ?>
				<?php foreach($languages as $language) : ?>
				<p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
					<label for="title"><?= __('Title' . '(' . $language->name . ')'); ?>:</label>
					<input maxlength="50" type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?=set_value('title_'.$language->key)?>" />
				</p>
				<?php endforeach; ?>
				<?php foreach($languages as $language) : ?>
				<p <?= (form_error("text_".$language->key) != '') ? 'class="error"' : '' ?>>
					<label for="text"><?= __('Description') ?> <?= '(' . $language->name . ')'; ?>:</label>
					<textarea name="text_<?php echo $language->key; ?>" id="text"><?= set_value('text_'.$language->key) ?></textarea>
				</p>
				<?php endforeach; ?>
			<?php else : ?>
				<p <?= (form_error("title_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
					<label for="title"><?= __('Name:') ?></label>
					<input type="text" name="title_<?php echo $app->defaultlanguage; ?>" id="title" value="<?=set_value('title_'.$app->defaultlanguage)?>" />
				</p>
				<p>
					<label for="text"><?= __('Description:') ?></label>
					<textarea name="text_<?php echo $app->defaultlanguage; ?>" id="title" <?= (form_error("text_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>><?= set_value('text_'.$app->defaultlanguage) ?></textarea>
				</p>
			<?php endif; ?>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:') ?></label>
				<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
				<input type="file" name="image" id="image" value="" class="logoupload" /> <br clear="all" />
			</p><br />
			<div class="row">
				<label for="tags">Tags:</label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<p>
				<label><?= __('Push notification?') ?></label>
				<input class="checkboxClass pushclass" type="radio" name="push" value="yes" id="pushyes"/> <?= __('Yes') ?> <br />
				<input class="checkboxClass pushclass" type="radio" name="push" value="no" id="pushno" checked /> <?= __('No') ?> <br />
			</p>
<!-- 				<p class="pushextra">
				<label>Type:</label>
				<input class="checkboxClass" type="radio" name="type" value="text" checked /> Text <br />
				<input class="checkboxClass" type="radio" name="type" value="number" /> Number <br />
			</p> -->
			<p class="pushextra <?= (form_error("pushtext") != '') ? ' error' : '' ?>" style="display:none;" >
				<label><?= __('Notification text:') ?></label>
				<input maxlength="255" type="text" name="pushtext" value="<?= set_value('pushtext') ?>" id="pushtext" />
			</p>
			<?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
				<?php if(_checkMultilang($m, $lang->key, $app)): ?>
					<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
						<?= _getInputField($m, $app, $lang, $languages); ?>
					</p>
				<?php endif; ?>
			<?php endforeach; ?>
			<?php endforeach; ?>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add News') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
	$('.pushclass').mousedown(function() {
		if ($(this).attr('id') == 'pushyes') {
			$(".pushextra").css('display','block');
		} else if($(this).attr('id') == 'pushno') {
			$(".pushextra").css('display','none');
		}
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if($postedtags){foreach($postedtags as $val){ echo '"'.$val.'", '; }} ?>],
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = '<?=$tag->tag?>';
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
	});
</script>