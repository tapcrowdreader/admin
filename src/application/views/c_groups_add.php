<div>
	<h1><?=__('Add group')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>         
			<?php foreach($languages as $language) : ?>
	        <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
	            <label for="name"><?=__('Name '. '(' . $language->name . ')'); ?>:</label>
	            <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?= htmlspecialchars_decode(set_value('name_'.$language->key, '' ), ENT_NOQUOTES) ?>" />
	        </p> 
	        <?php endforeach; ?>
			<p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl"><?= __('Image:') ?></label>
				<span class="hintAppearance"><?= __('Image must be in jpg or png format, max width: %s px, max height: %s px',2000,2000) ?></span>
				<input type="file" name="imageurl" id="imageurl" value="" class="logoupload" />
			</p><br clear="all"/>
			<p>
				<label><?= __('Display type: ')?></label>
				<select name="displaytype" id="displaytype">
					<option value="list" ><?= __('List') ?></option>
					<option value="slider"><?= __('Slider') ?></option>
					<option value="thumbs"><?= __('Thumbs') ?></option>
				</select>
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label for="order"><?= __('Order:') ?></label>
				<input type="text" name="order" id="order" value="<?= set_value('order') ?>" />
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>