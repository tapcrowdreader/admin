<script type="text/javascript">

$(document).ready(function(){
	$("#mytags").tagit({
		initialTags: [<?php if(isset($tags) && $tags != null){foreach($tags as $val){ echo '"'.$val.'", '; }} ?>],
		availableTags: function(req, add){
			$.ajax({
				url: '<?= site_url("event/autocompletetags/"); ?>',
				dataType: 'json',
				type: 'POST',
				data: req,
				success: function(data){
					if(data.response =='true'){
						add(data.message);
					}
				}
			});
		}
	});
});
</script>
<div>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<h1><?=__('Edit Catalog Group')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<?php if(!isset($venues)): ?>
				<p <?= (form_error("events") != '') ? 'class="error"' : '' ?> style="display:none;">
					<label><?= __('Event') ?></label>
					<select name="events" id="events">
						<option value=""><?= __('Select ...') ?></option>
						<?php foreach ($events as $evt): ?>
							<option value="<?= $evt->id ?>" <?= set_select('events', $evt->id, ($cataloggroup->eventid == $evt->id ? TRUE : '')) ?>><?= $evt->name ?></option>
						<?php endforeach ?>
					</select>
				</p>
			<?php else: ?>
				<p <?= (form_error("venues") != '') ? 'class="error"' : '' ?> style="display:none;">
					<label><?= __('Venues') ?></label>
					<select name="venues" id="venues">
						<option value=""><?= __('Select ...') ?></option>
						<?php foreach ($venues as $vnu): ?>
							<option value="<?= $vnu->id ?>" <?= set_select('venues', $vnu->id, ($cataloggroup->venueid == $vnu->id ? TRUE : '')) ?>><?= $vnu->name ?></option>
						<?php endforeach ?>
					</select>
				</p>
			<?php endif; ?>
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Name:') ?></label>
				<input type="text" name="name" value="<?= htmlspecialchars_decode(set_value('name', $cataloggroup->name), ENT_NOQUOTES) ?>" id="name">
			</p>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:') ?></label>
				<span class="evtlogo" <?php if($cataloggroup->image != ''){ ?>style="background:transparent url('<?= image_thumb($cataloggroup->image, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="image" id="image" value="" class="logoupload" />
				<?php if($cataloggroup->image != '' && file_exists($this->config->item('imagespath') . $cataloggroup->image)){ ?>
				<a href="<?= site_url('cataloggroups/crop/'.$cataloggroup->id . '/venue/') ?>"><?= __('Edit picture') ?></a>
				<?php } ?>
				<br clear="all"/>
				<?php 
				$type = 'event';
				if(isset($venue)) {
					$type = 'venue';
				}
				?>
				<?php if($cataloggroup->image != '' && file_exists($this->config->item('imagespath') . $cataloggroup->image)){ ?><span><a href="<?= site_url('cataloggroups/removeimage/'.$cataloggroup->id.'/'.$type) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
			</p><br />
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:') ?></label>
				<input type="text" name="order" value="<?= set_value('order', $cataloggroup->order) ?>" id="order">
			</p>
			<?php if($app->id != 393) : ?>
			<div class="row">
				<p style="min-height:0px;">
				<label for="tags"><?= __('Tags:') ?></label>
				</p>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<?php else: ?>
				<p>
					<label for="tagdelbar"><?= __('Brand:') ?></label><br/>
					<select name="tagdelbar">
						<option value="Audi" <?= (in_array('Audi', $tags)) ? 'selected' : '' ?>><?=__('Audi')?></option>
						<option value="Volkswagen" <?= (in_array('Volkswagen', $tags)) ? 'selected' : '' ?>><?=__('Volkswagen')?></option>
						<option value="skoda" <?= (in_array('skoda', $tags)) ? 'selected' : '' ?>><?=__('Skoda')?></option>
						<option value="volkswagenbedrijfsvoertuigen" <?= (in_array('volkswagenbedrijfsvoertuigen', $tags)) ? 'selected' : '' ?>><?=__('Volkswagen Bedrijfsvoertuigen')?></option>
					</select>
				</p>
			<?php endif; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
                <?php if(isset($venue)) : ?>
                    <a href="<?= site_url('cataloggroups/venue/'.$venue->id) ?>" class="btn"><?= __('Cancel') ?></a>
                <?php else : ?>
                    <a href="<?= site_url('cataloggroups/event/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
                <?php endif; ?>
				
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>