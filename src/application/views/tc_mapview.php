<?php
/**
 * Tapcrowd map view
 * @author Tom Van de Putte
 */

?>
<div style="overflow:scroll;height:350px;position: relative;" class="span8 tc_markermap">
	<img src="<?= $map->url ?>" />
</div>