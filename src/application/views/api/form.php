<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<meta name="author" content="Matthijs Stichelbaut">
	<meta http-equiv="cache-control" content="no-cache">
	<meta name="copyright" content="&COPY; <?= date('Y') ?> float:left & MobileJuice">
	<meta http-equiv="pragma" content="no-cache">
	<meta name="robots" content="none">
	<meta name="googlebot" content="noarchive">

	<title>TapCrowd - API Browser</title>
	
	<script src="../../js/jquery-1.4.2.min.js" type="text/javascript" charset="utf-8"></script>
	
	<link rel="stylesheet" href="../../css/reset.css" type="text/css" charset="utf-8">
	<style type="text/css" media="screen">
		<!--
		
		* { margin:0; padding:0; }
		body { margin:0; color:#222222; background:#F6F5F5; font-family:"Lucida Grande","Lucida Sans",Arial,Verdana,Sans-Serif; font-size:12px; }
		
		h1 { font-size:15px; font-weight:bold; border-bottom:1px dotted #CCC; margin-bottom:10px; padding:0 0 6px 0; }
		h2 { font-size:13px; display:block; border-bottom:1px dotted #CCC; margin-bottom:10px; padding:0 0 6px 0; }
		
		a { color:#000; border-bottom:1px dotted #666; text-decoration:none; }
		a:hover { }
		
		a#addrow { background:transparent url('../../img/icons/textfield_add.png') no-repeat 7px 4px; padding:5px 0px 5px 28px; border:none; }
		a.delete { background:transparent url('../../img/icons/textfield_delete.png') no-repeat 7px 4px; padding:5px 0px 5px 28px; border:none; }
		
		div#wrapper { background:#FFF; margin:15px auto; width:580px; }
		div#wrapper #header { padding:15px 40px; background:#222; }
		div#wrapper #header span { color:#FFF; font-size:23px; display:inline-block; float:right; margin-top:12px; }
		div#wrapper #content { padding:15px; }
		
		div#documentation {  }
		div#documentation h1 { }
		
		form#frmVars { }
		form#frmVars p { float:left; height:25px; padding:10px 0; margin-right:10px; }
		form#frmVars p.version { width:100px; }
		form#frmVars p.version select { width:50px; }
		form#frmVars p.function { width:430px; }
		form#frmVars p.function select { width:375px; }
		form#frmVars p label { color:#666; }
		form#frmVars p input { }
		form#frmVars p select { background:none; border:1px solid #CCC; outline:none; }
		form#frmVars p.buttonbar { width:100%; }
		form#frmVars p.buttonbar input[type="submit"] { float:right; background:#EDEDED url('../../img/icons/arrow_right.png') no-repeat 7px 4px; padding:5px 10px 5px 30px; border:none; margin-left:5px; }
		form#frmVars p.buttonbar input[type="reset"] { float:right; background:#EDEDED url('../../img/icons/arrow_refresh_small.png') no-repeat 7px 4px; padding:5px 10px 5px 30px; border:none; margin-left:5px; }
		
		form#frmVars table#tblvars { width:550px; }
		form#frmVars table#tblvars tr { }
		form#frmVars table#tblvars tr th { font-weight:bold; border-bottom:1px solid #666; padding:5px 0; }
		form#frmVars table#tblvars tr td { padding:5px 0; }
		form#frmVars table#tblvars tr td input { border:none; border-bottom:1px dotted #CCC; background:#F5F5F5; padding:6px 10px; width:90%; outline:none; }
		
		#feedback { padding:10px; border:1px dotted #CCC; display:none; }
		#feedback span { display:block; border-bottom:1px dotted #CCC;  padding:0 0 6px 0; }
		#feedback textarea { border:none; width:99%; height:auto; padding:0 10px; outline:none; margin-left:-10px; resize:none; }
		
		a#selectall { float:right; background:#EDEDED url('../../img/icons/textfield_rename.png') no-repeat 7px 4px; padding:5px 10px 5px 30px; border:none; margin:-35px 0 5px 5px; }
		
		.footer { text-align:center; color:#BBB; font-size:9px; }
		.footer a { color:#BBB; border:none; }
		
		-->
	</style>
	
</head>

<body>
	<div id="wrapper">
		<div id="header">
			<img src="../../img/logo.png" width="190" height="58" alt="TapCrowd Logo">
			<span>API Browser</span>
		</div>
		<div id="content">
			<form action="<?= site_url('api/browser') ?>" method="POST" accept-charset="utf-8" id="frmVars">
				<p class="version">
					<label for="sel_version">Version</label>
					<select name="sel_version" id="sel_version">
						<option value=""></option>
						<?php foreach ($versions as $version => $path): ?>
							<option value="<?= $path ?>"><?= $version ?></option>
						<?php endforeach ?>
					</select>
				</p>
				<p class="function">
					<label for="sel_function">Function</label>
					<select name="sel_function" id="sel_function">
						<option value=""></option>
					</select>
				</p>
				<br clear="all" />
				<div id="documentation"></div>
				<table border="0" cellspacing="0" cellpadding="5" id="tblvars">
					<tr>
						<th width="30"><a href="#" id="addrow">&nbsp;</a></th>
						<th width="260">Key</th>
						<th width="260">Value</th>
					</tr>
					<tr class="defaultrow">
						<td>&nbsp;</td>
						<td><input type="text" name="key[]" id="key" value=""></td>
						<td><input type="text" name="value[]" id="value" value=""></td>
					</tr>
				</table>
				<p class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="submit" id="send" value="Run">
					<input type="reset" id="reset" value="Reset">
				</p>
				<br clear="all" />
				<div id="feedback">
					<h1>Result</h1>
					<div id="result"></div>
				</div>
			</form>
		</div>
	</div>
	<div class="footer">
		Powered by <a href="http://www.mobilejuice.be">MobileJuice</a>
	</div>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			
			$('#send').click(function(e){
				e.preventDefault();
				$("#feedback").slideUp(500,function(){
					if($("#sel_version").val() != ''){
						if($("#sel_function").val() != ''){
							// post
							$.post(
								"<?= site_url('api/browser/') ?>", 
								$("#frmVars").serialize(),
								function(data){
									
									var datas = data.split('|||||');
									
									$("#result").html('<h2>Function : ' + $("#sel_function").val() + '</h2>\n<span>' + datas[1] + '</span><a href="#" id="selectall">Select all</a><br />\n<textarea name="txt_result" id="txt_result">' + formatJson(datas[0]) + '</textarea>');
									//$('#txt_result').css({'height': $('#txt_result').val().length + 'px'});
									
									$("#feedback").slideDown('fast', function(){
										var $textarea = $("#txt_result");
										$textarea.css("height", ($textarea.attr("scrollHeight") + 20));
									});
									$("#selectall").click(function(e){
										e.preventDefault();
										$("#txt_result").focus().select();
									});
								}
							);
						} else {
							$("#result").html("Choose function");
							$("#feedback").slideDown();
						}
					} else {
						$("#result").html("Choose version");
						$("#feedback").slideDown();
					}
				});
			});
			
			$('#reset').click(function(e){
				resetForm();
			});
			
			$('#addrow').click(function(e){
				e.preventDefault();
				$('#tblvars').append('<tr class="xtra"><td><a href="#" class="delete">&nbsp;</a></td><td><input type="text" name="key[]" id="key" value=""></td><td><input type="text" name="value[]" id="value" value=""></td></tr>');
				
				$(".delete").click(function(e){
					e.preventDefault();
					$(this).parent().parent().remove();
				});
			});
			
			$('#sel_version').change(function(e){
				resetForm();
				$('#sel_function').find('option').remove().end().append('<option value=""></option>');
				
				$.post(
					"<?= site_url('api/browser/getFunctions/') ?>", 
					'path=' + $(this).val(),
					function(data){
						for (var i = 0; i < data.length; i++){
							$('#sel_function').append('<option value="'+data[i]+'">'+data[i]+'</option>');
						}
					},
					"json"
				);
			});
			
			$('#sel_function').change(function(e){
				resetForm();
				var version = $('#sel_version').val().split('/');
				$.post(
					"<?= site_url() ?>api/" + version[version.length-1] + "/" + $('#sel_function').val() + "/getParams/", 
					'path=' + $(this).val(),
					function(data){
						for (var i = 0; i < data.length; i++){
							if(i == 0){
								$('#documentation').html('<h1>' + $('#sel_function').val() + '</h1>' + data[i] + '<br clear="all"/><br />').slideDown();
							} else if(i == 1) {
								$('.defaultrow').find('#key').val(data[i]);
							} else {
								$('#tblvars').append('<tr class="xtra"><td><a href="#" class="delete">&nbsp;</a></td><td><input type="text" name="key[]" id="key" value="' + data[i] + '"></td><td><input type="text" name="value[]" id="value" value=""></td></tr>');
								$(".delete").click(function(e){
									e.preventDefault();
									$(this).parent().parent().remove();
								});
							}
						}
					},
					"json"
				);
			});
			
			function resetForm() {
				$('.xtra').each(function(e){
					$(this).remove();
				});
				$("#feedback").slideUp(500, function(){
					$('#result').html('');
				});
				$('.defaultrow').find('input').val('');
				$('#documentation').slideUp().html();
			}
			
		});
		
		// formatJson() :: formats and indents JSON string
		function formatJson(val) {
			var retval = '';
			var str = val;
			var pos = 0;
			var strLen = str.length;
			var indentStr = '&nbsp;&nbsp;&nbsp;&nbsp;';
			var newLine = '\n';
			var char = '';
			
			for (var i=0; i<strLen; i++) {
				char = str.substring(i,i+1);
				
				if (char == '}' || char == ']') {
					retval = retval + newLine;
					pos = pos - 1;
					
					for (var j=0; j<pos; j++) {
						retval = retval + indentStr;
					}
				}
				
				retval = retval + char;	
				
				if (char == '{' || char == '[' || char == ',') {
					retval = retval + newLine;
					
					if (char == '{' || char == '[') {
						pos = pos + 1;
					}
					
					for (var k=0; k<pos; k++) {
						retval = retval + indentStr;
					}
				}
			}
			
			return retval;
		}
	</script>

</body>
</html>
