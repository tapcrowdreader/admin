<?php
if(!empty($channel->information)) { ?>
	<h1><?=__('Information')?></h1>
	<?=$channel->information?>
	<br clear="all" /><br /><?php
} ?>

<h1><?=__('Contact us')?></h1>
<?php if(validation_errors()) : ?>
	<div class="error">
		<?php echo validation_errors(); ?>
	</div>
<?php endif; ?>
<div class="frmsessions">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
		<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
			<label for="name"><?= __('Name:') ?></label>
			<input type="text" name="name" id="name" value="<?= set_value('name') ?>" />
		</p>
		<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
			<label for="email"><?=__('Email')?>:</label>
			<input type="text" name="email" id="email" value="<?= set_value('email') ?>" />
		</p>
		<p <?= (form_error("message") != '') ? 'class="error"' : '' ?>>
			<label for="message"><?= __('Message') ?>:</label>
			<textarea name="message"><?=set_value('message')?></textarea>
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" class="btn primary"><?= __('Send') ?></button>
			<br clear="all" />
		</div>
		<br clear="all" />
	</form>
</div>
