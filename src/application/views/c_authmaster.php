<!DOCTYPE html>
<html>
<head>
	<title><?=$title . ' | ' . $channel->name?></title>
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
	<script src="/js/jquery-1.7.2.js"></script>
	<style>
body {
	background-color:#F5F5F5;
}
#authbox {
	background-color:#FFFFFF;
	border-radius:8px;
	box-shadow: 0px 1px 3px 0px rgb(187, 187, 187);
}
h1 {
	margin-top:0;
	padding:0.6em;
	background-color:#414141;
	border-radius:8px 8px 0px 0px;
	font-variant:small-caps;
	color:white;
	font-size:20px;
}
form {
	background-color:#FFFFFF;
	padding:10px;
}
.center {
	text-align:center;
}

	</style>
</head>
<body>
<div class="" style="margin-top:10%;">

	<div id="authbox" style="display:block; width:500px; margin-left:auto; margin-right:auto;">
		<h1 class="center"><?=$title?> <img src="<?=$channel->logo?>" height="36px" width="" /></h1><?php

		/**
		 * Display System Notifications
		 */
		echo Notifications::getInstance(); ?>

		<span class="help-block alert alert-info msg-loggedout" style="display:none">
			<strong><?=__('Note:')?></strong> <?=__('Logged out, Please log in.')?>
		</span>
		<span class="help-block alert alert-error msg-invalidcredentials" style="display:none">
			<strong><?=__('Error:')?></strong> <?=__('Invalid Login/Password.')?>
		</span>
		<span class="help-block alert alert-error msg-passmismatch" style="display:none">
			<strong><?=__('Error:')?></strong> <?=__('Passwords do not match.')?>
		</span>
		<span class="help-block alert alert-success msg-pleasecheckemail" style="display:none">
			<strong><?=__('Note:')?></strong> <?=__('Please check your email, a confirmation email has been send.')?>
		</span>
		<span class="help-block alert alert-error msg-invalidemail" style="display:none">
			<strong><?=__('Error:')?></strong> <?=__('Invalid/Unknown email address.')?>
		</span>
		<span class="help-block alert alert-error msg-invalidtoken" style="display:none">
			<strong><?=__('Error:')?></strong> <?=__('Invalid/Expired session token.')?>
		</span>
		<span class="help-block alert alert-error msg-sessionexpired" style="display:none">
			<strong><?=__('Error:')?></strong> <?=__('Invalid/Expired session.')?>
		</span>
		<span class="help-block alert alert-error msg-loginexists" style="display:none">
			<strong><?=__('Error:')?></strong> <?=__('Login already used.')?>
		</span>

		<?=$content?>
	</div>
	<div id="footer" style="display:block; width:500px; margin-left:auto; margin-right:auto; text-align:center;">
		<p><small><?= '© ' . date('Y') . ' ' . $channel->name ?></small></p>
	<div>
</div>
<script>
$(document).ready(function()
{

	/**
	 * @see http://stackoverflow.com/a/901144
	 */
	window.getParameterByName = function(name)
	{
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.search);
		if(results == null)
			return "";
		else
			return decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	/**
	 * Show/Hide .help-block.alert-error 's
	 */
	$('span.help-block').hide();
	var errorMsg = getParameterByName('errormsg');
	if(errorMsg) $('span.msg-'+errorMsg).show();

});
</script>

<script type="text/javascript">
document.write(unescape("%3Cscript src='" + document.location.protocol +
  "//munchkin.marketo.net/munchkin.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script>Munchkin.init('771-HZH-025');</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10861070-2']);
  _gaq.push(['_setDomainName', 'tapcrowd.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
