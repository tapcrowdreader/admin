<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<h3><?= __('Edit Category') ?></h3>
		<p <?= (form_error("name") != '') ? 'class="error"' : '' ?> >
			<label for="name"><?= __('Name:') ?></label>
			<input type="text" name="name" id="name" value="<?= htmlspecialchars_decode(set_value('name', $category->name), ENT_NOQUOTES) ?>"/>
		</p>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<a href="<?= site_url('categories/event/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
		<button type="submit" class="btn primary"><?= __('Save') ?></button>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>