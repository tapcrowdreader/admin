<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="minimum-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no" name="viewport" />
		
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
		
		<title><?= isset($titel) ? $titel : "" ?></title>
		<base href="<?= base_url() ?>" target="" />
		
		<link rel="stylesheet" href="css/reset.css" type="text/css" />
		<link rel="stylesheet" href="css/social.css" type="text/css" />
	</head>
	<body>
		<div id="wrapper">
			<p><?= isset($error) ? $error : '404 Page not found' ?></p>
		</div>
	</body>
</html>
