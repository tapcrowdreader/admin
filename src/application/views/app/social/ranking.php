<div class="new_score">
	<h1>Leaderboard</h1>
	<table border="0" cellspacing="0" cellpadding="0" class="leaderboard">
		<?php foreach ($leaderboard as $topscore): ?>
			<tr <?= ($topscore['user'] == 1 ? "class='user'" : "") ?>>
				<td><span><?= $topscore['rank'] ?>.</span> <?= $topscore['name'] ?></td>
				<td class="score"><?= $topscore['score'] ?></td>
			</tr>
		<?php endforeach ?>
	</table>
	<p class="note">Note: Apple is not a sponsor nor is involved in any way</p>
</div>