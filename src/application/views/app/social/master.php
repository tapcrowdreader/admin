<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="minimum-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no" name="viewport" />
		
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
		
		<title><?= isset($titel) ? $titel : "" ?></title>
		<base href="<?= base_url() ?>" target="" />
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/reset.css" type="text/css" />
		<link rel="stylesheet" href="css/social.css" type="text/css" />
		
		<?php if ($_SERVER['HTTP_HOST'] == "admin.tapcrowd.com"): ?>
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-23619195-1']);
		  _gaq.push(['_setDomainName', '.tapcrowd.com']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
		<?php endif ?>
</head>
<body>
	<div id="wrapper">
		<?= isset($content) ? $content : '<p>404 Page not found</p>' ?>
	</div>
</body>
</html>
