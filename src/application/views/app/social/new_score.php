<div class="new_score">
	<h1>Congratulations</h1>
	<p class="description">
		You just scored some extra points to your Viking-account.
	</p>
	<table border="0" cellspacing="0" cellpadding="0" class="scoreboard">
		<tr>
			<th>Nice one! You earned:</th>
			<th class="score" style="width:10%;"><span class="highlight">+<?= $action_total ?></span></th>
		</tr>
		<?php if (is_array($action_data)): ?>
			<?php $first = TRUE; ?>
			<?php foreach ($action_data as $action): ?>
				<tr <?= ($first ? "class='first'" : "") ?>>
					<td><?= $action->description ?></td>
					<td class="score">+<?= $action->score ?></td>
				</tr>
				<?php $first = FALSE; ?>
			<?php endforeach ?>
		<?php else: ?>
		<tr class="first">
			<td><?= $action_data->description ?></td>
			<td class="score">+<?= $action_data->score ?></td>
		</tr>
		<?php endif ?>
		<?/*
		<tr class="btop">
			<td>Totaal in deze app</td>
			<td class="score"><?= $userscore_app ?></td>
		</tr>
		*/?>
		<tr>
			<td>Totaal</td>
			<td class="score"><?= $userscore_tot ?></td>
		</tr>
	</table>
	<h2>Leaderboard</h2>
	<table border="0" cellspacing="0" cellpadding="0" class="leaderboard">
		<?php $i = 1; ?>
		<?php foreach ($top3 as $topscore): ?>
			<tr>
				<td><span><?= $i ?>.</span> <?= $topscore['name'] ?></td>
				<td class="score"><?= $topscore['score'] ?></td>
			</tr>
			<?php $i++; ?>
		<?php endforeach ?>
	</table>
	<p class="goto">Ga naar het <a href="http://www.facebook.com/pages/Festiviteiten/220485121316059?sk=app_219927378041648">Mobile Viking Punten scherm</a> en ontdek hoe je nog meer punten kan verzamelen.</p>
	<p class="note">Note: Apple is not a sponsor nor is involved in any way</p>
</div>