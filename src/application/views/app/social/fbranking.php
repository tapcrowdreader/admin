<div id="fbframe">
	<?php if ($fbdata['page']['liked'] == 1): ?>
	
	<?php else: ?>
		<img src="img/page-like.png" width="494" height="227" alt="First like our page"><br />
	<?php endif; ?>
	
	<div class="intro">
		Amuseer je, deel het met je vrienden en verdien Mobile Viking punten waarmee je mooie prijzen kan winnen! 
		Elke maand verdienen de  eerste 20 een <strong>Mobile Viking tas</strong>, de volgende 100 een 
		<strong>Mobile Viking t-shirt</strong> en de volgende 100 een <strong>Mobile Viking gratis oplaadbeurt</strong>.
		<br />Zo kan je punten verdienen:
		<ul>
			<li>Deel je eerste foto: 3 punten</li>
			<li>Deel meer foto's: 1 punt</li>
			<li>Deel je eerste commentaar op nieuws: 3 punten</li>
			<li>Deel meer commentaar: 1 punt</li>
			<li>Deel je persoonlijke line-up: 5 punten</li>
		</ul>
	</div>
	
	<?php if ($latest_actions != FALSE): ?>
	<div class="score personal">
		<h1>Personal score</h1>
		<table border="0" cellspacing="0" cellpadding="0" class="personal">
			<?php if (is_array($latest_actions)): ?>
				<tr>
					<th>Latest 3 actions</th>
					<th>&nbsp;</th>
				</tr>
				<?php foreach ($latest_actions as $action): ?>
				<tr>
					<td><?= $action->description ?></td>
					<td class="score">+<?= $action->score ?></td>
				</tr>
				<?php endforeach ?>
			<?php endif ?>
			<tr>
				<td>&nbsp;</td>
				<td class="score">Je hebt al <span class="highlight"><?= $userscore_tot ?></span> &nbsp;Mobile Viking punten!</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="score">Daarmee sta je op plaats <span class="highlight"><?= $personal_ranking ?></span> &nbsp;in de rangschikking</td>
			</tr>
		</table>
	</div>
	<?php endif ?>
	
	<div class="score new_score">
		<h1>Leaderboard</h1>
		<table border="0" cellspacing="0" cellpadding="0" class="leaderboard">
			<?php $tellerke = 0; ?>
			<?php foreach ($leaderboard as $topscore): ?>
				<?php
				switch ($tellerke) {
					case '0':
						?><tr>
							<td colspan="2" class="title">Winnen een Mobile Viking tas:</td>
						</tr><?php
						break;
					case '20':
						?><tr>
							<td colspan="2" class="title">Winnen een Mobile Viking t-shirt:</td>
						</tr><?php
						break;
					case '120':
						?><tr>
							<td colspan="2" class="title">Winnen een Mobile Viking oplaadbeurt:</td>
						</tr><?php
						break;
				}
				?>
				<tr <?= ($topscore['user'] == 1 ? "class='user'" : "") ?>>
					<td><span><?= $topscore['rank'] ?>.</span> <?= $topscore['name'] ?></td>
					<td class="score"><?= $topscore['score'] ?></td>
				</tr>
				<?php $tellerke++; ?>
			<?php endforeach ?>
		</table>
	</div>
	
	<div class="outro">
		Dit is een voorlopige ranschikking. De definitieve wordt bepaald op het einde van deze maand. 
		De winnaars worden persoonlijk gecontacteerd via Facebook.
	</div>
</div>