<div>
	<h1><?=__('Edit Favourite')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
				<label for="email"><?=__('Email address:')?></label>
				<input type="text" name="email" value="<?= set_value('email', $favourite->useremail) ?>" id="email">
			</p>
			<p <?= (form_error("sel_session") != '') ? 'class="error"' : '' ?>>
				<label for="sel_session"><?= __('Session:') ?></label>
				<select name="sel_session" id="sel_session">
					<?php if (count($sessions) > 0): ?>
						<?php foreach ($sessions as $sgroup): ?>
							<optgroup label="<?= $sgroup->name ?>">
								<?php if (count($sgroup->sessions) > 0): ?>
									<?php foreach ($sgroup->sessions as $session): ?>
										<option value="<?= $session->id ?>" <?= set_select('sel_session', $session->id, ($session->id == $favourite->sessionid ? TRUE : '')) ?>><?= $session->name ?></option>
									<?php endforeach ?>
								<?php endif ?>
							</optgroup>
						<?php endforeach ?>
					<?php else: ?>
						<option value="0"><?= __('No sessions ...') ?></option>
					<?php endif ?>
				</select>
			</p>
			<p <?= (form_error("sel_exhibitor") != '') ? 'class="error"' : '' ?>>
				<label for="sel_exhibitor"><?= __('Exhibitor:') ?></label>
				<select name="sel_exhibitor" id="sel_exhibitor">
					<?php if (count($exhibitors) > 0): ?>
						<?php foreach ($exhibitors as $exhibitor): ?>
							<option value="<?= $exhibitor->id ?>" <?= set_select('sel_exhibitor', $exhibitor->id, ($exhibitor->id == $favourite->exhibitorid ? TRUE : '')) ?>><?= $exhibitor->name ?></option>
						<?php endforeach ?>
					<?php else: ?>
						<option value="0"><?= __('No sessions ...') ?></option>
					<?php endif ?>
				</select>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('favourites/event/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>