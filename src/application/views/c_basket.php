<?php if($error != ''): ?>
<div class="error"><?= $error ?></div>
<?php endif ?>
<h1><?= __('Basket Info') ?></h1>
<a href="<?= site_url('module/editByController/basket/venue/'.$venue->id) ?>" class="edit btn" style="float:right;margin-bottom:5px;margin-top:-35px;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a><br style="clear:both;"/>
<p  style="font-weight: bolder;">
	<?=__("This module adds a basket to your restaurant so that the customers can add drinks or food
	while in the restaurant without the need for the waiter to come to the table.
	When the order of the customer is ready he will then receive a notification saying that the order is ready.")?>
</p>
<br />
<p>
	<?=__("<strong>Tip:</strong> To introduce prices on items you should go back to the Menu Module as a new field, \"Price\", will be available to be filled in for each item on the menu.")?>
</p>


<a href="reports/view/basket/venue/" class="btn primary" style="float:right;"> <span><?= __("Reports")?></span></a><br clear="all" />
<?php if(_isAdmin()) : ?>
<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
<p>
<input type="checkbox" name="orderattable" id="orderattable" class="checkboxClassmodules" <?= $basket && $basket->orderattable == 1 ? 'checked="checked"' : '' ?> /> <?= __("Order at the table") ?><br />
<div id="orderattablecheckboxes" <?= (!$basket || $basket->orderattable == 0) ? 'style="display:none;"' : ''?>>
	<?php foreach($modules as $module) :?>
	<input type="checkbox" name="module_<?=$module->name?>" id="<?=$module->name?>" class="checkboxClassmodules" <?= $module->status == 1 ? 'checked="checked"' : '' ?> />
		<a id="a_<?=$module->name?>" href="<?= site_url('basket/'.$module->name.'/'.$venue->id)?>" <?= $module->status == 0 ? 'style="display:none;"' : ''?>>
		<?= __($module->title) ?>
		</a>
		<span id="span_<?=$module->name?>" <?= $module->status == 1 ? 'style="display:none;"' : ''?>><?= __($module->title) ?></span><br clear="all" />
	<?php endforeach ?>
</div>
<input type="checkbox" name="ordertakeaway" id="ordertakeaway" class="checkboxClassmodules" <?= $basket && $basket->ordertakeaway == 1 ? 'checked="checked"' : '' ?> /> <?= __("Order take away") ?><br />
<input type="checkbox" name="orderreservation" id="orderreservation" class="checkboxClassmodules" <?= $basket && $basket->orderreservation == 1 ? 'checked="checked"' : '' ?> /> <?= __("Order drinks/food in advance with table reservation") ?><br />
<input type="checkbox" name="popupperorder" id="popupperorder" class="checkboxClassmodules" <?= $basket && $basket->popupperorder == 1 ? 'checked="checked"' : '' ?> /> <?= __("Show comment field for remarks") ?><br />
<input type="checkbox" name="popupperitem" id="popupperitem" class="checkboxClassmodules" <?= $basket && $basket->popupperitem == 1 ? 'checked="checked"' : '' ?> /> <?= __("Show popup for remarks when adding an item") ?><br />
</p>
<p style="margin-top:20px;">
	<label><?= __('Send basket orders to POS system') ?></label>
	<select name="api" id="api">
		<option value=""><?= __("Don't Send") ?></option>
		<option value="alfazet" style="padding-right: 20px;" <?= $basket && $basket->send_order_to_api == 'alfazet' ? 'selected="selected"' : '' ?>><?=__('Alfazet')?></option>
	</select>  <br clear="all" />
	<label style="margin-top:5px;margin-bottom:5px;"><?= __('External venue id in POS system') ?>: </label>
	<input type="text" name="externalvenueid" value="<?= $basket ? $basket->externalvenueid : '' ?>" /><br clear="all" />
	<input type="hidden" name="postback" value="postback" id="postback">
	<input type="submit" value="Save" name="submit" class="btn primary"/>
</p>
</form>
<?php endif; ?>

<script type="text/javascript">
	//checkboxes
	var url = "<?= site_url('basket/activate/'.$venue->id)?>";
	$('.checkboxmodules').mousedown(function() {
			var id = $(this).attr('id');
	        if (!$(this).is(':checked')) {
			$.ajax({
			  url: url+"/"+id+"/0",
			}).done(function() {
				$("#a_"+id).css('display', 'inline');
			  	$("#span_"+id).css('display', 'none');
			});
        } else {
			$.ajax({
			  url: url+"/"+id+"/1",
			}).done(function() {
		  		$("#a_"+id).css('display', 'none');
			  	$("#span_"+id).css('display', 'inline');
			});
        }
    });

    $('#orderattable').mousedown(function() {
    	if (!$(this).is(':checked')) {
    		$("#orderattablecheckboxes").css('display', 'block');
    	} else {
    		$("#orderattablecheckboxes").css('display', 'none');
    	}
    });
</script>