<script type="text/javascript">

$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if(isset($postedtags) && $postedtags != false && !empty($postedtags)) { 
				foreach($postedtags as $val){ 
					echo '"'.$val.'", ';
				} 
			} elseif (isset($tags) && $tags != false) {
				foreach($tags as $val){ 
					echo "\"".$val->tag."\"," ;
				}
			} ?>]
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = "<?=$tag->tag?>";
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});

	// $("#jqueryselectcats").multiselect({
	//    click: function(event, ui){
	//    	if(ui.checked) {
	//    		$("#catshidden").val($("#catshidden").val()+'/'+ui.value);
	//    	} else {
	//    		var val = $("#catshidden").val();
	//    		var valarray = val.split('/');
	//    		var newstring = '';
	//    		for(var index in valarray) {
	//    			var obj = valarray[index];
	//    			if(obj != '' && obj != ui.value) {
	//    				newstring = newstring + '/' + obj;
	//    			}
	//    		}
	//    		$("#catshidden").val(newstring);
	//    	}
	//    },
	// 	selectedList: 4
	// });
});
</script>
<div>
<?php if($this->uri->segment(5) == 'services') : ?>
	<h1><?= __('Edit Service') ?></h1>
<?php elseif($this->uri->segment(5) == 'careers') : ?>
	<h1><?= __('Edit Career') ?></h1>
<?php elseif($this->uri->segment(5) == 'projects') : ?>
	<h1><?= __('Edit Project') ?></h1>
<?php else: ?>
	<h1><?= __('Edit item') ?></h1>
<?php endif; ?>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error']): ?>
            <div class="error">
            	<p>
					<?php
                    if( $_GET['error'] == 'image' ) 
						echo __('Something went wrong with the image upload.').' <br/> '.__('Maybe the image size exceeds the maximum width or height');
					elseif( $_GET['error'] == 'pdf' )
						echo __('Something went wrong with the pdf upload.').' <br/> '.__('Maybe the pdf size exceeds the maximum size limit.');
					elseif( $_GET['error'] == 'imagepdf' )
						echo __('Something went wrong with the image and pdf upload.');
					?>
                </p>
			</div>
            <?php endif ?>
			<?php if($this->uri->segment(5) == false) : ?>
				<?php if(!isset($newgroupid) || $newgroupid == 0 && $maincat == false) : ?>
					<p <?= (form_error("cataloggroup") != '') ? 'class="error"' : '' ?>>
						<label><?=__('Catalog Group')?></label>
						<select name="cataloggroup" id="cataloggroup">
							<option value=""><?= __('Select ...') ?></option>
							<?php foreach ($cataloggroups as $cataloggroup): ?>
								<option value="<?= $cataloggroup->id ?>" <?= set_select('cataloggroup', $cataloggroup->id, ($catalog->cataloggroupid == $cataloggroup->id ? TRUE : '')) ?>><?= $cataloggroup->name ?></option>
							<?php endforeach ?>
						</select>
		                <?php if(isset($venue)) : ?>
						<a href="<?= site_url('cataloggroups/add/'.$catalog->venueid.'/'.$this->uri->segment(4) . '/') ?>" style="position:absolute; margin:-22px 0 0 350px;"><?= __('Add cataloggroup') ?> &rsaquo;&rsaquo;</a>
		                <?php else : ?>
						<a href="<?= site_url('cataloggroups/add/'.$catalog->eventid.'/'.$this->uri->segment(4) . '/') ?>" style="position:absolute; margin:-22px 0 0 350px;"><?= __('Add cataloggroup') ?> &rsaquo;&rsaquo;</a>
		                <?php endif; ?>
					</p>
				<?php endif; ?>
			<?php endif; ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="name"><?=__('Name')?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('catalog', $catalog->id, 'name', $language->key); ?>
				<input type="text" name="name_<?=$language->key?>" id="name_<?=$language->key?>" value="<?= set_value('name_'.$language->key, ($trans != null) ? $trans : $catalog->name) ?>" />                
            </p>
            <?php endforeach; ?>
            <?php else : ?>
            <p <?= (form_error("name_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="name"><?= __('Name:') ?></label>
				<input type="text" name="name" id="name" value="<?= set_value('name_'.$app->defaultlanguage, $catalog->name) ?>" /> 
			</p>
            <?php endif; ?>
			<?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p>
                <label for="description"><?= __('Description') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('catalog', $catalog->id, 'description', $language->key); ?>
                <textarea name="description_<?php echo $language->key; ?>" id="description" <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>><?= htmlspecialchars_decode(set_value('description_'.$language->key, ($trans != null) ? $trans : $catalog->description), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endforeach; ?>
            <?php else : ?>
            <p>
                <label for="description"><?= __('Description:') ?></label>
                <textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description" <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>><?= htmlspecialchars_decode(set_value('description_'. $app->defaultlanguage, $catalog->description), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endif; ?>
            
			<?php if ($this->basket_model->active($catalog->venueid) && $basegroup != false && $catalogmodule == 'menu'): ?>
			<div class="errmsg" style="display: none">
			</div>
			<p <?= (form_error("price") != '') ? 'class="error"' : '' ?>>
				<label for="price"><?=__('Price:')?></label>
				<input type="text" name="price" value="<?= set_value('price', $item->price) ?>" id="price">
			</p>
			<?php endif ?>
			<?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
	            <p <?= (form_error("urltitle_".$language->key) != '') ? 'class="error"' : '' ?>>
	                <label for="name"><?=__('Title of url')?> <?= '(' . $language->name . ')'; ?>:</label>
					<?php $trans = _getTranslation('catalog', $catalog->id, 'urltitle', $language->key); ?>
					<input type="text" name="urltitle_<?=$language->key?>" id="urltitle_<?=$language->key?>" value="<?= set_value('urltitle_'.$language->key, ($trans != null) ? $trans : $catalog->urltitle) ?>" />
	            </p>
			 	<?php endforeach; ?>
			<?php endif; ?>
			<?php foreach($languages as $language) : ?>
				<p <?= (form_error("url_".$language->key) != '') ? 'class="error"' : '' ?>>
					<label><?= __('Link (Full url)') ?> <?= '(' . $language->name . ')'; ?>:</label>
					<?php $trans = _getTranslation('catalog', $catalog->id, 'url', $language->key); ?>
					<input type="text" name="url_<?=$language->key?>" value="<?= set_value('url_'.$language->key, $trans) ?>" id="url_<?=$language->key?>">
				</p>
			<?php endforeach; ?>
			<?php foreach($languages as $language) : ?>
			<p <?= (form_error("pdfdoc".$language->key) != '') ? 'class="error"' : '' ?>>
				<label for="pdfdoc<?=$language->key?>"><?= __('PDF') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<input type="file" name="pdfdoc<?=$language->key?>" id="pdfdoc<?=$language->key?>" value="<?= set_value('pdfdoc'.$language->key) ?>" /><br />
				<span class="note" style="text-align: left;width: 200px;"><?= __('Max size %s mb',2) ?></span><br />
                <?php if( isset( $catalog->pdfdoc[$language->key]['thumbnail'] ) && $catalog->pdfdoc[$language->key]['thumbnail'] ): ?>
                <img src="data:image/png;base64,<?= $catalog->pdfdoc[$language->key]['thumbnail'] ?>" style="border:1px solid #cccccc;" /><br />
                <span><a href="<?= site_url('catalog/removepdf/'.$catalog->id.'/'.$language->key.'/venue') ?>" class="deletemap"><?= __('Remove') ?></a></span>
                <?php endif;?>
				<br clear="all" />
			</p>
			<?php endforeach; ?>
			<p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl"><?= (!isset($venue)) ? __('Logo') : __('Image')?>:</label>
				<span class="evtlogo" <?php if($catalog->imageurl != ''){ ?>style="width:50px; height:50px; background:transparent url('<?= image_thumb($catalog->imageurl, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="imageurl" id="imageurl" value="" class="logoupload" value="<?= set_value('imageurl') ?>" style="width:360px;" /><br />
				<?php if($catalog->imageurl != null && $catalog->imageurl != '') : ?>
				<?php if($catalog->imageurl != '' && file_exists($this->config->item('imagespath') . $catalog->imageurl)){ ?><a href="<?= site_url('catalog/crop/'.$catalog->id . '/venue/'.$catalogtype) ?>"><?= __('Edit picture') ?></a><?php } ?>
				<?php endif; ?>
				<br />
				<span class="note" style="text-align: left;width: 200px;"><?= __('Max size %s px by %s px',2000,2000) ?></span>
				<br clear="all" />
				<?php if($catalog->imageurl != '' && file_exists($this->config->item('imagespath') . $catalog->imageurl)){ ?><span><a href="<?= site_url('catalog/removeimage/'.$catalog->id.'/venue') ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
			</p><br clear="all"/>
			<?php if($app->id != 393) : ?>
 			<div class="row">
				<p style="min-height:0px;">
				<label for="tags">Tags:</label>
				</p>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<?php else: ?>
				<p>
					<label for="tagdelbar"><?= __('Merk:') ?></label><br/>
					<select name="tagdelbar">
						<option value="Audi" <?= (in_array('Audi', $tags)) ? 'selected' : '' ?>><?=__('Audi')?></option>
						<option value="Volkswagen" <?= (in_array('Volkswagen', $tags)) ? 'selected' : '' ?>><?=__('Volkswagen')?></option>
						<option value="skoda" <?= (in_array('skoda', $tags)) ? 'selected' : '' ?>><?=__('Skoda')?></option>
						<option value="volkswagenbedrijf" <?= (in_array('volkswagenbedrijf', $tags)) ? 'selected' : '' ?>><?=__('Volkswagen Bedrijfsvoertuigen')?></option>
					</select>
				</p>
			<?php endif; ?>
			<?php if(!isset($newgroupid) || $newgroupid == 0 && $maincat == false) : ?>
				<p>
					<label for="selbrands"><?= __('Brands') ?></label>
					<select name="selbrands[]" id="selbrands" multiple size="15" class="multiple">
						<?php if ($brands != FALSE && count($brands) > 0): ?>
						<?php foreach ($brands as $brand): ?>
							<option value="<?= $brand->id ?>" <?= set_select('selbrands[]', $brand->id, (in_array($brand->id, $catalog->brands) ? TRUE : '')) ?>><?= $brand->name ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<p>
					<label for="selcategories"><?= __('Categories') ?></label>
					<select name="selcategories[]" id="selcategories" multiple size="15" class="multiple">
						<?php if ($categories != FALSE && count($categories) > 0): ?>
						<?php foreach ($categories as $category): ?>
							<option value="<?= $category->id ?>" <?= set_select('selcategories[]', $category->id, (in_array($category->id, $catalog->categories) ? TRUE : '')) ?>><?= $category->name ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<?php if (!isset($venue)): ?>
				<p><label></label><?= __('Add one or more brands/categories first <a href="%s">here</a>',site_url('catalogbrands/event/'.$event->id)) ?></p>
				<?php else: ?>
				<p><label></label><?= __('Add one or more brands/categories first <a href="%s>here</a>',site_url('catalogbrands/venue/'.$venue->id)) ?></p>
				<?php endif ?>
			<?php else: ?>
				<?php if($maincat != false && !empty($catshtml)) : ?>
					<br />
					<label style="margin-bottom:10px;"><?= __('Item groups') ?>:</label>
					<ul>
						<?php foreach($catshtml as $option) : ?>
						<?php $option = str_replace('catalogcategories', 'Catalog', $option); ?>
						<?= $option ?>
						<?php endforeach; ?>
					</ul><br />
				<?php endif; ?>
			<?php endif; ?>

			<?php if (!isset($venue)): ?>
					<p>
						<label for="imageurl"><?= __('Image:') ?></label>
						<span class="evtlogo" <?php if($catalog->imageurl != ''){ ?>style="background:transparent url('<?= image_thumb($catalog->imageurl, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
						<input type="file" name="imageurl" id="imageurl" value="" class="logoupload" />
						<?php if($catalog->imageurl != null && $catalog->imageurl != '') : ?>
						<a href="<?= site_url('catalog/crop/'.$catalog->id . '/venue/'.$catalogtype) ?>"><?= __('Edit picture') ?></a>
						<?php endif; ?>
					</p><br clear="all"/>
			<?php endif ?>
			<?php if($confbagactive) : ?>
			<p <?= (form_error("confbag") != '') ? 'class="error"' : '' ?>>
				<label for="confbag"><?= __('Conference bag content:') ?></label>
				<input type="file" name="confbagcontent" id="confbagcontent" />
				<br clear="all" />
				<?php foreach($catalogconfbag as $c) : ?>
					<span style="width:auto;"><?=substr($c->documentlink, strrpos($c->documentlink,'/') +1,strlen($c->documentlink))?><a href="<?=site_url('confbag/removeitem/'.$c->id.'/venue/catalog');?>"  style="border-bottom:none; margin-left:5px;"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></span><br clear="all"/>
				<?php endforeach;?>
			</p>
			<?php endif; ?>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order', $catalog->order) ?>" id="order"><br/>
			</p>
			<?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<?php if(_checkMultilang($m, $lang->key, $app)): ?>
						<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
							<?= _getInputField($m, $app, $lang, $languages, 'catalog', $catalog->id); ?>
						</p>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
<!-- 			<p <?= (form_error("premium") != '') ? 'class="error"' : '' ?> class="premiump">
				<input type="checkbox" name="premium" class="checkboxClass premiumCheckbox" <?= ($premium) ? 'checked="checked"' : '' ?> id="premium"><?=__(' Premium?')?>
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?> id="extralinep">
				<label><?=__('Premium order:')?></label>
				<input type="text" name="premiumorder" value="<?= set_value('premiumorder', $premium->sortorder) ?>" id="premiumorder">
				<label><?=__('Extra line:')?></label>
				<input type="text" name="extraline" value="<?= set_value('extraline', (isset($premium->extraline)) ? $premium->extraline : '') ?>" id="extraline">
			</p> -->
			<?php if($app->show_external_ids_in_cms == 1) : ?>
			<p  <?= (form_error("externalid") != '') ? 'class="error"' : '' ?>>
				<label><?=__('External id:')?></label>
				<input type="text" name="externalid" value="<?= set_value('externalid', $catalog->externalid) ?>" id="externalid"><br/>
			</p>
			<?php endif; ?>
			<div class="buttonbar">
				<input type="hidden" name="catshidden" value="<?= $currentCats ?>" id="catshidden">
				<input type="hidden" name="postback" value="postback" id="postback">
				<?php if($newgroupid != '') : ?>
				<a href="<?= ("javascript:history.go(-1);"); ?>" class="btn"><?= __('Cancel') ?></a>
				<?php else : ?>
				<a href="<?= ("javascript:history.go(-1);"); ?>" class="btn"><?= __('Cancel') ?></a>
				<?php endif; ?>
				
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script>
(function(){

$(document).ready(
	function()
	{
		$(".frmsessions input#price").change(
			function()
			{
				var val = $(".frmsessions input#price").val();
				
				if(isNaN(val))
				{
					$(".frmsessions div.errmsg").text("(Please introduce a valid number!)");
					$(".frmsessions div.errmsg").css("color","red");
					$(".frmsessions div.errmsg").show('slow').delay(2500).hide(800);
					
					$(".frmsessions input#price").val(<?=$item->price?>);
				}
				
			}
		);
	}
);

})();
</script>