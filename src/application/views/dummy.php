<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=7" />

	<title><?= ( isset($masterPageTitle) && $masterPageTitle ) ? $masterPageTitle : __("{$this->channel->name} Admin")?></title>
	<base href="<?= base_url() ?>" target="" />


	<!-- CSS -->
	<link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<?php if($this->templatefolder != ''){ ?>
		<link rel="stylesheet" href="templates/<?= $this->templatefolder ?>/css/master.css" type="text/css" />
	<?php } ?>
	<link rel="stylesheet" href="css/masters.css" type="text/css" />
</head>
<body style="background:none;">
<h1>Form</h1>
<?php if(validation_errors()) : ?>
	<div class="error">
		<?php echo validation_errors(); ?>
	</div>
<?php endif; ?>
<div class="frmsessions">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">         
		<p <?= (form_error("test") != '') ? 'class="error"' : '' ?>>
			<label for="test"><?= __('test:') ?></label>
			<input type="file" name="test" id="test" value="<?= set_value('test') ?>" />
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" class="btn primary"><?= __('Send') ?></button>
			<br clear="all" />
		</div>
		<br clear="all" />
	</form>
</div>
</body>