<a href="<?= site_url('module/editByController/about/event/'.$event->id.'/0') ?>" class="edit btn" style="float:right;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a>
<div id="myTab">
<div class="tabbable tabs-below">
	<ul class="nav nav-tabs">
		<li class="active">
			<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#info"><?= __('Info')?></a>
		</li>
		<li class="">
			<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#travelinfo"><?= __('Travelinfo')?></a>
		</li>
		<li class="">
			<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#schedule"><?= __('Schedule')?></a>
		</li>
	</ul>
	<br clear="all" />
	<div class="tab-content">
		<div id="info" class="tab-pane active">
			<form action="<?= site_url('event/edit/'.$event->id.'/'.$launcherid) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent" style="margin-top:-10px;">
				<?php if($error != ''): ?>
				<div class="error"><?= str_replace("The event type field can not exceed 1 characters in length.", "Please choose an event type.", $error) ?></div>
				<?php endif ?>
				<?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
				<div class="error"><p><?= __('Something went wrong with the image upload. <br/> Maybe the image size exceeds the maximum width or height') ?></p></div>
				<?php endif ?>
				<fieldset>
					<!-- <h3><?=__('Edit Event')?></h3> -->
			        <?php if(isset($languages) && !empty($languages)) : ?>
			        <?php foreach($languages as $language) : ?>
			        <p <?= (form_error("w_name_".$language->key) != '') ? 'class="error"' : '' ?>>
			            <label for="w_name"><?=__('Name ')?> <?= '(' . $language->name . ')'; ?>:</label>
						<?php $trans = _getTranslation('event', $event->id, 'name', $language->key); ?>
			            <input type="text" name="w_name_<?php echo $language->key; ?>" id="w_name" value="<?= htmlspecialchars_decode(set_value('w_name_'.$language->key, ($trans != null) ? $trans : $event->name ), ENT_NOQUOTES) ?>" />
			        </p>
			        <?php endforeach; ?>
			        <?php else : ?>
			        <p <?= (form_error("w_name_".$app->defaultlanguage ) != '') ? 'class="error"' : '' ?>>
			            <label for="w_name"><?= __('Name:') ?></label>
			            <input type="text" name="w_name_<?php echo $app->defaultlanguage; ?>" id="w_name_<?php echo $app->defaultlanguage; ?>" value="<?= htmlspecialchars_decode(set_value('w_name_'.$app->defaultlanguage, $event->name), ENT_NOQUOTES) ?>" />
			        </p>
			        <?php endif; ?>
					<p <?= (form_error("permanent") != '' || form_error("permanent") != '') ? 'error' : '' ?>>
						<label for="permanent"><?= __('Permanent:') ?></label>
						<input type="checkbox" name="permanent" id="permanent" value="checked" style="width:20px;clear:right;"><br clear="all" />
					</p>
					<p <?= (form_error("w_datefrom") != '' || form_error("w_dateto") != '') ? 'error' : '' ?>  id="dates">
						<label for="w_datefrom"><?= __('From - To:') ?></label>
						<input type="text" name="w_datefrom" id="w_datefrom" value="<?= set_value('w_datefrom', date('d-m-Y', strtotime($event->datefrom))) ?>" class="datepicker" autocomplete="off" />
						<input type="text" name="w_dateto" id="w_dateto" value="<?= set_value('w_dateto', date('d-m-Y', strtotime($event->dateto))) ?>" class="datepicker" autocomplete="off" /><br clear="all" />
					</p>
					<p>
						<label for="w_logo"><?= __('Logo:') ?></label>

						<?php if($event->eventlogo != "" && file_exists($this->config->item('imagespath') . $event->eventlogo)){ ?><span class="evtlogo" style="background:transparent url(<?= image_thumb($event->eventlogo, 50, 50) ?>) no-repeat top left;"></span><?php } ?>
						<input type="file" name="w_logo" id="w_logo" value="<?= set_value('w_logo')?>" class="logoupload" /><br clear="all"/>
						<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
						<?php if($event->eventlogo != '' && file_exists($this->config->item('imagespath') . $event->eventlogo)){ ?><span><a href="<?= site_url('event/removeimage/'.$event->id) ?>" class="deletemap"><?=__('Remove')?></a></span><?php } ?>
					</p>
			        <?php if(isset($languages) && !empty($languages)) : ?>
			        <?php foreach($languages as $language) : ?>
					<p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
						<label for="description"><?=__('Description')?> <?= '(' . $language->name . ')'; ?>:</label>
						<?php $trans = _getTranslation('event', $event->id, 'description', $language->key); ?>
						<textarea name="description_<?php echo $language->key; ?>" id="description"><?= htmlspecialchars_decode(set_value('description_'.$language->key, ($trans != null) ? $trans : $event->description), ENT_NOQUOTES) ?></textarea>
					</p>
			        <?php endforeach; ?>
			        <?php else : ?>
					<p <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
						<label for="description"><?= __('Description:') ?></label>
						<textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description"><?= htmlspecialchars_decode(set_value('description_'. $app->defaultlanguage, $event->description), ENT_NOQUOTES) ?></textarea>
					</p>
			        <?php endif; ?>
					<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
						<label><?= __('Website:') ?></label>
						<input type="text" name="website" value="<?= htmlspecialchars_decode(set_value('website', $event->website), ENT_NOQUOTES) ?>" id="website">
					</p>
					<p <?= (form_error("phonenr") != '') ? 'class="error"' : '' ?>>
						<label><?= __('Phone:') ?></label>
						<input type="text" name="phonenr" value="<?= htmlspecialchars_decode(set_value('phonenr', $event->phonenr), ENT_NOQUOTES) ?>" id="phonenr">
					</p>
					<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
						<label><?= __('Email:') ?></label>
						<input type="text" name="email" value="<?= htmlspecialchars_decode(set_value('email', $event->email), ENT_NOQUOTES) ?>" id="email">
					</p>
					<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
						<label><?= __('Order:') ?></label>
						<input type="text" name="order" value="<?= htmlspecialchars_decode(set_value('order', $event->order), ENT_NOQUOTES) ?>" id="order">
					</p>
					<p <?= (form_error("ticketlink") != '') ? 'class="error"' : '' ?>>
						<label><?= __('Ticketlink:') ?></label>
						<input type="text" name="ticketlink" value="<?= htmlspecialchars_decode(set_value('ticketlink', $event->ticketlink), ENT_NOQUOTES) ?>" id="ticketlink">
					</p>
					<?php foreach($metadata as $m) : ?>
						<?php foreach($languages as $lang) : ?>
							<?php if(_checkMultilang($m, $lang->key, $app)): ?>
								<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
									<?= _getInputField($m, $app, $lang, $languages, 'event', $event->id); ?>
								</p>
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endforeach; ?>
			<!-- 		<div class="row">
						<p style="min-height:0px;">
						<label for="tags">Tags:</label>
						</p>
						<ul id="mytags" style="margin-left:-10px;"></ul>
					</div> -->

				</fieldset>
				<fieldset>
					<h3><?=__('Location')?></h3>
			<!-- 		<?php if(($app->apptypeid == 2 || $app->apptypeid == 4) && !empty($venues)) : ?>
					<p <?= (form_error("selvenue") != '') ? 'class="error"' : '' ?>>
						<label for="login">Venue:</label>
						<select name="selvenue">
							<?php foreach($venues as $venue) : ?>
							<option value="<?=$venue->id?>" <?= ($venue->id == $event->venueid ? 'selected' : '') ?>><?=$venue->name?></option>
							<?php endforeach; ?>
						</select>
					</p>
					<?php else: ?> -->
					<p <?= (form_error("w_venuename") != '') ? 'class="error"' : '' ?>>
						<label for="login"><?= __('Name:') ?></label>
						<input type="text" name="w_venuename" id="w_venuename" value="<?= htmlspecialchars_decode(set_value('w_venuename', $venue->name), ENT_NOQUOTES) ?>" />
					</p>
					<p <?= (form_error("w_address") != '') ? 'class="error"' : '' ?>>
						<label for="venuestreet"><?= __('Address:') ?></label>
						<input type="text" name="w_address" value="<?= htmlspecialchars_decode(set_value('w_address', $venue->address), ENT_NOQUOTES) ?>" />
					</p>
			<!-- 		<?php endif; ?> -->
				</fieldset>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<a href="<?= site_url('event/view/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
					<button type="submit" class="btn primary"><?= __('Save Event') ?></button>
					<br clear="all" />
				</div>
				<br clear="all" />
			</form>
			<script type="text/javascript" charset="utf-8">
				$(document).ready(function(){
					$("#mytags").tagit({
						initialTags: [<?php if(isset($tags) && $tags != null){foreach($tags as $val){ echo '"'.$val->tag.'", '; }} ?>],
					});

			        var tags = new Array();
			        var i = 0;
			        <?php foreach($apptags as $tag) : ?>
			        tags[i] = '<?=$tag->tag?>';
			        i++;
			        <?php endforeach; ?>
			        $("#mytags input.tagit-input").autocomplete({
			            source: tags
			        });
				});
			</script>
			<script type="text/javascript">
			<?php if($event->datefrom == '1970-01-01') : ?>
				$('#permanent').attr('checked', 'checked');
				$('#dates').css('display', 'none');
			<?php endif; ?>
			$('#permanent').click(function() {
				if($('#permanent').attr('checked') == 'checked') {
					$('#dates').css('display', 'none');
				} else {
					$('#dates').css('display', 'block');
				}
			});
			</script>
		</div>
		<div id="travelinfo" class="tab-pane">
			<?php if($this->session->flashdata('event_feedback') != ''): ?>
			<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
			<?php endif ?>
			<div class="frmsessions">
				<form action="<?= site_url('event/travelinfo/'.$event->id.'/'.$launcherid) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
					<?php if($error != ''): ?>
					<div class="error"><?= $error ?></div>
					<?php endif ?>
					<?php
					if(isset($languages) && !empty($languages)) {
						foreach($languages as $language) {
							$trans = _getTranslation('venue', $venue->id, 'travelinfo', $language->key);
							$data = array(
									'name'			=> 'txt_travelinfo_'.$language->key,
									'id'			=> 'txt_travelinfo_'.$language->key,
									'toolbarset' 	=> 'Travelinfo',
									'value' 		=> htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('txt_travelinfo_'.$language->key, ($trans != null) ? $trans : $venue->travelinfo)))))),
									'width'			=> '630px',
									'height'		=> '400'
								);
							echo '<p><label>' . __('Travelinfo') . '('.$language->name.') </label></p>';
							echo form_fckeditor($data);
						}
					} else {
						$data = array(
								'name'			=> 'txt_travelinfo_'._currentApp()->defaultlanguage,
								'id'			=> 'txt_travelinfo_'._currentApp()->defaultlanguage,
								'toolbarset' 	=> 'Travelinfo',
								'value' 		=> htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('txt_travelinfo_'._currentApp()->defaultlanguage, $venue->travelinfo)))))),
								'width'			=> '700px',
								'height'		=> '400'
							);

						echo form_fckeditor($data);
					}
					?>
					<div class="buttonbar">
						<input type="hidden" name="postback" value="postback" id="postback">
						<button type="submit" class="btn primary"><?=__('Save')?></button>
						<br clear="all" />
					</div>
					<br clear="all" />
				</form>
			</div>
		</div>
		<div id="schedule" class="tab-pane">
			<div class="buttonbar" style="margin-right:0px;min-height: 40px;">
				<a href="<?= site_url('schedule/add/' . $event->id . '/event') ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i> <?= __('Add Schedule') ?></a>
			</div>
			<br clear="all" />
			<table id="listview" class="display zebra-striped">
				<thead>
					<tr>
						<?php foreach ($headers as $h => $field): ?>
							<?php if ($h == 'Order'): ?>
								<th class="data order"><?= $h ?></th>
							<?php else: ?>
								<th class="data"><?= $h ?></th>
							<?php endif ?>
						<?php endforeach; ?>
						<th style="text-align:right;"><?= __('Actions') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if ($schedule != FALSE): ?>
					<?php foreach($schedule as $row): ?>
							<tr>
								<?php foreach ($headers as $h => $field): ?>
		                        <?php if ($field != null && !empty($field)) : ?>
										<td><?= $row->{$field} ?></td>
		                        <?php endif; ?>
								<?php endforeach ?>
								<td>
								<div class="pull-right">
									<a href='<?= site_url("schedule/edit/" . $row->id . '/event') ?>' class="btn btn-mini btn-warning"><i class="icon-pencil icon-white"></i></a>
									<a href='<?= site_url("duplicate/index/" . $row->id . '/schedule/event') ?>' class="btn btn-mini btn-inverse"><i class="icon-tags icon-white"></i></a>
									<a href='<?= site_url("schedule/delete/" . $row->id . '/event') ?>' class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i></a>
								</div>
								</td>
							</tr>
					<?php endforeach ?>
					<?php endif ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>