<?php
/**
 * Tapcrowd list view
 * @author Tom Van de Putte
 */

if(!isset($settings)) {
	echo 'Problem: we need the settings variable';
	return;
}

$headers =& $settings->headers;
$colspan = count($headers)+1;			// # of headers + actions header
if($settings->checkboxes) {
	$colspan++;
}
?>

<?php
#
# Module settings button
#
if(isset($settings->module_url)) { ?>
	<div class="btn-group pull-right">

		<?php
		#
		# extra buttons on top
		#
		if(isset($settings->extrabuttons) && !empty($settings->extrabuttons)) : ?>
		<?php foreach($settings->extrabuttons as $btn) : ?>
		<a class="<?= $btn->btn_class ?>" href="<?= $btn->href ?>" target="<?=$btn->target ? $btn->target : '_self' ?>" style="margin-bottom:10px;">
			<?php if(stristr($btn->btn_class, 'edit')) { ?><i class="icon-pencil"></i><?php } ?>
			<?php if(!empty($btn->icon_class)) { ?> <i class="<?= $btn->icon_class?>"></i> <?php } ?>
			<?= $btn->title ?>
		</a>
		<?php endforeach; ?>
		<?php endif; ?>
		<a href="<?= 'http://clients.tapcrowd.com/questions?id='.$settings->form->id ?>" class="btn" target="_blank"><i class="icon-list-alt"></i> <?= __('View results page')?></a>
		<!-- <a href="<?=$settings->module_url?>" class="btn"><i class="icon-wrench"></i> <?=__('Module Settings')?></a> -->

	</div><?php
} ?>

<?php if(isset($settings->title)) : ?>
<h2><?= ucfirst($settings->title) ?></h2>
<?php else: ?>
<h2><?=(empty($settings->plural)? 'No title set' : ucfirst(__($settings->plural)))?></h2>
<?php endif; ?>

<?php if(isset($settings->premium)) : ?>
<div id="premiuminfo">
	<p><?= __('Extra: Premium listings.<br/>
				This feature allows you to give premium space to certain items by showing them on top of the list
				(regardless of the alphabetic order) and in a different color.
				') ?></p>
	<a href="<?= site_url('premium/'.$settings->parentType.'/'.$settings->parentId.'/'.$settings->premium) ?>" class="btn editPremiumButton"><?= __('Edit premium items') ?></a>
	<br clear="all" />
</div>
<?php endif; ?>

<?php
#
# Tabs
#
if(isset($settings->tabs)) : ?>
<div class="tabs-below">
	<ul class="nav nav-tabs">
		<?php foreach ($settings->tabs as $t)  : ?>
		<li <?= $t->active ? 'class="active"' : '' ?>>
			<a href="<?= $t->url ?>"><?= $t->title ?></a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>
<br clear="all" />
<?php endif; ?>
<?php
#
# Button toolbar
#
if(isset($settings->toolbar)) {?>
	<div class="btn-toolbar"><?php
	foreach($settings->toolbar as $btn) {
		if(!empty($btn->btn_class)) $btn->icon_class .= ' icon-white';
		echo '<div class="btn-group"><a href="'.$btn->href.'" class="btn '.$btn->btn_class;
		echo '"><i class="'.$btn->icon_class.'"></i> '.$btn->title.'</a></div>';
	} ?>
	</div><?php
} ?>

<?php
#
# ERROR Notifications
#
if(isset($_SESSION['error'])) { ?>
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<p><strong>Error!</strong> <?=$_SESSION['error']?></p>
	</div><?php
	unset($_SESSION['error']);
}

#
# MESSAGE Notifications
#
if(isset($_SESSION['message'])) { ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<p><strong>Success!</strong> <?=$_SESSION['message']?></p>
	</div><?php
	unset($_SESSION['message']);
}
?>

<?php
#
# Filter by group
#
if(isset($settings->filter)) : ?>
	<select id="<?= $settings->filter['id'] ?>" class="<?= $settings->filter['class'] ?>" name="<?= $settings->filter['name'] ?>">
	<option value=""><?= $settings->filter['label'] ?></option>
	<?php foreach($settings->filter['data'] as $d) : ?>
	<option value="<?= $d->id ?>" <?= $d->id == $settings->filter['selected'] ? 'selected="selected"' : ''?>><?= $d->name?></option>
	<?php endforeach; ?>
	</select>
	<?php if(isset($settings->filtermanage)) : ?>
	<a href="<?= $settings->filtermanage['url'] ?>" class="<?= $settings->filtermanage['class'] ?>" style="margin-top:-11px;"><?= $settings->filtermanage['title'] ?></a>
	<?php endif; ?>
	<script charset="utf-8" type="text/javascript">
		$(document).ready(function() {
			$("#<?= $settings->filter['id'] ?>").change(function(){
				window.location = "<?= site_url($settings->filter['url']) ?>/" + $("#<?= $settings->filter['id'] ?>").val();
			});
		});
	</script>
<?php endif; ?>

<table class="table table-striped table-condensed table-hover table-engine">
<thead>
	<tr>
	<th colspan="<?=$colspan?>" class="row-fluid">
		<div class="pull-right">
			<?php if(isset($sessions) && !empty($sessions)) : ?>
			<script type="text/javascript">
			function changeSession(){
			  var myselect = document.getElementById("session");
			  window.location = '<?= site_url("askaquestion/event/".$event->id) ?>'+'?sessionid='+myselect.options[myselect.selectedIndex].value;
			}
			</script>
			<select name="session" onchange="changeSession()" id="session" style="margin-top:10px;">
				<option value=""></option>
			<?php foreach($sessions as $s) : ?>
				<option value="<?=$s->id?>" <?= isset($_GET['sessionid']) && !empty($_GET['sessionid']) && $_GET['sessionid'] == $s->id ? 'selected="selected"' : '' ?>><?= $s->name ?></option>
			<?php endforeach; ?>
			</select>
			<?php endif; ?>
			<a href="javascript:history.go(0)" class="btn"><?=__('Refresh')?></a>
			<?php
			#
			# Delete all button
			#
			if($settings->checkboxes) { ?>
				<a href="<?=$settings->deletecheckedurl?>" class="btn btn-danger deletechecked"><i class="icon-remove icon-white"></i> <?=__('Delete checked')?></a>
			<?php
			} ?>
			<?php if(isset($settings->import_url) && !empty($settings->import_url)) : ?>
			<a href="<?=$settings->import_url?>" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> <?=__('Import')?></a>
			<?php endif; ?>
			<?php if(isset($settings->rss_url) && !empty($settings->rss_url)) : ?>
			<a href="<?=$settings->rss_url?>" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> <?=__('RSS')?></a>
			<?php endif; ?>
			<?php if(isset($settings->add_url) && !empty($settings->add_url)) : ?>
			<a href="<?=$settings->add_url?>" class="btn btn-primary"><i class="icon-plus-sign icon-white"></i> <?=__('Add Question')?></a>
			<?php endif; ?>
		</div>
		<br clear="all" />
		<form class="formsearch">
			<div class="input-append">
				<input name="q" type="text" class="input-large search-query">
				<input name="filter" type="hidden" value="name"  />
				<button type="submit" class="btn"><i class="icon-search"></i> <?= __('Search')?></button>
			</div>
		</form>
		<div class="infobox searchlist span12"></div>
	</th>
	</tr>
	<tr><?php
	$i = 0;
	foreach($headers as $h => $field) {
		if($settings->checkboxes && $i == 0) {
			echo '<th style="max-width:1em !important;"><input type="checkbox" class="selectallcheckbox" /></th>';
		}
			echo "<th class='sort-alfa'>$h</th>";
		$i++;
	} ?>
		<!-- <th style="text-align: right;min-width:100px;"><?=__('Actions')?></th> -->
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="<?=$colspan?>">
			<div class="pagination"></div>
			<br /><br />
		</td>
	</tr>
</tfoot>
<tbody><?php

# No data
if(empty($data)) { echo '<tr><td colspan="'.$colspan.'">' . __('No data') . '</td></tr></tbody></table>'; return; }

#
# Render rows
#
// $row_fmt = '<tr id="%s">' . str_repeat('%s', count($headers)) . '<td><div class="pull-right">';
// foreach($settings->actions as $a) {
// // 	$row_fmt.= '<a class="btn btn-small '.$a->btn_class.'" href="'.$a->href.'"><i class="'.$a->icon_class.'  icon-white"></i> '.$a->title.'</a>';
// 	$row_fmt.= ' <a class="btn btn-mini '.$a->btn_class.'" href="'.$a->href.'"><i class="'.$a->icon_class.'  icon-white" title="'.$a->title.'"></i></a>';
// }
// $row_fmt.= '</div></td></tr>';
foreach($data as $submission) {
	$row = '<tr>';
	$i = 0;
	foreach($submission as $submissionfield) {
		if($settings->checkboxes && $i == 0) {
			$row .= '<td><input type="checkbox" class="deletecheckbox" name="'.$submissionfield->formsubmissionid.'" /></td>';
		}
		if($i == 0) {
			$row .= '<td>'.$submission[0]->num.'</td>';
			if(in_array('moderation', $formopts)) {
				if($submissionfield->visible == 1) {
					$row .= '<td><a href="'.site_url('askaquestion/show/'.$submissionfield->formsubmissionid.'/hide').'" id="hide'.$submissionfield->formsubmissionid.'" class="questions onoff deactivate">&nbsp;</a></td>';
				} else {
					$row .= '<td><a href="'.site_url('askaquestion/show/'.$submissionfield->formsubmissionid.'/show').'" id="show'.$submissionfield->formsubmissionid.'" class="questions onoff activate">&nbsp;</a></td>';
				}
			}			
			$row .= '<td>'.$submissionfield->value.'</td>';
		} elseif($i == 3) {
			$row .= '<td>'.$submissionfield->speakername.'</td>';
		} elseif($i >= 1) {
			$row .= '<td>'.$submissionfield->value.'</td>';
		}

		$i++;
	}

	if(in_array('interesting', explode(',', $submission[0]->formsubmissionopts))) {
		$row .= '<td>'.'<input type="checkbox" class="checkboxClass interestingCheckbox" name="interesting'.$submission->id.'" id="interesting'.$submission[0]->formsubmissionid.'" checked="checked" /></td>';
	} else {
		$row .= '<td>'.'<input type="checkbox" class="checkboxClass interestingCheckbox" name="interesting'.$submission->id.'" id="interesting'.$submission[0]->formsubmissionid.'"/></td>';
	}

	if(in_array('answered', explode(',', $submission[0]->formsubmissionopts))) {
		$row .= '<td>'.'<input type="checkbox" class="checkboxClass answeredCheckbox" name="answered'.$submission->id.'" id="answered'.$submission[0]->formsubmissionid.'" checked="checked" /></td>';
	} else {
		$row .= '<td>'.'<input type="checkbox" class="checkboxClass answeredCheckbox" name="answered'.$submission->id.'" id="answered'.$submission[0]->formsubmissionid.'" /></td>';
	}
	
	$row .= '</tr>';
	echo $row;
}
?>
</tbody>
</table>

<script type="text/javascript">
$('.onoff').click(function(e){
	e.preventDefault();
	var url = $(this).attr('href');
	var baseurl = "<?=site_url()?>";
	var questionid = url.substring(baseurl.length + 18, url.length - 5);
	var id = e.target.id;
	$.ajax({
		type: "GET",
		url: url
	}).done(function( data ) {
		if(id.indexOf('show') === -1) {
			$('#'+id).css('background-position', '-64px 0');
			$('#'+id).attr('href',"<?=site_url('askaquestion/show')?>/"+questionid+'/show');
			$('#'+id).attr('id',id.replace('hide', 'show'));
 		} else {
			$('#'+id).css('background-position', '0 0');
			$('#'+id).attr('href',"<?=site_url('askaquestion/show')?>/"+questionid+'/hide');
			$('#'+id).attr('id',id.replace('show', 'hide'));
		}
		
	});
});

$('.interestingCheckbox').mousedown(function(){
	var url = "<?= site_url('askaquestion/opts')?>";

	var id = $(this).attr('id');
	if($(this).is(':checked')) {
		url += '/'+id.substring(11)+'/0/interesting';
	} else {
		url += '/'+id.substring(11)+'/1/interesting';
	}

	$.ajax({
		type: "GET",
		url: url
	}).done(function( data ) {
		
	});
});
$('.answeredCheckbox').mousedown(function(){
	var url = "<?= site_url('askaquestion/opts')?>";

	var id = $(this).attr('id');
	if($(this).is(':checked')) {
		url += '/'+id.substring(8)+'/0/answered';
	} else {
		url += '/'+id.substring(8)+'/1/answered';
	}

	$.ajax({
		type: "GET",
		url: url
	}).done(function( data ) {
		
	});
});
</script>