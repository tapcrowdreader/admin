<div id="frmlogin">
	<form action="<?= site_url('auth/login') ?>" method="POST" accept-charset="utf-8">
		<?php if(file_exists("templates/".$this->templatefolder."/img/logo.png")) : ?>
			<h1>Sign in <img src="templates/<?=$this->templatefolder?>/img/logo.png" height="25" alt="Tapcrowd"></h1>
		<?php else: ?>
			<h1>Sign in <img src="img/auth/logo-tapcrowd.png" width="143" height="25" alt="Tapcrowd"></h1>
		<?php endif; ?>
		
		<?php if (isset($error) && $error != ''): ?>
			<p class="feedback error fadeout"><?= $error ?></p>
		<?php endif ?>
		<?php if ($this->session->flashdata("form_feedback") != ''): ?>
			<p class="feedback succes fadeout"><?= $this->session->flashdata("form_feedback") ?></p>
		<?php endif ?>
		<p>
			<label for="login">Username or Email</label>
			<input type="text" name="login" value="<?= set_value("login") ?>" id="login" class="username" autocomplete="off" title="<?= set_value("login", "Username") ?>" />
		</p>
		<p>
			<label for="password">Password</label>
			<input type="password" name="password" value="" id="password" class="password" title="Password" />
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback" />
			<input type="submit" value="Log in" class="btn primary"/>
			<p>
				Not yet registered? Register <a href="<?= site_url('auth/register') ?>"><b>here</b></a>.
			</p>
			<p>
				Lost your password? Retrieve it <a href="<?= site_url('auth/recovery') ?>"><b>here</b></a>.
			</p>
                        
                        
			<br clear="all" />
		</div>
	</form>
</div>