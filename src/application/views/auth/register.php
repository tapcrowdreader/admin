<div id="frmlogin">
	<form class="frmregister" action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8">
		<?php if(file_exists("templates/".$this->templatefolder."/img/logo.png")) : ?>
			<h1>Sign up <img src="templates/<?=$this->templatefolder?>/img/logo.png" height="25" alt="Tapcrowd"></h1>
		<?php else: ?>
			<h1>Sign up <img src="img/auth/logo-tapcrowd.png" width="143" height="25" alt="Tapcrowd"></h1>
		<?php endif; ?>
		<?php if (isset($error) && $error != ''): ?>
			<p class="feedback error fadeout"><?= $error ?></p>
		<?php endif ?>
		<?php if ($this->session->flashdata("form_feedback") != ''): ?>
			<p class="feedback succes fadeout"><?= $this->session->flashdata("form_feedback") ?></p>
		<?php endif ?>
		<p>
			<label for="name">Name</label>
			<input type="text" name="name" value="<?= set_value('name'); ?>" autocomplete="off" id="name" />
        </p>
		<p>
			<label for="login">Username</label>
			<input type="text" name="login" value="<?= set_value('login'); ?>" autocomplete="off" id="login" />
		</p>
		<p>
			<label for="password">Password</label>
			<input type="password" name="password" value="<?= set_value('password'); ?>" autocomplete="off" id="password" />
		</p>
		<p>
			<label for="password2">Re-enter password</label>
			<input type="password" name="password2" value="<?= set_value('password2'); ?>" autocomplete="off" id="password2" />
		</p>
		<p>
			<label for="email">E-mail</label>
			<input type="text" name="email" value="<?= set_value('email'); ?>" autocomplete="off" id="email" />
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback" />
			<input type="submit" class="btn primary" value="Register" />
			
			<p>You already have an account? Login 
			<a href="<?= site_url('auth/login') ?>" class="button login"><b>here</b></a>
			</p>
			<br clear="all" />
		</div>
	</form>
</div>