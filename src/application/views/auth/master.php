<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="<?= base_url(); ?>" target="">
		
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=7" />
		
		<title>TapCrowd Admin</title>
		
		<link rel="stylesheet" href="css/auth/reset.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="css/auth/auth.css" type="text/css" media="screen" charset="utf-8" />
		
		<script src="js/auth/jquery-1.6.2.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/auth/jquery.labelify.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/auth/tapcrowd.js" type="text/javascript" charset="utf-8"></script>
		
		<!--[if IE 6]>
		<script type="text/javascript"> 
			if(typeof jQuery == 'undefined'){ document.write("<script type=\"text/javascript\"   src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\"></"+"script>"); var __noconflict = true; } 
			var IE6UPDATE_OPTIONS = { icons_path: "http://static.ie6update.com/hosted/ie6update/images/" }
		</script>
		<script type="text/javascript" src="http://static.ie6update.com/hosted/ie6update/ie6update.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="wrapper">
			<div id="header"></div>
			<div id="content">
				<?= isset($content) ? $content : "404 - Page not found" ?>
			</div>
			<div class="push"></div>
		</div>
		<div id="footer"></div>

		<script type="text/javascript">
		document.write(unescape("%3Cscript src='" + document.location.protocol +
		  "//munchkin.marketo.net/munchkin.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script>Munchkin.init('771-HZH-025');</script>

		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-10861070-2']);
		  _gaq.push(['_setDomainName', 'tapcrowd.com']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
	</body>
</html>