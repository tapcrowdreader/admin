<div id="login_form">
    <div id="frmlogin">
		<form class="frmlostpassword" action="<?= site_url('auth/recovery') ?>" class="login" id="login" method="post" accept-charset="utf-8">
			<?php if(file_exists("templates/".$this->templatefolder."/img/logo.png")) : ?>
				<h1>Password Recovery <img src="templates/<?=$this->templatefolder?>/img/logo.png" height="25" alt="Tapcrowd"></h1>
			<?php else: ?>
            	<h1>Password Recovery <img src="img/auth/logo-tapcrowd.png" width="143" height="25" alt="Tapcrowd"></h1>
            <?php endif; ?>
                <?php if (isset($error) && $error != ''): ?>
                    <p class="feedback error fadeout"><?= $error ?></p>
                <?php endif ?>
			<p>
				<label for="email">Your email: </label>
				<input type="text" name="email" id="emailrecovery" size='50' value="" />
			</p>
			<div class="buttonbar">
                <input type="hidden" name="postback" value="postback" id="postback" />
                <input type="submit" class="btn primary" value="Recover password" />
				<a href="<?= site_url('auth/login') ?>" class="button register">&laquo; Back</a>
                <br clear="all" />
			</div>
		</form>
    </div>
</div>