<h1><?=__('Travel info')?></h1>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<div class="frmsessions">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
		<?php if($error != ''): ?>
		<div class="error"><?= $error ?></div>
		<?php endif ?>
		<?php
		if(isset($languages) && !empty($languages)) {
			foreach($languages as $language) {
				$trans = _getTranslation('venue', $venue->id, 'travelinfo', $language->key);
				$data = array(
						'name'			=> 'txt_travelinfo_'.$language->key,
						'id'			=> 'txt_travelinfo_'.$language->key,
						'toolbarset' 	=> 'Travelinfo',
						'value' 		=> htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('txt_travelinfo_'.$language->key, ($trans != null) ? $trans : $venue->travelinfo)))))),
						'width'			=> '680px',
						'height'		=> '400'
					);
				echo '<p><label>' . __('Travelinfo') . '('.$language->name.') </label></p>';
				echo form_fckeditor($data);
			}
		} else {
			$data = array(
					'name'			=> 'txt_travelinfo_'._currentApp()->defaultlanguage,
					'id'			=> 'txt_travelinfo_'._currentApp()->defaultlanguage,
					'toolbarset' 	=> 'Travelinfo',
					'value' 		=> htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('txt_travelinfo_'._currentApp()->defaultlanguage, $venue->travelinfo)))))),
					'width'			=> '700px',
					'height'		=> '400'
				);

			echo form_fckeditor($data);
		}
		?>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" class="btn primary"><?=__('Save')?></button>
			<br clear="all" />
		</div>
		<br clear="all" />
	</form>
</div>