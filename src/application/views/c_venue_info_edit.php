<a href="<?= site_url('module/editByController/about/venue/'.$venue->id.'/0') ?>" class="edit btn" style="float:right;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a>
<form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<h3><?= __('Edit Info')?></h3>
<!--         <?php if(isset($languages) && !empty($languages)) : ?>
        <?php foreach($languages as $language) : ?>
        <p <?= (form_error("w_name_".$language->key) != '') ? 'class="error"' : '' ?>>
            <label for="w_name">Name <?= '(' . $language->name . ')'; ?>:</label>
			<?php $trans = _getTranslation('venue', $venue->id, 'name', $language->key); ?>
            <textarea name="w_name_<?php echo $language->key; ?>" id="name"><?= htmlspecialchars_decode(set_value('w_name_'.$language->key, ($trans != null) ? $trans : $venue->name), ENT_NOQUOTES) ?></textarea>
        </p>
        <?php endforeach; ?>
        <?php else : ?>
        <p <?= (form_error("w_name_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
            <label for="w_name">Name:</label>
            <textarea name="w_name_<?php echo $app->defaultlanguage; ?>" id="name"><?= htmlspecialchars_decode(set_value('w_name_'. $app->defaultlanguage, $venue->name), ENT_NOQUOTES) ?></textarea>
        </p>
        <?php endif; ?> -->
        <?php if(isset($languages) && !empty($languages)) : ?>
        <?php foreach($languages as $language) : ?>
		<p <?= (form_error("w_info_".$language->key) != '') ? 'class="error"' : '' ?>>
			<label for="w_info"><?= __('Info '. '(' . $language->name . ')'); ?>:</label>
			<?php $trans = _getTranslation('venue', $venue->id, 'info', $language->key); ?>
			<textarea name="w_info_<?php echo $language->key; ?>" id="w_info"><?= htmlspecialchars_decode(set_value('w_info_'.$language->key, ($trans != null) ? $trans : $venue->info), ENT_NOQUOTES) ?></textarea>
		</p>
        <?php endforeach; ?>
        <?php else : ?>
		<p <?= (form_error("w_info_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
			<label for="w_info"><?= __('Info:')?></label>
			<textarea name="w_info_<?php echo $app->defaultlanguage; ?>" id="w_info"><?= htmlspecialchars_decode(set_value('w_info_'. $app->defaultlanguage, $venue->info), ENT_NOQUOTES) ?></textarea>
		</p>
        <?php endif; ?>
		<p>
			<label for="imageurl1"><?= __('Image:')?></label>
			<span class="evtlogo" <?php if($venue->image1 != ''){ ?>style="background:transparent url('<?= image_thumb($venue->image1, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
			<input type="file" name="imageurl1" id="imageurl1" value="" class="logoupload" />
		</p><br clear="all"/>
		<?php foreach($metadata as $m) : ?>
			<?php foreach($languages as $lang) : ?>
				<?php if(_checkMultilang($m, $lang->key, $app)): ?>
					<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
						<?= _getInputField($m, $app, $lang, $languages, 'venue', $venue->id); ?>
					</p>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="btn"><?= __('Cancel')?></a>
		<button type="submit" class="btn primary"><?= __('Save Venue')?></button>
		<br clear="all">
	</div>
	<br clear="all">
</form>