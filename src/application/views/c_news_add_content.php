<div>
	<h1><?= __('Add Newsitem')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="title"><?= __('Title '.'(' . $language->name . ')'); ?>:</label>
                    <input maxlength="500" type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?=set_value('title_'.$language->key)?>" />
                </p>
                <?php endforeach; ?>
                <?php foreach($languages as $language) {
                    $data = array(
                            'name'          => 'txt_'.$language->key,
                            'id'            => 'txt_'.$language->key,
                            'toolbarset'    => 'Travelinfo',
                            'value'         => htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('txt_'.$language->key)))))),
                            'width'         => '600',
                            'height'        => '400'
                        );
                    echo '<p><label>'. __('Description'). '('.$language->name.')' .'</label>';
                    echo form_fckeditor($data);
                    echo '</p>';
                } ?>
            <?php endif; ?>
            <div class="row">
                <label for="tags"><?= __('Tags:') ?></label>
                <ul id="mytags" name="mytagsul"></ul>
            </div>
            <p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:') ?></label>
				<input type="file" name="image" id="image" value="" class="logoupload" />
			</p><br />
            <p <?= (form_error("url") != '') ? 'class="error"' : '' ?>>
                <label><?= __('Youtube video url:') ?></label>
                <span class="hintAppearance"><?= __('Please use the embed url of the video. (http://youtube.com/embed/yourvideoid)') ?></span>
                <input type="text" name="url" value="<?= set_value('url') ?>" id="url">
            </p>
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label><?= __('Order:') ?></label>
                <input type="text" name="order" value="<?= set_value('order') ?>" id="order">
            </p>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
                <a href="<?= ("javascript:history.go(-1);"); ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Add News') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $("#mytags").tagit({
            initialTags: [<?php if(set_value('item[tags][]')){
                                    foreach(set_value('item[tags][]') as $val){ 
                                        echo '"'.$val.'", '; 
                                    }
                                } elseif(isset($startingtags) && !empty($startingtags)) {
                                    foreach($startingtags as $val){ 
                                        echo '"'.$val.'", '; 
                                    }
                                } ?>],
        });

        var tags = new Array();
        var i = 0;
        <?php foreach($apptags as $tag) : ?>
        tags[i] = '<?=$tag->tag?>';
        i++;
        <?php endforeach; ?>
        $("#mytags input.tagit-input").autocomplete({
            source: tags
        });
    });
</script>