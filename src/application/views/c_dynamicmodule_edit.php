<div>
	<h1><?= __('Edit Web Module') ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><p><?= $error ?></p></div>
			<?php endif ?>
			<?php foreach($languages as $language) : ?>
	        <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
	            <label for="title"><?= __('Title') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('launcher', $launcher->id, 'title', $language->key); ?>
	            <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$language->key, ($trans != null && $trans != false) ? $trans : $launcher->title ), ENT_NOQUOTES) ?>" />
	        </p>
	        <?php endforeach; ?>
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Order:') ?></label>
                <input type="text" name="order" id="order" value="<?= htmlspecialchars_decode(set_value('order', $launcher->order), ENT_NOQUOTES) ?>" />
            </p>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="icon"><?= __('Icon:') ?></label>
				<span class="hintAppearance"><?= __('Image must be in png format, width: %s px, height: %s px',140,140) ?></span>
				<span class="evtlogo" <?php if($launcher->icon != ''){ ?>style="background:transparent url('<?= image_thumb($launcher->icon, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="icon" id="icon" value="" class="logoupload" />
				<br clear="all"/>
				<?php if(isset($venue)) : ?>
					<?php if($launcher->icon != '' && file_exists($this->config->item('imagespath') . $launcher->icon)){ ?><span><a href="<?= site_url('dynamiclauncher/removeimage/'.$launcher->id.'/venue/'.$venue->id) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
				<?php elseif(isset($event)) : ?>
					<?php if($launcher->icon != '' && file_exists($this->config->item('imagespath') . $launcher->icon)){ ?><span><a href="<?= site_url('dynamiclauncher/removeimage/'.$launcher->id.'/event/'.$event->id) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
				<?php elseif(isset($app)) : ?>
					<?php if($launcher->icon != '' && file_exists($this->config->item('imagespath') . $launcher->icon)){ ?><span><a href="<?= site_url('dynamiclauncher/removeimage/'.$launcher->id.'/app/'.$app->id) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
				<?php endif; ?>
			</p>
			<br clear="all"/>
<!--             <p <?= (form_error("youtube") != '') ? 'class="error"' : '' ?> style="min-height:0px;">
            	<input class="checkboxClass" type="checkbox" style="float:left; display:inline;" name="youtube" id="youtube" <?= $launcher->displaytype == 'youtube' ? 'checked="checked"' : '' ?> />
                <label for="youtube" style="float:left;width:450px;"><?= __('Add youtube channel') ?></label>
                <br clear="all" />
                <label id="channellabel" for="youtube" style="display:none;float:left;width:450px;"><?= __('Channel name:') ?></label>
                <input type="text" name="youtubechannel" id="youtubechannel" value="" style="display:none;"/>
            </p> -->
			<?php foreach($languages as $language) : ?>
	        <p <?= (form_error("url_".$language->key) != '') ? 'class="error"' : '' ?> id="urlp">
	            <label for="url"><?= __('Url') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('launcher', $launcher->id, 'url', $language->key); ?>
	            <input type="text" name="url_<?php echo $language->key; ?>" id="url" value="<?= htmlspecialchars_decode(set_value('url_'.$language->key, ($trans != null && $trans != false) ? $trans : $launcher->url ), ENT_NOQUOTES) ?>" />
	        </p>
	        <?php endforeach; ?>
            <p <?= (form_error("extraparams") != '') ? 'class="error"' : '' ?> id="paramsp">
            	<input class="checkboxClass" type="checkbox" style="float:left; display:inline;" name="extraparams" <?=$launcher->extragetparams ? 'checked="checked"' : ''?> />
                <label for="extraparams" style="float:left;width:450px;"><?= __('Add the appid and deviceid in the querystring of the URL') ?></label>
                <br clear="all" />
            </p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<?php if(isset($venue)) : ?>
					<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="btn"><?= __('Cancel') ?></a>
					<a href="<?= site_url('dynamiclauncher/delete/'.$launcher->id.'/'.$venue->id.'/venue') ?>" class="btn btn-danger"><?= __('Delete') ?></a>
				<?php elseif(isset($event)) : ?>
					<a href="<?= site_url('event/view/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
					<a href="<?= site_url('dynamiclauncher/delete/'.$launcher->id.'/'.$event->id.'/event') ?>" class="btn btn-danger"><?= __('Delete') ?></a>
				<?php elseif(isset($app)) : ?>
					<a href="<?= site_url('apps/view/'.$app->id) ?>" class="btn"><?= __('Cancel') ?></a>
					<a href="<?= site_url('dynamiclauncher/delete/'.$launcher->id.'/'.$app->id.'/app') ?>" class="btn btn-danger"><?= __('Delete') ?></a>
				<?php endif; ?>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
	if($('#youtube').is(':checked')) {
		$("#channellabel").css('display','block');
		$("#youtubechannel").css('display','block');
	}
	$('#youtube').mousedown(function() {
        if (!$(this).is(':checked')) {
            $("#channellabel").css('display','block');
            $("#youtubechannel").css('display','block');
            $("#urlp").css('display','none');
            $("#paramsp").css('display','none');
        } else {
			$("#channellabel").css('display','none');
			$("#youtubechannel").css('display','none');
			$("#urlp").css('display','block');
			$("#paramsp").css('display','block');
        }
    });
</script>
