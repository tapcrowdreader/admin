<style type="text/css">
iframe {
    margin-left:15px !important;
}
</style>
<div>
	<h1><?= __('Edit Content') ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
           
            <p <?= (form_error("text_nl") != '') ? 'class="error"' : '' ?>>
                <?php 
                $data = array(
                        'name'          => 'text_nl',
                        'id'            => 'text_nl',
                        'toolbarset'    => 'Travelinfo',
                        'value'         => htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('text_nl', $openingdelbar_nl)))))),
                        'width'         => '580px',
                        'height'        => '400'
                    );
                echo '<p style="min-height:0px;margin-top:-30px;"><label>Nederlands <span class="hintAppearance">Use shift + enter to go to insert new line</span></label></p>';
                echo form_fckeditor($data);
                ?>
            </p>


            <p <?= (form_error("text_fr") != '') ? 'class="error"' : '' ?>>
                <?php 
                $data = array(
                        'name'          => 'text_fr',
                        'id'            => 'text_fr',
                        'toolbarset'    => 'Travelinfo',
                        'value'         => htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('text_fr', $openingdelbar_fr)))))),
                        'width'         => '580px',
                        'height'        => '400'
                    );
                echo '<p style="min-height:0px;margin-top:-30px;"><label>Frans  <span class="hintAppearance">Use shift + enter to go to insert new line</span></label></p>';
                echo form_fckeditor($data);
                ?>
            </p>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('citycontent/app/'.$app->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>