<div>
	<h1><?=__('Layout questions page')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("headerHtml") != '') ? 'class="error"' : '' ?>>
				<label for="headerHtml"><?= __('Header text (HTML)') ?>:</label>
				<textarea name="headerHtml" id="headerHtml"><?=set_value('headerHtml', $data->header)?></textarea>
			</p>
			<p <?= (form_error("bodyHtml") != '') ? 'class="error"' : '' ?>>
				<label for="bodyHtml"><?= __('Body text (HTML)') ?>:</label>
				<textarea name="bodyHtml" id="bodyHtml"><?=set_value('bodyHtml', $data->body)?></textarea>
			</p>
			<p <?= (form_error("footerHtml") != '') ? 'class="error"' : '' ?>>
				<label for="footerHtml"><?= __('Footer text (HTML)') ?>:</label>
				<textarea name="footerHtml" id="footerHtml"><?=set_value('footerHtml', $data->footer)?></textarea>
			</p>
			<p <?= (form_error("css") != '') ? 'class="error"' : '' ?>>
				<label for="css"><?= __('Extra CSS') ?>:</label>
				<textarea name="css" id="css"><?=set_value('css', $data->css)?></textarea>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="javascript:history.go(-1);" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>