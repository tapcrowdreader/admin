<h1><?= $app->name ?> <?=__('App')?></h1>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<div class="eventinfo">
	<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
	<span class="info_title"><b><?= __('Name:') ?></b></span> <?= $app->name ?><br />
	<?php if(isset($flavor) && $flavor != false) : ?>
    <span class="info_title"><b><?= __('Flavor:') ?></b></span> <?= $flavor->name ?><br />
    <?php endif; ?>
    <br/>
    <div class="button_bar">
        <?php if (!$this->session->userdata('mijnevent')): ?>
	        <a href="<?= site_url('apps/delete/'.$app->id) ?>" class="delete btn btn-danger" id="deletebtn"><i class="icon-remove icon-white"></i> <span><?= __('Remove app') ?></span></a>
	        <a href="<?= site_url('news/app/'.$app->id) ?>" class="btn" id="managebtn"><span><?= __('Manage Content') ?></span></a>
		<a href="<?= site_url('inapppurchases/app/'.$app->id) ?>" class="btn" id="managebtn"><span><?= __('InAppPurchases') ?></span></a>
        <?php endif ?>
        <br clear="all"/>
	</div>
</div>
<div>
	<br />
	<a href="<?= site_url('contentmodule/add/'.$app->id) ?>" class="add btn primary" style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i> <?= __('Add Screen') ?>
	</a>
</div>
<br clear="all"/>
<div class="modules">
	<?php foreach($contentmodules as $c) : ?>
	<div class="module active">
		<a href="<?= site_url('contentmodule/view/'.$c->id) ?>" class="editlink" style="width:520px;"><span><?=$c->title?></span></a>
		<a href="<?= site_url('contentmodule/edit/'.$c->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Edit')?>" height="22px" /></a>
		<?php if($c->homemodule == 0) : ?>
		<a href="<?= site_url('contentmodule/delete/'.$c->id)?>" style="border-bottom: none;" class="deletemodule" ><img height="22px" alt="<?=__('Del')?>" src="img/icons/delete22.png"></a>
		<?php endif; ?>
	</div>
	<?php endforeach; ?>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.deletemodule').click(function(e) {
			var delurl = $(this).attr('href');
			jConfirm('<?= __('Are you sure you want to delete this module?<br />This cannot be undone!') ?>', '<?= __('Remove Entry') ?>', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					return false;
				}
			});
			return false;
		});
	});
</script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#deletebtn').click(function(event) {
			event.preventDefault();
			jConfirm('<?= __("This will <b>remove</b> this app!<br />! This cannot be undone!") ?>', '<?= __("Remove App") ?>', function(r) {
				if(r == true) {
					window.location = '<?= site_url("apps/delete/".$app->id)?>';
					return true;
				} else {
					jAlert('<?= __("App not removed!") ?>', '<?= __("Info") ?>');
					return false;
				}
			});
			return false;
		});
	});
</script>
