<h2><?= __('Oops, Something went wrong.') ?></h2>
<p><?= __('It seems that an error has occurred. This is our fault, not yours.') ?> <br/>
<?= __('The development team has been notified and will fix this error as soon as possible.') ?> <br /><br /><br /> </p>
<p>
	<a href="javascript:history.go(-1);" target="_self"><?= __('Click here to return to where you were previously.') ?></a>
</P>