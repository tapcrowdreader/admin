<link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
<script type="text/javascript">
    jQuery(function($){
      var jcrop_api, boundx, boundy;
      
        function setCoords(c) {
            jQuery('#x').val(c.x);
            jQuery('#y').val(c.y);
            jQuery('#w').val(c.w);
            jQuery('#h').val(c.h);
        }
            <?php if($newspicwidth != null && is_numeric($newspicwidth)) : ?>
                var width = <?php echo $newspicwidth; ?>;
            <?php else : ?>
                var width = 0;
                var ratio = 0;
            <?php endif; ?>
                
            <?php if($newspicheight != null && is_numeric($newspicheight)) : ?>
                var height = <?php echo $newspicheight; ?>;
                var ratio = width/height;
            <?php else : ?>
                var height = 0;
                var ratio = 0;
            <?php endif; ?>
      
        $('#target').Jcrop({
            aspectRatio: ratio,
            onChange: setCoords,
            onSelect: setCoords
        },function(){
        jcrop_api = this;
        jcrop_api.setOptions({
            minSize: [ width, height ],
            allowMove: true,
            allowResize: true,
            allowSelect: true
        });
        jcrop_api.focus();
        });
    });
</script>
<div>
	<h1><?= __('Edit picture')?></h1>
	<div class="frmsessions">
		<form name="newsform" action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <img src="<?= $this->config->item('publicupload').$newsitem->image ?>" id="target" />
			<div class="buttonbar">
                 <input type="hidden" name="x" id="x" value="" />
                 <input type="hidden" name="y" id="y" value="" />
                 <input type="hidden" name="w" id="w" value="" />
                 <input type="hidden" name="h" id="h" value="" />
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= (isset($venue) && $venue != false) ? site_url('news/venue/'.$venue->id) : (isset($app) && $app != false) ? site_url('news/app/'.$appid) : (isset($event) && $event != false) ? site_url('news/event/'.$event->id) : '' ?>" class="btn"><?= __('Cancel')?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>