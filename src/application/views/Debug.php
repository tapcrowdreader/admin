<?php

//require_once dirname(__FILE__) . '/../model/ConnectionFactory.php';

class Debug {
	private static $inob = false;
	public static $start = 0;
	private static $db = false;
    private static $ip = false;
    private static $fatalHandler = false;
    private static $taskId = false;
    private static $logInfo = false;


    public static function setLogInfo($val) {
        self::$logInfo = $val;
    }

    public static function setTaskId($taskId) {
        self::$taskId = $taskId;
    }

    public static function setFatalHandler($handler) {
        self::$fatalHandler = $handler;
    }

    public static function info($msg, $obj = false)
	{
		self::message($msg, 'info', $obj);
	}

	public static function message($msg, $category, $obj = false, $db = false, $type = "user")
    {
//        $table = 'errors';
//        if ((self::$logInfo && $db === false && ($category == 'info' || $category == 'warning' || $category == 'status'))
//        		|| (!self::$logInfo && $category == 'api')) { //logging for api
//            $db = true;
//            $table = 'info';
//        }
		// Do not add messages from website when debugging is disabled and not stiring in database
		
//		if (self::isWeb() && !self::isWebDebug() && $db === false) {
//			return;
//        }

//        if ((self::isWeb() && (
//				stripos($_SERVER['SERVER_NAME'], "local.oxynade.com") !== false ||
//				stripos($_SERVER['SERVER_NAME'], "test.mijnevent.be") !== false ||
//				getenv("SITE") === "LOCAL"
//			))  ||
//		(class_exists('Zend_Config') && Zend_Registry::get('config')->deploymentEnvironment != 'production')) {
//            $db = false;
//        }

//        /** if it's a testscript, cli and info: echo the message */
//    	if (!$db && self::isCli() && $category == "info" /** && getenv("SITE") === "LOCAL" */) {
//			echo $msg.PHP_EOL;
//		}
//		if (!$db && !self::isWeb() && ($category == "memcache" || $category == "filecache" || $category == "info")) {
//			return;
//		}

		if ($obj) {
			$msg .= '<br>' . Debug::dump($obj, false);
        }
//		self::log($type, $category, $msg, false, false, debug_backtrace(), false, $db, $table);
	}

	public static function warn($msg, $obj = false)
	{
		self::message($msg, 'warning', $obj, true);
	}

	public static function error($msg, $obj = false)
	{
		self::$errorCount++;
		self::message($msg, 'error', $obj, true);
	}

	private static $running = false;
	private static $logs = array();
	private static $errorCount = 0;

	public static function resetLogs() {
		self::$logs = array();
        self::$errorCount = 0;
	}

    public static function isWebDebug() {
        $ret = self::isWeb() && !empty($_REQUEST['debugbar']) && $_REQUEST['debugbar'] == 1;
        if ($ret && getenv("SITE") != "LOCAL") {
            if ($_SERVER['PHP_AUTH_USER'] != 'debug' || $_SERVER['PHP_AUTH_PW'] != 'coupure?2008') {
                header('WWW-Authenticate: Basic realm="Oxynade"');
                header('HTTP/1.0 401 Unauthorized');
                echo 'Goodbye!';
                die();
            }
        }
        return $ret;
	}

	private static $errorTypes = array(
		0 => 'E_UNKNOWN',
		1 => 'E_ERROR',
		2 => 'E_WARNING',
		4 => 'E_PARSE',
		8 => 'E_NOTICE',
		16 => 'E_CORE_ERROR',
		32 => 'E_CORE_WARNING',
		64 => 'E_COMPILE_ERROR',
		128 => 'E_COMPILE_WARNING',
		256 => 'E_USER_ERROR',
		512 => 'E_USER_WARNING',
		1024 => 'E_USER_NOTICE',
		2048 => 'E_STRICT',
		4096 => 'E_RECOVERABLE_ERROR',
		8191 => 'E_ALL',
	);

	public static function getError($type)
	{
		return isset(Debug::$errorTypes[$type]) ? Debug::$errorTypes[$type] : Debug::$errorTypes[0];
	}

	public static function start() {
		error_reporting(E_ALL - E_NOTICE);
		self::getIP();
		self::$running = true;
		self::$start = microtime(true);
		set_error_handler(array("Debug", "_handleError"), E_ALL - E_NOTICE);
		set_exception_handler(array("Debug", "_handleException"));
		register_shutdown_function(array("Debug", "_handleShutdown"));
		self::info("Before start was already running for " . (self::$start - $_SERVER['REQUEST_TIME']) . "s on " . $_SERVER["SERVER_ADDR"]);
	}

	public static function isAjax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }

	public static function stop() {
	}

	public static function _handleShutdown() {
    	if (self::$running) {
			$error = error_get_last();
			if (is_array($error)) {
				ini_set('memory_limit', '1024M');
				$errno = $error['type'];
				self::$inob = true;
				self::_handleError($error['type'], $error['message'], $error['file'], $error['line'], true);
				self::stop();
				self::$inob = false;
				if ($errno == E_ERROR || $errno == E_PARSE || $errno == E_COMPILE_ERROR || $errno == E_CORE_ERROR) {
					echo self::getFatal();
				}
            }
            $debugView = false;
            if (self::isWeb()) {
                if ((microtime(true) - self::$start) > 20) {
					Debug::error("Page load time > 20 seconds (" . (microtime(true) - self::$start) . " seconds)", 'error', false, true, "pageload");
				} else if ((microtime(true) - self::$start) > 5) {
					Debug::warn("Page load time > 5 seconds (" . (microtime(true) - self::$start) . " seconds)", 'error', false, true, "pageload");
				}
			}
            if (self::isWebDebug() && !self::isAjax() && !preg_match('/.*\.js.*$/', $_SERVER['REQUEST_URI'])) {
                $debugView = Debugger::render();
		echo $debugView;
            }
    	}
	}

	private static function getFatal() {
		$logs = array_merge(array(), self::$logs);
		$logs = array_reverse($logs);

		$debug = "";
		foreach($logs as $l) {
			$debug .= "<b>[" . $l["type"] . ":" . $l['category'] . "] " . $l['location'] . " " . $l['file'] .  "</b>";
			$debug .= '<div style="margin-left: 50px">' . "\n";
			$debug .= "<p>" . $l['error'] . "</p>";
			foreach($l['trace'] as $t)
				$debug .= "<small>" . $t["location"] . "</small><br />";
			$debug .= '</div>';
		}
		if (self::isCli()) {
			return $debug;
        	}
        	$text = self::$fatalHandler === false ? self::$fatalErrorText : call_user_func(self::$fatalHandler);
		return str_replace("__DEBUG__", (self::isWebDebug() ? $debug : ''), $text);
	}

	public static function getIP() {
		if (self::$ip === false) {
			$ip = 'unknown';
			if (getenv("HTTP_CLIENT_IP") && (strcasecmp(getenv("HTTP_CLIENT_IP"), 'unknown') != 0)) {
				$ip = getenv("HTTP_CLIENT_IP");
			} elseif (getenv("HTTP_X_FORWARDED_FOR") && (strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), 'unknown') != 0)) {
				$ip = getenv("HTTP_X_FORWARDED_FOR");
			} else {
				$ip = getenv("REMOTE_ADDR");
			}
			self::$ip = $ip;
			self::$ip = substr($ip, 0, 19);
		}
		return self::$ip;
	}

	public static function isCli() {
		return (!isset($_SERVER['HTTP_HOST']) || !isset($_SERVER['SERVER_NAME']));
	}

	public static function isWeb() {
		return !self::isCli();
	}

	private static function log($type, $category, $message, $file, $line, $trace, $fatal = false, $db = true, $loggingTable = 'errors') {
			if (is_array($trace))
				$trace = Debug::cleanBacktrace($trace);

			$location = $file === false ? '' . ':' . $line : basename($file) . ':' . $line;
			if (!empty($trace))
			{
				$location = $trace[0]['location'];
				if ($file === false || $line === false)
				{
					$file = $trace[0]['file'];
					$line = $trace[0]['line'];
				}
			}

			$error = array(
				'dateadd'	=> time(),
				'microtime'	=> microtime(true),
				'location'	=> $location,
				'type'		=> $type,
				'category'	=> $category,
				'error'		=> $message,
				'file'		=> $file,
				'host'      => $_SERVER['SERVER_NAME'],
				'line'		=> intval($line),
				'trace'		=> $trace,
				'fatal'		=> $fatal ? 1 : 0,
				'script'    => $_SERVER['PHP_SELF'],
				'remote_ip'	=> self::getIP(),
				'ip'		=> $_SERVER['SERVER_ADDR'],
				'userid'    => !empty($_SESSION["userid"]) ? intval($_SESSION["userid"]) : '-1',
				'url'		=> (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : ''),
				'task_id'   => 0,
			);
			if (self::$taskId)
				$error['task_id'] = self::$taskId;

			if (count(self::$logs) >= 500)
				self::$logs = array_slice(self::$logs, count(self::$logs) - 100);
			self::$logs[] = $error;

			if (self::isCli() && !self::$inob) {
				echo "[debug:$category:$location] $message\n";
			}

			if ($db && getenv("SITE") != "LOCAL") {
				if (self::$db === false || !is_resource(self::$db) || !mysql_ping(self::$db)) {
					self::$db = mysql_connect(ConnectionFactory::getIp('log'), "logging", "triplex?!7453");
					mysql_select_db(Servers::getCustomValueByName('loggingdbname'), self::$db);
				}
				$inserts = array();

				foreach($error as $k => $v) {
					if ($k == 'dateadd') {
						$v = 'NOW()';
					} elseif ($k == 'trace') {
						$str = '';
						if (is_array($v)) {
							foreach($v as $t) {
								$str .= $t['location'] . "\n";
							}
						}
						$v = "'" . mysql_escape_string($str) . "'";
					} else {
						$v = "'" . mysql_escape_string($v) . "'";
					}
					$inserts[] = "`$k` = " . $v;
				}
				if (mysql_query("INSERT INTO $loggingTable SET " . implode(', ', $inserts),  self::$db) === false) {
					echo "[error] " . mysql_error(self::$db) . "<br>";
				}
			}
	}

	public static function getLogs() {
		return self::$logs;
	}

	public static function getErrorCount() {
		return self::$errorCount;
	}

    public static function _handleError($errno , $errstr, $errfile, $errline, $errcontext) {
		// Skip errors prefixed with an @ symbol
		if ((error_reporting() === 0 && $errno == E_WARNING) || $errno == E_NOTICE) {
			return true;
		}
		self::$errorCount++;

		$trace = debug_backtrace();
		$fatal = $errno == E_ERROR || $errno == E_PARSE || $errno == E_COMPILE_ERROR || $errno == E_CORE_ERROR;
//		self::log("php", self::getError($errno), $errstr, $errfile, $errline, $trace, $fatal);
		return true;
	}

    public static function _handleException($ex) {
//		self::log("exception", get_class($ex), $ex->getMessage(), $ex->getFile(), $ex->getLine(), $ex->getTrace(), true);
        if (self::isWeb())
            header('HTTP/1.1 500 Internal Server Error');
		echo self::getFatal();
		return null;
	}

	public static function cleanBacktrace($trace, $filterClasses = array('ADOConnection', 'DooSqlMagic', 'Zend_Db_Table_Abstract', 'App_Db_Profiler', 'Zend_Db_Adapter_Pdo_Mysql', 'Zend_Db_Adapter_Pdo_Abstract', 'Zend_Db_Adapter_Abstract', 'Zend_Db_Statement', 'Zend_Db_Select', 'Zend_Db_Table_Row_Abstract'), $filterFunctions = array('logging', 'reportStatus')) {
		$ret = array();
		$lineo = 0;
		foreach ($trace as $item)
		{
			if (!empty($item['class']) && ($item['class'] == 'Debug') || (!empty($item['class']) && $item['class'] == '' && $item['function'] == 'Debug') || (!empty($item['class']) && in_array($item['class'], $filterClasses)))
			{
			} elseif(!empty($item['function']) && in_array($item['function'], $filterFunctions)){}
			else
            {
                $prefix = basename($item['file']) == $item['class'] . ".php"  || !isset($item['class']) ? "" : basename($item['file']) . " ";
				$ret[] = array(
		    			'location' => $prefix . ((isset($item['class']) && $item['class']) == '' ? basename($item['file']) . '$' : $item['class']) . (!empty($item['type']) ? $item['type'] : '') . $item['function'] . '():' . $lineo,
		    			'file' => isset($item['file']) ? $item['file'] : '',
		    			'line' => $lineo,
		    			'class' => !empty($item['class']) ? $item['class'] : '',
		    			'type' => isset($item['type']) ? $item['type'] : '',
		    			'function' => isset($item['function']) ? $item['function'] : '',
				);
			}
			$lineo = isset($item['line']) ? $item['line'] : 0;
        }
        if (empty($ret) && !empty($filterClasses)) {
            return self::cleanBacktrace($trace, array());
        }
		return $ret;
	}

	public static function dump($var, $echo = true, $fontsize= '9pt')
	{
		$ret = '<div style="font-size: '.$fontsize.'; font-family: monospace">' . self::_dump($var) . "</div>";
		if (self::isCli())
			$ret = strip_tags($ret);

		if ($echo)
		{
			if (self::isWebDebug())
				echo $ret;
			return true;
		}
		else
		{
			return $ret;
		}
	}

	public static $DUMP_MAX_WIDTH = 50;
	public static $DUMP_MAX_DEPTH = 10;

	private static function _dump($var, $depth = 1)
	{
		if (is_null($var))
		{
			return '<b>null</b>';
		}
		elseif (is_object($var) || is_array($var))
		{
			$ret = is_object($var) ? "<b>object</b>(<i>" . get_class($var) . "</i>)" : "<b>array</b>(" . (count($var) == 0 ? 'empty' : count($var)) . ")";
			$vars = is_object($var) ? get_object_vars($var) : $var;
			if (count($vars) > 0)
			{
				$ret .= "\n<ul style=\"list-style-type: none; padding-left: 20px; margin: 0px\">\n";
				$i = 0;
				foreach ($vars as $k => $v)
				{
					if ($i++ > self::$DUMP_MAX_WIDTH)
					{
						$ret .= "	<li>...</li>";
						break;
					}
					$kh = is_int($k) ? $k : '"' . htmlspecialchars($k) . '"';
					$ret .= "	<li>" . $kh . " <font color=\"#888a85\">=></font> " . ($depth > self::$DUMP_MAX_DEPTH ? '...' : '<span style="">' . self::_dump($v, $depth + 1) . '</span>') . "</li>\n";
				}
				$ret .= "</ul>\n";
			}
			return $ret;
		}
		elseif (is_float($var))
		{
			return '<i>(float)</i> <font color="green">' . $var . "</font>";
		}
		elseif (is_int($var))
		{
			return '<i>(int)</i> <font color="green">' . $var . "</font>";
		}
		elseif (is_string($var))
		{
			return '<font color="#cc0000">"' . htmlspecialchars($var) . '"</font>';
		}
		elseif (is_bool($var))
		{
			return '<font color="blue"><b>' . ($var ? 'true' : 'false') . '</b></font>';
		}
		else
		{
			return $var; //WTF??
		}
	}

	private static $fatalErrorText = '
        <html>
            <body>
                                <p>
                                    Something unexpected has just happened!
                                    If this problem would persist, then please contact us.
                                </p>
                                <p>
                                    <a href="/"> Go to homepage</a>
                                </p>
                                __DEBUG__
            </body>
        </html>';
}
