<div>
    <?php if(isset($currentSource)): ?>    
	<h1><?= __('Edit RSS Feed') ?></h1>
            <a href="<?= site_url('news/importrss/'.$appid) ?>" class="add btn primary" style="position: relative; top: -35px;"><img src="img/icons/add.png" width="16" height="16" alt="TapCrowd App"> <?= __('Add Rss feed')?></a>        
        <?php else: ?>
        <h1><?= __('Add RSS Feed') ?></h1>
        <?php endif; ?>
	<div class="frmsessions">
        <?php if($this->session->flashdata('error') != ''): ?>
        <div class="error"><?= $this->session->flashdata('error'); ?></div>
        <?php endif ?>
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          
                <p <?= form_error("url"); ?>>
                    <label for="url"><?= __('RSS Feed Url :')?></label>
                    <input type="text" name="url" id="url" value="<?php echo $currentSource->url; ?>" />
                </p>
                <p <?= (form_error("refreshrate") != '') ? 'class="error"' : '' ?>>
                    <label for="refreshrate"><?= __('Refresh rate (minutes) :')?></label>
                    <input type="text" name="refreshrate" id="refreshrate" value="<?php echo $currentSource->refreshrate; ?>" />
                </p>
                <p <?= (form_error("tag") != '') ? 'class="error"' : '' ?>>
                    <label for="tag"><?= __('Assign following tag(s) to all articles in this feed:')?></label>
                    <input type="text" name="tag" id="tag" value="<?=set_value('tag')?>" />
                </p>                
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
                <?php if(isset($currentSource)): ?>
				<button type="submit" class="btn primary"><?= __('Update')?></button>                                
                <?php else: ?>
				<button type="submit" class="btn primary"><?= __('Save')?></button>
                <?php endif; ?>
				<br clear="all" />
			</div>
				<br clear="all" />
		</form>
	</div>
    
    <?php if($currentRSSfeeds != null && !empty($currentRSSfeeds)): ?>
	<h1><?= __('RSS Feeds') ?></h1>
	<div id="listview_wrapper" class="dataTables_wrapper">
        <table id="listview" class="display">
			<?php if ($currentRSSfeeds != FALSE): ?>
            <thead>
                <tr>
                    <th colspan="3" class="data">
                        <?= __('Url')?>
                    </th>
                </tr>
            </thead>
            <tbody>
			<?php foreach($currentRSSfeeds as $feed): ?>
                <tr>
                    <td><?= $feed->url; ?></td>
                    <td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/editRss/" . $feed->id . "/".$appid) ?>' class="adelete"><img src="img/icons/pencil.png" width="16" height="16" title="Edit" alt="Edit" class="png" /></a></td>                    
                    <td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/deletesource/" . $feed->id) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="Delete" alt="Delete" class="png" /></a></td>
                </tr>
			<?php endforeach ?>
            </tbody>
			<?php else: ?>
			<?php endif ?> 
        </table>
	</div>
    <?php endif; ?>
</div>