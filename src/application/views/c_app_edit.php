<div>
	<h1><?=__('Edit mobile app')?></h1>
	<div class="tabs-below">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="apps/edit/<?=$app->id?>">Basic Settings</a>
			</li>
			<li>
				<a href="apps/appstoresettings/<?=$app->id?>">Appstore Settings</a>
			</li>
		</ul>
	</div>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
<!-- 			<p>
				<label for="flavor"><?= __('Flavor') ?></label>
				<select name="flavor" id="flavor" disabled>
					<?php foreach ($flavors as $flavor): ?>
						<option value="<?= $flavor->id ?>" <?= set_select('flavor', $flavor->id, ($flavor->id == $app->apptypeid ? TRUE : '')) ?>><?= $flavor->name ?></option>
					<?php endforeach ?>
				</select>
			</p> -->
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label for="name"><?= __('Name:') ?></label>
				<input type="text" name="name" id="name" value="<?= htmlspecialchars_decode(set_value('name', $app->name), ENT_NOQUOTES) ?>" />
			</p>
			<p <?= (form_error("bundle") != '') ? 'class="error"' : '' ?>>
				<label for="bundle"><?= __('Bundle:') ?></label>
				<input type="text" name="bundle" id="bundle" value="<?= htmlspecialchars_decode(set_value('bundle', $app->bundle), ENT_NOQUOTES) ?>" readonly="true" />
			</p>
			<p <?= (form_error("info")) ? 'class="error"' : '' ?>>
				<label for="info"><?= __('Info:') ?></label>
				<textarea name="info" id="info" ><?= htmlspecialchars_decode(set_value('info', $app->info)) ?></textarea>
			</p>
			<p <?= (form_error("urlname") != '') ? 'class="error"' : '' ?>>
				<label for="urlname" style="margin-bottom:5px;"><?= __('URL mobile site:') ?></label>
				<label><span style="width:auto;">http://</span><input type="text" name="urlname" id="urlname" value="<?= set_value('urlname', $app->subdomain) ?>" style="width:100px;"><span style="width:auto;">.m.tap.cr</span></label>
			</p>
			<p <?= (form_error("cname") != '') ? 'class="error"' : '' ?>>
				<label for="cname"><?= __('Mobile site CNAME:') ?></label>
				<input type="text" name="cname" id="cname" style="float:none;" value="<?= set_value('cname', $app->cname, 'http://m.mydomain.com') ?>"><br clear="all" />
			</p>
            <p style="min-height: 10px; padding-bottom:0px;">
                <label for="languages" style="vertical-align: top;"><?= __('Languages:') ?></label>
			</p>
			<p style="padding-top:0px;">
			<?php foreach($languages as $language) : ?>
				<?php if(in_array($language->key, $applanguages)) : ?>
				<input class="checkboxClass" style="float:none; display:inline; width:15px; margin-right:5px;" type="checkbox" name="language[]" checked="checked" value="<?php echo $language->key; ?>" <?= ($app->defaultlanguage == $language->key) ? 'disabled="disabled"' : '' ?> /> <?php echo $language->name; ?> <br />
				<?php else : ?>
				<input class="checkboxClass" style="float:none; display:inline; width:15px; margin-right:5px;" type="checkbox" name="language[]" value="<?php echo $language->key; ?>" /> <?php echo $language->name; ?> <br />
				<?php endif; ?>
			<?php endforeach; ?>
			</p>

			<h3><?= __('Redirect script (from website to mobile web app)') ?></h3>
			<p>
				 <label for="script" style="vertical-align: top;"><?= __('Script for mobile site:') ?></label>
				 <span class="hintAppearance"><?= __('You can copy/paste this script in your website to redirect visitors who are using a mobile device to your mobile website. (PHP)') ?><br/></span>
				 <textarea name="scriptphp" id="scriptphp" ><?=$scriptphp?></textarea>
				 <br /><br/>
				 <span class="hintAppearance"><?= __('This is the same script but in javascript. You can copy paste this into your website.') ?><br/></span>
				 <textarea name="scriptjs" id="scriptjs" ><?=$scriptjs?></textarea>
			</p>

			<h3><?= __('Smart App Banner (to download your native app)') ?></h3>
			<p>
				 <label for="script" style="vertical-align: top;"><?= __('Script which shows a popup on mobile devices to install or open native app (no redirect):') ?></label>
				 <span class="hintAppearance"><?= __('In head of your site:') ?></span>
				 <textarea name="popupscriptmeta" id="popupscriptmeta" ><?=$popupscript['meta']?></textarea>
				 <span class="hintAppearance"><?= __('On the bottom of the body of your site:') ?></span>
				 <textarea name="popupscriptjs" id="popupscriptjs" ><?=$popupscript['js']?></textarea>
			</p>
			<br />

			<?php if(_isAdmin()) : ?>
			<p>
				<input type="checkbox" name="externalids" <?= $app->show_external_ids_in_cms == 1 ? 'checked="checked"' : '' ?> style="width:25px;" /> <?= __('Show external ids in CMS')?>
			</p>
            <?php endif; ?>
            <p>
            	<label><?= __('Launcher type') ?>:</label>
	            <select name="launcherview">
	            	<option value="" <?= $app->launcherview == '' ? 'selected="selected"' : '' ?>><?= __('Default, with big icons') ?></option>
	            	<option value="list" <?= $app->launcherview == 'list' ? 'selected="selected"' : '' ?>><?= __('List with small icons') ?></option>
	            </select>
	        </p>
			<p <?= (form_error("apikey") != '') ? 'class="error"' : '' ?>>
				<label for="apikey"><?= __('Your API key:') ?> <br /> <?=$app->token?></label>
			</p>
			<?php if(_isAdmin()) : ?>
			<p>
				<label for="certificate"><?= __('Certificate:') ?></label>
				<input type="file" name="certificate" id="certificate" value="" class="logoupload" /><br clear="all"/>
			</p>
			<p>
				<label for="environment"><?= __('Environment:') ?></label>
				<select name="environment">
					<option value="live">Live</option>
					<option value="tapcrowd_demo">Demo</option>
				</select>
			</p>
			<?php endif; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('apps/view/'.$app->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
	