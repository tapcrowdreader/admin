<h2><?= __('Clone Application'); ?></h2>
<form id="formSelectApp" name="formSelectApp" action="<?= site_url($this->uri->uri_string()) ?>/cloneApp" method="POST" accept-charset="utf-8">
    <?php if ($this->session->flashdata('direrror')==1): ?>
        <div class="error" style="padding: 10px 10px 12px 35px;margin-bottom: 20px;"><?php echo $this->session->flashdata('direrrormsg'); ?></div>
    <?php endif; ?>
    <?php if ($this->session->flashdata('imageerror')==1): ?>
        <div class="error" style="padding: 10px 10px 12px 35px;margin-bottom: 20px;"><?php echo $this->session->flashdata('imageerrormsg'); ?></div>
    <?php endif; ?>        
    <?php if ($this->session->flashdata('nameerror') == 1): ?>
        <div class="error" style="padding: 10px 10px 12px 35px;margin-bottom: 20px;"><?= __('Some fields are missing.'); ?></div>
    <?php endif; ?>
    <?php if ($this->session->flashdata('successmessage') == 1): ?>
        <div class="feedback fadeout" style="display: block;"><?= __('Your changes have successfully been saved.'); ?></div>
    <?php endif; ?> 
    <div id="importprogress" style="display:none;" class="importprogress"><br /></div>    
    <fieldset>
        <p style="padding-bottom:5px;"><label for="NewApp"><?= __('Type the name of the new application:'); ?></label></p>
        <p class="<?php if ($this->session->flashdata('nameerror') == 1): echo 'erro';endif; ?>"><input name="newappname" id="newappname" ></p>
        <?php if(_isAdmin()== TRUE): ?>
        <p style="padding-bottom:5px;padding-top:10px;"><label for="appname"><?= __('Select an owner:'); ?></label></p>
        <select name="organizerid" id="organizerid">
            <?php foreach ($organizer as $key => $value) : ?>
                <?php if(!empty($value)): ?>
                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                <?php endif; ?>    
            <?php endforeach; ?>
        </select>
        <?php endif; ?>
        <p style="padding-bottom:5px;padding-top:10px;"><label for="appname"><?= __('Select an application to clone:'); ?></label></p>
        <select name="appid" id="appid">
            <?php foreach ($dropdown as $key => $value) : ?>
                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
            <?php endforeach; ?>
        </select>
        <button type="button" value="Clone" class="btn primary" id="clonebutton" onclick="submit_import()"><?= __('Clone Application'); ?></button>
    </fieldset>
</form>
<script language="javascript">
function submit_import()
{
 document.getElementById("importprogress").style.display = "block";
 document.getElementById("clonebutton").disabled = true;
 document.forms.formSelectApp.submit();
}
</script>