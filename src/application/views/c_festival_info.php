<div id="metadatadiv">
	<h1><?=__('Info')?></h1>
	<div class="frmsessions">
        <form action="<?= 'metadata/add' ?>" id="formMeta" name="formMeta" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
			<p style="width:300px;">
                <label style="float: left; width: 40px;"><?= __('Title:') ?></label>
                <input type="text" name="key" value="" id="key" style="float: left; width: 230px; margin-right: 10px;"/>
                <br clear="all"/>
                <label style="float: left; width: 40px;"><?= __('Text:') ?></label>
                <textarea name="value" value="" id="value" style="float: left; width: 230px;"></textarea>
                <br clear="all"/>
				<button type="submit" class="btn" style="float:right;margin-right:8px;"><?= __('Add') ?></button>
				<br clear="all" />
            </p>
            <?php if(isset($metadata)) : ?>
            <?php foreach($metadata as $data) : ?>
				<p style="height:auto;">
                <label id="lblkey<?=$data->id ?>" name="lblkey<?=$data->id ?>" style="float: left; width: 280px; margin-right: 10px;">
                    <?php echo $data->key; ?>: 
                </label>
                <label id="lblvalue<?=$data->id ?>" name="lblvalue<?=$data->id ?>" style="text-align:left; float: left; width: 280px; margin-right: 10px;">
                    <?php echo nl2br($data->value); ?><br class="clear" />
                </label>
				<input type="text" value="<?=$data->key?>" id="txtkey<?=$data->id ?>" name="key<?=$data->id ?>" style="float: left; width: 270px; display:none; margin-right: 10px;"/>
				<textarea class="txtvalue" id="txtvalue<?=$data->id ?>" name="value<?=$data->id ?>" style="float: left; width: 270px; display: none; margin-right: 10px;"><?=$data->value?></textarea>
				<a href='' id="<?=$data->id ?>" class="aedit metaEdit"><img id="imgEdit<?=$data->id ?>" src="img/icons/pencil.png" width="16" height="16" title="<?=__('Edit')?>" alt="<?=__('Edit')?>" class="png" /></a>
                <a href='<?= site_url("metadata/delete/event/".$data->id) ?>' id='<?=$data->id?>' class="adelete metaDelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a>
				<br class="clear" />
				</p>
				<br clear="all" />
            <?php endforeach; ?>
            <?php endif; ?>
            <p>
            	<br clear="all"/>
            </p>
        </form>
	</div>
</div>
<script type="text/javascript">
  $("#formMeta").submit(function(event) {
		event.preventDefault(); 
	    $.post('metadata/add/'+<?=$event->id?>+'/event', $("#formMeta").serialize(),
	      function( data ) {
	          	
				$('#metadatadiv').html(data);
	      }
	    );
	});

   $(".adelete").click(function(event) {
		event.preventDefault(); 
	    $.post('metadata/delete/event/'+<?=$event->id?>+'/'+this.id, '',
	      function( data ) {
				$('#metadatadiv').html(data);
	      }
	    );
	});

	$(".metaEdit").click(function(event){
		event.preventDefault();
		var id = $(this).attr("id");
		editMetadata(id);
	});

	function editMetadata(id) {
		if($('#imgEdit' + id).attr('src') == 'img/icons/accept.png') {
			$.post("<?= 'metadata/edit/'?>"+id, { id: id, key: $('#txtkey'+ id).val(), value: $('#txtvalue'+ id).val(), metadata: 'metadata'}, function(data) {
				$('#lblkey'+ id).text($('#txtkey'+ id).val());
				$('#lblvalue'+ id).html($('#txtvalue'+ id).val().replace(/\n/g, '<br/>'));
				$('#lblkey'+ id).css('display', '');
				$('#lblvalue'+ id).css('display', '');

				$('#txtkey'+ id).css('display', 'none');
				$('#txtvalue'+ id).css('display', 'none');
				$('#imgEdit' + id).attr('src', 'img/icons/pencil.png');
			});
		} else {
			$('#lblkey'+ id).css('display', 'none');
			$('#lblvalue'+ id).css('display', 'none');

			$('#txtkey'+ id).css('display', '');
			$('#txtvalue'+ id).css('display', '');
			$('#imgEdit' + id).attr('src', 'img/icons/accept.png');
		}
	}
</script>