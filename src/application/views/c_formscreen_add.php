<div>
	<h1><?=__('Add Screen')?></h1>
	<div class="frmformbuilder">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Title') ?> <?= '(' . $language->name . ')'; ?>:</label>
                    <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?=set_value('title_'.$language->key)?>" />
                </p>
                <?php endforeach; ?>
                
            <?php else : ?>
            
                <p <?= (form_error("name_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="title"><?= __('Title:') ?></label>
                    <input type="text" name="title_<?php echo $app->defaultlanguage; ?>" id="title" value="<?=set_value('title_'.$app->defaultlanguage)?>" />
                </p>
                
            <?php endif; ?>
			
             <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:') ?></label>
				<input type="text" name="order" value="<?= set_value('order') ?>" id="order">
			</p>
			
           
            <div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add Form Screen') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>