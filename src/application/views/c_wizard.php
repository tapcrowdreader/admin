<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>TapCrowd, Powerful App Builder</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="/creator/css/bootstrap.css" rel="stylesheet">

        <link href="/creator/css/tapcrowd-creator.css" rel="stylesheet">
        <style>
          body {
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
          }
        </style>
        <link href="/creator/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="/creator/css/datepicker.css" rel="stylesheet">
        <link href="/creator/css/jquery.dataTables.css" rel="stylesheet">
        <link rel="Stylesheet" type="text/css" href="/creator/css/jpicker-1.1.6.min.css" />
        <link rel="stylesheet" type="text/css" href="/creator/css/ui-lightness/jquery-ui-1.8.23.custom.css" />

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/creator/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/creator/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/creator/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/creator/ico/apple-touch-icon-57-precomposed.png">

        <script src="/creator/js/jquery.js"></script>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                  <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </a>
                  <a class="brand" href="#"><img src="/creator/img/logo-tapcrowd.png"/></a>
                  <div class="nav-collapse">
                  </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
		<div class="container container-inner">
		<div class="wizard-progress">
			<div class="progress progress-striped active">
				<div class="bar" style="width: 0%;"></div>
			</div>
			<div class="row-fluid">	
				<div class="span4">
					<span class="label label-inverse lblStep1">STEP 1</span><br/><?=__('Select an app type');?>
				</div>
				<div class="span4">
					<span class="label label-inverse lblStep2">STEP 2</span><br/><?=__('Configure your app');?>
				</div>
				<div class="span4">
					<div class="row-fluid">
		    			<div class="span6">
		    				<span class="label label-inverse lblStep3">STEP 3</span><br/><?=__('Almost there...');?>
		    			</div>
						<div class="span6" style="text-align: right;">
		    				<span class="label label-inverse lblStep4">STEP 4</span><br/><?=__('Confirmation');?>
		    			</div>
		    		</div>
				</div>
				
			</div>
		</div>

		<div id="wizardStep1">
			<h1><?=__('Welcome to TapCrowd Creator!');?></h1>
			<p class="lead"><?=__('Are you ready to start creating your own mobile app? Enjoy the ride because it’ll be over before you know it ;)');?></p>
			<div>
				<div class="row">
					<?php $flavorsAllowed = array(3, 10, 7, 8); ?>
					<?php foreach($flavors as $flavor) : ?>
						<?php if(in_array($flavor->id, $flavorsAllowed)) : ?>
							<div class="span3">
								<div class="thumbnail">
									<img src="http://placehold.it/260x180" alt="">
									<div class="caption">
										<h4><?= $flavor->name ?></h4>
										<p class="flavorDescriptionP"><?= $flavor->description ?><br/><br/></p>
										<p><a href="#" id="<?=$flavor->id?>" class="btn btn-primary flavorselect"><?=__("Let's Go!");?></a> <a href="#" class="btn"><?=__('View Examples');?></a></p>
									</div>
								</div>
							</div>
					<?php endif; ?>
				<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div id="wizardStep2" style="display:none;">
			<div id="step2event" class="step2div">
				<h1><?=__('Great choice! Now give us some more details about your event.')?></h1>
				<p class="lead"><?=__('Please fill in the form below so we can tailor your app to your needs.')?></p>
				<div>
					<form class="form-horizontal" id="step2FormEvent">
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="eventName">
									<?=__('What is the name of your event?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="eventName" name="eventName" />
									<label class="checkboxClass">
										<input type="checkbox" id="sameAsAppEvent" checked="checked" /> <?=__('Same as app name');?>
									</label>
								</div>
							</div>
							<div class="control-group" id="appNameEventDiv">
								<label class="control-label" for="appNameEvent">
									<?=__('What is the name of your app?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="appNameEvent" name="appNameEvent" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="date1Event">
									<?=__('When will it be taking place?');?>
								</label>
								<div class="controls">
									<input id="date1Event" class="span2" type="text" name="date1Event" value="<?=date("d")?>-<?=date("m")?>-<?=date("Y")?>"> <?=__('untill');?> 
									<input id="date2Event" class="span2" type="text" name="date2Event" value="<?=date("d")?>-<?=date("m")?>-<?=date("Y")?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="addressEvent">
									<?=__('Where will it be taking place?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="addressEvent" name="addressEvent"/>
									<div id="map_canvas" class="map" style="height:300px; width:610px;"></div>
								</div>
							</div>
							<div class="form-actions">
								<p><a href="#" class="btn btn-primary btnStep3 btnStep3Event"><?=__("Let's Go!");?></a></p>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
			<div id="step2festival" class="step2div">
				<h1><?=__('Great choice! Now give us some more details about your festival.')?></h1>
				<p class="lead"><?=__('Please fill in the form below so we can tailor your app to your needs.')?></p>
				<div>
					<form class="form-horizontal" id="step2FormFestival">
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="festivalName">
									<?=__('What is the name of your festival?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="festivalName" name="festivalName" />
									<label class="checkboxClass">
										<input type="checkbox" id="sameAsAppFestival" checked="checked" /> <?=__('Same as app name');?>
									</label>
								</div>
							</div>
							<div class="control-group" id="appNameFestivalDiv">
								<label class="control-label" for="appNameFestival">
									<?=__('What is the name of your app?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="appNameFestival" name="appNameFestival" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="date1Festival">
									<?=__('When will it be taking place?');?>
								</label>
								<div class="controls">
									<input id="date1Festival" class="span2" type="text" name="date1Festival" value="<?=date("d")?>-<?=date("m")?>-<?=date("Y")?>"> <?=__('untill');?> 
									<input id="date2Festival" class="span2" type="text" name="date2Festival" value="<?=date("d")?>-<?=date("m")?>-<?=date("Y")?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="addressFestival">
									<?=__('Where will it be taking place?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="addressFestival" name="addressFestival"/>
								</div>
							</div>
							<div class="form-actions">
								<p><a href="#" class="btn btn-primary btnStep3 btnStep3Festival"><?=__("Let's Go!");?></a></p>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
			<div id="step2resto" class="step2div">
				<h1>Great choice! Now give us some more details about your restaurant.</h1>
				<p class="lead">Please fill in the form below so we can tailor your app to your needs.</p>
				<div>
					<form class="form-horizontal" id="step2FormRestaurant">
							<div class="control-group">
								<label class="control-label" for="restaurantName">
									<?=__('What is the name of your restaurant?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="restaurantName" name="restaurantName" />
									<label class="checkboxClass">
										<input type="checkbox" id="sameAsAppRestaurant" checked="checked" /> <?=__('Same as app name');?>
									</label>
								</div>
							</div>
							<div class="control-group" id="appNameRestaurantDiv">
								<label class="control-label" for="appNameRestaurant">
									<?=__('What is the name of your app?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="appNameRestaurant" name="appNameRestaurant" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="addressRestaurant">
									<?=__('Where is it located?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="addressRestaurant" name="addressRestaurant"/>
								</div>
							</div>
							<div class="form-actions">
								<p><a href="#" class="btn btn-primary btnStep3 btnStep3Restaurant"><?=__("Let's Go!");?></a></p>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
			<div id="step2shop" class="step2div">
				<h1>Great choice! Now give us some more details about your shop.</h1>
				<p class="lead">Please fill in the form below so we can tailor your app to your needs.</p>
				<div>
					<form class="form-horizontal" id="step2FormShop">
							<div class="control-group">
								<label class="control-label" for="shopName">
									<?=__('What is the name of your shop?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="rshopName" name="shopName" />
									<label class="checkboxClass">
										<input type="checkbox" id="sameAsAppShop" checked="checked" /> <?=__('Same as app name');?>
									</label>
								</div>
							</div>
							<div class="control-group" id="appNameShopDiv">
								<label class="control-label" for="appNameShop">
									<?=__('What is the name of your app?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="appNameShop" name="appNameShop" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="addressShop">
									<?=__('Where is it located?');?>
								</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="addressShop" name="addressShop"/>
								</div>
							</div>
							<div class="form-actions">
								<p><a href="#" class="btn btn-primary btnStep3 btnStep3Shop"><?=__("Let's Go!");?></a></p>
							</div>
					</form>
				</div>
			</div>
		</div>
		<div id="wizardStep3" style="display:none;">
			<h1>You're almost there, just some more basic details.</h1>
			<p class="lead">Are you ready to start creating your own mobile app? Enjoy the ride because it’ll be over before you know it ;)</p>
			<div>
				<form class="form-horizontal" id="step3Form">
						<div class="control-group">
							<label class="control-label" for="mobileurl">
								<?=__('What should your mobile website url be?');?>
							</label>
							<div class="controls">
								http://<input type="text" class="input-xlarge" id="mobileurl" name="mobileurl" />.m.tap.cr
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="languages">
								<?= __('Languages') ?>
							</label>
							<div class="controls">
				                <?php foreach($languages as $language) : ?>
									<label class="checkboxClass">
										<input class="languageCheckbox" type="checkbox" name="language[]" value="<?= $language->key; ?>" /> <span><?=$language->name;?></span>
									</label>
								<?php endforeach; ?>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="defaultlang">
								<?= __('Default language:') ?>
							</label>
							<div class="controls">
								<select name="defaultlang" id="defaultlang">

								</select>
							</div>
						</div>
						<div class="form-actions">
							<p><a href="#" class="btn btn-primary btnStep4"><?=__("Let's Go!");?></a></p>
						</div>
				</form>
			</div>
		</div>
		<div id="wizardStep4" style="display:none;">
			<h1>Look at that! Your app has been created!</h1>
			<p class="lead">But hold on, there's still some work to be done.</p>
			<p>Click the button below to enter the TapCrowd Creator platform and start filling your app with content. You can log in to the TapCrowd Creator at any time to add, change or remove content, change the appearance of your app and after submission check the analytics of your app.</p>
			<p>So what are you waiting for? Go ahead, click that blue shiny button and enter TapCrowd Creator, the app building walhalla!</p>
			<div>
				<p><a href="http://<?=$_SERVER['SERVER_NAME']?>" class="btn btn-primary" id="btnFinish">Let's go crazy!</a></p>
			</div>
		</div>
	</div> <!-- /container -->

	<form id="creatorform" action="create" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
	<input type="hidden" name="creatorform" id="creatorformField" value="" />
	<input type="hidden" name="selectedFlavorid" id="selectedFlavorid" value="" />
	<input type="hidden" name="selectedAppName" id="selectedAppName" value="" />
	<input type="hidden" name="selectedEventName" id="selectedEventName" value="" />
	<input type="hidden" name="selectedVenueName" id="selectedVenueName" value="" />
	<input type="hidden" name="selectedStartdate" id="selectedStartdate" value="" />
	<input type="hidden" name="selectedEnddate" id="selectedEnddate" value="" />
	<input type="hidden" name="selectedAddress" id="selectedAddress" value="" />
	<input type="hidden" name="selectedLat" id="selectedLat" value="" />
	<input type="hidden" name="selectedLon" id="selectedLon" value="" />
	<input type="hidden" name="selectedMobileurl" id="selectedMobileurl" value="" />
	<input type="hidden" name="selectedLanguages" id="selectedLanguages" value="" />
	<input type="hidden" name="selectedDefaultLanguage" id="selectedDefaultLanguage" value="" />
	</form>

	<script type="text/javascript">
	$(document).ready(function() {
	    geocoder = new google.maps.Geocoder();
	    var latlng = new google.maps.LatLng('50.753887', '4.269936');
	    var myOptions = {
	        zoom: 8,
	        center: latlng,
	        mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	    bounds = new google.maps.LatLngBounds();

		$('#address').keydown(function(e) {
			if(e.keyCode == 13) {
				e.preventDefault();
				return false;
				codeAddress();
			}
		});
	});
	</script>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/creator/js/jquery-ui/jquery-1.8.0.min.js"></script>
    <script src="/creator/js/jquery-ui/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/creator/js/bootstrap-transition.js"></script>
    <script src="/creator/js/bootstrap-alert.js"></script>
    <script src="/creator/js/bootstrap-modal.js"></script>
    <script src="/creator/js/bootstrap-dropdown.js"></script>
    <script src="/creator/js/bootstrap-scrollspy.js"></script>
    <script src="/creator/js/bootstrap-tab.js"></script>
    <script src="/creator/js/bootstrap-tooltip.js"></script>
    <script src="/creator/js/bootstrap-popover.js"></script>
    <script src="/creator/js/bootstrap-button.js"></script>
    <script src="/creator/js/bootstrap-collapse.js"></script>
    <script src="/creator/js/bootstrap-carousel.js"></script>
    <script src="/creator/js/bootstrap-typeahead.js"></script>
    <script src="/creator/js/bootstrap-datepicker.js"></script>
    <script src="/creator/js/jquery.dataTables.js"></script>
    <script src="/creator/js/jquery.alerts.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="/creator/js/jpicker-1.1.6.min.js"></script>
    <script src="/creator/js/tag-it.js"></script>

    <script src="/creator/js/tapcrowd-creator.js"></script>
  </body>
</html>
