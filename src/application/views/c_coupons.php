<style type="text/css">

	img.hidden {
		display: none;
		position:relative;
		left: 5%;
		top: -37.5px;
		z-index: 1;
	}

</style>

<h1><?= __('Coupons') ?></h1>
<a href="<?= site_url('module/editByController/coupons/venue/'.$venue->id) ?>" class="edit btn" style="float:right;margin-bottom:5px;margin-top:-35px;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a><br style="clear:both;"/>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>

<div>
	<br />
	<a href="<?= site_url('coupons/add/'.$venue->id) ?>" class="add btn primary" style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i>  <?= __('Add Coupon')?>
	</a>
</div>

<br clear="all" />

<div class="modules">

	<?php $i = 1; foreach($coupons as $row) : ?>
		<div class="module active">

			<a href="<?= site_url('coupons/edit/'.$row->id)?>" class="editlink">
				<?php if($row->title)
						echo $row->title;
					  else {
					 	echo "Coupon ".$i;
						$i++;
					  }
			 	?>
		 	</a>

			<a href="<?= site_url('coupons/edit/'.$row->id)?>"><img src="img/Settings.png" alt="<?= __('Edit')?>" height="22px" /></a>
			<a href="<?= site_url('coupons/delete/'.$row->id)?>" class="deletesection" ><img height="22px" alt="<?= __('Del')?>" src="img/icons/delete22.png"></a>

		</div>

	<?php endforeach; ?>

</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.deletesection').click(function(e) {
			var delurl = $(this).attr('href');
			jConfirm('<?= __('Are you sure you want to delete this Coupon?') . '<br />' . __('This cannot be undone!') ?>', '<?= __('Remove Entry') ?>', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					return false;
				}
			});
			return false;
		});
	});
</script>