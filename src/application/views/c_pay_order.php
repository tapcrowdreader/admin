<h1><?= __('Payment for ') . ' ' . $app->name ?></h1>
	<table>
		<tr>
			<th> </th>
			<th style="text-align: right;"><?= __('EUR')?></th>
		</tr>

		<tr>
			<td><?= __('Total excl. VAT')?></td>
			<td style="text-align: right;"><?= number_format($order->amountexcludingVAT,2, ',','.'); ?></td>
		</tr>

		<tr>
			<td><?= __('VAT')?> (<?php echo $order->VATpercentage*100; ?> %)</td>
			<td style="text-align: right;"><?= number_format($order->amountVAT,2, ',','.'); ?></td>
		</tr>

		<tr>
			<td><?= __('Total incl. VAT')?></td>
			<td style="text-align: right;"><?= number_format($order->amountincludingVAT,2, ',','.'); ?></td>
		</tr>

		<tr>
			<td style="font-weight:bold;"><?= __('Amount Due') ?></td>
			<td style="text-align: right;"><?= number_format($order->amountincludingVAT,2, ',','.'); ?></td>
		</tr>
	</table>
	<?php if($allowpayment) : ?>
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
		<input type="hidden" name="cmd" value="_xclick">
		<input type="hidden" name="business" value="3U3GP9D2PM2VG">
		<input type="hidden" name="lc" value="BE">
		<input type="hidden" name="item_name" value="<?= $app->name; ?>">
		<input type="hidden" name="item_number" value="APP-<?= $app->id; ?>-<?= $order->id; ?>">
		<input type="hidden" name="amount" value="<?= $order->amountincludingVAT; ?>">
		<input type="hidden" name="currency_code" value="EUR">
		<input type="hidden" name="button_subtype" value="services">
		<input type="hidden" name="no_note" value="0">
		<input type="hidden" name="cn" value="Add special instructions to the seller:">
		<input type="hidden" name="no_shipping" value="1">
		<input type="hidden" name="rm" value="1">
		<input type="hidden" name="return" value="http://<?= $_SERVER['SERVER_NAME'] ?>/pay/success/<?= $app->id ?>">
		<input type="hidden" name="cancel_return" value="http://<?= $_SERVER['SERVER_NAME'] ?>/pay/cancel/<?= $app->id ?>">
		<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
		<input style="margin-top:20px;float:right;" type="image" src="https://www.paypalobjects.com/en_US/BE/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
		<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>


		<!-- <button type="submit" name="submit" class="btn primary" style="margin-top:20px;float:right;"><?= __('Submit app') ?></button> -->
	<script type="text/javascript">
		$(document).ready(function() {
			$('.options').mousedown(function() {
				var newprice = $("#totalprice").html();
				newprice = parseInt(newprice);
		        if (!$(this).is(':checked')) {
		            newprice = newprice + parseInt($("#"+$(this).attr('id')+"_price_setup").html()) + parseInt($("#"+$(this).attr('id')+"_price_month").html());
		        } else {
					newprice = newprice - parseInt($("#"+$(this).attr('id')+"_price_setup").html()) - parseInt($("#"+$(this).attr('id')+"_price_month").html());
		        }

		        $("#totalprice").html(newprice);
		        $("#priceField").val(newprice);
		    });
		});
	</script>
	<?php else: ?>
	<a class="btn primary" style="float:right;" href="<?= site_url('pay/confirm/'.$app->id) ?>"><?= __('Confirm') ?></a>

	<?php endif; ?>