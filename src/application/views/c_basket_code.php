<div id="basketcode">
	<h1><?= __('Code of the Day')?></h1>

	<p style="text-align: center">
		<input maxlength="5" type="text" id="code" name="code" value="<?=set_value('code',$code)?>" onchange="update()"/>
	</p>
	<!-- <p align="center"><?="Click the QR Code to print it:"?></p> -->
	<!-- <div id="qrcode" align="center"></div> -->
	<p></p>
</div>

<script type="text/javascript">

function applyqr(){
	if(navigator.appName == "Microsoft Internet Explorer"){
		jQuery("#qrcode").qrcode({
				render	: "table",
				text	: document.getElementById("code").value
		});
		
		$("#qrcode table").css("cursor","pointer");
		
		$("#qrcode table").click(
			function()
			{
				var popup=window.open('/basket/printcode/<?=$venue->id?>/'+document.getElementById("code").value,
					'_blank',
				 	'height=700,width=900,left=10,top=10,resizable=no,scrollbars=no,'+
				 	'toolbar=yes,menubar=no,location=no,directories=no,status=yes');
			}
		);
	}else{
		jQuery("#qrcode").qrcode({
				text	: document.getElementById("code").value
		});
		
		$("#qrcode canvas").css("cursor","pointer");
		
		$("#qrcode canvas").click(
			function()
			{
				var popup=window.open('/basket/printcode/<?=$venue->id?>/'+document.getElementById("code").value,
					'_blank',
					'height=700,width=900,left=10,top=10,resizable=no,scrollbars=no,'+
					'toolbar=yes,menubar=no,location=no,directories=no,status=yes');
			}
		);
	}
}

function update(){
	$.post("/basket/setcode/",{'venueid': <?=$venue->id?>, 'code': document.getElementById("code").value},function(response){
	});
	jQuery("#qrcode").html("");
	applyqr()
};

applyqr();
</script>