<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		<?php if(isset($venue) && isset($launcher->venueid)) {
			$location = "venue";
			$typeid = $launcher->venueid;
		} elseif(isset($event) && isset($launcher->eventid)) {
			$location = "event";
			$typeid = $launcher->eventid;
		} elseif(isset($app) && isset($launcher->appid)) {
			$location = "app";
			$typeid = $launcher->appid;
		} ?>
			var url = '';
		<?php if(isset($location) && isset($typeid)) : ?>
			url = '<?= site_url("module/reset/".$launcher->id."/".$location.'/'.$typeid)?>';
		<?php else: //hide element?>
			$('#resetdefaults').css('display', 'none');
		<?php endif; ?>

		$('#resetdefaults').click(function() {
			jConfirm('<?= __('This will reset all your custom launcher data for the module. Are you sure?')?>', '<?= __('Reset Launcher data')?>', function(r) {
				if(r == true) {
					window.location = url;
					return true;
				} else {
					jAlert('<?= __('Data not reset!')?>', '<?= __('Info')?>');
					return false;
				}
			});
			return false;
		});
	});
</script>
<div>
	<h1><?= __('Edit Module')?></h1>
	<?php if(_isAdmin()) : ?>
		<?php if(!($launcher->moduletypeid == 21 && $app->apptypeid == 10)) : ?>
			<a href="<?= site_url('module/fields/'.$launcher->id.'/'.$parentType.'/'.$parentId) ?>" class="btn" style="float:right;margin-top:-35px;"><?= __('Custom fields') ?></a>
			<br clear="all" />
		<?php endif; ?>
	<?php endif; ?>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
	        <?php foreach($languages as $language) : ?>
	        <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
	            <label for="title"><?= __('Title '. '(' . $language->name . ')'); ?>:</label>
				<?php if($launcher->newLauncherItem) {
							$trans = _getTranslation('defaultlauncher', $launcher->id, 'title', $language->key);
						} else {
							$trans = _getTranslation('launcher', $launcher->id, 'title', $language->key);
						} ?>
	            <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value((set_value('title_'.$language->key) != '') ? 'title_'.$language->key  : $trans, ($trans != null && $trans != false) ? $trans : $launcher->title ), ENT_NOQUOTES) ?>" />
	        </p>
	        <?php endforeach; ?>
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Order:') ?></label>
                <input type="text" name="order" id="order" value="<?= htmlspecialchars_decode(set_value((set_value('order') != '') ? set_value('order') : $launcher->order, $launcher->order), ENT_NOQUOTES) ?>" />
            </p>

            <?php if($launcher->module == 'webmodule') : ?>
            <p <?= (form_error("url") != '') ? 'class="error"' : '' ?>>
                <label for="url"><?= __('Url:') ?></label>
                <input type="text" name="url" id="url" value="<?= set_value('url', $launcher->url) ?>" />
            </p>
        	<?php endif; ?>

			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:')?><a href="faq#launchericons" style="border:none;"><i class="icon-question-sign"></i></a></label>
				<span class="hintAppearance"><?= __('Image must be in png format, width: %s px, height: %s px',140,140) ?></span>
				<span class="evtlogo" <?php if($launcher->icon != ''){ ?>style="background:transparent url('<?= image_thumb($launcher->icon, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="image" id="image" value="" class="logoupload" />

				<?php if(isset($venue)) : ?>
					<?php if($launcher->icon != '' && file_exists($this->config->item('imagespath') . $launcher->icon)){ ?><span><a href="<?= site_url('module/removeimage/'.$launcher->moduletypeid.'/venue/'.$venue->id) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
				<?php elseif(isset($event)) : ?>
					<?php if($launcher->icon != '' && file_exists($this->config->item('imagespath') . $launcher->icon)){ ?><span><a href="<?= site_url('module/removeimage/'.$launcher->moduletypeid.'/event/'.$event->id) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
				<?php elseif(isset($app)) : ?>
					<?php if($launcher->icon != '' && file_exists($this->config->item('imagespath') . $launcher->icon)){ ?><span><a href="<?= site_url('module/removeimage/'.$launcher->moduletypeid.'/app/'.$app->id) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
				<?php endif; ?>
				<br clear="all"/>
			</p>
			<?php if($launcher->moduletypeid == 10) : ?>
			<p>
				<label><?= __('Display type: ')?></label>
				<select name="displaytype" id="displaytype">
					<option value="alfa" <?= ($launcher->displaytype == 'alfa') ? 'selected="selected"' : '' ?>><?= __('Alphabetic') ?></option>
					<option value="overview" <?= ($launcher->displaytype == 'overview') ? 'selected="selected"' : '' ?>><?= __('Sessiongroups in view') ?></option>
					<option value="separator" <?= ($launcher->displaytype == 'separator') ? 'selected="selected"' : '' ?>><?= __('Sessiongroups in separator') ?></option>
					<option value="location" <?= ($launcher->displaytype == 'location') ? 'selected="selected"' : '' ?>><?= __('By location') ?></option>
					<!-- <option value="tracks" <?= ($launcher->displaytype == 'tracks') ? 'selected="selected"' : '' ?>><?= __('Sessiongroups in separate view') ?></option> -->
					<option value="timeline" <?= ($launcher->displaytype == 'timeline') ? 'selected="selected"' : '' ?>><?= __('Timeline') ?></option>
					<option value="type" <?= ($launcher->displaytype == 'type') ? 'selected="selected"' : '' ?>><?= __('Type') ?></option>
					<option value="datetime" <?= ($launcher->displaytype == 'datetime') ? 'selected="selected"' : '' ?>><?= __('Date/Time') ?></option>
					<?php if($app->apptypeid == 10) : ?>
					<option value="lineup" <?= ($launcher->displaytype == 'lineup') ? 'selected="selected"' : '' ?>><?= __('Line-up per stage') ?></option>
					<option value="lineupv2" <?= ($launcher->displaytype == 'lineupv2') ? 'selected="selected"' : '' ?>><?= __('Line-up per stage by hour') ?></option>
					<?php endif; ?>
				</select>
			</p>
			<?php elseif($launcher->moduletypeid == 15 || $launcher->moduletypeid == 24 || $launcher->moduletypeid == 25 || $launcher->moduletypeid == 27 || $launcher->moduletypeid == 41 || $launcher->moduletypeid == 50 || $launcher->moduletypeid == 51 || $launcher->moduletypeid == 52) : ?>
			<p>
				<label><?= __('Display type: ')?></label>
				<select name="displaytype" id="displaytype">
					<option value="list" <?= ($launcher->displaytype == 'list') ? 'selected="selected"' : '' ?>><?= __('List') ?></option>
					<option value="slider" <?= ($launcher->displaytype == 'slider') ? 'selected="selected"' : '' ?>><?= __('Slider') ?></option>
					<option value="thumbs" <?= ($launcher->displaytype == 'thumbs') ? 'selected="selected"' : '' ?>><?= __('Thumbs') ?></option>
					<?php if($launcher->moduletypeid == 15) : ?>
					<option value="imagelist" <?= ($launcher->displaytype == 'imagelist') ? 'selected="selected"' : '' ?>><?= __('Image list') ?></option>
					<?php endif; ?>
				</select>
			</p>
			<?php elseif($launcher->moduletypeid == 29) : ?>
			<p>
				<label><?= __('Display type: ')?></label>
				<select name="displaytype" id="displaytype">
					<option value="OnLauncher" <?= ($launcher->displaytype == '' || $launcher->displaytype == 'OnLauncher') ? 'selected="selected"' : '' ?>><?= __("Ad's on homescreen") ?></option>
					<option value="AllPages" <?= ($launcher->displaytype == 'AllPages') ? 'selected="selected"' : '' ?>><?= __("Ad's on all the pages") ?></option>
				</select>
			</p>
			<?php elseif($launcher->moduletypeid == 2) : ?>
			<p>
				<label><?= __('Display type: ')?></label>
				<select name="displaytype" id="displaytype">
					<option value="" <?= ($launcher->displaytype == '') ? 'selected="selected"' : '' ?>><?= __('Show exhibitors') ?></option>
					<option value="categories" <?= ($launcher->displaytype == 'categories') ? 'selected="selected"' : '' ?>><?= __('Show categories') ?></option>
				</select>
			</p>
			<?php elseif($launcher->moduletypeid == 39) : ?>
			<p <?= (form_error("watermark") != '') ? 'class="error"' : '' ?>>
				<label for="watermark"><?= __('Watermark:')?></label>
				<span class="hintAppearance"><?= __('Add a watermark to the pictures taken by users.') ?><br /><?= __('Image must be in png format, max size 612x612 pixels') ?></span>
				<span class="evtlogo" <?php if(file_exists($this->config->item('imagespath') .'upload/appimages/'.$app->id.'/watermark.png')){ ?>style="background:transparent url('<?= image_thumb('upload/appimages/'.$app->id.'/watermark.png', 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="watermark" id="watermark" value="" class="logoupload" />
				<br clear="all"/>
				<?php if(file_exists($this->config->item('imagespath') .'upload/appimages/'.$app->id.'/watermark.png')){ ?><span><a href="<?= site_url('module/removewatermark/'.$launcher->moduletypeid.'/event/'.$event->id) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
			</p>
			<?php elseif($launcher->moduletypeid == 49) : ?>
			<p  <?= (form_error("stayloggedin") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Stay logged in after closing app?')?></label>
				<input class="checkboxClass" type="checkbox" name="stayloggedin" value="yes" <?= $stayloggedin ? 'checked="checked"' : '' ?> /> 
			</p>
			<?php if(!empty($activeModules)) : ?>
			<p  <?= (form_error("usermodule") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Apply to whole app or seperate modules?')?></label>
				<?php 

				# Display modules
				$securedmoduleids = array();
				foreach($securedmodules as $s) $securedmoduleids[] = $s->launcherid;

				foreach($activeModules as $m) {
					$checked = in_array($m->id, $securedmoduleids)? 'checked="checked"' : ''; ?>
					<input type="checkbox" class="checkboxClass" name="securedmodules[]"<?=$checked?> value="<?=$m->id?>" />
					<?= $m->title ?><br clear="all"/>
<!-- 				<input class="checkboxClass" type="checkbox" name="securedmodules[]" value="<?=$m->id?>" <?= in_array($m->id, $securedmoduleids) ? 'checked="checked"' : '' ?> /><?= $m->title ?> <br /> -->
				<?php
				} ?>
			</p>
			<?php endif; ?>
			<p  <?= (form_error("pincodes") != '') ? 'class="error"' : '' ?>>
				<input type="radio" name="pincodes" value="0" class="checkboxClass" checked="checked" /> Use login and password (default) <br />
				<input type="radio" name="pincodes" value="1" class="checkboxClass" <?= $pincodes ? 'checked="checked"' : '' ?> /> Use pin codes
			</p>
			<?php elseif($launcher->moduletypeid == 69) : ?>
			<p  <?= (form_error("numberofquestionspergame") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Number of questions per game')?></label>
				<input type="text" name="numberofquestionspergame" value="<?= ($quiz) ? $quiz->numberofquestionspergame : '10' ?>" /> 
			</p>
			<p  <?= (form_error("facebooksharescore") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Allow sharing score on Facebook')?></label>
				<input class="checkboxClass" type="checkbox" name="facebooksharescore" value="yes" <?= ($quiz && $quiz->facebooksharescore == 1) ? 'checked="checked"' : '' ?> /> 
			</p>
			<p  <?= (form_error("defaultscorepercorrectanswer") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Default score per correct answer')?></label>
				<input type="text" name="defaultscorepercorrectanswer" value="<?= ($quiz) ? $quiz->defaultscorepercorrectanswer : '10' ?>" /> 
			</p>
			<p>
				<label><?= __('Form to show at end of quiz to send results by email (form must be built using form builder, form must have a field "email")')?></label>
				<select name="sendresultsbyemail_formid" id="sendresultsbyemail_formid">
					<option value="0"></option>
					<?php foreach($quizforms as $form) : ?>
					<option value="<?=$form->id?>" <?= ($quiz && $quiz->sendresultsbyemail_formid == $form->id) ? 'selected="selected"' : '' ?>><?= $form->title ?></option>
					<?php endforeach; ?>
				</select>
			</p>
			<?php endif; ?>


			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<?php if(isset($venue)) : ?>
					<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<?php elseif(isset($event)) : ?>
					<a href="<?= site_url('event/view/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<?php elseif(isset($app)) : ?>
					<a href="<?= site_url('apps/view/'.$app->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<?php endif; ?>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
