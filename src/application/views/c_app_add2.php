	<h1><?=__('Create new application')?></h1>
	<div class="frmsessions">
		<form action="<?=site_url('apps/addapp');?>" name="formappadd2" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent" onsubmit="$.post(this.action, $(this).serialize());">
			<?php if($error != ''): ?>
			<div class="error" style="padding: 10px 10px 12px 35px;"><?= $error ?></div>
			<?php endif ?>
			<fieldset>
			<?php if(isset($flavor)) : ?>
			<p>
				<label for="flavor"><?= __('Flavor:') ?></label>
				<input type="text" name="flavor" id="flavor" value="<?=($flavor->name == null || $flavor->name == '') ? set_value('flavor') : $flavor->name?>" disabled readonly/>
			</p>
			<?php endif; ?>
			<?php if($this->channel->id == 7 || $this->channel->id == 1) : ?>
<!-- 			<?php if(isset($subflavors) && $subflavors != null) : ?>
			<p>
				<label for="subflavor">Package:</label>
				<select name="subflavor" id="flavor">
					<?php foreach ($subflavors as $subflavor): ?>
						<option value="<?= $subflavor->id ?>"><?= $subflavor->name ?></option>
					<?php endforeach ?>
				</select>
			</p>
			<?php endif; ?> -->
			<?php endif; ?>
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label for="name"><?= __('Name:') ?></label>
				<input type="text" name="name" id="name" value="<?= htmlspecialchars_decode(set_value('name'), ENT_NOQUOTES) ?>" />
			</p>
			<p <?= (form_error("urlname") != '') ? 'class="error"' : '' ?>>
				<label for="urlname" style="margin-bottom:5px;"><?= __('URL mobile site:') ?></label>
				<label><span style="width:auto;">http://</span><input type="text" name="urlname" id="urlname" <?= (form_error("urlname") != '') ? 'class="error"' : '' ?> value="<?= set_value('urlname') ?>" style="width:100px;"><span style="width:auto;">.m.tap.cr</span></label>
			</p>
			<p <?= (form_error("cname") != '') ? 'class="error"' : '' ?>>
				<label for="cname"><?= __('Your URL for the mobile site (optional, click <a href="faq#cname" target="_blank">here</a> for more info):') ?></label>
				<input type="text" name="cname" id="cname" <?= (form_error("cname") != '') ? 'class="error"' : '' ?> value="<?= set_value('cname', 'http://m.mydomain.com') ?>">
			</p>
			<p <?= (form_error("language") != '') ? 'class="error"' : '' ?>>
				<label for="language"><?= __('Languages') ?></label>
                <?php foreach($languages as $language) : ?>
                    <input class="checkboxClass" type="checkbox" style="float:left; display:inline;" name="language[]" value="<?php echo $language->key; ?>" /> <label style="width:200px;float:left;"><?php echo $language->name; ?></label> <br clear="all" />
				<?php endforeach; ?>
			</p>
			<p> 
				<label><?= __('Default language:') ?></label>
				<select name="defaultlang" id="defaultlang">
					<option value="en"><?=__('English')?></option>
				</select>
			</p>
			</fieldset>
            
			<div class="buttonbar">
				<input type="hidden" name="postback" value="" id="postback">
				<input type="hidden" name="postback3" value="postback3" id="postback3">
				<input type="hidden" name="flavorid" value="<?=$flavorid?>" id="flavorid">
				<button class="btn primary" type="submit"><?= __('CREATE APP') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>