<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=7" />

	<title><?= ( isset($masterPageTitle) && $masterPageTitle ) ? $masterPageTitle : __("{$this->channel->name} Admin")?></title>
	<base href="<?= base_url() ?>" target="" />


	<!-- CSS -->
<!-- 	<link rel="stylesheet" href="css/reset.css" type="text/css" /> -->
	<link rel="stylesheet" href="css/jquery-ui-1.8.5.custom.css" type="text/css">
	<link rel="stylesheet" href="css/jquery.alerts.css" type="text/css" />
	<link rel="stylesheet" href="css/breadcrumb.css" type="text/css" />
	<link rel="stylesheet" href="css/datatables.css" type="text/css" />
	<link rel="stylesheet" href="css/debug.css" type="text/css" />
	<link rel="stylesheet" href="css/jquery.multiselect.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<!-- 	<link rel="stylesheet" href="css/uploadify.css" type="text/css" /> -->
	<link rel="stylesheet" href="css/masters.css" type="text/css" />
	<?php if($this->channel->templatefolder != ''){ ?>
		<link rel="stylesheet" href="templates/<?= $this->channel->templatefolder ?>/css/master.css" type="text/css" />
	<?php } ?>
	<!-- IE STYLESHEET -->
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" type="text/css" /><![endif]-->
	<!-- JAVASCRIPT -->
	<script type="text/javascript" charset="utf-8" src="js/jquery-1.7.2.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery-ui-1.8.5.custom.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.inlineedit.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.iphone-switch.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.maskedinput-1.2.2.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.jBreadCrumb.1.1.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.alerts.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery-ui-timepicker.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/tag-it.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/spin.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/bootstrap-dropdown.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/bootstrap-tooltip.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/tapcrowd.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.cookie.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.Jcrop.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/debug.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.multiselect.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/bootstrap-tab.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.qrcode.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/qrcode.js"></script>

    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
<!--     <script language="javascript" type="text/javascript" src="js/jquery.flot.js"></script> -->

  <link rel="Stylesheet" type="text/css" href="css/jpicker-1.1.6.min.css" />
  <link rel="Stylesheet" type="text/css" href="css/jPicker.css" />
  <script src="js/jpicker-1.1.6.min.js" type="text/javascript"></script>
<!--   <script type="text/javascript" charset="utf-8" src="js/uploadify/jquery.uploadify-3.1.js"></script> -->
		<!-- IE6 UPDATE MESSAGE -->
		<!--[if IE 6]>
			<script type="text/javascript">
				if(typeof jQuery == 'undefined'){ document.write("<script type=\"text/javascript\"   src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\"></"+"script>"); var __noconflict = true; }
				var IE6UPDATE_OPTIONS = {
					icons_path: "http://static.ie6update.com/hosted/ie6update/images/"
				}
			</script>
			<script type="text/javascript" src="http://static.ie6update.com/hosted/ie6update/ie6update.js"></script>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
				  $('<div></div>').html(IE6UPDATE_OPTIONS.message || 'U maakt momenteel gebruik van een verouderde browser I.E.6. Om de site optimaal te bezoeken dient u uw browser te updaten. Download hier een nieuwere versie van Internet Explorer... ').activebar(window.IE6UPDATE_OPTIONS);
				});
			</script>
			<script src="js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
			<script>DD_belatedPNG.fix('.png');</script>
		<![endif]-->
</head>
<body style="background:none;">
	<?php $app = _currentApp(); $isAdmin = _isAdmin(); ?>
	<div id="preloader">
		<span class="status"><?=__('Loading...')?></span>
	</div>
	<?php if (!$this->session->userdata('mijnevent') && $this->channel->id != '3' && $this->channel->id != '4' && $this->channel->id != '6'): ?>
	<div id="header">
		<div id="logo">
			<?php
			if(!empty($this->channel->templatefolder)) {
				$src = 'templates/'.$this->channel->templatefolder.'/img/header_logo.png'; ?>
				<a href="<?=base_url()?>"><img src="<?=$src?>" class="png" height="35" style="padding-top:5px;"/></a><?php
			} elseif($this->templatefolder != '') { ?>
				<?php if(file_exists("templates/".$this->templatefolder."/img/header_logo.png")) : ?>
				<a href="<?= base_url()?>"><img src="templates/<?=$this->templatefolder?>/img/header_logo.png" class="png" height="35" style="padding-top:5px;"/></a>
			<?php else: ?>
				<a href="<?= base_url() ?>"><img src="img/layout2/logo-tapcrowd.png" class="png" /></a>
			<?php endif; ?>
			<?php } else { ?>
				<a href="<?= base_url() ?>"><img src="img/layout2/logo-tapcrowd.png" class="png" /></a>
			<?php } ?>
		</div><?php

		# Language selector
		// if(defined('ENVIRONMENT') && ENVIRONMENT != 'production') { ?>
			<ul id="languageSelect"><?php
			$currentLang = $this->input->cookie('language', true);
			foreach(getAvailableLanguages() as $lang) {
				if($lang == 'en' || $lang == 'pt' || $lang == 'nl') {
				$class = (($lang == $currentLang)? 'selected locale_' : 'locale_') . $lang;  ?>
				<li class="<?= $class ?>"><a href="" hreflang="<?= $lang ?>"><?= $lang ?></a></li><?php
				}
			} ?>
			</ul><?php
		// } ?>

		<ul id="navigation">
			<a href="<?= site_url('contactus')?>" class="contactusbtn"><span class="icon-envelope icon-white contactusbtnicon"> &nbsp;</span><span><?= __('Contact us') ?></span></a>
			<?php
			if($this->session->userdata('name') != "") {

				# Get locations
				if($this->channel->hideMyAccount != 1) $account_url = \Tapcrowd\API::getLocation('','account');
				if($isAdmin) $admin_url = \Tapcrowd\API::getLocation('admin','admin');
				$logout_url = \Tapcrowd\API::getLocation('auth/logout','admin');

				# New account controller
				$account_url = \Tapcrowd\API::getLocation('account'); ?>

				<!-- <li><a href="" title="Dashboard"><i class="icon-home icon-white"></i> <?=__('Dashboard');?></a></li> -->
				<li><a href="<?= site_url('apps/myapps') ?>" title="Apps"><i class="icon-th-large icon-white"></i> <?=__('My Apps');?></a></li><?php
				if(!empty($account_url)) { ?>
					<li><a href="<?= $account_url ?>"><i class="icon-user icon-white"></i> <?=__('Profile');?></a></li><?php
				}
				if(!empty($admin_url)) { ?>
					<li><a href="<?= site_url('admin') ?>"><i class="icon-user icon-white"></i> <?=__('Admin');?></a></li><?php
				} ?>
				<li><a href="<?=$logout_url?>" class="last"><i class="icon-off icon-white"></i> <?=__('Logout');?></a></li>
			<?php } ?>
		</ul>
		<?php if(stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 7') || stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') || stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 5')) : ?>
		<div id="ienotification">
			<?= __("TapCrowd doesn't support older versions of Internet Explorer. We advise you to use <a href=\"http://www.mozilla.org/nl/firefox/\" target=\"_blank\">Mozilla Firefox</a> or <a href=\"http://www.google.com/chrome/\" target=\"_blank\">Google Chrome</a> or update your browser to the latest version.") ?>
		</div>
		<?php endif; ?>
	</div>
	<?php endif ?>

	<br clear="all">
	<div id="wrapper" <?= ($this->session->userdata('mijnevent')) ? "class='iframe'" : "" ?>>
		<div id="contentwrapper" style="float:none;">
			<div id="content" class="span8" style="float:none;margin: 0 auto; padding:20px;background-color:#f7f7f9;border-radius: 5px;height:auto;border: 1px solid #dddddd;"><?php
				# Page Header
				if(isset($pageheader)) { echo $pageheader; } ?>
				<?php if(isset($crumb) && $app != null ):?>
				<div id="breadCrumb" class="breadCrumb row span8">
					<ul>
						<li class="first"><a href="<?= site_url('apps') ?>"><?=__('Home')?></a></li>
						<?php if(isset($app) && $app != FALSE && $this->uri->segment(1) != 'account') : ?>
							<?php if($app->apptypeid == 1 || $app->apptypeid == 5 || $app->apptypeid == 9) : ?>
								<li><a href="<?= site_url('apps/view/'.$app->id) ?>"><?= $app->name ?></a></li>
							<?php endif; ?>
						<?php endif ?>
						<?php $count = count($crumb); $i = 1; ?>
						<?php foreach ($crumb as $crumbitem => $link): ?>
						<li <?= ($i == $count) ? 'class="last"' : '' ?>><a href="<?= site_url($link) ?>"><?= $crumbitem ?></a></li>
						<?php $i++; ?>
						<?php endforeach ?>
					</ul>
				</div>

				<div class="span8" />
				<?php endif ?>

				<?php
				/**
				* Display System Notifications
				*/
				echo Notifications::getInstance(); ?>

				<?/*Back button voor op MijnEvent*/?>
				<?php if ($this->session->userdata('mijnevent') && $this->uri->segment(1) != "event" && $this->uri->segment(2) != "view"): ?>
					<a href="<?= site_url('event/view/'.$event->id) ?>">&laquo; <?=__('Back')?></a>
				<?php endif ?>
				<?php if ($this->uri->segment(1) == "event" && $this->uri->segment(2) != "add" && $this->uri->segment(2) != "info"): ?>
					<h1 class="eventname"><?= $event->name ?></h1>
					<?php if(in_array($app->apptypeid, array(1,2,4,5,7,8,9,11))) : ?>
					<?php if ($event->active == 1): ?>
						<a href="<?= site_url('event/active/' . $event->id . '/off') ?>" class="onoff deactivate"><span><?=__('Deactivate event')?></span></a>
					<?php else: ?>
						<a href="<?= site_url('event/active/' . $event->id . '/on') ?>" class="onoff activate"><span><?=__('Activate event')?></span></a>
					<?php endif ?>
					<br clear="all" />
					<span class="eventstatus"><?=__('This event is currently')?> <strong><?= ($event->active == 1 ? 'active' : 'inactive') ?></strong></span>
					<?php endif; ?>
					<?php if($this->uri->segment(2) != 'edit') : ?>
						<br clear="all" />
					<?php endif; ?>

				<?php endif ?>
				<?php if ($this->uri->segment(1) == "venue" && $this->uri->segment(2) != "add"): ?>
					<h1 class="eventname"><?= $venue->name ?></h1>
					<?php if(in_array($app->apptypeid, array(3,5))) : ?>
					<?php if ($venue->active == 1): ?>
						<a href="<?= site_url('venue/active/' . $venue->id . '/off') ?>" class="onoff deactivate"><span><?=__('Deactivate venue')?></span></a>
					<?php else: ?>
						<a href="<?= site_url('venue/active/' . $venue->id . '/on') ?>" class="onoff activate"><span><?=__('Activate venue')?></span></a>
					<?php endif ?>
					<br clear="all" />
					<span class="eventstatus"><?=__('This' . ($app->apptypeid == 5) ? "POI" : "Venue" .' is currently')?> <strong><?= ($venue->active == 1 ? 'active' : 'inactive') ?></strong></span>
					<?php endif; ?>
					<?php if($this->uri->segment(2) != 'edit') : ?>
						<br clear="all" />
					<?php endif; ?>
				<?php endif ?>
				<?= isset($content) ? $content : __('"Page not found!"') ?>
			</div>
			<br clear="all" />
		</div>
		<div class="push"></div>
		<?php if (isset($sidebar) && $app): ?>
			<?php $sessionsidebar = $this->session->userdata('sidebar');?>
			<a href="<?=site_url('sidebar')?>" id="sidebar-btn"<?= ($sessionsidebar == 'hide') ? 'style="right:0px;"' : '' ?>><?= (!$sessionsidebar || $sessionsidebar == 'show') ? '<img src="img/layout2/button-close.png" />' : '<img src="img/layout2/button-open.png" />' ?></a>
			<div id="sidebar" <?= ($sessionsidebar == 'hide') ? 'style="display:none;right:-395px;"' : '' ?>>
				<div class="side-content">
					<?php if ($isAdmin && $this->uri->segment(1) == "admin"): ?>
						<ul class="general single">
							<li><a href="<?= site_url('admin/users') ?>" <?= ($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'users') ? 'class="sel"' : "" ?>><?=__('Users')?></a></li>
							<li><a href="<?= site_url('admin/apps') ?>" <?= ($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'apps') ? 'class="sel"' : "" ?>><?=__('Apps')?></a></li>
						</ul>
					<?php else: ?>
						<?= $sidebar?>
					<?php endif; ?>
				</div>
			</div>
		<?php endif ?>
	</div>

  <script type="text/javascript">

	// Enable Language selector (in header)
	$("#languageSelect a").click(function(e)
	{
		e.preventDefault();
		$.post('<?= site_url('translate/setlang') ?>/' + $(this).attr('hreflang'), function(data)
		{
			location.reload();
		});
		return false;
	});
  </script>
  <script type="text/javascript">
	$(document).ready(function() {
	<?php $sessionsidebar = $this->session->userdata('sidebar');
	if(!$sessionsidebar || $sessionsidebar == 'show') : ?>
	var sidebarToe = false;
	<?php else: ?>
	var sidebarToe = true;
	<?php endif; ?>
		$('#sidebar-btn').click(function(event) {
			event.preventDefault();
			var link = $(this).attr('href');
			if(!sidebarToe && $('#sidebar:animated').length <1){
				$(this).html('<img src="img/layout2/button-open.png" />');
				$('body').css('overflow-x', 'hidden');
				$(this).animate(
					{right:'-=395'},
					500,
					function() {});
				$('#sidebar').animate(
					{right:'-=395'},
					500,
					function(){
						sidebarToe = true;
						$('#sidebar').css('display', 'none');
						$('body').css('overflow-x', 'auto');
						$.ajax({
							type: "POST",
							url: link+'/hide'
						}).done(function( data ) {
						});
					}
				);
			}
			else if (sidebarToe && $('#sidebar:animated').length <1){
				$(this).html('<img src="img/layout2/button-close.png" />');
				$('body').css('overflow-x', 'hidden');
				$('#sidebar').css('display', 'block');

				$(this).animate(
					{right:'+=395'},
					500,
					function() {});
				$('#sidebar').animate(
					{right:'+=395'},
					500,
					function(){
						sidebarToe = false;
						$('body').css('overflow-x', 'auto');
						$.ajax({
							type: "POST",
							url: link+'/show'
						}).done(function( data ) {
						});
					}
				);
			}
		});

		var leftheight = $("#menu-container").height();
		var rightheight = $("#sidebar").height();
		var middleheight = $("#content").height();
		if(middleheight > leftheight) {
			leftheight = middleheight;
		}

		if(leftheight >= rightheight) {
			$("#sidebar").css('height', leftheight + 50);
		}

	});
  </script>
  <style type="text/css">
  	form.addevent {
  		width: auto;
  		padding-top:0px;
  		margin-top:-10px;
  	}
  	ul.flavors {
  		display: inline-block;
  	}
  </style>

	<script type="text/javascript">
	document.write(unescape("%3Cscript src='" + document.location.protocol +
	  "//munchkin.marketo.net/munchkin.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script>Munchkin.init('771-HZH-025');</script>

	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-10861070-2']);
	  _gaq.push(['_setDomainName', 'tapcrowd.com']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>

	<?php if(_currentUser() && ($this->uri->segment(1) == '' || ($this->uri->segment(1) == 'apps' && $this->uri->segment(2) == 'add'))) : ?>
	<?php 
		$name = explode(' ',_currentUser()->fullname);
		$firstname = $name[0];
		$lastname = '';
		unset($name[0]);
		foreach($name as $n) {
			$lastname .= $n;
		}
	?>
	<script>
	mktoMunchkin("771-HZH-025");

	mktoMunchkinFunction(
	   'associateLead',
	   {
	      Email: <?php echo "decodeURIComponent(\"" . rawurlencode(_currentUser()->email) . "\")" ?>,
		  FirstName: <?php echo "decodeURIComponent(\"" . rawurlencode($firstname) . "\")" ?>,
      	  LastName: <?php echo "decodeURIComponent(\"" . rawurlencode($lastname) . "\")" ?>
	   },
	   '<?php echo strtolower(hash('sha1', 'Eveloke123' . _currentUser()->email)); ?>'
	);
	</script>
	<?php endif; ?>
</body>
</html>
