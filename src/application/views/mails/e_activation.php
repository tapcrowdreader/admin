<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Tapcrowd Activation</title>

	<style type="text/css" media="screen">
	.activation { display:block; width:180px; text-align:center; border:1px solid #CCC; background:#EEE; padding:10px; margin:10px 0; }
	.activation:hover { text-decoration:underline; }
	</style>

</head>

<body>
	<p>Thank you for your registration, <?=$fullname?></p>
	<p>In order to properly use you're new account, You need to verify your email address.</p>
	<p><a href="<?=$url?>" class="activation" style="display:block; width:180px; text-align:center; border:1px solid #CCC; background:#EEE; padding:10px; margin:10px 0;">Activate your account &raquo;</a></p>
	<br />
	<p>Kind regards,</p>
	<p>The TapCrowd Team</p>
	<p><a href="http://tapcrowd.com" target="_blank">http://tapcrowd.com</a></p>
	<p><a href="mailto:info@tapcrowd.com">info@tapcrowd.com</a></p>
</body>
</html>
