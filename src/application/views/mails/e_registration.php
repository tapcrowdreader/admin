<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>e_registration</title>

</head>

<body>
	<p>Thank you for your registration, <?=$fullname?></p>
	<p>Your can login to your account using login '<?=$login?>'. </p>
	<br />
	<p>Kind regards,</p>
	<p>The TapCrowd Team</p>
	<p><a href="http://tapcrowd.com" target="_blank">http://tapcrowd.com</a></p>
	<p><a href="mailto:info@tapcrowd.com">info@tapcrowd.com</a></p>
</body>
</html>
