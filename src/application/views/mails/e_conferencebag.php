<?php
// Build conferencebag-string
$cbag = '<ul>';
foreach ($favorites as $fav): 
	$cbag .= '<li>' . (isset($fav->ename) && $fav->ename != '/' ? $fav->ename : $fav->sname . ' (' . $fav->sgroupname . ')' ) . ' ' . ($fav->presentation != '' ? '- <a href="'.$fav->presentation.'">Presentation &raquo;</a>' : '- No presentation') . '</li>';
endforeach;
$cbag .= '</ul>';

?>
<div><?= nl2br(str_replace('[userconferencebag]',$cbag, $message)) ?></div>