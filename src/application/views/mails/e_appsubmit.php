<h2>User</h2>
<p><?= _currentUser()->name ?> (<?= _currentUser()->login ?>)</p>

<h2>Applicationdata</h2>
<p>ID : <?= $app->id ?></p>

<h2>Description</h2>
<p><?= ($submitdata['remarks'] != '') ? nl2br($submitdata['remarks']) : 'No Description.' ?></p>

<p>Name : <?= $app->name ?></p>
<p>Stores : <?= $submitdata['appstore'] ?> <?= $submitdata['androidm'] ?></p>
<p>Regions : <?= $submitdata['worldwide'] ?> <?= $submitdata['europeonly'] ?> <?= $submitdata['usonly'] ?></p>
<p>Homescreen : <a href="<?= base_url() . $submitdata['homescreen'] ?>"><?= $submitdata['homescreen'] ?></a></p>
<p>Appicon : <a href="<?= base_url() . $submitdata['icon'] ?>"><?= $submitdata['icon'] ?></a></p>
<p>URL : <a href="http://<?= $submitdata['urlname'] ?>.m.tapcrowd.com">http://<?= $submitdata['urlname'] ?>.m.tap.cr</a></p>
<p>Searchterms : <?= $submitdata['searchterms'] ?></p>
<br />
<p>Kind regards,</p>
<p>The TapCrowd Team</p>
