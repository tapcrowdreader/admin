<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Tapcrowd Password Recovery</title>

</head>

<body>
	<p>You ( or someone else ) requested a password reset for your account. If it was not you, you can safely ignore this email,
	if intented, you can proceed by visiting below page:</p>
	<a href="<?=$url?>">Reset Password</a>
	<br />
	<p>Kind regards,</p>
	<p>The TapCrowd Team</p>
	<p><a href="http://tapcrowd.com" target="_blank">http://tapcrowd.com</a></p>
	<p><a href="mailto:info@tapcrowd.com">info@tapcrowd.com</a></p>
</body>
</html>
