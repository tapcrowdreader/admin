<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="en-us" />
	<base href="<?= base_url() ?>" />

	<title><?= $title ?></title>
	
	<style type="text/css" media="screen">
	
		* { margin:0; padding:0; }
		body { margin:0; color:#222222; background:#F6F5F5; font-family:"Lucida Grande","Lucida Sans",Arial,Verdana,Sans-Serif; font-size:12px; line-height:22px; }
		
		h1 { font-size:15px; font-weight:bold; border-bottom:1px dotted #CCC; margin-bottom:10px; padding:0 0 6px 0; }
		h2 { font-size:13px; display:block; border-bottom:1px dotted #CCC; margin-bottom:10px; padding:0 0 6px 0; }
		
		a.underlined { color:#000; border-bottom:1px dotted #666; text-decoration:none; }
		a:hover {  }
		
		div#wrapper { background:#FFF; margin:15px auto; width:580px; }
		div#wrapper #content { padding:15px; }
		
		ul { margin-left:25px; }
		
	</style>
	
</head>

<body style="margin:0; color:#222222; background:#F6F5F5; font-family:Lucida Grande,Lucida SansArial,Verdana,Sans-Serif; font-size:12px; line-height:22px;">
	<div id="wrapper" style="background:#FFF; margin:15px auto; width:580px;">
		<div id="header" style="position:relative; float:right; z-index:2; padding-left:400px; margin-top:10px;">
			<img width="200px" src="<?= $this->config->item('imagespath') . 'upload/confbagimages/517/vov.jpg'?>">
		</div>
		<div id="content" style="padding:15px; z-index:1;">
			<?= $content ?>
		</div>
	</div>
</body>
</html>
