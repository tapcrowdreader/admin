<?php
// Build conferencebag-string
$cbag = '<ul>';
foreach($inserts as $insert) {
	if($insert->order < 10) {
		$cbag .= '<li>'.$insert->text.' - <a href="'.$insert->link.'" target="_blank" class="underlined">'.$insert->link.'</a></li>';
	}
}
foreach ($favorites as $fav): 
	$cbag .= '<li>' . (isset($fav->ename) && $fav->ename != '/' ? $fav->ename : $fav->sname . ' (' . $fav->sgroupname . ')' ) . ' ' . ($fav->presentation != '' ? '- <a href="'.$fav->presentation.'">Presentatie &raquo;</a>' : '') . '</li>';
endforeach;

$cbag .= '</ul><br/>';
$cbag .= 'Links<br/>';
$cbag .= '<ul>';
foreach($favsExtra as $fav):
	if($fav->sessionid == 0 && $fav->exhibitorid == 0 && $fav->extra != '' && $fav->extra != '0') {
		if(substr($fav->extra, 0, 7) == 'http://') {
			$cbag .= '<li>'. $fav->extra .'</li>';
		}
	}
endforeach;
$cbag .= '</ul><br/>';
$cbag .= 'Gescande QR codes<br/>';
$cbag .= '<ul>';
$doneextras = array();
foreach($favsExtra as $fav):
	if($fav->sessionid == 0 && $fav->exhibitorid == 0) {
		if(substr($fav->extra, 0, 7) != 'http://' && $fav->extra != '' && $fav->extra != '0' && !in_array($fav->extra, $doneextras)) {
			$cbag .= '<li>'. $fav->extra .'</li>';
			$doneextras[] = $fav->extra;
		}
	}
endforeach;

$cbag .= '</ul>';
$cbag .= '<br/><br/>';
foreach($inserts as $insert) {
	if($insert->order > 10) {
		$cbag .= '<p>'.$insert->text.'</p>';
		$cbag .= '<p><a href="'.$insert->link.'" target="_blank"><img src="'.$this->config->item('imagespath').$insert->image.'" alt="Sponsor image" /></a></p>';
	}
}

?>
<div><?= nl2br(str_replace('[userconferencebag]',$cbag, $message)) ?></div>