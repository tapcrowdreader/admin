<div>
	<h1><?=__('Add Form')?></h1>
	<div class="frmformbuilder">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Title') ?> <?= '(' . $language->name . ')'; ?>:</label>
                    <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?=set_value('title_'.$language->key)?>" />
                </p>
                <?php endforeach; ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("submissionbuttonlabel_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Submit Button Label') ?> <?= '(' . $language->name . ')'; ?>:</label>
                    <input type="text" name="submissionbuttonlabel_<?php echo $language->key; ?>" id="submissionbuttonlabel" value="<?=set_value('submissionbuttonlabel_'.$language->key)?>" />
                </p>
                <?php endforeach; ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("submissionconfirmationtext_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="submissionconfirmationtext"><?= __('Confirmation Text') ?> <?= '(' . $language->name . ')'; ?>:</label>
                    <textarea name="submissionconfirmationtext_<?php echo $language->key; ?>" id="submissionconfirmationtext"><?= set_value('submissionconfirmationtext_'.$language->key) ?></textarea>
                </p>
                <?php endforeach; ?>
            <?php else : ?>
                <p <?= (form_error("submissionconfirmationtext_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="submissionconfirmationtext"><?= __('Confirmation Text:') ?></label>
                    <textarea name="submissionconfirmationtext_<?php echo $app->defaultlanguage; ?>" id="submissionconfirmationtext"><?= set_value('submissionconfirmationtext_'.$app->defaultlanguage) ?></textarea>
                </p>  
                <p <?= (form_error("name_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="title"><?= __('Title:') ?></label>
                    <input type="text" name="title_<?php echo $app->defaultlanguage; ?>" id="title" value="<?=set_value('title_'.$app->defaultlanguage)?>" />
                </p>
                <p <?= (form_error("submissionbuttonlabel_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="submissionbuttonlabel"><?= __('Submit Button Label:') ?></label>
                    <input type="text" name="submissionbuttonlabel_<?php echo $app->defaultlanguage; ?>" id="submissionbuttonlabel" value="<?=set_value('submissionbuttonlabel_'.$app->defaultlanguage)?>" />
                </p>
            <?php endif; ?>
             <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order', $defaultlauncher->order) ?>" id="order">
			</p>
            <p <?= (form_error("emailsendresult") != '') ? 'class="error"' : '' ?> >
				<label for="emailsendresult"><?= __('Email Results:') ?></label>
				<select name="emailsendresult" id="emailsendresult">
					<option value="0"><?= __('Select ...') ?></option>
					<option value="yes" selected="selected"><?= __('Yes') ?></option>
                    <option value="no"><?= __('No') ?></option>
				</select>
			</p>
            
            <div id="option_email" style="display:block;">
            <p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Email')?> (<?=__('separate email addresses with a comma')?>):</label>
				<input type="text" name="email" value="<?= set_value('email') ?>" id="email">
			</p>
            </div>
            
            <p <?= (form_error("allowmutliplesubmissions") != '') ? 'class="error"' : '' ?> >
				<label for="allowmutliplesubmissions"><?= __('Allow Mutliple Submissions:') ?></label>
				<select name="allowmutliplesubmissions" id="allowmutliplesubmissions">
					<option value="0"><?= __('Select ...') ?></option>
					<option value="yes" selected="selected"><?= __('Yes') ?></option>
                    <option value="no"><?= __('No') ?></option>
				</select>
			</p>
			
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Icon:') ?></a></label>
				<span class="hintAppearance"><?= __('Image must be in png format, width: %s px, height: %s px',140,140) ?></span>
				<input type="file" name="image" id="image" value="" class="logoupload" />
				<br clear="all"/>
			</p>
			
            <p>
				<label><input style="width: auto !important; display: inline !important;" type="checkbox" value="1" name="geolocation" id="geolocation" /> <?= __('Include User Location')?></label>
            </p>
            
            <p>
				<label><input style="width: auto !important; display: inline !important;" type="checkbox" <?php if($singlescreen == 0){ echo 'checked="checked"';} ?> value="1" name="multiscreen" id="multiscreen" /> <?= __('Multi Screen Form')?></label>
            </p>
            <p <?= (form_error("customurl") != '') ? 'class="error"' : '' ?>>
            	<?php
				$checked= ''; $customurlStyle = 'display:none;';
				
				if( !empty( $form->customurl ) || form_error("customurl") != '' ){ $checked= 'checked="checked"'; $customurlStyle = 'display:block;'; }?>
            	<label><input onclick="jQuery('#customurl').toggle();" style="width: auto !important; display: inline !important;" type="checkbox" <?= $checked;?> value="1" name="chkcustomurl" id="chkcustomurl" /> <?= __('Post to Custom URL')?></label>
                <input type="text" style="<?= $customurlStyle;?>" name="customurl" value="<?= set_value('customurl', $form->customurl) ?>" id="customurl" />
            </p>
            <p>
            	<label><input style="width: auto !important; display: inline !important;" type="checkbox" value="1" name="useflows" id="useflows" /> <?= __('Form with flows')?></label>
                <span class="hintAppearance"><?= __("Please Make sure to check 'Multi Screen Form' when using flows") ?></span>
            </p>
            <div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add Form') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
		// Sortables
		$(document).ready(function() {
			// Sessiongroups sorteren
			$( "#emailsendresult" ).change(function() {
			   var value = $(this).attr('value');
			   if(value == "yes") //If type = select/radio
			   {
				   document.getElementById("option_email").style.display='block';
			   }
			   else
			   {
				    document.getElementById("option_email").style.display='none';
			   }
			});
		});
</script>