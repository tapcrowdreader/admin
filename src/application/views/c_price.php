<h1><?= __('Submit app:') . ' ' . $app->name ?></h1>
<?php if(getCurrentChannel()->channelId == 15) : ?>
<p>
Dear customer,<br /><br />

Thank you for purchasing our mobile application service.<br />
You have chosen to acquire the services listed below for which you will receive an internal invoice from AXA Technology Services.<br /><br />

Do not hesitate to contact your AXA Technology Services account manager if you have any questions related to this service.<br /><br />

AXA Technology services
</p>
<?php endif; ?>
<form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<table>
		<tr>
			<th> </th>
			<th><?= __('Setup Price')?></th>
			<th><?= __('Monthly Price')?></th>
		</tr>

		<tr>
			<td><?= $flavor->title ?> (<?php echo $sflavor->name ?>)</td>
			<td><?php if($price->price_setup != 0) echo '&euro; ' . $price->price_setup; ?></td>
			<td><?php if($price->price_permonth != 0) echo '&euro; ' . $price->price_permonth;  ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><?= __('Select options:') ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>

		<?php foreach($options as $o) : ?>
		<tr>
			<td><input type="checkbox" data-min-contract="<?= $o->min_contract; ?>" name="<?= $o->id ?>" id="<?=$o->id?>" class="options checkboxClass"/> <?= $o->name ?></td>
			<td><span id="<?=$o->id?>_price_setupEuro"><?= ((int)$o->price_setup != 0) ? '&euro;' : '' ?></span> <span id="<?=$o->id?>_price_setup"><?php if($o->price_setup != 0) echo $o->price_setup; ?></span></td>
			<td><span id="<?=$o->id?>_price_monthEuro"><?= ((int)$o->price_permonth != 0) ? '&euro;' : '' ?></span> <span id="<?=$o->id?>_price_month"><?php if($o->price_permonth != 0) echo $o->price_permonth; ?></span></td>
		</tr>
		<?php endforeach; ?>
		
		<tr>
			<td style="font-weight:bold;"><?= __('Price (excl. VAT):') ?></td>
			<td style="font-weight:bold;"><span id="totalprice_setupEuro">&euro;</span> <span id="totalprice_setup"><?= $price->price_setup?></span></td>
			<td style="font-weight:bold;"><?= ((int)$price->price_permonth != 0) ? '&euro;' : '' ?></span> <span id="totalprice_month"><?= $price->price_permonth?></span><span id="totalprice_monthEuro"></td>
		</tr>
		<tr>
			<td style="font-weight:bold;"><?= __('VAT:') ?></td>
			<td style="font-weight:bold;"><span id="vat_setupEuro">&euro;</span> <span id="vat_setup"><?= $price->price_setup * $paymentinfo->vatAmount / 100 ?></span></td>
			<td style="font-weight:bold;"><span id="vat_monthEuro">&euro;</span> <span id="vat_month"><?= $price->price_permonth * $paymentinfo->vatAmount / 100 ?></span></td>
		</tr>
		<tr>
			<td style="font-weight:bold;"><?= __('Price (incl. VAT):') ?></td>
			<td style="font-weight:bold;"><span id="totalprice_setupEuro">&euro;</span> <span id="totalprice_setup_incl"><?= $price->price_setup + ($price->price_setup * $paymentinfo->vatAmount / 100) ?></span></td>
			<td style="font-weight:bold;"><?= ($price->price_permonth != 0) ? '&euro;' : '' ?></span> <span id="totalprice_month_incl"><?= $price->price_permonth + ($price->price_permonth * $paymentinfo->vatAmount / 100)?></span><span id="totalprice_monthEuro"></td>
		</tr>
	</table>
	<table>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th><?= __('Submission')?></th>
		</tr>

		<tr>
			<td><?= __('Setup:') ?></td>
			<td>&nbsp;</td>
			<td style=""><span id="totalprice_setup_1Euro">&euro;</span> <span id="totalprice_setup_1"><?= $price->price_setup ?></span></td>
		</tr>

		<tr>
			<td><?= __('First Month:') ?></td>
			<td>&nbsp;</td>
			<td style=""><?= ($price->price_permonth != 0) ? '&euro;' : '' ?></span> <span id="totalprice_month_1"><?= $price->price_permonth?></span><span id="totalprice_month_1Euro"></td>
		</tr>

		<tr>
			<td><?= __('Contract duration:') ?></td>
			<td>&nbsp;</td>
			<td id="min-contract-duration"><?= $price->minimumcontractduration . ' ' . __('Months') ?></td>
		</tr>
		<tr>
			<td><?= __('Billing period:') ?></td>
			<td>&nbsp;</td>
			<td><span id="billingperiod"><?= $price->billingperiod?></span> <?= __('Months') ?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;"><?= __('Total (excl. VAT):') ?></td>
			<td>&nbsp;</td>
			<td style="font-weight:bold;"><span id="totalpriceEuro"><?= ($price->price_setup + $price->price_permonth != 0) ? '&euro;' : '' ?></span> <span id="totalprice"><?= $price->price_setup + ($price->price_permonth * $price->billingperiod) ?></span></td>
		</tr>
		<tr>
			<td style="font-weight:bold;"><?= __('Total VAT:') ?></td>
			<td>&nbsp;</td>
			<td style="font-weight:bold;"><span id="totalvatEuro">&euro;</span> <span id="totalvat"><?= ($price->price_setup + ($price->price_permonth * $price->billingperiod)) * $paymentinfo->vatAmount / 100 ?></span></td>
		</tr>
		<tr>
			<td style="font-weight:bold;"><?= __('Total (incl. VAT):') ?></td>
			<td>&nbsp;</td>
			<td style="font-weight:bold;"><span id="totalinclEuro">&euro;</span> <span id="totalinclvat"><?= $price->price_setup + ($price->price_permonth * $price->billingperiod) + ($price->price_setup + ($price->price_permonth * $price->billingperiod)) * $paymentinfo->vatAmount / 100 ?></span></td>
		</tr>
		<!-- <tr>
			<td>&nbsp;</td>
			<td><?= __('Paid by upgrade:') ?></td>
			<td><?= $paid ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><?= __('To Pay:') ?></td>
			<td><?= $paid ?></td>
		</tr> -->
	</table>
	<input type="hidden" name="priceField" value="<?= (int)$price->price_setup + (int)$price->price_permonth + (int)$priceApp ?>" id="priceField">
	<input type="hidden" name="postback" value="postback" id="postback">
	<button type="submit" class="btn primary" style="margin-top:20px;float:right;"><?= __('Submit app') ?></button>
</form>
<script type="text/javascript">
	$(document).ready(function() {
		var cur_contract = parseInt("<?= $price->minimumcontractduration; ?>");
		var flavor_min_contract = parseInt("<?= $price->minimumcontractduration; ?>");
		var billingperiod = parseInt("<?= $price->billingperiod; ?>");
			
		function uncheckall() {
			var ins = document.getElementsByTagName('input');
			for (var i=0; i<ins.length; i++) {
				if (ins[i].getAttribute('type') == 'checkbox') {
					ins[i].checked = false;
				}
			}
		}
		uncheckall();
		$(".options").change(function () {
			cur_contract = flavor_min_contract;
			$('input:checked').each(function()
			{
				if(cur_contract < parseInt($(this).attr('data-min-contract'))) {
	        		cur_contract = parseInt($(this).attr('data-min-contract'));
		        }
			});	
			$("#min-contract-duration").html(cur_contract+" Months");
			$("#billingperiod").html(billingperiod);	

			var newprice = parseInt($("#totalprice").html());
			var tp_month  = parseInt($("#totalprice_month").html());
			var tp_setup  = parseInt($("#totalprice_setup").html());
			var p_setup = parseInt($("#"+$(this).attr('id')+"_price_setup").html());
	        var p_month = parseInt($("#"+$(this).attr('id')+"_price_month").html());
	        var p_billingperiod  = billingperiod;
	        var contract = $(this).attr('data-min-contract');
	        if ($(this).is(':checked')) {
	        	if(!isNaN(p_setup)) {
					tp_setup = tp_setup + p_setup;
	        	}
	        	if(!isNaN(p_month)) {
	        		tp_month = tp_month + p_month;
	        	}
	        } else {
				if(!isNaN(p_month)) {
					tp_month = tp_month - p_month;
	        	}
	        	if(!isNaN(p_setup)) {
	        		tp_setup = tp_setup - p_setup;
	        	}
	        }

	        newprice = tp_setup + (tp_month * p_billingperiod);	
	        var newprice_vat = newprice * <?= $paymentinfo->vatAmount ?> / 100;
	        var newprice_incl = newprice + newprice_vat;

	        var vatmonth = tp_month * <?= $paymentinfo->vatAmount ?> / 100; 
	        $("#vat_month").html(vatmonth);
	        var vatsetup = tp_setup * <?= $paymentinfo->vatAmount ?> / 100; 
	        $("#vat_setup").html(vatsetup);

	        $("#totalprice_setup_incl").html(vatsetup + tp_setup );
	        $("#totalprice_month_incl").html(vatmonth + tp_month);
	        
	        $("#totalprice_month").html(tp_month);
			$("#totalprice_setup").html(tp_setup);
			$("#totalprice_month_1").html(tp_month);
			$("#totalprice_setup_1").html(tp_setup);
	        
	        $("#totalprice").html(newprice);
	        $("#priceField").val(newprice);

	        $("#totalvat").html(newprice_vat);
	        $("#totalinclvat").html(newprice_incl);

	        //First check if this value is bigger

	        //Check rest of checkboxes
	        
	    });
	});
</script>