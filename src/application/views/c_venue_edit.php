<a href="<?= site_url('module/editByController/about/venue/'.$venue->id.'/0') ?>" class="edit btn" style="float:right;"><i class="icon-pencil"></i><?= __('Module Settings')?></a>
<div id="myTab">
<div class="tabbable tabs-below">
	<ul class="nav nav-tabs">
		<li class="active">
			<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#info"><?= __('Info')?></a>
		</li>
		<li class="">
			<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#travelinfo"><?= __('Travelinfo')?></a>
		</li>
		<li class="">
			<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#schedule"><?= __('Schedule')?></a>
		</li>
	</ul>
	<br clear="all" />
	<div class="tab-content">
		<div id="info" class="tab-pane active">
			<form action="<?= site_url('venue/edit/'.$venue->id.'/'.$launcherid) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
				<?php if($error != ''): ?>
				<div class="error"><?= $error ?></div>
				<?php endif ?>
				<?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
				<div class="error"><p><?= __('Something went wrong with the image upload.')?> <br/> <?= __('Maybe the image size exceeds the maximum width or height')?></p></div>
				<?php endif ?>
				<fieldset>
					<!-- <h3><?= __('Edit '. ($app->familyid == 3) ? "POI" : "Venue" )?></h3> -->
			        <?php if(isset($languages) && !empty($languages)) : ?>
			        <?php foreach($languages as $language) : ?>
			        <p <?= (form_error("w_name_".$language->key) != '') ? 'class="error"' : '' ?>>
			            <label for="w_name"><?= __('Name '.'(' . $language->name . ')'); ?>:</label>
						<?php $trans = _getTranslation('venue', $venue->id, 'name', $language->key); ?>
						<input type="text" name="w_name_<?php echo $language->key; ?>" id="name" value="<?= htmlspecialchars_decode(set_value('w_name_'.$language->key, ($trans != null) ? $trans : $venue->name), ENT_NOQUOTES) ?>"/>
			        </p>
			        <?php endforeach; ?>
			        <?php else : ?>
			        <p>
			            <label for="w_name"><?= __('Name:')?></label>
			            <textarea name="w_name_<?php echo $app->defaultlanguage; ?>" id="name" <?= (form_error("w_name_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>><?= htmlspecialchars_decode(set_value('w_name_'. $app->defaultlanguage, $venue->name), ENT_NOQUOTES) ?></textarea>
			        </p>
			        <?php endif; ?>

					<p id="p_address">
						<label for="w_address"><?= __('Address:')?></label>
						<input type="text" name="w_address" id="w_address" value="<?= htmlspecialchars_decode(set_value('w_address', $venue->address), ENT_NOQUOTES) ?>" <?= (form_error("w_address") != '') ? 'class="error"' : '' ?> onblur="codeAddress()" />
						<a href="#showonmap" class="btn" id="btnshowonmap" onclick="codeAddress(); return false;"><?= __('Show on map')?> &raquo;</a>
					</p>
					<div id="map_canvas" class="map" style="height:300px; width:620px; margin-left:0px;"></div>
					<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
					<script type="text/javascript">
						var geocoder;
						var map;
						var bounds;
						var marker;

						function codeAddress() {
							var address = document.getElementById('w_address').value;
							if(address != '') {
								geocoder.geocode( { 'address': address}, function(results, status) {
									if (status == google.maps.GeocoderStatus.OK) {
										if(marker != null){
											marker.setMap(null);
										}
										map.setCenter(results[0].geometry.location);
										marker = new google.maps.Marker({
											map: map,
											position: results[0].geometry.location
										});
										var lat = results[0].geometry.location.lat().toString();
										var lon = results[0].geometry.location.lng().toString();
										$('#w_lat').val(lat.substring(0,10));
										$('#w_lon').val(lon.substring(0,10));

										marker.draggable = true;
										google.maps.event.addListener(marker, 'drag', function() {
											var lat = marker.getPosition().lat().toString();
											var lon = marker.getPosition().lng().toString();
											$('#w_lat').val(lat.substring(0,10));
											$('#w_lon').val(lon.substring(0,10));
										});

										bounds = new google.maps.LatLngBounds();
										bounds.extend(results[0].geometry.location);
										map.fitBounds(bounds);
										map.setZoom(map.getZoom()-1);
									} else {
										if(status === 'ZERO_RESULTS'){
											alert('<?= __('No results found on the address ...')?>');
										} else {
											alert("<?= __('Geocode was not successful for the following reason: ')?>" + status);
										}
									}
								});
							}
						}

						$(document).ready(function() {
							geocoder = new google.maps.Geocoder();
							var latlng = new google.maps.LatLng(<?= ($venue->lat != '') ? $venue->lat : '50.753887' ?>, <?= ($venue->lon != '') ? $venue->lon : '4.269936' ?>);
							var myOptions = {
								zoom: 8,
								center: latlng,
								mapTypeId: google.maps.MapTypeId.ROADMAP
							}
							map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
							bounds = new google.maps.LatLngBounds();

							<?php if($venue->lat != '' && $venue->lon != '') { ?>
							map.setCenter(latlng);
							marker = new google.maps.Marker({
								map: map,
								position: latlng
							});

							marker.draggable = true;
							google.maps.event.addListener(marker, 'drag', function() {
								$('#w_lat').val(marker.getPosition().lat());
								$('#w_lon').val(marker.getPosition().lng());
							});

							bounds.extend(latlng);
							map.setZoom(16);
							<?php } ?>
						});

					</script>
					<a href="" id="showcordinates" class="btn" style="margin-top:-10px;margin-bottom:10px;"><?= __('Advanced') ?></a><br clear="all" />
					<div id="cordinates" style="display:none;">
						<p <?= (form_error("w_lat") != '') ? 'class="error"' : '' ?>>
							<label for="w_lat"><?= __('Latitude:')?></label>
							<input type="text" name="w_lat" id="w_lat" value="<?= substr(set_value('w_lat', $venue->lat), 0, 10) ?>" style="width:100px;" maxlength="10" /><br clear="all" />
						</p>
						<p <?= (form_error("w_lon") != '') ? 'class="error"' : '' ?>>
							<label for="w_lon"><?= __('Longitude:')?></label>
							<input type="text" name="w_lon" id="w_lon" value="<?= substr(set_value('w_lon', $venue->lon), 0, 10) ?>" style="width:100px;" maxlength="10" /><br clear="all" />
						</p>
					</div>
					<p <?= (form_error("w_tel") != '') ? 'class="error"' : '' ?>>
						<label for="w_tel"><?= __('Telephone:')?></label>
						<input type="text" name="w_tel" id="w_tel" value="<?= set_value('w_tel', $venue->telephone) ?>" />
					</p>
					<p <?= (form_error("w_fax") != '') ? 'class="error"' : '' ?>>
						<label for="w_tel"><?= __('Fax:')?></label>
						<input type="text" name="w_fax" id="w_fax" value="<?= set_value('w_fax', $venue->fax) ?>" />
					</p>
					<p <?= (form_error("w_email") != '') ? 'class="error"' : '' ?>>
						<label for="w_email"><?= __('Email:')?></label>
						<input type="text" name="w_email" id="w_email" value="<?= set_value('w_email', $venue->email) ?>" />
					</p>
					<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
						<label for="website">Website:</label>
						<input type="text" name="website" id="website" value="<?= set_value('website', $venue->website) ?>" />
					</p>
					<p <?= (form_error("facebookurl") != '') ? 'class="error"' : '' ?>>
						<label for="facebookurl"><?= __('Facebook url:')?></label>
						<input type="text" name="facebookurl" id="facebookurl" value="<?= set_value('facebookurl', $venue->facebookurl) ?>" />
					</p>
					<p style="display:none;" <?= (form_error("w_opening") != '') ? 'class="error"' : '' ?>>
						<label for="w_opening"><?= __('Opening')?><br /><?= __('hours:')?></label>
						<textarea name="w_opening" id="w_opening"><?= set_value('w_opening', $venue->openinghours) ?></textarea>
					</p>
			        <?php if(isset($languages) && !empty($languages)) : ?>
			        <?php foreach($languages as $language) : ?>
					<p <?= (form_error("w_info_".$language->key) != '') ? 'class="error"' : '' ?>>
						<label for="w_info"><?= __('Info '.'(' . $language->name . ')'); ?>:</label>
						<?php $trans = _getTranslation('venue', $venue->id, 'info', $language->key); ?>
						<textarea name="w_info_<?php echo $language->key; ?>" id="w_info"><?= htmlspecialchars_decode(set_value('w_info_'.$language->key, ($trans != null) ? $trans : $venue->info), ENT_NOQUOTES) ?></textarea>
					</p>
			        <?php endforeach; ?>
			        <?php else : ?>
					<p>
						<label for="w_info"><?= __('Info:')?></label>
						<textarea name="w_info_<?php echo $app->defaultlanguage; ?>" id="w_info" <?= (form_error("w_info_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>><?= htmlspecialchars_decode(set_value('w_info_'. $app->defaultlanguage, $venue->info), ENT_NOQUOTES) ?></textarea>
					</p>
			        <?php endif; ?>
			<!--         <div class="row">
						<p style="min-height:0px;">
						<label for="tags">Tags:</label>
						</p>
						<ul id="mytags" name="mytagsul"></ul>
			        </div> -->
					<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
						<label for="order"><?= __('Order:')?></label>
						<input type="text" name="order" id="order" value="<?= set_value('order', $venue->order) ?>" />
					</p>
					<?php foreach($metadata as $m) : ?>
						<?php foreach($languages as $lang) : ?>
							<?php if(_checkMultilang($m, $lang->key, $app)): ?>
								<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
									<?= _getInputField($m, $app, $lang, $languages, 'venue', $venue->id); ?>
								</p>
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endforeach; ?>
					<p>
						<label for="imageurl1"><?= ($app->familyid != 3) ? 'Image' : 'Image 1' ?>:</label>
						<?php if($app->apptypeid != 7 && $app->apptypeid != 8) : ?>
							<span class="hintAppearance" style="padding-left:3px;"><?= __('Optimized for pictures with 280 pixels width and 180px height or same dimension.')?></span><br/>
						<?php else: ?>
							<span class="hintAppearance" style="padding-left:3px;"><?= __('Optimized for pictures with 560 pixels width and 360px height.')?></span><br/>
						<?php endif; ?>
						<span class="evtlogo" <?php if($venue->image1 != ''){ ?>style="background:transparent url('<?= $this->config->item('publicupload') . $venue->image1 ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
						<input type="file" name="imageurl1" id="imageurl1" value="" class="logoupload" /><br clear="all"/>
						<?php if($venue->image1 != '' && file_exists($this->config->item('imagespath') . $venue->image1)){ ?><span><a href="<?= site_url('venue/removeimage/'.$venue->id.'/1') ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
					</p><br clear="all"/>
					<p>
						<label for="imageurl2"><?= __('Image2:')?></label>
						<span class="evtlogo" <?php if($venue->image2 != ''){ ?>style="background:transparent url('<?= $this->config->item('publicupload') . $venue->image2 ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
						<input type="file" name="imageurl2" id="imageurl2" value="" class="logoupload" /><br clear="all"/>
						<?php if($venue->image2 != '' && file_exists($this->config->item('imagespath') . $venue->image2)){ ?><span><a href="<?= site_url('venue/removeimage/'.$venue->id.'/2') ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
					</p><br clear="all"/>
					<p>
						<label for="imageurl3"><?= __('Image3:')?></label>
						<span class="evtlogo" <?php if($venue->image3 != ''){ ?>style="background:transparent url('<?= $this->config->item('publicupload') . $venue->image3 ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
						<input type="file" name="imageurl3" id="imageurl3" value="" class="logoupload" /><br clear="all"/>
						<?php if($venue->image3 != '' && file_exists($this->config->item('imagespath') . $venue->image3)){ ?><span><a href="<?= site_url('venue/removeimage/'.$venue->id.'/3') ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
					</p><br clear="all"/>
					<p>
						<label for="imageurl4"><?= __('Image4:')?></label>
						<span class="evtlogo" <?php if($venue->image4 != ''){ ?>style="background:transparent url('<?= $this->config->item('publicupload') . $venue->image4 ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
						<input type="file" name="imageurl4" id="imageurl4" value="" class="logoupload" /><br clear="all"/>
						<?php if($venue->image4 != '' && file_exists($this->config->item('imagespath') . $venue->image4)){ ?><span><a href="<?= site_url('venue/removeimage/'.$venue->id.'/4') ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
					</p><br clear="all"/>
					<p>
						<label for="imageurl5"><?= __('Image5:')?></label>
						<span class="evtlogo" <?php if($venue->image5 != ''){ ?>style="background:transparent url('<?= $this->config->item('publicupload') . $venue->image5 ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
						<input type="file" name="imageurl5" id="imageurl5" value="" class="logoupload" /><br clear="all"/>
						<?php if($venue->image5 != '' && file_exists($this->config->item('imagespath') . $venue->image5)){ ?><span><a href="<?= site_url('venue/removeimage/'.$venue->id.'/5') ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
					</p><br clear="all"/>
				</fieldset>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="btn"><?= __('Cancel')?></a>
					<button type="submit" class="btn primary"><?= __("Save")?></button>
					<br clear="all">
				</div>
				<br clear="all">
			</form>
			<script type="text/javascript" charset="utf-8">
				$(document).ready(function(){
					$("#mytags").tagit({
						initialTags: [<?php if(isset($tags) && $tags != null){foreach($tags as $val){ echo '"'.$val->tag.'", '; }} ?>],
					});
					var tags = new Array();
					var i = 0;
					<?php foreach($apptags as $tag) : ?>
					tags[i] = '<?=$tag->tag?>';
					i++;
					<?php endforeach; ?>
					$("#mytags input.tagit-input").autocomplete({
						source: tags
					});

					$("#showcordinates").click(function(event) {
						event.preventDefault();
						$("#cordinates").show();
						$(this).hide();
					});
				});
			</script>
		</div>
		<div id="travelinfo" class="tab-pane">
			<?php if($this->session->flashdata('event_feedback') != ''): ?>
			<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
			<?php endif ?>
			<div class="frmsessions">
				<form action="<?= site_url('venue/travelinfo/'.$venue->id.'/'.$launcherid);?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
					<?php if($error != ''): ?>
					<div class="error"><?= $error ?></div>
					<?php endif ?>
					<?php
					if(isset($languages) && !empty($languages)) {
						foreach($languages as $language) {
							$trans = _getTranslation('venue', $venue->id, 'travelinfo', $language->key);
							$data = array(
									'name'			=> 'txt_travelinfo_'.$language->key,
									'id'			=> 'txt_travelinfo_'.$language->key,
									'toolbarset' 	=> 'Travelinfo',
									'value' 		=> htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('txt_travelinfo_'.$language->key, ($trans != null) ? $trans : $venue->travelinfo)))))),
									'width'			=> '680px',
									'height'		=> '400'
								);
							echo '<p><label>' . __('Travelinfo') . '('.$language->name.') </label></p>';
							echo form_fckeditor($data);
						}
					} else {
						$data = array(
								'name'			=> 'txt_travelinfo_'._currentApp()->defaultlanguage,
								'id'			=> 'txt_travelinfo_'._currentApp()->defaultlanguage,
								'toolbarset' 	=> 'Travelinfo',
								'value' 		=> htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('txt_travelinfo_'._currentApp()->defaultlanguage, $venue->travelinfo)))))),
								'width'			=> '700px',
								'height'		=> '400'
							);

						echo form_fckeditor($data);
					}
					?>
					<div class="buttonbar">
						<input type="hidden" name="postback" value="postback" id="postback">
						<button type="submit" class="btn primary"><?=__('Save')?></button>
						<br clear="all" />
					</div>
					<br clear="all" />
				</form>
			</div>
		</div>
		<div id="schedule" class="tab-pane">
			<div class="buttonbar" style="margin-right:0px;min-height: 40px;">
				<a href="<?= site_url('schedule/add/' . $venue->id . '/venue') ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i> <?= __('Add Schedule') ?></a>
			</div>
			<br clear="all" />
			<table id="listview" class="display zebra-striped">
				<thead>
					<tr>
						<?php foreach ($headers as $h => $field): ?>
							<?php if ($h == 'Order'): ?>
								<th class="data order"><?= $h ?></th>
							<?php else: ?>
								<th class="data"><?= $h ?></th>
							<?php endif ?>
						<?php endforeach; ?>
						<th width="17"></th>
						<th width="17"></th>
						<th width="17"></th>
					</tr>
				</thead>
				<tbody>
					<?php if ($schedule != FALSE): ?>
					<?php foreach($schedule as $row): ?>
							<tr>
								<?php foreach ($headers as $h => $field): ?>
		                        <?php if ($field != null && !empty($field)) : ?>
										<td><?= $row->{$field} ?></td>
		                        <?php endif; ?>
								<?php endforeach ?>
								<td class="icon"><a href='<?= site_url("schedule/edit/" . $row->id . '/venue') ?>' ><i class="icon-pencil"></i></a></td>
								<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->id . '/schedule/venue') ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
								<td class="icon"><a href='<?= site_url("schedule/delete/" . $row->id . '/venue') ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
							</tr>
					<?php endforeach ?>
					<?php endif ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>