<?php
/**
 * Account display page
 */

if(empty($account)) {
	echo __('No account');
	return;
}
$edit_url = "account/edit/{$account->accountId}";
$browse_url = "account/browse/{$account->accountId}";
?>

<div class="pull-right">
	<a class="btn" href="<?=$browse_url?>"><i class="icon-wrench"></i> <?=__('Manage Accounts')?></a>
	<a class="btn btn-primary" href="<?=$edit_url?>"><?=__('Edit Account')?></a>
</div>

<h1><?= ($account === _currentUser())? __('My Account') : __('Account %s', $account->name) ?></h1>
<div class="">
	<h2><?=__('Account Information')?></h2>
	<dl class="dl-horizontal well well-large">
		<dt><?= __('Name:'); ?></dt><dd><?=$account->fullname?></dd>
		<dt><?= __('Email:'); ?></dt><dd><a href="mailto:<?=$account->email?>"><?=$account->email?></a></dd>
	</dl>
</div>
