<script language="javascript">
function submit_import()
{
	document.getElementById("importprogress").style.display = "block";
	document.forms.xpoimport.submit();
}

</script>
<div>
	<div class="frmsessions">
		<h1><?= __('Import') ?></h1>
		<p><?= __('Please download our template file and fill in your data like the example.') ?><br />
		<br clear="all" />
        <a href="<?= $template_file; ?>" target="_blank" class="btn primary"><strong><?= __('You can download it right here.') ?></strong></a>
		<?php if(!empty($template_example)) : ?>
        <a href="<?= $template_example; ?>" target="_blank" class="btn primary"><strong><?= __('Example') ?></strong></a></p><br clear="all" />
		<?php endif; ?>
        <form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit" id="xpoimport" name="xpoimport">
			<?php if($error != ''): ?>
            <br clear="all" />
			<div class="error"><p><?= $error ?></p></div>
			<?php endif ?>
            
            <div id="importprogress" style="display:none;" class="importprogress"><br /></div>
            
			<p <?= (form_error("excelfile") != '') ? 'class="error"' : '' ?>>
				<label for="excelfile"><?= __('XLS File:') ?></label>
				<input type="file" name="excelfile" id="excelfile" value="<?=set_value('excelfile')?>" class="logoupload" /><br clear="all"/>
			</p>          
                        <?php /*
			<p <?= (form_error("zip_file") != '') ? 'class="error"' : '' ?>>
				<label for="zip_file"><?php echo isset($zip_field_label)?$zip_field_label:'Zip File';?>:</label>
				<input type="file" name="zip_file" id="excelfile" value="<?=set_value('zip_file')?>" class="logoupload" /><br clear="all"/>
			</p>          
                        <?php if($speaker=='yes'): ?>
			<p <?= (form_error("speaker_zip_file") != '') ? 'class="error"' : '' ?>>
				<label for="speaker_zip_file">Speaker Zip File:</label>
				<input type="file" name="speaker_zip_file" id="excelfile" value="<?=set_value('speaker_zip_file')?>" class="logoupload" /><br clear="all"/>
			</p>          
                        <?php endif; ?>
                        <?php */ ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="button" class="btn primary" id="startimport" onclick="submit_import();"><?= __('Start Import') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>	
</div>