<h3><?= __('Table Selection Options') ?></h3>

<p  style="font-weight: bolder;">
	<?=__("We advise you to select just one option for the table number introduction process.")?>
</p>
</br>

<div class="modules" style="margin-top: 10px">
	<?php foreach($options as $option) : ?>
		<div class="module <?=($option[1]==1) ? "active" : "inactive"?>">
			<a class="editlink">
				<span>
					<?php
						switch($option[0])
						{
							case "qrscan"	: echo 'QR code scanning';
												break;
							case "input"	: echo 'Input table number';
												break;
							case "color"	: echo 'Color localization';
												break;
							case "nfc"		: echo 'Near field communication (NFC)';
												break;
							case "counter"	: echo 'Get order at counter';
												break;
							case "floorplan": echo 'Select table from floorplan';
												break;
						}
					?>
				</span>
			</a>
			<a href="/basket/table_option/<?=$venue->id?>/<?=$option[0]?>/<?=$option[1]?>" class="activate <?=($option[1]==1) ? "delete" : "add white"?>">&nbsp;</a>
		</div>
	<?php endforeach?>
</div>