<div id="loyalty">
	<h1><?= __('Loyalty')?></h1>
	<a href="<?= site_url('module/editByController/loyalty/venue/'.$venue->id) ?>" class="edit btn" style="float:right;margin-bottom:5px;margin-top:-35px;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a><br style="margin-bottom:15px;clear:both;"/>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="add">

			<?php if($error != ''): ?>
				<div class="error"><?= $error ?></div>
			<?php endif ?>

            <?php if(count($languages)>1 ) : ?>
                <?php if(isset($languages) && !empty($languages)) : ?>
	                <?php foreach($languages as $language) : ?>
		                <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
		                    <label for="title"><?= __('Title '.'(' . $language->name . ')'); ?>:</label>
		                    <input maxlength="50" type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?=set_value('title_'.$language->key,__("Earn Points!"))?>" />
		                    <label><?= __('max 50 characters')?></label>
		                </p>
	                <?php endforeach; ?>
	                <?php foreach($languages as $language) : ?>
		                <p <?= (form_error("text_".$language->key) != '') ? 'class="error"' : '' ?>>
		                    <label for="text"><?= __('Text') ?> <?= '(' . $language->name . ')'; ?>:</label>
		                    <textarea name="text_<?php echo $language->key; ?>" id="text">
<?= set_value('text_'.$language->key,__("Each time you visit us, you can input the code at the bar. With each code, you will earn 1 point. When you have 5 points, we will offer you a free drink."))?>
		                    </textarea>
		                </p>
	                <?php endforeach; ?>
        		<?php endif; ?>
            <?php else : ?>
                <p <?= (form_error("title_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="title"><?= __('Title:') ?></label>
                    <input type="text" name="title_<?php echo $app->defaultlanguage; ?>" id="title" value="<?=set_value('title_'.$app->defaultlanguage, __("Earn Points!"))?>" />
                </p>
                <p>
                    <label for="text"><?= __('Text:') ?></label>
                    <textarea name="text_<?php echo $app->defaultlanguage; ?>" id="text" <?= (form_error("text_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
<?= set_value('text_'.$app->defaultlanguage,__("Each time you visit us, input the code at the bar. With each scan, you will earn 1 point. When you have 5 points, we will offer you a free drink."))?>
                    </textarea>
                </p>
            <?php endif; ?>

 			<p <?= (form_error("code") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Code:') ?></label>
				<?php $code = ''.rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).'';?>
				<input maxlength="5" type="text" id="code" name="code" value="<?=set_value('code',$code)?>" onchange="update()"/>
			</p>

			<div id="qrcode" align="center"></div>
			<p></p>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
                <a href="<?= ("javascript:history.go(-1);"); ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Submit') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>

<script type="text/javascript">
<?php $domain = $app->subdomain.'.'.$this->config->item('mobilesite');?>
$(document).ready(function() {
	function applyqr(){
		if(navigator.appName == "Microsoft Internet Explorer"){
			jQuery("#qrcode").qrcode({
					render	: "table",
					text	: "<?= $domain.'/loyalty/venue/'.$venue->id.'/'?>"+document.getElementById("code").value
			});

			$("#qrcode table").css("cursor","pointer");

			$("#qrcode table").click(
				function()
				{
					var popup=window.open('/loyalty/printcode/<?=$venue->id?>/'+document.getElementById("code").value,'_blank', 'height=700,width=900,left=10,top=10,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
				}
			);
		}else{
			jQuery("#qrcode").qrcode({
					text	: "<?= $domain.'/loyalty/venue/'.$venue->id.'/'?>"+document.getElementById("code").value
			});

			$("#qrcode canvas").css("cursor","pointer");

			$("#qrcode canvas").click(
				function()
				{
					var popup=window.open('/loyalty/printcode/<?=$venue->id?>/'+document.getElementById("code").value,'_blank', 'height=700,width=900,left=10,top=10,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
				}
			);
		}
	}

	function update(){
		jQuery("#qrcode").html("");
		applyqr()
	};

	applyqr();
});
</script>