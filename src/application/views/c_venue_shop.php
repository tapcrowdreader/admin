<div>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
<!-- 	<div class="eventinfo">
		<?php if($venue->image1 != '') : ?>
		<div class="eventlogo"><img src="<?=$this->config->item('publicupload') . $venue->image1 ?>" width="50" height="50"></div>
		<?php else : ?>
		<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
		<?php endif; ?>
		<span class="info_title"><?=__('Venue:')?></span> <?= $venue->name ?><br />
		<span class="info_title"><?=__('Address:')?></span> <?= $venue->address ?>
	</div> -->
	<?php if (!$this->session->userdata('mijnevent')): ?>
	<!-- <br/> -->
	<!-- <a href="<?= site_url('venue/remove/'.$venue->id) ?>" class="delete btn" id="deletebtn" style="float:right;margin-top:-65px;margin-right:10px;"><span><?= __("Edit Venue")?></span></a> -->
	<?php if(isset($app) && $app != false && $app->apptypeid != 4) : ?>
    <!-- <a href="<?= site_url('venue/travelinfo/'.$venue->id) ?>" class="travel btn"><span><?=__('Travel info')?></span></a> -->
    <?php endif; ?>
    <?php if($app->apptypeid != 4) : ?>
    <!-- <a href="<?= site_url('schedule/venue/'.$venue->id) ?>" class="travel btn"><span><?=__('Schedule')?></span></a> -->
    <?php endif; ?>
	<!-- <a href="<?= site_url('venue/edit/'.$venue->id) ?>" class="edit btn"><span><?= __("Edit Venue")?></span></a> -->
	<?php endif ?>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#deletebtn').click(function() {
				jConfirm('<?=__('This will <b>remove</b> this venue!<br />! This cannot be undone!')?>', '<?=__('Remove Venue')?>', function(r) {
					if(r == true) {
						window.location = '<?= site_url("venue/remove/".$venue->id)?>';
						return true;
					} else {
						jAlert('<?=__('Venue not removed!')?>', '<?=__('Info')?>');
						return false;
					}
				});
				return false;
			});
		});
	</script>
	<!-- <h2><?=__('Menu items')?></h2> -->
	<a href="dynamiclauncher/add/<?=$venue->id?>/venue" class="btn primary" style="float:right;"><?=__('Add Web Module')?></a>
	<!-- <a href="groups/add/<?=$venue->id?>/venue" class="btn primary" style="float:right;margin-right: 10px;">Add menu item with internal content</a> -->
        <a href="forms/add/<?=$venue->id?>/venue" class="btn primary" style="float:right; margin-right:5px;"><?=__('Add Form Module')?></a>
	<br clear="all" /><br/>
	<div class="modules">
		<?php $activated = 0; ?>
		<?php if(_currentApp()->apptypeid != 4) : ?>
		<?php if ($eventmodules != FALSE): ?>
		<?php
			$mapactive = FALSE;
			foreach ($eventmodules as $module):
				if ($module->name == "Interactive map"):
					if ($module->active):
						$mapactive = TRUE;
					endif;
				endif;
			endforeach;
		?>

		<?php foreach($subflavors as $subflavor): ?>
		<h3 class="packageH2"><?=ucfirst($subflavor->name)?></h3>
			<?php if(!$infoshowed) : ?>
			<?php foreach($launchers as $l) : ?>
				<?php if($l->moduletypeid == 21) : ?>
					<?php if($l->active == 1) : ?>
						<div class="module active">
							<a href="<?= site_url('venue/edit/'.$venue->id) ?>" class="editlink">
								<span class="moduletitle"><?=$l->title?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_info')?>"> <i class="icon-question-sign"></i></span></span>
								<span class="module-availabilty">
									<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
									<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
									<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
								</span>
							</a>
							<!-- <a href="<?= site_url('module/edit/21/venue/'.$venue->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Edit')?>" height="22px" /></a> -->
							<a href="<?= site_url('venue/module/deactivate/'.$venue->id.'/'.$l->moduletypeid) ?>" class="activate delete">&nbsp;</a>
						</div>
					<?php else: ?>
						<div class="module inactive">
							<span class="moduletitle"><?= $l->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_info')?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty-inactive">
								<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
								<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
								<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
							</span>
							<a href="<?= site_url('venue/module/activate/'.$venue->id.'/21') ?>" class="activate add white">&nbsp;</a>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
		<?php $infoshowed = true; ?>
		<?php if(strtolower($subflavor->name) == "plus") : ?>
		<?php endif; ?>
		<?php foreach ($eventmodules as $module): ?>
		<?php if($module->package == $subflavor->name) : ?>
			<?php if($launchers != null && $launchers != false) {
					foreach($launchers as $launcher) {
						if($launcher->moduletypeid == $module->id) {
							$module->title = $launcher->title;
							$module->groupid = $launcher->groupid;
							$module->active = $launcher->active;
						}
					}
				}?>
				<!-- catalogmodule (15) not displayed for now -->
				<?php if($module->id != 21 && $module->id != 22 && $module->id != 23 && $module->id != 24 && $module->id != 25 && $module->id != 26 && $module->id != 27 && $module->id != 4 && $module->id != 30 && $module->id != 31 && $module->id != 0) :?>
				<?php
					$show = false;
				if($app->apptypeid != 5) {
					$show = true;
				}
				?>
					<?php if($module->controller == 'forms') : ?>
						<?php if($launchers != null && $launchers != false): ?>
						<?php foreach ($launchers as $launcher): ?>

						    <?php if ($launcher->moduletypeid == $module->id): ?>

						        <?php if ($launcher->active): ?>
						            <?php $activated++; ?>
						                <div class="module active">
						                    <a href="<?= site_url($module->controller.'/venue/'.$venue->id.'/'.$launcher->id) ?>" class="editlink"><span class="moduletitle"><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
											<span class="module-availabilty">
												<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
											</span>
						                    </a>
						<!-- 					                                <a href="<?= site_url($module->controller.'/venue/'.$venue->id.'/'.$launcher->id) ?>" class="editimage"><img src="img/icons/pencil.png" width="16" height="16" alt="Content" title="Content" class="png" /></a>
						                    <a href="<?= site_url('forms/editlauncher/'.$launcher->id.'/venue/'.$venue->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="Module Settings" title="Module Settings" height="22px" /></a> -->
						                    <a href="<?= site_url('forms/module/deactivate/'.$venue->id.'/'.$launcher->id.'/venue') ?>" class="activate delete">&nbsp;</a>
						                </div>
						        <?php elseif($launcher->title == $module->name && $launcher->title == 'Form'): ?>
						                <div class="module inactive">
						                    <span class="moduletitle"><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
											<span class="module-availabilty-inactive">
												<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
											</span>
						                    <a href="<?= site_url('forms/add/'.$venue->id.'/venue') ?>" id="activateForm" class="activate add white">&nbsp;</a>
						                </div>
						        <?php elseif(!isset($launcher->topurlevent)): ?>
						                <div class="module inactive">
						                    <span class="moduletitle"><?= $launcher->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
											<span class="module-availabilty-inactive">
												<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
											</span>
						                    <a href="<?= site_url('forms/module/activate/'.$venue->id.'/'.$launcher->id.'/venue') ?>" id="activate" class="activate add white">&nbsp;</a>
						                </div>
						         <?php else:?>
						                <div class="module inactive">
						                    <span class="moduletitle"><?= $launcher->title;?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
											<span class="module-availabilty-inactive">
												<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
												<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
											</span>
						                    <a href="javascript:void(0);" id="activateForm" onClick="return getEmailId('<?= site_url('formtemplate/add/'.$venue->id.'/'.$launcher->moduletypeid) ?>', 'venue');" class="activate add white">&nbsp;</a>
						                </div>
						        <?php endif ?>
						    <?php endif ?>
						<?php endforeach ?>
						<?php endif ?>

				<?php elseif($show) : ?>
	            <?php if($module->id == 50 || $module->id == 51 || $module->id == 52) : ?>
					<?php if ($module->active): ?>
						<div class="module active">
							<a href="<?= site_url('groups/view/'.$module->groupid.'/venue/'.$venue->id.'/catalog')?>" class="editlink">
								<span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty">
								<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
								<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
								<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
							</span>
							</a>
							<a href="<?= site_url('venue/module/deactivate/'.$venue->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>
						</div>
					<?php else: ?>
						<div class="module inactive">
							<span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty-inactive">
								<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
								<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
								<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
							</span>
							<a href="<?= site_url('venue/module/activate/'.$venue->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
						</div>
					<?php endif; ?>
				<?php elseif ($module->active): ?>
					<?php $activated++; ?>
					<div class="module active">
						<?php if($module->id == 21) : ?>
							<a href="<?= site_url('venue/edit/'.$venue->id.'/') ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty">
								<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
							</span>
							</a>
						<?php else : ?>
							<?php if($module->name == 'Artists') : ?>
								<a href="<?= site_url($module->controller) ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
								<span class="module-availabilty">
									<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
									<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
									<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
								</span>
								</a>
							<?php else: ?>
								<a href="<?= site_url($module->controller.'/venue/'.$venue->id.'/') ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
								<span class="module-availabilty">
									<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
									<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
									<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
								</span>
								</a>
							<?php endif; ?>
						<?php endif; ?>
						<?php if($module->id != 28) : ?>
						<!-- <a href="<?= site_url('module/edit/'.$module->id.'/venue/'.$venue->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" height="22px" /></a> -->
						<?php endif; ?>
						<a href="<?= site_url('venue/module/deactivate/'.$venue->id.'/'.$module->id) ?>" class="activate delete">&nbsp;</a>

					</div>
				<?php else: ?>
					<?php if ($module->name == "POI's on interactive map"): ?>
						<div class="module inactive activatemap">
							<a href="<?= site_url($module->controller.'/venue/'.$venue->id.'/') ?>" class="editlink"><span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty-inactive">
								<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
							</span>
							</a>
							<a href="<?= site_url('venue/module/activate/'.$venue->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
						</div>
						<?php if (!$mapactive): ?>
						<script type="text/javascript" charset="utf-8">
							$(document).ready(function() {
								$('.module.inactive.activatemap a.activate').click(function() {
									jConfirm('<?=__('Make sure you have installed the interactive map!<br />Do you want to install floorplan first?')?>', '<?=__('Install POI-module')?>', function(r) {
										if(r == true) {
											window.location = "<?= site_url('venue/module/activate/'.$venue->id.'/5/') ?>";
											return true;
										} else {
											jAlert('<?=__("POI\'s module not installed!")?>', '<?=__('Info')?>');
											return false;
										}
									});
									return false;
								});
							});
						</script>
						<?php endif ?>
					<?php else: ?>
						<div class="module inactive">
							<span class="moduletitle"><?= $module->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$module->name)?>"> <i class="icon-question-sign"></i></span></span>
							<span class="module-availabilty-inactive">
								<?= ($module->ios == 1) ? '<img src="img/layout2/appstore-active.png" class="availabilty-image" />' : '<img src="img/layout2/appstore-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->android == 1) ? '<img src="img/layout2/android-active.png" class="availabilty-image" />' : '<img src="img/layout2/android-nonactive.png" class="availabilty-image" />' ?>
								<?= ($module->webapp == 1) ? '<img src="img/layout2/html5-active.png" class="availabilty-image" />' : '<img src="img/layout2/html5-nonactive.png" class="availabilty-image" />' ?>
							</span>
							<?php if($module->id != 20) : ?>
							<a href="<?= site_url('venue/module/activate/'.$venue->id.'/'.$module->id) ?>" class="activate add white">&nbsp;</a>
							<?php else: ?>
							<a href="<?= site_url('venue/module/activate/'.$venue->id.'/'.$module->id) ?>" id="activatePush" class="activate add white">&nbsp;</a>
							<?php endif; ?>
						</div>
					<?php endif ?>

				<?php endif ?>
			<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
		<?php endforeach ?>
                <?php /* if (count($module->template)>0):?>
                                <?php
                                foreach($module->template as $frmtemplate):
                                    if($subflavor->name == $frmtemplate->package) : ?>
                                        <div class="module inactive">
                                            <span><?= $frmtemplate->title;?></span>
                                            <a href="javascript:void(0);" id="activateForm" onClick="return getEmailId('<?= site_url('formtemplate/add/'.$venue->id.'/'.$frmtemplate->id) ?>', 'venue');" class="activate add white">&nbsp;</a>
                                        </div>
                                <?php
                                    endif;
                                endforeach;
                                ?>
                <?php endif; */?>


		<?php endforeach; ?>
		<?php foreach($dynamicModules as $dynMod) : ?>
			<?php if ($dynMod->active): ?>
				<div class="module active">
					<a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/venue/'.$venue->id)?>" class="editlink"><span class="moduletitle"><?= $dynMod->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$dynMod->module)?>"> <i class="icon-question-sign"></i></span></span>
						<span class="module-availabilty">
							<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
							<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
							<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
						</span>
					</a>
					<!-- <a href="<?= site_url('dynamiclauncher/edit/'.$dynMod->id.'/venue/'.$venue->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" height="22px" /></a> -->
					<a href="<?= site_url('dynamiclauncher/deactivate/'.$venue->id.'/venue/'.$dynMod->id) ?>" class="activate delete">&nbsp;</a>
				</div>
			<?php else: ?>
				<div class="module inactive">
					<span class="moduletitle"><?= $dynMod->title ?><span class="tooltip2" rel="tooltip" title="<?= __('tooltip_module_'.$dynMod->module)?>"> <i class="icon-question-sign"></i></span></span>
					<span class="module-availabilty-inactive">
						<?= '<img src="img/layout2/appstore-active.png" class="availabilty-image" />'?>
						<?='<img src="img/layout2/android-active.png" class="availabilty-image" />' ?>
						<?= '<img src="img/layout2/html5-active.png" class="availabilty-image" />' ?>
					</span>
					<a href="<?= site_url('dynamiclauncher/activate/'.$venue->id.'/venue/'.$dynMod->id) ?>" class="activate add white">&nbsp;</a>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
		<?php else: ?>
			<p><?=__('No modules found for venue.')?></p>
		<?php endif ?>
		<?php endif; ?>
		<!-- Show extra modules but not allow acivation untill fully tested -->
	</div>

        <script>
            function getEmailId(jumpUrl, type)
            {
                var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;

                jPrompt('Enter your email address. You will receive an email for each submission.', '', 'Please enter your email', function(r)
                {
                    if(r != null){
                        if(!r){jAlert('Please enter valid email.', 'Error'); return false;}

                        if(regMail.test(r) == true){
                            window.location = jumpUrl + '/' + r + '/' + type;
                        }
                        else
                            jAlert('Invalid email.', 'Error');
                        }
                });

                return false;
            }
        </script>
</div>