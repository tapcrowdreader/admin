<h1><?= __('Quiz Report') ?></h1>
<p><?= __('Click the results for more detailed information'); ?></p>
<table>
	<tr>
		<th>Name</th>
		<th>Email</th>
		<th>Address</th>
		<th>Score</th>
		<th>Time</th>
		<th>Extra</th>
	</tr>
	<?php foreach($submissions as $s) : ?>
		<tr class="submissionRow" id="<?=$s->id?>">
			<td><?= $s->name ?></td>
			<td><?= $s->email ?></td>
			<td><?= $s->address ?></td>
			<td><?= $s->score ?></td>
			<td><?= $s->submissiontimestamp ?></td>
			<td><?= $s->extra ?></td>
		</tr>
		<tr class="questionsRow" id="questions_<?=$s->id?>">
			<td colspan="5">
				<table>
					<tr>
						<th>Question</th>
						<th>Answer</th>
						<th>Correct?</th>
						<th>Score</th>
					</tr>
					<?php foreach($s->questions as $q) : ?>
						<tr>
							<td><?= $q->question ?></td>
							<td><?= $q->quizquestionoptionid != 0 ? $q->optiontext : $q->quizquestionanswer ?></td>
							<td><?= $q->answeriscorrect ? 'Yes' : 'No' ?></td>
							<td><?= $q->score ?></td>
						</tr>
					<?php endforeach; ?>
				</table>
			</td>
		</tr>
	<?php endforeach; ?>
</table>
<style>
.questionsRow {
	display:none;
}
.submissionRow {
	cursor: pointer;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$(".submissionRow").click(function() {
		$(".questionsRow").css('display', 'none');
		$("#questions_"+$(this).attr('id')).css('display', 'table-row');
	});
});
</script>