<script type="text/javascript" charset="utf-8">
	function iframeLoaded() {
		$('#loaderimage').css('visibility', 'hidden');
	}
</script>
<div id="tabs">

	<div id="preview" class="tab<?php
		if(isset($_SERVER['SERVER_NAME']) && $_SERVER['SERVER_NAME'] == 'admin.tom.tapcrowd.com') echo ' nokia' ?>"><?php

	$app = _currentApp();
	$url = $this->config->item('mobilesite');

	if($this->config->item('mobilesite.use_subdomain') === false) {
		$app->subdomain = '';
	}

	if($this->iframeurl != null) {
		$domain = (empty($app->subdomain)? '' : $app->subdomain . '.') . $this->config->item('mobilesite');

		$ts = time();
		$hash = sha1("tcadm2012".$ts);
		$url = sprintf('http://%s/%s?frame=true&topurl=%s&appid=%d&ph=%s&ts=%s', $domain, $this->iframeurl, urlencode(site_url()), $app->id, $hash, $ts);
	}

	if($app != false) {
		if(empty($this->iframeurl)) { ?>
			<div style="height:460px;">&nbsp;</div><?php
		} else { ?>
		<?php $scroll = 'auto';
		if(stristr($this->iframeurl, 'event/index') || stristr($this->iframeurl, 'venue/index')) {
			$scroll = 'hidden';
		}  ?>
			<div id="loaderimage" style="background-image: url('img/loading-thumb.gif'); height: 39px; left:170px; position: relative; top: 250px;width: 41px; ">&nbsp;</div>
			<iframe id="iframe" scrolling="<?=$scroll?>" frameborder="0" onload="iframeLoaded()" src="<?= $url ?>" width="320" height="523"></iframe><?php
		} ?>
<!-- 		<div style="margin-top:100px;">&nbsp;</div>
		<h4><a href="<?= site_url('buildapp/iphone/'.$app->id);?>" target="_self" style="border:none;"><img src="img/button-previewoniphone.png" /></a></h4>
 		<h4><a href="<?= site_url('buildapp/android/'.$app->id);?>" target="_self" style="border:none;"><img src="img/button-previewonandroid.png" /></a></h4>
		<h4><a href="" onclick="window.open('http://<?= $domain ?>', '<?=__('Preview')?>', 'width=320,height=480,location=no,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no,scrollbars=yes'); return false" style="border:none;">
			<img src="img/button-previewwebapp.png" /></a>
		</hadmin>
		<br /><br />
		<h4><a href="<?= site_url('submit/app/'.$app->id);?>" target="_self" style="border:none;"><img src="img/button-submitapp.png" /></a></h4> -->
	<?php } ?>
	</div>
</div>