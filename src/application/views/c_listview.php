<?php
// Event or Venue ??
$type = 'event';
if(isset($event)) {
	$settingstypeid = $event->id;
}
if(isset($venue)) {
	$type = 'venue';
	$event = $venue;
	$settingstypeid = $venue->id;
}
if(isset($typeapp)) {
	$type = 'app';
	$settingstypeid = $app->id;
}

?>
<?php if(isset($contentflavor) && $contentflavor == true) : ?>
	<h1><?= $app->name ?> <?= __('App') ?></h1>
	<div class="eventinfo">
		<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
		<span class="info_title"><b><?= __('Name:') ?></b></span> <?= $app->name ?><br />
		<?php if(isset($flavor) && $flavor != false) : ?>
		<span class="info_title"><b><?= __('Flavor:') ?></b></span> <?= $flavor->name ?><br />
	<?php endif; ?>
	<br/>
	<div class="button_bar">
		<?php if (!$this->session->userdata('mijnevent')): ?>
		<a href="<?= site_url('apps/delete/'.$app->id) ?>" class="delete btn btn-danger" id="deletebtn"><i class="icon-remove icon-white"></i> <span> <?= __('Remove app') ?></span></a>
		<a href="<?= site_url('apps/view/'.$app->id) ?>" class="btn" id="managebtn"><span><?= __('Manage app structure') ?></span></a>
	<?php endif ?>
	<br clear="all"/>
</div>
</div>
<br />
<?php endif; ?>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<?php if($this->session->flashdata('event_error') != ''): ?>
	<div class="error fadeout"><?= $this->session->flashdata('event_error') ?></div>
<?php endif ?>
<div class="listview">
	<?php
	$items = array('sessions','sessiongroups','news','exhibitors','brands','categories','schedule','attendees','poi','catalog', 'catalogbrands', 'catalogcategories', 'cataloggroups', 'artists', 'sponsors', 'sponsorgroups', 'push', 'ad', 'formscreens', 'formfields', 'festivalinfo','coupons','messages');
	if (!isset($venue)) {
		$newname = array(
			'sessions'              => 'session',
			'sessiongroups' => 'sessiongroup',
			'news'                  => 'news item',
			'exhibitors'    => 'exhibitor',
			'brands'                => 'brand',
			'categories'    => 'category',
			'schedule'              => 'schedule',
			'attendees'             => 'attendee',
			'poi'                   => 'POI',
			'catalog'       => 'Catalog item',
			'catalogs'      => 'Catalog item',
			'catalogbrands' => 'catalogbrand',
			'catalogcategories' => 'catalogcategory',
			'cataloggroups' => 'cataloggroup',
			'artists'               => 'artist',
			'sponsors'      => 'sponsor',
			'sponsorgroups' => 'sponsorgroup',
			'push'                  => 'Push Notification',
			'services'              => 'Service',
			'projects'              => 'Project',
			'careers'               => 'Career',
			'ad'                    => 'Ad',
			'citycontent'   => 'Content',
			'group'                 => 'Group',
			'forms'                 => 'Form',
			'formscreens'   => 'Screen',
			'formfields'            => 'Field',
			'festivalinfo'  => 'Info',
			'findmytent'    => 'Find my tent',
			'findfbfriends' => 'Find my friends',
			'coupons'               => 'coupon',
			'findmycar'    => 'Find my car',
			'messages'		=> 'message'
			);

if(isset($app) && $app->apptypeid == '4') {
	$newname['artists'] = 'Teammember';
}
} else {
	$newname = array(
		'sessions'              => 'session',
		'sessiongroups' => 'sessiongroup',
		'news'                  => 'news item',
		'exhibitors'    => 'catalog-item',
		'brands'                => 'brand',
		'categories'    => 'category',
		'schedule'              => 'schedule',
		'attendees'             => 'attendee',
		'poi'                   => 'POI',
		'catalog'       => 'Catalog item',
		'catalogs'      => 'Catalog item',
		'catalogbrands' => 'catalogbrand',
		'catalogcategories' => 'catalogcategory',
		'cataloggroups' => 'cataloggroup',
		'artists'               => 'artist',
		'sponsors'      => 'sponsor',
		'sponsorgroups' => 'sponsorgroup',
		'push'                  => 'Push Notification',
		'services'              => 'Service',
		'projects'              => 'Project',
		'careers'               => 'Career',
		'ad'                    => 'Ad',
		'citycontent'   => 'Content',
		'group'                 => 'Group',
		'forms'                 => 'Form',
		'formscreens'   => 'Screen',
		'formfields'            => 'Field',
		'festivalinfo'  => 'Info',
		'findmytent'    => 'Find my tent',
		'findfbfriends' => 'Find my friends',
		'coupons'               => 'coupon',
		'findmycar'    => 'Find my car',
		'messages'		=> 'message'
		);

if(isset($app) && $app->apptypeid == '4') {
	$newname['artists'] = 'Teammember';
}
}
?>
<?php if($this->uri->segment(1) == 'groups' && isset($launcher) && !empty($launcher)) : ?>
	<h1 style="float:left;"><?= $launcher->title ?></h1>
<?php elseif($this->uri->segment(1) == 'groups'): ?>
	<h1 style="float:left;"><?= $group->name ?></h1>
<?php elseif($this->uri->segment(1) == 'formscreens') : ?>
	<h1 style="float:left;"><?= __('Screens of "%s"', $form->title) ?></h1>
<?php elseif($this->uri->segment(1) == 'formfields' && $form->singlescreen == 1) : ?>
	<h1 style="float:left;"><?= __('Fields of "%s"', $form->title) ?></h1>
<?php elseif($this->uri->segment(1) == 'formfields' && $form->singlescreen == 0) : ?>
	<h1 style="float:left;"><?= __('Fields of "%s"', $formscreen->title) ?></h1>
<?php elseif($this->uri->segment(1) == 'section') : ?>
	<h1 style="float:left;"><?= __('Section "%s"', $section->title); ?></h1>
<?php else: ?>
	<h1 style="float:left;"><?= __(ucfirst( isset($newname[$this->uri->segment(1)]) ? $newname[$this->uri->segment(1)] : $this->uri->segment(1))); ?><?php if($this->uri->segment(1) != 'findfbfriends' && $this->uri->segment(1) != 'findmyfriends' && $this->uri->segment(1) != 'findmycar') { echo 's'; } ?></h1>
<?php endif; ?>

<?php if($this->uri->segment(1) == 'forms' || $this->uri->segment(1) == 'formscreens' || $this->uri->segment(1) == 'formfields' || $this->uri->segment(1) == 'ad' || $this->uri->segment(1) == 'push') : ?>
	<!--                    <a href="<?= site_url('module/editByController/'.$this->uri->segment(1).'/'.$type.'/'.$settingstypeid.'/'.$launcherid) ?>" class="edit btn" style="float:right;"><i class="icon-wrench"></i> <?= __('Module Settings')?></a><br style="margin-bottom:15px;"/> -->
<?php elseif($this->uri->segment(1) == 'catalog' && $moduletype) : ?>
	<a href="<?= site_url('module/edit/'.$moduletype->id.'/'.$type.'/'.$settingstypeid) ?>" class="edit btn" style="float:right;margin-bottom:5px;"><i class="icon-wrench"></i> <?= __('Module Settings')?></a><br style="margin-bottom:15px;clear:both;"/>
<?php elseif($this->uri->segment(1) == 'groups') : ?>
	<?php if(isset($launcher) && !empty($launcher)) : ?>
		<a href="<?= site_url('module/editLauncher/'.$launcher->id) ?>" class="edit btn" style="float:right;margin-bottom:5px;"><i class="icon-wrench"></i> <?= __('Module Settings')?></a><br style="margin-bottom:15px;clear:both;"/>
	<?php endif; ?>
<?php elseif($this->uri->segment(1) == 'festivalinfo') : ?>
	<a href="<?= site_url('module/editByController/about/'.$type.'/'.$settingstypeid.'/0') ?>" class="edit btn" style="float:right;margin-bottom:5px;"><i class="icon-wrench"></i> <?= __('Module Settings')?></a><br style="margin-bottom:15px;clear:both;"/>
<?php elseif($this->uri->segment(1) != 'section') : ?>
	<a href="<?= site_url('module/editByController/'.$this->uri->segment(1).'/'.$type.'/'.$settingstypeid.'/0') ?>" class="edit btn" style="float:right;margin-bottom:5px;"><i class="icon-wrench"></i> <?= __('Module Settings')?></a><br style="margin-bottom:15px;clear:both;"/>
<?php endif; ?>

<?php if(isset($contentflavor) && $contentflavor == true) : ?>
	<div class="groupchooser">
		<?php if (isset($newstags) && $newstags != false): ?>
		<select name="newstag" id="newstag" class="groupselect">
			<option value=""><?= __('All Tags') ?></option>
			<?php foreach ($newstags as $t): ?>
			<option value="<?= $t->id ?>" <?= ($this->uri->segment(4) == $t->id) ? "selected" : "" ?>><?= $t->tag ?></option>
		<?php endforeach ?>
	</select>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#newstag').change(function(){
			window.location = "<?= site_url('news/app') . "/" . $this->uri->segment(3) ?>/" + $('#newstag').val();
		});
	});
	</script>
<?php else: ?>
<?php endif ?>
</div>
<?php endif; ?>
<?php if ($this->uri->segment(1) == 'sessions'): ?>
	<div class="groupchooser">
		<?php if (isset($sessgroups) && $sessgroups != false): ?>
		<select name="sessgroup" id="sessgroup" class="groupselect">
			<option value=""><?= __('All sessiongroups') ?></option>
			<?php foreach ($sessgroups as $group): ?>
			<option value="<?= $group->id ?>" <?= ($this->uri->segment(4) == $group->id) ? "selected" : "" ?>><?= $group->name ?></option>
		<?php endforeach ?>
	</select>
	<?php if (!isset($venue)): ?>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#sessgroup').change(function(){
			window.location = "<?= site_url('sessions/event') . "/" . $this->uri->segment(3) ?>/" + $('#sessgroup').val() + "/";
		});
	});
	</script>
<?php else: ?>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#sessgroup').change(function(){
			window.location = "<?= site_url('sessions/venue') . "/" . $this->uri->segment(3) ?>/" + $('#sessgroup').val() + "/";
		});
	});
	</script>
<?php endif; ?>
<?php endif ?>
</div>
<?php elseif ($this->uri->segment(1) == 'sponsors'): ?>
	<div class="groupchooser">
		<?php if (isset($sponsorgroups) && $sponsorgroups != false): ?>
		<select name="sponsorgroup" id="sponsorgroup" class="groupselect">
			<option value=""><?= __('All sponsorgroups') ?></option>
			<?php foreach ($sponsorgroups as $group): ?>
			<option value="<?= $group->id ?>" <?= ($this->uri->segment(4) == $group->id) ? "selected" : "" ?>><?= $group->name ?></option>
		<?php endforeach ?>
	</select>
	<?php if (!isset($venue)): ?>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#sponsorgroup').change(function(){
			window.location = "<?= site_url('sponsors/event') . "/" . $this->uri->segment(3) ?>/" + $('#sponsorgroup').val() + "/";
		});
	});
	</script>
<?php else: ?>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#sponsorgroup').change(function(){
			window.location = "<?= site_url('sponsors/venue') . "/" . $this->uri->segment(3) ?>/" + $('#sponsorgroup').val() + "/";
		});
	});
	</script>
<?php endif; ?>
<?php endif ?>
</div>
<?php elseif($this->uri->segment(1) == 'brands' || $this->uri->segment(1) == 'categories'): ?>
	<div class="groupchooser">
		<select name="types" id="types" class="groupselect">
			<option value="brands" <?= ($this->uri->segment(1) == 'brands') ? "selected" : "" ?>><?= __('Brands') ?></option>
			<option value="categories" <?= ($this->uri->segment(1) == 'categories') ? "selected" : "" ?>><?= __('Categories') ?></option>
		</select>
		<?php if (!isset($venue)): ?>
		<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#types').change(function(){
				window.location = "<?= base_url() ?>" + $('#types').val() + "/event/<?= $this->uri->segment(3) ?>";
			});
		});
		</script>
	<?php else: ?>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#types').change(function(){
			window.location = "<?= base_url() ?>" + $('#types').val() + "/venue/<?= $this->uri->segment(3) ?>";
		});
	});
	</script>
<?php endif; ?>
</div>
<?php elseif($this->uri->segment(1) == 'catalogbrands' || $this->uri->segment(1) == 'catalogcategories'): ?>
	<div class="groupchooser">
		<select name="types" id="types" class="groupselect">
			<option value="catalogbrands" <?= ($this->uri->segment(1) == 'catalogbrands') ? "selected" : "" ?>><?= __('Brands') ?></option>
			<option value="catalogcategories" <?= ($this->uri->segment(1) == 'catalogcategories') ? "selected" : "" ?>><?= __('Categories') ?></option>
		</select>
		<?php if (!isset($venue)): ?>
		<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#types').change(function(){
				window.location = "<?= base_url() ?>" + $('#types').val() + "/event/<?= $this->uri->segment(3) ?>";
			});
		});
		</script>
	<?php else: ?>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#types').change(function(){
			window.location = "<?= base_url() ?>" + $('#types').val() + "/venue/<?= $this->uri->segment(3) ?>";
		});
	});
	</script>
<?php endif; ?>
</div>
<?php elseif($this->uri->segment(1) == 'catalog' && $this->uri->segment(5) == false && $catalogcategoriesgroupid == false) : ?>
	<div class="groupchooser">
		<select name="cataloggroup" id="cataloggroup" class="groupselect">
			<option value="0"><?= __('All groups') ?></option>
			<?php foreach ($cataloggroups as $group): ?>
			<option value="<?= $group->id ?>" <?= ($this->uri->segment(4) == $group->id) ? "selected" : "" ?>><?= $group->name ?></option>
		<?php endforeach ?>
	</select>

	<?php if (!isset($venue)): ?>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#cataloggroup').change(function(){
			window.location = "<?= site_url('catalog/event') . "/" . $this->uri->segment(3) ?>/" + $('#cataloggroup').val() + "/";
		});
	});
	</script>
<?php else: ?>
	<?php if($this->uri->segment(5) != null) : ?>
	<?php //for catalogtypes like services and projects ?>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#cataloggroup').change(function(){
			window.location = "<?= site_url('catalog/venue') . "/" . $this->uri->segment(3) ?>/" + $('#cataloggroup').val() + "/<?= $this->uri->segment(5) ?>";
		});
	});
	</script>
<?php else : ?>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#cataloggroup').change(function(){
			window.location = "<?= site_url('catalog/venue') . "/" . $this->uri->segment(3) ?>/" + $('#cataloggroup').val() + "/";
		});
	});
	</script>
<?php endif; ?>
</div>
<?php endif; ?>
<?php endif ?>
<?php //groups ?>
<div style="float:left;">
	<?php if($this->uri->segment(1) == 'catalog' && $this->uri->segment(5) == false): ?>
	<?php if(isset($venue)) : ?>
	<?php if(isset($catalogcategoriesgroupid) && $catalogcategoriesgroupid != false) : ?>
	<a href="<?= site_url('groups/view/' . $catalogcategoriesgroupid . '/venue/' . $venue->id. '/catalogs') ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Categories')?></a>
<?php else: ?>
	<a href="<?= site_url('cataloggroups/venue/' . $venue->id) ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Categories') ?></a>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<?php if($this->uri->segment(1) == 'sessions') : ?>
	<?php if(isset($event)) : ?>
	<a href="<?= site_url('sessiongroups/event/' . $event->id) ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Sessiongroups') ?></a>
<?php endif; ?>
<?php endif; ?>
<?php if($this->uri->segment(1) == 'sponsors') : ?>
	<?php if(isset($venue)) : ?>
	<a href="<?= site_url('sponsorgroups/venue/' . $venue->id) ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Sponsorgroups') ?></a>
<?php elseif(isset($event)) : ?>
	<a href="<?= site_url('sponsorgroups/event/' . $event->id) ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Sponsorgroups') ?></a>
<?php endif; ?>
<?php endif; ?>
<?php if($this->uri->segment(1) == 'sponsorgroups') : ?>
	<?php if(isset($venue)) : ?>
	<a href="<?= site_url('sponsors/venue/' . $venue->id) ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Sponsors') ?></a>
<?php elseif(isset($event)) : ?>
	<a href="<?= site_url('sponsors/event/' . $event->id) ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Sponsors') ?></a>
<?php endif; ?>
<?php endif; ?>
<?php if($this->uri->segment(1) == 'exhibitors') : ?>
	<?php if(isset($event)) : ?>
	<?php if(isset($exhibitorcategoriesgroupid) && $exhibitorcategoriesgroupid != false && isset($exhibitorbrandsgroupid) && $exhibitorbrandsgroupid != false) : ?>
	<a href="<?= site_url('groups/view/' . $exhibitorbrandsgroupid . '/event/' . $event->id . '/exhibitors') ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Brands')?></a>
	<a href="<?= site_url('groups/view/' . $exhibitorcategoriesgroupid . '/event/' . $event->id. '/exhibitors') ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Categories')?></a>
<?php else: ?>
	<a href="<?= site_url('brands/event/' . $event->id) ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Brands') ?></a>
	<a href="<?= site_url('categories/event/' . $event->id) ?>" class="edit btn"><i class="icon-pencil"></i> <?= __('Edit Categories') ?></a>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
</div>

<?php if($this->uri->segment(1) == 'groups') : ?>
	<div class="buttonbar" style="float:left;width:100%;">
	<?php else: ?>
	<div class="buttonbar">
	<?php endif; ?>
        <?php //groups and items for exhibitors
        $buttonname = __('Add Group');
        if($this->uri->segment(1) == 'groups') : ?>
        <?php $lastparent = 0; ?>
        <?php $i = 0; ?>

        <?php foreach($parents as $parent) : ?>
        <?php if($parent->name == "exhibitorbrands") {
        	$parent->name = __('Brands');
        	if($i == 0) {
        		$buttonname = __('Add Brand');
        		$catbrand = __('Brand');
        		$i++;
        	}
        } elseif($parent->name == "exhibitorcategories" || $parent->name == 'catalogcategories') {
        	$parent->name = __('Categories');
        	if($i == 0) {
        		$buttonname = __('Add Category');
        		$catbrand = __('Category');
        		$i++;
        	}
        } ?>
    <?php endforeach; ?>
    <?php if($group->name == "exhibitorbrands") {
    	$group->name = __('Brands');
    	$buttonname = __('Add Brand');
    	$catbrand = __('Brand');
    } elseif($group->name == "exhibitorcategories"|| $group->name == 'catalogcategories') {
    	$group->name = __('Categories');
    	$buttonname = __('Add Category');
    	$catbrand = __('Category');
    } ?>
    <?php $lastparent = $group->id; ?>
    <br clear="all" />
    <div>
    	<?php if($this->uri->segment(6) == 'catalogs' || $this->uri->segment(6) == 'catalog') : ?>
    	<a href="<?= site_url($this->uri->segment(1) . '/add/'.$typeid.'/'.$type.'/'.$lastparent.'/'.$object.'/'.$basegroupid) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?=$buttonname ?> </a>
    	<a href="<?= site_url('catalog/add/'.$typeid.'/'.$type.'/catalog/'.$lastparent.'/'.$basegroupid) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i> <?= __('Add Item') ?></a>
    <?php elseif($this->uri->segment(6) == 'exhibitors') : ?>
    <a href="<?= site_url($this->uri->segment(1) . '/add/'.$typeid.'/'.$type.'/'.$lastparent.'/'.$object.'/') ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?=$buttonname ?> </a>
    <a href="<?= site_url('exhibitors/add/'.$typeid.'/'.$type.'/'.$lastparent) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i> <?= __('Add Item') ?></a>
<?php else: ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/'.$typeid.'/'.$type.'/'.$lastparent.'/'.$object.'/') ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?=$buttonname ?> </a>
	<a href="<?= site_url('groupitems/add/'.$typeid.'/'.$type.'/'.$lastparent) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i> <?= __('Add Item')?></a>
<?php endif; ?>

</div>
<?php elseif($this->uri->segment(1) == 'section') : ?>
	<a href="<?= site_url('news/addtosection/'.$section->id) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i> <?= __('Add Item') ?></a>
	<a href="<?= site_url('section/rsslist/'.$section->id) ?>" class="add btn primary" style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i> <?= __('RSS')?>
	</a>
<?php elseif (in_array($this->uri->segment(1), $items)): ?>
	<?php if($this->uri->segment(1) == "artists") : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/'.$app->id) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i> <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a>
<?php elseif($this->uri->segment(1) == 'news') : ?>
	<?php if($this->uri->segment(2) == 'app') : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $app->id . '/app') ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a>
	<?php if($flavor->id == 5) : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/rsslist/' . $app->id . '/' . $type) ?>" class="add btn primary"><?=__('RSS')?></a>
<?php endif; ?>
<?php elseif($this->uri->segment(2) == 'section') : ?>
	<a href="<?= site_url('section/addnews/' . $section->id) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a>
<?php else : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $event->id . '/' . $type) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a> <a href="<?= site_url($this->uri->segment(1) . '/rsslist/' . $event->id . '/' . $type) ?>" class="add btn primary"><?=__('RSS')?></a>
<?php endif; ?>
<?php elseif($this->uri->segment(1) == 'cataloggroups') : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $event->id . '/' . $type) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a>
	<a href="<?= site_url('catalog/venue/'.$venue->id) ?>" class="edit btn" style="float:right;"><i class="icon-pencil"></i><?= __('Edit catalog items') ?></a>

<?php elseif($this->uri->segment(1) == 'catalog') : ?>
	<?php //for services and projects ?>
	<?php if($this->uri->segment(5) != null) : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $event->id . '/' . $type . '/' . $this->uri->segment(5)) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(5)]) ?></a>
	<!-- <a href="<?= site_url($this->uri->segment(1) . '/rsslist/' . $event->id . '/' . $type . '/' . $this->uri->segment(5)) ?>" class="add btn primary">RSS</a> -->
<?php else : ?>
	<?php if(isset($event)) : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $event->id . '/' . $type) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add '. $newname[$this->uri->segment(1)]) ?></a>
<?php elseif(isset($venue)) : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $venue->id . '/' . $type) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add '. $newname[$this->uri->segment(1)]) ?></a>
<?php endif; ?>
<!-- <a href="<?= site_url($this->uri->segment(1) . '/rsslist/' . $event->id . '/' . $type) ?>" class="add btn primary">RSS</a> -->
<?php endif; ?>
<?php elseif($this->uri->segment(1) != 'formfields' && $this->uri->segment(1) != 'formscreens') : ?>
	<?php if(isset($event)) : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $event->id . '/' . $type) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add '. $newname[$this->uri->segment(1)]) ?></a>
<?php elseif(isset($venue)) : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $venue->id . '/' . $type) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ') ?> <?= $newname[$this->uri->segment(1)] ?></a>
<?php endif; ?>
<?php if(isset($app) && $this->uri->segment(1) == 'push' && $this->uri->segment(2) == 'app') : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $app->id . '/app') ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a>
<?php endif; ?>
<?php if(isset($app) && $this->uri->segment(1) == 'ad' && $this->uri->segment(2) == 'app') : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/' . $app->id . '/app') ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a>
<?php endif; ?>
<?php endif; ?>
<?php elseif($this->uri->segment(1) == 'favourites'): ?>
	<!-- <a href="<?= site_url($this->uri->segment(1) . '/notify/' . $event->id) ?>" class="add btn primary"><?= __('Notify Users') ?></a> -->
<?php endif ?>
<?php if($this->uri->segment(1) == 'citycontent') : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/'.$app->id) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a>
<?php endif; ?>

<?php if($this->uri->segment(1) == 'forms') : ?>
	<?php if($data == false || count($data)<1) : ?>
<!--                    <?php if(isset($event)) : ?>
                        <a href="<?= site_url($this->uri->segment(1) . '/add/' . $event->id . '/' . $type) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?>
</a>
                        <?php elseif(isset($venue)) : ?>
                        <a href="<?= site_url($this->uri->segment(1) . '/add/' . $venue->id . '/' . $type) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?>
</a>
                        <?php elseif(isset($app)) : ?>
                        <a href="<?= site_url($this->uri->segment(1) . '/add/' . $app->id . '/app') ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?>
</a>
<?php endif; ?> -->
<?php endif; ?>
<?php endif; ?>

<?php if($this->uri->segment(1) == 'formscreens') : ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/'.$form->id) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a>
	<?= isset( $formSettingBtns ) ? $formSettingBtns : '' ;?>
<?php endif; ?>

<?php if($this->uri->segment(1) == 'formfields') : ?>
	<?php if($form->singlescreen != 1) : ?>
	<a href='<?= site_url("formscreens/edit/" . $formscreen->id) ?>' class="btn edit" ><i class="icon-pencil"></i> <?= __('Screen Settings') ?></a>
	<?php endif; ?>
	<a href="<?= site_url($this->uri->segment(1) . '/add/'.$formscreen->id) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Add ' . $newname[$this->uri->segment(1)]) ?></a>
	<?= isset( $formSettingBtns ) ? $formSettingBtns : '' ;?>
<?php endif; ?>

<?php // excelimport ?>
<?php if ($this->uri->segment(1) == 'sessions'): ?>
	<a href="<?= site_url('excelimport/sessions/'.$event->id) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  Import Excel</a>
<?php endif; ?>

<?php if ($this->uri->segment(1) == 'sessions'): ?>
	<a href="<?= site_url('import/sessions/'.$event->id.'/'.$this->uri->segment(2)) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Import') ?></a>
<?php endif; ?>

<?php if ($this->uri->segment(1) == 'exhibitors'): ?>
	<a href="<?= site_url('import/exhibitors/'.$event->id.'/'.$this->uri->segment(2)) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Import') ?></a>
<?php endif; ?>

<?php if ($this->uri->segment(1) == 'attendees'): ?>
	<a href="<?= site_url('import/attendees/'.$event->id.'/'.$this->uri->segment(2)) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Import') ?></a>
<?php endif; ?>

<?php if ($this->uri->segment(1) == 'sponsors'): ?>
	<a href="<?= site_url('import/sponsors/'.$event->id.'/'.$this->uri->segment(2)) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?= __('Import') ?></a>
<?php endif; ?>

</div>
<br clear="all">
<table id="listview" class="display zebra-striped">
	<thead>
		<tr>
			<?php foreach ($headers as $h => $field): ?>
			<?php if ($h == 'Order'): ?>
			<th class="data order"><?= $h ?></th>
		<?php else: ?>
		<th class="data"><?= $h ?></th>
	<?php endif ?>
<?php endforeach; ?>
<?php if($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'users' && _isAdmin()): ?>
	<th>&nbsp;</th>
	<th>&nbsp;</th>
	<th>&nbsp;</th>
<?php elseif($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'apps' && _isAdmin()): ?>
	<th>&nbsp;</th>
<?php elseif($this->uri->segment(1) == 'forms') : ?>
	<th><?=__('Edit')?></th>
	<th><?=__('Content')?></th>
	<th><?=__('Reports')?></th>
	<th><?=__('Delete')?></th>
<?php elseif($this->uri->segment(1) == 'formscreens') : ?>
	<th><?=__('Edit')?></th>
	<th><?=__('Delete')?></th>
<?php elseif($this->uri->segment(1) != 'apps'): ?>
	<th width="17"></th>
	<?php if($this->uri->segment(1) != 'formfields') : ?>
	<th width="17"></th>
	<?php endif; ?>
	<th width="17"></th>
<?php endif?>
</tr>
</thead>
<tbody>
	<?php if ($data != FALSE): ?>
	<?php foreach($data as $row): ?>
	<tr>
		<?php foreach ($headers as $h => $field): ?>
		<?php if ($field != null && !empty($field)) : ?>
		<?php if($this->uri->segment(1) == 'groups') : ?>
		<?php if($row->groupitem == true) : ?>
		<?php if($object == 'catalog') : ?>e
		<td><a href='<?= site_url($object."/edit/" . $row->itemid . '/' . $type . '/catalog/' . $row->groupid.'/'.$basegroupid) ?>' ><img src="img/file.png" class="imageInListview" /><?= $row->{$field} ?></a></td>
	<?php else: ?>
	<?php if($object == '') : ?>
	<?php if(strtolower($row->itemtable) == 'catalog') : ?>
	<td><a href='<?= site_url(strtolower($row->itemtable)."/edit/" . $row->itemid . '/' . $type . '/' . strtolower($row->itemtable) . '/'  . $row->groupid) ?>' ><img src="img/file.png" class="imageInListview" /><?= $row->{$field} ?></a></td>
<?php else: ?>
	<td><a href='<?= site_url(strtolower($row->itemtable)."/edit/" . $row->itemid . '/' . $type . '/' . $row->groupid) ?>' ><img src="img/file.png" class="imageInListview" /><?= $row->{$field} ?></a></td>
<?php endif; ?>
<?php else: ?>
	<td><a href='<?= site_url($object."/edit/" . $row->itemid . '/' . $type . '/' . $object . '/'  . $row->groupid) ?>' ><img src="img/file.png" class="imageInListview" /><?= $row->{$field} ?></a></td>
<?php endif; ?>
<?php endif; ?>
<?php else : ?>
	<?php if(isset($object)) : ?>
	<td><a href='<?= site_url("groups/view/" . $row->id . '/' . $type . '/' . $typeid . '/' . $object) ?>' ><img src="img/folder.png" class="imageInListview" /><?= $row->{$field} ?></a></td>
<?php else: ?>
	<td><a href='<?= site_url("groups/view/" . $row->id . '/' . $type . '/' . $typeid) ?>' ><img src="img/folder.png" class="imageInListview" /><?= $row->{$field} ?></a></td>
<?php endif; ?>
<?php endif; ?>
<?php else :?>
	<?php if($field == "datum" && $this->uri->segment(1) == 'news') : ?>

	<td><?= date("F j, Y, H:m:s", strtotime($row->{$field})) ?></td>
<?php else: ?>
	<td><?= $row->{$field} ?></td>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<?php endforeach ?>
<?php if($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'users' && _isAdmin()): ?>
	<td class="icon"><a href='<?= site_url("admin/users/edit/" . $row->id) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url("admin/apps/user/" . $row->id) ?>' ><img src="img/icons/application_cascade.png" width="16" height="16" alt="<?=__('Apps from user')?>" title="<?=__('Apps from user')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url("admin/users/loginas/" . $row->id) ?>' ><img src="img/icons/user_go.png" width="16" height="16" alt="<?=__('Login as')?>" title="<?=__('Login as')?>" class="png" /></a></td>
<?php elseif($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'apps' && _isAdmin()): ?>
	<td class="icon"><a href='<?= site_url("admin/apps/edit/" . $row->id) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>

<?php elseif($this->uri->segment(1) == 'artists' && ($this->uri->segment(2) == 'app' || $this->uri->segment(2) == false)) : ?>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/edit/" . $row->id . '/app/'. $app->id) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->id . '/' . $this->uri->segment(1) . '/app') ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/delete/" . $row->id . '/app/'. $app->id) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
<?php elseif($this->uri->segment(1) != 'apps'): ?>
	<?php if($this->uri->segment(1) != 'push'): ?>
	<?php if($this->uri->segment(5) != null) : ?>

	<?php if($this->uri->segment(1) == 'groups' && $row->groupitem == true) : ?>
	<?php if($object == 'catalog') : ?>
	<td class="icon"><a href='<?= site_url($object."/edit/" . $row->itemid . '/' . $type . '/catalog/' . $row->groupid.'/'.$basegroupid) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->itemid . '/catalog/' . $type. '/' . $row->groupid) ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url("groupitems/delete/" . $row->id . '/' . $type . '/' . $this->uri->segment(5).'/catalog') ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
<?php else : ?>
	<?php if($object == '') : ?>
	<?php if(strtolower($row->itemtable) == 'catalog') : ?>
	<td class="icon"><a href='<?= site_url(strtolower($row->itemtable)."/edit/" . $row->itemid . '/' . $type . '/' . strtolower($row->itemtable) . '/' . $row->groupid) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>
<?php else: ?>
	<td class="icon"><a href='<?= site_url(strtolower($row->itemtable)."/edit/" . $row->itemid . '/' . $type . '/' . $row->groupid) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>
<?php endif; ?>
<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->itemid . '/' . $this->uri->segment(1) . '/' . $type . '/' . $row->groupid) ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>
<td class="icon"><a href='<?= site_url(strtolower($row->itemtable)."/delete/" . $row->itemid . '/' . $type . '/' . $this->uri->segment(5)) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
<?php else: ?>
	<td class="icon"><a href='<?= site_url($object."/edit/" . $row->itemid . '/' . $type . '/' . $object . '/' . $row->groupid) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->itemid . '/' . $this->uri->segment(1) . '/' . $type . '/' . $row->groupid) ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url($object."/delete/" . $row->itemid . '/' . $type . '/' . $this->uri->segment(5)) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
<?php endif; ?>
<?php endif; ?>
<?php elseif($this->uri->segment(1) == 'groups' && $row->groupitem != true) : ?>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/edit/" . $row->id . '/' . $type . '/' . $this->uri->segment(5) . '/' . $row->parentid . '/' . $object) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->id . '/' . $this->uri->segment(1) . '/' . $type . '/' .$object) ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url("groups/delete/" . $row->id . '/' . $type.'/'.$this->uri->segment(3).'/'.$this->uri->segment(6)) ?>' class="adeletegroup"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
<?php else: ?>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/edit/" . $row->id . '/' . $type . '/' . $this->uri->segment(5)) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="<?=__('Edit')?>" title="<?=__('Edit')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->id . '/' . $this->uri->segment(1) . '/' . $type) ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/delete/" . $row->id . '/' . $type. '/' . $this->uri->segment(5)) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
<?php endif; ?>


<?php elseif($this->uri->segment(1) == 'forms') : ?>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/edit/" . $row->id . '/' . $type . '/' . $this->uri->segment(5) . '/' . $row->parentid . '/' . $object) ?>' ><img src="img/Settings.png" width="22" height="22" alt="Bewerken" title="Bewerken" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url($childcon . '/' .$confunc.'/'. $row->id . '/') ?>'><img src="img/icons/pencil.png" width="16" height="16" alt="Content" title="Content" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url('forms/export/'. $row->id . '/') ?>' id="downloadreport"><img src="img/icons/report.png" width="16" height="16" alt="Report" title="Report" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/delete/" . $row->id . '/' . $type) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="Verwijderen" alt="Verwijderen" class="png" /></a></td>
<?php elseif($this->uri->segment(1) == 'formscreens') : ?>
	<td class="icon"><a href='<?= site_url($childcon . '/' .$confunc.'/'. $row->id . '/') ?>'><img src="img/icons/pencil.png" width="16" height="16" alt="Content" title="Content" class="png" /></a></td>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/delete/" . $row->id . '/' . $type) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="Verwijderen" alt="Verwijderen" class="png" /></a></td>
<?php elseif($this->uri->segment(1) == 'section' && $this->uri->segment(2) == 'news') : ?>
	<td></td>
	<td class="icon"><a href='<?= site_url("section/editnews/" . $row->id . '/'.$section->id) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="Edit" title="Edit" class="png" /></a></td>

	<td class="icon"><a href='<?= site_url("section/deletenews/" . $row->id  . '/'.$section->id) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="Delete" alt="Delete" class="png" /></a></td>
<?php else : ?>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/edit/" . $row->id . '/' . $type) ?>' ><img src="img/icons/pencil.png" width="16" height="16" alt="Edit" title="Edit" class="png" /></a></td>
	<?php if($this->uri->segment(1) != 'formfields') : ?>
	<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->id . '/' . $this->uri->segment(1) . '/' . $type) ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="Duplicate" title="Duplicate" class="png" /></a></td>
	<?php endif; ?>
	<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/delete/" . $row->id . '/' . $type) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="Delete" alt="Delete" class="png" /></a></td>
<?php endif; ?>
<?php else : ?>
	<td></td>
	<td></td>
<?php endif; ?>
<?php endif?>
</tr>
<?php endforeach ?>
<?php else: ?>

<?php endif ?>
</tbody>
</table>
</div>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('.adelete').click(function(e) {
		var delurl = $(this).attr('href');
		jConfirm('<?= __("Are you sure you want to delete this row?<br />This cannot be undone!") ?>', '<?= __("Remove Entry") ?>', function(r) {
			if(r == true) {
				window.location = delurl;
				return true;
			} else {
				return false;
			}
		});
		return false;
	});

	$('#downloadreport').click(function(e) {
		var downloadexcelurl = $(this).attr('href');
		jConfirm('Are you sure you want to download the Excel Report?', 'Download Report', function(r) {
			if(r == true) {
				window.location = downloadexcelurl;
				return true;
			} else {
				return false;
			}
		});
		return false;
	});

	$('.adeletegroup').click(function(e) {
		var delurl = $(this).attr('href');
		jConfirm('<?= __("Are you sure you want to delete this categorie/brand?<br />All items below will be removed as well!") ?>', '<?= __("Remove Entry") ?>', function(r) {
			if(r == true) {
				window.location = delurl;
				return true;
			} else {
				return false;
			}
		});
		return false;
	});

	$('.adeleteform').click(function(e) {
		var delurl = $(this).attr('href');
		jConfirm('<?= __("Are you sure you want to delete this form?") ?>', '<?= __("Remove Form") ?>', function(r) {
			if(r == true) {
				window.location = delurl;
				return true;
			} else {
				return false;
			}
		});
		return false;
	});

	$("#sidebar").height($('#content').height());

});
</script>