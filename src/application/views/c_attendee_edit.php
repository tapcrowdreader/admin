<h1><?= __('Edit %s', $attendee->name)?></h1>
<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
    <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
    <div class="error"><p><?= __('Something went wrong with the upload.').' <br/>'.__('Maybe the image size exceeds the maximum width or height</p>') ?></div>
    <?php endif ?>
	<fieldset>
		<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
			<label for="name"><?= __('Name:') ?></label>
			<input type="text" name="name" id="name" value="<?= htmlspecialchars_decode(set_value('name', $attendee->name), ENT_NOQUOTES) ?>" />
		</p>
		<p <?= (form_error("fname") != '') ? 'class="error"' : '' ?>>
			<label for="fname"><?= __('First Name:') ?></label>
			<input type="text" name="fname" id="fname" value="<?= htmlspecialchars_decode(set_value('fname', $attendee->firstname), ENT_NOQUOTES) ?>" />
		</p>
		<?php if(externalIds()) : ?>
		<p <?= (form_error("external_id") != '') ? 'class="error"' : '' ?>>
			<label for="external_id"><?= __('External id:') ?></label>
			<input type="text" name="external_id" id="external_id" value="<?= htmlspecialchars_decode(set_value('external_id', $attendee->external_id), ENT_NOQUOTES) ?>" />
		</p>
		<?php endif; ?>
		<p <?= (form_error("company") != '') ? 'class="error"' : '' ?>>
			<label for="company"><?= __('Company:') ?></label>
			<input type="text" name="company" id="company" value="<?= htmlspecialchars_decode(set_value('company', $attendee->company), ENT_NOQUOTES) ?>" />
		</p>
        <?php foreach($languages as $language) : ?>
		<p <?= (form_error("function_".$language->key) != '') ? 'class="error"' : '' ?>>
			<label for="function_<?=$language->key?>"><?= __('Function') ?> <?= '(' . $language->name . ')'; ?>:</label>
			<?php $trans = _getTranslation('attendees', $attendee->id, 'function', $language->key) ?>
			<input type="text" name="function_<?=$language->key?>" id="function_<?=$language->key?>" value="<?= set_value('function_'.$language->key, ($trans != null) ? $trans : $attendee->function) ?>" />
		</p>
        <?php endforeach; ?>
		<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
			<label for="email"><?= __('Email:') ?></label>
			<input type="text" name="email" id="email" value="<?= htmlspecialchars_decode(set_value('email', $attendee->email), ENT_NOQUOTES) ?>" />
		</p>
		<p <?= (form_error("linkedin") != '') ? 'class="error"' : '' ?>>
			<label for="linkedin"><?= __('Linkedin:') ?></label>
			<input type="text" name="linkedin" id="linkedin" value="<?= htmlspecialchars_decode(set_value('linkedin', $attendee->linkedin), ENT_NOQUOTES) ?>" />
			<span class="note" style="width:500px;">http://www.linkedin.com/in/profilename</span>
		</p>
		<p <?= (form_error("facebook") != '') ? 'class="error"' : '' ?>>
			<label for="facebook"><?= __('Facebook:') ?></label>
			<input type="text" name="facebook" id="facebook"  value="<?= htmlspecialchars_decode(set_value('facebook', $attendee->facebook), ENT_NOQUOTES) ?>" />
		</p>
		<p <?= (form_error("twitter") != '') ? 'class="error"' : '' ?>>
			<label for="twitter"><?= __('Twitter:') ?></label>
			<input type="text" name="twitter" id="twitter"  value="<?= htmlspecialchars_decode(set_value('twitter', $attendee->twitter), ENT_NOQUOTES) ?>" />
		</p>
		<p <?= (form_error("phone") != '') ? 'class="error"' : '' ?>>
			<label for="phone"><?= __('Phone:') ?></label>
			<input type="text" name="phone" id="phone" value="<?= htmlspecialchars_decode(set_value('phone', $attendee->phonenr), ENT_NOQUOTES) ?>" />
		</p>
		<p <?= (form_error("country") != '') ? 'class="error"' : '' ?>>
			<label for="phone"><?= __('Country:') ?></label>
			<input type="text" name="country" id="country" value="<?= set_value('country', $attendee->country) ?>" />
		</p>
        <?php if(isset($languages) && !empty($languages)) : ?>
        <?php foreach($languages as $language) : ?>
		<p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
			<label for="description_<?=$language->key?>"><?= __('Description') ?> <?= '(' . $language->name . ')'; ?>:</label>
			<?php $trans = _getTranslation('attendees', $attendee->id, 'description', $language->key) ?>
			<textarea name="description_<?php echo $language->key; ?>" id="description_<?=$language->key?>"><?= htmlspecialchars_decode(set_value('description_'.$language->key, ($trans != null) ? $trans : $attendee->description ), ENT_NOQUOTES) ?></textarea>
		</p>
        <?php endforeach; ?>
        <?php else : ?>
		<p <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
			<label for="description"><?= __('Description:') ?></label>
			<textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description"><?= htmlspecialchars_decode(set_value('description_'. $app->defaultlanguage, $attendee->description), ENT_NOQUOTES) ?></textarea>
		</p>
        <?php endif; ?>
		<p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
			<label for="image"><?= __('Image') ?>:</label>
			<span class="evtlogo" <?php if($attendee->imageurl != ''){ ?>style="width:50px; height:50px; background:transparent url('<?= image_thumb($attendee->imageurl, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
			<input type="file" name="imageurl" id="imageurl" value="" class="logoupload" style="width:360px;" /><br />
			<br clear="all"/>
			<span class="hintImage"><?= __('Maximum size: %sx%spx, jpg or png',2000,2000) ?></span>
			<?php
			$type = 'event';
			if(isset($venue)) {
				$type = 'venue';
			}
			?>
			<?php if($attendee->imageurl != '' && file_exists($this->config->item('imagespath') . $attendee->imageurl)){ ?><span><a href="<?= site_url('attendees/removeimage/'.$attendee->id.'/'.$type) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
		</p><br clear="all"/>
		<div class="row">
			<label for="tags">Tags:</label>
			<ul id="mytags" name="mytagsul"></ul>
		</div>
		<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
			<label><?=__('Order:')?></label>
			<input type="text" name="order" value="<?= set_value('order', $attendee->order) ?>" id="order"><br/>
		</p>
		<?php if($maincat != false && !empty($catshtml)) : ?>
			<br />
			<label style="margin-bottom:10px;"><?= __('Item groups') ?>:</label>
			<ul>
				<?php foreach($catshtml as $option) : ?>
				<?php $option = str_replace('attendeecategories', 'Attendees', $option); ?>
				<?= $option ?>
				<?php endforeach; ?>
			</ul><br />
		<?php endif; ?>
        <?php foreach($metadata as $m) : ?>
			<?php foreach($languages as $lang) : ?>
				<?php if(_checkMultilang($m, $lang->key, $app)): ?>
					<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
						<?= _getInputField($m, $app, $lang, $languages, 'attendees', $attendee->id); ?>
					</p>
				<?php endif; ?>
			<?php endforeach; ?>
        <?php endforeach; ?>
<!-- 		<p <?= (form_error("premium") != '') ? 'class="error"' : '' ?> class="premiump">
			<input type="checkbox" name="premium" class="checkboxClass premiumCheckbox" <?= ($premium) ? 'checked="checked"' : '' ?> id="premium"><?=__(' Premium?')?><br clear="all" />
		</p>
		<p <?= (form_error("order") != '') ? 'class="error"' : '' ?> id="extralinep">
			<label><?=__('Premium order:')?></label>
			<input type="text" name="premiumorder" value="<?= set_value('premiumorder', $premium->sortorder) ?>" id="premiumorder" /><br clear="all" />
			<label style="clear:both;"><?=__('Extra line:')?></label>
			<input type="text" name="extraline" value="<?= set_value('extraline', (isset($premium->extraline)) ? $premium->extraline : '') ?>" id="extraline"><br clear="all" />
		</p> -->
		<?php if($confbagactive) : ?>
		<p <?= (form_error("confbag") != '') ? 'class="error"' : '' ?>>
			<label for="confbag"><?= __('Conference bag content:') ?></label>
			<input type="file" name="confbagcontent" id="confbagcontent" />
			<br clear="all" />
			<?php foreach($attendeeconfbag as $c) : ?>
				<span style="width:auto;"><?=substr($c->documentlink, strrpos($c->documentlink,'/') +1,strlen($c->documentlink))?><a href="<?=site_url('confbag/removeitem/'.$c->id.'/event/attendees');?>"  style="border-bottom:none;margin-left:5px;"><img src="img/icons/delete.png" width="16" height="16" title="<?= __('Delete')?>" alt="<?= __('Delete')?>" class="png" /></a></span><br clear="all"/>
			<?php endforeach;?>
		</p>
		<?php endif; ?>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="catshidden" value="<?= $currentCats ?>" id="catshidden">
		<input type="hidden" name="postback" value="postback" id="postback">
		<button type="submit" class="btn primary"><?= __('Save') ?></button>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>
<script type="text/javascript">
$(document).ready(function(){
	$("#mytags").tagit({
		initialTags: [<?php if(isset($postedtags) && $postedtags != false && !empty($postedtags)) { 
			foreach($postedtags as $val){ 
				echo '"'.$val.'", ';
			} 
		} elseif (isset($tags) && $tags != false) {
			foreach($tags as $val){ 
				echo "\"".$val->tag."\"," ;
			}
		} ?>]
	});

	var tags = new Array();
	var i = 0;
	<?php foreach($apptags as $tag) : ?>
	tags[i] = "<?=$tag->tag?>";
	i++;
	<?php endforeach; ?>
	$("#mytags input.tagit-input").autocomplete({
		source: tags
	});
});
</script>