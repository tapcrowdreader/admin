<?php if(!isset($_GET['blank'])) : ?>
<a href="<?= site_url($this->uri->uri_string()) ?>?blank" class="btn" target="_blank" style="float:right;"><?= __('Open in new window')?></a>
<a href="<?= site_url('formscreens/resultlayout/'.$formscreen->id) ?>" class="btn" target="_self" style="float:right;margin-right:10px;"><i class="icon-pencil"></i> <?= __('Result page settings')?></a>
<br clear="all" />
<?php endif; ?>
<?php foreach($results as $id => $res) : ?>
	<?php $answers = array(); ?>
	<?php $possiblevalues = explode(',', $res[0]->possiblevalues); ?>
	<h1><?= $res[0]->label ?></h1>
	<p><span class="resultVotes"><?= __('Total votes: %s',count($res)) ?></span></p>
	<div style="height: 600px; width: 100%; position: relative;" id="pie<?= $id ?>" class="pie">

	</div>
<?php endforeach; ?>
<script type="text/javascript">
$(document).ready(function(){
    <?php foreach($results as $id => $res) : ?>
   		var s1 = new Array();
		<?php $answers = array(); ?>
		<?php 
		$possiblevalues = explode(',', $res[0]->possiblevalues);
		foreach($possiblevalues as &$p) {
			$p = trim($p);
		}
		 ?>
		<?php foreach($res as $r) {
			$r->value = trim($r->value);
			if(in_array($r->value, $possiblevalues)) {
				$answers[] = trim($r->value); 
			}
		} ?>
		var i = 0;
		<?php foreach($possiblevalues as $possiblevalue) : ?>
			<?php $countvalue = array_count_values($answers); ?>
			s1[i] = ['<?= str_replace("'", "\'", $possiblevalue) ?>', <?= $countvalue[$possiblevalue] != 0 ? (int)($countvalue[$possiblevalue] / count($answers) * 100) : 0 ?>];
			i++;
		<?php endforeach; ?> 
		draw(<?= $id ?>);
    <?php endforeach; ?>

    function draw(id) {
		 var plot = $.jqplot('pie'+id, [s1], {
		        grid: {
		            drawBorder: false,
		            drawGridlines: false,
		            background: '#ffffff',
		            shadow:false
		        },
		        axesDefaults: {
		             
		        },
		        seriesDefaults:{
		            renderer:$.jqplot.PieRenderer,
		            rendererOptions: {
		                showDataLabels: true
		            }
		        },
		        legend: {
		            show: true,
		            // rendererOptions: {
		            //     numberRows: 1
		            // },
		            location: 's'
		        },
		        seriesColors: [ 
			        <?php foreach($colors as $c) : ?>
			        	"<?= $c ?>",
		        	<?php endforeach; ?>
		        	]
		    });
    }
});
</script>