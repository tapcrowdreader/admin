<div>
	<h1><?= __('Add Module') ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
	        <?php foreach($languages as $language) : ?>
	        <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
	            <label for="title"><?= __('Title '. '(' . $language->name . ')'); ?>:</label>
	            <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$language->key), ENT_NOQUOTES) ?>" />
	        </p>
	        <?php endforeach; ?>
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Order:') ?></label>
                <input type="text" name="order" id="order" value="<?= htmlspecialchars_decode(set_value('order'), ENT_NOQUOTES) ?>" />
            </p>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:') ?><a href="faq#launchericons" style="border:none;"><i class="icon-question-sign"></i></a></label>
				<span class="hintAppearance"><?= __('Image must be in png format, width: %s px, height: %s px',140,140) ?></span>
				<input type="file" name="image" id="image" value="" class="logoupload" />
				<br clear="all"/>
			</p>
			<?php if($app->familyid == 3) : ?>
			<p>
				<label for="type"><?= __('Attach events, poi\'s or website to module?') ?></label>
				<input class="checkboxClass eventsorvenues" type="radio" name="eventsorvenues" value="events" checked /> <?= __('Events') ?> <br />
				<input class="checkboxClass eventsorvenues" type="radio" name="eventsorvenues" value="venues" /> <?= __('Poi\'s') ?><br/>
				<input class="checkboxClass eventsorvenues" type="radio" name="eventsorvenues" value="website" /> <?= __('Website') ?><br clear="all"/>
			</p>
            <p <?= (form_error("extraparams") != '') ? 'class="error"' : '' ?> style="display:none;"  id="extraparamsp">
       			<label><?= __('Url:')?></label>
            	<input type="text" name="url" id="url" value="<?= htmlspecialchars_decode(set_value('url'), ENT_NOQUOTES) ?>" /><br/>
            	<input class="checkboxClass" type="checkbox" style="float:left; display:inline;" name="extraparams" />
                <label for="extraparams" style="float:left;width:450px;"><?= __('Add the appid and deviceid in the querystring of the URL') ?></label>
                <br clear="all" />
            </p>
			<?php endif; ?>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<?php if(isset($venue)) : ?>
					<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<?php elseif(isset($event)) : ?>
					<a href="<?= site_url('event/view/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<?php elseif(isset($app)) : ?>
					<a href="<?= site_url('apps/view/'.$app->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<?php endif; ?>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
$('.eventsorvenues').change(function() {
  if($(this).val() != 'website') {
  	$('#extraparamsp').hide();
  } else {
  	$('#extraparamsp').show();
  }
});
</script>