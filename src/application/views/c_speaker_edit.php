<div>
	<h1><?= __('Edit %s', $speaker->name)?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
            <div class="error"><p><?= __('Something went wrong with the image upload.')?> <br/> <?= __('Maybe the image size exceeds the maximum width or height')?></p></div>
            <?php endif ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="name">Name <?= '(' . $language->name . ')'; ?>:</label>
                <?php $trans = _getTranslation('speaker', $speaker->id, 'name', $language->key); ?>
                <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?=html_entity_decode(set_value('name_'.$language->key, ($trans != null) ? $trans : $speaker->name))?>" />
            </p>
            <?php endforeach; ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="description">Description <?= '(' . $language->name . ')'; ?>:</label>
                <?php $trans = _getTranslation('speaker', $speaker->id, 'description', $language->key); ?>
                <textarea name="description_<?php echo $language->key; ?>" id="description"><?= html_entity_decode(set_value('description_'.$language->key, ($trans != null) ? $trans : $speaker->description)) ?></textarea>
            </p>
            <?php endforeach; ?>
			<p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl"><?= __('Image:')?></label>
				<?php if($speaker->imageurl != '' && file_exists($this->config->item('imagespath') . $speaker->imageurl)){ ?>
				<span class="evtlogo" style="width:50px; height:50px; background:transparent url('<?= image_thumb($speaker->imageurl, 50, 50) ?>') no-repeat center center;">&nbsp;</span>
				<?php } ?>
				<input type="file" name="imageurl" id="imageurl" value="" /><br />
				<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
				<br clear="all" />
				<?php if($speaker->imageurl != '' && file_exists($this->config->item('imagespath') . $speaker->imageurl)){ ?><span><a href="<?= site_url('speakers/removeimage/'.$speaker->id.'/'.$type) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?><br clear="all" />
			</p>
			<p class="clear <?= (form_error("company") != '') ? 'error' : '' ?>">
				<label><?= __('Company:')?></label>
				<input type="text" name="company" value="<?= set_value('company', $speaker->company) ?>" id="company">
			</p>
			<p <?= (form_error("function") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Function:')?></label>
				<input type="text" name="function" value="<?= set_value('function', $speaker->function) ?>" id="function">
			</p>
			<div class="row">
				<label for="tags">Tags:</label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order', $speaker->order) ?>" id="order">
			</p>
			<?php if($maincat != false && !empty($catshtml)) : ?>
				<br />
				<label style="margin-bottom:10px;"><?= __('Item groups') ?>:</label>
				<ul>
					<?php foreach($catshtml as $option) : ?>
					<?php $option = str_replace('speakercategories', 'Speakers', $option); ?>
					<?= $option ?>
					<?php endforeach; ?>
				</ul><br />
			<?php endif; ?>
            <?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<?php if(_checkMultilang($m, $lang->key, $app)): ?>
						<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
							<?= _getInputField($m, $app, $lang, $languages, 'speaker', $speaker->id); ?>
						</p>
					<?php endif; ?>
				<?php endforeach; ?>
            <?php endforeach; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('speakers/event/'.$event->id) ?>" class="btn"><?= __('Cancel')?></a>
				<button type="submit" class="btn primary"><?= __('Save')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#mytags").tagit({
		initialTags: [<?php if(isset($postedtags) && $postedtags != false && !empty($postedtags)) { 
			foreach($postedtags as $val){ 
				echo '"'.$val.'", ';
			} 
		} elseif (isset($tags) && $tags != false) {
			foreach($tags as $val){ 
				echo "\"".$val->tag."\"," ;
			}
		} ?>]
	});

	var tags = new Array();
	var i = 0;
	<?php foreach($apptags as $tag) : ?>
	tags[i] = "<?=$tag->tag?>";
	i++;
	<?php endforeach; ?>
	$("#mytags input.tagit-input").autocomplete({
		source: tags
	});
});
</script>