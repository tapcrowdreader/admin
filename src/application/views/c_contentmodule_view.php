<h1><?= __('Screen "%s"', $contentmodule->title)  ?></h1>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<div>
	<br />
<!-- 	<a href="<?= site_url('contentmodule/importrss/'.$contentmodule->id) ?>" class="add btn primary" style="margin-left:10px;margin-bottom:10px;">
		<img src="img/icons/add.png" width="16" height="16" alt="TapCrowd App"> <?= __('Import Rss')?>
	</a>   -->      
	<a href="<?= site_url('section/add/'.$contentmodule->id) ?>" class="add btn primary" style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i>  <?= __('Add Section')?>
	</a>
</div>
<br clear="all" />
<div class="modules">
	<?php foreach($sections as $s) : ?>
	<div class="module active">
		<a href="<?= site_url('section/news/'.$s->id) ?>" class="editlink" style="width:520px;"><span><?=$s->title?> <span class="hintAppearance" style="float:left;display:inline !important;width:100px;">(<?=$s->sectiontype?>)</span></span></a>
		<a href="<?= site_url('section/edit/'.$s->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?= __('Edit')?>" height="22px" /></a>
		<a href="<?= site_url('section/delete/'.$s->id)?>" style="border-bottom: none;" class="deletesection" ><img height="22px" alt="<?= __('Del')?>" src="img/icons/delete22.png"></a>
	</div>
	<?php endforeach; ?>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.deletesection').click(function(e) {
			var delurl = $(this).attr('href');
			jConfirm('<?= __('Are you sure you want to delete this section?') . '<br />' . __('This cannot be undone!') ?>', '<?= __('Remove Entry') ?>', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					return false;
				}
			});
			return false;
		});
	});
</script>
