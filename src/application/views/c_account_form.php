<?php
/**
 * Account edit page
 */

if(!$account instanceOf \Tapcrowd\Account) { ?>
	<div class="alert alert-error"><strong><?=__('Error')?> </strong><?=__('Invalid account')?></div><?php
	return;
}

// $channels->ksort();
$post_url = (empty($account->accountId))? 'add' : 'edit/'.$account->accountId;
?>

<div class="btn-group pull-right">
	<a href="account/browse/" class="btn"><i class="icon-wrench"></i> <?=__('Manage Accounts')?></a>
</div>

<h1><?=(empty($account->accountId))? __('Add %s', $settings->singular) : __('Edit %s', $settings->singular) ?></h1>

<form class="form-horizontal" method="POST" action="account/<?=$post_url?>" autocomplete="off">
<?php /* DISABLE CHANNEL SELECT
	<div class="control-group">
		<label class="control-label" for="inputName"><?=__('Channel')?></label>
		<div class="controls">
			<select name="account_channelId" id="inputChannel">
			<?php
			$iterator = $channels->getIterator();
			for( $iterator = $channels->getIterator(); $iterator->valid(); $iterator->next() ) {
				$channel = $iterator->current();
				$selected = ($account->channelId == $channel->id)? ' selected="true"' : '';
				echo "<option value=\"{$channel->id}\"$selected>{$channel->name}</option>";
			} ?>
			</select>
			<span class="help-block">
				<em><?=__('Note: Channel is not changable at this time')?></em>
			</span>
		</div>
	</div>
*/ ?>
	<div class="control-group">
		<label class="control-label" for="inputName"><?=__('Name')?></label>
		<div class="controls">
			<input class="span6" type="text" id="inputName" placeholder="<?=__('Name')?>" name="account_fullname" value="<?=$account->fullname?>">
			<span class="help-block alert alert-error">
				<strong><?=__('Error:')?></strong> <?=__('Name is too long')?>
			</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputEmail"><?=__('Email')?></label>
		<div class="controls controls-row">
			<input class="span6" type="text" id="inputEmail" placeholder="<?=__('Email')?>" name="account_email" value="<?=$account->email?>">
			<span class="help-block alert alert-error">
				<strong><?=__('Error:')?></strong> <?=__('Invalid Email entered')?>
			</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputLogin"><?=__('Login')?></label>
		<div class="controls">
			<input class="span6" type="text" id="inputLogin" placeholder=""  name="account_login" value="<?=$account->login?>">
			<span class="help-block alert alert-error">
				<strong><?=__('Error:')?></strong> <?=__('Invalid Login entered')?>
			</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputPassword"><?=__('Password (twice)')?></label>
		<div class="controls controls-row">
			<input class="span6" type="password" id="inputPassword" placeholder="<?=__('Password')?>" name="account_pass">
			<br /><br />
			<input class="span6" type="password" id="inputPassword2" placeholder="<?=__('Password (again)')?>" name="account_pass2">
			<span class="help-block alert alert-info">
				<strong><?=__('Note:')?></strong> <?=__('If not entered, password will NOT be overwritten')?>
			</span>
			<span class="help-block alert alert-error msg-passwordsdonotmatch">
				<strong><?=__('Error:')?></strong> <?=__('Passwords do not match')?>
			</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label"><?=__('Access Rights')?></label>
		<div class="controls controls-row">
			<label class=" inline hide"><input type="checkbox" /></label><?php
			foreach($apps as $app) {
				$checked = in_array($app->id, $rights)? ' checked="true"' : '';
				?><label class=" inline span2">
					<input type="checkbox" name="account_apprights[]" value="<?=$app->id?>"<?=$checked?>><?=$app->name?>
				</label><?php
			} ?>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<a class="btn" href="account/browse/"><?=__('Cancel')?></a>
			<button type="submit" class="btn btn-primary"><?=__('Save')?></button>
		</div>
	</div>
</form>

<script>
$(document).ready(function()
{

//
// Show/Hide .help-block.alert-error 's
//
$('span.help-block.alert-error').hide();
var errorMsg = getParameterByName('errormsg');
if(errorMsg) $('span.msg-'+errorMsg).show();

});
</script>
