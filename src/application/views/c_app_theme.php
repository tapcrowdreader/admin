<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<a href="<?=site_url('apps/appearance/'.$app->id);?>" class="btn primary" style="float: right;"><?=__('Advanced');?></a>
<h1><?= __('Choose a theme'); ?></h1>
<?php if(isset($_GET['newapp'])) : ?>
<style type="text/css">
ul.themes {
    display: block !important;
}
</style>
<div class="progress progress-striped active">
	<div class="bar" style="width: 100%;"></div>
</div>
<?php endif; ?>
<ul class="themes">
	<?php foreach($themes as $theme) : ?>
		<li>
			<p style="text-align:center;">
				<b class="title"><?= __($theme->name) ?></b>
			</p>
			<img src="<?=$theme->screenshot?>" style="width:200px;height:160px;"/>
			<p style="text-align: center;margin-top:10px;">
			<a href="<?=site_url('apps/applytheme/'.$theme->id.'/'.$app->id);?>" id="<?=$theme->id?>" class="btn primary apply">
				<?=__('Apply Theme'); ?>
			</a>
			</p>
		</li>
	<?php endforeach; ?>
</ul>

<?php if(isset($_GET['newapp'])) : ?>
<a href="<?=site_url('apps/view/'.$app->id)?>" class="btn primary" style="float:right;clear:left;"><?= __('No Theme') ?></a><br clear="all" />
<?php endif; ?>

<?php if(!isset($_GET['newapp'])) : ?>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.apply').click(function() {
			var id = $(this).attr('id');
			jConfirm('<?= __("This will override previous colors and images. Would you like apply this theme?") ?>', '<?= __("Apply Theme") ?>', function(r) {
				if(r == true) {
					window.location = '<?=site_url("apps/applytheme/")?>/'+id+'/'+'<?=$app->id?>';
					return true;
				} else {
					return false;
				}
			});
			return false;
		});
	});
</script>
<?php endif; ?>