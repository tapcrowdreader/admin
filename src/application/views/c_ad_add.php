<div>
	<h1><?=__('Add Item')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label for="name"><?= __('Name:') ?></label>
				<input type="text" name="name" id="name" value="<?= set_value('name') ?>" />
			</p>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:') ?></label>
				<span class="hintAppearance"><?= __('Required size: 640x100') ?></span>
				<input type="file" name="image" id="image" value="" class="logoupload" />
			</p><br />
			<p <?= (form_error("image_ipad") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image iPad:') ?></label>
				<span class="hintAppearance"><?= __('Required size: 500x200') ?></span>
				<input type="file" name="image_ipad" id="image_ipad" value="" class="logoupload" />
			</p><br />
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label for="order"><?=__('Order:')?></label>
				<input type="text" name="order" id="order" value="<?= set_value('order') ?>" />
			</p>
			<p <?= (form_error("time") != '') ? 'class="error"' : '' ?>>
				<label for="time"><?= __('Display time (In Seconds):') ?></label>
				<input type="text" name="time" id="time" value="<?= set_value('time', '5') ?>" />
			</p>
			<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
				<label for="website"><?= __('Website:') ?></label>
				<input type="text" name="website" id="website" value="<?= set_value('website') ?>" />
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>