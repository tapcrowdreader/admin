<style type="text/css" media="screen">
	#import_upload { }
	.uploads { display:block; background:#FFF; padding:10px; }
	.uploads .file { display:block; border-bottom:1px solid #CCC; padding:4px 6px; }
	.uploads a.import { display:inline-block; background:#EEE url('../img/icons/cog_go.png') no-repeat 3px 5px; border:none; padding:3px 6px 3px 25px; margin-left:5px; }
	.uploads a.import:hover { color:#666; background-color:#DDD; }
	.uploads a.delete { display:inline-block; float:none; background:#EEE url('../img/icons/delete.png') no-repeat 3px 5px; border:none; padding:3px 6px 3px 25px; margin-left:5px; }
	.uploads a.delete:hover { color:#666; background-color:#DDD; }

</style>
<div id="import_upload">
	<h2><?=__('New upload: ')?></h2>
	<?php if ($this->session->flashdata("import_feedback") != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata("import_feedback") ?></div>
	<?php endif ?>
	<div class="frmsessions	">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("userfile") != '') ? 'class="error"' : '' ?>>
				<label for="userfile"><?=__('File:')?></label>
				<input type="file" name="userfile" id="userfile" value="" /><br />
				<span class="note" style="width:540px; margin-left:130px;"><?=__('File must be a TapCrowd-valid CSV (;-separated)')?></span>
				<br />
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="save"><?=__('UPLOAD')?></button>
			</div>
		</form>
	</div>
	
	<h2><?=__('Previous uploads')?></h2>
	<div class="uploads">
		<?php
		$this->load->helper('directory');
		$uploaddir = directory_map("upload/import/$type/".$event->id);
		if($uploaddir){
			rsort($uploaddir);
			if(count($uploaddir) > 0) {
				foreach($uploaddir as $file) {
					$date = explode('_', $file);
					?>
					<div class="file">
						<a href="<?= site_url("excelimport/process/$event->id/$type/$file") ?>" class="import"><?=__('Import')?></a>
						<a href="<?= site_url("excelimport/delete/$event->id/$type/$file") ?>" class="delete"><?=__('Delete')?>/a>
						<a href="upload/import/<?= $type ?>/<?= $event->id ?>/<?= $file ?>"><?= $file ?></a>
					</div>
				<?php
				}
			} else {
			?>
			<p><?=__('There are no uploads.')?></p>
			<?php
			}
		} else {
		?>
		<p><?=__('There aren\'t any uploads yet')?></p>
		<?php
		}
		?>
	</div>
</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('a.delete').click(function(e) {
			var delurl = $(this).attr('href');
			jConfirm('<?=__('Are you sure you want to delete this file? <br/> This cannot be undone!')?>', '<?=__('Delete file')?>', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					return false;
				}
			});
			return false;
		});
		
		$('a.import').click(function(e) {
			var delurl = $(this).attr('href');
			jConfirm('<?=__('Are you sure you want to start import?<br />This may affect your app-data!')?>', '<?=__('Import')?>', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					return false;
				}
			});
			return false;
		});
	});
</script>