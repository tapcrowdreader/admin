<style type="text/css" media="screen">
	div.logging { display:block; height:250px; overflow:scroll; background:#FFF; border:10px solid #FFF;  }
	div.info { display:block; height:auto; padding:10px; line-height:22px; background:#FFF; }
	.finalize { display:block; background:#DDD url('../img/icons/cog_go.png') no-repeat 3px 5px; border:#CCC; padding:5px 10px 5px 25px; float:right; color:#666; cursor:pointer; }
</style>
<div>
	<h1><?=__('Import Validation')?></h1>
	<?php if ($errors == 0): ?>
		<h2><?=__('Test-run results')?></h2>
		<div class="info">
			<strong><?=__('# Rows :')?></strong> <?= $aantal ?><br />
			<strong><?=__('Errors :')?></strong> <?= $errors ?>
		</div>
		<h2><?=__('Log')?></h2>
		<div class="logging"><?= $log ?></div>
		
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8">
			<input type="hidden" name="final" value="final" id="final">
			<input type="submit" class="finalize" value="Run import">
		</form>
	<?php else: ?>
		<h2><?=__('Test-run results')?></h2>
		<div class="info">
			<?=__('Errors :')?> <?= $errors ?>
		</div>
	<?php endif ?>
</div>