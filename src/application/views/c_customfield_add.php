<div>
	<?php if(isset($field)) : ?>
		<h1><?=__('Edit %s', $field->name)?></h1>
	<?php else : ?>
		<h1><?=__('Add custom field')?></h1>
	<?php endif; ?>

	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          

			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label for="name"><?= __('Name') ?>:</label>
				<input type="text" name="name" id="name" value="<?= set_value('name', $field->name) ?>" />
			</p>

			<p <?= (form_error("type") != '') ? 'class="error"' : '' ?>>
				<label for="type"><?= __('Type') ?>:</label>
				<select name="type">
					<?php foreach($types as $t) : ?>
					<option value="<?= $t ?>" <?= isset($field) && $field->type == $t ? 'selected="selected"' : '' ?>><?= $t ?></option>
					<?php endforeach; ?>
				</select>
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label for="order"><?=__('Order:')?></label>
				<input type="text" name="order" id="order" value="<?= set_value('order', isset($field) ? $field->sortorder : '1') ?>" />
			</p>
			<?php if(!empty($modules)) : ?>
			<p <?= (form_error("moduletype") != '') ? 'class="error"' : '' ?>>
				<label for="moduletype"><?= __('Module') ?>:</label>
				<select name="moduletype">
					<?php foreach($modules as $m) : ?>
					<option value="<?= $m->moduletypeid ?>" <?= isset($field) && $field->moduletypeid == $m->moduletypeid ? 'selected="selected"' : '' ?>><?= $m->title ?></option>
					<?php endforeach; ?>
				</select>
			</p>
			<?php endif; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= isset($field) ? __('Save') : __('Add custom field') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>