<?php
$allow_sticky_fieldtypes = array(1, 3, 4, 6, 7, 8, 13);
?>
<h1>Edit Field</h1>
<div class="frmformbuilder">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
		<?php if($formfield->formfieldtypeid == 6) { $display = "block";} else {$display = "none";} ?>
        <?php if($formfield->formfieldtypeid == 19) { $display2 = "block";} else {$display2 = "none";} ?>
		<?php if($formfield->formfieldtypeid != 10 || $formfield->formfieldtypeid != 11) { $reqdisplay = "block";} else {$reqdisplay = "none";} ?>
		<?php if($error != ''): ?>
		<div class="error"><?= $error ?></div>
		<?php endif ?>
        			
		<?php if(isset($languages) && !empty($languages)) : ?>
			<?php foreach($languages as $language) : ?>
            <p <?= (form_error("label_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="label">Title <?= '(' . $language->name . ')'; ?>:</label>
                <?php $trans = _getTranslation('formfield', $formfield->id, 'label', $language->key); ?>
                <input type="text" name="label_<?php echo $language->key; ?>" id="label" value="<?= htmlspecialchars_decode(set_value('label_'.$language->key, ($trans != null) ? $trans : $formfield->label ), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
        <?php else : ?>
        
            <p <?= (form_error("label_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="name">Label:</label>
                <input type="text" name="label_<?php echo $app->defaultlanguage; ?>" id="label" value="<?= htmlspecialchars_decode(set_value('label_'.$app->defaultlanguage, _getTranslation('formfield', $formfield->id, 'label', $app->defaultlanguage)), ENT_NOQUOTES) ?>" />
            </p>
        <?php endif; ?>
        
        <p <?= (form_error("formfieldtypeid") != '') ? 'class="error"' : '' ?>>
            <label for="formfieldtypeid">Type:</label>
            <select id="formfieldtypeid" name="formfieldtypeid" >
                <option value="select">Select...</option>
                <?php if(isset($fieldtypes)) { ?>
                    <?php foreach($fieldtypes as $e) : ?>
                        <option value="<?= $e->id ?>" <?= set_select('formfieldtypeid', $e->id, ($e->id == $formfield->formfieldtypeid ? TRUE : '')) ?>><?= $e->name; ?></option>
                    <?php endforeach; ?>
                <?php } ?>
            </select>
        </p>
        
        <?php if(isset($languages) && !empty($languages)) : ?>
            <div id="option_fields" style="display:<?= $display; ?>">
    			<?php /* foreach($languages as $language) : ?>
                <p <?= (form_error("possiblevalues_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="possiblevalues">Possible Values, comma separated <?= '(' . $language->name . ')'; ?>:</label>
                    <?php $trans = _getTranslation('formfield', $formfield->id, 'possiblevalues', $language->key); ?>
                    <input type="text" name="possiblevalues_<?php echo $language->key; ?>" id="possiblevalues" value="<?= htmlspecialchars_decode(set_value('possiblevalues_'.$language->key, ($trans != null) ? $trans : $formfield->possiblevalues ), ENT_NOQUOTES) ?>" />
                </p>
                <?php endforeach; */ ?>
                
                <p class="fieldvalues" style="display:none;">
				<?php foreach($languages as $language) : ?>                
                    <label for="fieldvalue" id="lbl<?= $language->key; ?>"><?=__('Option')?> <span style="float:inherit; font-style:inherit; text-align:inherit; display:inline; margin-right:0px; color:inherit;">0</span> <?= '(' . $language->name . ')'; ?>:</label>
                    <input type="text" name="new_field_option[<?= $language->key; ?>][]" />
                <?php endforeach; ?>
                </p>
                
                <?php $index = 0; 
					foreach($formfield->fieldOptions as $option):
						$index++;
				?>
                	<p class="fieldvalues">
                <?php
						foreach($languages as $language) :  ?>                
						<label for="fieldvalue" id="lbl<?= $language->key; ?>" style="float:left;width:200px;"><?=__('Option')?> <span style="float:none; font-style:inherit; text-align:inherit; display:inline; margin-right:0px; color:inherit;"><?=$index?></span> <?= '(' . $language->name . ')'; ?>:</label>
                        <?php if($form->useflows):?>
                        <label style="display:inline; float:right;width:200px;text-align: right;"><?= __('Screen to load on select') ?></label>
                        <?php endif; ?>
						<?php $trans = _getTranslation('formfieldoption', $option->id, 'value', $language->key); ?>
						<input type="text" class="<?=$option->id?>" style="width:45%;" name="fieldvalue[<?= $language->key?>][<?=$option->id?>][]" value="<?= htmlspecialchars_decode(set_value("fieldvalue[<?= $language->key; ?>][]", ($trans != null) ? $trans : $option->value ), ENT_NOQUOTES) ?>" />
                <?php
						endforeach;
				?>
                		<a href="<?= site_url('formfields/deleteoption/'.$option->id) ?>" style="float:inherit; margin:0px 30px 9px 20px;" class="btn btn-mini btn-danger"><i class="icon-remove icon-white" title="Delete"></i></a>
                        <?php if($formscreens_flows):?>
                        <select name="flowscreen[<?=$app->defaultlanguage?>][<?=$option->id?>][]" style="display:inline; float:right;">
                        	<option value="">Select...</option>
                            <?php
							foreach($formscreens_flows as $flowscreen):
								if($flowscreen->id == $formscreen->id): continue; endif;
							?>
                            <option value="<?=$flowscreen->id?>" <?php if($flowscreen->id == $option->formscreenid) echo 'selected'?>><?=$flowscreen->title?></option>
							<?php endforeach;?>
                        </select>
                        <?php endif;?>
                		</p>
				<?php
					endforeach;
				?>
                <p style="padding-top:0px;"><button type="button" id="add_option" class="add btn primary"><?=__('Add Option')?></button></p>
                
            </div>
            <div id="option_fields2" style="display:<?= $display2 ?>;">
                <p class="fieldvalues2">
                    <?php if($form->useflows):?>
                        <label><?= __('Screen to load on select') ?></label>
                      <select name="flowscreen2[<?=$app->defaultlanguage?>]" style="display:inline; float:left;">
                        <option value="">Select...</option>
                        <?php
                        foreach($formscreens_flows as $flowscreen):
                        ?>
                        <option value="<?=$flowscreen->id?>" <?php if($flowscreen->id == $formfield->fieldOptions[0]->formscreenid) echo 'selected'?>><?=$flowscreen->title?></option>
                      <?php endforeach;?>
                      </select>
                      <br clear="all" />
                  <?php endif;?>
                </p>
            </div>    
            <div id="date_time_option" style="display:none;">
                 <p>
                    <label for="date/time">Time Format:</label>
                    <select name="date-time" id="date-time" onChange="changeTimeFormt()">
                        <option value="am-pm">AM-PM</option>
                        <option value="24hr">24 Hours</option>
                    </select>
                 </p>
             </div>
            <div id="default_values">
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("defaultvalue_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="defaultvalue">Default Value <?= '(' . $language->name . ')'; ?>:</label>
                <?php $trans = _getTranslation('formfield', $formfield->id, 'defaultvalue', $language->key); ?>
                <input type="text" name="defaultvalue_<?php echo $language->key; ?>" id="defaultvalue" value="<?= htmlspecialchars_decode(set_value('defaultvalue_'.$language->key, ($trans != null) ? $trans : $formfield->defaultvalue ), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            </div>
		<?php else : ?>            
            <div id="option_fields" style="display:<?= $display; ?>">            
                <p <?= (form_error("possiblevalues_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="possiblevalues">Possible Values, comma separated:</label>
                    <input type="text" name="possiblevalues_<?php echo $app->defaultlanguage; ?>" id="possiblevalues" value="<?= htmlspecialchars_decode(set_value('possiblevalues_'.$app->defaultlanguage, _getTranslation('formfield', $formfield->id, 'possiblevalues', $app->defaultlanguage)), ENT_NOQUOTES) ?>" />
                </p>
                
            </div>
            <div id="default_values">
            <p <?= (form_error("defaultvalue_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="defaultvalue">Default Value:</label>
                <input type="text" name="defaultvalue_<?php echo $app->defaultlanguage; ?>" id="defaultvalue" value="<?= htmlspecialchars_decode(set_value('defaultvalue_'. $app->defaultlanguage, _getTranslation('formfield', $formfield->id, 'defaultvalue',  $app->defaultlanguage)), ENT_NOQUOTES) ?>" />
            </p>
            </div>
        <?php endif; ?>
		
        
		<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
			<label>Order:</label>
			<input type="text" name="order" value="<?= set_value('order', $formfield->order) ?>" id="order">
		</p>
		            
		<!--<p <?= (form_error("xpos") != '') ? 'class="error"' : '' ?>>
			<label>Xpos:</label>
			<input type="text" name="xpos" value="<?= set_value('xpos', $formfield->xpos) ?>" id="xpos">
		</p>
        
        <p <?= (form_error("ypos") != '') ? 'class="error"' : '' ?>>
			<label>Ypos:</label>
			<input type="text" name="ypos" value="<?= set_value('ypos', $formfield->ypos) ?>" id="ypos">
		</p>
        
        <p <?= (form_error("width") != '') ? 'class="error"' : '' ?>>
			<label>Width:</label>
			<input type="text" name="width" value="<?= set_value('width', $formfield->width) ?>" id="width">
		</p>
        
        <p <?= (form_error("height") != '') ? 'class="error"' : '' ?>>
			<label>Height:</label>
			<input type="text" name="height" value="<?= set_value('height', $formfield->height) ?>" id="height">
		</p>-->
        <div id="required_fields" style="display:<?= $reqdisplay; ?>">
        
        <p  class="checklist <?= (form_error("required") != '') ? 'error' : '' ?>">
			
            <label>Required Field:</label>
		    <input type="checkbox" name="required" value="yes" id="required" <?= set_checkbox('required', $e->id, ("yes" == $formfield->required ? TRUE : '')) ?>>
        </p>
        </div>
        
        <?php if($launcher->moduletypeid != 60 && $launcher->moduletypeid != 61 && $launcher->moduletypeid != 62) : ?>
        <div id="sticky_field" style="display:<?= in_array($formfield->formfieldtypeid, $allow_sticky_fieldtypes) ? 'block' : 'none';?>">
        
        <p  class="checklist">
			
            <label>Sticky:</label>
		    <input type="checkbox" name="sticky" value="1" id="sticky" <?= ($formfield->sticky == 0) ? 'none' : 'checked="checked"'; ?> />
        </p>
        
        </div>
        <?php endif; ?>
        <p <?= (form_error("customproperty") != '') ? 'class="error"' : '' ?>>
            <label>Custom property:</label>
            <input type="text" name="customproperty" value="<?= set_value('customproperty', $formfield->customproperty) ?>" id="customproperty">
            <span class="hintAppearance">e.g. style="background-color=#FF0000" onchange="if (this.value=='') alert('Cannot be empty');0" </span>
        </p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<a href="<?= site_url('formfields/formscreen/'.$formscreen->id) ?>" class="btn">Cancel</a>
			<button type="submit" class="btn primary">Save</button>
			<br clear="all" />
		</div>
		<br clear="all" />
	</form>
</div>

<script type="text/javascript" charset="utf-8">
	// Sortables
	jQuery(document).ready(function() {
		// Sessiongroups sorteren
        jQuery( "#formfieldtypeid" ).change(function() {
		   var value = $(this).attr('value'); 
           setFieldsByType(value);           
		});
                    
        setFieldsByType(jQuery( "#formfieldtypeid" ).val());        
	});
            
    function setFieldsByType(type)
    {
        // Show option fields for radio, and select
       if(type == 6 || type == 3)
	   {
            jQuery("#option_fields").show();                                
	   }
	   else
	   {
			jQuery("#option_fields").hide();                                
       }
                       
       // Hide default value for datepicker, time, checkbox, label, radio, select, image
       if(type == 3 || type == 4 || type == 6 || type == 9 || type == 10 || type == 12 || type == 15)
       {
           jQuery("#default_values").hide();
       }
       else
       {
           jQuery("#default_values").show();
       }
                       
       // Hide required field for label, image
	   if(type == 10 || type == 15){
           jQuery("#required_fields").hide();                               
       }else {
	    	jQuery("#required_fields").show();
       }
	   
	   // Hide/Show sticky field
	   if(type == 1 || type == 3 || type == 4 || type == 6 || type == 7 || type == 8 || type == 13){
           	jQuery("#sticky_field").show();
       }else {
			jQuery("#sticky_field").hide();
       }
    }
	
	jQuery(document).ready(function() {
		jQuery('#add_option').click(function(){
			var cloned = jQuery('.fieldvalues').last().clone(true);
			cloned.removeAttr('style');
			cloned.find('span').html(parseInt(cloned.find('span').html())+1);
			cloned.find('input:text').val('');
			var optionid = cloned.find('input:text').attr('class')
			cloned.find('input:text').attr('class', 'cls_'+optionid);
			cloned.find('select').attr('class', 'ddcls_'+optionid);
			jQuery('#add_option').parent().before(cloned);
			cloned.find('a').remove(); // Remove delete button
			
			jQuery('.cls_'+optionid).each(function(index, value){
				  var name = jQuery(value).attr('name');
				  var newname = name.replace('['+optionid+']', '');
				  newname = newname.replace('fieldvalue', 'new_field_option');
				  jQuery(value).attr('name', newname);
				  jQuery(value).removeClass('cls_'+optionid);
				  jQuery(value).addClass('0');
			});
			
			jQuery('.ddcls_'+optionid).each(function(index, value){
				  var name = jQuery(value).attr('name');
				  jQuery(value).val('');
				  var newname = name.replace('['+optionid+']', '');
				  newname = newname.replace('flowscreen', 'new_flow_screen');
				  jQuery(value).attr('name', newname);
				  jQuery(value).removeClass('ddcls_'+optionid);
				  jQuery(value).addClass('0');
			});
			
		});
	});
	
</script> 