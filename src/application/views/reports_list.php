<div>
<div >
	<a href="<?php echo $serverpath; ?>report/users" class="btn primary" style="padding-right:10px;">Users Reports</a>
	<a href="<?php echo $serverpath; ?>report/apps" class="btn primary">App Reports</a>
	<a href="<?php echo $serverpath; ?>report/funnel" class="btn primary">Funnel Report</a>
  <a href="<?php echo $serverpath; ?>report/revenue" class="btn primary">Revenue Report</a>        
</div>
<div class="space"></div>
<?php if(isset($users)): ?>
	<table style="border:0px;">
		<tr><td colspan="4"><h2>NEW USERS OVERVIEW REPORT</h2></td></tr>
		<tr><th>Date</th><th>Number of new users</th></tr>
		<?php foreach($totalUsers as $user): ?>	
			<tr><td style="width:200px;"><?php echo date('D d M Y', strtotime($user['crdate'])) ?></td><td><?php echo $user['total'] ?></td></tr>
		<?php endforeach; ?>
	</table>
<div class="space"></div>
	<table style="border:0px;">
		<tr><td colspan="4"><h2>NEW USERS DETAILED REPORT</h2></td></tr>
		<tr><th>Date</th><th>New user name</th><th>Email</th><th>Channel</th></tr>
		<?php foreach($users as $user): ?>	
			<tr><td style="width:200px;"><?php echo date('D d M Y', strtotime($user['cdate'])) ?></td><td><?php echo substr($user['fullname'],0,30); if(strlen($user['fullname']) > 30): echo "....."; endif;  ?></td><td><?php echo substr($user['email'],0,30); if(strlen($user['email']) > 30): echo "....."; endif;  ?></td><td><?php echo $user['channelId'] ?></td></tr>
		<?php endforeach; ?>	
	</table>
<?php endif; ?>

<?php if(isset($newApps)): ?>
	<table style="border:0px;">	
		<tr><td colspan="2"><h2>NEW APPS OVERVIEW REPORT</h2></td></tr>
		<tr><th>Date</th><th>Number of new Apps</th></tr>
		<?php foreach($newApps as $app): ?>	
			<tr><td style="width:200px;"><?php echo date('D d M Y', strtotime($app['creation'])) ?></td><td><?php echo $app['total'] ?></td></tr>
		<?php endforeach; ?>
	</table>
<div class="space"></div>
	<table style="border:0px;">
                <tr><td colspan="5"><h2>NEW APPS DETAILED REPORT</h2></td></tr>
                <tr>
                        <td>Select a Period to view the report: </td>
                        <td colspan="4">
                                <form name="sdate" id="sdate" action="<?php echo $serverpath; ?>report/apps" method="post">
                                        <select name="mymonth">
                                                <option value="0">----Select a Month---</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>												
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>																																																						
                                        <select>
                                        <select name="myyear">
                                                <option value="0">----Select a Year---</option>
                                                <option value="2011">2011</option>
                                                <option value="2012">2012</option>	
                                                <option value="2013">2013</option>
                                                <option value="2014">2014</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option>
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                                <option value="2019">2019</option>
                                                <option value="2020">2020</option>
                                        <select> 
                                        <input type="submit" class="btn primary" value="Show">    
                                </form>
                        </td>
                </tr>	                        

        <?php if(isset($appData[0])): ?>
                        <tr><th>Date</th><th>New app name</th><th>Owner</th><th>Flavor</th><th>Channel</th></tr>
                        <?php foreach($appData as $app): ?>	
                                <tr><td style="width:200px;"><?php echo date('D d M Y', strtotime($app['creation'])) ?></td><td><?php echo $app['appname'] ?></td><td><?php echo $app['owner'] ?></td><td><?php echo $app['flavor'] ?></td><td><?php echo $app['channel'] ?></td></tr>
                        <?php endforeach; ?>	
        <?php endif; ?>
        </table>                                
<?php endif; ?>
<?php if(isset($funnelData)): ?>
	<table style="border:0px;">
		<tr><td colspan="5"><h2>Funnel Report</h2></td></tr>
		<tr><th>Date</th><th>New users</th><th>New apps</th><th>New revenue</th><th>Paid Apps</th></tr>
    <?php $i=0; ?>
		<?php foreach($funnelData as $key => $value): ?>	
			<tr><td style="width:200px;"><?php echo date('D d M Y', strtotime($key)); ?></td><td ><?php echo $value['totalusers']; ?></td><td ><?php echo $value['appcount']; ?></td><td ><?php echo $value['revenue'];?></td><td><?php echo $value['paid'] ?></td></tr>
		<?php $i++;endforeach; ?>	
	</table>
<?php endif; ?>
<?php if(isset($revenuetodate)): ?>
  <table style="border:0px;">
    <tr><td colspan="5"><h2>Revenue Report</h2></td></tr>
    <tr><th>Date</th><th>Revenue</th></tr>
    <?php foreach($revenuetodate as $value): ?>  
      <tr><td style="width:200px;"><?php echo date('D d M Y', strtotime($value[0]->startdate)); ?></td><td ><?php echo $value[0]->totalamount;?></td></tr>
    <?php $i++;endforeach; ?> 
  </table>
<?php endif; ?>
</div>