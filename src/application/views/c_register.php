
<form class="form-horizontal" method="POST" action="/auth/save" accept-charset="utf-8">

	<div class="control-group">
		<label class="control-label" for="inputName"><?=__('Fullname')?></label>
		<div class="controls">
			<input type="text" id="inputName" placeholder="Fullname" name="auth_fn">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputLogin"><?=__('Login')?></label>
		<div class="controls">
			<input type="text" id="inputLogin" placeholder="Login" name="auth_login">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputName"><?=__('Password')?></label>
		<div class="controls">
			<input type="password" name="auth_pass" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputName"><?=__('Confirm Password')?></label>
		<div class="controls">
			<input type="password" name="auth_pass2" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputEmail"><?=__('Email')?></label>
		<div class="controls">
			<input type="text" id="inputEmail" placeholder="Email" name="auth_email" />
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<input type="hidden" name="auth_token" value="<?=$auth_token?>" />
			<button type="submit" class="btn btn-primary"><?=__('Register')?></button><br clear="all" /><br/>
		</div>
		<span style="width:100%;text-align:center;display:block;"><?=__('Already have an account ?')?><a href="<?= site_url('auth/login') ?>"><?= __('Login here') ?></a></span>
	</div>

</form>

