<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />

	<title><?= ( isset($masterPageTitle) && $masterPageTitle ) ? $masterPageTitle : __("{$this->channel->name} Admin")?></title>
	<base href="<?= base_url() ?>" target="" />


	<!-- CSS -->
<!-- 	<link rel="stylesheet" href="css/reset.css" type="text/css" /> -->
	<link rel="stylesheet" href="css/jquery-ui-1.8.5.custom.css" type="text/css">
	<link rel="stylesheet" href="css/jquery.alerts.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/jquery.jqplot.min.css" type="text/css" />
	<link rel="stylesheet" href="css/blank.css" type="text/css" />
	<?php if($this->channel->templatefolder != ''){ ?>
		<link rel="stylesheet" href="templates/<?= $this->channel->templatefolder ?>/css/master.css" type="text/css" />
	<?php } ?>
	<!-- IE STYLESHEET -->
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" type="text/css" /><![endif]-->
	<!-- JAVASCRIPT -->
	<script type="text/javascript" charset="utf-8" src="js/jquery-1.7.2.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery-ui-1.8.5.custom.min.js"></script>
   	<script type="text/javascript" charset="utf-8" src="js/jquery.jqplot.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jqplot.pieRenderer.min.js"></script>
   	

    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
<!--     <script language="javascript" type="text/javascript" src="js/jquery.flot.js"></script> -->
    <!--[if IE 7]><link rel="stylesheet" href="css/ie7.css" type="text/css" /></script><![endif]-->

  <script src="js/jpicker-1.1.6.min.js" type="text/javascript"></script>
</head>
<body>
	<?= $customhtml->header ?>
	<?= $customhtml->body ?>
	<?= isset($content) ? $content : __('"Page not found!"') ?>
	<?= $customhtml->footer ?>
	<?= $customhtml->css ?>

	<script type="text/javascript">
	document.write(unescape("%3Cscript src='" + document.location.protocol +
	  "//munchkin.marketo.net/munchkin.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script>Munchkin.init('771-HZH-025');</script>

	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-10861070-2']);
	  _gaq.push(['_setDomainName', 'tapcrowd.com']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>

	<!-- Google Analytics UA -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-44893075-1', 'tapcrowd.com');
	  ga('send', 'pageview');

	</script>
	<!-- End Google Analytics -->
</body>
</html>
