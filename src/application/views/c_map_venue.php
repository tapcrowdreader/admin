<a href="<?= site_url('module/edit/5/venue/'.$venue->id) ?>" class="edit btn" style="float:right;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a>
<?php

if ($this->uri->segment(4) == 'subfloorplan') {
	$itemtype = 'subfloorplan';
} elseif($this->uri->segment(4) == 'resto') {
	$itemtype = 'resto';
}else
{
	$itemtype = 'subfloorplan';
}

?>
<h1><?= __('Floorplan')?></h1>
<?php if(isset($_GET['error']) && $_GET['error'] == 'noimage') : ?>
<p style="color:#FF0000;"><?= __('Please select an image to upload.')?></p>
<?php endif; ?>
<?php if(isset($_GET['error']) && $_GET['error'] == 'image') : ?>
<p style="color:#FF0000;"><?= __('Failed to upload floorplan. Please select an image smaller than 3000px width and height and filesize 2MB.') ?></p>
<?php endif; ?>
<p><?= __('Image must be in jpg or png format. Max width and height is 3000px. Minimum width is 800px, minimum height is 600px. Max filesize is 2MB.') ?></p>
<div class="note">
    <form action="<?= site_url('map/add/'.$venue->id.'/venue') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
        <fieldset>
		<?php if($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)) : ?>
        <p>
            <label for="userfile"><?= __('Map:') ?></label>
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)){ ?><span class="evtlogo" style="background:transparent url('<?= image_thumb($map->imageurl, 50, 50) ?>') no-repeat top left;"></span><span class="hintAppearance" style="margin-left:60px;"><?= __('If you just replace the floorplan your items will still be mapped on the same locations.')?><br /> <?= __('If you remove the floorplan the mapping will be lost as well.')?></span><?php } ?>
            <input type="file" name="w_map" id="w_map" value="" class="<?= (form_error("w_map") != '') ? 'error' : '' ?> logoupload" /><br clear="all" />
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)){ ?><span><a href="<?= site_url('venue/removemap/'.$map->id.'/'.$venue->id) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
        </p>
        <div class="buttonbar">
            <input type="hidden" name="postback" value="postback" id="postback">
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)) : ?>
            <input type="hidden" name="replaceId" value="<?=$map->id?>" />
            <button type="submit" class="btn primary"><?= __('Replace Floorplan') ?></button>
       	 	<?php else: ?>
       	 	<button type="submit" class="btn primary"><?= __('Upload Floorplan') ?></button>
    		<?php endif; ?>
			<br clear="all" />
        </div>
		<br clear="all" />
        <?php else: ?>
        <p>
            <label style="width: 200px;"><?= __('No map found, add a map') ?></label>
            <input type="file" name="w_map" id="w_map" value="" <?= (form_error("w_map") != '') ? 'class="error"' : '' ?> />
        </p>
        <div class="buttonbar">
            <input type="hidden" name="postback" value="postback" id="postback">
            <button type="submit" class="btn primary"><?= __('Upload Floorplan') ?></button>
			<br clear="all" />
        </div>
		<br clear="all" />
        <?php endif; ?>
		</fieldset>
    </form>
</div>

<?php if ($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)): ?>
<br clear="all"/>
<div class="switch" style="width: auto;">
	<a href="<?= site_url('map/venue/'.$venue->id.'/subfloorplan/'.$map->id) ?>" class="part <?= ($itemtype == 'subfloorplan' ? "active" : "") ?>"><?= __('Add Sub floorplan') ?></a>
	<!-- <a href="<?= site_url('map/venue/'.$venue->id.'/resto/'.$map->id) ?>" class="part <?= ($itemtype == 'resto' ? "active" : "") ?>"><?= __('Add Table to floorplan') ?></a> -->
</div>

	<script src="js/mobilymap.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('.map').mobilymap({
				position: 'top left',
				popupClass: 'bubble',
				markerClass: 'point',
				popup: true,
				cookies: true,
				caption: false,
				setCenter: true,
				outsideButtons: null,
				onMarkerClick: function(){},
				onPopupClose: function(){},
				onMapLoad: function(){
					content.bind({
						dblclick: function(e){
							var position = $(this).position();
							var offset = $(this).offset();
							var x = e.pageX - (offset.left);
							var y = e.pageY - (offset.top);
							if($('#sel_koppel option').size() <= 1){
								$('#saveadd').addClass('disabled');
							}
							$('#posx').val(x); $('#posy').val(y);
							$('#sel_koppel').removeClass('error');
							$('.map .imgContent').append('<div class="point added" id="p-' + x + '-' + y + '"><div class="markerContent"></div></div>');
							$('.mapform_bg').css({'opacity':0, 'display':'block'}).animate({'opacity':0.6});
							$('.mapform').fadeIn();

							reinitmap();
						}
					});
				}
			});

			// Cancel NEW MARKER
			$('#canceladd').click(function(e){
				e.preventDefault();
				$('.point.added').remove();
				$('.mapform_bg').fadeOut();
				$('.mapform').fadeOut();
			});

			// SAVE NEW MARKER
			$('#saveadd').click(function(e){
				e.preventDefault();
				if(!$(this).hasClass('disabled')) {
					if($('#sel_koppel').val() != ''){
						$.ajax({
							url: '<?= ($itemtype == "subfloorplan") ? site_url("map/addSubfloorplan") : ($itemtype == "resto") ? site_url("map/addTable") : site_url("map/addPoi") ?>',
							type: 'POST',
							data: $('#frmMarkerDetails').serialize(),
							complete: function(xhr, textStatus) {
								$('.point.added .markerContent').html($("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").text() + '<br /><a href="<?= site_url("map/removemarkerVenue/".$map->venueid."/".$map->id) ?>/' + $("#sel_koppel").val() + '" class="delete">'+<?=__("DELETE")?>+'</a>');
								$('.point.added').removeClass('added');

								$('.mapform_bg').fadeOut();
								$('.mapform').fadeOut();

								$("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").remove();
								$('#sel_koppel').val('');
							},
							success: function(data, textStatus, xhr) {

							},
							error: function(xhr, textStatus, errorThrown) {

							}
						});
					} else {
						// no exhibitor chosen
						$('#sel_koppel').addClass('error');
					}
				}
			});

			$("#saveaddfloorplan").click(function(event) {
				event.preventDefault();
				if($("#subfloorplan").val() == '') {
					alert("<?= __('Please choose a floorplan') ?>");
				} else {
					$("#frmMarkerDetailsFloorplan").submit();
				}
			});

		});

		function reinitmap(){
			var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
			var point=$this.find(".point");

			point.each(function(){
				var $this=$(this),pos=$this.attr("id").split("-");
				x = pos[1] - $this.width()/2, y = pos[2] - $this.height() + 4;
				$this.css({position:"absolute",zIndex:"2",top:y+"px",left:x+"px"});
				<?php if($itemtype == 'exhibitor' || $itemtype == '') : ?>
				$this.css('background-image', "url('../img/marker-exhibitor.png')");
				<?php elseif($itemtype == 'session') : ?>
				$this.css('background-image', "url('../img/marker-session.png')");
				<?php elseif($itemtype == 'subfloorplan') : ?>
				$this.css('background-image', "url('../img/marker-map.png')");
				<?php endif; ?>
				$this.css('width', '44px');
			});
			//.wrapInner($("<div />").addClass("markerContent").css({display:"none"}));
			point.click(function(){
				var $this=$(this),pointw=$this.width(),pointh=$this.height(),pos=$this.position(),py=pos.top,px=pos.left,wrap=$this.find(".markerContent").html();
				/*if(sets.setCenter){
					var center_y=-py+divh/2-pointh/2,center_x=-px+divw/2-pointw/2,center=map.check(center_x,center_y);
					content.animate({top:center.y+"px",left:center.x+"px"})
				}*/
				if(sets.popup){
					$("."+sets.popupClass).remove();
					$this.after($("<div />").addClass(sets.popupClass).css({position:"absolute",zIndex:"3"}).html(wrap).append($("<a />").addClass("close")));
					var popup=$this.next("."+sets.popupClass),popupw=popup.innerWidth(),popuph=popup.innerHeight(),y=py,x=px;popup.css({top:y+pointh+"px",left:x+"px",marginLeft:-(popupw/2-pointw/2)+"px"})
				}else{
					sets.onMarkerClick.call(this)
				}
				return false;
			});
			$this.find(".close").live("click",function(){
				var $this=$(this);
				$this.parents("."+sets.popupClass).remove();
				setTimeout(function(){
					sets.onPopupClose.call(this)
				},100);
				return false;
			});
			if(sets.outsideButtons!=null){
				$(sets.outsideButtons).click(function(){
					var $this=$(this),rel=$this.attr("rel");
					div=content.find("."+sets.markerClass).filter(function(){
						return $(this).attr("id")==rel
					});
					div.click();
					return false;
				});
			}
		}
	</script>
	<?php list($width, $height, $type, $attr) = getimagesize($this->config->item('imagespath') .$map->imageurl); ?>
	<?php if($map->parentId != 0) : ?>
	<a href="<?=site_url('map/venue/'.$map->venueid.'/subfloorplan/'.$map->parentId);?>" class="btn" style="float:right;margin-top:-3px;margin-bottom:5px;"><?= __('Go to parent floorplan') ?></a>
	<?php endif; ?>
	<div class="note">
	<?php
		if($itemtype == "subfloorplan")
		{
			echo __('Double-click the map on the spot you would like to add a sub floorplan.');
		}
		elseif($itemtype == "resto")
		{
			echo __('Double-click the map on the spot you would like to add a table to floorplan.');
		}

	?>
	</div>

	<div class="map">
		<img src="<?= $this->config->item('publicupload') . $map->imageurl ?>" width="<?= ($width) ?>" height="<?= ($height) ?>" />
		<?php if ($markers): ?>

			<?php if ($itemtype == 'subfloorplan'): ?>
				<?php foreach ($markers as $marker): ?>
					<div class="point" id="p-<?= $marker->x ?>-<?= $marker->y ?>">
						<div class="markerContent">
							<a href="<?=site_url('map/venue/'.$venue->id.'/subfloorplan/'.$marker->id);?>"><?= $marker->title ?></a><br />
							<a href="<?= site_url('map/removefloorplanvenue/'.$map->venueid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE') ?></a>
						</div>
					</div>
				<?php endforeach ?>
			 <?php elseif($itemtype == 'resto'):  ?>
                <?php foreach ($markers as $marker): ?>
                    <div class="point" id="p-<?= $marker->xpos."-".$marker->ypos ?>">
                        <div class="markerContent">
                            <?= ' (' . $marker->tableid . ')' ?><br />
                            <a href="<?= site_url('map/removeTable/'.$map->venueid.'/'.$map->id.'/'.$marker->tableid) ?>" class="delete"><?= __('DELETE') ?></a>
                        </div>
                    </div>
                <?php endforeach ?>
              <?php endif ?>
		<?php endif ?>
	</div>
	<div class="mapform_bg"></div>

	<?php if($itemtype == "subfloorplan") :?>
	<div class="mapform" style="height:190px;">
		<form action="<?= site_url('map/addSubfloorplanVenue/') ?>" method="POST" enctype="multipart/form-data" accept-charset="utf-8" id="frmMarkerDetailsFloorplan">
			<h2><?= __('Add Floorplan') ?></h2>
			<p>
				<label for="floorplanname"><?= __('Name') ?></label>
				<input type="text" name="floorplanname" value="" id="floorplanname">
			</p>
			<p>
				<input type="file" name="subfloorplan" id="subfloorplan" value="" <?= (form_error("subfloorplan") != '') ? 'class="error"' : '' ?> />
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<input type="hidden" name="venueid" value="<?= $venue->id ?>" id="venueid">
				<input type="hidden" name="parentid" value="<?= $this->uri->segment(5) ?>" id="parentid">
				<input type="hidden" name="markerid" value="" id="markerid">
				<input type="hidden" name="posx" value="" id="posx">
				<input type="hidden" name="posy" value="" id="posy">
				<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
				<input type="submit" id="saveaddfloorplan" class="btn primary" value="<?= __('Save') ?>"/>
			</div>
		</form>
	</div>
	<?php elseif($itemtype == "resto") :?>
	<div class="mapform" style="height:190px;">
		<form action="<?= site_url('map/addTable/') ?>" method="POST" enctype="multipart/form-data" accept-charset="utf-8" id="frmMarkerDetails">
			<h2><?= __('Add Table') ?></h2>
			<p>
				<label for="tableid"><?= __('Number') ?></label>
				<input type="text" name="tableid" value="" id="tableid">
			</p>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<input type="hidden" name="venueid" value="<?= $venue->id ?>" id="venueid">
				<input type="hidden" name="mapid" value="<?= $map->id ?>" id="mapid">
				<input type="hidden" name="parentid" value="<?= $this->uri->segment(5) ?>" id="parentid">
				<input type="hidden" name="markerid" value="" id="markerid">
				<input type="hidden" name="posx" value="" id="posx">
				<input type="hidden" name="posy" value="" id="posy">
				<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
				<input type="submit" id="saveaddtable" class="btn primary" value="<?= __('Save') ?>"/>
			</div>
		</form>
	</div>
	<?php endif ?>
<?php endif ?>
<?php if($itemtype == 'exhibitor' || $itemtype == '') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/marker-exhibitor.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php elseif($itemtype == 'session') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/marker-session.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php elseif($itemtype == 'subfloorplan') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/marker-map.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php elseif($itemtype == 'resto') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/table-30.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function() {
	$('.deletemap').click(function(e) {
		e.preventDefault();
		var delurl = $(this).attr('href');
		jConfirm('If you remove the floorplan all items will be removed from the floorplan as well. Are you sure?', 'Remove floorplan', function(r) {
			if(r === true) {
				location.href = delurl;
			} else {
				return false;
			}
		});
	});
});
</script>
