<html>
	<head>
		<meta content='initial-scale=0.5; maximum-scale=1.0; minimum-scale=0.5; user-scalable=0;' name='viewport' /> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		
		<title>Voucher</title>
		<base href="<?= base_url() ?>" target="" />
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/reset.css" type="text/css" />
		<style type="text/css" media="screen">
			
			html {  }
			body { /*background:#F6F6F6;*/ background:#FFF; font-family:"Lucida Grande","Lucida Sans",Arial,Verdana,Sans-Serif; font-size:1.5em; }
			hr { border-bottom:1px dotted #CCC; }
			
			#wrapper { }
			h1 { display:block; width:100%; height:auto; padding:5%; }
			
			.feedback { padding:5% 5% 5% 5%; background:#B4EDAD; color:#336306; font-size:1.5em; }
			.error { padding:5% 5% 5% 5%; background:#F8AEAC; color:#5F0004; font-size:1.5em; }
			
			form { padding:5%; }
			form p { width:inherit; margin:0 0 5% 0; display:block; }
			form p.small { font-size:0.5em; }
			form p label { width:100%; font-size:1.5em; padding:5% 0; }
			form p input { width:100%; border:none; padding:3%; font-size:1.5em; }
			form p input[type="submit"] { background:#DDD; }
			
		</style>
		
	</head>
	<body>
		<div id="wrapper">
			<?php if ($this->session->flashdata('feedback_voting') != ''): ?>
				<div class="feedback"><?= $this->session->flashdata('feedback_voting') ?></div>
			<?php else: ?>
				<?php if ($error != ''): ?>
				<p class="error"><?= $error ?></p>
				<?php endif ?>
			<?php endif ?>
			<?php if (!$granted): ?>
				<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8">
					<?/*
					<p>
						Congratulations,<br />
						You earned a voucher.
					</p>
					*/?>
					<img src="img/palm-iphone.jpg" style="width:100%;" /><br /><br />
					<p>
						Voici votre coupon électronique pour 1 Palm gratuite.
						Validez-le dans le café que vous avez choisi auprès du staff Palm et recevez votre Palm gratuite ! Santé ! Et surtout, profitez bien du Brussels Jazz Marathon 2011 !
					</p><hr />
					<p>
						Hierbij ontvang u uw elektronische voucher voor 1 gratis Palm.
						Laat hem valideren, in het café dat je gekozen hebt, bij het Palm-team et ontvang uw gratis Palm! Santé! En geniet uiteraard met volle teugen van de Brussels Jazz Marathon, editie 2011!
					</p><hr />
					<p>
						Please find your electronic voucher for 1 free Palm.
						The Palm promo team will validate this coupon at the bar you have chosen, after validation you'll get your free Palm! Cheers! And off course, don't forget to enjoy the Brussels Jazz Marathon, edition 2011!
					</p><hr />
					<p class="small">
						Notre savoir-faire se déguste avec sagesse - Ons vakmanschap drink je met verstand - Please drink responisibly
					</p>
					<p>
						<label for="code">Code</label>
						<input type="text" name="code" value="<?= set_value('code') ?>" id="code" autocomplete="off">
					</p>
					<p>
						<input type="hidden" name="postback" value="postback" id="postback">
						<input type="submit" value="Complete!">
					</p>
					<p class="small">
						* Action valable le vendredi 27/05 & le samedi 28/05 de 21:00 à 01h00 dans les cafés participant à l'action (repris préalablement).<br />
						* Actie geldig op vrijdag 27/05 & zaterdag 28/05 tussen 21:00 en 01:00 in eerder vermelde cafés. <br />
						* Valid dates for the action are Friday 27 & Saturday 28 May from 9 PM till 1 AM in the pubs mentioned before.
					</p>
				</form>
			<?php else: ?>
				<form>
					<p>Voucher has been validated!</p>
				</form>
			<?php endif ?>
		</div>
	</body>
</html>
