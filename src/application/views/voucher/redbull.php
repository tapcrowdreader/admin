<html>
	<head>
		<meta content='initial-scale=0.5; maximum-scale=1.0; minimum-scale=0.5; user-scalable=0;' name='viewport' /> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		
		<title>Voucher</title>
		<base href="<?= base_url() ?>" target="" />
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/reset.css" type="text/css" />
		<style type="text/css" media="screen">
			
			html {  }
			body { /*background:#F6F6F6;*/ background:#FFF; font-family:"Lucida Grande","Lucida Sans",Arial,Verdana,Sans-Serif; font-size:1.5em; }
			hr { border-bottom:1px dotted #CCC; }
			
			#wrapper { }
			h1 { display:block; width:100%; height:auto; padding:5%; }
			
			.feedback { padding:5% 5% 5% 5%; background:#B4EDAD; color:#336306; font-size:1.5em; }
			.error { padding:5% 5% 5% 5%; background:#F8AEAC; color:#5F0004; font-size:1.5em; }
			
			form { padding:5%; }
			form p { width:inherit; margin:0 0 5% 0; display:block; }
			form p.small { font-size:0.5em; }
			form p label { width:100%; font-size:1.5em; padding:5% 0; }
			form p input { width:100%; border:none; padding:3%; font-size:1.5em; }
			form p input[type="submit"] { background:#DDD; }
			
		</style>
		
	</head>
	<body>
		<div id="wrapper">
			<?php if ($this->session->flashdata('feedback_voting') != ''): ?>
				<div class="feedback"><?= $this->session->flashdata('feedback_voting') ?></div>
				<a href="<?= site_url($this->uri->uri_string()) ?>" style="display:block; background:#DDD; padding:3%; font-size:1.5em; margin:3%; text-align:center; text-decoration:none; color:#333;">&lsaquo;&lsaquo; Back</a>
			<?php else: ?>
				<?php if ($error != ''): ?>
				<p class="error"><?= $error ?></p>
				<?php endif ?>
			<?php if ($granted): ?>
				<p class="error">Voucher already used!</p>
			<?php endif ?>
			<img src="img/redbull-voucher.png" style="width:100%;" />
			<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8">
				<?/*
				<p>
					Congratulations,<br />
					You earned a voucher.
				</p>
				*/?>
				<p>
					<label for="code">Code</label>
					<input type="text" name="code" value="<?= set_value('code') ?>" id="code" autocomplete="off">
				</p>
				<p>
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="submit" value="Complete!">
				</p>
			</form>
			<?php endif ?>
		</div>
	</body>
</html>
