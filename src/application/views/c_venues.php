<script>
 //  $(document).ready(function() {
	// $( "#sortable" ).sortable({
	//    stop: function(event, ui) {
	// 	   var result = $("#sortable").sortable('serialize');
	// 	   var ids = [];
	// 	   $(".event_wrapper").each(function(index) {
	// 		   ids.push(this.id);
	// 	   });
	// 		$.post("venues/sort", { ids: ids } );
	// 	}
	// });
 //  });

</script>

<h1><?= ($app->familyid == 3) ? "POI's" : "Venues" ?> in "<?= ($tagParam != '') ? $tagParam : $app->name ?>"</h1>
<br>
<div class="events">
	<?php if(($app->familyid != 2) || empty($venues) || _isAdmin()) : ?>
		<?php if(($app->apptypeid == 1 || $app->apptypeid == 8 || $app->apptypeid == 7) && $venues != false && count($venues) > 0) : ?>

		<?php else: ?>
			<a href="<?= site_url('venue/add/'.$launcherid) ?>" class="add btn primary">
				<i class="icon-plus-sign icon-white"></i> <?=__('Add ' . ($app->familyid == 3) ? "POI" : "Venue" )?>
			</a>
			<?php if(($app->apptypeid == 5)) : ?>
                 <a href="<?= site_url('import/venues/'.$launcherid) ?>" class="add btn primary" style="margin-right:5px;">
                    <i class="icon-plus-sign icon-white"></i> <?=__('Import')?>
                </a>
				<?php if(isset($launcherid) && $launcherid != 0) : ?>
					<a href="<?= site_url('venues/removelauncher/'.$launcherid) ?>" class="btn btn-danger" style="float:right;margin-right:5px;">
						<?=__('Remove module')?>
					</a>
					<?php if(_isAdmin()) : ?>
					<a href="<?= site_url('module/fields/'.$launcherid.'/app/'.$app->id) ?>" class="btn" style="float:right;margin-right:5px;">
						<?=__('Custom fields')?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
				<?php if($showButtonMetadata) : ?>
<!-- 					<a href="<?= site_url('metadata/copy/'.$launcherid) ?>" class="btn" style="float:right;margin-right:5px;">
						<?=__('Copy Metadata')?>
					</a> -->
				<?php endif; ?>
            <?php endif; ?>
			<br>
		<?php endif; ?>
	<?php endif; ?>
	<br><br>
		<div id="sortable">
		<?php if ($venues != FALSE): ?>
			<?php foreach ($venues as $venue): ?>
				<div class="event_wrapper" id="<?=$venue->id?>">
					<a href="<?= site_url('venue/view/'.$venue->id.'/'.$launcherid) ?>" class="event">
						<?php if ($venue->image1 != ''): ?>
							<div class="eventlogo" style="background:transparent url('<?= image_thumb($venue->image1, 50, 50) ?>') no-repeat center center">&nbsp;</div>
						<?php else: ?>
							<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
						<?php endif ?>
						<h1><span><?= $venue->name ?></span></h1>
						<?= $venue->address ?>
					</a>
					
					<?php foreach($venue->tags as $tag) : ?>
						<span class="tagInVenueList"><?=$tag ?></span>
					<?php endforeach; ?>
					<br />
					<a href="<?= site_url('venue/edit/'.$venue->id.'/'.$launcherid) ?>" class="edit btn">
						<i class="icon-pencil"></i>  <?=__('Edit')?>
					</a>
					<?php if ($venue->active == 1): ?>
						<a href="<?= site_url('venue/active/' . $venue->id . '/off/'.$launcherid) ?>" class="onoff deactivate"><span><?=__('Deactivate venue')?></span></a>
					<?php else: ?>
						<a href="<?= site_url('venue/active/' . $venue->id . '/on/'.$launcherid) ?>" class="onoff activate"><span><?=__('Activate venue')?></span></a>
					<?php endif ?>
				</div>
			<?php endforeach ?>
		<?php else: ?>
			<p><?=__('No venues found')?></p>
		<?php endif ?>
		</div>
</div>