<h1><?= __('POI\'s') ?></h1>
<?/*
<select name="sel_maps" id="sel_maps">
	<option value="">Select map ...</option>
	<?php foreach ($maps as $sel_map): ?>
		<option value="<?= $sel_map->id ?>"><?= $sel_map->id ?></option>
	<?php endforeach ?>
</select>
*/?>
<?php if ($map != FALSE): ?>
	<script src="js/mobilymap.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			
			$('.map').mobilymap({
				position: 'top left', 
				popupClass: 'bubble',
				markerClass: 'point',
				popup: true, 
				cookies: true, 
				caption: false, 
				setCenter: true, 
				outsideButtons: null, 
				onMarkerClick: function(){}, 
				onPopupClose: function(){}, 
				onMapLoad: function(){
					content.bind({
						dblclick: function(e){
							var position = $(this).position();
							var offset = $(this).offset();
							var x = e.pageX - (offset.left);
							var y = e.pageY - (offset.top);
							if($('#sel_koppel option').size() <= 1){
								$('#saveadd').addClass('disabled');
							}
							$('#posx').val(x); $('#posy').val(y);
							$('#sel_koppel').removeClass('error');
							$('.map .imgContent').append('<div class="point added" id="p-' + x + '-' + y + '"><div class="markerContent"></div></div>');
							$('.mapform_bg').css({'opacity':0, 'display':'block'}).animate({'opacity':0.6});
							$('.poiform').fadeIn();

							reinitmap();
						}
					});
				} 
			});
			
			// Cancel NEW MARKER
			$('#canceladd').click(function(e){
				e.preventDefault();
				$('.point.added').remove();
				$('.mapform_bg').fadeOut();
				$('.poiform').fadeOut();
			});
			
			// SAVE NEW MARKER
			$('#saveadd').click(function(e){
				e.preventDefault();
				if(!$(this).hasClass('disabled')) {
					if($('#sel_koppel').val() != ''){
						$.ajax({
							url: '<?= site_url("poi/addPoi") ?>',
							type: 'POST',
							data: $('#frmMarkerDetails').serialize(),
							complete: function(xhr, textStatus) {
								$('.point.added .markerContent').html($('#name').val() + "(" + $("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").text() + ')<br /><a href="<?= site_url("poi/removepoiVenue/".$map->venueid."/".$map->id) ?>/' + $("#sel_koppel").val() + '" class="delete">'.<?= __('DELETE')?></a>');
								$('.point.added').removeClass('added');
								
								$('.mapform_bg').fadeOut();
								$('.poiform').fadeOut();
								
								//$("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").remove();
								$('#sel_koppel').val('');
								
								$('#name').val('');
							},
							success: function(data, textStatus, xhr) {
								
							},
							error: function(xhr, textStatus, errorThrown) {
								
							}
						});
					} else {
						// no poitype chosen
						$('#sel_koppel').addClass('error');
					}
				}
			});
			
		});
		
		function reinitmap(){
			var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
			var point=$this.find(".point");
			
			point.each(function(){
				var $this=$(this),pos=$this.attr("id").split("-");
				x = pos[1] - $this.width()/2, y = pos[2] - $this.height() + 4;
				$this.css({position:"absolute",zIndex:"2",top:y+"px",left:x+"px"});
			}); 
			//.wrapInner($("<div />").addClass("markerContent").css({display:"none"}));
			point.click(function(){
				var $this=$(this),pointw=$this.width(),pointh=$this.height(),pos=$this.position(),py=pos.top,px=pos.left,wrap=$this.find(".markerContent").html();
				/*if(sets.setCenter){
					var center_y=-py+divh/2-pointh/2,center_x=-px+divw/2-pointw/2,center=map.check(center_x,center_y);
					content.animate({top:center.y+"px",left:center.x+"px"})
				}*/
				if(sets.popup){
					$("."+sets.popupClass).remove();
					$this.after($("<div />").addClass(sets.popupClass).css({position:"absolute",zIndex:"3"}).html(wrap).append($("<a />").addClass("close")));
					var popup=$this.next("."+sets.popupClass),popupw=popup.innerWidth(),popuph=popup.innerHeight(),y=py,x=px;popup.css({top:y+pointh+"px",left:x+"px",marginLeft:-(popupw/2-pointw/2)+"px"})
				}else{
					sets.onMarkerClick.call(this)
				}
				return false;
			});
			$this.find(".close").live("click",function(){
				var $this=$(this);
				$this.parents("."+sets.popupClass).remove();
				setTimeout(function(){
					sets.onPopupClose.call(this)
				},100);
				return false;
			});
			if(sets.outsideButtons!=null){
				$(sets.outsideButtons).click(function(){
					var $this=$(this),rel=$this.attr("rel");
					div=content.find("."+sets.markerClass).filter(function(){
						return $(this).attr("id")==rel
					});
					div.click();
					return false;
				});
			}
		}
		
	</script>
	<?php list($width, $height, $type, $attr) = getimagesize($map->imageurl); ?>
	<div class="note">
		<?= __('Double-click the map on the spot you would like to add a POI.') ?>
	</div>
	<div class="map">
		<img src="<?= $map->imageurl ?>" width="<?= ($width) ?>" height="<?= ($height) ?>" />
		<?php if ($markers): ?>
			<?php foreach ($markers as $marker): ?>
				<div class="point" id="p-<?= ($marker->x2 != 0) ? $marker->x1+($marker->x2/2) : $marker->x1 ?>-<?= ($marker->y2 != 0) ? $marker->y1+($marker->y2/2) : $marker->y1 ?>">
					<div class="markerContent">
						<?= $marker->name . ' (' . $marker->poitypename . ')' ?><br />
						<a href="<?= site_url('poi/removepoiVenue/'.$map->venueid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE') ?></a>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>
	</div>
	<div class="mapform_bg"></div>
	<div class="poiform">
		<form action="<?= site_url('poi/poi/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
			<h2><?= __('Choose Poi') ?></h2>
			<span<?= __('>Lorem ipsum dolor sit amet, consectetur adipisicing elit.') ?></span>
			<p>
				<label for="name"><?= __('Name') ?></label>
				<input type="text" name="name" value="" id="name">
			</p>
			<p>
				<label for="sel_koppel"><?= __('Poitype') ?></label>
				<select name="sel_koppel" id="sel_koppel">
					<option value=""><?= __('Select ...') ?></option>
					<?php if ($poitypes != FALSE): ?>
					<?php foreach ($poitypes as $poitype): ?>
						<option value="<?= $poitype->id ?>"><?= $poitype->name ?></option>
					<?php endforeach ?>
					<?php endif ?>
				</select>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<input type="hidden" name="eventid" value="" id="eventid">
				<input type="hidden" name="venueid" value="<?= $venue->id ?>" id="venueid">
				<input type="hidden" name="markerid" value="" id="markerid">
				<input type="hidden" name="posx" value="" id="posx">
				<input type="hidden" name="posy" value="" id="posy">
				<a href="#" id="canceladd" class="delete"><?= __('Cancel') ?></a>
				<a href="#" id="saveadd" class="add <?= ($poitype != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
			</div>
		</form>
	</div>
<?php else: ?>
	<div class="note"><?= __('No map found, you can add a map <a href="%s">here.</a>',site_url('venue/edit/'.$venue->id)) ?></div>
<?php endif ?>

