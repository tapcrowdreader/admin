<div>
	<h1>Add Field</h1>
	<div class="frmformbuilder">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>

			<?php if(isset($languages) && !empty($languages)) : ?>
				<?php foreach($languages as $language) : ?>
				<p <?= (form_error("label_".$language->key) != '') ? 'class="error"' : '' ?>>
					<label for="name">Label <?= '(' . $language->name . ')'; ?>:</label>
					<input type="text" name="label_<?php echo $language->key; ?>" id="label" value="<?=set_value('label_'.$language->key)?>" />
				</p>
				<?php endforeach; ?>
			<?php else : ?>    
				<p <?= (form_error("label_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
					<label for="label">Label:</label>
					<input type="text" name="label_<?php echo $app->defaultlanguage; ?>" id="label" value="<?=set_value('label_'.$app->defaultlanguage)?>" />
				</p>
			<?php endif; ?>

			<p <?= (form_error("formfieldtypeid") != '') ? 'class="error"' : '' ?>>
				<label for="formfieldtypeid">Type:</label>
				<select id="formfieldtypeid" name="formfieldtypeid" >
					<option value="select">Select...</option>
					<?php if(isset($fieldtypes)) { ?>
					<?php foreach($fieldtypes as $e) : ?>
					<option value="<?= $e->id;?>" <?= set_select('formfieldtypeid', $e->id) ?>><?= $e->name; ?></option>
				<?php endforeach; ?>
				<?php } ?>
			</select>
			</p>

		<?php if(isset($languages) && !empty($languages)) : ?>
			<div id="option_fields" style="display:none;">
          		<p class="fieldvalues">
	              	<?php $index = 1; foreach($languages as $language) : ?>                
		              	<label for="fieldvalue" id="lbl<?= $language->key; ?>" style="float:left;width:200px;"><?=__('Option')?> <span style="float:none; font-style:inherit; text-align:inherit; display:inline; margin-right:0px; color:inherit;"><?=$index?></span> <?= '(' . $language->name . ')'; ?>:</label>
		              	<?php if($form->useflows):?>
		              	<label style="display:inline; float:right;width:200px;text-align: right;"><?= __('Screen to load on select') ?></label>
		              	<?php endif; ?>
		              	<br clear="all" />
		              	<input type="text" name="fieldvalue[<?= $language->key; ?>][]" style="width:45%;" />
		            <?php endforeach; ?>
		            <?php if($formscreens_flows):?>
		              <select name="flowscreen[<?=$app->defaultlanguage?>][]" style="display:inline; float:right;">
		              	<option value="">Select...</option>
		              	<?php
		              	foreach($formscreens_flows as $flowscreen):
		              		if($flowscreen->id == $formscreen->id): continue; endif;
		              	?>
		              	<option value="<?=$flowscreen->id?>"><?=$flowscreen->title?></option>
		              <?php endforeach;?>
		              </select>
		          <?php endif;?>
		        </p>
		        <p style="padding-top:0px;"><button type="button" id="add_option" class="add btn primary"><?=__('Add Option')?></button></p>
		      </div>
				<div id="option_fields2" style="display:none;">
          		<p class="fieldvalues2">
		            <?php if($form->useflows):?>
		            	<label><?= __('Screen to load on select') ?></label>
		              <select name="flowscreen2[<?=$app->defaultlanguage?>]" style="display:inline; float:left;">
		              	<option value="">Select...</option>
		              	<?php
		              	foreach($formscreens_flows as $flowscreen):
		              		if($flowscreen->id == $formscreen->id): continue; endif;
		              	?>
		              	<option value="<?=$flowscreen->id?>"><?=$flowscreen->title?></option>
		              <?php endforeach;?>
		              </select>
		              <br clear="all" />
		          <?php endif;?>
		        </p>
		      </div>
		      <div id="date_time_option" style="display:none;">
		      	<p>
		      		<label for="date/time">Time Format:</label>
		      		<select name="date-time" id="date-time" onChange="changeTimeFormt()">
		      			<option value="am-pm">AM-PM</option>
		      			<option value="24hr">24 Hours</option>
		      		</select>
		      	</p>
		      </div>
		      <div id="default_values">
		      	<?php foreach($languages as $language) : ?>
		      	<p <?= (form_error("defaultvalue_".$language->key) != '') ? 'class="error"' : '' ?>>
		      		<label for="defaultvalue">Default Value <?= '(' . $language->name . ')'; ?>:</label>
		      		<input type="text" name="defaultvalue_<?php echo $language->key; ?>" id="defaultvalue" value="<?= set_value('defaultvalue_'.$language->key) ?>" />
		      	</p>
		      <?php endforeach; ?>
		    </div>
		<?php else : ?>
			  <div id="default_values">
			  	<p <?= (form_error("defaultvalue_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
			  		<label for="defaultvalue">Default Value:</label>
			  		<input type="text" name="defaultvalue_<?php echo $app->defaultlanguage; ?>" id="defaultvalue" value="<?= set_value('defaultvalue_'.$app->defaultlanguage) ?>" />
			  	</p> 
			  </div>

			  <div id="option_fields" style="display:none;">	
			  	<p <?= (form_error("possiblevalues_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
			  		<label for="possiblevalues">Possible Values, comma separated:</label>
			  		<input type="text" name="possiblevalues_<?php echo $app->defaultlanguage; ?>" id="possiblevalues" value="<?=set_value('possiblevalues_'.$app->defaultlanguage)?>" />
			  	</p>
			  </div>
		<?php endif; ?>

			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label>Order:</label>
				<input type="text" name="order" value="<?= set_value('order',$formscreen->max_order) ?>" id="order">
			</p>
			<div id="required_fields" style="display:none;">
				<p  class="checklist <?= (form_error("required") != '') ? 'error' : '' ?>">
					<label>Required Field:</label>
					<input type="checkbox" name="required" value="yes" id="required">
				</p>
			</div>
			<?php if($launcher->moduletypeid != 60 && $launcher->moduletypeid != 61 && $launcher->moduletypeid != 62) : ?>
			<div id="sticky_field" style="display:none;">
				<p  class="checklist">
					<label>Sticky:</label>
					<input type="checkbox" name="sticky" value="1" id="sticky">
				</p>
			</div>
			<?php endif; ?>
			<p <?= (form_error("customproperty") != '') ? 'class="error"' : '' ?>>
				<label>Custom property:</label>
				<input type="text" name="customproperty" value="<?= set_value('customproperty') ?>" id="customproperty">
				<span class="hintAppearance">e.g. style="background-color=#FF0000" onchange="if (this.value=='') alert('Cannot be empty');0" </span>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary">Add Field</button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
var value = jQuery( "#formfieldtypeid" ).attr('value');
if(value == 6 || value == 3){
	jQuery("#option_fields").show();                                
}

	// Sortables
	jQuery(document).ready(function() {
		// Sessiongroups sorteren
		jQuery( "#formfieldtypeid" ).change(function() {
			var value = $(this).attr('value');          
           // Show option fields for radio, and select
           if(value == 6 || value == 3)
           {
           	jQuery("#option_fields").show();                                
           }
           else
           {
           	jQuery("#option_fields").hide();                                
           }

           if(value == 19)
           {
           	jQuery("#option_fields2").show();                                
           }
           else
           {
           	jQuery("#option_fields2").hide();                                
           }


           // Hide default value for datepicker, time, checkbox, label, radio, select, image
           if(value == 3 || value == 4 || value == 6 || value == 9 || value == 10 || value == 12 || value == 15)
           {
           	jQuery("#default_values").hide();
           }
           else
           {
           	jQuery("#default_values").show();
           }

           // Hide required field for label, image
           if(value == 10 || value == 15){
           	jQuery("#required_fields").hide();
           }else {
           	jQuery("#required_fields").show();
           }
		   // Hide/Show sticky field
		   if(value == 1 || value == 3 || value == 4 || value == 6 || value == 7 || value == 8 || value == 13){
		   	jQuery("#sticky_field").show();
		   }else {
		   	jQuery("#sticky_field").hide();
		   }
		 });

jQuery('#add_option').click(function(){
	var cloned = jQuery('.fieldvalues').last().clone(true);
	cloned.find('span').html(parseInt(cloned.find('span').html())+1);
	cloned.find('input').val('');

	jQuery('#add_option').parent().before(cloned);

});

jQuery('#add_option2').click(function(){
	var cloned = jQuery('.fieldvalues2').last().clone(true);
	cloned.find('span').html(parseInt(cloned.find('span').html())+1);
	cloned.find('input').val('');

	jQuery('#add_option2').parent().before(cloned);

});

});                
</script>