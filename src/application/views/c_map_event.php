
<?php
if ($this->uri->segment(4) == 'session') {
	$itemtype = 'session';
} elseif($this->uri->segment(4) == 'poi') {
	$itemtype = 'poi';
} else {
	$itemtype = 'exhibitor';
}
?>

<h1><?= __('Floorplan')?></h1>
<?php if(isset($_GET['error']) && $_GET['error'] == 'noimage') : ?>
<p style="color:#FF0000;"><?= __('Please select an image to upload.') ?></p>
<?php endif; ?>
<?php if(isset($_GET['error']) && $_GET['error'] == 'image') : ?>
<p style="color:#FF0000;"><?= __('Failed to upload floorplan. Please select an image smaller than 3000px width and height and filesize 2MB.') ?></p>
<?php endif; ?>
<p><?= __('Image must be in jpg or png format. Max width and height is 3000px. Minimum width is 800px, minimum height is 600px. Max filesize is 2MB.') ?></p>
<div class="note">
    <form action="<?= site_url('map/add/'.$event->id.'/event') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
        <fieldset>
		<?php if($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)) : ?>
        <p>
            <label for="userfile">Map:</label>
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)){ ?><span class="evtlogo" style="background:transparent url(<?= image_thumb($map->imageurl, 50, 50) ?>) no-repeat top left;"></span><?php } ?>
            <input type="file" name="w_map" id="w_map" value="" class="<?= (form_error("w_map") != '') ? 'error' : '' ?> logoupload" /><br clear="all" />
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)){ ?><span><a href="<?= site_url('event/removemap/'.$map->id.'/'.$event->id) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
        </p>
        <div class="buttonbar">
            <input type="hidden" name="postback" value="postback" id="postback">
            <button type="submit" class="btn primary"><?= __('Upload Floorplan') ?></button>
			<br clear="all" />
        </div>
		<br clear="all" />
        <?php else: ?>
        <p>
            <label style="width: 130px;"><?= __('No map found, add a map') ?></label>
            <input type="file" name="w_map" id="w_map" value="" <?= (form_error("w_map") != '') ? 'class="error"' : '' ?> />
        </p>
        <div class="buttonbar">
            <input type="hidden" name="postback" value="postback" id="postback">
            <button type="submit" class="btn primary"><?= __('Upload Floorplan') ?></button>
			<br clear="all" />
        </div>
		<br clear="all" />
        <?php endif; ?>
		</fieldset>
    </form>
</div>
<?php if ($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)): ?>
<br clear="all"/>
<div class="switch" style="width: auto;">
	<a href="<?= site_url('map/event/'.$event->id.'/exhibitor') ?>" class="part <?= ($itemtype == 'exhibitor' ? "active" : "") ?>"><?= __('Add Exhibitor locations') ?></a>
	<a href="<?= site_url('map/event/'.$event->id.'/session') ?>" class="part <?= ($itemtype == 'session' ? "active" : "") ?>"><?= __('Add Session locations') ?></a>
</div>
	<script src="js/mobilymap.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			
			$('.map').mobilymap({
				position: 'top left', 
				popupClass: 'bubble',
				markerClass: 'point',
				popup: true, 
				cookies: true, 
				caption: false, 
				setCenter: true, 
				outsideButtons: null, 
				onMarkerClick: function(){}, 
				onPopupClose: function(){}, 
				onMapLoad: function(){
					content.bind({
						dblclick: function(e){
							var position = $(this).position();
							var offset = $(this).offset();
							var x = e.pageX - (offset.left);
							var y = e.pageY - (offset.top);
							if($('#sel_koppel option').size() <= 1){
								$('#saveadd').addClass('disabled');
							}
							$('#posx').val(x); $('#posy').val(y);
							$('#sel_koppel').removeClass('error');
							$('.map .imgContent').append('<div class="point added" id="p-' + x + '-' + y + '"><div class="markerContent"></div></div>');
							$('.mapform_bg').css({'opacity':0, 'display':'block'}).animate({'opacity':0.6});
							$('.mapform').fadeIn();

							reinitmap();
						}
					});
				} 
			});
			
			// Cancel NEW MARKER
			$('#canceladd').click(function(e){
				e.preventDefault();
				$('.point.added').remove();
				$('.mapform_bg').fadeOut();
				$('.mapform').fadeOut();
			});
			
			// SAVE NEW MARKER
			$('#saveadd').click(function(e){
				e.preventDefault();
				if(!$(this).hasClass('disabled')) {
					if($('#sel_koppel').val() != ''){
                        var urlValue
                        <?php if(($itemtype == "session")) : ?>
                            urlValue = <?= site_url("map/addSessionMarker"); ?>;
                         <?php elseif($itemtype == "poi") : ?>
                            urlValue = <?= site_url("map/addPoi"); ?>;
                         <?php else : ?>
                            urlValue = <?= site_url("map/addMarker"); ?>;
                        <?php endif; ?>
						$.ajax({

							url: urlValue,
							type: 'POST',
							data: $('#frmMarkerDetails').serialize(),
							complete: function(xhr, textStatus) {
								$('.point.added .markerContent').html($("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").text() + '<br /><a href="<?= site_url("map/removemarker/".$map->eventid."/".$map->id) ?>/' + $("#sel_koppel").val() + '" class="delete">'.__('DELETE').'</a>');
								$('.point.added').removeClass('added');
								
								$('.mapform_bg').fadeOut();
								$('.mapform').fadeOut();
								
								$("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").remove();
								$('#sel_koppel').val('');
							},
							success: function(data, textStatus, xhr) {
								
							},
							error: function(xhr, textStatus, errorThrown) {
								
							}
						});
					} else {
						// no exhibitor chosen
						$('#sel_koppel').addClass('error');
					}
				}
			});
			
			$("#saveaddfloorplan").click(function(event) {
				event.preventDefault();
				if($("#subfloorplan").val() == '') {
					alert("<?= __('Please choose a floorplan') ?>");
				} else {
					$("#frmMarkerDetailsFloorplan").submit();
				}
			});
		});
		
		function reinitmap(){
			var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
			var point=$this.find(".point");
			
			point.each(function(){
				var $this=$(this),pos=$this.attr("id").split("-");
				x = pos[1] - $this.width()/2, y = pos[2] - $this.height() + 4;
				$this.css({position:"absolute",zIndex:"2",top:y+"px",left:x+"px"});
			}); 
			//.wrapInner($("<div />").addClass("markerContent").css({display:"none"}));
			point.click(function(){
				var $this=$(this),pointw=$this.width(),pointh=$this.height(),pos=$this.position(),py=pos.top,px=pos.left,wrap=$this.find(".markerContent").html();
				/*if(sets.setCenter){
					var center_y=-py+divh/2-pointh/2,center_x=-px+divw/2-pointw/2,center=map.check(center_x,center_y);
					content.animate({top:center.y+"px",left:center.x+"px"})
				}*/
				if(sets.popup){
					$("."+sets.popupClass).remove();
					$this.after($("<div />").addClass(sets.popupClass).css({position:"absolute",zIndex:"3"}).html(wrap).append($("<a />").addClass("close")));
					var popup=$this.next("."+sets.popupClass),popupw=popup.innerWidth(),popuph=popup.innerHeight(),y=py,x=px;popup.css({top:y+pointh+"px",left:x+"px",marginLeft:-(popupw/2-pointw/2)+"px"})
				}else{
					sets.onMarkerClick.call(this)
				}
				return false;
			});
			$this.find(".close").live("click",function(){
				var $this=$(this);
				$this.parents("."+sets.popupClass).remove();
				setTimeout(function(){
					sets.onPopupClose.call(this)
				},100);
				return false;
			});
			if(sets.outsideButtons!=null){
				$(sets.outsideButtons).click(function(){
					var $this=$(this),rel=$this.attr("rel");
					div=content.find("."+sets.markerClass).filter(function(){
						return $(this).attr("id")==rel
					});
					div.click();
					return false;
				});
			}
			
			$('.imgContent').css('top', '0px');
			$('.imgContent').css('left', '0px');
		}
		
	</script>
	<?php list($width, $height, $type, $attr) = getimagesize($this->config->item('imagespath') . $map->imageurl); ?>
	<div class="note">
		<?= __('Double-click the map on the spot you would like to add a marker.')?>
	</div>
	<div class="map" style="position: relative; overflow: hidden; cursor: move;">
        <div class="imgContent" style="z-index: 1; position: absolute; top: -248px; left: 0px;">
		<img src="<?= $this->config->item('publicupload') . $map->imageurl ?>" width="<?= ($width) ?>" height="<?= ($height) ?>" />
		<?php if ($markers): ?>
			<?php if ($itemtype == 'session'): ?>
				<?php foreach ($markers as $marker): ?>
					<div class="point" id="p-<?= $marker->xpos ?>-<?= $marker->ypos ?>">
						<div class="markerContent">
							<?= $marker->name ?><br />
							<a href="<?= site_url('map/removeSessionmarker/'.$map->eventid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE')?></a>
						</div>
					</div>
				<?php endforeach ?>
            <?php elseif($itemtype == 'poi'):  ?>
                <?php foreach ($markers as $marker): ?>
                    <div class="point" id="p-<?= ($marker->x2 != 0) ? $marker->x1+($marker->x2/2) : $marker->x1 ?>-<?= ($marker->y2 != 0) ? $marker->y1+($marker->y2/2) : $marker->y1 ?>">
                        <div class="markerContent">
                            <?= $marker->name . ' (' . $marker->poitypename . ')' ?><br />
                            <a href="<?= site_url('map/removepoi/'.$map->eventid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE')?></a>
                        </div>
                    </div>
                <?php endforeach ?>
			<?php else: ?>
				<?php foreach ($markers as $marker): ?>
					<div class="point" id="p-<?= ($marker->x2 != 0) ? $marker->x1+($marker->x2/2) : $marker->x1 ?>-<?= ($marker->y2 != 0) ? $marker->y1+($marker->y2/2) : $marker->y1 ?>">
						<div class="markerContent">
							<?= $marker->name ?><br />
							<a href="<?= site_url('map/removemarker/'.$map->eventid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE')?></a>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>
		<?php endif ?>
        </div>
	</div>
	<div class="mapform_bg"></div>
	<?php if ($itemtype == 'session'): ?>
		<div class="mapform">
			<form action="<?= site_url('map/marker/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
				<h2><?= __('Choose Session')?></h2>
				<p>
					<label for="sel_koppel"><?= __('Session') ?></label>
					<select name="sel_koppel" id="sel_koppel">
						<option value=""><?= __('Select ...') ?></option>
						<?php if ($sessions != FALSE): ?>
						<?php foreach ($sessions as $session): ?>
							<option value="<?= $session->id ?>"><?= $session->name ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="hidden" name="markerid" value="" id="markerid">
					<input type="hidden" name="posx" value="" id="posx">
					<input type="hidden" name="posy" value="" id="posy">
					<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
					<a href="#" id="saveadd" class="btn primary <?= ($session != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
				</div>
			</form>
		</div>
    <?php elseif($itemtype == 'poi') : ?>
	<div class="poiform">
		<form action="<?= site_url('map/marker/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
			<h2><?= __('Choose Poi')?></h2>
			<p>
				<label for="name"><?= __('Name') ?></label>
				<input type="text" name="name" value="" id="name">
			</p>
			<p>
				<label for="sel_koppel"><?= __('Poitype') ?></label>
				<select name="sel_koppel" id="sel_koppel">
					<option value=""><?= __('Select ...') ?></option>
					<?php if ($poitypes != FALSE): ?>
					<?php foreach ($poitypes as $poitype): ?>
						<option value="<?= $poitype->id ?>"><?= $poitype->name ?></option>
					<?php endforeach ?>
					<?php endif ?>
				</select>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<input type="hidden" name="markerid" value="" id="markerid">
				<input type="hidden" name="posx" value="" id="posx">
				<input type="hidden" name="posy" value="" id="posy">
				<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
				<a href="#" id="saveadd" class="btn primary <?= ($poitype != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
			</div>
		</form>
	</div>
	<?php else: ?>
		<div class="mapform">
			<form action="<?= site_url('map/marker/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
				<h2><?= __('Choose Exhibitor')?></h2>
				<p>
					<label for="sel_koppel"><?= __('Exhibitor') ?></label>
					<select name="sel_koppel" id="sel_koppel">
						<option value=""><?= __('Select ...') ?></option>
						<?php if ($exhibitors != FALSE): ?>
						<?php foreach ($exhibitors as $exhibitor): ?>
							<option value="<?= $exhibitor->id ?>"><?= $exhibitor->booth . ' - ' . $exhibitor->name ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
                    <input type="hidden" name="eventid" value="<?= $event->id ?>" id="eventid">
                    <input type="hidden" name="venueid" value="" id="venueid">
					<input type="hidden" name="markerid" value="" id="markerid">
					<input type="hidden" name="posx" value="" id="posx">
					<input type="hidden" name="posy" value="" id="posy">
					<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
					<a href="#" id="saveadd" class="btn primary <?= ($exhibitors != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
				</div>
			</form>
		</div>
	<?php endif ?>
<?php endif ?>

