<div>
	<h1><?= __('Edit %s', $newsitem->title)?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
            <div class="error"><p><?= __('Something went wrong with the image upload. <br/> Maybe the image size exceeds the maximum width or height') ?></p></div>
            <?php endif ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
            <?php $trans = _getTranslation('newsitem', $newsitem->id, 'title', $language->key); ?>
                <label for="title">Title <?= '(' . $language->name . ')'; ?>: <span class="hintAppearance"><?= __('max 50 characters')?></span></label>
                <input maxlength="50" type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$language->key, ($trans != null) ? $trans : $newsitem->title), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
            <?php $trans = _getTranslation('newsitem', $newsitem->id, 'txt', $language->key);?>
                <label for="description"><?= __('Description '. '(' . $language->name . ')'); ?>:</label>
                <textarea name="text_<?php echo $language->key; ?>" id="text"><?= htmlspecialchars_decode(set_value('text_'.$language->key, ($trans != null) ? $trans : $newsitem->txt), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endforeach; ?>
            <?php else : ?>
            <p <?= (form_error("title_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Title:') ?></label>
                <input type="text" name="title_<?php echo $app->defaultlanguage; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$app->defaultlanguage, _getTranslation('newsitem', $newsitem->id, 'title', $app->defaultlanguage)), ENT_NOQUOTES) ?>" />
            </p>
            <p <?= (form_error("description_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="description"><?= __('Description:') ?></label>
                <textarea name="text_<?php echo $app->defaultlanguage; ?>" id="text"><?= htmlspecialchars_decode(set_value('text_'. $app->defaultlanguage, _getTranslation('newsitem', $newsitem->id, 'txt',  $app->defaultlanguage)), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endif; ?>
 			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:') ?></label>
                <span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
				<span class="evtlogo" <?php if($newsitem->image != ''){ ?>style="background:transparent url('<?= image_thumb($newsitem->image, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="image" id="image" value="" class="logoupload" />
                <?php $type = (isset($venue)) ? 'venue' : (isset($event)) ? 'event' : 'app';?>
<!--                 <?php if($newsitem->image != null && $newsitem->image != '') : ?>
                <?php if($newsitem->image != '' && file_exists($this->config->item('imagespath') . $newsitem->image)){ ?><a href="<?= site_url('news/crop/'.$newsitem->id . '/' .$type) ?>"><?= __('Edit picture')?></a><?php } ?>
                <?php endif; ?> -->
				<br clear="all"/>
				<?php if($newsitem->image != '' && file_exists($this->config->item('imagespath') . $newsitem->image)){ ?><span><a href="<?= site_url('news/removeimage/'.$newsitem->id.'/'.$type) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
			</p><br /> 
            <div class="row">
                <label for="tags">Tags:</label>
                <ul id="mytags" name="mytagsul"></ul>
            </div>
            <?php foreach($metadata as $m) : ?>
                <?php foreach($languages as $lang) : ?>
                    <?php if(_checkMultilang($m, $lang->key, $app)): ?>
                        <p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
                            <?= _getInputField($m, $app, $lang, $languages, 'newsitem', $newsitem->id); ?>
                        </p>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="javascript:history.go(-1);" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#mytags").tagit({
        initialTags: [<?php if(isset($postedtags) && $postedtags != false && !empty($postedtags)) { 
            foreach($postedtags as $val){ 
                echo '"'.$val.'", ';
            } 
        } elseif (isset($tags) && $tags != false) {
            foreach($tags as $val){ 
                echo "\"".$val->tag."\"," ;
            }
        } ?>]
    });

    var tags = new Array();
    var i = 0;
    <?php foreach($apptags as $tag) : ?>
    tags[i] = "<?=$tag->tag?>";
    i++;
    <?php endforeach; ?>
    $("#mytags input.tagit-input").autocomplete({
        source: tags
    });
});
</script>