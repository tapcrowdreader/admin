<?php if(isset($app)) : ?>
	<div>
		<h1><?=__('App Settings')?></h1>
		<div class="tabs-below">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="apps/blank/<?=$app->id?>">Basic Settings</a>
				</li>
				<li>
					<a href="apps/appstoresettings/<?=$app->id?>">Appstore Settings</a>
				</li>
			</ul>
		</div>
		<div class="frmsessions">
			<form action="<?= site_url('apps/blank/'.$app->id) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
				<?php if($error != ''): ?>
				<div class="error"><?= $error ?></div>
				<?php endif ?>
				<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
					<label for="name"><?= __('Name:') ?></label>
					<input type="text" name="name" id="name" value="<?= htmlspecialchars_decode(set_value('name', $app->name), ENT_NOQUOTES) ?>" />
				</p>
<!-- 				<p <?= (form_error("info")) ? 'class="error"' : '' ?>>
					<label for="info"><?= __('Info:') ?></label>
					<textarea name="info" id="info" ><?= htmlspecialchars_decode(set_value('info', $app->info)) ?></textarea>
				</p> -->
				<p <?= (form_error("blankurl") != '') ? 'class="error"' : '' ?>>
					<label for="url" style="margin-bottom:5px;"><?= __('URL of your HTML5 app:') ?></label>
					<input type="text" name="blankurl" id="blankurl" value="<?= set_value('blankurl', $app->blankurl) ?>">
				</p>

				<?php if(_isAdmin()) : ?>
				<p>
					<label for="certificate"><?= __('Certificate:') ?></label>
					<input type="file" name="certificate" id="certificate" value="" class="logoupload" /><br clear="all"/>
				</p>
				<p>
					<label for="environment"><?= __('Environment:') ?></label>
					<select name="environment">
						<option value="live">Live</option>
						<option value="tapcrowd_demo">Demo</option>
					</select>
				</p>
				<?php endif; ?>
	            
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<button type="submit" class="btn primary"><?= __('Save') ?></button>
					<br clear="all" />
				</div>
				<br clear="all" />
			</form>
		</div>
	</div>
<?php else: ?>
	<div>
		<h1><?=__('Create new application')?></h1>
		<div class="frmsessions">
			<form action="<?= site_url('apps/blank') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
				<?php if($error != ''): ?>
				<div class="error"><?= $error ?></div>
				<?php endif ?>
				<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
					<label for="name"><?= __('Name:') ?></label>
					<input type="text" name="name" id="name" value="<?= htmlspecialchars_decode(set_value('name', ''), ENT_NOQUOTES) ?>" />
				</p>
<!-- 				<p <?= (form_error("info")) ? 'class="error"' : '' ?>>
					<label for="info"><?= __('Info:') ?></label>
					<textarea name="info" id="info" ><?= htmlspecialchars_decode(set_value('info', '')) ?></textarea>
				</p> -->
				<p <?= (form_error("blankurl") != '') ? 'class="error"' : '' ?>>
					<label for="blankurl" style="margin-bottom:5px;"><?= __('URL of your HTML5 app:') ?></label>
					<input type="text" name="blankurl" id="blankurl" value="<?= set_value('blankurl', '') ?>">
				</p>
	            
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<button type="submit" class="btn primary"><?= __('Save') ?></button>
					<br clear="all" />
				</div>
				<br clear="all" />
			</form>
		</div>
	</div>
<?php endif; ?>