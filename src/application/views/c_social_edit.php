<div>
	<h1><?= __('Edit Social media')?></h1>
	<div id="myTab">
	<div class="tabbable tabs-below">
		<ul class="nav nav-tabs">
			<li class="active">
				<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#facebookpane"><?= __('Facebook')?></a>
			</li>
			<li class="">
				<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#twitterpane"><?= __('Twitter')?></a>
			</li>
		</ul>
		<br clear="all" />
		<div class="frmsessions">
			<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
				<?php if($error != ''): ?>
				<div class="error"><?= $error ?></div>
				<?php endif ?>    
				<div class="tab-content">
					<div id="facebookpane" class="tab-pane active">      
						<p <?= (form_error("facebookid") != '') ? 'class="error"' : '' ?>>
							<label for="facebookid"><?= __('Facebook page id or name:')?></label>
							<input type="text" name="facebookid" id="facebookid" value="<?= (set_value('facebookid') != '') ? set_value('facebookid') : $social->facebookid ?>" />
						</p>
						<p>
							<label for="postorwall"><?= __('Show facebook posts or wall:')?></label>
							<input class="checkboxClass" type="radio" name="postorwall" value="posts" <?= ($social->postorwall == 'posts') ? 'checked' : '' ?> /> <?= __('Posts from me only')?> <br />
							<input class="checkboxClass" type="radio" name="postorwall" value="wall" <?= ($social->postorwall == 'wall') ? 'checked' : '' ?> /> <?= __('Posts from everyone')?> 
						</p>
			<!-- 			<p <?= (form_error("facebookappid") != '') ? 'class="error"' : '' ?>>
							<label for="facebookappid">Facebook app id:</label>
							<input type="text" name="facebookappid" id="facebookappid" value="<?= (set_value('facebookappid') != '') ? set_value('facebookappid') : $social->facebookappid ?>" />
						</p> -->
			<!-- 			<p <?= (form_error("rss") != '') ? 'class="error"' : '' ?>>
							<label for="rss">RSS page:</label>
							<input type="text" name="rss" id="rss" value="<?= (set_value('rss') != '') ? set_value('rss') : $social->RSS ?>" />
						</p> -->
			<!-- 			<p <?= (form_error("youtube") != '') ? 'class="error"' : '' ?>>
							<label for="youtube">Youtube Channel:</label>
							<input type="text" name="youtube" id="youtube" value="<?= (set_value('youtube') != '') ? set_value('youtube') : $social->youtube ?>" />
						</p> -->
					</div>
					<div id="twitterpane" class="tab-pane">
						<?php if($error != ''): ?>
						<div class="error"><?= $error ?></div>
						<?php endif ?>          
						<p <?= (form_error("twitter") != '') ? 'class="error"' : '' ?>>
							<label for="twitter"><?= __('Twitter name:')?></label>
							<input type="text" name="twitter" id="twitter" value="<?= (set_value('twitter') != '') ? set_value('twitter') : $social->twitter  ?>" />
						</p>
						<p>
							<input class="checkboxClass" type="checkbox" name="mentions" <?= ($social->mentions == 1) ? 'checked="checked"' : '' ?> /> <?= __('Show mentions as well?')?>
						</p>
			 			<p <?= (form_error("twithash") != '') ? 'class="error"' : '' ?>>
							<label for="twithash"><?= __('Twitter hash:')?></label>
							<input type="text" name="twithash" id="twithash" value="<?= (set_value('twithash') != '') ? set_value('twithash') : $social->twithash ?>" />
						</p>						
					</div>
					<div class="buttonbar">
						<input type="hidden" name="postback" value="postback" id="postback">
						<button type="submit" class="btn primary"><?= __('Edit Social Media')?></button>
						<br clear="all" />
					</div>
					<br clear="all" />
				</form>
			</div>
		</div>
	</div>
	</div>
</div>