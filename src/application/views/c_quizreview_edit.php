<h1><?= __('Edit Review') ?></h1>
<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<p <?= (form_error("minimum_score") != '') ? 'class="error"' : '' ?>>
			<label for="minimum_score"><?= __('Minimum score:') ?></label>
			<input type="text" name="minimum_score" id="minimum_score" value="<?= set_value('minimum_score', $review->minimum_score) ?>" />
		</p>
		<p <?= (form_error("maximum_score") != '') ? 'class="error"' : '' ?>>
			<label for="maximum_score"><?= __('Maximum score:') ?></label>
			<input type="text" name="maximum_score" id="maximum_score" value="<?= set_value('maximum_score', $review->maximum_score) ?>" />
		</p>
		<p <?= (form_error("reviewtext") != '') ? 'class="error"' : '' ?>>
			<label for="reviewtext"><?= __('Review text:') ?></label>
			<input type="text" name="reviewtext" id="reviewtext" value="<?= set_value('reviewtext', $review->reviewtext) ?>" />
		</p>
		<p <?= (form_error("reviewimage") != '') ? 'class="error"' : '' ?>>
			<label for="reviewimage"><?= __('Image:') ?></label>
			<input type="file" name="reviewimage" id="reviewimage" />
			<br clear="all" />
		</p>
		<p <?= (form_error("reviewvideo") != '') ? 'class="error"' : '' ?>>
			<label for="reviewvideo"><?= __('Video url:') ?></label>
			<input type="text" name="reviewvideo" id="reviewvideo" value="<?= set_value('reviewvideo', $review->reviewvideo) ?>" />
		</p>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<button type="submit" class="btn primary"><?= __('Edit Review') ?></button>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>