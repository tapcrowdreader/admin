<?php
/**
 * Tapcrowd list view
 * @author Tom Van de Putte
 */

$edit_url = str_replace('--id--', $data->id, $settings->edit_url);

# Get struct values
if(!is_object($data)) {
	echo 'Data is not a object';
	return;
}

if(empty($data->id)) {
	$title = 'Add %s';
} else {
	$title = 'Edit %s #%d';
}
?>
<h2><?=__($title, $settings->singular, $data->id)?></h2>

<form method="POST" action="<?=$edit_url?>" class="form-horizontal">
<?php

# Hidden params
if(!empty($data->id)) { ?>
	<input type="hidden" name="id" value="<?=$data->id?>" /><?php
}

# Get struct specific parameters
// $prefix = strtolower(get_class($data)) . '_';
// $struct_params = array_diff_key(get_object_vars($data), get_class_vars('\Tapcrowd\Struct'));
$struct_params = array_diff_key(get_object_vars($data), array('id'=>null, 'appid'=>null));

while(list($n,$v) = each($struct_params)) {
	if(!empty($data->$n)) $v = $data->$n; ?>
	<div class="control-group">
		<label class="control-label" for="input_<?=$n?>"><?=$n?></label>
		<div class="controls">
			<input type="text" id="input_<?=$n?>" placeholder="" name="<?=$n?>" value="<?=$v?>" />
		</div>
	</div><?php
}

?>
<div class="control-group">
	<div class="form-actions">
		<button type="submit" class="btn btn-primary"><?=__('Save')?></button>
		<button onclick="history.back()" type="button" class="btn"><?=__('Cancel')?></button>
	</div>
</div>
</form>
<?php //var_dump($data);?>
