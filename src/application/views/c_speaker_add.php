<div>
	<h1><?= __('Add Item')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="name"><?= __('Name '. '(' . $language->name . ')'); ?>:</label>
                <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?=set_value('name_'.$language->key)?>" />
            </p>
            <?php endforeach; ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="description"><?= __('Description'. '(' . $language->name . ')'); ?>:</label>
                <textarea name="description_<?php echo $language->key; ?>" id="description"><?= set_value('description_'.$language->key) ?></textarea>
            </p>
            <?php endforeach; ?>
			<p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl"><?= __('Image:')?></label>
				<input type="file" name="imageurl" id="imageurl" value="" /><br />
				<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
				<br clear="all" />
			</p>
			<p class="clear <?= (form_error("company") != '') ? 'error' : '' ?>">
				<label><?= __('Company:')?></label>
				<input type="text" name="company" value="<?= set_value('company') ?>" id="company">
			</p>
			<p <?= (form_error("function") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Function:')?></label>
				<input type="text" name="function" value="<?= set_value('function') ?>" id="function">
			</p>
			<div class="row">
				<label for="tags">Tags:</label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order') ?>" id="order">
			</p>
            <?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<?php if(_checkMultilang($m, $lang->key, $app)): ?>
						<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
							<?= _getInputField($m, $app, $lang, $languages); ?>
						</p>
					<?php endif; ?>
				<?php endforeach; ?>
            <?php endforeach; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add Speaker')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if($postedtags){foreach($postedtags as $val){ echo '"'.$val.'", '; }} ?>],
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = '<?=$tag->tag?>';
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
	});
</script>