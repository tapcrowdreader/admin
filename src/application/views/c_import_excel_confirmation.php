<style>
    .myOdd { background-color: #F5F5F5;}
    .myEven { background-color: #FFFFFF;}
	.activeTab{display:block; cursor:pointer; float:left; width:auto; padding:8px; background-color:#FFFFFF; border:1px solid #DDDDDD; border-bottom:1px solid #FFFFFF; margin-bottom:-1px;}
	.inactiveTab{display:block; cursor:pointer; float:left; width:auto; padding:8px; background-color:#f5f5f5; border:1px solid #DDDDDD; border-bottom:1px solid #DDDDDD; margin-bottom:-1px;}
</style>
<div>
	<div class="frmsessions">
		<h1><?= __('Import') ?></h1>
		<p><?= __('Are you sure you want to add this content?') ?><br />
		
  		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit" id="xpoimport" name="xpoimport">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
         
        <br clear="all"/>
            
	    <?php if($view_data != '') { $totalRows = count($view_data); $total_cols = count($view_data[0]); ?>
        <?php
			$contentIndex = array();
						
			$totalSheets = ( isset( $sheetsName ) && count( $sheetsName ) ) > 0 ? count( $sheetsName ) : 0;
			if($totalSheets):
				$cls = 'activeTab';
				foreach($sheetsName as $key=>$val):
				$contentIndex[] = $key;
				?>
                	<div id="tab_<?=$key?>" onclick="displayToggle('<?=$key?>');" class="<?=$cls?>"><?=$val?></div>
                <?php
					$cls = 'inactiveTab';
				endforeach;
				?>
            <div style="clear:both; width:100%"></div>
        <?php endif;?>
        <div style="overflow:auto; max-height: 450px;" id="content_<?= isset( $contentIndex[0] ) ? $contentIndex[0] : 1;?>">
            <table class="display zebra-striped">
            <?php
            for($i=0; $i< $totalRows; $i++){
                if($i%2 == 0) $ccClass = 'myEven';
                else $ccClass = 'myOdd';
                
                if($i == 0):
           ?>
                <thead>
                    <tr class="<?= $ccClass ?>">
                        <?php
                        foreach($view_data[$i] as $cell=>$val){//for($j=0; $j< $total_cols; $j++)
                            echo '<th><div style="min-width:125px;">'.$view_data[$i][$cell].'</div></th>';
                        }
                        ?>
                    </tr>
                </thead>
                <?php
                else:
                    echo '<tr class="'.$ccClass.'">';
                    foreach($view_data[$i] as $cell=>$val){ //for($j=0; $j< $total_cols; $j++)
                        echo '<td>'.$view_data[$i][$cell].'</td>';
                    }
                    echo '</tr>';                    
                endif;
                }
            ?>
            </table>
            
            
            
        </div>
        
        <?php } ?>
        
        <?php if($view_data2 != '') { $totalRows = count($view_data2); $total_cols = count($view_data2[0]); ?>
        
        <div style="overflow:scroll; height: auto; display:none;" id="content_<?= isset( $contentIndex[1] ) ? $contentIndex[1] : 2;?>">
            <table class="display zebra-striped">
            <?php
            for($i=0; $i< $totalRows; $i++){
                if($i%2 == 0) $ccClass = 'myEven';
                else $ccClass = 'myOdd';
                
                if($i == 0):
           ?>
                <thead>
                    <tr class="<?= $ccClass ?>">
                        <?php
                        foreach($view_data2[$i] as $cell=>$val){//for($j=0; $j< $total_cols; $j++)
                            echo '<th><div style="min-width:125px;">'.$view_data2[$i][$cell].'</div></th>';
                        }
                        ?>
                    </tr>
                </thead>
                <?php
                else:
                    echo '<tr class="'.$ccClass.'">';
                    foreach($view_data2[$i] as $cell=>$val){ //for($j=0; $j< $total_cols; $j++)
                        echo '<td>'.$view_data2[$i][$cell].'</td>';
                    }
                    echo '</tr>';                    
                endif;
                }
            ?>
            </table>
        </div>
        
        <?php } ?>
            
		
        <br clear="all"/>
        
		<div class="buttonbar">
           	<input type="hidden" name="datafile" value="<?= $datafile; ?>" id="datafile">
				<input type="hidden" name="postback" value="saveconfirm" id="postback">
                <button type="button" class="btn primary" id="cancelimport" onclick="cancel_import();"><?= __('Cancel') ?></button>
				<button type="submit" class="btn primary" id="startimport"><?= __('Import') ?></button>
                
				<br clear="all" />
		</div>
			<br clear="all" />
		</form>
	</div>	
</div>
<script language="javascript">
function displayToggle(arg){	
	jQuery('[id^="tab_"]').attr('class', 'inactiveTab');
	jQuery('#tab_'+arg).attr('class', 'activeTab');
	
	jQuery('[id^="content_"]').hide();
	jQuery('#content_'+arg).show();
}
function cancel_import()
{
	document.forms.xpoimport.postback.value = "cancelimport";
	
	document.forms.xpoimport.submit();
}
</script>