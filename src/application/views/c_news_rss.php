<div>
	<h1><?= __('Import RSS') ?></h1>
	<div class="frmsessions">
		<form id="form" action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><p><?= $error ?></p></div>
			<?php endif ?>          
			<p <?= form_error("url") ? 'class="error"' : '' ?>>
			    <label for="url"><?= __('Url :')?></label>
			    <input type="text" name="url" id="url" value="<?=set_value('url') ?>" />
			</p>
			<p <?= (form_error("refreshrate") != '') ? 'class="error"' : '' ?>>
			    <label for="refreshrate"><?= __('Refresh the content after how many minutes?')?></label>
			    <input type="text" name="refreshrate" id="refreshrate" value="<?=set_value('refreshrate', 60)?>" />
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" id="submitbtn" class="btn primary"><?= __('IMPORT')?></button>
				<br clear="all" />
			</div>
				<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#submitbtn').click(function(event) {
			event.preventDefault();
			$('#submitbtn').attr("disabled", "true");
			$("#form").submit();
		});
	});
</script>