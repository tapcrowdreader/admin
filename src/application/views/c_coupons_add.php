<style type="text/css">

	.hidden {
		display: none;
	}
	
	img.previewimg {
		 max-width: 600px;
		 max-height: 480px;
	}
</style>

<div id="coupons">
	<h1><?= __('Add Coupon')?></h1>
	<div class="frmsessions">
		<form name="addcoupon" action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="add">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			
<!-- 			<p>
				<select class="options">
					<option value="basic"><?=__("Basic")?></option>
					<option value="advanced"><?=__("Advanced")?></option>
				</select>
				
				<label class="modedesc"><?=__("Basic Mode will allow you to introduce an image as your coupon")?></label>
			</p> -->
			
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p>
                    <label for="title"> <?= __("Title ").'(' . $language->name . ')'; ?>:</label>
                    <input maxlength="50" type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?=set_value('title_'.$language->key)?>"/>
                    <label><?= __('max 50 characters')?></label>
                </p>
                <?php endforeach; ?>
                <?php foreach($languages as $language) : ?>
                <p>
                    <label for="text"><?= __('Content ').'(' . $language->name . ')'; ?>:</label>
                    <textarea name="text_<?php echo $language->key; ?>" id="text"><?= set_value('text_'.$language->key) ?></textarea>
                </p>
                <?php endforeach; ?>
            <?php else : ?>
                <p>
                    <label for="title"><?= __('Title:') ?></label>
                    <input type="text" name="title_<?php echo $app->defaultlanguage; ?>" id="title" value="<?=set_value('title_'.$app->defaultlanguage)?>"/>
                </p>
                <p>
                    <label for="text"><?= __('Content:') ?></label>
                    <textarea name="text_<?php echo $app->defaultlanguage; ?>" id="text"><?= set_value('text_'.$app->defaultlanguage) ?></textarea>
                </p>
            <?php endif; ?>
           
 			<p align="center" <?= (form_error("image") != '') ? 'class="error"' : '' ?> >
 				<!-- <img class="previewimg" src=""> -->
				<label for="image"><?= __('Image:') ?></label>
				<input type="file" name="image" id="image" value="" class="imageupload"/>
			</p><br />
               
			<label for="group1"><?=__("Select for how long you want this coupon to be available:")?></label>
			<input type="radio" name="group1" value="1" checked><?=__("One Time")?><br>
			<input type="radio" name="group1" value="-1"><?=__("Unlimited")?><br>
         
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="button" class="btn primary cancel"><?= __('Cancel') ?></button>
				<button type="submit" class="btn primary"><?= __('Add Coupon') ?></button>
				<br clear="all" />
			</div> 
			
			<br clear="all" />
		</form>
	</div>
</div>

<script>
$(document).ready(function() {
	// $("#coupons select.options").change(
	// 	function ()
	// 	{
	// 		$("form[name=addcoupon] div.hidden").toggle();
	// 		var text=$("form[name=addcoupon] label.modedesc").text();
	// 		if(text.indexOf("Basic Mode")!=-1)
	// 			$("form[name=addcoupon] label.modedesc").text("<?=__("Advanced Mode allows you to add a specific title and content to your coupon and also an image")?>");
	// 		else
	// 			$("form[name=addcoupon] label.modedesc").text("<?=__("Basic Mode will allow you to introduce an image as your coupon")?>");
	// 	}
	// );
	
	
	$("form[name=addcoupon] button.cancel").click(
		function (){
			window.location.href = "<?php echo 'coupons/venue/'.$venue->id?>";
		}
	);
	// function readURL(input) {
	//     if (input.files && input.files[0]) {
	//         var reader = new FileReader();

	//         reader.onload = function (e) {
	//             $('.previewimg').attr('src', e.target.result);
	//     	}
	// 	}
	//     reader.readAsDataURL(input.files[0]);
	// }
});

</script>