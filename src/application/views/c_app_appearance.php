<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<?php if($app->familyid != 4) : ?>
<a href="<?=site_url('apps/theme/'.$app->id);?>" class="btn primary" style="float: right;"><?=__('Theme');?></a>
<a href="http://upload.tapcrowd.com/upload/adminuploads/Design_apps_V2.pdf" class="btn primary" style="float: right;margin-right:10px;"><?=__('Extra info');?></a>
<?php endif; ?>
<div id="jPicker">
	<h1><?=__('App Appearance')?></h1>
<script type="text/javascript">
  $(document).ready(
    function()
    {
      $('.Expandable').jPicker(
        {
          window:
          {
            expandable: true
          }
        });
    });
</script>
<div class="frmsessions">
	<?php if($app->apptypeid == 9 && !isset($_GET['appearance'])) : ?>
	<form action="<?= site_url('cssupload/updateapperance/'.$app->id) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
		<p style="min-height: 0px;"><?= __('<b>Customize CSS.</b><br /> Click <a href="http://clients.tapcrowd.com/defaultcss/contentflavor.css" target="_blank">here</a> to download the template stylesheet.') ?></p>
		<p>
		<textarea id="txt_css" name="txt_css" colls="50" rows="30" style="height:300px;width:590px;" ><?=(htmlspecialchars_decode(html_entity_decode(set_value('txt_css', $css))))?></textarea>
		</p>
<!-- 	<hr />        
		<p style="min-height: 0px;"><?= __('<b>Customize CSS for tablets.</b><br /> Click <a href="http://clients.tapcrowd.com/defaultcss/contentflavortablet.css" target="_blank">here</a> to download the template stylesheet.') ?></p>
		<p>
		<textarea id="txt_tabletcss" name="txt_tabletcss" colls="50" rows="30" style="height:300px;width:590px;" ><?=(htmlspecialchars_decode(html_entity_decode(set_value('txt_tabletcss', $tabletcss))))?></textarea>
		</p> -->
        <hr />
        
		<p style="min-height: 0px;"><?= __('<b>Customize HTML Header.</b>') ?></p>
		<p>
		<textarea id="header_html_phones" name="header_html_phones" colls="50" rows="20" style="height:200px;width:590px;" ><?=(htmlspecialchars_decode(html_entity_decode(set_value('header_html_phones', $headerphone))))?></textarea>
		</p>
<!-- 	<hr />
		<p style="min-height: 0px;"><?= __('<b>Customize HTML Header for tablets.</b>') ?></p>
		<p>
		<textarea id="txt_tabletcss" name="header_html_tablets" colls="50" rows="20" style="height:200px;width:590px;" ><?=(htmlspecialchars_decode(html_entity_decode(set_value('header_html_tablets', $headertablet))))?></textarea>
		</p> -->
        <hr />
		<p style="min-height: 0px;"><?= __('<b>Customize HTML Footer.</b>') ?></p>
		<p>
		<textarea id="txt_css" name="footer_html_phones" colls="50" rows="20" style="height:200px;width:590px;" ><?=(htmlspecialchars_decode(html_entity_decode(set_value('footer_html_phones', $footerphone))))?></textarea>
		</p>
<!-- 	<hr />
		<p style="min-height: 0px;"><?= __('<b>Customize HTML Footer for tablets.</b>') ?></p>
		<p>
		<textarea id="txt_tabletcss" name="footer_html_tablets" colls="50" rows="20" style="height:200px;width:590px;" ><?=(htmlspecialchars_decode(html_entity_decode(set_value('footer_html_tablets', $footertablet))))?></textarea>
		</p> -->
		<div class="buttonbar" style="margin-right:0px;">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" id="submit" class="btn primary"><?= __('Save') ?></button>
		</div>
	</form>
	<?php 
		//for contentmodule hide all other appearance settings
		else : 
	?>
		<form action="<?= site_url($this->uri->uri_string()) ?><?= isset($_GET['appearance']) ? '?appearance' : '' ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit" onsubmit="$('#preloader').css('display','block');">
			<table>
				<?php foreach($controls as $control) : ?>
						<tr>
						<?php if($control->typeid == 3) : ?>
							<td><label><?= $control->controltitle ?></label> </td>
							<td><input class="Expandable" style="display:none;" name="<?=$control->controlid?>" type="text" value="<?= (isset($control->value) && $control->value != '') ? $control->value : $control->defaultcolor  ?>" /></td>
						<?php elseif($control->typeid == 2) : ?>
								<td><label><?= $control->controltitle ?>: <span class="hintAppearance"><?= __('(recommended width is %s px, height:  %s px.', $control->image_width, $control->image_height) ?> <?= ($control->controlid == 4 || $control->controlid == 5 || $control->controlid == 55) ? '<strong>'.__('png image').'</strong>' : '' ?> <?= ($control->controlid == 4 || $control->controlid == 5) ? __("This is required to submit the app.") : '' ?>)</span></label> </td>

								<td><?php if($control->value != "" && file_exists($this->config->item('imagespath') . $control->value)){ ?><a href="<?=$this->config->item('publicupload') . $control->value?>" target="_blank"><span class="evtlogo" id="logo<?=$control->controlid?>" ><img src="<?= $this->config->item('publicupload') . $control->value ?>" width="50" /></span></a>
									<a href="<?= site_url('apps/removeimage/'.$app->id.'/'.$control->appearanceid)?>" id="remove<?=$control->controlid?>" class="deletemap" style="color:#000000;"><?= __('Remove image')?></a><?php } ?>

								<input type="file" name="<?=$control->controlid . '_image'?>" id="<?=$control->controlid?>" value="" class="logoupload" /><br clear="all"/>
								</td>
						<?php endif; ?>
						</tr>
				<?php endforeach; ?>
			</table>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" id="submit" class="btn primary"><?=__('Save')?></button>
				<br clear="all" />
			</div>
		</form>
	<?php endif; ?>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.deletemap').click(function(event) {
			event.preventDefault();
			var id = $(this).attr('id');
			var url = $(this).attr('href');
			$.ajax({
				type: "POST",
				url: url
			}).done(function( data ) {
				var logoid = id.substring(6,id.length);
				$("#"+id).css('display', 'none');
				$("#logo"+logoid).css('display', 'none');
			});
		});
	});
</script>
