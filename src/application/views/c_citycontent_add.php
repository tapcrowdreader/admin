<style type="text/css">
iframe {
	margin-left:15px !important;
}
</style>
<div>
	<h1><?= __('Add Content') ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
	                <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
	                    <label for="title"><?=__('Title')?> <?= '(' . $language->name . ')'; ?>:</label>
	                    <input type="text" name="title_<?=$language->key?>" value="<?= set_value('title_'.$language->key)?>" id="title">
	                </p>
                <?php endforeach; ?>
				<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
					<label for="image"><?= __('Image:') ?></label>
					<input type="file" name="image" id="image" value="" /><br />
					<span class="note" style="width: 490px; margin-left:130px;"><?= __('Max size %s px by %s px',2000,2000) ?></span>
					<br clear="all" />
				</p>
                <?php foreach($languages as $language) : ?>
	                <p style="min-height: 0px;">
		            <?php 
					$data = array(
							'name'			=> 'text_'.$language->key,
							'id'			=> 'text_'.$language->key,
							'toolbarset' 	=> 'Travelinfo',
							'value' 		=> htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('text_'.$language->key,'')))))),
							'width'			=> '500px',
							'height'		=> '400'
						);
					echo '<p style="min-height:0px;margin-top:-30px;"><label>Text ('.$language->name.')</label></p>';
					echo form_fckeditor($data);
					?>
					</p>
                <?php endforeach; ?>
  			<?php endif; ?>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add Content') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>