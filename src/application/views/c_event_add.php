<style>
form div.row ul.tagit input[type="text"] {
    background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 3px 3px 3px 3px;
    color: #333333;
    line-height: 18px;
    margin: 0;
    padding: 3px 2px;
    width: 610px;
}
</style>
<?php if(isset($_GET['newapp'])) : ?>
	<h1><?=__('Some more info')?></h1>
	<div class="progress progress-striped active">
		<div class="bar" style="width: 75%;"></div>
	</div>
<?php else: ?>
	<h1><?=__('Add New Event')?></h1>
<?php endif; ?>
<form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= str_replace(__('The event type field can not exceed 1 characters in length.'), __('Please choose an event type.'), $error) ?></div>
	<?php endif ?>
	<fieldset>
		<h3><?= __('General Info') ?></h3>
        <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("w_name_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="w_name_"><?= __('What is the name of your event?') ?> <?= '(' . $language->name . ')'; ?>:</label>
                <?php $defaultname = ''; if(isset($_GET['newapp'])) { $defaultname = $app->name;} ?>
                <input type="text" name="w_name_<?php echo $language->key; ?>" id="w_name" value="<?= set_value('w_name_'.$language->key, $defaultname) ?>" />
            </p>
            <?php endforeach; ?>
        <?php else : ?>
            <p>
                <label for="w_name"><?= __('What is the name of your event?') ?></label>
                <input type="text" name="w_name_<?php echo $app->defaultlanguage; ?>" id="w_name" value="<?= set_value('w_name_'.$app->defaultlanguage, ($app->familyid) != 3 ? $app->name : '') ?>" />
            </p>
        <?php endif; ?>
		<p <?= (form_error("permanent") != '' || form_error("permanent") != '') ? 'error' : '' ?>>
			<label for="permanent"><?= __('Permanent event?') ?></label>
			<input type="checkbox" name="permanent" id="permanent" value="checked" style="width:20px;"><br clear="all" />
		</p>
		<p <?= (form_error("w_datefrom") != '' || form_error("w_dateto") != '') ? 'error' : '' ?> id="dates">
			<label for="w_datefrom"><?= __('When will it be taking place?') ?></label>
			<input type="text" name="w_datefrom" id="w_datefrom" value="<?= set_value('w_datefrom', date('d-m-Y', time())) ?>" class="datepicker " autocomplete="off" />
			<input type="text" name="w_dateto" id="w_dateto" value="<?= set_value('w_dateto', date('d-m-Y', time())) ?>" class="datepicker" autocomplete="off" /><br clear="all" />
		</p>
		<p <?= (form_error("w_logo") != '') ? 'class="error"' : '' ?>>
			<label for="w_logo"><?= __('Logo:') ?></label>
			<input type="file" name="w_logo" id="w_logo" value="<?=set_value('w_logo')?>" class="logoupload" /><br clear="all"/>
			<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
		</p>
        <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="description"><?= __('Description') ?> <?= '(' . $language->name . ')'; ?>:</label>
                <textarea name="description_<?php echo $language->key; ?>" id="description"><?= set_value('description_'.$language->key) ?></textarea>
            </p>
            <?php endforeach; ?>
        <?php else : ?>
            <p <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="description"><?= __('Description:') ?></label>
                <textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description"><?= set_value('description_'.$app->defaultlanguage) ?></textarea>
            </p>
        <?php endif; ?>
		<p <?= (form_error("phonenr") != '') ? 'class="error"' : '' ?> >
			<label for="phonenr"><?= __('Phone number:') ?></label>
			<input type="text" name="phonenr" id="phonenr" value="<?= set_value('phonenr') ?>"/>
		</p>
		<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
			<label for="website"><?= __('Website:') ?></label>
			<input type="text" name="website" id="website" value="<?= set_value('website') ?>" />
		</p>
		<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
			<label for="email"><?= __('Email:') ?></label>
			<input type="text" name="email" id="email" value="<?= set_value('email') ?>" />
		</p>
		<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
			<label for="order"><?= __('Order:') ?></label>
			<input type="text" name="order" id="order" value="<?= set_value('order') ?>" />
		</p>
		<p <?= (form_error("ticketlink") != '') ? 'class="error"' : '' ?> >
			<label for="ticketlink"><?= __('Ticket link:') ?></label>
			<input type="text" name="ticketlink" id="ticketlink" value="<?= set_value('ticketlink') ?>"/>
		</p>
<!--         <div class="row">
			<label for="tags" id="labeltags">Tags:</label>
			<ul id="mytags" style="padding-left: 0px;"></ul>	
        </div> -->
	</fieldset>
	<fieldset>
		<h3><?=__('Location')?></h3>
<!-- 		<?php if(($app->apptypeid == 2 || $app->apptypeid == 4 || $app->apptypeid == 11) && !empty($venues)) : ?>
		<p <?= (form_error("selvenue") != '') ? 'class="error"' : '' ?>>
			<label for="login">Venue:</label>
			<select name="selvenue">
				<?php foreach($venues as $venue) : ?>
				<option value="<?=$venue->id?>"><?=$venue->name?></option>
				<?php endforeach; ?>
			</select>
		</p>
		<?php else: ?> -->
		<p <?= (form_error("w_venuename") != '') ? 'class="error"' : '' ?>>
			<label for="login"><?= __('Name:') ?></label>
			<input type="text" name="w_venuename" id="w_venuename" value="<?= set_value('w_venuename') ?>"/>
		</p>
		<p <?= (form_error("w_venuestreet") != '' || form_error("w_venuenumber")) ? 'class="error"' : '' ?> >
			<label for="venuestreet"><?= __('Street - N&deg;:') ?></label>
			<input type="text" name="w_venuestreet" id="w_venuestreet" value="<?= set_value('w_venuestreet') ?>" />
			<input type="text" name="w_venuenumber" id="w_venuenumber" value="<?= set_value('w_venuenumber') ?>" /><br clear="all" />
		</p>
		<p <?= (form_error("w_venuezip") != '' || form_error("w_venuetown") != '') ? 'class="error"' : '' ?>>
			<label for="venuezip"><?= __('Zip - Town:') ?></label>
			<input type="text" name="w_venuezip" id="w_venuezip" value="<?= set_value('w_venuezip') ?>" />
			<input type="text" name="w_venuetown" id="w_venuetown" value="<?= set_value('w_venuetown') ?>" />
		</p>
		<p <?= (form_error("w_venuecountry") != '') ? 'class="error"' : '' ?>>
			<label for="venuecountry"><?= __('Country:') ?></label>
			<input type="text" name="w_venuecountry" id="w_venuecountry" value="<?= set_value('w_venuecountry') ?>" />
		</p>
<!-- 		<?php endif; ?> -->
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<button type="submit" class="btn primary"><?= __('Add Event') ?></button>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if(set_value('item[tags][]')){foreach(set_value('item[tags][]') as $val){ echo '"'.$val.'", '; }} ?>],
		});

        var tags = new Array();
        var i = 0;
        <?php foreach($apptags as $tag) : ?>
        tags[i] = '<?=$tag->tag?>';
        i++;
        <?php endforeach; ?>
        $("#mytags input.tagit-input").autocomplete({
            source: tags
        });
		
        $("#labeltags").css('width','60px');
        $("#mytags").css('width','610px');

        //dates
        $('#w_datefrom').change(function(event) {
        	event.preventDefault();
        	$('#w_dateto').val(this.value);
        });
	});
</script>
<script type="text/javascript">
$('#permanent').click(function() {
	if($('#permanent').attr('checked') == 'checked') {
		$('#dates').css('display', 'none');
	} else {
		$('#dates').css('display', 'block');
	}
});
</script>