<html>
	<head>
		<meta content='initial-scale=0.5; maximum-scale=1.0; minimum-scale=0.5; user-scalable=0;' name='viewport' /> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		
		<title><?=__('Conference Bag')?></title>
		<base href="<?= base_url() ?>" target="" />
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/reset.css" type="text/css" />
		<style type="text/css" media="screen">
			
			html {  }
			body { background:#F6F6F6; font-family:"Lucida Grande","Lucida Sans",Arial,Verdana,Sans-Serif; font-size:1.3em; }
			
			#wrapper { }
			h1 { display:block; width:100%; height:auto; padding:5%; }
			
			.feedback { padding:5% 5% 5% 5%; background:#B4EDAD; color:#336306; }
			.error { padding:5% 5% 5% 5%; background:#F8AEAC; color:#5F0004; }
			
			form { padding:5%; }
			form p { width:inherit; margin:0 0 5% 0; }
			form p label { display:block; }
			form p input { width:100%; border:none; padding:10px; font-size:1.3em; }
			form p input[type="submit"] { background:#DDD; }
			
			p { text-align:center; }
			
		</style>
		
	</head>
	<body>
		<div id="wrapper" style="padding:5%;">
			<?php /* ?>
			<?php if ($this->session->flashdata('feedback_giftbag') != ''): ?>
				<div class="feedback"><?= $this->session->flashdata('feedback_giftbag') ?></div>
			<?php endif ?>
			<?php if ($error != ''): ?>
			<p class="error"><?= $error ?></p>
			<?php endif ?>
			<?php if ($complete): ?>
				<h1>Giftbag already collected!</h1>
			<?php else: ?>
				<?php
				switch ($aantal) {
					case 2:
				?>
				<h1>You can now collect your giftbag</h1>
				<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8">
					<p>
						<label for="code">Code</label>
						<input type="text" name="code" value="<?= set_value('code') ?>" id="code" autocomplete="off">
					</p>
					<p>
						<input type="hidden" name="collect" value="collect" id="collect">
						<input type="submit" value="Collect">
					</p>
				</form>
				<?php
						break;
					case 0:
				?>
				<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8">
					<p>
						<label for="name">Name</label>
						<input type="text" name="name" value="<?= set_value('name') ?>" id="name" autocomplete="off">
					</p>
					<p>
						<label for="email">E-mail</label>
						<input type="text" name="email" value="<?= set_value('email') ?>" id="email" autocomplete="off">
					</p>
					<p>
						<label for="code">Code</label>
						<input type="text" name="code" value="<?= set_value('code') ?>" id="code" autocomplete="off">
					</p>
					<p>
						<input type="hidden" name="postback" value="postback" id="postback">
						<input type="submit" value="Check in">
					</p>
				</form>
				<?php
						break;
					default:
				?>
				<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8">
					<p>
						<label for="code">Code</label>
						<input type="text" name="code" value="<?= set_value('code') ?>" id="code" autocomplete="off">
					</p>
					<p>
						<input type="hidden" name="checkin" value="checkin" id="checkin">
						<input type="submit" value="Check in">
					</p>
				</form>
				<?php
						break;
				}
				?>
			<?php endif ?>
			<?php */ ?>
			<h1 style="padding:2% 0; margin:0 0 2% 0; font-size:1.5em; font-weight:bold; text-align:center; border-bottom:1px solid #DDD;"><?=__('Conference Bag')?></h1>
			<p>
				<?=__('Add presentation that you want <br />to receive to your \'Conferene Bag\' by pressing the <br /><strong>\'Add&nbsp;to&nbsp;conference&nbsp;bag\'</strong><br /> button in the Sessions.')?> 
			</p><br />
			<p>
				<?=__('Type your email once and we will automatically send you an email with your favorite presentations after the conference.')?>
			</p>
		</div>
	</body>
</html>
