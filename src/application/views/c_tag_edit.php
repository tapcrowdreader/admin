<div>
	<h1><?= __('Edit Tag')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            
            <p <?= (form_error("tagname") != '') ? 'class="error"' : '' ?>>
                <label for="tagname"><?= __('Tagname:')?></label>
                <input type="text" name="tagname" id="tagname" value="<?=$tagname?>" />
            </p>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:')?></label>
				<span class="evtlogo" <?php if($image != ''){ ?>style="background:transparent url('<?= image_thumb($image, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="image" id="image" value="" class="logoupload" />
				<br clear="all"/>
				<?php if($image != '' && file_exists($this->config->item('imagespath') . $image)){ ?><span><a href="<?= site_url('tag/removeimage/'.$tagname.'/'.$app->id) ?>" class="deletemap"><?= __('Remove')?></a></span><?php } ?>
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order', $order) ?>" id="order">
			</p>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('apps/view/'.$app->id) ?>" class="btn"><?= __('Cancel')?></a>
				<button type="submit" class="btn primary"><?= __('Save')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>