<div>
	<h1><?=__('Add Question')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<?php foreach($fields as $field) : ?> 
				<?php if($field->formfieldtypeid == 10) : ?>
					<p <?= (form_error($field->label) != '') ? 'class="error"' : '' ?>>
						<label for="<?= str_replace(' ', '', $field->label) ?>"><?= $field->label ?></label>
					</p>
				<?php elseif($field->formfieldtypeid == 1 || $field->formfieldtypeid == 5) : ?>
					<p <?= (form_error($field->label) != '') ? 'class="error"' : '' ?>>
						<label for="<?= str_replace(' ', '', $field->label) ?>"><?= $field->label ?></label>
						<input type="text" name="<?= str_replace(' ', '', $field->label) ?>" id="<?= str_replace(' ', '', $field->label) ?>" value="<?= set_value($field->label) ?>" />
					</p>
				<?php elseif($field->formfieldtypeid == 17) : ?>
					<p <?= (form_error($field->label) != '') ? 'class="error"' : '' ?>>
						<label for="<?= str_replace(' ', '', $field->label) ?>"><?= $field->label ?></label>
						<select name="<?= str_replace(' ', '', $field->label) ?>">
							<option value=""></option>
							<?php foreach($speakers as $speaker) : ?>
							<option value="<?= $speaker->id ?>"><?= $speaker->name ?></option>
							<?php endforeach; ?>
						</select>
					</p>
				<?php endif; ?>
			<?php endforeach; ?>
	
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>