<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />

	<title><?= ( isset($masterPageTitle) && $masterPageTitle ) ? $masterPageTitle : __("{$this->channel->name} Admin")?></title>
	<base href="<?= base_url() ?>" target="" />


	<!-- CSS -->
<!-- 	<link rel="stylesheet" href="css/reset.css" type="text/css" /> -->

	<link rel="stylesheet" href="css/masters.css" type="text/css" />

	<link rel="stylesheet" href="css/jquery-ui-1.8.5.custom.css" type="text/css">
	<link rel="stylesheet" href="css/jquery.alerts.css" type="text/css" />
	<link rel="stylesheet" href="css/breadcrumb.css" type="text/css" />
	<link rel="stylesheet" href="css/datatables.css" type="text/css" />
	<link rel="stylesheet" href="css/debug.css" type="text/css" />
	<link rel="stylesheet" href="css/jquery.multiselect.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/uploadify.css" type="text/css" />
	<link rel="stylesheet" href="css/jquery.jqplot.min.css" type="text/css" />
	<!-- <link rel="stylesheet" href="/js/iviewer/jquery.iviewer.css" type="text/css" /> -->
	<?php if($this->channel->templatefolder != ''){ ?>
		<link rel="stylesheet" href="templates/<?= $this->channel->templatefolder ?>/css/master.css" type="text/css" />
	<?php } ?>

	<link rel="Stylesheet" type="text/css" href="css/jpicker-1.1.6.min.css" />
	<link rel="Stylesheet" type="text/css" href="css/jPicker.css" />

	<?php
	#
	# Load jQuery : Decide jQuery version to use
	#
	$jquery_version = (ENVIRONMENT == 'production')? '1.7.2/jquery.min.js' : '1.7.2/jquery.js';?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/<?=$jquery_version?>"></script>


	<!-- IE STYLESHEET -->
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" type="text/css" /><![endif]-->





<!--   <script type="text/javascript" charset="utf-8" src="js/uploadify/jquery.uploadify-3.1.js"></script> -->
		<!-- IE7 UPDATE MESSAGE -->
		<!--[if lte IE 7]>
			<script type="text/javascript">
				if(typeof jQuery == 'undefined'){ document.write("<script type=\"text/javascript\"   src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\"></"+"script>"); var __noconflict = true; }
				var IE6UPDATE_OPTIONS = {
					icons_path: "http://static.ie6update.com/hosted/ie6update/images/"
				}
			</script>
			<script type="text/javascript" src="http://static.ie6update.com/hosted/ie6update/ie6update.js"></script>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
				  $('<div></div>').html(IE6UPDATE_OPTIONS.message || 'U maakt momenteel gebruik van een verouderde browser.Om de site optimaal te bezoeken dient u uw browser te updaten. Download hier een nieuwere versie van Internet Explorer... ').activebar(window.IE6UPDATE_OPTIONS);
				});
			</script>
			<script src="js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
			<script>DD_belatedPNG.fix('.png');</script>
		<![endif]-->

<!--
		<?php if ($_SERVER['HTTP_HOST'] == "admin.tapcrowd.com"): ?>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-23619195-1']);
			_gaq.push(['_setDomainName', '.tapcrowd.com']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<?php endif ?> -->
</head>
<body>
	<?php $app = _currentApp(); $isAdmin = _isAdmin(); ?>
	<div id="preloader">
		<span class="status"><?=__('Loading...')?></span>
	</div>
	<?php if (!$this->session->userdata('mijnevent') && $this->channel->id != '3' && $this->channel->id != '4' && $this->channel->id != '6'): ?>
	<div id="header">
		<div id="logo">
			<?php
			if(!empty($this->channel->templatefolder)) {
				$src = 'templates/'.$this->channel->templatefolder.'/img/header_logo.png'; ?>
				<a href="<?=base_url()?>"><img src="<?=$src?>" class="png" height="35" style="padding-top:5px;"/></a><?php
			} elseif($this->templatefolder != '') { ?>
				<?php if(file_exists("templates/".$this->templatefolder."/img/header_logo.png")) : ?>
				<a href="<?= base_url()?>"><img src="templates/<?=$this->templatefolder?>/img/header_logo.png" class="png" height="35" style="padding-top:5px;"/></a>
			<?php else : ?>
				<a href="<?= base_url() ?>"><img src="img/layout2/logo-tapcrowd.png" width="182" height="41" class="png" /></a>
			<?php endif; ?>
			<?php } else { ?>
				<a href="<?= base_url() ?>"><img src="img/layout2/logo-tapcrowd.png" width="182" height="41" class="png" /></a>
			<?php } ?>
		</div><?php

		# Language selector
		// if(defined('ENVIRONMENT') && ENVIRONMENT != 'production') { ?>
			<ul id="languageSelect"><?php
			$currentLang = $this->input->cookie('language', true);
			foreach(getAvailableLanguages() as $lang) {
				if($lang == 'en' || $lang == 'pt' || $lang == 'nl') {
				$class = (($lang == $currentLang)? 'selected locale_' : 'locale_') . $lang;  ?>
				<li class="<?= $class ?>"><a href="" hreflang="<?= $lang ?>"><?= $lang ?></a></li><?php
				}
			} ?>
			</ul><?php
		// } ?>


		<ul id="navigation">
			<a href="<?= site_url('contactus')?>" class="contactusbtn"><span class="icon-envelope icon-white contactusbtnicon"> &nbsp;</span><span><?= __('Contact us') ?></span></a>
			<?php
			if($this->session->userdata('name') != "") {

				# Get locations
				if($this->channel->hideMyAccount != 1) $account_url = \Tapcrowd\API::getLocation('','account');
				if($isAdmin) $admin_url = \Tapcrowd\API::getLocation('admin','admin');
				$logout_url = \Tapcrowd\API::getLocation('auth/logout','admin');


				# New account controller
				$account_url = \Tapcrowd\API::getLocation('account'); ?>

				<!-- <li><a href="" title="Dashboard"><i class="icon-home icon-white"></i> <?=__('Dashboard');?></a></li> -->
				<li><a href="<?= site_url('apps/myapps') ?>" title="Apps"><i class="icon-th-large icon-white"></i> <?=__('My Apps');?></a></li><?php
				if(!empty($account_url)) { ?>
					<li><a href="<?= $account_url ?>"><i class="icon-user icon-white"></i> <?=__('Profile');?></a></li><?php
				}
				if(!empty($admin_url)) { ?>
					<li><a href="<?= site_url('admin') ?>"><i class="icon-user icon-white"></i> <?=__('Admin');?></a></li><?php
				} ?>
				<li><a href="<?=$logout_url?>" class="last"><i class="icon-off icon-white"></i> <?=__('Logout');?></a></li>
			<?php } ?>
		</ul>
	</div>

	<?php endif ?>
	<br clear="all">
	<div id="wrapper" <?= ($this->session->userdata('mijnevent')) ? "class='iframe'" : "" ?>>
		<div id="menu-container">
			<?php if($app) : ?>
			<div class="menuApp well">
				<div class="app-header row-fluid">
					<div class="span3">
						<?php if($app->app_icon != '') : ?>
							<a href="<?= site_url('apps/edit/'.$app->id); ?>"><img alt="" src="<?= $this->config->item('publicupload') . $app->app_icon ?>" width="70"></a>
						<?php else: ?>
							<a href="<?= site_url('apps/edit/'.$app->id); ?>"><img alt="" src="img/placeholder-70px.png"></a>
						<?php endif; ?>
					</div>
				<div class="span8">
					<span class="nav-header"><?=$app->name?></span>
					<span class="app-flavor"><?=$app->flavortitle?></span>
				</div>
				</div>
			</div>
			<ul class="nav nav-list">
				<?php if($app->apptypeid != 12) : ?>
				<li>
					<a href="<?= site_url('apps/view/'._currentApp()->id); ?>">
						<i class="icon-white icon-home"></i> <?=__('Modules');?>
					</a>
				</li>
				<?php endif; ?>
				<?php if(isset($modules)) : ?>
					<ul class="modules-list">
					<?php foreach($modules as $m) :?>
						<?php if($m->controller != 'speakers') : ?>
							<?php if(!empty($m->controller)) : ?>
								<li <?= $m->selected == 1 ? 'class="active"' : '' ?>>
									<?php if(isset($m->active)) : ?>
										<?php if($m->controller == 'forms') : ?>
											<a href="<?= site_url($m->controller.'/'.$m->type.'/'.$m->typeid.'/'.$m->launcherid);?>"><?= restrictLength($m->title, 27)?></a>
										<?php elseif($m->controller == 'groups') : ?>
											<?php if($m->id == 50 || $m->id == 51 || $m->id == 52) : ?>
												<a href="<?= site_url($m->controller.'/view/'.$m->groupid.'/'.$m->type.'/'.$m->typeid.'/catalog');?>"><?= restrictLength($m->title, 27) ?></a>
											<?php else: ?>
												<a href="<?= site_url($m->controller.'/view/'.$m->groupid.'/'.$m->type.'/'.$m->typeid);?>"><?= restrictLength($m->title, 27) ?></a>
											<?php endif; ?>
										<?php elseif($m->controller == 'venues' || $m->controller == 'events') : ?>
											<a href="<?= site_url($m->controller.'/launcher/'.$m->launcherid);?>"><?= restrictLength($m->title, 27)?></a>
										<?php elseif($app->apptypeid == 4 && ($m->id == 21 || $m->id == 22 || $m->id == 23)) : ?>
											<a href="<?= site_url($m->controller.'/edit/'.$m->typeid.'/'.$m->type.'/'.$m->component);?>"><?= restrictLength($m->title, 27) ?></a>
										<?php elseif($m->id == 24 || $m->id == 25 || $m->id == 27) : ?>
											<a href="<?= site_url($m->controller.'/'.$m->type.'/'.$m->typeid.'/0/'.$m->component);?>"><?= restrictLength($m->title, 27) ?></a>
										<?php else: ?>
											<a href="<?= site_url($m->controller.'/'.$m->type.'/'.$m->typeid);?>"><?= restrictLength($m->title, 27) ?></a>
										<?php endif; ?>
									<?php else: ?>
										<span><?= $m->title ?></span>
									<?php endif; ?>
								</li>
							<?php else: ?>
								<li <?= $m->selected == 1 ? 'class="active"' : '' ?>>
									<a href="<?= site_url('dynamiclauncher/edit/'.$m->id.'/'.$m->type.'/'.$m->typeid);?>"><?= restrictLength($m->title, 27) ?></a>
								</li>
							<?php endif; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					</ul>
				<?php endif; ?>
				<?php if($app->apptypeid != 12) : ?>
				<li>
					<a href="<?=site_url('apps/theme/'.$app->id);?>">
						<i class="icon-white icon-pencil"></i> <?=__('Appearance');?>
					</a>
				</li>
				<?php endif; ?>
				<li>
					<a href="<?=site_url('apps/edit/'.$app->id);?>">
						<i class="icon-white icon-book"></i> <?=__('App Settings');?>
					</a>
				</li>
				<li>
					<a href="<?=site_url('analytics/app/'.$app->id);?>">
						<i class="icon-white icon-home"></i> <?=__('Analytics');?>
					</a>
				</li>
				<?php if($this->uri->segment(1) == 'analytics') : ?>
				<ul class="modules-list">
					<li <?= $this->uri->segment(2) == 'app' ? 'class="active"' : '' ?>>
						<a href="<?= site_url('analytics/app/'.$app->id) ?>"><?=__('Dashboard') ?></a>
					</li>
					<?php if(analyticsGrant($app->id)) : ?>
<!-- 					<li <?= $this->uri->segment(2) == 'usage' ? 'class="active"' : '' ?>>
						<a href="<?= site_url('analytics/usage/'.$app->id) ?>"><?=__('Usage') ?></a>
					</li> -->
					<li <?= $this->uri->segment(2) == 'content' ? 'class="active"' : '' ?>>
						<a href="<?= site_url('analytics/content/'.$app->id) ?>"><?=__('Content') ?></a>
					</li>
					<li <?= $this->uri->segment(2) == 'livemap' ? 'class="active"' : '' ?>>
						<a href="<?= site_url('analytics/livemap/'.$app->id) ?>"><?=__('Live Map') ?></a>
					</li>
					<li <?= $this->uri->segment(2) == 'replay' ? 'class="active"' : '' ?>>
						<a href="<?= site_url('analytics/replay/'.$app->id) ?>"><?=__('Replay') ?></a>
					</li>
					<li <?= $this->uri->segment(2) == 'realtime' ? 'class="active"' : '' ?>>
						<a href="<?= site_url('analytics/realtime/'.$app->id) ?>"><?=__('Realtime') ?></a>
					</li>
					<li <?= $this->uri->segment(2) == 'logs' ? 'class="active"' : '' ?>>
						<a href="<?= site_url('analytics/logs/'.$app->id) ?>"><?=__('Logs') ?></a>
					</li>
<!-- 					<li <?= $this->uri->segment(2) == 'report' ? 'class="active"' : '' ?>>
						<a href="<?= site_url('analytics/report/'.$app->id) ?>"><?=__('Report') ?></a>
					</li> -->
					<?php endif; ?>
				</ul>
				<?php endif; ?>
				<li>
					<a href="<?= site_url('buildapp/iphone/'.$app->id);?>">
						<i class="icon-white icon-search"></i> <?= __('Preview app'); ?>
					</a>
				</li>
				<?php if($this->uri->segment(1) == 'buildapp') : ?>
				<ul class="modules-list">
					<li <?= ($this->uri->segment(1) == 'buildapp' && $this->uri->segment(2) == 'iphone') ? 'class="active"' : '' ?>>
						<a href="<?= site_url('buildapp/iphone/'.$app->id);?>"><?= __('iPhone/iPad'); ?></a>
					</li>
					<li <?= ($this->uri->segment(1) == 'buildapp' && $this->uri->segment(2) == 'android') ? 'class="active"' : '' ?>>
						<a href="<?= site_url('buildapp/android/'.$app->id);?>"><?= __('Android'); ?></a>
					</li>
					<?php if($app->apptypeid != 12) : ?>
					<li>
						<a href="" onclick="window.open('http://<?=$app->subdomain?>.m.tap.cr', 'Preview', 'width=320,height=480,location=no,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no,scrollbars=yes'); return false"><?= __('Web app'); ?></a>
					</li>
					<?php endif; ?>
				</ul>
			<?php endif;

			# Submit Button
			$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
			if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.submit', $account) !== false) { ?>
				<li>
					<a href="<?= site_url('price/vat/'.$app->id);?>">
						<i class="icon-white icon-circle-arrow-right"></i>
						<?= __('Submit app') ?>
					</a>
				</li><?php
			} ?>

			</ul>
<!-- 			<br clear="all"/>
			<div style="width:255px;text-align:center;">
				<div class="dropdown">
					<a class="dropdown-toggle btn" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="" style="margin-top:20px;">
						<?= __('Preview app'); ?>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						<li><a href="<?= site_url('buildapp/iphone/'.$app->id);?>"><?= __('iPhone/iPad'); ?></a></li>
						<li><a href="<?= site_url('buildapp/android/'.$app->id);?>"><?= __('Android'); ?></a></li>
						<li><a href="" onclick="window.open('http://<?=$app->subdomain?>.m.tap.cr', 'Preview', 'width=320,height=480,location=no,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no,scrollbars=yes'); return false"><?= __('Web app'); ?></a></li>
					</ul>
				</div>

				<a href="<?= site_url('price/app/'.$app->id);?>" class="btn" style="margin-top:20px;"><?= __('Submit app') ?></a>
			</div> -->
			<?php endif; ?>
		</div>
		<div id="contentwrapper">
			<div id="content" class="span8"><?php

				/**
				 * Display System Notifications
				 */
				echo Notifications::getInstance();

				# Page Header
				if(isset($pageheader)) { echo $pageheader; } ?>
				<?php if(isset($crumb) && $app != null ):?>
				<div id="breadCrumb" class="breadCrumb row span8">
					<ul>
						<li class="first"><a href="<?= site_url('apps') ?>"><?=__('Home')?></a></li>
						<?php if(isset($app) && $app != FALSE && $this->uri->segment(1) != 'account') : ?>
							<?php if($app->apptypeid == 1 || $app->apptypeid == 5 || $app->apptypeid == 9) : ?>
								<li><a href="<?= site_url('apps/view/'.$app->id) ?>"><?= $app->name ?></a></li>
							<?php endif; ?>
						<?php endif ?>
						<?php $count = count($crumb); $i = 1; ?>
						<?php foreach ($crumb as $crumbitem => $link): ?>
						<li <?= ($i == $count) ? 'class="last"' : '' ?>><a href="<?= site_url($link) ?>"><?= $crumbitem ?></a></li>
						<?php $i++; ?>
						<?php endforeach ?>
					</ul>
				</div>
				<div class="span8" />
				<?php endif ?>

				<?/*Back button voor op MijnEvent*/?>
				<?php if ($this->session->userdata('mijnevent') && $this->uri->segment(1) != "event" && $this->uri->segment(2) != "view"): ?>
					<a href="<?= site_url('event/view/'.$event->id) ?>">&laquo; <?=__('Back')?></a>
				<?php endif ?>
				<?php if ($this->uri->segment(1) == "event" && $this->uri->segment(2) != "add" && $this->uri->segment(2) != "info"): ?>
					<h1 class="eventname"><?= $event->name ?></h1>
					<?php if(in_array($app->apptypeid, array(1,2,4,5,7,8,9,11))) : ?>
					<?php if ($event->active == 1): ?>
						<a href="<?= site_url('event/active/' . $event->id . '/off') ?>" class="onoff deactivate"><span><?=__('Deactivate event')?></span></a>
					<?php else: ?>
						<a href="<?= site_url('event/active/' . $event->id . '/on') ?>" class="onoff activate"><span><?=__('Activate event')?></span></a>
					<?php endif ?>
					<br clear="all" />
					<span class="eventstatus"><?=__('This event is currently')?> <strong><?= ($event->active == 1 ? 'active' : 'inactive') ?></strong></span>
					<?php endif; ?>
					<?php if($this->uri->segment(2) != 'edit') : ?>
						<br clear="all" />
					<?php endif; ?>

				<?php endif ?>
				<?php if ($this->uri->segment(1) == "venue" && $this->uri->segment(2) != "add"): ?>
					<h1 class="eventname"><?= $venue->name ?></h1>
					<?php if(in_array($app->apptypeid, array(3,5))) : ?>
					<?php if ($venue->active == 1): ?>
						<a href="<?= site_url('venue/active/' . $venue->id . '/off') ?>" class="onoff deactivate"><span><?=__('Deactivate venue')?></span></a>
					<?php else: ?>
						<a href="<?= site_url('venue/active/' . $venue->id . '/on') ?>" class="onoff activate"><span><?=__('Activate venue')?></span></a>
					<?php endif ?>
					<br clear="all" />
					<span class="eventstatus"><?=__('This' . ($app->apptypeid == 5) ? "POI" : "Venue" .' is currently')?> <strong><?= ($venue->active == 1 ? 'active' : 'inactive') ?></strong></span>
					<?php endif; ?>
					<?php if($this->uri->segment(2) != 'edit') : ?>
						<br clear="all" />
					<?php endif; ?>
				<?php endif ?>
				<?= isset($content) ? $content : __('"Page not found!"') ?>
			</div>
			<br clear="all" />
		</div>
		<div class="push"></div>
		<?php if (isset($sidebar) && $app): ?>
			<?php $sessionsidebar = $this->session->userdata('sidebar');?>
			<a href="<?=site_url('sidebar')?>" id="sidebar-btn"<?= ($sessionsidebar == 'hide') ? 'style="right:0px;"' : '' ?>><?= (!$sessionsidebar || $sessionsidebar == 'show') ? '<img src="img/layout2/button-close.png" />' : '<img src="img/layout2/button-open.png" />' ?></a>
			<div id="sidebar" <?= ($sessionsidebar == 'hide') ? 'style="display:none;right:-395px;"' : '' ?>>
				<div class="side-content">
					<?php if ($isAdmin && $this->uri->segment(1) == "admin"): ?>
						<ul class="general single">
							<li><a href="<?= site_url('admin/users') ?>" <?= ($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'users') ? 'class="sel"' : "" ?>><?=__('Users')?></a></li>
							<li><a href="<?= site_url('admin/apps') ?>" <?= ($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'apps') ? 'class="sel"' : "" ?>><?=__('Apps')?></a></li>
						</ul>
					<?php else: ?>
						<?= $sidebar?>
					<?php endif; ?>
				</div>
			</div>
		<?php endif ?>
	</div>



	<!-- JAVASCRIPT -->
	<script src="js/jquery-ui-1.8.5.custom.min.js"></script>
	<script src="js/jquery-ui-timepicker.js"></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.3.1/jquery.cookie.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/0.9.12/js/jquery.Jcrop.min.js"></script>
	<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBCeqS1Ln_lwZA8ZavvIi5qNQPTNjwcmt8&amp;v=3.exp&amp;sensor=false"></script>

	<!--[if lt IE 9]><script src="js/jquery.jqplot.1.0.8/excanvas.js"></script><![endif]-->
	<script src="js/jquery.jqplot.1.0.8/jquery.jqplot.min.js"></script>
	<script src="js/jquery.jqplot.1.0.8/plugins/jqplot.pieRenderer.min.js"></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
	<script src="js/bootstrap-dropdown.js"></script>
	<script src="js/bootstrap-tooltip.js"></script>
	<script src="js/bootstrap-tab.js"></script>

	<script src="js/jquery.maskedinput.1.3.1.min.js"></script>

	<script src="js/jquery.inlineedit.js"></script>
	<script src="js/jquery.iphone-switch.js"></script>
	<script src="js/jquery.jBreadCrumb.1.1.js"></script>
	<script src="js/jquery.dataTables.js"></script>
	<script src="js/jquery.qrcode.js"></script>
	<script src="js/jquery.multiselect.js"></script>
	<script src="js/jpicker-1.1.6.min.js"></script>

	<script src="js/tapcrowd.alerts.js"></script>
	<script src="js/tag-it.js"></script>
	<script src="js/spin.js"></script>
	<script src="js/debug.js"></script>
	<script src="js/qrcode.js"></script>
	<script src="js/metadata.js"></script>
	<script src="js/tapcrowd.js"></script>
	<script src="js/raphael-min.js"></script>


	<script>
	$(document).ready(function(){
		var scripts = [
		];
		for(var i in scripts) {
			var head= document.getElementsByTagName('head')[0];
			var script= document.createElement('script');
			script.type= 'text/javascript';
			script.src= scripts[i];
			head.appendChild(script);
		}
	});
	</script>



  <script type="text/javascript">

	// Enable Language selector (in header)
	$("#languageSelect a").click(function(e)
	{
		e.preventDefault();
		$.post('<?= site_url('translate/setlang') ?>/' + $(this).attr('hreflang'), function(data)
		{
			location.reload();
		});
		return false;
	});
  </script>
  <script type="text/javascript">
	$(document).ready(function() {
	<?php $sessionsidebar = $this->session->userdata('sidebar');
	if(!$sessionsidebar || $sessionsidebar == 'show') : ?>
	var sidebarToe = false;
	<?php else: ?>
	var sidebarToe = true;
	<?php endif; ?>
		$('#sidebar-btn').click(function(event) {
			event.preventDefault();
			var link = $(this).attr('href');
			if(!sidebarToe && $('#sidebar:animated').length <1){
				$(this).html('<img src="img/layout2/button-open.png" />');
				$('body').css('overflow-x', 'hidden');
				$(this).animate(
					{right:'-=395'},
					500,
					function() {});
				$('#sidebar').animate(
					{right:'-=395'},
					500,
					function(){
						sidebarToe = true;
						$('#sidebar').css('display', 'none');
						$('body').css('overflow-x', 'auto');
						$.ajax({
							type: "POST",
							url: link+'/hide'
						}).done(function( data ) {
						});
					}
				);
			}
			else if (sidebarToe && $('#sidebar:animated').length <1){
				$(this).html('<img src="img/layout2/button-close.png" />');
				$('body').css('overflow-x', 'hidden');
				$('#sidebar').css('display', 'block');

				$(this).animate(
					{right:'+=395'},
					500,
					function() {});
				$('#sidebar').animate(
					{right:'+=395'},
					500,
					function(){
						sidebarToe = false;
						$('body').css('overflow-x', 'auto');
						$.ajax({
							type: "POST",
							url: link+'/show'
						}).done(function( data ) {
						});
					}
				);
			}
		});

		var leftheight = $("#menu-container").height();
		var rightheight = $("#sidebar").height();
		var middleheight = $("#content").height();
		if(middleheight > leftheight) {
			leftheight = middleheight;
		}

		if(leftheight >= rightheight) {
			$("#sidebar").css('height', leftheight + 500);
		}

	});
  </script>


	<script type="text/javascript">
	document.write(unescape("%3Cscript src='" + document.location.protocol +
	  "//munchkin.marketo.net/munchkin.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script>Munchkin.init('771-HZH-025');</script>

	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-10861070-2']);
	  _gaq.push(['_setDomainName', 'tapcrowd.com']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>

	<!-- Google Analytics UA -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-44893075-1', 'tapcrowd.com');
	  ga('send', 'pageview');

	</script>
	<!-- End Google Analytics -->

</body>
</html>
