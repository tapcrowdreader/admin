<div>
	<div class="frmsessions">
		<h1><?= __('Import') ?></h1>
		<p><?= __('Please download our template file and fill in your data like the example.<br />
			<a href=" %s \'upload/excelfiles/templates/sessions.csv\'?>" target="_blank">You can download it right here.</a>',$this->config->item('publicupload')) ?></p>
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit" id="xpoimport">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("excelfile") != '') ? 'class="error"' : '' ?>>
				<label for="excelfile"><?=__('CSV File:')?></label>
				<input type="file" name="excelfile" id="excelfile" value="<?=set_value('excelfile')?>" class="logoupload" /><br clear="all"/>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary" id="startimport"><?= __('Start Import') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>	
</div>