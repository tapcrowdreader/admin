<div>
	<h1><?= __('Add Schedule') ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" class="addevent">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<?php foreach($languages as $language) : ?>
			<p <?= (form_error("when_".$language->key) != '') ? 'class="error"' : '' ?>>
				<label for="when_<?=$language->key?>"><?= __('When') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<input type="text" name="when_<?php echo $language->key; ?>" id="when" value="<?=set_value('when_'.$language->key)?>" />
			</p>
			<p <?= (form_error("fromtill_".$language->key) != '') ? 'class="error"' : '' ?>>
				<label for="when_<?=$language->key?>"><?= __('From-till') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<input type="text" name="fromtill_<?php echo $language->key; ?>" id="when" value="<?=set_value('fromtill_'.$language->key)?>" />
			</p>
			<?php endforeach; ?>
			<p <?= (form_error("sortorder") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Order:')?></label>
				<input type="text" name="sortorder" value="<?= set_value('sortorder') ?>" id="sortorder"><br/>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
                <?php if(isset($venue)) : ?>
                    <a href="<?= site_url('schedule/venue/'.$venue->id) ?>" class="btn"><?= __('Cancel') ?></a>
                <?php else : ?>
                    <a href="<?= site_url('schedule/event/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
                <?php endif; ?>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>