    
	<h1><?= __('Edit Screen') ?></h1>
    
    
    <div class="frmformbuilder">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            			
            
			<?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="title"><?=__('Title ')?> <?='(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('formscreen', $formscreen->id, 'title', $language->key); ?>
                <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$language->key, ($trans != null) ? $trans : $formscreen->title ), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            
            <?php else : ?>
            
            <p <?= (form_error("title_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="name"><?= __('Title:') ?></label>
                <input type="text" name="title_<?php echo $app->defaultlanguage; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$app->defaultlanguage, _getTranslation('formscreen', $formscreen->id, 'title', $app->defaultlanguage)), ENT_NOQUOTES) ?>" />
            </p>
            
            <?php endif; ?>
			
            
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:') ?></label>
				<input type="text" name="order" value="<?= set_value('order', $formscreen->order) ?>" id="order">
			</p>
			 
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('formscreens/form/'.$form->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>