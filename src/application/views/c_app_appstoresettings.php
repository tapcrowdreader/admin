<script src="js/jquery.fileupload.js"></script>
<script src="js/jquery.fileupload-ui.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		if($('#avatar').val() != '') {
			$('.avatar').attr('src', $('#avatar').val());
		}
		$('#avatar').change(function(){
			$('.avatar').attr('src', $('#avatar').val());
		});
		
	});
</script>
<div>
	<h1><?=__('Appstore Settings')?></h1>
	<div class="tabs-below">
		<ul class="nav nav-tabs">
			<li>
				<?php if($app->apptypeid == 12) : ?>
					<a href="apps/blank/<?=$app->id?>">Basic Settings</a>
				<?php else: ?>
					<a href="apps/edit/<?=$app->id?>">Basic Settings</a>
				<?php endif; ?>
			</li>
			<li class="active">
				<a href="apps/appstoresettings/<?=$app->id?>">Appstore Settings</a>
			</li>
		</ul>
	</div>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit" id="submitform">
			<?php if($error != ''): ?>
			<div class="error" style="clear:both;"><p><?= $error ?></p></div>
			<?php endif ?>
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label for="name"><?= __('Name:')?></label>
				<input type="text" name="name" value="<?= set_value('name', $app->name) ?>" id="name" class="required" readonly>
			</p>
			<p <?= (form_error("remarks") != '') ? 'class="error"' : '' ?>>
				<label for="remarks"><?= __('Description:')?></label>
				<textarea name="remarks" id="remarks"><?= set_value('remarks', $app->submit_description) ?></textarea>
			</p>
			<p <?= (form_error("category") != '') ? 'class="error"' : '' ?>>
				<label for="category"><?= __('App Category:')?></label>
				<select name="category[]" id="category" multiple="multiple" style="display:block; float:left; margin-left:7px;height:200px;">
					<option value="Book" <?= set_select('category[]', 'Book', ($app->category == 'Book' ? TRUE : FALSE)) ?>><?= __('Book')?></option>
					<option value="Business" <?= set_select('category[]', 'Business', ($app->category == 'Business' ? TRUE : FALSE)) ?>><?= __('Business')?></option>
					<option value="Catalogs" <?= set_select('category[]', 'Catalogs', ($app->category == 'Catalogs' ? TRUE : FALSE)) ?>><?= __('Catalogs')?></option>
					<option value="Education" <?= set_select('category[]', 'Education', ($app->category == 'Education' ? TRUE : FALSE)) ?>><?= __('Education')?></option>
					<option value="Entertainment" <?= set_select('category[]', 'Entertainment', ($app->category == 'Entertainment' ? TRUE : FALSE)) ?>><?= __('Entertainment')?></option>
					<option value="Finance" <?= set_select('category[]', 'Finance', ($app->category == 'Finance' ? TRUE : FALSE)) ?>><?= __('Finance')?></option>
					<option value="Food & Drink" <?= set_select('category[]', 'Food & Drink', ($app->category == 'Food & Drink' ? TRUE : FALSE)) ?>><?= __('Food & Drink')?></option>
					<option value="Games" <?= set_select('category[]', 'Games', ($app->category == 'Games' ? TRUE : FALSE)) ?>><?= __('Games')?></option>
					<option value="Health & Fitness" <?= set_select('category[]', 'Health & Fitness', ($app->category == 'Health & Fitness' ? TRUE : FALSE)) ?>><?= __('Health & Fitness')?></option>
					<option value="Lifestyle" <?= set_select('category[]', 'Lifestyle', ($app->category == 'Lifestyle' ? TRUE : FALSE)) ?>><?= __('Lifestyle')?></option>
					<option value="Medical" <?= set_select('category[]', 'Medical', ($app->category == 'Medical' ? TRUE : FALSE)) ?>><?= __('Medical')?></option>
					<option value="Music" <?= set_select('category[]', 'Music', ($app->category == 'Music' ? TRUE : FALSE)) ?>><?= __('Music')?></option>
					<option value="Navigation" <?= set_select('category[]', 'Navigation', ($app->category == 'Navigation' ? TRUE : FALSE)) ?>><?= __('Navigation')?></option>
					<option value="Photo & Video" <?= set_select('category[]', 'Photo & Video', ($app->category == 'Photo & Video' ? TRUE : FALSE)) ?>><?= __('Photo & Video')?></option>
					<option value="Productivity" <?= set_select('category[]', 'Productivity', ($app->category == 'Productivity' ? TRUE : FALSE)) ?>><?= __('Productivity')?></option>
					<option value="Reference" <?= set_select('category', 'Reference', ($app->category == 'Reference' ? TRUE : '')) ?>><?= __('Reference')?></option>
					<option value="Social Networking" <?= set_select('category[]', 'Social Networking', ($app->category == 'Social Networking' ? TRUE : FALSE)) ?>><?= __('Social Networking')?></option>
					<option value="Sports" <?= set_select('category[]', 'Sports', ($app->category == 'Sports' ? TRUE : FALSE)) ?>><?= __('Sports')?></option>
					<option value="Travel" <?= set_select('category[]', 'Travel', ($app->category == 'Travel' ? TRUE : FALSE)) ?>><?= __('Travel')?></option>
					<option value="Utilities" <?= set_select('category[]', 'Utilities', ($app->category == 'Utilities' ? TRUE : FALSE)) ?>><?= __('Utilities')?></option>
					<option value="Weather" <?= set_select('category[]', 'Weather', ($app->category == 'Weather' ? TRUE : FALSE)) ?>><?= __('Weather')?></option>
				</select>
				<br clear="all"/>
			</p>
			<p>
				<label><?= __('App icon:')?></label>
				<span class="hintAppearance"><?= __('Width must be %spx, height must be %spx. Fileformat must be PNG. The icon is required to submit the app.', 1024, 1024) ?></span>
				<span class="column">
					<?php foreach($appearance as $a) :?>
						<?php if($a->controlid == 5 && $a->value != '' && file_exists($this->config->item('imagespath') . $a->value)) :?>
							<span style="display:block; width:50px; height:50px; background:transparent url('<?= image_thumb($a->value, 50, 50) ?>') no-repeat center center; float:left;">&nbsp;</span>
						<?php endif; ?>
					<?php endforeach; ?>
				</span><br clear="both" />
				<input type="file" name="appicon" id="appicon" value="" style=" width:410px;">
			</p>
			<p <?= (form_error("homescreen") != '') ? 'class="error"' : '' ?>>
				<label for="homescreen"><?= __('Splash screen:')?><span class="hintAppearance"><?=__('iPhone, iPod touch & Android Splash screen must be .png file that is 640x960');?></span></label>
				<span class="column" style="width:60px;">
					<?php foreach($appearance as $a) :?>
						<?php if($a->controlid == 4 && $a->value != '' && file_exists($this->config->item('imagespath') . $a->value)) :?>
							<span style="display:block; width:50px; height:50px; background:transparent url('<?= image_thumb($a->value, 50, 50) ?>') no-repeat center center; float:left;">&nbsp;</span>
						<?php endif; ?>
					<?php endforeach; ?>
				</span><br clear="both" />
				<input type="file" name="homescreen" id="homescreen" value="" style="width:410px;"><br clear="all" />

				<label for="splash5"><?= __('Splash screen iPhone 5:')?><span class="hintAppearance"><?=__('iPhone 5 and iPod touch (5th gen) Splash screen must be .png file that is 640x1136');?></span></label>
				<span class="column" style="width:60px;">
					<?php foreach($appearance as $a) :?>
						<?php if($a->controlid == 55 && $a->value != '' && file_exists($this->config->item('imagespath') . $a->value)) :?>
							<span style="display:block; width:50px; height:50px; background:transparent url('<?= image_thumb($a->value, 50, 50) ?>') no-repeat center center; float:left;">&nbsp;</span>
						<?php endif; ?>
					<?php endforeach; ?>
				</span><br clear="both" />
				<input type="file" name="splash5" id="splash5" value="" style=" width:410px;"><br clear="all" />
			</p>
			<table>
				<tr>
					<td><?= 'iPhone screenshots' ?>:</td>
					<td>
						<?php $i = 1; while($i <= 5) :?>
							<p style="min-width:340px;margin-bottom:0px;min-height:auto;">
								<?php if(isset($iphonescreenshots[$i-1]) && file_exists($this->config->item('imagespath') . $iphonescreenshots[$i-1])) :?>
								<span style="display:block; width:50px; height:50px; background:transparent url('<?= image_thumb($iphonescreenshots[$i-1], 50, 50) ?>') no-repeat center center; float:left;">&nbsp;</span><br clear="both" />
								<?php else: ?>
								<input type="file" name="iphone_<?=$i?>" />
								<?php endif; ?>
							</p>
						<?php $i++; endwhile; ?>
					</td>
					<td><?= __('iPhone and iPod touch Screenshots must be .jpeg, .jpg, .tif, .tiff, or .png file that is %s pixels, at least 72 DPI, and in the RGB color space.', '960x640, 960x600, 640x960 or 640x920')?></td>
				</tr>
				<tr>
					<td><?= 'iPhone 5 screenshots' ?>:</td>
					<td>
						<?php $i = 1; while($i <= 5) :?>
							<p style="min-width:340px;margin-bottom:0px;min-height:auto;">
								<?php if(isset($iphone5screenshots[$i-1]) && file_exists($this->config->item('imagespath') . $iphones5creenshots[$i-1])) :?>
								<span style="display:block; width:50px; height:50px; background:transparent url('<?= image_thumb($iphone5screenshots[$i-1], 50, 50) ?>') no-repeat center center; float:left;">&nbsp;</span><br clear="both" />
								<?php else: ?>
								<input type="file" name="iphone5_<?=$i?>" />
								<?php endif; ?>
							</p>
						<?php $i++; endwhile; ?>
					</td>
					<td><?= __('iPhone 5 and iPod touch (5th gen) Screenshots must be .jpeg, .jpg, .tif, .tiff, or .png file that is %s pixels, at least 72 DPI, and in the RGB color space.', '1136x640, 1136x600, 640x1136 or 640x1096')?></td>
				</tr>
				<tr>
					<td><?= 'Android screenshots' ?>:</td>
					<td>
						<?php $i = 1; while($i <= 5) :?>
							<p style="min-width:340px;margin-bottom:0px;min-height:auto;">
								<?php if(isset($androidscreenshots[$i-1]) && file_exists($this->config->item('imagespath') . $androidscreenshots[$i-1])) :?>
								<span style="display:block; width:50px; height:50px; background:transparent url('<?= image_thumb($androidscreenshots[$i-1], 50, 50) ?>') no-repeat center center; float:left;">&nbsp;</span><br clear="both" />
								<?php else: ?>
								<input type="file" name="android_<?=$i?>" />
								<?php endif; ?>
							</p>
						<?php $i++; endwhile; ?>
					</td>
					<td>320 x 480, 480 x 800, 480 x 854, 1280 x 720, 1280 x 800 <?= _('24 bit PNG or JPEG (no alpha) Full bleed, no border.') ?></td>
				</tr>
			</table>
<!-- 			<p class="checklist">
				<label><?= __('App Store(s):')?></label>
				<input type="checkbox" name="chk_appstore" id="chk_appstore" value="Apple AppStore" <?= set_checkbox('chk_appstore', 'Apple AppStore') ?>>
				<label for="chk_appstore" class="chk"><?= __('Apple AppStore')?></label><br clear="all"/>
				<input type="checkbox" name="chk_androidm" id="chk_androidm" value="Android Market" <?= set_checkbox('chk_androidm', 'Android Market') ?>>
				<label for="chk_androidm" class="chk"><?= __('Google Play')?></label><br clear="all"/>
			</p>
			<p class="checklist">
				<label><?= __('Store region(s):')?></label>
				<input type="checkbox" name="chk_worldwide" id="chk_worldwide" value="Worldwide" <?= set_checkbox('chk_worldwide', 'Worldwide') ?>>
				<label for="chk_worldwide" class="chk"><?= __('Worldwide')?></label><br clear="all"/>
				<input type="checkbox" name="chk_europe" id="chk_europe" value="Europe only" <?= set_checkbox('chk_europe', 'Europe only') ?>>
				<label for="chk_europe" class="chk"><?= __('Europe only')?></label><br clear="all"/>
				<input type="checkbox" name="chk_us" id="chk_us" value="US only" <?= set_checkbox('chk_us', 'US only') ?>>
				<label for="chk_us" class="chk"><?= __('US only')?></label><br clear="all"/>
			</p> -->
			<div class="row">
				<label for="tags"><?= __('Searchterms:')?></label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" id="submitbtn" class="btn primary"><?= __('Save')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#mytags").tagit({
		initialTags: [<?php if($postedtags){foreach($postedtags as $val){ echo '"'.$val.'", '; }} ?>],
	});
});
</script>
