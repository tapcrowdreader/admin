<div>
	<h1><?= __('Edit %s', $object->name) ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
            <div class="error"><p><?= __('Something went wrong with the image upload. <br/> Maybe the image size exceeds the maximum width or height</p>') ?></div>
            <?php endif ?>

			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Premium order:')?></label>
				<input type="text" name="premiumorder" value="<?= set_value('premiumorder', $premium->sortorder) ?>" id="premiumorder">
				<label><?=__('Extra line:')?></label>
				<input type="text" name="extraline" value="<?= set_value('extraline', (isset($premium->extraline)) ? $premium->extraline : '') ?>" id="extraline">
				<label><?=__('Title (applied on all items):')?></label>
				<input type="text" name="premiumtitle" value="<?= set_value('premiumtitle', $premiumtitle) ?>" id="premiumtitle">
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="javascript:history.go(-1);" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>