<div>
	<h1><?= __('Edit Newsitem')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
            <div class="error"><p><?= __('Something went wrong with the image upload. <br/> Maybe the image size exceeds the maximum width or height') ?></p></div>
            <?php endif ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
            <?php $trans = _getTranslation('newsitem', $newsitem->id, 'title', $language->key); ?>
                <label for="title"><?= __('Title '.'(' . $language->name . ')'); ?>:</label>
                <input maxlength="500" type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$language->key, ($trans != null) ? $trans : $newsitem->title), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            <?php foreach($languages as $language) {
                $trans = _getTranslation('newsitem', $newsitem->id, 'txt', $language->key);
                $data = array(
                        'name'          => 'txt_'.$language->key,
                        'id'            => 'txt_'.$language->key,
                        'toolbarset'    => 'Travelinfo',
                        'value'         => htmlspecialchars_decode(html_entity_decode(htmlspecialchars(htmlentities(utf8_encode(set_value('txt_'.$language->key, ($trans != null) ? $trans : $newsitem->txt)))))),
                        'width'         => '680px',
                        'height'        => '400'
                    );
                echo '<p><label>'.__('Description').' ('.$language->name.')</label>';
                echo form_fckeditor($data);
                echo '</p>';
            } ?>
            <div class="row">
                <label for="tags"><?= __('Tags:') ?></label>
                <ul id="mytags" name="mytagsul"></ul>
            </div>
            <p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
                <label for="image"><?= __('Image:') ?></label>
                <span class="evtlogo" <?php if($newsitem->image != '' && file_exists($this->config->item('imagespath') . $newsitem->image)){ ?>style="width:50px; height:50px; background:transparent url('<?= image_thumb($newsitem->image, 50, 50) ?>') no-repeat center center;"<?php } ?>>&nbsp;</span>
                <input type="file" name="image" id="image" value="" class="logoupload" />
            </p><br />
            <p <?= (form_error("url") != '') ? 'class="error"' : '' ?>>
                <label><?= __('Youtube video url:') ?></label>
                <span class="hintAppearance"><?= __('Please use the embed url of the video. (http://youtube.com/embed/yourvideoid)') ?></span>
                <?php if(isset($newsitem->videourl) && !empty($newsitem->videourl) && empty($newsitem->url)) {
                    $newsitem->url = $newsitem->videourl;
                } ?>
                <input type="text" name="url" value="<?= set_value('url', $newsitem->url) ?>" id="url">
            </p>
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label><?= __('Order:') ?></label>
                <input type="text" name="order" value="<?= set_value('order', $newsitem->order) ?>" id="order">
            </p>
            <?php endif; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= (isset($venue)) ? site_url('news/venue/'.$venue->id) : site_url('news/event/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $("#mytags").tagit({
            initialTags: [<?php if(isset($tags) && $tags != false){foreach($tags as $val){ echo '"'.$val->tag.'", '; }} ?>],
        });

        var tags = new Array();
        var i = 0;
        <?php foreach($apptags as $tag) : ?>
        tags[i] = '<?=$tag->tag?>';
        i++;
        <?php endforeach; ?>
        $("#mytags input.tagit-input").autocomplete({
            source: tags
        });
    });
</script>