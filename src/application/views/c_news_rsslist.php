<div>
	<h1><?= __('RSS feeds') ?></h1>
	<?php if($currentRSSfeeds == null || count($currentRSSfeeds) < 5) : ?>
		<?php if(isset($section)) : ?>
			<a href="<?= site_url('section/importrss/'.$section->id) ?>" class="btn primary add" style="margin-top:-30px;"><?=__('Import RSS') ?></a><br clear="all"/>
		<?php else: ?>
			<a href="<?= site_url('news/rss/'.$typeid.'/'.$type) ?>" class="btn primary add" style="margin-top:-30px;"><?=__('Import RSS') ?></a><br clear="all"/>
		<?php endif; ?>
	<?php else : ?>
		<p>
			<?= __("You've reached your maximum amount of RSS feeds. Please remove one before you can add a new feed.") ?>
		</p>
	<?php endif; ?>
	<?php if($currentRSSfeeds != null && !empty($currentRSSfeeds)): ?>
		<div id="listview_wrapper" class="dataTables_wrapper">
			<table id="listview" class="display">
				<?php if ($currentRSSfeeds != FALSE): ?>
				<thead>
					<tr>
						<th class="data">
							<?= __('Url')?>
						</th>
						<th>
						</th>
						<th>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($currentRSSfeeds as $feed): ?>
						<tr>
							<td><?= $feed->url; ?></td>
							<!-- <td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/refreshsource/" . $feed->id) ?><?= isset($section) ? '/'.$section->id : '' ?>' class="adelete"><img src="img/icons/arrow_refresh_small.png" width="16" height="16" title="refresh" alt="Refresh" class="png" /></a></td> -->
							<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/deletesource/" . $feed->id) ?><?= isset($section) ? '/'.$section->id : '' ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="Delete" alt="Delete" class="png" /></a></td>
						</tr>
					<?php endforeach ?>
				</tbody>
				<?php endif ?> 
			</table>
		</div>
	<?php endif; ?>
</div>