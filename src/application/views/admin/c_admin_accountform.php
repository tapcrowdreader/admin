<?php

$displayed_account_properties = array(
	'channelId' => 'Channel',
	'login' => 'Login',
	'role' => 'Role',
	'email' => 'Email',
	'status' => 'Status',
	'mdate' => 'Last Modification',
	'cdate' => 'Creation time',
);

$apps = array();
?>

<div class="row span12">
<h2>
	<?=$account->fullname?>
<!-- 	<span class="label"><?=$account->role?></span> -->
<!-- 	<span class="label"><?=$account->status?></span> -->
</h2>

<div class="span8">
<form class="form-horizontal" method="POST" action="admin/users/save" autocomplete="off">

	<input type="hidden" name="accountId" value="<?=$account->accountId?>" />

	<div class="control-group">
		<label class="control-label" for="inputName"><?=__('Fullname')?></label>
		<div class="controls">
			<input class="span6" type="text" id="inputName" placeholder="<?=__('Name')?>" name="account_fullname" value="<?=$account->fullname?>">
			<span class="help-block alert alert-error hide">
				<strong><?=__('Error:')?></strong> <?=__('Name is too long')?>
			</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputEmail"><?=__('Email')?></label>
		<div class="controls controls-row">
			<input class="span6" type="text" id="inputEmail" placeholder="<?=__('Email')?>" name="account_email" value="<?=$account->email?>">
			<span class="help-block alert alert-error hide">
				<strong><?=__('Error:')?></strong> <?=__('Invalid Email entered')?>
			</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputRole"><?=__('Role')?></label>
		<div class="controls controls-row"><?php
		foreach($user_roles as $role) {
			$role = (object)$role;
			$checked = ($account->role == $role->name)? ' checked' : ''; ?>
			<label class="radio">
				<input type="radio" name="account_role" id="roleId<?=$role->name?>" value="<?=$role->name?>"<?=$checked?> />
				<strong><?=$role->name?></strong>
				<span class="help-inline"><?=$role->desc?></span>
			</label><?php
		} ?>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputStatus"><?=__('Status')?></label>
		<div class="controls controls-row"><?php
		foreach($user_statuses as $status) {
			$checked = ($account->status == $status)? ' checked' : ''; ?>
			<label class="radio inline">
				<input type="radio" name="account_status" id="statusId<?=$status?>" value="<?=$status?>"<?=$checked?> />
				<?=$status?>
			</label><?php
		} ?>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputLogin"><?=__('Login')?></label>
		<div class="controls">
			<input class="span6" type="text" id="inputLogin" placeholder=""  name="account_login" value="<?=$account->login?>">
			<span class="help-block alert alert-error hide">
				<strong><?=__('Error:')?></strong> <?=__('Invalid Login entered')?>
			</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputPassword"><?=__('Password (twice)')?></label>
		<div class="controls controls-row">
			<input class="span6" type="password" id="inputPassword" placeholder="<?=__('Password')?>" name="account_pass">
			<br /><br />
			<input class="span6" type="password" id="inputPassword2" placeholder="<?=__('Password (again)')?>" name="account_pass2">
			<span class="help-block alert alert-info">
				<strong><?=__('Note:')?></strong> <?=__('If not entered, password will NOT be overwritten')?>
			</span>
			<span class="help-block alert alert-error msg-passwordsdonotmatch hide">
				<strong><?=__('Error:')?></strong> <?=__('Passwords do not match')?>
			</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputIsPartner"><?=__('Is partner? (0 or 1)')?></label>
		<div class="controls">
			<input class="span6" type="text" id="inputIsPartner" placeholder=""  name="account_ispartner" value="<?=$account->ispartner?>">
		</div>
	</div>

	<div class="control-group hide">
		<label class="control-label"><?=__('Access Rights')?></label>
		<div class="controls controls-row">
			<label class=" inline hide"><input type="checkbox" /></label><?php
			foreach($apps as $app) {
				$checked = in_array($app->id, $rights)? ' checked="true"' : '';
				?><label class=" inline span2">
					<input type="checkbox" name="account_apprights[]" value="<?=$app->id?>"<?=$checked?>><?=$app->name?>
				</label><?php
			} ?>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<a class="btn" href="account/browse/"><?=__('Cancel')?></a>
			<button type="submit" class="btn btn-primary"><?=__('Save')?></button>
		</div>
	</div>
</form>
</div>

<div class="span4 pull-right">
<!-- 	<h3>Applications Rights</h3> -->
	<table class="table table-hover table-bordered">
		<tbody><?php
		foreach($applications as $app) {
			$owner = ($app->accountId == $account->id); ?>
			<tr>
				<td><a href="apps/view/<?=$app->id?>" title="<?=__('View Application')?>"><?=$app->name?></a></td>
				<td>
					<a href="admin/apps/permissions/<?=$app->id?>"
					   title="<?=__('Edit rights')?>" class="btn btn-mini btn-warning">
						<i class="icon-user icon-white"></i>
					</a>
				</td>
			</tr><?php
		} ?>
		</tbody>
	</table>
</div>
</div>
