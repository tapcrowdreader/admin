<?php

$_t = microtime(true); ?>

<table class="table table-striped table-bordered table-condensed table-hover table-engine"> <!--tc_bilities-->
	<colgroup class="table-sort-columns">
		<col class="sort-alpha" />
		<col span="<?=count($headers)+1?>" class="sort-num" />
	</colgroup>
	<thead>
		<th colspan="<?=count($headers)?>">
		<div class="row  well well-small">
			<h6>Import/Export</h6>
			<form action="<?= site_url('admin/translate/') ?>" method="POST" accept-charset="utf-8"
				class="data  pull-left" enctype="multipart/form-data">
				<input type="file" name="langfile" id="langfile" />
				<input type="hidden" name="postback" value="importlocalization" id="postback">
				<button type="submit" name="upload" value="upload" class="btn btn-primary"><?= __('Upload') ?></button>
			</form>
			<div class="pull-right">
				<a href="<?= site_url('admin/translate/export/android')?>" class="btn btn-success"><?=__('Download Android')?></a>
				<a href="<?= site_url('admin/translate/export/ios')?>" class="btn btn-success"><?= __('Download iOS') ?></a>
			</div>
		</div>
		<div class="row">
			<hr />
			<form class="form-search pull-left">
				<div class="input-append">
					<input name="q" type="text" class="input-large search-query" placeholder="<?=__(' Search')?>">
					<input name="filter" type="hidden" value="name"  />
					<button type="submit" class="btn"><i class="icon-search"></i></button>
				</div>
			</form>
			<form method="GET" action="" class="pull-left" style="margin-left:1em;"><?php
				foreach($languages as $lang) {
					$checked = '';
					foreach ($filteredLanguages as $filter) {
						if ($filter->language == $lang->language) {
							$checked = 'checked="checked"';
						}
					}
					$language = $lang->language;
					?>
					<label class="checkbox inline">
						<input type="checkbox" name="<?=$language?>" id="chkLang<?=$language?>" class="langOverview" value="1"<?=$checked?> />
						<?=$language?>
					</label><?php
				} ?>

				<strong style="margin-left:1em;"><?=__('Source: ')?></strong><?php
				foreach($sources as $source) {
					$selected = ($source == $selectedSource)? ' checked="checked"' : ''; ?>
					<label class="radio inline">
						<input type="radio" name="source" value="<?=$source?>"<?=$selected?>> <?=$source?>
					</label><?php
				} ?>
				<button type="submit" class="btn btn-primary" title="<?=__('Filter')?>"><i class="icon-filter icon-white"></i></button>
			</form>
			<div class="pull-right">
				<a href="<?= site_url('admin/translate/add/')?>" class="btn btn-success"><i class="icon-plus-sign icon-white"></i> <?=__('Add Text')?></a>
			</div>
		</div>
		<div class="infobox row span12">...</div>
		</th>
		</tr>
		<tr><?php

		$_cols = array('');

		foreach($headers as $h => $field) {
			$sort = ($field == 'lineCount')? ' class="sort-num"' : '';
// 			echo '<th'.(($field == 'edit')? ' class=""' : '')."$sort>$h</th>";
			if(strlen($h) == 2) {
				echo '<th class="sort-num">'."$h</th>";
			} else {
				echo '<th'."$sort>$h</th>";
			}
		} ?>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="<?=count($headers)?>">
				<div class="pagination"></div>
				<br /><br />
			</td>
		</tr>
	</tfoot>
	<tbody><?php

	# No data
	if(empty($data)) { echo '<tr><td colspan="'.count($headers).'">' . __('No data') . '</td></tr></tbody></table>'; return; }

	$url_edit = site_url('admin/translate/edit/'.$selectedSource.'/');
	$url_remove = site_url('admin/translate/remove/'.$selectedSource.'/');
	$lang_cnt = empty($filteredLanguages)? count($languages) : count($filteredLanguages);
	$lang_sel = empty($filteredLanguages)? $languages : $filteredLanguages;
	$row_fmt = "
	<tr>
		<td id=\"key_%s\">
			<p class=\"span2\">%s</p>
			<form class=\"table-filter\">
				<input type=\"hidden\" name=\"count\" value=\"%s\" />
				<input type=\"hidden\" name=\"name\" value=\"%2\$s\" />
			</form>
		</td>".
		str_repeat("\n\t\t".'<td><p class="span2">%s</p></td>', $lang_cnt)./* 		# For each language */ "
		<td>%3\$s</td>
		<td>
			<div class=\"pull-right\">
			<a href=\"$url_edit/%1\$s\" class=\"btn btn-mini btn-warning\" title=\"".__('Edit')."\"><i class=\"icon-pencil icon-white\"></i></a>
			<a href=\"$url_remove/%1\$s\" class=\"btn btn-mini btn-danger\" title=\"".__('Del')."\"><i class=\"icon-remove icon-white\"></i></a>
			</div>
		</td>
	</tr>";
	while($row = array_pop($data)) {
		$cnt = ($row->lineCount == 1)? '-' : (int)$row->lineCount;
		$d = array( $row_fmt, $row->id, htmlentities($row->k), $cnt );
		foreach($lang_sel as $l) {
			$v = $row->{$l->language};
			if(empty($v)) {
				$d[] = '-';
// 				$d[] = '';
			} else {
// 				$d[] = 'check';
				$d[] = htmlentities($v);
			}
		}
		echo call_user_func_array('sprintf', $d);
	}
?>
	</tbody>
</table>

<div id="tc_bilities_box" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Modal header</h3>
	</div>
	<div class="modal-body">
		<p>One fine body…</p>
	</div>
<!--	<div class="modal-footer">
		<a href="#" class="btn">Close</a>
		<a href="#" class="btn btn-primary">Save changes</a>
	</div>-->
</div>

<?php var_dump(microtime(true) - $_t); //3.784sec   //0.0125 ?>