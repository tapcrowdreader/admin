<div>
    <h1><?=__('Edit Module Template')?></h1>
    <div class="frmformbuilder">
        <form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
            <?php if($error != ''): ?>
                <div class="error"><?= $error ?></div>
            <?php endif ?>			
            <p <?= (form_error("title") != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Title:') ?></label>
                <input type="text" name="title" id="title" value="<?= htmlspecialchars_decode(set_value('title', $module->title ), ENT_NOQUOTES) ?>" />
            </p>
            
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label><?=__('Order:')?></label>
                <input type="text" name="order" value="<?= set_value('order', $module->order ) ?>" id="order">
            </p>

            <p <?= (form_error("url") != '') ? 'class="error"' : '' ?>>
                <label><?=__('Url:')?></label>
                <input type="text" name="url" value="<?= set_value('url', $module->url) ?>" id="url">
            </p>

            <p <?= (form_error("channelid") != '') ? 'class="error"' : '' ?>>
                <label for="channelid"><?= __('Channel:') ?></label>
                <span class="hintAppearance">If the module is available for all channels please select none.</span>  
                <?php foreach($channels as $channel) : ?>
                    <input type="checkbox" class="checkboxClass" name="channelid[]" value="<?= $channel->id ?>" <?= in_array($channel->id, $currentChannelids) ? 'checked="checked"' : '' ?> /> <?= $channel->name ?><br />
                <?php endforeach; ?>
            </p>
            
            <div <?= (form_error("apptype") != '') ? 'class="error"' : '' ?> style="margin-left: 15px;">
                <label for="apptype"><?= _('App Type')?>:</label>
                <?php foreach($apptypes as $apptype):
					$isChecked = ( $apptype->checked == 1 ) ? TRUE : FALSE;
				?>
                <input style="display: inline !important; width: auto !important; margin:5px 3px 8px 5px; height: 10px;" <?= ( $isChecked )? 'checked="checked"' : ''?> type="checkbox" name="apptype[]" id="apptype_<?=$apptype->id?>" value="<?=$apptype->id?>" onclick="changeSubFlavor(<?=$apptype->id?>);" /> <label style="display: inline-block !important; width: 130px !important;"><?= __($apptype->name)?></label>
                    <?php
                    if(count($apptype->subflavor)>0):
                    ?>
                    <div id="appSubFlavor_<?=$apptype->id?>" <?= ( $apptype->checked == 1 )? 'style="display:inline;"' : 'style="display:none;"'?>>
                    <?php
                        echo __('Sub Flavor').':';
                    ?>    
                       <select name="appsubflavor_<?=$apptype->id?>" style="width: 100px !important; display: inline !important;">
                           <?php foreach($apptype->subflavor as $key => $value):
                                    $selectedIndex = $apptype->subflavor['selected'];
                                    if($key !== 'selected'):?>
                                        <option <?= ( $selectedIndex == $apptype->subflavor[$key]->id ) ? 'selected="selected"' : '' ?> value="<?=$apptype->subflavor[$key]->id ?>"><?=ucfirst(__($apptype->subflavor[$key]->name))?></option>
                           <?php 
                                    endif;
                                endforeach;?>
                       </select>
                    </div>    
                    <?php
                    endif;
                    ?>
                    <br />
                <?php endforeach;?>
            </div>                                
            <div class="buttonbar">
                <input type="hidden" name="postback" value="postback" id="postback">
                    <a class="btn" href="<?= site_url('admin/moduletemplate')?>"><?= __('Cancel')?></a>
                    <button type="submit" class="btn primary"><?= __('Save') ?></button>
                    <br clear="all" />
            </div>
            <br clear="all" />
	</form>
    </div>
</div>
<script>
    function changeSubFlavor(flavorId){
        if(jQuery("#apptype_"+flavorId).is(':checked'))
            jQuery("#appSubFlavor_"+flavorId).css('display', 'inline');
        else
            jQuery("#appSubFlavor_"+flavorId).css('display', 'none');
    }
</script>