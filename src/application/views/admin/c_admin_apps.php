<!-- <script src="js/bootstrap.typeahead.js"></script> -->
<table class="table table-striped table-bordered table-condensed table-hover table-engine" style="width:900px !important;max-width:none;">
<!-- <caption>Application in Channel</caption> -->
<thead>
	<tr>
		<th colspan="4" class="row-fluid">
			<div class="pull-right">
			<form class="pull-left" style="margin-left:1em;" action="">
				<div class="input-append">
					<input name="q" type="text" class="input-medium search-query" placeholder="Search App" />
					<input name="filter" type="hidden" value="name"  />
					<button type="submit" class="btn"><i class="icon-search"></i></button>
				</div>
			</form>
			<form class="pull-left" style="margin-left:1em;">
				<div class="input-append">
					<input name="q" type="text" class="input-medium search-query" placeholder="Search Name" />
					<input name="filter" type="hidden" value="accountname"  />
					<button type="submit" class="btn"><i class="icon-search"></i></button>
				</div>
			</form>
			<form class="pull-left" style="margin-left:1em;">
				<div class="input-append">
					<input name="q" type="text" class="input-medium search-query" placeholder="Search Email" />
					<input name="filter" type="hidden" value="email"  />
					<button type="submit" class="btn"><i class="icon-search"></i></button>
				</div>
			</form>
			</div>
			<div class="row">
				<div class="infobox span6">...</div>
			</div>
		</th>

	</tr>
	<tr>
		<th class="span1">&nbsp;</th>
		<th class="span4">Title</th>
		<th class="span4">Account</th>
		<th class="span3">&nbsp;</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="4" class="pageindex">
			<div class="pagination"></div>
		</td>
	</tr>
</tfoot>
<tbody><?php
// $row = '
// <tr>
// 	<td><img src="%4$s" height="36" /></td>
// 	<td>
// 		<strong><a href="/apps/view/%d">%s</a></strong><br /><small>%s</small>
// 		<form class="table-filter" style="display:none;">
// 			<input type="hidden" name="name" value="%2$s" />
// 			<input type="hidden" name="accountname" value="%5$s" />
// 			<input type="hidden" name="email" value="%7$s" />
// 		</form>
// 	</td>
// 	<td>%5$s<br/><em>%7$s</em><span class="label pull-right">%6$s</span></td>
// 	<td style="text-align:right;">
// 		<a href="/admin/apps/analytics/%1$d" class="btn btn-small btn-info"><i class="icon-eye-open icon-white"></i> '.__('Stats').'</a>
// 		<a href="/apps/edit/%1$d" class="btn btn-small btn-warning"><i class="icon-pencil icon-white"></i> '.__('Edit').'</a>
// 		<a href="/apps/remove/%1$d" class="btn btn-small btn-danger"><i class="icon-remove icon-white"></i> '.__('Remove').'</a>
// 	</td>
// </tr>';
$row = '
<tr>
	<td><img src="%4$s" height="36" /></td>
	<td>
		<strong><a href="/apps/view/%d">%s</a></strong><br /><small>%s</small>
		<form class="table-filter" style="display:none;">
			<input type="hidden" name="name" value="%2$s" />
			<input type="hidden" name="accountname" value="%5$s" />
			<input type="hidden" name="email" value="%7$s" />
		</form>
	</td>
	<td>%5$s<br/><em>%7$s</em><span class="label pull-right">%6$s</span></td>
	<td style="text-align:right;">
		<a href="/admin/apps/permissions/%1$d" class="btn btn-small" title="'.__('Permissions').'"><i class="icon-user"></i></a>
		<a href="/admin/apps/analytics/%1$d" class="btn btn-small btn-info" title="'.__('Stats').'"><i class="icon-eye-open icon-white"></i></a>
		<a href="/apps/edit/%1$d" class="btn btn-small btn-warning" title="'.__('Edit').'"><i class="icon-pencil icon-white"></i></a>
		<a href="/apps/delete/%1$d" class="btn btn-small btn-danger" title="'.__('Remove').'"><i class="icon-remove icon-white"></i></a>
	</td>
</tr>';
foreach($apps as $app) {
	echo sprintf($row, $app->id, $app->name, $app->flavor->title, $app->app_icon, $app->accountname, $app->channel->name, $app->email);
} ?>
</tbody>
</table>
