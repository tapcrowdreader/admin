<table class="table table-striped table-bordered table-condensed table-hover table-engine" style="width:900px !important;max-width:none;">
<thead>
	<tr>
		<th colspan="5" class="row-fluid">
			<div class="pull-right">
			<form class="form-search pull-left" style="margin-left:1em;">
				<div class="input-append">
					<input name="q" type="text" class="input-medium search-query" placeholder="Search Name" />
					<input name="filter" type="hidden" value="accountname"  />
					<button type="submit" class="btn"><i class="icon-search"></i></button>
				</div>
			</form>
			<form class="form-search pull-left" style="margin-left:1em;">
				<div class="input-append">
					<input name="q" type="text" class="input-medium search-query" placeholder="Search Email" />
					<input name="filter" type="hidden" value="email"  />
					<button type="submit" class="btn"><i class="icon-search"></i></button>
				</div>
			</form>
			</div>
			<div class="row">
				<div class="infobox span8">...</div>
			</div>
		</th>

	</tr>
	<tr>
		<th class="span1">&nbsp;</th>
		<th class="span4">Name</th>
		<th class="span4">Email</th>
		<th class="span4">Role, Status</th>
		<th class="span3">&nbsp;</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="5" class="pageindex">
			<div class="pagination"></div>
		</td>
	</tr>
</tfoot>
<tbody><?php
$role_labels = array(
	'admin' => 'label-important',
	'user' => '',
);
$status_labels = array(
	'active' => 'label-success',
	'disabled' => 'label-warning',
	'' => 'label-important',
	'pending' => 'label-info',
	'deleted' => 'label-inverse',
);
$label = '<span class="label %s">%s</span> ';
$row = '
<tr>
	<td>&nbsp;</td>
	<td>
		<strong><a href="/admin/users/edit/%d">%s</a></strong><br />
		<form class="table-filter" style="display:none;">
			<input type="hidden" name="fullname" value="%2$s" />
			<input type="hidden" name="email" value="%3$s" />
		</form>
	</td>
	<td>%s</td>
	<td>%s</td>
	<td style="text-align:right;">
		<a href="/admin/users/edit/%1$d" class="btn btn-small btn-warning" title="'.__('Edit').'"><i class="icon-pencil icon-white"></i></a>
		<a href="/admin/users/remove/%1$d" class="btn btn-small btn-danger" title="'.__('Remove').'"><i class="icon-remove icon-white"></i></a>
	</td>
</tr>';
foreach($accounts as $account) {
	$labels = sprintf($label, $role_labels[$account->role], $account->role);
	$labels.= sprintf($label, $status_labels[$account->status], $account->status);
	echo sprintf($row, $account->id, $account->fullname, $account->email, $labels);
} ?>
</tbody>
</table>
