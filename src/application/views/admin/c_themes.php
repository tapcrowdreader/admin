<h1 style="text-align:center;"><?= __('Themes') ?></h1>
<a href="<?=site_url('admin/theme/add');?>" class="btn add"><?= __('Add Theme') ?></a>
<ul class="flavors">
	<?php foreach($themes as $theme) : ?>
		<li style="text-align: center;">
			<a href="<?=site_url('admin/theme/edit/'.$theme->id);?>" style="border-bottom: none;">
			<p style="text-align:center;">
				<b class="title"><?= __($theme->name) ?></b>
			</p>
			<img src="<?=$theme->screenshot?>" style="width:200px;"/>
			</a>
			<br />
			<span style="text-align: center;">(<?= $countIcons - $theme->countIcons . ' ' . __('Icons missing'); ?>)</span>
		</li>
	<?php endforeach; ?>
</ul>
