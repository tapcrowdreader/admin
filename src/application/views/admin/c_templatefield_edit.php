<div>  
    <h1>Edit Field</h1>
    <div class="frmformbuilder">
        <form action="<?= site_url($this->uri->uri_string()) ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
            <?php if($formfield->formfieldtypeid == 6) { $display = "block";} else {$display = "none";} ?>
            <?php if($formfield->formfieldtypeid != 10 || $formfield->formfieldtypeid != 11) { $reqdisplay = "block";} else {$reqdisplay = "none";} ?>
            <?php if($error != ''): ?><div class="error"><?= $error ?></div><?php endif ?>
            
            <p <?= (form_error("label") != '') ? 'class="error"' : '' ?>>
                <label for="name">Label:</label>
                <input type="text" name="label" id="label" value="<?= htmlspecialchars_decode(set_value('label', $formfield->label), ENT_NOQUOTES) ?>" />
            </p>
            
            <p <?= (form_error("formfieldtypeid") != '') ? 'class="error"' : '' ?>>
                <label for="formfieldtypeid">Type:</label>
                <select id="formfieldtypeid" name="formfieldtypeid" >
                    <option value="select">Select...</option>
                    <?php if(isset($fieldtypes)) { ?>
                    <?php foreach($fieldtypes as $e) : ?>
                    <option value="<?= $e->id ?>" <?= set_select('formfieldtypeid', $e->id, ($e->id == $formfield->formfieldtypeid ? TRUE : '')) ?>><?= $e->name; ?></option>
                <?php endforeach; ?>
                <?php } ?>
            </select>
        </p>
        
        <div id="option_fields" style="display:<?= $display; ?>">            
            <p <?= (form_error("possiblevalues") != '') ? 'class="error"' : '' ?>>
                <label for="possiblevalues"><?= __('Possible Values, comma separated')?>:</label>
                <input type="text" name="possiblevalues" id="possiblevalues" value="<?= htmlspecialchars_decode(set_value('possiblevalues', $formfield->possiblevalues), ENT_NOQUOTES) ?>" />
            </p>                
        </div>
        
        <div id="default_values">
            <p <?= (form_error("defaultvalue") != '') ? 'class="error"' : '' ?>>
                <label for="defaultvalue"><?= __('Default Value')?>:</label>
                <input type="text" name="defaultvalue" id="defaultvalue" value="<?= htmlspecialchars_decode(set_value('defaultvalue', $formfield->defaultvalue), ENT_NOQUOTES) ?>" />
            </p>
        </div>
        
        <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
            <label>Order:</label>
            <input type="text" name="order" value="<?= set_value('order', $formfield->order) ?>" id="order">
        </p>
        
			<!--<p <?= (form_error("xpos") != '') ? 'class="error"' : '' ?>>
				<label>Xpos:</label>
				<input type="text" name="xpos" value="<?= set_value('xpos', $formfield->xpos) ?>" id="xpos">
			</p>
            
            <p <?= (form_error("ypos") != '') ? 'class="error"' : '' ?>>
				<label>Ypos:</label>
				<input type="text" name="ypos" value="<?= set_value('ypos', $formfield->ypos) ?>" id="ypos">
			</p>
            
            <p <?= (form_error("width") != '') ? 'class="error"' : '' ?>>
				<label>Width:</label>
				<input type="text" name="width" value="<?= set_value('width', $formfield->width) ?>" id="width">
			</p>
            
            <p <?= (form_error("height") != '') ? 'class="error"' : '' ?>>
				<label>Height:</label>
				<input type="text" name="height" value="<?= set_value('height', $formfield->height) ?>" id="height">
			</p>-->
            <div id="required_fields" style="display:<?= $reqdisplay; ?>">
                
                <p  class="checklist" <?= (form_error("required") != '') ? 'class="error"' : '' ?>>
                    
                    <label>Required Field:</label>
                    <input type="checkbox" name="required" value="yes" id="required" <?= set_checkbox('required', $e->id, ("yes" == $formfield->required ? TRUE : '')) ?>>
                </p>
                
            </div>
            
            <div class="buttonbar">
                <input type="hidden" name="postback" value="postback" id="postback">
                <a href="<?= site_url('admin/templatefields/formscreen/'.$formscreen->id) ?>" class="btn">Cancel</a>
                <button type="submit" class="btn primary">Save</button>
                <br clear="all" />
            </div>
            <br clear="all" />
        </form>
    </div>
</div>