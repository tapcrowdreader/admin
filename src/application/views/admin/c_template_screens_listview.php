<?php if($this->session->flashdata('event_feedback') != ''): ?>
    <div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<?php if($this->session->flashdata('event_error') != ''): ?>
    <div class="error fadeout"><?= $this->session->flashdata('event_error') ?></div>
<?php endif ?>
<br clear="all">
<div class="listview">
    <div class="buttonbar">
        <a style="float:right;" class="add btn primary" href="admin/templatescreens/add/<?= $form->id?>"><i class="icon-plus-sign icon-white"></i> Add Screen</a></td>
    </div>
    <br clear="all">
    <table id="listview" class="display zebra-striped">
        <thead>
            <tr>
                <?php foreach ($headers as $h => $field): ?>
                <?php if ($h == 'Order'): ?>
                    <th class="data order"><?= $h ?></th>
                <?php else: ?>
                    <th class="data"><?= $h ?></th>
                <?php endif ?>
                 <?php endforeach; ?>
                <th width="17"><?=__('Edit')?></th>
                <th width="17"><?=__('Content')?></th>
                <th width="17"><?=__('Delete')?></th>
            </tr>
        </thead>
        <tbody>
            <?php if ($data != FALSE): ?>
            <?php foreach($data as $row): ?>
            <tr>
                <?php foreach ($headers as $h => $field): ?>
                <?php if ($field != null && !empty($field)) : ?>
                <td><?= $row->{$field} ?></td>
            <?php endif; ?>
        <?php endforeach ?>
        <td class="icon"><a href='<?= site_url("admin/templatescreens/edit/".$row->id)?>'><img src="img/Settings.png" width="22" height="22" alt="Bewerken" title="Bewerken" class="png" /></a></td>
        <td class="icon"><a href='<?= site_url('admin/templatefields/formscreen/'.$row->id)?>' ><i class="icon-pencil"></i></a></td>
        <td class="icon"><a href='<?= site_url("admin/templatescreens/delete/".$row->id)?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="Delete" alt="Delete" class="png" /></a></td>
        </tr>
        <?php endforeach ?>
        <?php endif ?>
        </tbody>
</table>
</div>
<script type="text/javascript" charset="utf-8">
jQuery(document).ready(function() {
    jQuery('.adelete').click(function(e) {
        var delurl = jQuery(this).attr('href');
        jConfirm('<?= __('Are you sure you want to delete this row?<br />This cannot be undone!') ?>', '<?= __('Remove Entry') ?>', function(r) {
            if(r == true) {
                window.location = delurl;
                return true;
            } else {
                return false;
            }
        });
        return false;
    });

    jQuery("#sidebar").height(jQuery('#content').height());
});
</script>