<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<?php if($this->session->flashdata('event_error') != ''): ?>
<div class="error fadeout"><?= $this->session->flashdata('event_error') ?></div>
<?php endif ?>
<br clear="all">
<div class="listview">
<div class="buttonbar">
    <a style="float:right;" class="add btn primary" href="admin/formtemplate/add"><i class="icon-plus-sign icon-white"></i> Add Form Template</a></td>
</div>
<br clear="all">
<table id="listview" class="display zebra-striped">
    <thead>
        <tr>
            <?php
            foreach ($headers as $h => $field):
                if ($h == 'Order'): ?>
                    <th class="data order"><?= $h ?></th>
                <?php else: ?>
                    <th class="data"><?= $h ?></th>
                <?php endif ?>
            <?php endforeach; ?>
            <th><?=__('Edit')?></th>
            <th><?=__('Content')?></th>
            <th><?=__('Delete')?></th>
        </tr>
    </thead>
    <tbody>
    <?php if ($data != FALSE):
            foreach($data as $row): ?>
                <tr>
                    <?php foreach ($headers as $h => $field): ?>
                        <?php if ($field != null && !empty($field)) : ?>
                                <td><?= $row->{$field} ?></td>
                        <?php endif; ?>
                    <?php endforeach ?>
                    <?php if($this->uri->segment(1) == 'admin' && $this->uri->segment(3) == 'forms'): ?>
                            <td class="icon"><a href='<?= site_url($this->uri->segment(1). '/' . $this->uri->segment(2) . "/edit/" . $row->id) ?>'><img src="img/Settings.png" width="22" height="22" alt="Bewerken" title="Bewerken" class="png" /></a></td>
                            <td class="icon"><a href='<?= $row->redirecturl ?>'><i class="icon-pencil"></i></a></td>
                            <td class="icon"><a href='<?= site_url('admin/formtemplate/remove/'.$row->id) ?>'><i class="icon-remove"></i></a></td>
                    <?php endif ;?>
		</tr>
            <?php endforeach ?>
	<?php endif; ?>
    </tbody>
    </table>
</div>
<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function() {
		jQuery("#sidebar").height($('#content').height());

	});
</script>
