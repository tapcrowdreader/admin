<h1 style="text-align:center;"><?= $theme->name ?></h1>
<img src="<?=$theme->screenshot?>" style="width:200px;margin-bottom:20px;"/>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<div id="jPicker">
<script type="text/javascript">
  $(document).ready(
    function()
    {
      $('.Expandable').jPicker(
        {
          window:
          {
            expandable: true
          }
        });
    });
</script>
<a href="<?= site_url('admin/theme/remove/'.$theme->id) ?>" class="delete btn" id="deletebtn" style="float:right;margin-top:-50px;margin-right:150px;"><span><?= __('Delete theme') ?></span></a>
<a href="<?=site_url('admin/theme/icons/'.$theme->id);?>" class="btn primary" style="color:#FFFFFF;float:right;margin-top:-50px;"><?=__('Edit icons');?></a>
<div class="frmsessions">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit" onsubmit="$('#preloader').css('display','block');">
		<button type="submit" id="submit" class="btn primary" style="float:right;"><?=__('Save')?></button>
		<p>
			<label><?= __('Name') ?>: </label>
			<input type="text" name="name" id="name" value="<?= $theme->name ?>" />
		</p>
		<p>
			<label><?= __('Screenshot image') ?>: </label>
			<input type="file" name="screenshot" id="screenshot" value="" class="logoupload" /><br clear="all"/>
		</p>
		<p>
			<label><?= __('Order') ?>: </label>
			<input type="text" name="sortorder" id="sortorder" value="<?= $theme->sortorder ?>" />
		</p>
		<p>
			<input type="checkbox" name="islive" id="islive" class="checkboxClass" <?= $theme->islive > 0 ? 'checked="checked"' : '' ?> /> <?= __('Theme available?') ?>
		</p>
		<table>
			<?php foreach($controls as $control) : ?>
					<tr>
					<?php if($control->appearancetype == 3) : ?>
						<td><label><?= $control->title ?></label> </td>
						<td><input class="Expandable" style="display:none;" name="<?=$control->id?>" type="text" value="<?= $control->defaultcolor ?>" /></td>
					<?php elseif($control->appearancetype == 2) : ?>
							<td><label><?= $control->title ?>: <span class="hintAppearance"><?= __('(recommended width is %s px, height:  %s px.', $control->image_width, $control->image_height) ?><?= ($control->id == 4 || $control->id == 5) ? __("This image can't be edited once the app has been submitted.") : '' ?>)</span></label> </td>
							<td><?php if(!empty($appearance[$control->id]->value)){ ?><span class="evtlogo" style="background:transparent url('<?= image_thumb($appearance[$control->id]->value, 50, 50) ?>') no-repeat top left; width:50px; height:50px;"></span><?php } ?>
							<?php if(!empty($appearance[$control->id]->value)){ ?><span style="text-align:left;margin-left:-15px;"><a href="<?= site_url('admin/theme/removeimage/'.$theme->id.'/'.$control->id) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?><br clear="all" />
							<input type="file" name="<?=$control->id . '_image'?>" id="<?=$control->id?>" value="" class="logoupload" /><br clear="all"/>
							
							</td>
					<?php endif; ?>
					</tr>
			<?php endforeach; ?>
		</table>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" id="submit" class="btn primary"><?=__('Save')?></button>
			<br clear="all" />
		</div>
	</form>
</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.delete').click(function(e) {
			delurl = $(this).attr('href');
			jConfirm('<?= __("Are you sure you want to remove this theme?") ?>', '<?= __("Remove Theme") ?>', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					return false;
				}
			});
			return false;
		});
	});
</script>