<h1 style="text-align:center;"><?= $theme->name ?></h1>
<img src="<?=$theme->screenshot?>" style="width:200px;margin-bottom:20px;"/>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<a href="<?=site_url('admin/theme/edit/'.$theme->id);?>" class="btn primary" style="color:#FFFFFF;float:right;margin-top:120px;"><?=__('Back');?></a>
<div class="frmsessions">
	<p><?= __('The maximum width and height of the icons is 140px. Format must be png'); ?></p>
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit" onsubmit="$('#preloader').css('display','block');">
		<button type="submit" id="submit" class="btn primary" style="float:right;"><?=__('Save')?></button>
		<table>
			<?php foreach($icons as $icon) : ?>
				<tr>
					<td>
						<label><?= $icon->title ?>: </label> 
						<img src="<?=$icon->icon;?>" width="50" />
					</td>
					<td>
					<input type="file" name="<?=$icon->moduletypeid?>" id="<?=$icon->moduletypeid?>" value="" class="logoupload" /><?php if(!empty($icon->icon)){ ?><span style="text-align:left;"><a href="<?= site_url('admin/theme/removeicon/'.$theme->id.'/'.$icon->moduletypeid) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?><br clear="all" />
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" id="submit" class="btn primary"><?=__('Save')?></button>
			<br clear="all" />
		</div>
	</form>
</div>