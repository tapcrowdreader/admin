<script>
(function(){
	var head = document.getElementsByTagName('head')[0];
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = '/js/admin.js';
	head.appendChild(script);
})();
</script>
<div class="row">
	<div class="row span12">
		<h1 class="span6"><?=$pagetitle?> <small><?=__('Administration')?></small></h1>
		<div class="btn-group pull-right">
			<a class="btn" href="/admin/apps/browse"><?=__('Apps')?></a>
			<a class="btn" href="/admin/theme"><?=__('Themes')?></a>
			<a class="btn" href="/admin/users"><?=__('Accounts')?></a>
		<?php
		# TapCrowd Only
		if(\Tapcrowd\Model\Channel::getInstance()->getCurrentChannel()->channelId == 1) { ?>
			<a class="btn" href="/admin/moduletemplate"><?=__('Module Templates')?></a>
			<a class="btn" href="/admin/formtemplate/forms"><?=__('Form Templates')?></a>
			<a class="btn" href="/admin/translate"><?=__('Translations')?></a>
			<a class="btn" href="/report"><?=__('Reports')?></a><?php
		} ?>
		</div>
	</div>
	<div class="row span12">
		<ul class="breadcrumb">
			<li><a href="/"><?=__('Home')?></a> <span class="divider">/</span></li>
			<li><a href="/admin"><?=__('Administration')?></a> <span class="divider">/</span></li><?php
			foreach($crumbs as $url => $title) { ?>
				<li><a href="<?=$url?>"><?=$title?></a> <span class="divider">/</span></li><?php
			} ?>
			<li class="active"><?=$pagetitle?></li>
		</ul>
	</div>
	<div class="clearfix"></div>
</div><?php

# Errors
if($error instanceof \Exception) {   ?>
	<div class="alert alert-error span12">
		<strong><?=__(get_class($error))?> </strong> <?=$error->getMessage()?>
	</div>
	<div class="clearfix"></div><?php
}
