<h1>Edit userdata</h1>
<?php if ($this->session->flashdata('edit_user') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('edit_user') ?></div>
<?php endif ?>
<div class="account">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" class="data">
		<?php if($error != ''): ?>
		<div class="error"><?= $error ?></div>
		<?php endif ?>
		<p <?= (form_error("username") != '') ? 'class="error"' : '' ?>>
			<label>Username:</label>
			<input type="text" name="username" value="<?= set_value('username', $user->name) ?>" id="username">
		</p>
		<p <?= (form_error("login") != '') ? 'class="error"' : '' ?>>
			<label>Login:</label>
			<input type="text" name="login" value="<?= set_value('login', $user->login) ?>" id="login">
		</p>
		<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
			<label>E-mail:</label>
			<input type="text" name="email" value="<?= set_value('email', $user->email) ?>" id="email">
		</p>
		<p <?= (form_error("credits") != '') ? 'class="error"' : '' ?>>
			<label>Credits (!):</label>
			<input type="text" name="credits" value="<?= set_value('credits', $user->credits) ?>" id="credits">
		</p>
		<p <?= (form_error("status") != '') ? 'class="error"' : '' ?>>
			<label>Status (!):</label>
			<select name="status" id="status">
				<option value="inactive" <?= set_select("status", "inactive", ($user->activation == 'inactive' ? TRUE : '')) ?>>inactive</option>
				<option value="pending" <?= set_select("status", "pending", ($user->activation == 'pending' ? TRUE : '')) ?>>pending</option>
				<option value="active" <?= set_select("status", "active", ($user->activation == 'active' ? TRUE : '')) ?>>active</option>
			</select>
		</p>
		<p>
			<label>Channel</label>
			<input type="text" name="channel" value="<?= $user->channelname ?>" id="channel" readonly>
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" class="save">Save</button>
		</div>
	</form>
</div>

<h1>Change Password <span class="note">( Minimum 6 characters, maximum 12 characters )</span></h1>
<div class="account">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" class="data">
		<?php if($error2 != ''): ?>
		<div class="error"><?= $error2 ?></div>
		<?php endif ?>
		<p <?= (form_error("npassword") != '') ? 'class="error"' : '' ?>>
			<label>New password:</label>
			<input type="password" name="npassword" value="" id="npassword">
		</p>
		<p <?= (form_error("cnpassword") != '') ? 'class="error"' : '' ?>>
			<label>Confirm password:</label>
			<input type="password" name="cnpassword" value="" id="cnpassword">
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postbackpass" value="postbackpass" id="postbackpass">
			<button type="submit" class="save">CHANGE PASSWORD</button>
		</div>
	</form>
</div>