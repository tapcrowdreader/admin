<div>
    <h1><?=__('Add Module Template')?></h1>
    <div class="frmformbuilder">
        <form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
            <?php if($error != ''): ?>
                <div class="error"><?= $error ?></div>
            <?php endif ?>			
            
            <p <?= (form_error("title") != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Title:') ?></label>
                <input type="text" name="title" id="title" value="<?=set_value('title')?>" />
            </p>
            
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label><?=__('Order:')?></label>
                <input type="text" name="order" value="<?= set_value('order') ?>" id="order">
            </p>

            <p <?= (form_error("url") != '') ? 'class="error"' : '' ?>>
                <label><?=__('Url:')?></label>
                <input type="text" name="url" value="<?= set_value('url') ?>" id="url">
            </p>

            <p <?= (form_error("channelid") != '') ? 'class="error"' : '' ?>>
                <label for="channelid"><?= __('Channels:') ?></label> 
                <span class="hintAppearance">If the module is available for all channels please select none.</span>                   
                <?php foreach($channels as $channel) : ?>
                    <input type="checkbox" class="checkboxClass" name="channelid[]" value="<?= $channel->id ?>" /> <?= $channel->name ?><br />
                <?php endforeach; ?>
            </p>
            
            <div <?= (form_error("apptype") != '') ? 'class="error"' : '' ?> style="margin-left: 15px;">
                <label for="apptype"><?= _('App Type')?>:</label>
                <?php foreach($apptypes as $apptype):?>
                <input style="display: inline !important; width: auto !important; margin:5px 3px 8px 5px; height: 10px;" type="checkbox" name="apptype[]" id="apptype_<?=$apptype->id?>" value="<?=$apptype->id?>" onclick="changeSubFlavor(<?=$apptype->id?>);" /> <label style="display: inline-block !important; width: 130px !important;"><?= __($apptype->title)?></label>
                    <?php
                    if(count($apptype->subflavor)>0):
                    ?>
                    <div id="appSubFlavor_<?=$apptype->id?>" style="display:none;">
                    <?php
                        echo __('Sub Flavor').':';
                    ?>    
                       <select name="appsubflavor_<?=$apptype->id?>" style="width: 100px !important; display: inline !important;">
                           <?php foreach($apptype->subflavor as $subflavor): ?>
                               <option value="<?=$subflavor->id ?>"><?=ucfirst(__($subflavor->name))?></option>
                           <?php endforeach;?>
                       </select>
                    </div>    
                    <?php
                    endif;
                    ?>
                    <br />
                <?php endforeach;?>
            </div>            
            <div class="buttonbar">
                <input type="hidden" name="postback" value="postback" id="postback">
                    <a class="btn" href="<?= site_url('admin/moduletemplate')?>"><?= __('Cancel')?></a>
                    <button type="submit" class="btn primary"><?= __('Add') ?></button>
                    <br clear="all" />
            </div>
            <br clear="all" />
	</form>
    </div>
</div>
<script>
    function changeSubFlavor(flavorId){
        if(jQuery("#apptype_"+flavorId).is(':checked'))
            jQuery("#appSubFlavor_"+flavorId).css('display', 'inline');
        else
            jQuery("#appSubFlavor_"+flavorId).css('display', 'none');
    }
</script>