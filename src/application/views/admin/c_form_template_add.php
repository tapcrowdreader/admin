<div>
    <h1><?=__('Add Form Template')?></h1>
    <div class="frmformbuilder">
        <form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
            <?php if($error != ''): ?>
                <div class="error"><?= $error ?></div>
            <?php endif ?>			
            
            <p <?= (form_error("title") != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Title:') ?></label>
                <input type="text" name="title" id="title" value="<?=set_value('title')?>" />
            </p>
            
            <p <?= (form_error("submissionbuttonlabel") != '') ? 'class="error"' : '' ?>>
                <label for="submissionbuttonlabel"><?= __('Submit Button Label:') ?></label>
                <input type="text" name="submissionbuttonlabel" id="submissionbuttonlabel" value="<?=set_value('submissionbuttonlabel')?>" />
            </p>	
            
            <p <?= (form_error("submissionconfirmationtext") != '') ? 'class="error"' : '' ?>>
                <label for="submissionconfirmationtext"><?= __('Confirmation Text:') ?></label>
                <textarea name="submissionconfirmationtext" id="submissionconfirmationtext"><?= set_value('submissionconfirmationtext') ?></textarea>
            </p>
            
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label><?=__('Order:')?></label>
		<input type="text" name="order" value="<?= set_value('order') ?>" id="order">
            </p>
            
            <p <?= (form_error("emailsendresult") != '') ? 'class="error"' : '' ?> >
                <label for="emailsendresult"><?= __('Email Results:') ?></label>
		<select name="emailsendresult" id="emailsendresult">
                    <option value="0"><?= __('Select ...') ?></option>
                    <option value="yes" selected="selected"><?= __('Yes') ?></option>
                    <option value="no"><?= __('No') ?></option>
		</select>
            </p>
            
            <p <?= (form_error("allowmutliplesubmissions") != '') ? 'class="error"' : '' ?> >
                <label for="allowmutliplesubmissions"><?= __('Allow Mutliple Submissions:') ?></label>
		<select name="allowmutliplesubmissions" id="allowmutliplesubmissions">
                    <option value="0"><?= __('Select ...') ?></option>
                    <option value="yes" selected="selected"><?= __('Yes') ?></option>
                    <option value="no"><?= __('No') ?></option>
		</select>
            </p>
            
            <p>
				<input style="display: inline !important;width: auto !important; margin:5px 3px 8px 5px;" type="checkbox" value="1" name="geolocation" id="geolocation" /><label style="display: inline !important;width: auto !important;"><?= _("Get User Location")?></label>
            </p>
            <p>
				<label style="display: inline !important;"><?= __('Multi Screen Form')?>: </label> <input style="width: auto !important; display: inline !important;" <?= ( $singlescreen == 0 ) ? 'checked="checked"' : ''; ?> type="radio" value="0" name="multiscreen" /> <?=  __('Yes') ?> &nbsp;&nbsp;<input style="width: auto !important; display: inline !important;" <?= ( $singlescreen == 1 ) ? 'checked="checked"' : ''; ?> type="radio" value="1" name="multiscreen" /> <?=  __('No') ?>
            </p>
            <div <?= (form_error("apptype") != '') ? 'class="error"' : '' ?> style="margin-left: 15px;">
                <label for="apptype"><?= _('App Type')?>:</label>
                <?php foreach($apptypes as $apptype):?>
                <input style="display: inline !important; width: auto !important; margin:5px 3px 8px 5px; height: 10px;" type="checkbox" name="apptype[]" id="apptype_<?=$apptype->id?>" value="<?=$apptype->id?>" onclick="changeSubFlavor(<?=$apptype->id?>);" /> <label style="display: inline-block !important; width: 130px !important;"><?= __($apptype->title)?></label>
                    <?php
                    if(count($apptype->subflavor)>0):
                    ?>
                    <div id="appSubFlavor_<?=$apptype->id?>" style="display:none;">
                    <?php
                        echo __('Sub Flavor').':';
                    ?>    
                       <select name="appsubflavor_<?=$apptype->id?>" style="width: 100px !important; display: inline !important;">
                           <?php foreach($apptype->subflavor as $subflavor): ?>
                               <option value="<?=$subflavor->id ?>"><?=ucfirst(__($subflavor->name))?></option>
                           <?php endforeach;?>
                       </select>
                    </div>    
                    <?php
                    endif;
                    ?>
                    <br />
                <?php endforeach;?>
            </div>            
            <div class="buttonbar">
                <input type="hidden" name="postback" value="postback" id="postback">
                    <a class="btn" href="<?= site_url('admin/formtemplate/forms')?>"><?= __('Cancel')?></a>
                    <button type="submit" class="btn primary"><?= __('Add Form') ?></button>
                    <br clear="all" />
            </div>
            <br clear="all" />
	</form>
    </div>
</div>
<script>
    function changeSubFlavor(flavorId){
        if(jQuery("#apptype_"+flavorId).is(':checked'))
            jQuery("#appSubFlavor_"+flavorId).css('display', 'inline');
        else
            jQuery("#appSubFlavor_"+flavorId).css('display', 'none');
    }
</script>