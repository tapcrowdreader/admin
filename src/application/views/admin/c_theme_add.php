<h1 style="text-align:center;"><?= $theme->name ?></h1>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<?php if($error != ''): ?>
<div class="error"><?= $error ?></div>
<?php endif ?>
<div class="frmsessions">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
		<p>
			<label><?= __('Name') ?>: </label>
			<input type="text" name="name" id="name" value="" />
		</p>
		<p>
			<label><?= __('Screenshot image') ?>: </label>
			<input type="file" name="screenshot" id="screenshot" value="" class="logoupload" /><br clear="all"/>
		</p>
		<p>
			<label><?= __('Order') ?>: </label>
			<input type="text" name="sortorder" id="sortorder" value="" />
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" id="submit" class="btn primary"><?=__('Save')?></button>
			<br clear="all" />
		</div>
	</form>
</div>
