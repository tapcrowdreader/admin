<div>
	<h1>Add Field</h1>
	<div class="frmformbuilder">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
   <?php endif ?>

   <p <?= (form_error("label") != '') ? 'class="error"' : '' ?>>
    <label for="label">Label:</label>
    <input type="text" name="label" id="label" value="<?=set_value('label')?>" />
  </p>

  <p <?= (form_error("formfieldtypeid") != '') ? 'class="error"' : '' ?>>
    <label for="formfieldtypeid">Type:</label>
    <select id="formfieldtypeid" name="formfieldtypeid" >
      <option value="select">Select...</option>
      <?php if(isset($fieldtypes)) { ?>
      <?php foreach($fieldtypes as $e) : ?>
      <option value="<?= $e->id;?>" <?= set_select('formfieldtypeid', $e->id) ?>><?= $e->name; ?></option>
    <?php endforeach; ?>
    <?php } ?>
  </select>
</p>
<div id="default_values">
  <p <?= (form_error("defaultvalue") != '') ? 'class="error"' : '' ?>>
    <label for="defaultvalue">Default Value:</label>
    <input type="text" name="defaultvalue" id="defaultvalue" value="<?= set_value('defaultvalue') ?>" />
  </p>
</div>

<div id="option_fields" style="display:none;">	
  <p <?= (form_error("possiblevalues") != '') ? 'class="error"' : '' ?>>
    <label for="possiblevalues">Possible Values, comma separated:</label>
    <input type="text" name="possiblevalues" id="possiblevalues" value="<?=set_value('possiblevalues')?>" />
  </p>
</div>

<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
  <label>Order:</label>
  <input type="text" name="order" value="<?= set_value('order',$formscreen->max_order) ?>" id="order">
</p>

            <!--<p <?= (form_error("xpos") != '') ? 'class="error"' : '' ?>>
				<label>Xpos:</label>
				<input type="text" name="xpos" value="<?= set_value('xpos') ?>" id="xpos">
			</p>
            
            <p <?= (form_error("ypos") != '') ? 'class="error"' : '' ?>>
				<label>Ypos:</label>
				<input type="text" name="ypos" value="<?= set_value('ypos') ?>" id="ypos">
			</p>
            
            <p <?= (form_error("width") != '') ? 'class="error"' : '' ?>>
				<label>Width:</label>
				<input type="text" name="width" value="<?= set_value('width') ?>" id="width">
			</p>
            
            <p <?= (form_error("height") != '') ? 'class="error"' : '' ?>>
				<label>Height:</label>
				<input type="text" name="height" value="<?= set_value('height') ?>" id="height">
			</p>-->

      <div id="required_fields" style="display:none;">
        <p  class="checklist" <?= (form_error("required") != '') ? 'class="error"' : '' ?>>
          <label>Required Field:</label>
          <input type="checkbox" name="required" value="yes" id="required">
        </p>
      </div>

      <div class="buttonbar">
        <input type="hidden" name="postback" value="postback" id="postback">
        <button type="submit" class="btn primary">Add Field</button>
        <br clear="all" />
      </div>
      <br clear="all" />
    </form>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
		// Sortables
		jQuery(document).ready(function() {
			// Sessiongroups sorteren
			jQuery( "#formfieldtypeid" ).change(function() {
        var value = $(this).attr('value');
        // Show option fields for radio, and select
        if(value == 6 || value == 3)
        {
        jQuery("#option_fields").show();                                
        }
        else
        {
        jQuery("#option_fields").hide();                                
        }

        // Hide default value for datepicker, time, checkbox, label, radio, and select
        if(value == 3 || value == 4 || value == 6 || value == 9 || value == 10 || value == 12)
        {
         jQuery("#default_values").hide();
        }
        else
        {
         jQuery("#default_values").show();
        }

        // Hide required field for label
        if(value == 10){
         jQuery("#required_fields").hide();                               
        }else {
        jQuery("#required_fields").show();
        }

        });
});                
</script>