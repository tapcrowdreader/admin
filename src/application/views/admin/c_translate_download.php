<h1><?= __('Download Translation : ') ?></h1>
<div class="frmsessions">
	<form action="<?= site_url('admin/translate/download/') ?>" method="POST" accept-charset="utf-8" class="data" enctype="multipart/form-data">
		<?php if($error != ''): ?>
		<div class="error"><?= $error ?></div>
		<?php endif ?>
		<p>
			<label><?= __('Source: ') ?></label>
			<select id="source" name="source">
            	<option value="android">Android</option>
                <option value="ios">IOS</option>
            </select>
		</p>

		<p <?= (form_error("lang") != '') ? 'class="error"' : '' ?>>
        	<label><?= __('Language: ') ?></label>
            <select id="language" name="language">
			<?php foreach ($languages as $lang) {?>
			<option value="<?= $lang->language?>"><?= $lang->language?></option>
			<?php }?>
            </select>
		</p>
        <p <?= (form_error("langfile") != '') ? 'class="error"' : '' ?>>
        	<label><?= __('File: ') ?></label>
            <input type="file" name="langfile" id="langfile" />
        </p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" class="btn primary"><?= __('Submit') ?></button>
			<br clear="all" />
		</div>
		<br clear="all" />
	</form>
</div>