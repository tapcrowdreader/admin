<div>
    <h1><?=__('Add Screen')?></h1>
    <div class="frmformbuilder">
        <form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
            <?php if($error != ''): ?><div class="error"><?= $error ?></div><?php endif ?>

            <p <?= (form_error("title") != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Title:') ?></label>
                <input type="text" name="title" id="title" value="<?=set_value('title')?>" />
            </p>

            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label><?= __('Order:') ?></label>
                <input type="text" name="order" value="<?= set_value('order') ?>" id="order">
            </p>


            <div class="buttonbar">
                <input type="hidden" name="postback" value="postback" id="postback">
                <a href="<?= site_url('admin/templatescreens/form/'.$form->id) ?>" class="btn"><?= __('Cancel') ?></a>
                <button type="submit" class="btn primary"><?= __('Add Form Screen') ?></button>
                <br clear="all" />
            </div>
            <br clear="all" />
        </form>
    </div>
</div>