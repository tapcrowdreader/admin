<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<?php if($this->session->flashdata('event_error') != ''): ?>
<div class="error fadeout"><?= $this->session->flashdata('event_error') ?></div>
<?php endif ?>
<div class="listview">
	<?php if ($this->uri->segment(1) == 'admin' && ($this->uri->segment(2) == 'users' || $this->uri->segment(2) == 'apps')): ?>
		<div class="groupchooser">
			<?php if (isset($channels) && _currentUser()->admin_channel == 0): ?>
				<select name="channel" id="channel" class="groupselect">
					<option value="0">All channels</option>
					<?php foreach ($channels as $channel): ?>
					<option value="<?= $channel->id ?>" <?= ($this->uri->segment(4) == $channel->id) ? "selected" : "" ?>><?= $channel->name ?></option>
					<?php endforeach ?>
				</select>
				<script type="text/javascript" charset="utf-8">
					$(document).ready(function() {
						$('#channel').change(function(){
							window.location = "<?= site_url('admin/'.$this->uri->segment(2).'/channel') ?>/" + $('#channel').val() + "/";
						});
					});
				</script>
			<?php endif ?>
		</div>
	<?php endif ?>
	<table id="listview" class="display">
		<thead>
			<tr>
				<?php $isadmin = _isAdmin(); ?>
				<?php foreach ($headers as $h => $field): ?>
					<?php if ($h == 'AppStore'): ?>
					<th class="data"><img src="img/ios.png" alt="Apple App Store" title="Apple App Store" /></th>
					<?php elseif ($h == 'AndroidMarket'): ?>
					<th class="data"><img src="img/droid.png" alt="Android Market" title="Android Market" /></th>
					<?php elseif ($h == 'MobileWebsite'): ?>
					<th class="data"><img src="img/mw.png" alt="Mobile Website" title="Mobile Website" /></th>
					<?php else: ?>
					<th class="data"><?= $h ?></th>
					<?php endif ?>
				<?php endforeach; ?>
				<?php if($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'users' && $isadmin): ?>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				<?php elseif($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'apps' && $isadmin): ?>
					<th>&nbsp;</th>
				<?php endif?>
			</tr>
		</thead>
		<tbody>
			<?php if ($data != FALSE): ?>
			<?php $isadmin = _isAdmin(); ?>
			<?php foreach($data as $row): ?>
					<tr>
						<?php foreach ($headers as $h => $field): ?>
						<?php if ($h == 'AppStore' || $h == 'AndroidMarket' || $h == 'MobileWebsite'): ?>
						<td><?= ($row->{$field} != '' ? '<img src="img/icons/tick.png" />' : '<img src="img/icons/cross.png" style="opacity:0.5;" />') ?></td>
						<?php else: ?>
						<td><?= $row->{$field} ?></td>
						<?php endif ?>
						<?php endforeach ?>
						<?php if($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'users' && $isadmin): ?>
						<td class="icon"><a href='<?= site_url("admin/users/edit/" . $row->id) ?>' ><i class="icon-pencil"></i></a></td>
						<td class="icon"><a href='<?= site_url("admin/apps/user/" . $row->id) ?>' ><img src="img/icons/application_cascade.png" width="16" height="16" alt="Apps from user" title="Apps from user" class="png" /></a></td>
						<td class="icon"><a href='<?= site_url("admin/users/loginas/" . $row->id) ?>' ><img src="img/icons/user_go.png" width="16" height="16" alt="Login as" title="Login as" class="png" /></a></td>
						<?php elseif($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'apps' && $isadmin): ?>
						<td class="icon"><a href='<?= site_url("admin/apps/edit/" . $row->id) ?>' ><i class="icon-pencil"></i></a></td>
						<?php endif?>
					</tr>
			<?php endforeach ?>
			<?php else: ?>

			<?php endif ?>
		</tbody>
	</table>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.adelete').click(function(e) {
			var delurl = $(this).attr('href');
			jConfirm('Are you sure you want to delete this row?<br />This cannot be undone!', 'Remove Entry', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					return false;
				}
			});
			return false;
		});
	});

</script>