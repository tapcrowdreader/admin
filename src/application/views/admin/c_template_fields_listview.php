<?php if($this->session->flashdata('event_feedback') != ''): ?>
    <div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<?php if($this->session->flashdata('event_error') != ''): ?>
    <div class="error fadeout"><?= $this->session->flashdata('event_error') ?></div>
<?php endif ?>
<br clear="all">
<div class="listview">
    <div class="buttonbar">
        <a class="add btn primary" href="<?= site_url('admin/templatefields/add/'.$formscreen->id)?>">
            <i class="icon-plus-sign icon-white"></i> Add Field
        </a>
    </div>
    <br clear="all">
    <table id="listview" class="display zebra-striped">
        <thead>
            <tr>
                <?php foreach ($headers as $h => $field): ?>
                <?php if ($h == 'Order'): ?>
                <th class="data order"><?= $h ?></th>
            <?php else: ?>
            <th class="data"><?= $h ?></th>
        <?php endif ?>
    <?php endforeach; ?>
    <th width="17"></th>
    <th width="17"></th>
    <th width="17"></th>
</tr>
</thead>
<tbody>
    <?php if ($data != FALSE): ?>
    <?php foreach($data as $row): ?>
    <tr>
        <?php foreach ($headers as $h => $field): ?>
        <?php if ($field != null && !empty($field)) : ?>
        <td><?= $row->{$field} ?></td>
    <?php endif; ?>
<?php endforeach ?>
<td class="icon">
    <a href="<?= site_url('admin/templatefields/edit/'.$row->id)?>">
        <i class="icon-pencil"></i>
    </a>
</td>
<td class="icon">
    <a href="<?= site_url('duplicate/index/'. $row->id .'/templatefields')?>">
        <img class="png" width="16" height="16" title="Duplicate" alt="Duplicate" src="img/icons/btn-duplicate.png">
    </a>
</td>
<td class="icon">
    <a class="adelete" href="<?= site_url('admin/templatefields/delete/'.$row->id)?>">
        <img class="png" width="16" height="16" alt="Delete" title="Delete" src="img/icons/delete.png">
    </a>
</td>
</tr>
<?php endforeach ?>
<?php endif ?>
</tbody>
</table>
</div>
<script type="text/javascript" charset="utf-8">
jQuery(document).ready(function() {
    jQuery('.adelete').click(function(e) {
        var delurl = jQuery(this).attr('href');
        jConfirm('<?= __('Are you sure you want to delete this row?<br />This cannot be undone!') ?>', '<?= __('Remove Entry') ?>', function(r) {
            if(r == true) {
                window.location = delurl;
                return true;
            } else {
                return false;
            }
        });
        return false;
    });
})
</script>