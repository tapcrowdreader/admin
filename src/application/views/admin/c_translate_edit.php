<?php

if(!isset($translation)) {
	echo __('There is no translation found for key %s.', $key);
	return;
}

$post_url = site_url('admin/translate/edit/' . $translation->source . '/' . $translation->id);

?>
<h3><?= __('Edit Translation for : ') ?> <small><?= htmlentities($translation->k) ?></small></h3>

<div class="frmsessions">
	<form action="<?=$post_url?>" method="POST" accept-charset="utf-8" class="data">
	<?php
// 		echo '<input type="hidden" name="key" value="'.$translation->id.'">';
// 		echo '<input type="hidden" name="source" value="'.$translation->source.'">';
// 		echo '<input type="hidden" name="key_default" value="'.htmlentities($translation->k).'">';
		foreach ($languages as $l) { ?>
			<p>
			<label><?= __('Value : ') ?>( <?= $l->language ?> )</label>
			<textarea name="translation_<?= $l->language ?>"><?= $translation->{$l->language} ?></textarea>
			</p><?php
		}
	?>

		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<a class="btn" href="<?=site_url('admin/translate?source='. $translation->source)?>"><?= __('Cancel') ?></a>
			<button type="submit" class="btn primary"><?= __('Save') ?></button>
			<br clear="all" />
		</div>
		<br clear="all" />
	</form>
</div>
