<?php
/**
 * Application Permissions View
 */

# Handle errors
if(isset($error)) { ?>
	<div class="alert alert-error"><b><?=__('Error:')?></b> <?=$error->getMessage()?></div><?php
	return;
}
?>
<div class="page-header">
	<h1>
		<img src="<?=$app->app_icon?>" style="height:30px;" />
		<?=$app->name?>
		<span class="label label-info"><?=$app->flavor->title?></span>
		<span class="label label-normal"><?=$app->channel->name?></span>
	</h1>
</div>

<div class="row span12">
	<div class="span5">
		<h4>Application Information</h4>
		<dl class="dl-horizontal">
			<dt><?=__('Identifier')?></dt><dd><?=$app->id?></dd>
			<dt><?=__('Account')?></dt><dd><?=$app->accountname?> #<?=$app->accountId?><br /><?=$app->email?></dd>

			<dt><?=__('Domain')?></dt><dd><?=$app->subdomain?></dd>
			<dt><?=__('CNAME')?></dt><dd><?=($app->cname)? $app->cname : '--'?></dd>
			<dt><?=__('Creation')?></dt><dd><?=$app->creation?></dd>
			<dt><?=__('Languages')?></dt><dd><?=$app->defaultlanguage?> (default)</dd>
		</dl>
	</div>
	<div class="span7">
		<h4>Application Permissions</h4>

		<form class="form-inline" method="POST" action="<?=site_url('admin/apps/permissions/'.$app->id)?>" name="add_entity">
			<input type="text" class="input-xlarge typeahead" placeholder="Account name"
				data-source="<?=$data_source?>" />
			<select class="input-xlarge hide" multiple="true"></select>
			<input type="hidden" name="account_id" value="" />
			<input type="hidden" name="entity_id" value="<?=$app->id?>" />
			<input type="hidden" name="entity" value="application" />
			<input type="hidden" name="action" value="add" />
			<button type="submit" class="btn btn-primary disabled" disabled="disabled">Add</button>
		</form>

		<hr />

		<form class="form-inline" method="POST" action="<?=site_url('admin/apps/permissions/'.$app->id)?>">
			<input type="hidden" name="entity_id" value="<?=$app->id?>" />
			<input type="hidden" name="entity" value="application" />
			<input type="hidden" name="action" value="remove" />
			<table class="table table-condensed">
			<tbody>
			<?php
			foreach($perms as $permission) { ?>
				<tr>
					<td><?=$permission->accountname?></td>
					<td><button type="submit" name="account_id"
						value="<?=$permission->accountId?>" class="close">&times;</button>
					</td>
				</tr><?php
			} ?>
			</tbody>
			</table>
		</form>
	</div>
</div>
'
