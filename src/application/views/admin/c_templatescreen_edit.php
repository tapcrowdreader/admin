<div>    
    <h1><?= __('Edit Screen') ?></h1>
    <div class="frmformbuilder">
        <form action="<?= site_url($this->uri->uri_string()) ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
           <?php if($error != ''): ?><div class="error"><?= $error ?></div><?php endif ?>	
           <p <?= (form_error("title") != '') ? 'class="error"' : '' ?>>
            <label for="name"><?= __('Title:') ?></label>
            <input type="text" name="title" id="title" value="<?= htmlspecialchars_decode(set_value('title', $formscreen->title), ENT_NOQUOTES) ?>" />
        </p>
        
        <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
            <label><?= __('Order:') ?></label>
            <input type="text" name="order" value="<?= set_value('order', $formscreen->order) ?>" id="order">
        </p>
        
        <div class="buttonbar">
            <input type="hidden" name="postback" value="postback" id="postback">
            <a href="<?= site_url('admin/templatescreens/form/'.$form->id) ?>" class="btn"><?= __('Cancel') ?></a>
            <button type="submit" class="btn primary"><?= __('Save') ?></button>
            <br clear="all" />
        </div>
        <br clear="all" />
    </form>
</div>
</div>