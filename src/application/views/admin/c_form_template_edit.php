<div>
    <h1><?=__('Edit Form Template')?></h1>
    <div class="frmformbuilder">
        <form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
            <?php if($error != ''): ?>
                <div class="error"><?= $error ?></div>
            <?php endif ?>			
            
            <p <?= (form_error("title") != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Title:') ?></label>
                <input type="text" name="title" id="title" value="<?= htmlspecialchars_decode(set_value('title', $form[0]->title ), ENT_NOQUOTES) ?>" />
            </p>
            
            <p <?= (form_error("submissionbuttonlabel") != '') ? 'class="error"' : '' ?>>
                <label for="submissionbuttonlabel"><?= __('Submit Button Label:') ?></label>
                <input type="text" name="submissionbuttonlabel" id="submissionbuttonlabel" value="<?= htmlspecialchars_decode(set_value('submissionbuttonlabel', $form[0]->submissionbuttonlabel ), ENT_NOQUOTES) ?>" />
            </p>	
            
            <p <?= (form_error("submissionconfirmationtext") != '') ? 'class="error"' : '' ?>>
                <label for="submissionconfirmationtext"><?= __('Confirmation Text:') ?></label>
                <textarea name="submissionconfirmationtext" id="submissionconfirmationtext"><?= htmlspecialchars_decode(set_value('submissionconfirmationtext', $form[0]->submissionconfirmationtext ), ENT_NOQUOTES) ?></textarea>
            </p>
            
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label><?=__('Order:')?></label>
		<input type="text" name="order" value="<?= set_value('order', $form[0]->order ) ?>" id="order">
            </p>
            
            <p <?= (form_error("emailsendresult") != '') ? 'class="error"' : '' ?> >
                <label for="emailsendresult"><?= __('Email Results:') ?></label>
		<select name="emailsendresult" id="emailsendresult">
                    <option value="0"><?= __('Select ...') ?></option>
                    <option value="yes" <?= ( $form[0]->emailsendresult == 'yes' ) ? 'selected="selected"' : '' ?>><?= __('Yes') ?></option>
                    <option value="no" <?= ( $form[0]->emailsendresult == 'no' ) ? 'selected="selected"' : '' ?>><?= __('No') ?></option>
		</select>
            </p>
            
            <p <?= (form_error("allowmutliplesubmissions") != '') ? 'class="error"' : '' ?> >
                <label for="allowmutliplesubmissions"><?= __('Allow Mutliple Submissions:') ?></label>
		<select name="allowmutliplesubmissions" id="allowmutliplesubmissions">
                    <option value="0"><?= __('Select ...') ?></option>
                    <option value="yes" <?= ( $form[0]->allowmutliplesubmissions == 'yes' ) ? 'selected="selected"' : '' ?>><?= __('Yes') ?></option>
                    <option value="no" <?= ( $form[0]->allowmutliplesubmissions == 'no' ) ? 'selected="selected"' : '' ?>><?= __('No') ?></option>
		</select>
            </p>
            		
            <p>
		<input style="display: inline !important;width: auto !important; margin:5px 3px 8px 5px;" <?= ($form[0]->getgeolocation == 1) ? 'checked="checked"' : '' ?> type="checkbox" value="1" name="geolocation" id="geolocation" /><label style="display: inline !important;width: auto !important;"><?= _("Get User Location")?></label>
            </p>
            
            <p>
				<label style="display: inline !important;"><?= __('Multi Screen Form')?>:</label> <input style="width: auto !important; display: inline !important;" <?= ( $singlescreen == 0 ) ? 'checked="checked"' : ''; ?> type="radio" value="0" name="multiscreen" /> <?=  __('Yes') ?> &nbsp;&nbsp;<input style="width: auto !important; display: inline !important;" <?= ( $singlescreen == 1 ) ? 'checked="checked"' : ''; ?> type="radio" value="1" name="multiscreen" /> <?=  __('No') ?>
            </p>
            
            <div <?= (form_error("apptype") != '') ? 'class="error"' : '' ?> style="margin-left: 15px;">
                <label for="apptype"><?= _('App Type')?>:</label>
                <?php foreach($apptypes as $apptype):
					$isChecked = ( $apptype->checked == 1 ) ? TRUE : FALSE;
				?>
                <input style="display: inline !important; width: auto !important; margin:5px 3px 8px 5px; height: 10px;" <?= ( $isChecked )? 'checked="checked" disabled="disabled"' : ''?> type="checkbox" name="apptype[]" id="apptype_<?=$apptype->id?>" value="<?=$apptype->id?>" onclick="changeSubFlavor(<?=$apptype->id?>);" /> <label style="display: inline-block !important; width: 130px !important;"><?= __($apptype->name)?></label>
                    <?php
                    if(count($apptype->subflavor)>0):
                    ?>
                    <div id="appSubFlavor_<?=$apptype->id?>" <?= ( $apptype->checked == 1 )? 'style="display:inline;"' : 'style="display:none;"'?>>
                    <?php
                        echo __('Sub Flavor').':';
                    ?>    
                       <select name="appsubflavor_<?=$apptype->id?>" style="width: 100px !important; display: inline !important;" <?= ( $isChecked )? 'disabled="disabled"' : ''?>>
                           <?php foreach($apptype->subflavor as $key => $value):
                                    $selectedIndex = $apptype->subflavor['selected'];
                                    if($key !== 'selected'):?>
                                        <option <?= ( $selectedIndex == $apptype->subflavor[$key]->id ) ? 'selected="selected"' : '' ?> value="<?=$apptype->subflavor[$key]->id ?>"><?=ucfirst(__($apptype->subflavor[$key]->name))?></option>
                           <?php 
                                    endif;
                                endforeach;?>
                       </select>
                    </div>    
                    <?php
                    endif;
                    ?>
                    <br />
                <?php endforeach;?>
            </div>                                
            <div class="buttonbar">
                <input type="hidden" name="postback" value="postback" id="postback">
                    <a class="btn" href="<?= site_url('admin/formtemplate/forms')?>"><?= __('Cancel')?></a>
                    <button type="submit" class="btn primary"><?= __('Save') ?></button>
                    <br clear="all" />
            </div>
            <br clear="all" />
	</form>
    </div>
</div>
<script>
    function changeSubFlavor(flavorId){
        if(jQuery("#apptype_"+flavorId).is(':checked'))
            jQuery("#appSubFlavor_"+flavorId).css('display', 'inline');
        else
            jQuery("#appSubFlavor_"+flavorId).css('display', 'none');
    }
</script>