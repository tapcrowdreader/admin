<h1>Edit application data</h1>
<?php if ($this->session->flashdata('edit_app') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('edit_app') ?></div>
<?php endif ?>
<div class="account">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" class="data">
		<?php if($error != ''): ?>
		<div class="error"><?= $error ?></div>
		<?php endif ?>
		<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
			<label>Name:</label>
			<input type="text" name="name" value="<?= set_value('name', $app->name) ?>" id="name">
		</p>
		<p <?= (form_error("subdomain") != '') ? 'class="error"' : '' ?>>
			<label>Subdomain:</label>
			<input type="text" name="subdomain" value="<?= set_value('subdomain', $app->subdomain) ?>" id="subdomain">
		</p>
		<p <?= (form_error("status") != '') ? 'class="error"' : '' ?>>
			<label>Submission Status (!):</label>
			<select name="status" id="status">
				<option value="inactive" <?= set_select("status", "inactive", ($app->status == 'inactive' ? TRUE : '')) ?>>inactive</option>
				<option value="pending" <?= set_select("status", "pending", ($app->status == 'pending' ? TRUE : '')) ?>>pending</option>
				<option value="submitted" <?= set_select("status", "submitted", ($app->status == 'submitted' ? TRUE : '')) ?>>submitted</option>
			</select>
		</p>
		<br /><br />
		<p <?= (form_error("appleappstore") != '') ? 'class="error"' : '' ?>>
			<label>Apple AppStore:</label>
			<input type="text" name="appleappstore" value="<?= set_value('appleappstore', $app->appleappstore, 'http://itunes.apple.com/') ?>" id="appleappstore">
		</p>
		<p <?= (form_error("androidmarket") != '') ? 'class="error"' : '' ?>>
			<label>Android Market:</label>
			<input type="text" name="androidmarket" value="<?= set_value('androidmarket', $app->androidmarket, 'market://') ?>" id="androidmarket">
		</p>
		<p <?= (form_error("mobwebsite") != '') ? 'class="error"' : '' ?>>
			<label>Mobile Website:</label>
			<input type="text" name="mobwebsite" value="<?= set_value('mobwebsite', $app->mobwebsite, 'http://*******.m.tapcrowd.com/') ?>" id="mobwebsite">
		</p>
		<p>
			<label>Channel</label>
			<input type="text" name="channel" value="<?= $app->channelname ?>" id="channel" readonly>
		</p>
		<p>
			<label>Username</label>
			<input type="text" name="userlogin" value="<?= $app->userlogin ?>" id="userlogin" readonly>
		</p>
		<p>
			<label>Useremail</label>
			<input type="text" name="useremail" value="<?= $app->useremail ?>" id="useremail" readonly>
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" class="save">Save</button>
		</div>
	</form>
</div>