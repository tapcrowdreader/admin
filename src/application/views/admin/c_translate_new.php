<h1><?= __('Add Translation : ') ?>
	<?= $keyData[0]->blabla ?></h1>
<div class="frmsessions">
	<form action="<?= site_url('admin/translate/add/') ?>" method="POST" accept-charset="utf-8" class="data">
		<?php if($error != ''): ?>
		<div class="error"><?= $error ?></div>
		<?php endif ?>
		<p>
			<label><?= __('Translation key: ') ?></label>
			<input type="text" name="translationKey" value="" id="translationKey">
		</p>

		<p <?= (form_error("lang") != '') ? 'class="error"' : '' ?>>
			<?php
			foreach ($languages as $lang) {
				//echo $keyDat[$lang];
				//var_dump($languages);
				//
				//var_dump($lang->language);
				?>
			<label><?= __('Value : ') ?>( <?= $lang->language ?> )</label>
			<input type="text" name="translation<?= $lang->language ?>" value="<?= $keyDat->{$lang->language} ?>" id="translation<?= $keyDat->{$lang->language} ?>">
			<input type="hidden" name="keyVal" value="<?= $keyData[0]->blabla ?>">
		<?php
			}
		 ?>
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" class="btn primary"><?= __('Save') ?></button>
			<br clear="all" />
		</div>
		<br clear="all" />
	</form>
</div>
