<h1><?= __('Add Question') ?></h1>
<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<p>
			<label><?= __('Question type')?></label>
			<select name="quizquestiontypeid" id="quizquestiontypeid">
				<?php foreach($questiontypes as $type) : ?>
				<option value="<?=$type->id?>"><?= $type->name ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		<p <?= (form_error("questiontext") != '') ? 'class="error"' : '' ?>>
			<label for="questiontext"><?= __('Question text:') ?></label>
			<input type="text" name="questiontext" id="questiontext" value="<?= set_value('questiontext') ?>" />
		</p>
		<p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
			<label for="imageurl"><?= __('Image:') ?></label>
			<input type="file" name="imageurl" id="imageurl" />
			<br clear="all" />
		</p>
		<p <?= (form_error("videourl") != '') ? 'class="error"' : '' ?>>
			<label for="videourl"><?= __('Video url:') ?></label>
			<input type="text" name="videourl" id="videourl" value="<?= set_value('videourl') ?>" />
		</p>
		<div class="row">
			<label for="tags">Tags:</label>
			<ul id="mytags" name="mytagsul"></ul>
		</div>
		<div id="questionoptionsdiv" style="display:none;">
		<p>
			<label for="quizquestionoption"><?= __('Multiple choice answers:') ?></label>
			<div id="questionoptions" style="margin-top:-10px;">
			<?php foreach($questionoptions as $option) : ?>
			<span id="<?=$option->id?>">
			<a class="btn btn-mini btn-danger removeoption" id="remove_<?=$option->id?>" href="<?=site_url('quiz/removequestionoption/'.$option->id)?>" onclick="return removeoption(this)"><i class="icon-remove icon-white" title="Delete"></i></a> <span class="" id="option<?=$option->id?>"><?=$option->optiontext?></span><br  style="margin-bottom:20px;"/></span>
			<?php endforeach; ?>
			</div>
			<input type="text" name="addquestionoption" id="addquestionoption" value="" style="width:200px;float:left;margin-right:5px;" /> <a href="<?= site_url('quiz/addquestionoptiontoempty/'.$quiz->id)?>" id="addquestionoptionButton" class="btn-primary btn"><?= __('Add Option')?></a>
		</p>
		<p style="margin-top:-25px;">
			<label for="correctanswer_quizquestionoptionid"><?= __('Correct multiple choice answer:') ?></label>
			<select name="correctanswer_quizquestionoptionid" id="correctanswer_quizquestionoptionid">
				<?php foreach($questionoptions as $option) : ?>
				<option name="correct_<?=$option->id?>" id="correct_<?=$option->id?>" value="<?=$option->id?>"><?=$option->optiontext?></option>
				<?php endforeach; ?>
			</select>
		</p>
		</div>
		<p id="correctanswerp" <?= (form_error("correctanswer") != '') ? 'class="error"' : '' ?>>
			<label for="correctanswer"><?= __('Correct answer:') ?></label>
			<input type="text" name="correctanswer" id="correctanswer" value="<?= set_value('correctanswer') ?>" />
		</p>
		<p <?= (form_error("correctanswer_score") != '') ? 'class="error"' : '' ?>>
			<label for="correctanswer_score"><?= __('Score for correct answer:') ?></label>
			<input type="text" name="correctanswer_score" id="correctanswer_score" value="<?= set_value('correctanswer_score') ?>" />
		</p>
		<p>
			<br style="margin-top:50px;" />
			<label><?= __('Result text + image or video to show after question was answered') ?></label>
		</p>
		<p <?= (form_error("explanationtext") != '') ? 'class="error"' : '' ?>>
			<label for="explanationtext"><?= __('Explanation text:') ?></label>
			<input type="text" name="explanationtext" id="explanationtext" value="<?= set_value('explanationtext') ?>" />
		</p>
		<p <?= (form_error("explanationimage") != '') ? 'class="error"' : '' ?>>
			<label for="explanationimage"><?= __('Explanation image:') ?></label>
			<input type="file" name="explanationimage" id="explanationimage" />
			<br clear="all" />
		</p>
		<p <?= (form_error("explanationvideo") != '') ? 'class="error"' : '' ?>>
			<label for="explanationvideo"><?= __('Video url:') ?></label>
			<input type="text" name="explanationvideo" id="explanationvideo" value="<?= set_value('explanationvideo') ?>" />
		</p>

	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<button type="submit" class="btn primary"><?= __('Add Question') ?></button>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if($postedtags){foreach($postedtags as $val){ echo '"'.$val.'", '; }} ?>],
		});

		$("#addquestionoptionButton").click(function(e) {
			e.preventDefault();
			var url = $(this).attr('href');
			$.ajax({
				type: "POST",
				url: url,
				data: { optiontext: $("#addquestionoption").val() }
			}).done(function( data ) {
				if(data != 'false' && data != false) {
					$("#questionoptions").append('<span id="'+data+'"><a class="btn btn-mini btn-danger removeoption" id="remove_'+data+'" href="'+"<?=site_url('quiz/removequestionoption')?>"+'/'+data+'"  onclick="return removeoption(this)"><i class="icon-remove icon-white" title="Delete"></i></a> <span class="" id="option'+data+'">'+$("#addquestionoption").val()+'</span><br style="margin-bottom:20px;" /></span>');
					$("#correctanswer_quizquestionoptionid").append('<option name="'+data+'" value="'+data+'" id="correct_'+data+'">'+$("#addquestionoption").val()+'</option>');
					$("#addquestionoption").val('');
				}
			});
		});

		$("#quizquestiontypeid").change(function(e) {
			if($(this).val() == 2) {
				$("#questionoptionsdiv").css('display', 'block');
				$("#correctanswerp").css('display', 'none');
			} else {
				$("#questionoptionsdiv").css('display', 'none');
				$("#correctanswerp").css('display', 'block');
			}
		});
	});

	function removeoption(current_object) {
		var url = current_object.href;
		var id = current_object.id;
		$.ajax({
			type: "GET",
			url: url,
			data: { }
		}).done(function( data ) {
			if(data != 'false' && data != false) {
				$("#"+data).remove();
				$("#correct_"+data).remove();
			}
		});

		return false;
	};
</script>