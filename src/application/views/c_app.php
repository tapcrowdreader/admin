<style>
.editTagA {
    float: left;
    width: 480px;
}

</style>
<script>
  $(document).ready(function() {
//	$( "#sortable_venues" ).sortable({
//	   stop: function(event, ui) {
//		   var result = $("#sortable_venues").sortable('serialize');
//		   var ids = [];
//		   $(".venue_order").each(function(index) {
//			   ids.push(this.id);
//		   });
//			$.post("venues/sort", { ids: ids } );
//		}
//	});
//
//	$( "#sortable_events" ).sortable({
//	   stop: function(event, ui) {
//		   var result = $("#sortable_events").sortable('serialize');
//		   var ids = [];
//		   $(".event_order").each(function(index) {
//			   ids.push(this.id);
//		   });
//			$.post("events/sort", { ids: ids } );
//		}
//	});

	$('.changeTag').change(function() {
		var id = $(this).val();
		var text = $(this).find("option:selected").text();
		if(id != 'add' && id.substring(0, 3) != 'new') {
			$.post("venue/addtag", { venueid: id, tag: text }, function(data) {
				location.reload();
			} );
		}

		if(id.substring(0, 3) == 'new') {
			$('#divTagsForm').css('display','block');
			$('#venueidForm').val(id.substring(3,id.length));
		}
	});

	$('.changeTagEvent').change(function() {
		var id = $(this).val();
		var text = $(this).find("option:selected").text();
		if(id != 'add' && id.substring(0, 3) != 'new') {
			$.post("event/addtag", { eventid: id, tag: text }, function(data) {
				location.reload();
			} );
		}

		if(id.substring(0, 3) == 'new') {
			$('#divTagsFormEvent').css('display','block');
			$('#eventidForm').val(id.substring(3,id.length));
		}
	});

	$('.changeTagCitycontent').change(function() {
		var id = $(this).val();
		var text = $(this).find("option:selected").text();
		if(id != 'add' && id.substring(0, 3) != 'new') {
			$.post("citycontent/addtag", { citycontentid: id, tag: text }, function(data) {
				location.reload();
			} );
		}

		if(id.substring(0, 3) == 'new') {
			$('#divTagsFormCitycontent').css('display','block');
			$('#citycontentidForm').val(id.substring(3,id.length));
		}
	});

	$('#cancelButton').click(function(event) {
		event.preventDefault();
		$('#divTagsForm').css('display','none');
	});
  });

</script>
<div id="divTagsForm" class="frmsessions frmAddTag">
	<form action="<?= site_url('venue/addtag') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
		<h3 style="color:#FFFFFF"><?= __('Add new menu item') ?></h3>
		<p style="color: #FFFFFF">
			<label style="color: #FFFFFF"><?= __('Name:') ?></label>
			<input type="text" name="tag" value="" width="100px" />
		</p>
		<div class="buttonbar">
		<input type="hidden" id="venueidForm" name="venueid"/>
		<a href="<?= site_url('apps/view/'.$app->id)?>" id="cancelButton" class="btn"><?= __('Cancel') ?></a>
		<button type="submit" class="btn primary" name="saveTag"><?= __('Save menu item') ?></button>
		</div>
	</form>
</div>
<div id="divTagsFormEvent" class="frmsessions frmAddTag">
	<form action="<?= site_url('event/addtag') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
		<h3 style="color:#FFFFFF"><?= __('Add new menu item') ?></h3>
		<p style="color: #FFFFFF">
			<label style="color: #FFFFFF"><?= __('Name:') ?></label>
			<input type="text" name="tag" value="" width="100px" />
		</p>
		<div class="buttonbar">
		<input type="hidden" id="eventidForm" name="eventid"/>
		<a href="<?= site_url('apps/view/'.$app->id)?>" id="cancelButton" class="btn"><?= __('Cancel') ?></a>
		<button type="submit" class="btn primary" name="saveTag"><?= __('Save menu item') ?></button>
		</div>
	</form>
</div>
<div id="divTagsFormCitycontent" class="frmsessions frmAddTag">
	<form action="<?= site_url('citycontent/addtag') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
		<h3 style="color:#FFFFFF"><?= __('Add new menu item') ?></h3>
		<p style="color: #FFFFFF">
			<label style="color: #FFFFFF"><?= __('Name:') ?></label>
			<input type="text" name="tag" value="" width="100px" />
		</p>
		<div class="buttonbar">
		<input type="hidden" id="citycontentidForm" name="citycontentid"/>
		<a href="<?= site_url('apps/view/'.$app->id)?>" id="cancelButton" class="btn"><?= __('Cancel') ?></a>
		<button type="submit" class="btn primary" name="saveTag"><?= __('Save menu item') ?></button>
		</div>
	</form>
</div>
<div>
<?php $CI =& get_instance(); ?>
	<h1><?= $app->name ?> <?=__('App')?></h1>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<div class="eventinfo">
		<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
		<span class="info_title"><b><?= __('Name:') ?></b></span> <?= $app->name ?><br />
		<?php if(isset($flavor) && $flavor != false) : ?>
        <span class="info_title"><b><?= __('Flavor:') ?></b></span> <?= $flavor->name ?><br />
        <?php endif; ?>
        <br/>
        <div class="button_bar">
	        <?php if (!$this->session->userdata('mijnevent')): ?>
	        <a href="<?= site_url('apps/delete/'.$app->id) ?>" class="delete btn btn-danger" id="deletebtn"><i class="icon-remove icon-white"></i>  <span><?= __('Remove app') ?></span></a>
			<?php if($app->familyid == 3) : ?>
			<a href="<?= site_url('events') ?>" class="btn" id="deletebtn"><span><?= __('Events') ?></span></a>
			<a href="<?= site_url('venues') ?>" class="btn" id="deletebtn"><span><?= __('Venues') ?></span></a>
			<?php endif; ?>
	        <!--<a href="<?= site_url('submit/app/'.$app->id) ?>" class="build btn"><img src="img/icons/email_go.png" width="16" height="16" alt="TapCrowd App"> <span> Submit</span></a>-->
	        <!--<a href="<?= site_url('apps/edit/'.$app->id) ?>" class="edit btn"><img src="img/icons/cog.png" width="16" height="16" alt="TapCrowd App"> <span> Settings</span></a>
	        <a href="<?= site_url('apps/manage/'.$app->id) ?>" class="edit btn"><img src="img/icons/box.png" width="16" height="16" alt="TapCrowd App"> <span> Content</span></a>-->
	        <?php endif ?>
	        <br clear="all"/>
		</div>
	</div>
	<br>
	<?php if($app->apptypeid == 6) : ?>
	<div class="modules">
		<?php if($artistsActive) : ?>
			<div class="module active">
				<?php if ($this->session->userdata('mijnevent')): ?><a href="<?= site_url('artists/app/'.$app->id) ?>"><?php endif ?>
					<a href="<?= site_url('artists/app/'.$app->id) ?>" class="editlink"><span><?=__('Artists')?></span></a>
				<?php if ($this->session->userdata('mijnevent')): ?></a><?php endif ?>
				<a href="<?= site_url('module/edit/8/app/'.$app->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" height="22px" /></a>
				<a href="<?= site_url('apps/module/deactivate/'.$app->id.'/18') ?>" class="activate delete">&nbsp;</a>
			</div>
		<?php else: ?>
			<div class="module inactive">
				<span><?= __('Artists') ?></span>
				<a href="<?= site_url('apps/module/activate/'.$app->id.'/18') ?>" class="activate add white">&nbsp;</a>
			</div>
		<?php endif; ?>
		<?php if($socialActive): ?>
			<div class="module active">
				<?php if ($this->session->userdata('mijnevent')): ?><a href="<?= site_url('social/app/'.$app->id) ?>"><?php endif ?>
					<a href="<?= site_url('social/app/'.$app->id) ?>" class="editlink"><span><?= __('Social Media') ?></span></a>
				<?php if ($this->session->userdata('mijnevent')): ?></a><?php endif ?>
				<a href="<?= site_url('module/edit/28/app/'.$app->id)?>" style="border-bottom: none;"><img src="img/Settings.png" alt="<?=__('Module Settings')?>" height="22px" /></a>
				<a href="<?= site_url('apps/module/deactivate/'.$app->id.'/28') ?>" class="activate delete">&nbsp;</a>
			</div>
		<?php else:?>
			<div class="module inactive">
				<span><?= __('Social Media') ?></span>
				<a href="<?= site_url('apps/module/activate/'.$app->id.'/28') ?>" class="activate add white">&nbsp;</a>
			</div>
		<?php endif; ?>
	</div>
	<?php endif; ?>
	<?php //content ?>
	<?php if($app->familyid == 3) : ?>
	<h2><?=__('Menu items')?></h2>
<!-- 	<a href="<?= site_url('citycontent/add') ?>" class="add btn primary"style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i>  Add Content
	</a> -->
	<a href="<?= site_url('module/addlauncher/app/'.$app->id) ?>" class="add btn primary"style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i> <?= __('Add Module') ?>
	</a>
	<a href="<?= site_url('event/add') ?>" class="add btn primary"style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i> <?= __('Add Event') ?>
	</a>
	<a href="<?= site_url('venue/add') ?>" class="add btn primary" style="margin-left:10px;margin-bottom:10px;">
		<i class="icon-plus-sign icon-white"></i> <?= __('Add POI') ?>
	</a>

    <a href="forms/add/<?=$app->id?>/app" class="btn primary" style="float:right; margin-right:5px;"><?= __('Add Form Module') ?></a>

	<?php //CITY flavor ?>
		<div class="events eventlistapp">
		<?php $infoshowed = false; ?>
		<?php foreach($subflavors as $subflavor): ?>
		<h3 class="packageH2" style="width:auto;"><?=ucfirst($subflavor->name)?></h3>
			<?php foreach($launchers as $launcher) : ?>
			<?php if($launcher->moduletypeid == 21 && $infoshowed == false) : ?>
			<div class="event_wrapper">
				<a href="<?= site_url('apps/info/'.$app->id) ?>" class="event editTagA">
					<?php if($launcher->icon != 'l_info' && $launcher->icon != '') : ?>
					<div class="eventlogo"><img src="<?=image_thumb($launcher->icon, 50, 50) ?>" width="50" height="50" /></div>
					<?php else : ?>
					<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50" /></div>
					<?php endif; ?>
					<h1><span><?=$launcher->title?></span></h1>
				</a>
				<a href="<?= site_url('module/edit/21/app/'.$app->id)?>" style="border-bottom: none;"><img class="editTagImg" src="img/Settings.png" alt="<?=__('Edit')?>" height="22px" /></a>
				<?php if ($launcher->active): ?>
					<a href="<?= site_url('module/deactivatelauncherbyid/'.$launcher->id.'/app/' . $app->id) ?>" class="onoff deactivate"><span><?= __('Deactivate venue') ?></span></a>
				<?php else: ?>
					<a href="<?= site_url('module/activatelauncherById/'.$launcher->id.'/app/' . $app->id) ?>" class="onoff activate"><span><?= __('Activate venue') ?></span></a>
				<?php endif ?>
			</div>
			<?php $infoshowed = true; ?>
			<?php else: ?>
				<?php if(strtolower($launcher->package) == strtolower($subflavor->name)) : ?>
				<div class="event_wrapper">
                	<?php if ($launcher->moduletypeid == 44): ?>
					  <?php if ($launcher->active): ?>
                              <a href="<?= site_url('/forms/app/'.$app->id.'/'.$launcher->id) ?>" class="event editTagA">
                                  <div class="eventlogo">
									  <?php if($launcher->icon != '') : ?>
                                      <img src="<?=image_thumb($launcher->icon, 50, 50) ?>" width="50" height="50">
                                      <?php endif; ?>
                                  </div>
                                  <h1><span><?= $launcher->title ?></span></h1></a>

                                  <!--<a href="<?= site_url('/forms/app/'.$app->id.'/'.$launcher->id) ?>" class="editimage"><img src="img/icons/pencil.png" width="16" height="16" alt="Content" title="Content" class="png" /></a>-->
                              <a href="<?= site_url('forms/editlauncher/'.$launcher->id.'/app/'.$app->id)?>"  style="border-bottom: none;"><img class="editTagImg" src="img/Settings.png" alt="<?=__('Edit')?>" height="22px" /></a>
                              <a href="<?= site_url('forms/module/deactivate/'.$app->id.'/'.$launcher->id.'/app') ?>" class="onoff deactivate"><span><?= __('Deactivate Form') ?></span></a>

                      <?php elseif($launcher->title == "Form"): ?>
                                  <a href="<?= site_url('forms/add/'.$app->id.'/app') ?>" class="event editTagA">
                                  <div class="eventlogo">
									  <?php if($launcher->icon != '') : ?>
                                      <img src="<?=image_thumb($launcher->icon, 50, 50) ?>" width="50" height="50">
                                      <?php endif; ?>
                                  </div>
                                  <h1><span><?= $launcher->title ?></span></h1>
                                  </a>
                                  <a href="<?= site_url('forms/add/'.$app->id.'/app') ?>" id="activate" class="onoff activate"><span><?= __('Activate Form') ?></span></a>

                      <?php else: ?>
                                  <a href="#" class="event editTagA">
                                  <div class="eventlogo">
									  <?php if($launcher->icon != '') : ?>
                                      <img src="<?=image_thumb($launcher->icon, 50, 50) ?>" width="50" height="50">
                                      <?php endif; ?>
                                  </div>
                                  <h1><span><?= $launcher->title ?></span></h1>
                                  </a>
                                  <a href="<?= site_url('forms/module/activate/'.$app->id.'/'.$launcher->id.'/app') ?>" id="activate" class="onoff activate"><span><?= __('Activate Form') ?></span></a>

                      <?php endif ?>

                    <?php else : ?>

					  <?php if($launcher->moduletypeid == 30) : ?>
					<a href="<?= site_url('events/launcher/'.$launcher->id) ?>" class="event editTagA">
					<?php elseif($launcher->moduletypeid == 31) : ?>
					<a href="<?= site_url('venues/launcher/'.$launcher->id) ?>" class="event editTagA">
					<?php elseif($launcher->moduletypeid == 0) : ?>
					<a href="<?= site_url('dynamiclauncher/edit/'.$launcher->id.'/app/'.$app->id);?>" class="event editTagA">
					<?php else: ?>
					<?php if($launcher->module == 'ads') {
						$launcher->module = 'ad';
					} elseif(strtolower($launcher->module) == 'news feeds') {
						$launcher->module = 'news';
					} ?>
					<a href="<?= site_url($launcher->module.'/app/'.$app->id) ?>" class="event editTagA">
					<?php endif; ?>
						<div class="eventlogo">
							<?php if($launcher->icon != '') : ?>
							<img src="<?=image_thumb($launcher->icon, 50, 50) ?>" width="50" height="50">
							<?php endif; ?>
						</div>
						<h1><span><?=$launcher->title?></span></h1>
					</a>
					<?php if($launcher->moduletypeid != 28) : ?>
					<a href="<?= site_url('module/editLauncher/'.$launcher->id.'')?>" style="border-bottom: none;"><img class="editTagImg" src="img/Settings.png" alt="<?=__('Edit')?>" height="22px" /></a>
					<?php endif; ?>
					<?php if ($launcher->active): ?>
						<a href="<?= site_url('module/deactivatelauncherbyid/'.$launcher->id.'/app/' . $app->id) ?>" class="onoff deactivate"><span><?= __('Deactivate venue') ?></span></a>
					<?php else: ?>
						<a href="<?= site_url('module/activatelauncherById/'.$launcher->id.'/app/' . $app->id) ?>" class="onoff activate"><span><?= __('Activate venue') ?></span></a>
					<?php endif ?>

                    <?php endif ?>

				</div>
				<?php endif; ?>
			<?php endif; ?>
			<?php endforeach; ?>
			<?php
			    foreach($templateform as $frmtemplate):
			        if($subflavor->name == $frmtemplate->package) : ?>
			            <div class="event_wrapper">
			                <a href="javascript:void(0);" class="event editTagA" onClick="return getEmailId('<?= site_url('formtemplate/add/'.$app->id.'/'.$frmtemplate->id) ?>', 'app');"><div class="eventlogo"></div><h1><span><?= $frmtemplate->title?></span></h1></a>
			                <a href="javascript:void(0);" onClick="return getEmailId('<?= site_url('formtemplate/add/'.$app->id.'/'.$frmtemplate->id) ?>', 'app');" id="activate" class="onoff activate"><span>Activate Form</span></a>
			            </div>
			        <?php
			            endif;
			        ?>
			    <?php
			    endforeach;
			 ?>
		<?php endforeach; ?>
		</div>

		<?php if($events != false && count($events) > 0) : ?>
		<?php
		$countedEvents = 0;
		foreach ($events as $event) {
			if(count($event->tagNames) == 0) {
				$countedEvents++;
			}
		}
		?>
		<?php if($countedEvents > 0 && $app->apptypeid != 1) : ?>
		<br/><br/>
		<h3><?=($events != FALSE && count($events) != 0) ? __('Events not added to a menu item') : __('Events') ?></h3>
		<br/>
		<div class="events">
			<div id="sortable_events">
				<?php if ($events != FALSE): ?>
					<?php foreach ($events as $event): ?>
					<?php if(count($event->tagNames) == 0): ?>
						<?php $countedEvents++;?>
						<div class="event_wrapper event_order" id="<?=$event->id ?>">
							<a href="<?= site_url('event/view/'.$event->id) ?>" class="event">
								<?php if ($event->eventlogo != ''): ?>
									<div class="eventlogo" style="background:transparent url('<?= image_thumb($event->eventlogo, 50, 50) ?>') no-repeat center center">&nbsp;</div>
								<?php else: ?>
									<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
								<?php endif ?>
								<h1><span><?= $event->name ?></span></h1>
							</a>
							<br/>
							<?php foreach($event->tagNames as $tag) : ?>
								<span class="tagInEventList"><?=$tag ?></span>
							<?php endforeach; ?>
								<select class="changeTagEvent">
									<option value="add"><?=__('Add Event to menu item')?></option>
									<?php foreach($launchers as $l) : ?>
										<?php if($l->moduletypeid == 30) : ?>
										<option value="<?=$event->id?>"><?=$l->title?></option>
										<?php endif; ?>
									<?php endforeach; ?>
									<?php if(_isAdmin()):?>
									<option value="new<?=$event->id?>"><?=__('New...')?></option>
									<?php endif; ?>
								</select>
							<a href="<?= site_url('event/edit/'.$event->id) ?>" class="edit btn">
								<i class="icon-pencil"></i>  <?=__('Edit')?>
							</a>
						</div>
					<?php endif; ?>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>
		<?php endif; ?>
		<?php endif; ?>
		<?php if($venues != false && count($venues) > 0) : ?>
		<?php
		$countedVenues = 0;
		foreach ($venues as $venue) {
			if(count($venue->tagNames) == 0) {
				$countedVenues++;
			}
		}
		?>
		<?php if($countedVenues > 0 && $app->apptypeid != 1) : ?>
		<br /><br />
		<h3><?=($venues != FALSE && count($venues) != 0) ? __('POI\'s not added to a menu item') : __('POI\'s') ?></h3>
		<br/>
		<div class="events">
			<div id="sortable_venues">
				<?php if ($venues != FALSE): ?>
					<?php foreach ($venues as $venue): ?>
					<?php if(count($venue->tagNames) == 0): ?>
						<div class="event_wrapper venue_order" id="<?=$venue->id ?>">
							<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="event">
								<?php if ($venue->image1 != ''): ?>
									<div class="eventlogo" style="background:transparent url('<?= image_thumb($venue->image1, 50, 50) ?>') no-repeat center center">&nbsp;</div>
								<?php else: ?>
									<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
								<?php endif ?>
								<h1><span><?= $venue->name ?></span></h1>
								<?= $venue->address ?>
							</a>
							<br/>
							<?php
							foreach($venue->tagNames as $tag) {
								echo '<span class="tagInVenueList">'.$tag.'</span>';
							} ?>
								<select class="changeTag">
									<option value="add"><?=__('Add POI to menu item')?></option>
									<?php foreach($launchers as $l) : ?>
										<?php if($l->moduletypeid == 31) : ?>
										<option value="<?=$venue->id?>"><?=$l->title?></option>
										<?php endif; ?>
									<?php endforeach; ?>
									<?php if(_isAdmin()):?>
									<option value="new<?=$venue->id?>"><?=__('New...')?></option>
									<?php endif; ?>
								</select>
							<a href="<?= site_url('venue/edit/'.$venue->id) ?>" class="edit btn">
								<i class="icon-pencil"></i> <?= __('Edit') ?>
							</a>
						</div>
					<?php endif; ?>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>
		<?php endif; ?>
		<?php endif; ?>
		<?php if($citycontent != false && count($citycontent) > 0) : ?>
		<?php
		$countedCitycontent = 0;
		foreach ($citycontent as $cc) {
			if(count($cc->tagNames) == 0) {
				$countedCitycontent++;
			}
		}
		?>
		<?php if($countedCitycontent > 0) : ?>
	<!-- 		<br /><br />
			<h3><?=($citycontent != FALSE && count($citycontent) != 0) ? "Content not added to a menu item" : "Extra Content" ?></h3>
			<br/>
			<div class="events">
				<div id="sortable_content">

					<?php if ($citycontent != FALSE): ?>
						<?php foreach ($citycontent as $cc): ?>
						<?php if(count($cc->tagNames) == 0): ?>
							<div class="event_wrapper venue_order" id="<?=$cc->id ?>">
								<a href="<?= site_url('citycontent/edit/'.$cc->id) ?>" class="event">
									<?php if ($cc->image != ''): ?>
										<div class="eventlogo" style="background:transparent url('<?= image_thumb($cc->image, 50, 50) ?>') no-repeat center center">&nbsp;</div>
									<?php else: ?>
										<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
									<?php endif ?>
									<h1><span><?= $cc->title ?></span></h1>
								</a>
								<br/>
								<?php foreach($cc->tagNames as $tag) : ?>
									<span class="tagInVenueList"><?=$tag ?></span>
								<?php endforeach; ?>
									<select class="changeTagCitycontent">
										<option value="add">Add Content to menu item</option>
										<?php foreach($apptagsCitycontent as $tag) : ?>
											<option value="<?=$cc->id?>"><?=$tag->tag?></option>
										<?php endforeach; ?>
										<option value="new<?=$cc->id?>">New...</option>
									</select>
								<a href="<?= site_url('citycontent/edit/'.$cc->id) ?>" class="edit btn">
									<i class="icon-pencil"></i>  Edit
								</a>
								<?php if (isset($venue) && $venue != false && $venue->active == 1): ?>
									<a href="<?= site_url('citycontent/active/' . $cc->id . '/off') ?>" class="onoff deactivate"><span>Deactivate Content</span></a>
								<?php else: ?>
									<a href="<?= site_url('citycontent/active/' . $cc->id . '/on') ?>" class="onoff activate"><span>Activate Content</span></a>
								<?php endif ?>
							</div>
						<?php endif; ?>
						<?php endforeach ?>
					<?php endif ?>
				</div>
			</div>  -->
		<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>



	<?php if($app->familyid == 2) : ?>
		<?php if($app->apptypeid != 1) : ?>
		<br /><br />
		<h3><?=__('Venues in')?> "<?= $app->name ?>"</h3>
		<?php if(isset($_GET['newapp'])) : ?>
			<img src="img/layout2/progress3.png" />
		<?php endif; ?>
		<br>
		<?php endif; ?>
		<div class="events">
			<div id="sortable_venues">
				<?php if(($app->familyid != 2) || empty($venues) || _isAdmin()) : ?>
					<?php if(($app->apptypeid == 1 || $app->apptypeid == 7) && $venues != false && count($venues) > 0) : ?>

					<?php else: ?>
					<a href="<?= site_url('venue/add') ?>" class="add btn primary">
						<i class="icon-plus-sign icon-white"></i> <?= __('Add Venue') ?>
					</a>
					<br><br>
					<?php endif; ?>
				<?php endif; ?>
				<?php if ($venues != FALSE): ?>
					<?php foreach ($venues as $venue): ?>
						<div class="event_wrapper venue_order" id="<?=$venue->id ?>">
							<a href="<?= site_url('venue/view/'.$venue->id) ?>" class="event">
								<?php if ($venue->image1 != ''): ?>
									<div class="eventlogo" style="background:transparent url('<?= image_thumb($venue->image1, 50, 50) ?>') no-repeat center center">&nbsp;</div>
								<?php else: ?>
									<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
								<?php endif ?>
								<h1><span><?= $venue->name ?></span></h1>
								<?= $venue->address ?>
							</a>
							<br />
							<?php foreach($venue->tags as $tag) : ?>
								<span class="tagInVenueList"><?=$tag->tag ?></span>
							<?php endforeach; ?>
							<a href="<?= site_url('venue/edit/'.$venue->id) ?>" class="edit btn">
								<i class="icon-pencil"></i>  <?=__('Edit')?>
							</a>
							<?php if ($venue->active == 1): ?>
								<a href="<?= site_url('venue/active/' . $venue->id . '/off') ?>" class="onoff deactivate"><span><?= __('Deactivate venue') ?></span></a>
							<?php else: ?>
								<a href="<?= site_url('venue/active/' . $venue->id . '/on') ?>" class="onoff activate"><span><?= __('Activate venue') ?></span></a>
							<?php endif ?>
						</div>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if($app->apptypeid == 6 || $app->familyid == 1 || $app->apptypeid == 1) : ?>
			<?php if($app->apptypeid != 1) : ?>
				<h3><?=__('My events in mobile app')?> "<?= $app->name ?>"</h3>
			<?php else: ?>
				<br />
				<h3 style="float:left;"><?=__('Events in')?> "<?= $app->name ?>"</h3>
			<?php endif; ?>
			<div class="events">
				<div id="sortable_events">
					<?php if(($app->familyid == 1) && $events != false && count($events) > 0) : ?>

					<?php else: ?>
					<a href="<?= site_url('event/add') ?>" class="add btn primary">
						<i class="icon-plus-sign icon-white"></i> <?= __('Add New Event') ?>
					</a>
					<?php endif; ?>
					<br><br>
					<?php if ($events != FALSE): ?>
						<?php foreach ($events as $event): ?>
							<div class="event_wrapper event_order" id="<?=$event->id?>">
								<a href="<?= site_url('event/view/'.$event->id) ?>" class="event">
									<?php if ($event->eventlogo != ''): ?>
										<div class="eventlogo" style="background:transparent url('<?= image_thumb($event->eventlogo, 50, 50) ?>') no-repeat center center">&nbsp;</div>
									<?php else: ?>
										<div class="eventlogo"><img src="img/event-logo-50.png" width="50" height="50"></div>
									<?php endif ?>
									<h1><span><?= $event->name ?></span></h1>
									<?php if(date('d-m-Y', strtotime($event->datefrom)) == '01-01-1970') : ?>
										<?=__('Permanent event')?>
									<?php else: ?>
										<?= date('d-m-Y', strtotime($event->datefrom)) ?> - <?= date('d-m-Y', strtotime($event->dateto)) ?>
									<?php endif; ?>
								</a>
								<br/>
								<a href="<?= site_url('event/edit/'.$event->id) ?>" class="edit btn">
									<i class="icon-pencil"></i>  <?=__('Edit')?>
								</a>
								<a href="<?= site_url('event/remove/'.$event->id) ?>" class="delete btn btn-danger" style="margin-top: -27px;margin-right:5px;float:right;">
									<i class="icon-remove icon-white"></i></i>  <?=__('Delete')?>
								</a>
								<?php if ($event->active == 1): ?>
									<a href="<?= site_url('event/active/' . $event->id . '/off') ?>" class="onoff deactivate"><span><?= __('Deactivate event') ?></span></a>
								<?php else: ?>
									<a href="<?= site_url('event/active/' . $event->id . '/on') ?>" class="onoff activate"><span><?= __('Activate event') ?></span></a>
								<?php endif ?>
							</div>
						<?php endforeach ?>
					<?php endif ?>
				</div>
			</div>
	<?php endif; ?>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#deletebtn').click(function() {
				jConfirm('<?= __("This will <b>remove</b> this app!<br />! This cannot be undone!") ?>', '<?= __("Remove App") ?>', function(r) {
					if(r == true) {
						window.location = '<?= site_url("apps/delete/".$app->id)?>';
						return true;
					} else {
						jAlert('<?= __("App not removed!") ?>', '<?= __("Info") ?>');
						return false;
					}
				});
				return false;
			});
		});
	</script>
    <script>
        function getEmailId(jumpUrl, type)
        {
            var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;

            jPrompt('Email', '', 'Prompt Dialog', function(r)
            {
                if(r != null){
                    if(!r){jAlert('Please enter valid email.', 'Error'); return false;}

                    if(regMail.test(r) == true){
                        window.location = jumpUrl + '/' + r + '/' + type;
                    }
                    else
                        jAlert('Invalid email.', 'Error');
                    }
            });

            return false;
        }
    </script>
</div>

