<div>
	<h1><?= __('Edit %s', $sponsor->name)?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
            <div class="error"><p><?= __('Something went wrong with the image upload.')?> <br/> <?= __('Maybe the image size exceeds the maximum width or height')?></p></div>
            <?php endif ?>
			<p <?= (form_error("sponsorgroup") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Sponsorgroup')?></label>
				<select name="sponsorgroup" id="sponsorgroup">
					<option value=""><?= __('Select ...')?></option>
					<?php foreach ($sponsorgroups as $group): ?>
						<option value="<?= $group->id ?>" <?= set_select('sponsorgroup', $group->id, ($sponsor->sponsorgroupid == $group->id ? TRUE : '')) ?>><?= $group->name ?></option>
					<?php endforeach ?>
				</select>
				<?php if($this->uri->segment(4) == 'event') : ?>
				<a href="<?= site_url('sponsorgroups/add/'.$sponsor->eventid.'/'.$this->uri->segment(4) . '/') ?>" style="position:absolute; margin:-30px 0 0 210px;"><?= __('Add sponsorgroup')?> &rsaquo;&rsaquo;</a>
				<?php elseif($this->uri->segment(4) == 'venue') : ?>
				<a href="<?= site_url('sponsorgroups/add/'.$sponsor->venueid.'/'.$this->uri->segment(4) . '/') ?>" style="position:absolute; margin:-30px 0 0 210px;"><?= __('Add sponsorgroup')?> &rsaquo;&rsaquo;</a>
				<?php endif; ?>

			</p>
            <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
               <label for="description"><?= __('Name') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('sponsor', $sponsor->id, 'name', $language->key); ?>
                <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?= htmlspecialchars_decode(set_value('name_'.$language->key, ($trans != null) ? $trans : $sponsor->name), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="description"><?= __('Description') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('sponsor', $sponsor->id, 'description', $language->key); ?>
                <textarea name="description_<?php echo $language->key; ?>" id="text"><?= htmlspecialchars_decode(set_value('description_'.$language->key, ($trans != null) ? $trans : $sponsor->description), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endforeach; ?>
            <?php else : ?>
            <p <?= (form_error("w_name") != '') ? 'class="error"' : '' ?> >
                <label for="name"><?= __('Name:')?></label>
                <input type="text" name="name_<?php echo $app->defaultlanguage; ?>" id="name" value="<?= htmlspecialchars_decode(set_value('name_'.$app->defaultlanguage, $sponsor->description), ENT_NOQUOTES) ?>"/>
            </p>
            <p <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>
                <label for="description"><?= __('Description:')?></label>
                <textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description"><?= htmlspecialchars_decode(set_value('description_'. $app->defaultlanguage, $sponsor->description), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endif; ?>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Photo:')?></label>
				<span class="evtlogo" <?php if($sponsor->image != ''){ ?>style="background:transparent url('<?= image_thumb($sponsor->image, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="image" id="image" value="" class="logoupload" />
				<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
				<br clear="all"/>
				<?php 
				$type = 'event';
				if(isset($venue)) {
					$type = 'venue';
				}
				?>
				<?php if($sponsor->image != '' && file_exists($this->config->item('imagespath') . $sponsor->image)){ ?><span><a href="<?= site_url('sponsors/removeimage/'.$sponsor->id.'/'.$type) ?>" class="deletemap"><?= __('Remove')?></a></span><br clear="all" /><?php } ?>
			</p>
			<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Website:')?></label>
				<input type="text" name="website" value="<?= htmlspecialchars_decode(set_value('website', $sponsor->website), ENT_NOQUOTES) ?>" id="website">
			</p>
<!-- 			<p <?= (form_error("premium") != '') ? 'class="error"' : '' ?> class="premiump">
				<input type="checkbox" name="premium" class="checkboxClass premiumCheckbox" <?= ($premium) ? 'checked="checked"' : '' ?> id="premium"> <?= __('Premium?')?>
			</p> -->
			<div class="row">
				<label for="tags">Tags:</label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order', $sponsor->order) ?>"><br/>
<!-- 				<label><?= __('Extra line: ')?></label>
				<input type="text" name="extraline" value="<?= set_value('extraline', (isset($premium->extraline)) ? $premium->extraline : '') ?>" id="order"> -->
			</p>
			<?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<?php if(_checkMultilang($m, $lang->key, $app)): ?>
						<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
							<?= _getInputField($m, $app, $lang, $languages, 'sponsor', $sponsor->id); ?>
						</p>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= (isset($venue)) ? site_url('sponsors/venue/'.$venue->id) : site_url('sponsors/event/'.$event->id) ?>" class="btn"><?= __('Cancel')?></a>
				<button type="submit" class="btn primary"><?= __('Save')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#mytags").tagit({
		initialTags: [<?php if(isset($postedtags) && $postedtags != false && !empty($postedtags)) { 
			foreach($postedtags as $val){ 
				echo '"'.$val.'", ';
			} 
		} elseif (isset($tags) && $tags != false) {
			foreach($tags as $val){ 
				echo "\"".$val->tag."\"," ;
			}
		} ?>]
	});

	var tags = new Array();
	var i = 0;
	<?php foreach($apptags as $tag) : ?>
	tags[i] = "<?=$tag->tag?>";
	i++;
	<?php endforeach; ?>
	$("#mytags input.tagit-input").autocomplete({
		source: tags
	});
});
</script>