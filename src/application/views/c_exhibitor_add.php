<script type="text/javascript">
$(function(){
	$("#jqueryselectbrands").multiselect({
	   click: function(event, ui){
	   	if(ui.checked) {
	   		$("#brandshidden").val($("#brandshidden").val()+'/'+ui.value);
	   	} else {
	   		var val = $("#brandshidden").val();
	   		var valarray = val.split('/');
	   		var newstring = '';
	   		for(var index in valarray) {
	   			var obj = valarray[index];
	   			if(obj != '' && obj != ui.value) {
	   				newstring = newstring + '/' + obj;
	   			}
	   		}
	   		$("#brandshidden").val(newstring);
	   	}
	   },
		selectedList: 4
	});

	$("#jqueryselectcats").multiselect({
	   click: function(event, ui){
	   	if(ui.checked) {
	   		$("#catshidden").val($("#catshidden").val()+'/'+ui.value);
	   	} else {
	   		var val = $("#catshidden").val();
	   		var valarray = val.split('/');
	   		var newstring = '';
	   		for(var index in valarray) {
	   			var obj = valarray[index];
	   			if(obj != '' && obj != ui.value) {
	   				newstring = newstring + '/' + obj;
	   			}
	   		}
	   		$("#catshidden").val(newstring);
	   	}
	   },
		selectedList: 4
	});
});
</script>
<div>
	<h1><?= __('Add Item') ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Name') ?> <?= '(' . $language->name . ')'; ?>:</label>
                    <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?=set_value('name_'.$language->key)?>" />
                </p>
                <?php endforeach; ?>
                <?php else : ?>
                <p <?= (form_error("name_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Name:') ?></label>
                    <input type="text" name="name_<?php echo $app->defaultlanguage; ?>" id="name" value=<?=set_value('name_'.$app->defaultlanguage)?>"" />
                </p>
            <?php endif; ?>
			<?php if (!isset($venue)): ?>
			<p <?= (form_error("booth") != '') ? 'class="error"' : '' ?>>
				<label for="booth"><?= __('Booth:') ?></label>
				<input type="text" name="booth" value="<?= set_value('booth') ?>" id="booth">
			</p>
			<?php endif ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="description"><?= __('Description') ?> <?= '(' . $language->name . ')'; ?>:</label>
                    <textarea name="description_<?php echo $language->key; ?>" id="description" ><?= set_value('description_'.$language->key) ?></textarea>
                </p>
                <?php endforeach; ?>
            <?php else : ?>
                <p <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="description"><?= __('Description:') ?></label>
                    <textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description" ><?= set_value('description_'.$app->defaultlanguage) ?></textarea>
                </p>
            <?php endif; ?>
			<p <?= (form_error("address") != '') ? 'class="error"' : '' ?>>
				<label for="address"><?= __('Address:') ?></label>
				<input type="text" name="address" value="<?= set_value('address') ?>" id="address">
			</p>
			<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
				<label for="email"><?= __('Email:') ?></label>
				<input type="text" name="email" value="<?= set_value('email') ?>" id="email">
			</p>
			<?php if (!isset($venue)): ?>
			<p <?= (form_error("tel") != '') ? 'class="error"' : '' ?>>
				<label for="tel"><?= __('Tel.:') ?></label>
				<input type="text" name="tel" value="<?= set_value('tel') ?>" id="tel">
			</p>
			<p <?= (form_error("web") != '') ? 'class="error"' : '' ?>>
				<label for="web"><?= __('Web:') ?></label>
				<input type="text" name="web" value="<?= set_value('web') ?>" id="web">
			</p>
			<?php endif ?>
			<p <?= (form_error("imageurl1") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl1"><?= (!isset($venue)) ? __('Logo') : __('Image')?>:</label>
				<input type="file" name="imageurl1" id="imageurl1" value="" /><br />
				<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
				<br clear="all" />
			</p>
			<?php if(!isset($groupid) || $groupid == '' && $maincat == false) : ?>
				<p>
					<label for="selbrands"><?= __('Brands') ?></label>
					<select name="selbrands[]" id="selbrands" multiple size="15" class="multiple">
						<?php if(isset($brands) && !empty($brands) && $brands != false) : ?>
							<?php foreach ($brands as $brand): ?>
								<option value="<?= $brand->id ?>" <?= set_select('selbrands[]', $brand->id) ?>><?= $brand->name ?></option>
							<?php endforeach ?>
						<?php endif; ?>
					</select>
				</p>
				<p>
					<label for="selcategories"><?=__('Categories')?></label>
					<select name="selcategories[]" id="selcategories" multiple size="15" class="multiple">
						<?php if(isset($brands) && !empty($brands) && $brands != false) : ?>
							<?php foreach ($categories as $category): ?>
								<option value="<?= $category->id ?>" <?= set_select('selcategories[]', $category->id) ?>><?= $category->name ?></option>
							<?php endforeach ?>
						<?php endif; ?>
					</select>
				</p>
				
				<?php if (!isset($venue)): ?>
				<p><label></label><?= __('Add one or more brands/categories first <a href="%s">here</a>', site_url('brands/event/'.$event->id)) ?></p>
				<?php else: ?>
				<p><label></label><?= __('Add one or more brands/categories first <a href="%s">here</a>', site_url('brands/venue/'.$venue->id)) ?></p>
				<?php endif ?>
<!-- 				<?php if (!isset($venue)): ?>
					<?php for($i=1; $i < 21; $i++){ ?>
						<p <?= (form_error("imageurl".$i) != '') ? 'class="error"' : '' ?>>
							<label for="imageurl<?= $i ?>">Image <?= $i ?>:</label>
							<input type="file" name="imageurl<?= $i ?>" id="imageurl<?= $i ?>" value="" />
						</p>
					<?php } ?>
				<?php endif ?> -->
			<?php else: ?>
				<?php //we're adding an item to a group so let user choose groups ?>
<!-- 				<p>
					<label><?=__('Add to Brand')?></label>
					<select title="<?=__('Basic example')?>" class="jqueryselect" multiple="multiple" name="brands" size="5" id="jqueryselectbrands">
						<?php foreach($brandshtml as $option) : ?>
							<?= $option ?>
						<?php endforeach; ?>
					</select>
				</p> -->
				<p>
					<label><?=__('Add to Category')?></label>
					<select title="<?=__('Basic example')?>" class="jqueryselect" multiple="multiple" name="cats" size="5" id="jqueryselectcats">
						<?php foreach($catshtml as $option) : ?>
							<?= $option ?>
						<?php endforeach; ?>
					</select>
				</p>
			<?php endif; ?>

 			<?php if($confbagactive) : ?>
			<p <?= (form_error("confbag") != '') ? 'class="error"' : '' ?>>
				<label for="confbag"><?=__('Conference bag content:')?></label>
				<input type="file" name="confbagcontent" id="confbagcontent" />
				<br clear="all" />
			</p>
			<?php endif; ?>
			<div class="row">
				<label for="tags">Tags:</label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order') ?>" id="order"><br/>
			</p>
			<?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<?php if(_checkMultilang($m, $lang->key, $app)): ?>
						<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
							<?= _getInputField($m, $app, $lang, $languages); ?>
						</p>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
			<p <?= (form_error("premium") != '') ? 'class="error"' : '' ?> class="premiump">
				<input type="checkbox" name="premium" class="checkboxClass premiumCheckbox" id="premium"> <?=__('Premium?')?>
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?> id="extralinep">
				<label><?=__('Premium order:')?></label>
				<input type="text" name="premiumorder" value="<?= set_value('premiumorder') ?>" id="premiumorder">
				<label><?=__('Extra line:')?></label>
				<input type="text" name="extraline" value="<?= set_value('extraline') ?>" id="extraline">
				<label><?=__('Title:')?></label>
				<input type="text" name="premiumtitle" value="<?= set_value('premiumtitle', $premiumtitle) ?>" id="premiumtitle">
			</p>
			<div class="buttonbar">
				<input type="hidden" name="brandshidden" value="" id="brandshidden">
				<input type="hidden" name="catshidden" value="" id="catshidden">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?=__('Save')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if($postedtags){foreach($postedtags as $val){ echo '"'.$val.'", '; }} ?>],
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = '<?=$tag->tag?>';
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
	});
</script>