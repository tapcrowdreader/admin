<h1>FAQ</h1>
<div id="cname">
	<h3><?= __('CNAME')?></h3>
	<p>
		<?= __('You can configure your own URL for your mobile website. Enter the URL that you would like to use, e.g. "m.yourdomain.com" Configure following record in the nameservers of your domain: m.yourdomain.com CNAME m.tapcrowd.com') ?>
	</p>
</div>
<div id="launchericons">
	<h3><?= __('Launchericons') ?></h3>
	<p>
		<?= __('Apple has some restrictions for icons used as launcher buttons. We advise to avoid icons with rounded corners and glossy effects. Apple has the right to refuse your app in the App Store if your icons show too many similarities with Apple\'s icons.') ?>
	</p>
</div>
