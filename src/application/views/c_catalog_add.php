<script type="text/javascript">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if($postedtags){foreach($postedtags as $val){ echo '"'.$val.'", '; }} ?>],
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = '<?=$tag->tag?>';
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
	});
</script>
<script type="text/javascript">
$(function(){
	$("#jqueryselectcats").multiselect({
	   click: function(event, ui){
	   	if(ui.checked) {
	   		$("#catshidden").val($("#catshidden").val()+'/'+ui.value);
	   	} else {
	   		var val = $("#catshidden").val();
	   		var valarray = val.split('/');
	   		var newstring = '';
	   		for(var index in valarray) {
	   			var obj = valarray[index];
	   			if(obj != '' && obj != ui.value) {
	   				newstring = newstring + '/' + obj;
	   			}
	   		}
	   		$("#catshidden").val(newstring);
	   	}
	   },
		selectedList: 4
	});
});
</script>
<div>
<?php if($this->uri->segment(5) == 'services') : ?>
	<h1><?= __('Add Service') ?></h1>
<?php elseif($this->uri->segment(5) == 'careers') : ?>
	<h1><?= __('Add Career') ?></h1>
<?php elseif($this->uri->segment(5) == 'projects') : ?>
	<h1><?= __('Add Project') ?></h1>
<?php else: ?>
	<h1><?= __('Add item') ?></h1>
<?php endif; ?>
	
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<?php if($this->uri->segment(5) == false) : ?>
			<?php if(!isset($newgroupid) || $newgroupid == '' && $maincat == false) : ?>
				<p <?= (form_error("cataloggroup") != '') ? 'class="error"' : '' ?>>
					<label><?=__('Catalog Group')?></label>
					<select name="cataloggroup" id="cataloggroup">
						<option value="">Select ...</option>
						<?php foreach ($cataloggroups as $cataloggroup): ?>
							<option value="<?= $cataloggroup->id ?>" <?= set_select('cataloggroup', $cataloggroup->id) ?>><?= $cataloggroup->name ?></option>
						<?php endforeach ?>
					</select>
					<a href="<?= site_url('cataloggroups/add/'.$this->uri->segment(3).'/'.$this->uri->segment(4) . '/') ?>" style="position:absolute; margin:-22px 0 0 350px;"><?= __('Add cataloggroup') ?> &rsaquo;&rsaquo;</a>
				</p>
			<?php endif; ?>
			<?php endif; ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Name') ?> <?= '(' . $language->name . ')'; ?>:</label>
                    <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?= set_value('name_'.$language->key) ?>" />
                </p>
                <?php endforeach; ?>
                <?php else : ?>
                <p <?= (form_error("name_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Name:') ?></label>
                    <input type="text" name="name_<?php echo $app->defaultlanguage; ?>" id="name" value="<?=set_value('name_'.$app->defaultlanguge)?>" />
                </p>
            <?php endif; ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="description"><?= __('Description') ?><?= '(' . $language->name . ')'; ?>:</label>
                    <textarea name="description_<?php echo $language->key; ?>" id="description"><?= set_value('description_'.$language->key) ?></textarea>
                </p>
                <?php endforeach; ?>
            <?php else : ?>
                <p <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="description"><?= __('Description:') ?></label>
                    <textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description"><?= set_value('description_'.$app->defaultlanguage) ?></textarea>
                </p>
            <?php endif; ?>
            
			<?php if ($this->basket_model->active($venue->id) && $basegroup != false && $catalogmodule == 'menu'): ?>
			<div class="errmsg" style="display: none">
			</div>
			<p <?= (form_error("price") != '') ? 'class="error"' : '' ?>>
				<label for="price"><?=__('Price:')?></label>
				<input type="text" name="price" value="" id="price">
			</p>
			<?php endif ?>
			<?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
	            <p <?= (form_error("urltitle_".$language->key) != '') ? 'class="error"' : '' ?>>
	                <label for="name"><?=__('Title of url')?> <?= '(' . $language->name . ')'; ?>:</label>
					<input type="text" name="urltitle_<?=$language->key?>" id="urltitle_<?=$language->key?>" value="<?= set_value('urltitle_'.$language->key) ?>" />
	            </p>
			 	<?php endforeach; ?>
			<?php endif; ?>
			<?php foreach($languages as $language) : ?>
				<p <?= (form_error("url_".$language->key) != '') ? 'class="error"' : '' ?>>
					<label><?= __('Link (Full url):') ?> <?= '(' . $language->name . ')'; ?></label>
					<input type="text" name="url_<?=$language->key?>" value="<?= set_value('url_'.$language->key) ?>" id="url_<?=$language->key?>">
				</p>
			<?php endforeach; ?>
			<?php foreach($languages as $language) : ?>
			<p <?= (form_error("pdfdoc".$language->key) != '') ? 'class="error"' : '' ?>>
				<label for="pdfdoc<?=$language->key?>"><?= __('PDF') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<input type="file" name="pdfdoc<?=$language->key?>" id="pdfdoc<?=$language->key?>" value="<?= set_value('pdfdoc'.$language->key) ?>" /><br />
				<span class="note" style="text-align: left;width: 200px;"><?= __('Max size %s mb',2) ?></span>
				<br clear="all" />
			</p>
			<?php endforeach; ?>
            
            <p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl"><?= (!isset($venue)) ? __('Logo') : __('Image') ?>:</label>
				<input type="file" name="imageurl" id="imageurl" value="<?= set_value('imageurl') ?>" /><br />
				<span class="note" style="text-align: left;width: 200px;"><?= __('Max size %s px by %s px',2000,2000) ?></span>
				<br clear="all" />
			</p>            
			<?php if($app->id != 393) : ?>
			<div class="row">
				<label for="tags">Tags:</label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<?php else: ?>
				<p>
					<label for="tagdelbar"><?= __('Brand:') ?></label><br/>
					<select name="tagdelbar">
						<option value="Audi"><?=__('Audi')?></option>
						<option value="Volkswagen"><?=__('Volkswagen')?></option>
						<option value="skoda"><?=__('Skoda')?></option>
						<option value="volkswagenbedrijfsvoertuigen"><?=__('Volkswagen Bedrijfsvoertuigen')?></option>
					</select>
				</p>
			<?php endif; ?>

			<?php if(!isset($newgroupid) || $newgroupid == '' && $maincat == false) : ?>
				<p<?= (form_error("selbrands") != '') ? 'class="error"' : '' ?>>
					<label for="selbrands"><?= __('Brands') ?></label>
					<select name="selbrands[]" id="selbrands" multiple size="15" class="multiple">
						<?php if(isset($brands) && $brands != false) : ?>
						<?php foreach ($brands as $brand): ?>
							<option value="<?= $brand->id ?>" <?= set_select('selbrands[]', $brand->id) ?>><?= $brand->name ?></option>
						<?php endforeach ?>
						<?php endif; ?>
					</select>
				</p>
				<p <?= (form_error("selcategories") != '') ? 'class="error"' : '' ?>>
					<label for="selcategories"><?= __('Categories') ?></label>
					<select name="selcategories[]" id="selcategories" multiple size="15" class="multiple">
						<?php if(isset($categories) && $categories != false) : ?>
						<?php foreach ($categories as $category): ?>
							<option value="<?= $category->id ?>" <?= set_select('selcategories[]', $category->id) ?>><?= $category->name ?></option>
						<?php endforeach ?>
						<?php endif; ?>
					</select>
				</p>
				<?php if (!isset($venue)): ?>
				<p><label></label><?= __('Add one or more brands/categories first <a href="%s">here</a>', site_url('catalogbrands/event/'.$event->id)) ?></p>
				<?php else: ?>
				<p><label></label><?= __('Add one or more brands/categories first <a href="%s">here</a>', site_url('catalogbrands/venue/'.$venue->id)) ?></p>
				<?php endif ?>
			<?php else: ?>
				<?php //we're adding an item to a group so let user choose groups ?>
				<?php if($newgroupid != '') : ?>

				<?php else : ?>
					<?php if(($app->apptypeid == 4 || $app->apptypeid == 11) && isset($catalogtype) && $catalogtype != '') : ?>
					<?php else: ?>
						<p>
							<label><?= __('Add to Category') ?></label>
							<select title="<?=__('Basic example')?>" class="jqueryselect" multiple="multiple" name="cats" size="5" id="jqueryselectcats">
								<?php foreach($catshtml as $option) : ?>
									<?= $option ?>
								<?php endforeach; ?>
							</select>
						</p>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
			<?php if (!isset($venue)): ?>
				<?php for($i=1; $i < 21; $i++){ ?>
					<p <?= (form_error("imageurl".$i) != '') ? 'class="error"' : '' ?>>
						<label for="imageurl<?= $i ?>"><?= __('Image') ?> <?= $i ?>:</label>
						<input type="file" name="imageurl<?= $i ?>" id="imageurl<?= $i ?>" value="" />
					</p>
				<?php } ?>
			<?php endif ?>
 			<?php if($confbagactive) : ?>
			<p <?= (form_error("confbag") != '') ? 'class="error"' : '' ?>>
				<label for="confbag"><?=__('Conference bag content:')?></label>
				<input type="file" name="confbagcontent" id="confbagcontent" />
				<br clear="all" />
			</p>
			<?php endif; ?>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order') ?>" id="order"><br/>
			</p>
			<?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<?php if(_checkMultilang($m, $lang->key, $app)): ?>
						<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
							<?= _getInputField($m, $app, $lang, $languages); ?>
						</p>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
<!-- 			<p <?= (form_error("premium") != '') ? 'class="error"' : '' ?> class="premiump">
				<input type="checkbox" name="premium" class="checkboxClass premiumCheckbox" id="premium"> <?=__('Premium?')?>
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?> id="extralinep">
				<label><?=__('Premium order:')?></label>
				<input type="text" name="premiumorder" value="<?= set_value('premiumorder') ?>" id="premiumorder">
				<label><?=__('Extra line:')?></label>
				<input type="text" name="extraline" value="<?= set_value('extraline') ?>" id="extraline">
			</p> -->
			<?php if($app->show_external_ids_in_cms == 1) : ?>
			<p  <?= (form_error("externalid") != '') ? 'class="error"' : '' ?>>
				<label><?=__('External id:')?></label>
				<input type="text" name="externalid" value="<?= set_value('externalid') ?>" id="externalid"><br/>
			</p>
			<?php endif; ?>
			<div class="buttonbar">
				<?php if(($app->apptypeid == 8 || $app->apptypeid == 7) && $newgroupid != '') : ?>
				<input type="hidden" name="catshidden" value="<?=$newgroupid?>" id="catshidden">
				<?php else: ?>
				<input type="hidden" name="catshidden" value="" id="catshidden">
				<?php endif; ?>
				
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?=__('Save')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>

<script>
(function(){

$(document).ready(
	function()
	{
		$(".frmsessions input#price").change(
			function()
			{
				var val = $(".frmsessions input#price").val();
				
				if(isNaN(val))
				{
					$(".frmsessions div.errmsg").text("(Please introduce a valid number!)");
					$(".frmsessions div.errmsg").css("color","red");
					$(".frmsessions div.errmsg").show('normal').delay(2500).hide(500);
					
					$(".frmsessions input#price").val("");
				}
				
			}
		);
	}
);

})();
</script>