<style>
	.frmsessions form p input {
	width:590px;	
	}
</style>
<?php if ($step == 1): ?>
	<h1 style="text-align:center;"><?= __('Choose your app flavor') ?></h1>
	<!-- <img src="img/layout2/progress1.png" style="width:600px;" /> -->
	<div class="progress progress-striped active">
		<div class="bar" style="width: 25%;"></div>
	</div>
	<ul class="flavors">
		<?php foreach($flavors as $flavor) : ?>
			<li>
				<a onclick="document.<?=$flavor->formname?>.submit();" class="add_app_a">
				<p>
					<b class="title"><?= __($flavor->title) ?></b>
					<span class="description"><?= __($flavor->description) ?></span>
				</p>
				<img src="<?=$flavor->image_phone?>" width="150" height="272" />
				<form id="<?=$flavor->formname?>" name="<?=$flavor->formname?>" action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8">
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="hidden" name="flavorid" value="<?=$flavor->id?>" id="flavorid">
				</form>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
<?php elseif ($step == 2): ?>
	<h1><?= __('Basic set-up') ?></h1>
	<div class="progress progress-striped active">
		<div class="bar" style="width: 50%;"></div>
	</div>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
			<?php if($error != ''): ?>
			<div class="error" style="padding: 0px 10px 2px 35px;"><?= $error ?></div>
			<?php endif ?>
			<fieldset>
			<?php if(isset($flavor)) : ?>
			<p>
				<label for="flavor"><?=__('Flavor:')?></label>
				<input type="text" name="flavor" id="flavor" value="<?=($flavor->title == null || $flavor->title == '') ? set_value('flavor') : $flavor->title?>" disabled readonly style="background-color:#EEE;"/>
			</p>
			<?php endif; ?>
			<?php if($this->channel->id == 7 || $this->channel->id == 1) : ?>
<!-- 			<?php if(isset($subflavors) && $subflavors != null) : ?>
			<p>
				<label for="subflavor">Package:</label>
				<select name="subflavor" id="flavor">
					<?php foreach ($subflavors as $subflavor): ?>
						<option value="<?= $subflavor->id ?>"><?= $subflavor->name ?></option>
					<?php endforeach ?>
				</select>
			</p>
			<?php endif; ?> -->
			<?php endif; ?>
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label for="name"><?=__("What's the name of your app?")?></label>
				<input type="text" name="name" id="name" value="<?= htmlspecialchars_decode(set_value('name'), ENT_NOQUOTES) ?>" />
			</p>
			<p <?= (form_error("urlname") != '') ? 'class="error"' : '' ?>>
				<label for="urlname" style="margin-bottom:5px;"><?= __('What should your mobile website url be?') ?></label>
				<label><span style="width:auto;">http://</span><input type="text" name="urlname" id="urlname" <?= (form_error("urlname") != '') ? 'class="error"' : '' ?> value="<?= set_value('urlname') ?>" style="width:100px;"><span style="width:auto;">.m.tap.cr</span></label><br clear="all" />
			</p>
<!-- 			<p <?= (form_error("cname") != '') ? 'class="error"' : '' ?>>
				<label for="cname"><?= __('Do you have your own url for the mobile site? (optional, click <a href="faq#cname" target="_blank">here</a> for more info):') ?></label>
				<input type="text" name="cname" id="cname" <?= (form_error("cname") != '') ? 'class="error"' : '' ?> value="<?= set_value('cname', 'm.mydomain.com') ?>">
			</p> -->
			<p <?= (form_error("language") != '') ? 'class="error"' : '' ?>>
				<label for="language"><?= __('Languages') ?></label>
                <?php foreach($languages as $language) : ?>

                <?php if(isset($sellanguages) && in_array($language->key, $sellanguages)) {
                	$checked = true;
                } else {
                	$checked = false;
                } ?>
                    <input class="checkboxClass" type="checkbox" style="float:left; display:inline; width:15px; margin-right:5px;" name="language[]" id="lang_<?=$language->key?>" value="<?php echo $language->key; ?>" <?= ($checked) ? 'checked=checked' : ''?> /> <label style="width:200px;float:left;" for="lang_<?=$language->key?>"><?php echo $language->name; ?></label> <br clear="all" />
				<?php endforeach; ?>
			</p>
			<p> 
				<label><?= __('Default language:') ?></label>
				<select name="defaultlang" id="defaultlang">
					<?php if(isset($sellanguages)):?>
						<?php foreach($sellanguages as $sl) : ?>
						<?php if(isset($seldefaultlang) && $sl == $seldefaultlang) : ?>
							<option value="<?=$seldefaultlang?>" selected="selected"><?=$languagesarray[$sl]?></option>
						<?php else: ?>
							<option value="<?=$sl?>"><?=$languagesarray[$sl]?></option>
						<?php endif; ?>
						<?php endforeach;?>
					<?php endif; ?>
				</select>
			</p>
			</fieldset>
            
			<div class="buttonbar" style="margin-right:0px;">
				<input type="hidden" name="postback2" value="postback2" id="postback2">
				<input type="hidden" name="flavorid" value="<?=$flavorid?>" id="flavorid">
				<button class="btn primary" type="submit"><?= __('CREATE APP') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
<?php endif; ?>
<script type="text/javascript">
	$(".checkboxClass").click(function(event) {
		var value = $(this).attr('value');
		var text = $(this).next('label').text();
		if($(this).is(':checked')) {
			changeDefaultLanguages(value,text, true);
		} else {
			changeDefaultLanguages(value,text, false);
		}
		
	});
	function changeDefaultLanguages(value,text, check) {
		if(check) {
			$('#defaultlang').append(
				$('<option></option>').val(value).html(text)
			);
		} else {
			$("#defaultlang option[value='"+value+"']").remove();
		}
	}
</script>
