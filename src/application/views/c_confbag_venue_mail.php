<h1><?= $title ?></h1>
<p>
	<?= __('If you proceed you will send a mail to all users who have added things to their conference bag. The mail will contain all the information about their added items.') ?><br/>
</p>
<img src="img/loader.gif" style="display:none;float:right;" id="loader" /><br clear="all" />
<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit" id="formmails">
	<p>
		<label><?= __('Your email address (sender)')?>:</label>
		<input type="text" name="email" value="<?= $event->email ?>" />
	</p>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<button type="submit" class="btn primary" id="sendmails"><?= __('Send mails')?></button>
		<br clear="all" />
	</div>
</form>
<script type="text/javascript">
$(document).ready(function() {
	$("#sendmails").click(function(event) {
		event.preventDefault();
		$("#loader").css('display', 'block');
		$("#formmails").submit();
	});
});
</script>