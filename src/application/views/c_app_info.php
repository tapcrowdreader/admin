<a href="<?= site_url('module/edit/21/app/'.$app->id) ?>" class="edit btn" style="float:right;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a>
<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<h3><?=$app->name?> <?= __('Info') ?></h3>
        <?php foreach($applanguages as $language) : ?>
        <p <?= (form_error("info_".$language->key) != '') ? 'class="error"' : '' ?>>
            <label for="info"><?=__('Info')?> <?= '(' . $language->name . ')'; ?></label>
			<?php $trans = _getTranslation('app', $app->id, 'info', $language->key); ?>
            <textarea name="info_<?php echo $language->key; ?>" id="info" <?= (form_error("info_". $language->key) != '') ? 'class="error"' : '' ?>><?= htmlspecialchars_decode(set_value('info_'.$language->key, ($trans != null) ? $trans : $app->info), ENT_NOQUOTES) ?></textarea>
        </p>
        <?php endforeach; ?>
		<p id="p_address">
			<label for="address"><?= __('Address:') ?></label>
			<input type="text" name="address" id="address" value="<?= htmlspecialchars_decode(set_value('address', $app->address), ENT_NOQUOTES) ?>" <?= (form_error("address") != '') ? 'class="error"' : '' ?> onblur="codeAddress()" />
			<a href="<?=$this->uri->uri_string()?>#showonmap" class="btn" id="btnshowonmap" onclick="codeAddress(); return false;"><?= __('Show on map') ?></a>
		</p>
		<div id="map_canvas" class="map" style="height:300px; width:620px; margin-left:0px;"></div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript">
			var geocoder;
			var map;
			var bounds;
			var marker;
			
			function codeAddress() {
				var address = document.getElementById('address').value;
				if(address != '') {
					geocoder.geocode( { 'address': address}, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							if(marker != null){
								marker.setMap(null);
							}
							map.setCenter(results[0].geometry.location);
							marker = new google.maps.Marker({
								map: map, 
								position: results[0].geometry.location
							});

							$('#lat').val(results[0].geometry.location.lat());
							$('#lon').val(results[0].geometry.location.lng());

							marker.draggable = true;
							google.maps.event.addListener(marker, 'drag', function() {
								$('#lat').val(marker.getPosition().lat());
								$('#lon').val(marker.getPosition().lng());
							}); 

							bounds = new google.maps.LatLngBounds();
							bounds.extend(results[0].geometry.location);
							map.fitBounds(bounds);
							map.setZoom(map.getZoom()-1);
						} else {
							if(status === 'ZERO_RESULTS'){
								alert('<?= __('No results found on the address ...') ?>');
							} else {
								alert('<?= __("Geocode was not successful for the following reason: %s", status) ?>');
							}
						}
					});
				}
			}
			
			$(document).ready(function() {
				geocoder = new google.maps.Geocoder();
				var latlng = new google.maps.LatLng(<?= ($app->lat != '') ? $app->lat : '50.753887' ?>, <?= ($app->lon != '') ? $app->lon : '4.269936' ?>);
				var myOptions = {
					zoom: 8,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
				bounds = new google.maps.LatLngBounds();
				
				<?php if($app->lat != '' && $app->lon != '') { ?>
				map.setCenter(latlng);
				marker = new google.maps.Marker({
					map: map, 
					position: latlng
				});
				
				marker.draggable = true;
				google.maps.event.addListener(marker, 'drag', function() {
					$('#lat').val(marker.getPosition().lat());
					$('#lon').val(marker.getPosition().lng());
				}); 
				
				bounds.extend(latlng);
				map.setZoom(16);
				<?php } ?>
			});
			
		</script>
		<p style="display:none;" >
			<label for="lat"><?= __('Latitude:') ?></label>
			<input type="text" name="lat" id="lat" value="<?= set_value('lat', $app->lat) ?>" />
		</p>
		<p style="display:none;">
			<label for="lon"><?= __('Longitude:') ?></label>
			<input type="text" name="lon" id="lon" value="<?= set_value('lon', $app->lon) ?>" />
		</p>
		<p <?= (form_error("tel") != '') ? 'class="error"' : '' ?>>
			<label for="tel"><?= __('Telephone:') ?></label>
			<input type="text" name="tel" id="tel" value="<?= set_value('tel', $app->telephone) ?>" />
		</p>
		<p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
			<label for="email"><?= __('Email:') ?></label>
			<input type="text" name="email" id="email" value="<?= set_value('email', $app->email) ?>" />
		</p>
		<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
			<label for="website"><?= __('Website:') ?></label>
			<input type="text" name="website" id="website" value="<?= set_value('website', $app->website) ?>" />
		</p>

		<?php foreach($metadata as $m) : ?>
			<?php foreach($applanguages as $lang) : ?>
				<?php if(_checkMultilang($m, $lang->key, $app)): ?>
					<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
						<?= _getInputField($m, $app, $lang, $applanguages, 'app', $app->id); ?>
					</p>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<a href="<?= site_url('apps/view/'.$app->id) ?>" class="btn"><?= __('Cancel') ?></a>
		<button type="submit" class="btn primary"><?= __('Save') ?></button>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>