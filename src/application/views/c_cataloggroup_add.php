<script type="text/javascript">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if(isset($tags) && $tags != null){foreach($tags as $val){ echo '"'.$val->tag.'", '; }} ?>],

		});
	});
</script>
<div>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<h1><?=__('Add Cataloggroup')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<?php if(!isset($venues)): ?>
				<p <?= (form_error("events") != '') ? 'class="error"' : '' ?> style="display:none;">
					<label><?= __('Event') ?></label>
					<select name="events" id="events">
						<option value=""><?= __('Select ...') ?></option>
						<?php foreach ($events as $evt): ?>
							<option value="<?= $evt->id ?>" <?= set_select('events', $evt->id) ?>><?= $evt->name ?></option>
						<?php endforeach ?>
					</select>
				</p>
			<?php else: ?>
				<p <?= (form_error("venues") != '') ? 'class="error"' : '' ?> style="display:none;">
					<label><?= __('Venue') ?></label>
					<select name="venues" id="venues">
						<option value=""><?= __('Select ...') ?></option>
						<?php foreach ($venues as $vnu): ?>
							<option value="<?= $vnu->id ?>" <?= set_select('venues', $vnu->id) ?>><?= $vnu->name ?></option>
						<?php endforeach ?>
					</select>
				</p>
			<?php endif; ?>
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Name:') ?></label>
				<input type="text" name="name" value="<?= set_value('name') ?>" id="name">
			</p>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:') ?></label>
				<input type="file" name="image" id="image" value="<?=set_value('image')?>" class="logoupload" />
			</p><br />
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:') ?></label>
				<input type="text" name="order" value="<?= set_value('order') ?>" id="order">
			</p>
			<?php if($app->id != 393) : ?>
			<div class="row">
				<label for="tags"><?= __('Tags:') ?></label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<?php else: ?>
				<p>
					<label for="tagdelbar"><?= __('Merk:') ?></label><br/>
					<select name="tagdelbar">
						<option value="Audi"><?=__('Audi')?></option>
						<option value="Volkswagen"><?=__('Volkswagen')?></option>
						<option value="skoda"><?=__('Skoda')?></option>
						<option value="volkswagenbedrijfsvoertuigen" ><?=__('Volkswagen Bedrijfsvoertuigen')?></option>
					</select>
				</p>
			<?php endif; ?>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>