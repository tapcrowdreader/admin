<div>
	
	<script type="text/javascript" charset="utf-8">
		// Sortables
		$(document).ready(function() {
			// Sessiongroups sorteren
			$( "#sessions" ).sortable({
				handle : '.position',
				placeholder: 'placeholder',
				axis: 'y',
				start: function() {},
				stop: function() {},
				update : function () { 
					var sorturl = "<?= site_url('sessions/sort/groups/') ?>";
					var data = new Array();
					$(this).children().each(function(i) {
						var li = $(this);
						if(li.attr("class") == "group"){
							data.push('' + li.attr("rel") + '=' + (i+1) + '');
						}
					});
					$.post(sorturl, {"records[]": data});
				}
			});
			
			// Sessions sorteren
			$(".groups").each(function(){
				$(this).sortable({
					handle : '.position',
					placeholder: 'gplaceholder',
					axis: 'y',
					start: function() {},
					stop: function() {},
					update : function () { 
						var sorturl = "<?= site_url('sessions/sort/sessions/') ?>";
						var data = new Array();
						$(this).children().each(function(i) {
							var li = $(this);
							if(li.attr("class") == "session"){
								data.push('' + li.attr("rel") + '=' + (i+1) + '');
							}
						});
						$.post(sorturl, {"records[]": data});
					}
				});
			});
		});
	</script>
	
	<h1><?=__('Sessions')?></h1>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<div class="sessions">
	<?php if ($sessions != FALSE): ?>
		<ul id="sessions">
		<?php foreach ($sessions as $group): ?>
			<li class="group" rel="<?= $group->id ?>">
				<a href="#" class="position"><span>&nbsp;</span></a>
				<a href="<?= site_url('sessions/delete/group/'.$group->id) ?>" class="delete" id="deletebtn"><span><?= __('Delete') ?></span></a>
				<a href="<?= site_url('sessions/edit/group/'.$group->id) ?>" class="edit"><span><?= __('Edit sessiongroup') ?></span></a>
				<h2><?= $group->name ?></h2>
				<?php if ($group->sessions != FALSE): ?>
					<ul class="groups">
					<?php foreach ($group->sessions as $session): ?>
						<li class="session" rel="<?= $session->id ?>">
							<div class="buttons">
								<a href="<?= site_url('sessions/edit/session/'.$session->id) ?>" class="edit" alt="Edit session">&nbsp;<span><?= __('Edit session') ?></span></a>
								<a href="<?= site_url('sessions/delete/session/'.$session->id) ?>" class="delete" id="deletebtn" alt="Delete session">&nbsp;<span><?= __('Delete session') ?></span></a>
								<a href="#" class="position" alt="Switch position">&nbsp;<span><?= __('Switch position') ?></span></a>
							</div>
							<div class="data">
								<label><?= __('Name :') ?> </label><span><?= $session->name ?></span><br />
								<label><?= __('Description :') ?> </label><span><?= nl2br($session->description) ?></span><br />
								<label><?= __('From - to :') ?> </label>
									<span><?= date('d/m/Y', strtotime($session->starttime)) ?> 
									<?= date('H', strtotime($session->starttime)) ?>:<?= date('i', strtotime($session->starttime)) ?> - 
									<?= date('d/m/Y', strtotime($session->endtime)) ?> 
									<?= date('H', strtotime($session->endtime)) ?>:<?= date('i', strtotime($session->endtime)) ?></span><br />
								<label><?= __('Speaker :') ?> </label><span><?= $session->speaker ?></span>
							</div>
							<br clear="all" />
						</li>
					<?php endforeach ?>
					</ul>
				<?php else: ?>
					<p><?= __('No sessions in the group') ?></p>
				<?php endif ?>
			</li>
		<?php endforeach ?>
		</ul>
	<?php else: ?>
		<p><?= __('No sessions found for this event.') ?></p>
	<?php endif ?>
	</div>
</div>