<?php
if(!isset($auth_token)) { ?>
	<form class="form-horizontal" method="POST" action="/auth/recovery">

		<div class="control-group">
			<label class="control-label" for="inputEmail"><?=__('Email')?></label>
			<div class="controls">
				<input type="text" id="inputEmail" placeholder="Email" class="span3" name="auth_email" />
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
				<a href="/auth/login" class="btn"><?=__('Cancel')?></a>
				<button type="submit" class="btn btn-primary"><?=__('Recover password')?></button>
			</div>
		</div>
	</form><?php
} else { ?>
	<form class="form-horizontal" method="POST" action="/auth/save">

		<div class="control-group">
			<label class="control-label" for="inputLogin"><?=__('Login')?></label>
			<div class="controls">
				<select name="auth_login" class="span3"><?php
				foreach($logins as $login) {
					echo "<option>$login</option>";
				} ?>
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="inputName"><?=__('Password')?></label>
			<div class="controls">
				<input type="password" name="auth_pass" class="span3" />
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="inputName"><?=__('Confirm Password')?></label>
			<div class="controls">
				<input type="password" name="auth_pass2" class="span3" />
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
				<input type="hidden" name="auth_token" value="<?=$auth_token?>" />
				<a href="/auth/login" class="btn"><?=__('Cancel')?></a>
				<button type="submit" class="btn btn-primary" name="auth_reset"><?=__('Reset password')?></button>
			</div>
		</div>
	</form><?php
} ?>