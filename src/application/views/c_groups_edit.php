<div>
	<h1><?=__('Edit')?> <?=$group->name?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>         
			<?php foreach($languages as $language) : ?>
	        <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
	        	 <?php $trans = _getTranslation('group', $group->id, 'name', $language->key); ?>
	            <label for="name"><?= __('Name') ?> <?= '(' . $language->name . ')'; ?>:</label>
	            <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?= htmlspecialchars_decode(set_value('name_'.$language->key, ($trans != null) ? $trans : $group->name)) ?>" />
	        </p> 
	        <?php endforeach; ?>
			<p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl"><?= __('Image:') ?></label>
				<?php if($group->imageurl != "" && file_exists($this->config->item('imagespath') . $group->imageurl)){ ?><span class="evtlogo" style="background:transparent url(<?= image_thumb($group->imageurl, 50, 50) ?>) no-repeat top left;"></span><?php } ?>
				<span class="hintAppearance"><?=__('Image must be in jpg or png format, max width: 2000px, max height: 2000px')?></span>
				<input type="file" name="imageurl" id="imageurl" value="" class="logoupload" />
				<?php if($group->imageurl != '' && file_exists($this->config->item('imagespath') . $group->imageurl)){ ?><span><a href="<?= site_url('groups/removeimage/'.$group->id.'/'.$type.'/'.$object) ?>" class="deletemap"><?=__('Remove')?></a></span><?php } ?>
			</p><br clear="all" />
			<p>
				<label><?= __('Display type: ')?></label>
				<select name="displaytype" id="displaytype">
					<option value="list" <?= ($group->displaytype == 'list') ? 'selected="selected"' : '' ?>><?= __('List') ?></option>
					<option value="slider" <?= ($group->displaytype == 'slider') ? 'selected="selected"' : '' ?>><?= __('Slider') ?></option>
					<option value="thumbs" <?= ($group->displaytype == 'thumbs') ? 'selected="selected"' : '' ?>><?= __('Thumbs') ?></option>
				</select>
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:') ?></label>
				<input type="text" name="order" value="<?= set_value('order', $group->order) ?>" id="order">
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div> 
</div>