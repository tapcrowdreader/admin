    
	<h1><?=__('Edit Form')?></h1>
    
    
	<div class="frmformbuilder">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            			
            
			<?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="title"><?=__('Title')?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('form', $form->id, 'title', $language->key); ?>
                <input type="text" name="title_<?php echo $language->key; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$language->key, ($trans != null) ? $trans : $form->name ), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            
            
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("submissionbuttonlabel_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="submissionbuttonlabel"><?= __('Submit Button Label') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('form', $form->id, 'submissionbuttonlabel', $language->key); ?>
                <input type="text" name="submissionbuttonlabel_<?php echo $language->key; ?>" id="submissionbuttonlabel" value="<?= htmlspecialchars_decode(set_value('submissionbuttonlabel_'.$language->key, ($trans != null) ? $trans : $form->submissionbuttonlabel ), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
            
            
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("submissionconfirmationtext_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="submissionconfirmationtext"><?= __('Confirmation Text') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('form', $form->id, 'submissionconfirmationtext', $language->key); ?>
                <textarea name="submissionconfirmationtext_<?php echo $language->key; ?>" id="submissionconfirmationtext"><?= htmlspecialchars_decode(set_value('submissionconfirmationtext_'.$language->key, ($trans != null) ? $trans : $form->submissionconfirmationtext ), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endforeach; ?>
            <?php else : ?>
            
            <p <?= (form_error("title_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="name"><?= __('Title:') ?></label>
                <input type="text" name="title_<?php echo $app->defaultlanguage; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$app->defaultlanguage, _getTranslation('form', $form->id, 'title', $app->defaultlanguage)), ENT_NOQUOTES) ?>" />
            </p>
            
            
            <p <?= (form_error("submissionbuttonlabel_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="submissionbuttonlabel"><?= __('Submit Button Label:') ?></label>
                <input type="text" name="submissionbuttonlabel_<?php echo $app->defaultlanguage; ?>" id="submissionbuttonlabel" value="<?= htmlspecialchars_decode(set_value('submissionbuttonlabel_'.$app->defaultlanguage, _getTranslation('form', $form->id, 'submissionbuttonlabel', $app->defaultlanguage)), ENT_NOQUOTES) ?>" />
            </p>
            
            
            <p <?= (form_error("submissionconfirmationtext_".$app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="submissionconfirmationtext"><?= __('Confirmation Text:') ?></label>
                <textarea name="submissionconfirmationtext_<?php echo $app->defaultlanguage; ?>" id="submissionconfirmationtext"><?= htmlspecialchars_decode(set_value('submissionconfirmationtext_'. $app->defaultlanguage, _getTranslation('form', $form->id, 'submissionconfirmationtext',  $app->defaultlanguage)), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endif; ?>
			
            
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order', $form->order) ?>" id="order">
			</p>
			            
			
			<p <?= (form_error("emailsendresult") != '') ? 'class="error"' : '' ?> >
				<label for="emailsendresult"><?= __('Email Results:') ?></label>
				<select name="emailsendresult" id="emailsendresult">
					<option value="0"><?= __('Select ...') ?></option>
					<option value="yes" <?= set_select('emailsendresult', "yes", ('yes' == $form->emailsendresult ? TRUE : '')) ?>><?= __('Yes') ?></option>
                    <option value="no" <?= set_select('emailsendresult', "no", ('no' == $form->emailsendresult ? TRUE : '')) ?>><?= __('No') ?></option>
				</select>
			</p>
            
            <?php if($form->emailsendresult == "yes") {
				$display = "block";
			}else {
				$display = "none";
			} ?>
            <div id="option_email" style="display:<?= $display ?>;">
            <p <?= (form_error("email") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Email')?> (<?=__('separate email addresses with a comma')?>):</label>
				<input type="text" name="email" value="<?= set_value('email', $form->email) ?>" id="email">
			</p>
            </div>
            
            <p <?= (form_error("allowmutliplesubmissions") != '') ? 'class="error"' : '' ?> >
				<label for="allowmutliplesubmissions"><?=__('Allow Mutliple Submissions:')?></label>
				<select name="allowmutliplesubmissions" id="allowmutliplesubmissions">
					<option value="0"><?= __('Select ...') ?></option>
					<option value="yes" <?= set_select('allowmutliplesubmissions', "yes", ("yes" == $form->allowmutliplesubmissions ? TRUE : '')) ?>><?= __('Yes') ?></option>
                    <option value="no" <?= set_select('allowmutliplesubmissions', "no", ("no" == $form->allowmutliplesubmissions ? TRUE : '')) ?>><?= __('No') ?></option>
				</select>
			</p>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Icon:') ?></a></label>
				<span class="hintAppearance"><?= __('Image must be in png format, width: %s px, height: %s px',140,140) ?></span>
				<span class="evtlogo" <?php if($launcher->icon != ''){ ?>style="background:<?= isset($launcherbackgroundcolor) ? $launcherbackgroundcolor : 'transparent' ?> url('<?= image_thumb($launcher->icon, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="image" id="image" value="" class="logoupload" />
				<?php if(isset($venue)) : ?>
					<?php if($launcher->icon != '' && file_exists($this->config->item('imagespath') . $launcher->icon)){ ?><span><a href="<?= site_url('module/removelauncherimage/'.$launcher->id.'/venue/'.$venue->id) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
				<?php elseif(isset($event)) : ?>
					<?php if($launcher->icon != '' && file_exists($this->config->item('imagespath') . $launcher->icon)){ ?><span><a href="<?= site_url('module/removelauncherimage/'.$launcher->id.'/event/'.$event->id) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
				<?php elseif(isset($app)) : ?>
					<?php if($launcher->icon != '' && file_exists($this->config->item('imagespath') . $launcher->icon)){ ?><span><a href="<?= site_url('module/removelauncherimage/'.$launcher->id.'/app/'.$app->id) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
				<?php endif; ?>
				<br clear="all" />
			</p>
			<?php if($launcher->moduletypeid != 60 && $launcher->moduletypeid != 61) : ?>
            <p>
				<label><input style="width: auto !important; display: inline !important;" type="checkbox" <?php if($form->getgeolocation == 1){ echo 'checked="checked"';} ?> value="1" name="geolocation" id="geolocation" /> <?= __('Include User Location')?></label>                
            </p>	
            <p>
				<label><input style="width: auto !important; display: inline !important;" type="checkbox" <?php if($singlescreen == 0){ echo 'checked="checked"';} ?> value="1" name="multiscreen" id="multiscreen" /> <?= __('Multi Screen Form')?></label>
            </p>
			<p>
				<label><input style="width: auto !important; display: inline !important;" type="checkbox" <?php if($form->emailconfirmation == 1){ echo 'checked="checked"';} ?> value="1" name="emailconfirmation" id="emailconfirmation" /> <?= __('Send user confirmation mail')?></label>
				<span class="hintAppearance"><?= __("Please make sure to add a field to the form with name and type 'email'") ?></span>
			</p>
        	<?php endif; ?>
			<p>
				<label><input style="width: auto !important; display: inline !important;" type="checkbox" <?php if($launcher->displaytype != 'none'){ echo 'checked="checked"';} ?> value="1" name="displaytype" id="displaytype" /> <?= __('Display in launcher')?></label>
			</p>
			<p <?= (form_error("customurl") != '') ? 'class="error"' : '' ?>>
            	<?php
				$checked= ''; $customurlStyle = 'display:none;';
				
				if( !empty( $form->customurl ) || form_error("customurl") != '' ){ $checked= 'checked="checked"'; $customurlStyle = 'display:block;'; }?>
            	<label><input onclick="jQuery('#customurl').toggle();jQuery('#customurl').val('');" style="width: auto !important; display: inline !important;" type="checkbox" <?= $checked;?> value="1" name="chkcustomurl" id="chkcustomurl" /> <?= __('Post to Custom URL')?></label>
                <input type="text" style="<?= $customurlStyle;?>" name="customurl" value="<?= set_value('customurl', $form->customurl) ?>" id="customurl" />
            </p>
            <?php if($launcher->moduletypeid != 60 && $launcher->moduletypeid != 61 && $launcher->moduletypeid != 62) : ?>
            <p>
            	<label><input style="width: auto !important; display: inline !important;" type="checkbox" <?php if($form->useflows){ echo 'checked="checked"';} ?> value="1" name="useflows" id="useflows" /> <?= __('Form with flows')?></label>
                <span class="hintAppearance"><?= __("Please Make sure to check 'Multi Screen Form' when using flows") ?></span>
            </p>
        	<?php endif; ?>
			<?php if($launcher->moduletypeid == 61) : 
			$opts = explode(',', $form->opts);
			?>
			<p>
				<label><?= __('Question moderation?')?> <input style="width: auto !important; display: inline !important;" type="checkbox" <?php if(in_array('moderation', $opts)){ echo 'checked="checked"';} ?> value="1" name="moderation" id="moderation" /></label>
			</p>
			<?php endif; ?>
            <div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<?php if($form->venueid != 0) {
					$type = 'venue';
				} elseif($form->eventid != 0) {
					$type = 'event';
				} else {
					$type = 'app';
				} ?>
				<a class="btn btn-danger" href="<?= site_url('forms/delete/'.$form->id.'/'.$type) ?>">
					<img width="16" height="16" alt="TapCrowd App" src="img/icons/delete.png">
					<?= __('Delete Form') ?>
				</a>
                <a href="javascript:history.go(-1);" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
    
<script type="text/javascript" charset="utf-8">
		// Sortables
		$(document).ready(function() {
			// Sessiongroups sorteren
			$( "#emailsendresult" ).change(function() {
			   var value = $(this).attr('value');
			   if(value == "yes") //If type = select/radio
			   {
				   document.getElementById("option_email").style.display='block';
			   }
			   else
			   {
				    document.getElementById("option_email").style.display='none';
			   }
			});
		});
</script>