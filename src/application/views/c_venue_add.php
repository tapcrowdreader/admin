<style>
form.addevent div.row ul.tagit input[type="text"] {
    margin-left: -10px;
    padding: 3px 8px 3px 0;
}
</style>

<form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<?php if($app->apptypeid == 5) : ?>
		<h3><?= __('Add New POI')?></h3>
		<?php else: ?>
			<?php if(isset($_GET['newapp'])) : ?>
				<h1><?=__('Some more info')?></h1>
				<div class="progress progress-striped active">
					<div class="bar" style="width: 75%;"></div>
				</div>
			<?php else: ?>
				<h1><?=__('Add New Venue')?></h1>
			<?php endif; ?>
		<?php endif; ?>
        <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("w_name_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="w_name_"><?= __('What is the name of your venue?' .' (' . $language->name . ')'); ?></label>
                <input type="text" name="w_name_<?php echo $language->key; ?>" id="w_name" value="<?=set_value('w_name_'.$language->key, ($app->familyid) != 3 ? $app->name : '')?>" />
            </p>
            <?php endforeach; ?>
        <?php else : ?>
            <p <?= (form_error("w_name_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="w_name"><?= __('What is the name of your venue?')?></label>
                <input type="text" name="w_name_<?php echo $app->defaultlanguage; ?>" id="w_name" value="<?=set_value('w_name_'.$app->defaultlanguage, ($app->familyid) != 3 ? $app->name : '')?>" />
            </p>
        <?php endif; ?>
		
		<p <?= (form_error("w_address") != '') ? 'class="error"' : '' ?>>
			<label for="w_address"><?= __('Where is it located?')?></label>
			<input type="text" name="w_address" id="w_address" value="<?= set_value('w_address') ?>" onblur="codeAddress()" style="width:470px;" />
			<a href="#showonmap" onclick="codeAddress(); return false;" class="btn" style="margin-left:5px;"><?= __('Show on map')?> &raquo;</a>
		</p>
		<div id="map_canvas" class="map" style="height:300px; width:610px;"></div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript">
			var geocoder;
			var map;
			var bounds;
			var marker;
			
			function codeAddress() {
				var address = document.getElementById('w_address').value;
				if(address != '') {
					geocoder.geocode( { 'address': address}, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							if(marker != null){
								marker.setMap(null);
							}
							map.setCenter(results[0].geometry.location);
							marker = new google.maps.Marker({
								map: map, 
								position: results[0].geometry.location
							});
							var lat = results[0].geometry.location.lat().toString();
							var lon = results[0].geometry.location.lng().toString();
							$('#w_lat').val(lat.substring(0,10));
							$('#w_lon').val(lon.substring(0,10));

							marker.draggable = true;
							google.maps.event.addListener(marker, 'drag', function() {
								var lat = marker.getPosition().lat().toString();
								var lon = marker.getPosition().lng().toString();
								$('#w_lat').val(lat.substring(0,10));
								$('#w_lon').val(lon.substring(0,10));
							}); 

							bounds = new google.maps.LatLngBounds();
							bounds.extend(results[0].geometry.location);
							map.fitBounds(bounds);
							map.setZoom(map.getZoom()-1);
						} else {
							if(status === 'ZERO_RESULTS'){
								alert('<?= __('No results found on the address ...')?>');
							} else {
								alert("<?= __('Geocode was not successful for the following reason: ')?> + status");
							}
						}
					});
				}
			}
			
			$(document).ready(function() {
				geocoder = new google.maps.Geocoder();
				var latlng = new google.maps.LatLng(<?= set_value('w_lat','50.753887') ?>, <?= set_value('w_lon','4.269936') ?>);
				var myOptions = {
					zoom: 8,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
				bounds = new google.maps.LatLngBounds();
				
				<?php if (set_value('w_address') != '') { ?>
					codeAddress();
				<?php }?>
				
				$('#w_address').keydown(function(e) {
					if(e.keyCode == 13) {
						e.preventDefault();
						return false;
						//codeAddress();
					}
				});
			});
			
		</script>
		<a href="" id="showcordinates" class="btn" style="margin-top:-10px;margin-bottom:10px;"><?= __('Advanced') ?></a><br clear="all" />
		<div id="cordinates" style="display:none;">
			<p <?= (form_error("w_lat") != '') ? 'class="error"' : '' ?>>
				<label for="w_lat"><?= __('Latitude:')?></label>
				<input type="text" name="w_lat" id="w_lat" value="<?= substr(set_value('w_lat', 0), 0, 10) ?>" style="width:100px;" maxlength="10" /><br clear="all" />
			</p>
			<p <?= (form_error("w_lon") != '') ? 'class="error"' : '' ?> >
				<label for="w_lon"><?= __('Longitude:')?></label>
				<input type="text" name="w_lon" id="w_lon" value="<?= substr(set_value('w_lon', 0), 0, 10) ?>" style="width:100px;" maxlength="10" /><br clear="all" />
			</p>
		</div>
		<p <?= (form_error("w_tel") != '') ? 'class="error"' : '' ?>>
			<label for="w_tel"><?= __('Telephone:')?></label>
			<input type="text" name="w_tel" id="w_tel" value="<?= set_value('w_tel') ?>" />
		</p>
		<p <?= (form_error("w_fax") != '') ? 'class="error"' : '' ?>>
			<label for="w_tel"><?= __('Fax:')?></label>
			<input type="text" name="w_fax" id="w_fax" value="<?= set_value('w_fax') ?>" />
		</p>
		<p <?= (form_error("w_email") != '') ? 'class="error"' : '' ?>>
			<label for="w_email"><?= __('Email:')?></label>
			<input type="text" name="w_email" id="w_email" value="<?= set_value('w_email') ?>" />
		</p>
		<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
			<label for="website"><?= __('Website:')?></label>
			<input type="text" name="website" id="website" value="<?= set_value('website') ?>" />
		</p>
		<p <?= (form_error("facebookurl") != '') ? 'class="error"' : '' ?>>
			<label for="facebookurl"><?= __('Facebook url:')?></label>
			<input type="text" name="facebookurl" id="facebookurl" value="<?= set_value('facebookurl') ?>" />
		</p>
		<p style="display:none;" <?= (form_error("w_opening") != '') ? 'class="error"' : '' ?>>
			<label for="w_opening"><?= __('Opening')?><br /><?= __('hours:')?></label>
			<textarea name="w_opening" id="w_opening"><?= set_value('w_opening') ?></textarea>
		</p>
        <?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("w_info_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="w_info"><?= __('Info'.'(' . $language->name . ')'); ?>:</label>
                <textarea name="w_info_<?php echo $language->key; ?>" id="w_info"><?= set_value('w_info_'.$language->key) ?></textarea>
            </p>
            <?php endforeach; ?>
        <?php else : ?>
            <p <?= (form_error("w_info_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                <label for="w_info"><?= __('Info:')?></label>
                <textarea name="w_info_<?php echo $app->defaultlanguage; ?>" id="w_info"><?= set_value('w_info_'.$app->defaultlanguage) ?></textarea>
            </p>
        <?php endif; ?>
<!--         <div class="row">
			<p style="min-height:0px;">
			<label for="tags">Tags:</label>
			</p>
			<ul id="mytags" name="mytagsul"></ul>
        </div> -->
		<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
			<label for="order"><?= __('Order:')?></label>
			<input type="text" name="order" id="order" value="<?= set_value('order') ?>" />
		</p>
		<p <?= (form_error("imageurl1") != '') ? 'class="error"' : '' ?>>
			<label for="imageurl1"><?= ($app->apptypeid != 5) ? __('Image') : __('Image 1') ?>:</label>
			<?php if($app->apptypeid != 7 && $app->apptypeid != 8) : ?>
				<span class="hintAppearance" style="padding-left:3px;"><?= __('Optimized for pictures with 280 pixels width and 180px height or same dimension.')?></span><br/>
			<?php else: ?>
				<span class="hintAppearance" style="padding-left:3px;"><?= __('Optimized for pictures with 560 pixels width and 360px height.')?></span><br/>
			<?php endif; ?>
			<input type="file" name="imageurl1" id="imageurl1" value="" />
		</p>
		<?php if($app->apptypeid == 5) : ?>
		<p <?= (form_error("imageurl2") != '') ? 'class="error"' : '' ?>>
			<label for="imageurl2"><?= __('Image 2:')?></label>
			<input type="file" name="imageurl2" id="imageurl2" value="" />
		</p>
		<p <?= (form_error("imageurl3") != '') ? 'class="error"' : '' ?>>
			<label for="imageurl3"><?= __('Image 3:')?></label>
			<input type="file" name="imageurl3" id="imageurl3" value="" />
		</p>
		<p <?= (form_error("imageurl4") != '') ? 'class="error"' : '' ?>>
			<label for="imageurl4"><?= __('Image 4:')?></label>
			<input type="file" name="imageurl4" id="imageurl4" value="" />
		</p>
		<p <?= (form_error("imageurl5") != '') ? 'class="error"' : '' ?>>
			<label for="imageurl5"><?= __('Image 5:')?></label>
			<input type="file" name="imageurl5" id="imageurl5" value="" />
		</p>
		<?php endif; ?>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<?php if($app->apptypeid == 5) : ?>
		<button type="submit" class="btn primary"><?= __('Add POI')?></button>
		<?php else: ?>
		<button type="submit" class="btn primary"><?= __('Add Venue')?></button>
		<?php endif; ?>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [],
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = '<?=$tag->tag?>';
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});

		$("#showcordinates").click(function(event) {
			event.preventDefault();
			$("#cordinates").show();
			$(this).hide();
		});
	});
</script>