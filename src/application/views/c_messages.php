
<?php
#
# Module settings button
#
if(isset($settings->module_url)) { ?>
	<div class="btn-group pull-right">
		<?php if(isset($settings->premium)) : ?>
			<a href="<?= site_url('premium/'.$settings->parentType.'/'.$settings->parentId.'/'.$settings->premium) ?>" class="btn editPremiumButton"><i class="icon-pencil"></i> <?= __('Edit premium items') ?></a>
		<?php endif; ?>
		<a href="<?=$settings->module_url?>" class="btn"><i class="icon-wrench"></i> <?=__('Module Settings')?></a>
	</div><?php
} ?>

<?php if(isset($settings->title)) : ?>
<h2><?= ucfirst($settings->title) ?></h2>
<?php else: ?>
<h2><?=(empty($settings->plural)? 'No title set' : ucfirst(__($settings->plural)))?></h2>
<?php endif; ?>

<p>
<?= __("This module is a personalised inbox for app users.
MeetMe messages, Push notifications and other personalised messages will
be displayed in this module of the mobile app."); ?>
</p>
<form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent" id="form">
	<p>
		<?= __("You can send a message to all the users by filling in the form below:"); ?>
	</p>
	<p <?= (form_error("sender") != '') ? 'class="error"' : '' ?>>
		<label for="sender"><?= __('From:') ?></label>
		<select name="sender" id="sender" style="width:auto !important;">
			<option value="">-- <?= __('Please select a sender') ?> --</option>
			<?php foreach($users as $user) : ?>
			<option value="<?=$user->id?>" <?= set_select("sender", $user->id, set_value('sender') == $user->id ? true : false) ?>><?=$user->name?></option>
			<?php endforeach; ?>
		</select>
	</p>
	<p>
		<label for=""><?= __('To: <strong>ALL USERS</strong>') ?></label>
	</p>
	<p <?= (form_error("title") != '') ? 'class="error"' : '' ?>>
		<label for="title"><?= __('Title:') ?></label>
		<input type="text" name="title" id="title" value="<?= set_value('title') ?>"/>
	</p>
	<p <?= (form_error("message") != '') ? 'class="error"' : '' ?>>
		<label for="message"><?= __('Message:') ?></label>
		<textarea name="message" id="message"><?= set_value('message') ?></textarea>
	</p>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<button type="submit" class="btn primary" id="submit"><?= __('Send') ?></button>
		<br clear="all" />
	</div>
</form>

<script type="text/javascript">
$("#form").submit(function(event) {
	if($("#submit").val() != 'ok') {
		jConfirm('Are you sure you want to send this message?', 'Confirm', function(r) {
			if(r == true) {
				$("#submit").val('ok');
				$("#submit").click();
				return true;
			}
		});

		return false;
	}
});
</script>