<div>
	<h1><?=__('Build App')?></h1>
	<?php if($status == 'nothing') : ?>
		<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8" class="edit">
			<?php if($this->session->flashdata('event_feedback') != ''): ?>
			<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
			<?php endif ?>
			<p class="clear <?= (form_error("company") != '') ? 'error' : '' ?>">
				<label><?= __('Note: your design and content will update automatically in the apps but
				it is advised to only build your app once you have implemented all graphical elements.') ?> <br/>
				<?=__('When continuing this process, your demo-app will be built within 15 minutes.') ?><br/>
				<?=__('An e-mail will be sent to you, when your demo-app is ready. Enter an email address below:') ?></label>
				<input type="text" name="email" value="<?= set_value('email') ?>" id="email">
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Build') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />		
		</form>
		</div>
	<?php elseif($status == 'queue') : ?>
		<p>
			<?= __('Thanks,<br/>
			We have received your build-request.') ?><br/>
			<?=__('We will notify you, when your app is ready.') ?>
		</p>
	<?php elseif($status == 'busy') : ?>
		<p>
			<?= __('Your app is building... You\'ll be notified when your app has been built.') ?>
		</p>
	<?php elseif($status == 'ended') : ?>
		<p>
			<?php if($env == 'ios') : ?>
				<div class="frmsessions">
				<form action="<?= site_url($this->uri->uri_string()) ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8" class="edit">
				<?php $url = urlencode("http://clients.tapcrowd.com/demo/demoplist.php?appid=$app->id"); 
				$url = 'itms-services://?action=download-manifest&url=' . $url?>
				<p class="clear <?= (form_error("email2") != '') ? 'error' : '' ?>">
				<label><?= __('Please fill in your email address below. You will receive an email with a URL to download the app on your phone or tablet. Open the email on your phone or tablet and click the link to install the app.') ?><br/></label>
				<input type="text" name="email2" value="<?= set_value('email2') ?>" id="email2">
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback2" value="postback2" id="postback2">
					<button type="submit" class="btn primary"><?= __('Mail')?></button>
					<br clear="all" />
				</div>
				<br clear="all" />
				</form>
				</div>
			<?php elseif($env == 'android') : ?>
				<div class="frmsessions">
				<form action="<?= site_url($this->uri->uri_string()) ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8" class="edit">
				<p class="clear <?= (form_error("email2") != '') ? 'error' : '' ?>">
				<label><?= __('Please fill in your email address below. You will receive an email with a URL to download the app on your phone or tablet. Open the email on your phone or tablet and click the link to install the app.') ?><br/></label>
				<input type="text" name="email2" value="<?= set_value('email2') ?>" id="email2">
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback2" value="postback2" id="postback2">
					<button type="submit" class="btn primary"><?= __('Mail')?></button>
					<br clear="all" />
				</div>
				<br clear="all" />
				</form>
				</div>					
		<?php endif; ?>
		</p>
	<?php elseif($status == 'mailsent') : ?>
		<p>
			<?= __('Thank you! You will receive an email within a few minutes. Open the email on your phone or tablet and click the link to install the app.') ?>
		</p>
	<?php endif; ?>
</div>