<div>
	<h1><?= __('Import')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" class="edit" id="xpoimport">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("sel_beurs") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Exposition:') ?></label>
				<select name="sel_beurs" id="sel_beurs">
					<?php foreach ($beurzen as $beurs): ?>
						<option value="<?= $beurs->id ?>"><?= $beurs->name ?></option>
					<?php endforeach ?>
				</select>
			</p>
			<div class="importprogress"><?= __('Importing ...') ?></div>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary" id="startimport"><?= __('START IMPORT') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			
			$('#startimport').click(function(e){
				e.preventDefault();
				jConfirm('<?= __('Are you sure you want to start import!') ?>', '<?= __('Import Event') ?>', function(r) {
					if(r == true) {
						$('.importprogress').slideDown();
						$.ajax({
							url: '<?= site_url("dataimport/beurs2/". $event->id) ?>',
							type: 'POST',
							data: $('#xpoimport').serialize(),
							complete: function(xhr, textStatus) {
								$('.importprogress').slideUp();
								jAlert("<?= __('Import succesfull completed!') ?>', '<?= __('Succes') ?>");
							},
							success: function(data, textStatus, xhr) {
								
							},
							error: function(xhr, textStatus, errorThrown) {
								
							}
						});
						return true;
					} else {
						jAlert('<?= __('Import stopped!') ?>', '<?= __('Info') ?>');
						return false;
					}
				});
				return false;
			});
		});
	</script>
	
</div>