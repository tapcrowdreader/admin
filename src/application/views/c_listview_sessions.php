<div id="myTab">
<h1 style="float:left;"><?= __(ucfirst('Sessions')); ?></h1>
<a href="<?= site_url('module/editByController/sessions/event/'.$event->id.'/0') ?>" class="edit btn" style="float:right;margin-bottom:5px;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a><br style="margin-bottom:15pxclear:both;"/>
<div class="tabbable tabs-below">
	<?php if($app->apptypeid != 10) : ?>
	<ul class="nav nav-tabs">
		<li class="active">
			<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#sessions"><?= __('Sessions') ?></a>
		</li>
		<li class="">
			<a data-toggle="tab" class="taba" href="<?=$this->uri->uri_string;?>#speakers"><?= __('Speakers') ?></a>
		</li>
	</ul>
	<?php endif; ?>
	<br clear="all" />
	<div class="tab-content">
		<div id="sessions" class="tab-pane active">
		<?php
		// Event or Venue ??
		$type = 'event';
		if(isset($venue)) {
			$type = 'venue';
			$event = $venue;
		}
		if(isset($typeapp)) {
			$type = 'app';
		}

		?>
		<?php if($this->session->flashdata('event_feedback') != ''): ?>
		<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
		<?php endif ?>
		<?php if($this->session->flashdata('event_error') != ''): ?>
		<div class="error fadeout"><?= $this->session->flashdata('event_error') ?></div>
		<?php endif ?>
		<div class="listview">

			<?php
				$items = array('sessions','sessiongroups','news','exhibitors','brands','categories','schedule','attendees','poi','catalog', 'catalogbrands', 'catalogcategories', 'cataloggroups', 'artists', 'sponsors', 'sponsorgroups', 'push', 'ad');
				if (!isset($venue)) {
					$newname = array(
						'sessions' 		=> 'session',
						'sessiongroups' => 'sessiongroup',
						);

						if(isset($app) && $app->apptypeid == '4') {
							$newname['artists'] = 'Teammember';
						}
				} else {
					$newname = array(
						'sessions' 		=> 'session',
						'sessiongroups' => 'sessiongroup',
						);

						if(isset($app) && $app->apptypeid == '4') {
							$newname['artists'] = 'Teammember';
						}
				}
				?>
			<p>
				<div class="groupchooser">
					<?php if (isset($sessgroups) && $sessgroups != false): ?>
						<select name="sessgroup" id="sessgroup" class="groupselect">
							<option value=""><?=__('All sessiongroups')?></option>
							<?php foreach ($sessgroups as $group): ?>
							<option value="<?= $group->id ?>" <?= ($this->uri->segment(4) == $group->id) ? "selected" : "" ?>><?= $group->name ?></option>
							<?php endforeach ?>
						</select>
						<?php if (!isset($venue)): ?>
						<script type="text/javascript" charset="utf-8">
							$(document).ready(function() {
								$('#sessgroup').change(function(){
									window.location = "<?= site_url('sessions/event') . "/" . $this->uri->segment(3) ?>/" + $('#sessgroup').val() + "/";
								});
							});
						</script>
						<?php else: ?>
						<script type="text/javascript" charset="utf-8">
							$(document).ready(function() {
								$('#sessgroup').change(function(){
									window.location = "<?= site_url('sessions/venue') . "/" . $this->uri->segment(3) ?>/" + $('#sessgroup').val() + "/";
								});
							});
						</script>
						<?php endif; ?>
					<?php endif ?>
				</div>
			</p>
			<?php if($this->uri->segment(1) == 'sessions') : ?>
				<?php if(isset($event)) : ?>
					<a href="<?= site_url('sessiongroups/event/' . $event->id) ?>" class="edit btn"><i class="icon-pencil"></i>  <?= __('Edit Sessiongroups') ?></a>
				<?php endif; ?>
			<?php endif; ?>
			<?php if($this->uri->segment(1) == 'groups') : ?>
			<div class="buttonbar" style="float:left;">
			<?php else: ?>
			<div class="buttonbar">
			<?php endif; ?>
			<a href="<?= site_url('sessions/add/' . $event->id . '/' . $type) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i> <?= __('Add session') ?></a>
            <a href="<?= site_url('import/sessions_speakers/'.$event->id) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i> <?= __('Import') ?></a>
			</div>
			<br clear="all">
			<table id="listview" class="display zebra-striped">
				<thead>
					<tr>
						<?php foreach ($headers as $h => $field): ?>
							<?php if ($h == 'Order'): ?>
								<th class="data order"><?= $h ?></th>
							<?php else: ?>
								<th class="data"><?= $h ?></th>
							<?php endif ?>
						<?php endforeach; ?>
						<?php if($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'users' && _isAdmin()): ?>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						<?php elseif($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'apps' && _isAdmin()): ?>
							<th>&nbsp;</th>
						<?php elseif($this->uri->segment(1) != 'apps'): ?>
							<th width="17"></th>
							<th width="17"></th>
							<th width="17"></th>
						<?php endif?>
					</tr>
				</thead>
				<tbody>
					<?php if ($data != FALSE): ?>
					<?php foreach($data as $row): ?>
							<tr>
								<?php foreach ($headers as $h => $field): ?>
		                        <?php if ($field != null && !empty($field)) : ?>
		                        <?php if($this->uri->segment(1) == 'groups') : ?>
		                        	<?php if($row->groupitem == true) : ?>
		                        	<?php else : ?>
			                        	<?php if(isset($object)) : ?>
			                        	<td><a href='<?= site_url("groups/view/" . $row->id . '/' . $type . '/' . $typeid . '/' . $object) ?>' ><?= $row->{$field} ?></a></td>
			                        	<?php else: ?>
			                        	<td><a href='<?= site_url("groups/view/" . $row->id . '/' . $type . '/' . $typeid) ?>' ><?= $row->{$field} ?></a></td>
			                        	<?php endif; ?>
		                        	<?php endif; ?>
		                        <?php else :?>
		                        	<?php if($field == "datum" && $this->uri->segment(1) == 'news') : ?>

		                        		<td><?= date("F j, Y, H:m:s", strtotime($row->{$field})) ?></td>
		                        	<?php else: ?>
										<td><?= $row->{$field} ?></td>
									<?php endif; ?>
								<?php endif; ?>
		                        <?php endif; ?>
								<?php endforeach ?>
								<?php if($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'users' && _isAdmin()): ?>
								<td class="icon"><a href='<?= site_url("admin/users/edit/" . $row->id) ?>' ><i class="icon-pencil"></i></a></td>
								<td class="icon"><a href='<?= site_url("admin/apps/user/" . $row->id) ?>' ><img src="img/icons/application_cascade.png" width="16" height="16" alt="<?=__('Apps from user')?>" title="<?=__('Apps from user')?>" class="png" /></a></td>
								<td class="icon"><a href='<?= site_url("admin/users/loginas/" . $row->id) ?>' ><img src="img/icons/user_go.png" width="16" height="16" alt="<?=__('Login as')?>" title="<?=__('Login as')?>" class="png" /></a></td>
								<?php elseif($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'apps' && _isAdmin()): ?>
								<td class="icon"><a href='<?= site_url("admin/apps/edit/" . $row->id) ?>' ><i class="icon-pencil"></i></a></td>

								<?php elseif($this->uri->segment(1) == 'artists' && ($this->uri->segment(2) == 'app' || $this->uri->segment(2) == false)) : ?>
										<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/edit/" . $row->id . '/app/'. $app->id) ?>' ><i class="icon-pencil"></i></a></td>
										<td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/delete/" . $row->id . '/app/'. $app->id) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
								<?php elseif($this->uri->segment(1) != 'apps'): ?>
									<?php if($this->uri->segment(1) != 'push'): ?>
										<?php if($this->uri->segment(5) != null) : ?>
											<?php if($this->uri->segment(1) == 'groups' && $row->groupitem == true) : ?>
												<?php if($object == 'catalog') : ?>
													<td class="icon"><a href='<?= site_url($object."/edit/" . $row->itemid . '/' . $type . '/catalog/' . $row->groupid) ?>' ><i class="icon-pencil"></i></a></td>
													<td class="icon"><a href='<?= site_url("groupitems/delete/" . $row->id . '/' . $type . '/' . $this->uri->segment(5).'/catalog') ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
												<?php else : ?>
				                        			<?php if($object == '') : ?>
				                        			<?php if(strtolower($row->itemtable) == 'catalog') : ?>
													<td class="icon"><a href='<?= site_url(strtolower($row->itemtable)."/edit/" . $row->itemid . '/' . $type . '/' . strtolower($row->itemtable) . '/' . $row->groupid) ?>' ><i class="icon-pencil"></i></a></td>
													<?php else: ?>
													<td class="icon"><a href='<?= site_url(strtolower($row->itemtable)."/edit/" . $row->itemid . '/' . $type . '/' . $row->groupid) ?>' ><i class="icon-pencil"></i></a></td>
													<?php endif; ?>
													<td class="icon"><a href='<?= site_url(strtolower($row->itemtable)."/delete/" . $row->itemid . '/' . $type . '/' . $this->uri->segment(5)) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
					                        		<?php else: ?>
													<td class="icon"><a href='<?= site_url($object."/edit/" . $row->itemid . '/' . $type . '/' . $object . '/' . $row->groupid) ?>' ><i class="icon-pencil"></i></a></td>
													<td class="icon"><a href='<?= site_url($object."/delete/" . $row->itemid . '/' . $type . '/' . $this->uri->segment(5)) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
					                        		<?php endif; ?>
												<?php endif; ?>
											<?php elseif($this->uri->segment(1) == 'groups' && $row->groupitem != true) : ?>
												<td class="icon"><a href='<?= site_url("sessions/edit/" . $row->id . '/' . $type . '/' . $this->uri->segment(5) . '/' . $row->parentid . '/' . $object) ?>' ><i class="icon-pencil"></i></a></td>
												<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->id . '/session') ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
												<td class="icon"><a href='<?= site_url("sessions/delete/" . $row->id . '/' . $type) ?>' class="adeletegroup"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
											<?php else: ?>
												<td class="icon"><a href='<?= site_url("sessions/edit/" . $row->id . '/' . $type . '/' . $this->uri->segment(5)) ?>' ><i class="icon-pencil"></i></a></td>
												<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->id . '/session') ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
												<td class="icon"><a href='<?= site_url("sessions/delete/" . $row->id . '/' . $type. '/' . $this->uri->segment(5)) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
											<?php endif; ?>
										<?php else : ?>
										<td class="icon"><a href='<?= site_url("sessions/edit/" . $row->id . '/' . $type) ?>' ><i class="icon-pencil"></i></a></td>
										<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->id . '/session') ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
										<td class="icon"><a href='<?= site_url("sessions/delete/" . $row->id . '/' . $type) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
										<?php endif; ?>
									<?php else : ?>
									<td></td>
									<td></td>
									<?php endif; ?>
								<?php endif?>
							</tr>
					<?php endforeach ?>
					<?php else: ?>

					<?php endif ?>
				</tbody>
			</table>
		</div>
		</div>
		<div id="speakers" class="tab-pane">
		<div id="sessions" class="tab-pane active">
		<?php
		// Event or Venue ??
		$type = 'event';
		if(isset($venue)) {
			$type = 'venue';
			$event = $venue;
		}
		if(isset($typeapp)) {
			$type = 'app';
		}

		?>
		<?php if($this->session->flashdata('event_feedback') != ''): ?>
		<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
		<?php endif ?>
		<?php if($this->session->flashdata('event_error') != ''): ?>
		<div class="error fadeout"><?= $this->session->flashdata('event_error') ?></div>
		<?php endif ?>
		<div class="listview">
				<a href="<?= site_url('speakers/add/' . $event->id) ?>" class="add btn primary"><i class="icon-plus-sign icon-white"></i>  <?=__('Add Speaker')?></a>
				<br />
			</div>
			<br clear="all">
			<table id="listview_speakers" class="display zebra-striped">
				<thead>
					<tr>
						<?php foreach ($headersSpeakers as $h => $field): ?>
							<?php if ($h == 'Order'): ?>
								<th class="data order"><?= $h ?></th>
							<?php else: ?>
								<th class="data"><?= $h ?></th>
							<?php endif ?>
						<?php endforeach; ?>
						<?php if($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'users' && _isAdmin()): ?>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						<?php elseif($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'apps' && _isAdmin()): ?>
							<th>&nbsp;</th>
						<?php elseif($this->uri->segment(1) != 'apps'): ?>
							<th width="17"></th>
							<th width="17"></th>
							<th width="17"></th>
						<?php endif?>
					</tr>
				</thead>
				<tbody>
					<?php if ($speakers != FALSE): ?>
					<?php foreach($speakers as $row): ?>
							<tr>
								<?php foreach ($headers as $h => $field): ?>
		                        <?php if ($field != null && !empty($field)) : ?>
		                        	<?php if($field == "datum" && $this->uri->segment(1) == 'news') : ?>

		                        		<td><?= date("F j, Y, H:m:s", strtotime($row->{$field})) ?></td>
		                        	<?php else: ?>
										<td><?= $row->{$field} ?></td>
									<?php endif; ?>
		                        <?php endif; ?>
								<?php endforeach ?>
								<td class="icon"><a href='<?= site_url("speakers/edit/" . $row->id . '/' . $type . '/' . $this->uri->segment(5)) ?>' ><i class="icon-pencil"></i></a></td>
								<td class="icon"><a href='<?= site_url("duplicate/index/" . $row->id . '/speaker') ?>' ><img src="img/icons/btn-duplicate.png" width="16" height="16" alt="<?=__('Duplicate')?>" title="<?=__('Duplicate')?>" class="png" /></a></td>
								<td class="icon"><a href='<?= site_url("speakers/delete/" . $row->id . '/' . $type. '/' . $this->uri->segment(5)) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?=__('Delete')?>" alt="<?=__('Delete')?>" class="png" /></a></td>
							</tr>
					<?php endforeach ?>
					<?php endif ?>
				</tbody>
			</table>
		</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.adelete').click(function(e) {
			var delurl = $(this).attr('href');
			jConfirm('<?= __('Are you sure you want to delete this row?<br />This cannot be undone!') ?>', '<?= __('Remove Entry') ?>', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					return false;
				}
			});
			return false;
		});

		$('.adeletegroup').click(function(e) {
			var delurl = $(this).attr('href');
			jConfirm('<?= __('Are you sure you want to delete this categorie/brand?<br />All items below will be removed as well!') ?>', '<?= __('Remove Entry') ?>', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					return false;
				}
			});
			return false;
		});

		$("#sidebar").height($('#content').height());

		$('#myTab .taba').click(function (e) {
	    e.preventDefault();
	    $(this).tab('show');
    	});

	    <?php if($this->uri->segment(1) == 'speakers') : ?>
	    	$('a[href="/speakers/event/<?=$event->id?>#speakers"]').click();
	    <?php endif; ?>
	});
</script>