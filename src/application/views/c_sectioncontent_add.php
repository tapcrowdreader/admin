<div>
	<h1><?= __('Add Button')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Title '. '(' . $language->name . ')'); ?>:</label>
                <input maxlength="255" type="text" name="title_<?= $language->key; ?>" id="title" value="<?=set_value('title_'.$language->key)?>" />
            </p>
            <?php endforeach; ?>
            <p> 
                <label><?= __('Content module: ')?></label>
                <select name="contentmodule" id="contentmodule">
                    <?php foreach($contentmodules as $c) : ?>
                            <option value="<?=$c->id?>"><?=ucfirst($c->title)?></option>
                    <?php endforeach; ?>
                </select>
            </p>
            <p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
                <label for="imageurl"><?= __('Image:')?></label>
                <input type="file" name="imageurl" id="imageurl" value="" /><br />
                <span class="note" style="width:200px;"><?= __('Max size 2000px by 2000px')?></span>
                <br clear="all" />
            </p>
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label><?= __('Order:')?></label>
                <input type="text" name="order" value="<?= set_value('order') ?>" id="order">
            </p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
                <a href="<?= ("javascript:history.go(-1);"); ?>" class="btn"><?= __('Cancel')?></a>
				<button type="submit" class="btn primary"><?= __('Add Button')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>