<div>
	<h1><?=__('Edit POI')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label for="name"><?= __('Name:') ?></label>
				<input type="text" name="name" value="<?= htmlspecialchars_decode(set_value('name', $poi->name), ENT_NOQUOTES) ?>" id="name">
			</p>
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label for="latitude"><?= __('Latitude:') ?></label>
				<input type="text" name="latitude" value="<?= htmlspecialchars_decode(set_value('latitude', $poi->latitude), ENT_NOQUOTES) ?>" id="latitude">
			</p>
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label for="longitude"><?= __('Longitude:') ?></label>
				<input type="text" name="longitude" value="<?= htmlspecialchars_decode(set_value('longitude', $poi->longitude), ENT_NOQUOTES) ?>" id="longitude">
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('poi/venue/'.$venue->id) ?>" class="delete"><?= __('Cancel') ?></a>
				<button type="submit" class="save"><?= __('Save') ?></button>
			</div>
		</form>
		<form action="#" method="post" accept-charset="utf-8" class="edit" onsubmit="return false;">
			<div>
				<span class="note" style="margin-left:140px; display:block;">
					<?= __('Enter the POI\'s location and you\'ll automatically get the coordinates.<br />
					If the location isn\'t correct, you can always drag the marker to a more precise location.') ?>
				</span>
				<p>
					<label for="address"><?= __('Address/location') ?></label>
					<input id="address" type="textbox" value="" onblur="codeAddress()">
				</p>
				<div id="map_canvas" class="map" style="height:300px;"></div>
			</div>
			
			<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
			<script type="text/javascript">
				var geocoder;
				var map;
				var bounds;
				var marker_old;
				var marker;
				
				function codeAddress() {
					var address = document.getElementById('address').value;
					geocoder.geocode( { 'address': address}, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							marker.setMap(null);
							map.setCenter(results[0].geometry.location);
							marker = new google.maps.Marker({
								map: map, 
								position: results[0].geometry.location
							});
							
							$('#latitude').val(results[0].geometry.location.lat());
							$('#longitude').val(results[0].geometry.location.lng());
							
							marker.draggable = true;
							google.maps.event.addListener(marker, 'drag', function() {
								$('#latitude').val(marker.getPosition().lat());
								$('#longitude').val(marker.getPosition().lng());
							}); 
							
							bounds = new google.maps.LatLngBounds();
							bounds.extend(results[0].geometry.location);
							map.fitBounds(bounds);
							map.setZoom(map.getZoom()-1);
						} else {
							if(status === 'ZERO_RESULTS'){
								alert('<?= __('No results found on the address ...') ?>');
							} else {
								alert("<?= __('Geocode was not successful for the following reason:') ?>" + status);
							}
						}
					});
				}
				
				$(document).ready(function() {
					geocoder = new google.maps.Geocoder();
					var latlng = new google.maps.LatLng(<?= $poi->latitude ?>, <?= $poi->longitude ?>);
					var myOptions = {
						zoom: 8,
						center: latlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					}
					map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
					bounds = new google.maps.LatLngBounds();
					
					map.setCenter(latlng);
					marker = new google.maps.Marker({
						map: map, 
						position: latlng
					});
					
					marker.draggable = true;
					google.maps.event.addListener(marker, 'drag', function() {
						$('#latitude').val(marker.getPosition().lat());
						$('#longitude').val(marker.getPosition().lng());
					}); 
					
					bounds.extend(latlng);
					map.setZoom(16);
					
					$('#address').keyup(function(e) {
						if(e.keyCode == 13) {
							codeAddress();
							$("form").submit(function() {
								return false;
							});
						}
					});
					
				});
				
			</script>
		</form>
	</div>
</div>