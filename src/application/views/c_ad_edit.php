<div>
	<h1><?=__('Edit %s', $ad->name)?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
            <?php if(isset($_GET['error']) && $_GET['error'] == 'image'): ?>
            <div class="error"><p><?= __('Something went wrong with the image upload. <br/> Maybe the image size exceeds the maximum width or height') ?></p></div>
            <?php endif ?>
            <p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
                <label for="name"><?= __('Name:') ?></label>
                <input type="text" name="name" id="name" value="<?= set_value('name', $ad->name) ?>"/>
            </p>
			<p <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
				<label for="image"><?= __('Image:') ?></label>
				<span class="hintAppearance"><?= __('Required size: 640x100') ?></span>
				<span class="evtlogo" <?php if($ad->image != ''){ ?>style="background:transparent url('<?= image_thumb($ad->image, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="image" id="image" value="" class="logoupload" /><br clear="all"/>
				<?php $type = (isset($venue)) ? 'venue' : 'event';?>
                <?php if($ad->image != null && $ad->image != '') : ?>
                <?php if($ad->image != '' && file_exists($this->config->item('imagespath') . $ad->image)){ ?><a href="<?= site_url('ad/crop/'.$ad->id . '/' .$type) ?>"><?= __('Edit image') ?></a><?php } ?>
                <?php endif; ?>
			</p><br />
			<p <?= (form_error("image_ipad") != '') ? 'class="error"' : '' ?>>
				<label for="image_ipad"><?= __('Image iPad:') ?></label>
				<span class="hintAppearance"><?= __('Required size: 500x200') ?></span>
				<span class="evtlogo" <?php if($ad->image_ipad != ''){ ?>style="background:transparent url('<?= image_thumb($ad->image_ipad, 50, 50) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="image_ipad" id="image_ipad" value="" class="logoupload" /><br clear="all"/>
				<?php $type = (isset($venue)) ? 'venue' : 'event';?>
			</p><br />
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label for="order"><?= __('Order:') ?></label>
                <input type="text" name="order" id="order" value="<?= set_value('order', $ad->order) ?>"/>
            </p>
			<p <?= (form_error("time") != '') ? 'class="error"' : '' ?>>
                <label for="time"><?= __('Display time (In Seconds):') ?></label>
                <input type="text" name="time" id="order" value="<?= set_value('time', $ad->time) ?>"/>
            </p>
			<p <?= (form_error("website") != '') ? 'class="error"' : '' ?>>
                <label for="website"><?= __('Website:') ?></label>
                <input type="text" name="website" id="website" value="<?= set_value('website', $ad->website) ?>"/>
            </p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= ("javascript:history.go(-1);"); ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>