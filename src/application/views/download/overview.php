<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="en-us" />
	<base href="<?= base_url() ?>" />

	<title>TapCrowd - <?= $app->name ?></title>
	
	<style type="text/css" media="screen">
	
		* { margin:0; padding:0; }
		body { margin:0; color:#222222; background:#F6F5F5; font-family:"Lucida Grande","Lucida Sans",Arial,Verdana,Sans-Serif; font-size:12px; line-height:22px; }
		
		h1 { font-size:15px; font-weight:bold; border-bottom:1px dotted #CCC; margin-bottom:10px; padding:0 0 6px 0; }
		h2 { font-size:13px; display:block; border-bottom:1px dotted #CCC; margin-bottom:10px; padding:0 0 6px 0; }
		
		a { color:#000; border-bottom:1px dotted #666; text-decoration:none; }
		a:hover {  }
		
		div#wrapper { background:#FFF; margin:15px auto; width:580px; }
		div#wrapper #header { padding:15px 40px; background:#222; }
		div#wrapper #header span { color:#FFF; font-size:23px; display:inline-block; float:right; margin-top:17px; }
		div#wrapper #content { padding:15px; }
		
	</style>
	
</head>

<body>
	<div id="wrapper">
		<div id="header">
			<img src="img/logo.png" width="190" height="58" alt="TapCrowd Logo">
			<span><?= $app->name?></span>
		</div>
		<div id="content">
			<h1>Stores</h1>
			<?php if ($app->appleappstore != '') { ?>
				<a href="<?= $app->appleappstore ?>">Apple App Store</a><br />
			<?php } ?>
			<?php if ($app->androidmarket != '') { 
					$android = explode('pname:', $app->androidmarket);
			?>
				<a href="http://market.android.com/details?id=<?= $android[1] ?>">Android Market</a><br />
			<?php } ?>
			<?php if ($app->mobwebsite != '') { ?>
				<a href="<?= $app->mobwebsite ?>">Mobile website</a> 
			<?php } ?>
		</div>
	</div>
</body>
</html>
