<div>
	<h1><?= __('Edit Module')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="title"><?= __('Title') ?> <?= '(' . $language->name . ')'; ?>:</label>
                <?php $trans = _getTranslation('contentmodule', $contentmodule->id, 'title', $language->key); ?>
                <input maxlength="255" type="text" name="title_<?= $language->key; ?>" id="title" value="<?= set_value('title_'.$language->key, ($trans != null) ? $trans : $contentmodule->title) ?>" />
            </p>
            <?php endforeach; ?>
            <p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
                <label><?= __('Order:') ?></label>
                <input type="text" name="order" value="<?= set_value('order', $contentmodule->order) ?>" id="order">
            </p>
            <p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
                <label for="imageurl"><?= __('Image:') ?></label>
                <span class="evtlogo" <?php if($contentmodule->icon != '' && file_exists($this->config->item('imagespath') . $contentmodule->icon)){ ?>style="width:50px; height:50px; background:transparent url('<?= image_thumb($contentmodule->icon, 50, 50) ?>') no-repeat center center;"<?php } ?>>&nbsp;</span>
                <input type="file" name="imageurl" id="imageurl" value="" /><br />
                <span class="note" style="width:200px;"><?= __('Max size %s px by %s px',2000,2000) ?></span>
                <br clear="all" />
            </p>
            <div class="row">
                <label for="tags"><?= __('Tags:') ?></label>
                <ul id="mytags" name="mytagsul"></ul>
            </div>
<!--             <p <?= (form_error("hideinmenu") != '') ? 'class="error"' : '' ?>>
                <label for="hideinmenu" style="float:left;width:150px;"><?= __('Hide in home menu') ?></label>
                <input class="checkboxClass" type="checkbox" style="float:left; display:inline;" name="hideinmenu" value="true" <?= set_value('hideinmenu', $contentmodule->hideinmenu) == 1 ? 'checked=checked' : '' ?> /><br clear="all" />
                <label class="hintAppearance"><?= __('If checked, this module will not be shown as an icon on the home menu.') ?><br/>
                    <?= __('It is still possible to link to this module from within other modules') ?></label>
            </p>
            <p <?= (form_error("homemodule") != '') ? 'class="error"' : '' ?>>
                <label for="homemodule" style="float:left;width:150px;"><?= __('Set as home module') ?></label>
                <input class="checkboxClass" type="checkbox" style="float:left; display:inline;" name="homemodule" value="true" <?= set_value('homemodule', $contentmodule->homemodule) == 1 ? 'checked=checked' : '' ?> /><br clear="all" />
                <label class="hintAppearance"><?= __('This option can only be set for one module. If set, this module will replace the home menu with icons.') ?></label>
            </p> -->
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
                <a href="<?= ("javascript:history.go(-1);"); ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Edit Module') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $("#mytags").tagit({
            initialTags: [<?php if(isset($tags) && $tags != false){foreach($tags as $val){ echo '"'.$val->tag.'", '; }} ?>],
        });

        var tags = new Array();
        var i = 0;
        <?php foreach($apptags as $tag) : ?>
        tags[i] = '<?=$tag->tag?>';
        i++;
        <?php endforeach; ?>
        $("#mytags input.tagit-input").autocomplete({
            source: tags
        });
    });
</script>