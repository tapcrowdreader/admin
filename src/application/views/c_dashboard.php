<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<style type="text/css">
		* {
			padding:0;
			margin:0;
		}
	</style>
	<title><?=__('Dashboard')?></title>
	<link rel="stylesheet" href="<?= site_url('css/bootstrap.css') ?>" type="text/css" />
	<?php
	#
	# Load jQuery : Decide jQuery version to use
	#
	$jquery_version = (ENVIRONMENT == 'production')? '1.7.2/jquery.min.js' : '1.7.2/jquery.js';?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/<?=$jquery_version?>"></script>
	<style type="text/css">
		body {
			background-color: #eaedf1;
			background-image: none !important;
		}

		h1 {
			margin-bottom:10px;
		}

/*		#header {
			background-color: #353535;
			width:100%;
			display: block;
			height:50px;
		}

		#headerLine {
			height:5px;
			background-color: #15a8da;
			width:100%;
			margin-bottom:10px;
		}*/

		#wrapper {
			margin: 65px auto;
			width:1020px;
		}

		#header #logo {
		  display: block;
		  height: 47px;
		  padding-top: 1px;
		  width: 221px;
		  margin-top:10px;
		  margin-left:5px;
		}

		#header {
		  left: 0;
		  margin-bottom: 0;
		  position: fixed;
		  right: 0;
		  z-index: 1030;
		  color: #555555;
		  box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 10px rgba(0, 0, 0, 0.1);
		  -webkit-box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 10px rgba(0, 0, 0, 0.1);
		  border-radius: 0 0 0 0;
		  padding-left: 0;
		  padding-right: 0;
		  background: #51575F;
		  background-image: -moz-linear-gradient(center top , #51575F, #0B1018);
		  background-image: -webkit-linear-gradient(top , #51575F, #0B1018);
		  background-image: -webkit-linear-gradient(top, #51575F 0%, #0B1018 100%);
		  -ms-filter: "progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#4E4E4E', endColorstr='#2E2E2E')";
		  background: -ms-linear-gradient(#51575F, #0B1018);
		  background-repeat: repeat-x;
		  height:61px;
		}

		#header #logo a {
		border: none;
		}

		#navigation li a {
		    border-left: 1px solid #222222;
		    color: #EEEEEE;
		    padding-top: 20px;
		    padding-bottom: 20px;
		    text-shadow: none;
		}

		[class*=" icon-"] {
		    background-image: url("../img/layout2/glyphicons-halflings-white.png");
		    background-repeat: no-repeat;
		    display: inline-block;
		    height: 14px;
		    line-height: 14px;
		    margin-top: 1px;
		    vertical-align: text-top;
		    width: 14px;
		}

		#header #navigation {
		    -moz-border-bottom-colors: none;
		    -moz-border-left-colors: none;
		    -moz-border-right-colors: none;
		    -moz-border-top-colors: none;
		    display: inline;
		    height: 61px;
		    padding-right: 2px;
		    position: absolute;
		    right: 0;
		    top: 0;
		    border:none;
		    padding-right:0px;
		}
		#header #navigation li {
		    color: #FFFFFF;
		    display: inline;
		    float: left;
		    height: 61px;
		    list-style: none outside none;
		    width: 100px;
		    border-left:1px solid #000000;
		}
		#header #navigation li a {
		    -moz-border-bottom-colors: none;
		    -moz-border-left-colors: none;
		    -moz-border-right-colors: none;
		    -moz-border-top-colors: none;
		    background: none repeat scroll 0 0 transparent;
		    display: block;
		    height: 20px;
		    width:auto;
		    text-align:center;
		    border:none;
		}
		#header #navigation li a.last {
		    border:none;
		    border-radius:none;
		}

		#languageSelect li{
			float:left;
			margin-left:0.8em;
			list-style-type:none;
			height:20px;
		}

		#languageSelect a{
			text-indent:0px;
			display:block;
			color:#fff;
			border:none;
			font-size:11px;
			text-decoration:none;
		}

		#languageSelect .selected a{
			color: #26A8E0;
			text-decoration:underline;
		}

		ul#languageSelect{
			position: absolute;
			left:200px;
			top: 24px;
			margin:0;
			padding:0;
		}

		.contactusbtn {
		    color: #EEEEEE;
		    cursor: pointer;
		    font-size: 13px;
		    padding: 9px 17px 10px 16px;
		    text-decoration: none;
		    text-shadow: 0 -1px 1px rgba(0, 0, 0, 0.25);
		    float:left;
		    margin-top:10px;
		    margin-right:10px;
		    width:85px;
		    height:22px;
		    background-image: url('../img/button-contact-us.png');
		    border-bottom:none;
		}

		.contactusbtnicon {
		  margin-right:2px;
		  margin-top:2px;
		}


		#div1, #div2, #div3, #div4, #div5, #div6 {
			background-color: #FFFFFF;
			padding:20px;
			margin-bottom:20px;
			min-height:150px;
		}

		#div4 {
			background-color: #71c387;
			color: #FFFFFF;
			width:660px;
			margin-right:20px;
			min-height:161px;
		}

		#div5 {
			background-color: #de6165;
			color: #FFFFFF;
		}

		.greyText {
			color: #999999;
		}

		.subdiv {
			text-align: center;
			padding:5px;
		}

		.marginbottom10 {
			margin-bottom:10px;
		}

		.manageText, .manageText a {
			color:#000000;
			font-weight: bold;
		}

		#div2 {
			background-color: #15a8da;
			color: #FFFFFF;
		}

		.fontExtraLarge {
			font-size: 50px;
			line-height: 50px;
			vertical-align: middle;
		}

		.fontLarge {
			font-size: 20px;
			line-height: 30px;
			vertical-align: middle;
		}

		.textLeft {
			text-align: left;
		}

		#div1 .subdiv, #div3 .subdiv, #div6 .subdiv {
			background-color: #f9f9f9;
		}

		#div5 .subdiv {
			background-color: #d65c5c;
			min-height:105px;
		}

		#div2 .subdiv {
			background-color: #37b4e4;
		}

		.toprightArrow {
			float: right;
			margin-right: -20px;
			margin-top: -20px;
		}

		#div2 a, #div4 a, #div5 a {
			color: #FFFFFF;
		}

		.analyticsApp {
			font-size:12px;
		}

		#div1 .subdiv { 
			min-height:120px;
		}
		#div3 .subdiv, #div6 .subdiv {
			min-height:100px;
		}

		.icon {
			margin-bottom:5px;
		}

		#div1, #div2, #div3, #div6 {
			clear:both;
		}

		#div4 {
			clear:left;
		}

		#div5 {
			clear:right;
		}

	</style>
	<?php if($this->channel->templatefolder != ''){ ?>
		<link rel="stylesheet" href="../templates/<?= $this->channel->templatefolder ?>/css/master.css" type="text/css" />
	<?php } ?>
	<!-- IE STYLESHEET -->
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" type="text/css" /><![endif]-->
</head>
<body>
	<?php if(isset($_GET['welcome'])) : ?>
<!-- 	<div id="welcome">
		<p>Welcome to TapCrowd.</p>
	</div> -->
	<?php endif; ?>

	<?php $isAdmin = _isAdmin(); ?>
	<div id="header">
		<div id="logo">
			<?php
			if(!empty($this->channel->templatefolder)) {
				$src = 'templates/'.$this->channel->templatefolder.'/img/header_logo.png'; ?>
				<a href="<?=base_url()?>"><img src="<?=site_url($src)?>" class="png" height="35" style="padding-top:5px;"/></a><?php
			} elseif($this->templatefolder != '') { ?>
					<?php if(file_exists("templates/".$this->templatefolder."/img/header_logo.png")) : ?>
					<a href="<?= base_url()?>"><img src="templates/<?=$this->templatefolder?>/img/header_logo.png" class="png" height="35" style="padding-top:5px;"/></a>
					<?php else: ?>
					<a href="<?= base_url() ?>"><img src="img/layout2/logo-tapcrowd.png" class="png" /></a>
				<?php endif; ?>
			<?php } else { ?>
				<a href="<?= base_url() ?>"><img src="img/layout2/logo-tapcrowd.png" class="png" /></a>
			<?php } ?>
		</div>

		<ul id="languageSelect"><?php
		$currentLang = $this->input->cookie('language', true);
		foreach(getAvailableLanguages() as $lang) {
			if($lang == 'en' || $lang == 'pt' || $lang == 'nl') {
			$class = (($lang == $currentLang)? 'selected locale_' : 'locale_') . $lang;  ?>
			<li class="<?= $class ?>"><a href="" hreflang="<?= $lang ?>"><?= $lang ?></a></li><?php
			}
		} ?>
		</ul>

		<ul id="navigation">
			<a href="<?= site_url('contactus')?>" class="contactusbtn"><span class="icon-envelope icon-white contactusbtnicon"> &nbsp;</span><span><?= __('Contact us') ?></span></a>
			<?php
			if($this->session->userdata('name') != "") {
				# Get locations
				if($this->channel->hideMyAccount != 1) $account_url = \Tapcrowd\API::getLocation('','account');
				if($isAdmin) $admin_url = \Tapcrowd\API::getLocation('admin','admin');
				$logout_url = \Tapcrowd\API::getLocation('auth/logout','admin');

				# New account controller
				$account_url = \Tapcrowd\API::getLocation('account'); ?>

				<!-- <li><a href="" title="Dashboard"><i class="icon-home icon-white"></i> <?=__('Dashboard');?></a></li> -->
				<li><a href="<?=site_url('apps/myapps')?>" title="Apps"><i class="icon-th-large icon-white"></i> <?=__('My Apps');?></a></li><?php
				if(!empty($account_url)) { ?>
					<li><a href="<?= $account_url ?>"><i class="icon-user icon-white"></i> <?=__('Profile');?></a></li><?php
				}
				if(!empty($admin_url)) { ?>
					<li><a href="<?= site_url('admin') ?>"><i class="icon-user icon-white"></i> <?=__('Admin');?></a></li><?php
				} ?>
				<li><a href="<?=$logout_url?>" class="last"><i class="icon-off icon-white"></i> <?=__('Logout');?></a></li>
			<?php } ?>
		</ul>
		<?php if(stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 7') || stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') || stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 5')) : ?>
		<div id="ienotification">
			<?= __("TapCrowd doesn't support older versions of Internet Explorer. We advise you to use <a href=\"http://www.mozilla.org/nl/firefox/\" target=\"_blank\">Mozilla Firefox</a> or <a href=\"http://www.google.com/chrome/\" target=\"_blank\">Google Chrome</a> or update your browser to the latest version.") ?>
		</div>
		<?php endif; ?>
	</div>
	<br clear="all" />
	<div id="wrapper">
	<div class="container-fluid">
		<div class="">
			<div id="div1" class="span12">
				<div class="toprightArrow">
					<a href="<?= site_url('apps/myapps') ?>">
						<img src="<?= site_url('img/icons-dashboard/arrows/arrow-blue-white.png') ?>" />
					</a>
				</div>
				<h1><?=__('Manage Mobile Apps')?></h1>
				<p class="greyText marginbottom10">Build, publish &amp; maintain mobile native apps and HTML 5 web apps</p>
				<div class="row-fluid">
					<div class="span2 subdiv">
						<a href="<?=site_url('apps/myapps')?>">
							<img src="<?= site_url('img/icons-dashboard/manage-apps.png') ?>" class="icon" />
							<p class="manageText">Manage my apps</p>
							<p class="manageSubText greyText">Apps &amp; App store submissions</p>
						</a>
					</div>
					<div class="span2 subdiv">
						<a href="<?=site_url('apps/add')?>">
							<img src="<?= site_url('img/icons-dashboard/build-app-from-template.png') ?>" class="icon" />
							<p class="manageText">Build app from template</p>
							<p class="manageSubText greyText">Native + Web Apps</p>
						</a>
					</div>
					<div class="span2 subdiv">
						<a href="<?=site_url('apps/add')?>">
							<img src="<?= site_url('img/icons-dashboard/import-existing-app.png') ?>" class="icon" />
							<p class="manageText">Import existing app</p>
						</a>
					</div>
					<div class="span2 subdiv">
						<a href="<?=site_url('apps/add?apptypeid=12')?>">
							<img src="<?= site_url('img/icons-dashboard/new-container-app.png') ?>" class="icon" />
							<p class="manageText">New container app</p>
							<p class="manageSubText greyText">For HTML5 apps + use proxy</p>
						</a>
					</div>
					<div class="span2 subdiv">
						<a href="<?=site_url('contactus')?>">
							<img src="<?= site_url('img/icons-dashboard/request-code.png') ?>" class="icon" />
							<p class="manageText">Request Quote</p>
							<p class="manageSubText greyText">For custom apps</p>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="">
			<div id="div2" class="span12">
				<div class="toprightArrow">
					<a href="<?='http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']) . '/campaigns'?>">
						<img src="<?= site_url('img/icons-dashboard/arrows/arrow-white-blue.png') ?>" />
					</a>
				</div>
				<h1><?=__('Analytics')?></h1>
				<p class="marginbottom10">Get insight in app downloads, app usage, real-time results of campaigns</p>
				<div class="row-fluid">
					<?php foreach($latestApps as $app) : ?>
						<div class="span2 subdiv">
							<a href="<?= site_url('analytics/app/'.$app->id) ?>">
								<img src="<?= site_url('img/icons-dashboard/analytics.png') ?>" class="icon" />
								<?php if(strlen($app->name) > 18) : ?>
								<p><?= substr($app->name, 0, 18).'...' ?></p>
								<?php else: ?>
								<p><?= $app->name ?></p>
								<?php endif; ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>

		<div class="">
			<div id="div3" class="span12">
				<div class="toprightArrow">
					<a href="<?= 'http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']) . '/campaigns' ?>">
						<img src="<?= site_url('img/icons-dashboard/arrows/arrow-blue-white.png') ?>" />
					</a>
				</div>
				<h1><?=__('Manage Mobile Campaigns')?></h1>
				<p class="greyText marginbottom10">Build campaigns with push notifications, location &amp; behavioural targeting</p>
				<div class="row-fluid">
					<div class="span3 subdiv">
						<a href="<?= 'http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']) . '/campaigns'?>">
							<img src="<?= site_url('img/icons-dashboard/manage-campaigns.png') ?>" class="icon" />
							<p class="manageText">Manage my campaigns</p>
						</a>
					</div>
					<div class="span3 subdiv">
						<a href="<?= 'http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']) . '/campaignsettings'?>">
							<img src="<?= site_url('img/icons-dashboard/add-campaigns.png') ?>" class="icon" />
							<p class="manageText">Add Campaign</p>
						</a>
					</div>
					<div class="span3 subdiv textLeft">
						<p class="manageText">
							<a href="<?= 'http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']) . '/segment'?>">
							Add Segment<br />
							</a>
							<a href="<?= 'http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']) . '/geofence'?>">
								Add Geofence<br />
							</a>
							<a href="<?= 'http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']) . '/schedule'?>">
								Add Schedule<br />
							</a>
							<a href="<?= 'http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']) . '/myaction'?>">
								Add Action (Push Notification)
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="">
			<div id="div4" class="span9">
				<div class="toprightArrow">
					<a href="http://tapcrowd.com/resources/demo-movies/" target="_blank">
						<img src="<?= site_url('img/icons-dashboard/arrows/arrow-white-green.png') ?>" />
					</a>
				</div>
				<h1><?=__('Knowledge Center')?></h1>
				<p class="marginbottom10">Learn about mobile apps &amp; mobile marketing</p>
				<div class="row-fluid">
					<div class="span4 subdiv textLeft">
						<h4>For Marketeers</h4>
						<p class="">
							<a href="http://tapcrowd.com/resources/demo-movies" target="_blank">
								Getting Started<br />
							</a>
							<a href="">
								App Idea Center (coming soon)<br />
							</a>
							<a href="http://tapcrowd.com/resources/webinars" target="_blank">
								Webinars
							</a>
						</p>
					</div>
					<div class="span4 subdiv textLeft">
						<h4>For Developers</h4>
						<p class="">
							<a href="http://confluence.tapcrowd.com/display/dev/TapCrowd+API+and+SDK" target="_blank">
								TapCreator API + SDK<br />
							</a>
							<a href="http://confluence.tapcrowd.com/display/dev/TapTarget" target="_blank">
								TapTarget SDK<br />
							</a>
							<a href="http://confluence.tapcrowd.com/display/dev/Home" target="_blank">
								Developer Portal
							</a>
						</p>
					</div>
					<div class="span4 subdiv textLeft">
						<h4>For Partners</h4>
						<p class="">
							<?php if($account->ispartner == 1) : ?>
								<a href="http://tapcrowd.box.com/s/904233e77005ced349fd" target="_blank">
									Brochures<br />
								</a>
								<a href="http://tapcrowd.box.com/s/904233e77005ced349fd" target="_blank">
									Slides<br />
								</a>
								<a href="http://tapcrowd.box.com/s/904233e77005ced349fd" target="_blank">
									Proposals
								</a>
							<?php else: ?>
								Brochures (only for partners)<br />
								Slides (only for partners)<br />
								Proposals (only for partners)
							<?php endif; ?>
						</p>
					</div>
				</div>
			</div>
			<div id="div5" class="span3">
				<div class="toprightArrow">
					<a href="<?= site_url('contactus') ?>" target="_blank">
						<img src="<?= site_url('img/icons-dashboard/arrows/arrow-white-red.png') ?>" class="icon" />
					</a>
				</div>
				<h1><?=__('Get Support')?></h1>
				<p class="marginbottom10">Contact our helpdesk</p>
				<div class="row-fluid">
					<div class="span6 subdiv">
						<a href="<?= site_url('contactus') ?>" target="_blank">
							<img src="<?= site_url('img/icons-dashboard/white-contact-us.png') ?>" class="icon" />
							<p class="">Contact Us</p>
						</a>
					</div>
					<div class="span6 subdiv">
						<a href="http://jira.tapcrowd.com" target="_blank">
							<img src="<?= site_url('img/icons-dashboard/white-support-tickets.png') ?>" class="icon" />
							<p class="">Support Tickets <?php if(!$isAdmin) { echo '<br />(only for admins)'; } ?></p>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="">
			<div id="div6" class="span12">
				<div class="toprightArrow">
					<a href="<?= site_url('admin') ?>">
						<img src="<?= site_url('img/icons-dashboard/arrows/arrow-blue-white.png') ?>" />
					</a>
				</div>
				<h1><?=__('Administration')?></h1>
				<p class="greyText marginbottom10">Manage users, app templates, app themes <?php if(!$isAdmin) { echo '<strong>(only for admins)</strong>'; } ?></p>
				<?php if($isAdmin) : ?>
				<div class="row-fluid">
					<div class="span3 subdiv">
						<a href="<?= site_url('admin/users') ?>">
							<img src="<?= site_url('img/icons-dashboard/manage-users.png') ?>" class="icon" />
							<p class="manageText">Manage users</p>
						</a>
					</div>
					<div class="span3 subdiv">
						<a href="<?= site_url('admin/users') ?>">
							<img src="<?= site_url('img/icons-dashboard/user-roles.png') ?>" class="icon" />
							<p class="manageText">Manage user roles</p>
						</a>
					</div>
					<div class="span3 subdiv">
						<a href="<?= site_url('admin/formtemplate/forms') ?>">
							<img src="<?= site_url('img/icons-dashboard/add-templates-and-modules.png') ?>" class="icon" />
							<p class="manageText">App templates &amp; modules</p>
						</a>
					</div>
					<div class="span3 subdiv">
						<a href="<?= site_url('admin/theme') ?>">
							<img src="<?= site_url('img/icons-dashboard/app-themes.png') ?>" class="icon" />
							<p class="manageText">App Themes</p>
						</a>
					</div>
				</div>
				<?php else: ?>
				<div class="row-fluid">
					<div class="span3 subdiv">
						<img src="<?= site_url('img/icons-dashboard/manage-users.png') ?>" />
						<p class="manageText">Manage users</p>
					</div>
					<div class="span3 subdiv">
						<img src="<?= site_url('img/icons-dashboard/user-roles.png') ?>" />
						<p class="manageText">Manage user roles</p>
					</div>
					<div class="span3 subdiv">
						<img src="<?= site_url('img/icons-dashboard/add-templates-and-modules.png') ?>" />
						<p class="manageText">App templates &amp; modules</p>
					</div>
					<div class="span3 subdiv">
						<img src="<?= site_url('img/icons-dashboard/app-themes.png') ?>" />
						<p class="manageText">App Themes</p>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>	
	</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
<!-- Google Analytics UA -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44893075-1', 'tapcrowd.com');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics -->
</body>
</html>
