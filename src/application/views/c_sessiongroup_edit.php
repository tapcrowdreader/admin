<div>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<h1><?= __('Edit Sessiongroup')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<?php if(!isset($venues)): ?>
				<p <?= (form_error("events") != '') ? 'class="error"' : '' ?> style="display:none;">
					<label><?= __('Event')?></label>
					<select name="events" id="events">
						<option value=""><?= __('Select ...')?></option>
						<?php foreach ($events as $evt): ?>
							<option value="<?= $evt->id ?>" <?= set_select('events', $evt->id, ($sessiongroup->eventid == $evt->id ? TRUE : '')) ?>><?= $evt->name ?></option>
						<?php endforeach ?>
					</select>
				</p>
			<?php else: ?>
				<p <?= (form_error("venues") != '') ? 'class="error"' : '' ?> style="display:none;">
					<label><?= __('Venues')?></label>
					<select name="venues" id="venues">
						<option value=""><?= __('Select ...')?></option>
						<?php foreach ($venues as $vnu): ?>
							<option value="<?= $vnu->id ?>" <?= set_select('venues', $vnu->id, ($sessiongroup->venueid == $vnu->id ? TRUE : '')) ?>><?= $vnu->name ?></option>
						<?php endforeach ?>
					</select>
				</p>
			<?php endif; ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="name"><?= __('Name '. '(' . $language->name . ')'); ?>:</label>
				<?php $trans = _getTranslation('sessiongroup', $sessiongroup->id, 'name', $language->key); ?>
                <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?= htmlspecialchars_decode(set_value('name_'.$language->key, ($trans != null) ? $trans : $sessiongroup->name ), ENT_NOQUOTES) ?>" />
            </p>
            <?php endforeach; ?>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order', $sessiongroup->order) ?>" id="order">
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('sessiongroups/event/'.$event->id) ?>" class="btn"><?= __('Cancel')?></a>
				<button type="submit" class="btn primary"><?= __('Save')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>