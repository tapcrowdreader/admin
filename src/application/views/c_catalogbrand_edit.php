<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
	<?php if($error != ''): ?>
	<div class="error"><?= $error ?></div>
	<?php endif ?>
	<fieldset>
		<h3><?= __('Edit Brand') ?></h3>
		<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
			<label for="name"><?= __('Name:') ?></label>
			<input type="text" name="name" id="name" value="<?= htmlspecialchars_decode(set_value('name', $brand->name), ENT_NOQUOTES) ?>" />
		</p>
	</fieldset>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
        <?php if(isset($event)) : ?>
		<a href="<?= site_url('catalogbrands/event/'.$event->id) ?>" class="btn"><?= __('Cancel') ?></a>
        <?php elseif(isset($venue)) : ?>
        <a href="<?= site_url('catalogbrands/venue/'.$venue->id) ?>" class="btn"><?= __('Cancel') ?></a>
        <?php endif; ?>
		<button type="submit" class="btn primary"><?= __('Save') ?></button>
		<br clear="all" />
	</div>
	<br clear="all" />
</form>