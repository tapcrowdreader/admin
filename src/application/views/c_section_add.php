<div>
	<h1><?= __('Add Section')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>          
			<?php foreach($languages as $language) : ?>
			<p <?= (form_error("title_".$language->key) != '') ? 'class="error"' : '' ?>>
				<label for="title"><?= __('Title '. '(' . $language->name . ')'); ?>:</label>
				<input maxlength="255" type="text" name="title_<?= $language->key; ?>" id="title" value="<?=set_value('title_'.$language->key)?>" />
			</p>
			<?php endforeach; ?>
			<p> 
				<label><?= __('Section type:')?> </label>
				<select name="sectiontype" id="sectiontype" onChange="showlabelsf()">
					<?php foreach($sectiontypes as $st) : ?>
						<option value="<?=$st->id?>"><?=ucfirst($st->name)?></option>
					<?php endforeach; ?>
				</select>
			</p>
			<div class="row">
				<label for="tags"><?= __('Show items with following tag(s):')?></label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<p <?= (form_error("maxitems") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Max. items to show:')?></label>
				<input type="text" name="maxitems" value="<?= set_value('maxitems', 10) ?>" id="order">
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order') ?>" id="order">
			</p>
			<p id="showlabelsp" <?= (form_error("showlabels") != '') ? 'class="error"' : '' ?> style="display:none;">
				<label><?= __('Show labels with icon:')?></label>
				<input class="checkboxClass" type="checkbox" name="showlabels" /> <br />
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= ("javascript:history.go(-1);"); ?>" class="btn"><?= __('Cancel')?></a>
				<button type="submit" class="btn primary"><?= __('Add Section')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
	$("#mytags").tagit({
		initialTags: [<?php if(set_value('item[tags][]')){foreach(set_value('item[tags][]') as $val){ echo '"'.$val.'", '; }} ?>],
	});

	var tags = new Array();
	var i = 0;
	<?php foreach($apptags as $tag) : ?>
	tags[i] = '<?=$tag->tag?>';
	i++;
	<?php endforeach; ?>
	$("#mytags input.tagit-input").autocomplete({
		source: tags
	});
});

function showlabelsf() {
	if($("#sectiontype").val() == 2) {
		$('#showlabelsp').css('display', 'block');
	} else {
		$('#showlabelsp').css('display', 'none');
	}
}

$('#title').blur(function() {
	$("#mytags").tagit("add", $('#title').val(), $('#title').val());
});
</script>