<div>
	<h1><?=__('Edit User')?></h1>
	<div class="frmformbuilder">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?><div class="error"><?= $error ?></div><?php endif ?>
			<p <?= (form_error("Name") != '') ? 'class="error"' : '' ?>>
				<label for="title"><?= __('Name') ?>:</label>
				<input type="text" name="name" id="name" value="<?=set_value('name', $user->name)?>" />
			</p>
			<?php if(externalIds()) : ?>
			<p <?= (form_error("external_id") != '') ? 'class="error"' : '' ?>>
				<label for="external_id"><?= __('External id:') ?></label>
				<input type="text" name="external_id" id="external_id" value="<?= htmlspecialchars_decode(set_value('external_id', $user->external_id), ENT_NOQUOTES) ?>" />
			</p>
			<?php endif; ?>
			
			<?php if($pincodes == 0) : ?>
			<p <?= (form_error("login") != '') ? 'class="error"' : '' ?>>
				<label for="title"><?= __('Login') ?>:</label>
				<input type="text" name="login" id="login" value="<?=set_value('login', $user->login)?>" readonly="yes" disabled/>
			</p>
			
			<p <?= (form_error("Password") != '') ? 'class="error"' : '' ?>>
				<label for="title"><?= __('Password') ?>:</label>
				<input type="password" name="password" id="password" value="" />
			</p>
			<?php endif; ?>

<!-- 			<p <?= (form_error("Repassword") != '') ? 'class="error"' : '' ?>>
				<label for="title"><?= __('Re. Password') ?>:</label>
				<input type="password" name="repassword" id="repassword" value="" />
			</p> -->
			<?php if($pincodes == 1) : ?>
				<p <?= (form_error("pincode") != '') ? 'class="error"' : '' ?>>
					<label for="title"><?= __('Pincode') ?>:</label>
					<input type="text" name="pincode" id="login" value="<?=set_value('pincode', $user->login)?>"/>
				</p>
			<?php endif; ?>

			<p <?= (form_error("Email") != '') ? 'class="error"' : '' ?>>
				<label for="title"><?= __('Email') ?>:</label>
				<input type="text" name="email" id="email" value="<?=set_value('email', $user->email)?>" />
			</p>
            <?php if(!empty($securedmodules) && !in_array($securedmodules, array(array('event'), array('venue')))) : ?>
            <p <?= (form_error("modules") != '') ? 'class="error"' : '' ?>>
                <label><?= __('Access to which modules?')?></label>
                <?php foreach($securedmodules as $m) : ?>
                <input class="checkboxClass" type="checkbox" name="modules[]" value="<?=$m->launcherid?>" <?= in_array($m->launcherid, $usersecuredmoduleids) ? 'checked="checked"' : '' ?> /><?= $m->title ?> <br />
                <?php endforeach; ?> 
            </p>
            <?php endif; ?>

			<?php if($meetme && !empty($attendees)) : ?>
			<p> 
				<label><?= __('Link to attendee:') ?></label>
				<select name="attendee" id="attendee">
					<option value="0"></option>
				<?php foreach($attendees as $a) : ?>
					<option value="<?= $a->id ?>" <?= $user->attendeeid == $a->id ? 'selected="selected"' : '' ?>><?= $a->name . ' ' . $a->firstname ?></option>
				<?php endforeach; ?>
				</select>
			</p>
			<?php endif; ?>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url($cancelbtnurl) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Update') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>