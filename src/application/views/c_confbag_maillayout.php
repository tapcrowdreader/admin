<div>
	<h1><?=__('Layout conference bag mail')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<?php foreach($languages as $language) : ?>
			<p <?= (form_error("subject_".$language->key) != '') ? 'class="error"' : '' ?>>
				<label for="subject_<?php echo $language->key; ?>"><?= __('Subject') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('confbagmail', $data->id, 'subject', $language->key); ?>
				<input type="text" name="subject_<?php echo $language->key; ?>" id="subject_<?php echo $language->key; ?>" value="<?=set_value('subject_'.$language->key, ($trans != null) ? $trans : $data->subject)?>" />
			</p>
			<p <?= (form_error("header_".$language->key) != '') ? 'class="error"' : '' ?>>
				<label for="header_<?php echo $language->key; ?>"><?= __('Header text (HTML)') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('confbagmail', $data->id, 'header', $language->key); ?>
				<textarea name="header_<?php echo $language->key; ?>" id="header_<?php echo $language->key; ?>"><?=set_value('header_'.$language->key, ($trans != null) ? $trans : $data->header)?></textarea>
			</p>
			<p <?= (form_error("footer_".$language->key) != '') ? 'class="error"' : '' ?>>
				<label for="footer_<?php echo $language->key; ?>"><?= __('Footer text (HTML)') ?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('confbagmail', $data->id, 'footer', $language->key); ?>
				<textarea name="footer_<?php echo $language->key; ?>" id="footer_<?php echo $language->key; ?>"><?=set_value('footer_'.$language->key, ($trans != null) ? $trans : $data->footer)?></textarea>
			</p>
			<?php endforeach; ?>
			<p <?= (form_error("viewtype") != '') ? 'class="error"' : '' ?>>
				<label for="viewtype"><?= __('Viewtype') ?>:</label>
				<select name="viewtype">
					<option value=""></option>
					<option value="tables" <?= $data->viewtype == 'tables' ? 'selected="selected"' : '' ?>>Tables (for easy excel export)</option>
				</select>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="javascript:history.go(-1);" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>