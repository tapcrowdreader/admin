<script type="text/javascript">    
    function nameSelected() {
        var selected = $("#nameselect option:selected").text();
        document.getElementById('name').value = selected;
    }
</script>
<div>
	<h1><?= __('Add Item')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8" class="edit" onSubmit="return checkdate()">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("sessgroup") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Sessiongroup')?></label>
				<input type="text" name="sessgroup" value="<?=set_value('sessgroup')?>" id="sessgroup" onblur="checkgroup()" >
				<span id="sessgroupvalidation" class="validationspan"><?= __('You will add a new sessiongroup.')?></span>
			</p>
            <?php if($artists != null && !empty($artists) && $app->apptypeid == 6) : ?>
            <p id="selectname" name="selectname" <?= (form_error("nameselect") != '') ? 'class="error"' : '' ?>>
                <label><?= __('Artist')?></label>
                <select name="nameselect" id="nameselect" onChange="nameSelected();">
                    <option value=""><?= __('Select ...')?></option>
                    <?php foreach ($artists as $artist): ?>
                        <option value="<?= $artist->id ?>" <?= set_select('nameselect', $artist->id) ?>><?= $artist->name ?></option>
                    <?php endforeach ?>
                </select>
            </p>
            <p>
                <label><?= __('Session name same as artist name?')?></label>
                <input class="checkboxClass" type="checkbox" name="sessionsameasartist" checked /> <br />
            </p>
            <?php endif; ?>
            <?php if(isset($languages) && !empty($languages)) : ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Name '. '(' . $language->name . ')'); ?>:</label>
                    <input type="text" name="name_<?php echo $language->key; ?>" id="name" value="<?=set_value('name_'.$language->key)?>" />
                </p>
                <?php endforeach; ?>
                <?php foreach($languages as $language) : ?>
                <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
                    <label for="description"><?= __('Description '. '(' . $language->name . ')'); ?>:</label>
                    <textarea name="description_<?php echo $language->key; ?>" id="description"><?= set_value('description_'.$language->key) ?></textarea>
                </p>
                <?php endforeach; ?>
            <?php else : ?>
                <p <?= (form_error("description_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="description"><?= __('Description:')?></label>
                    <textarea name="description_<?php echo $app->defaultlanguage; ?>" id="description"><?= set_value('description_'.$app->defaultlanguage) ?></textarea>
                </p>  
                <p <?= (form_error("name_". $app->defaultlanguage) != '') ? 'class="error"' : '' ?>>
                    <label for="name"><?= __('Name:')?></label>
                    <input type="text" name="name_<?php echo $app->defaultlanguage; ?>" id="name" value="<?=set_value('name_'.$app->defaultlanguage)?>" />
                </p>
            <?php endif; ?>
			<?php if(externalIds()) : ?>
			<p <?= (form_error("external_id") != '') ? 'class="error"' : '' ?>>
				<label for="external_id"><?= __('External id:') ?></label>
				<input type="text" name="external_id" id="external_id" value="<?= set_value('external_id') ?>" />
			</p>
			<?php endif; ?>
			<p class="small <?= (form_error("startdate") != '' || form_error("starttime") != '') ? 'error' : '' ?>">
				<label><?= __('Starttime:')?></label>
				<input type="text" name="startdate" value="<?=set_value('startdate')?>" id="startdate" class="datepicker">
				<input type="text" name="starttime" value="<?=set_value('starttime')?>" id="starttime" class="fromtill">
			</p>
			<br clear="all" />
			<p class="small <?= (form_error("enddate") != '' || form_error("endtime") != '') ? 'error' : '' ?>">
				<label><?= __('Endtime:')?></label>
				<input type="text" name="enddate" value="<?=set_value('enddate')?>" id="enddate" class="datepicker">
				<input type="text" name="endtime" value="<?=set_value('endtime')?>" id="endtime" class="fromtill">
			</p>
			<br clear="all" />
			<p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl">Image:</label>
				<input type="file" name="imageurl" id="imageurl" value="" /><br />
				<span class="hintImage"><?= __('Required size: %sx%spx, png',640,320) ?></span>
				<br clear="all" />
			</p>
			<?php if($app->apptypeid != 10 ) : ?>
			<div class="row">
				<label for="speakers"><?= __('Speakers:')?></label>
				<ul id="myspeakers" name="myspeakersul"></ul>
				<span id="speakersvalidation" class="validationspan"><?= __('You will add a new speaker.') ?></span>
			</div>
			<?php endif; ?>
			<p <?= (form_error("location") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Location:')?></label>
				<input type="text" name="location" value="<?= set_value('location') ?>" id="location">
			</p>
			<?php if($app->apptypeid == 10) : ?>
			<p <?= (form_error("twitter") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Twitter (Full url):')?></label>
				<input type="text" name="twitter" value="<?= set_value('twitter') ?>" id="twitter">
			</p>
			<p <?= (form_error("facebook") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Facebook (Full url):')?></label>
				<input type="text" name="facebook" value="<?= set_value('facebook') ?>" id="facebook">
			</p>
			<p <?= (form_error("youtube") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Youtube (Full url):')?></label>
				<input type="text" name="youtube" value="<?= set_value('youtube') ?>" id="youtube">
			</p>
			<?php endif; ?>
			<p <?= (form_error("url") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Website (Full url):')?></label>
				<input type="text" name="url" value="<?= set_value('url') ?>" id="url">
			</p>
			<p <?= (form_error("host") != '') ? 'class="error"' : '' ?> style="display:none">
				<label for="host"><?= __('Host:')?></label>
				<select name="host" id="host">
					<option value="0"><?= __('Select ...')?></option>
					<?php if ($exhibitors != FALSE): ?>
					<?php foreach ($exhibitors as $exhibitor): ?>
						<option value="<?= $exhibitor->id ?>"><?= $exhibitor->booth . ' - ' . $exhibitor->name ?></option>
					<?php endforeach ?>
					<?php endif ?>
				</select>
			</p>
			<div class="row">
				<label for="tags">Tags:</label>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?=__('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order') ?>" id="order"><br/>
			</p>
            <?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<?php if(_checkMultilang($m, $lang->key, $app)): ?>
						<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
							<?= _getInputField($m, $app, $lang, $languages); ?>
						</p>
					<?php endif; ?>
				<?php endforeach; ?>
            <?php endforeach; ?>
			<p <?= (form_error("premium") != '') ? 'class="error"' : '' ?> class="premiump">
				<input type="checkbox" name="premium" class="checkboxClass premiumCheckbox" id="premium"> <?=__('Premium?')?>
			</p>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?> id="extralinep">
				<label><?=__('Premium order:')?></label>
				<input type="text" name="premiumorder" value="<?= set_value('premiumorder') ?>" id="premiumorder">
				<label><?=__('Extra line:')?></label>
				<input type="text" name="extraline" value="<?= set_value('extraline') ?>" id="extraline">
				<label><?=__('Title:')?></label>
				<input type="text" name="premiumtitle" value="<?= set_value('premiumtitle', $premiumtitle) ?>" id="premiumtitle">
			</p>
 			<?php if($confbagactive) : ?>
			<p <?= (form_error("confbag") != '') ? 'class="error"' : '' ?>>
				<label for="confbag"><?= __('Conference bag content:')?></label>
				<input type="file" name="confbagcontent" id="confbagcontent" />
				<br clear="all" />
			</p>
			<?php endif; ?>
			<p <?= (form_error("parentid") != '') ? 'class="error"' : '' ?>>
				<label for="parentid"><?= __('Parent Session:')?></label>
				<select name="parentid">
					<option value="0" id="0"> </option>
					<?php foreach($parentsessions as $s) : ?>
					<option value="<?=$s->id?>" id="<?=$s->id?>"><?= $s->name ?></option>
					<?php endforeach; ?>
				</select>
				<br clear="all" />
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add Session')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if($postedtags){foreach($postedtags as $val){ echo '"'.$val.'", '; }} ?>],
		});

		$("#myspeakers").tagit({
			initialTags: [<?php if($postedspeakers){foreach($postedspeakers as $val){ echo '"'.$val.'", '; }} ?>],
		});

		$(".fromtill").mask("99:99");

		var groups = new Array();
		var i = 0;
		<?php foreach($sessgroups as $group) : ?>
		groups[i] = '<?=$group->name?>';
		i++;
		<?php endforeach; ?>
		$("input#sessgroup").autocomplete({
			source: groups
		});

		var speakers = new Array();
		<?php
		$speakernames = array();
		foreach($eventspeakers as $s) $speakernames[] = $s->name; ?>		
		var speakers = <?=json_encode($speakernames);?>;
		$("#myspeakers input.tagit-input").autocomplete({
			source: speakers
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = '<?=$tag->tag?>';
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
	});

	function checkgroup() {
		//check sessiongroup
		var groups = new Array();
		var i = 0;
		<?php foreach($sessgroups as $group) : ?>
		groups[i] = '<?=$group->name?>';
		i++;
		<?php endforeach; ?>
		if ($.inArray($('#sessgroup').val(), groups) == -1) { 
			$('#sessgroupvalidation').css('display','block');
		} else {
			$('#sessgroupvalidation').css('display','none');
		}
	}

	$('.row').on("change", "select", function() {
		//check sessiongroup
		var speakers = new Array();
		<?php
		$speakernames = array();
		foreach($eventspeakers as $s) $speakernames[] = $s->name; ?>		
		var speakers = <?=json_encode($speakernames);?>;

		var showlabel = false;
		for(var i in this.options) {
			if(!this.options.hasOwnProperty(i)) continue;
			var name = this.options[i].value;
			if ($.inArray(name, speakers) == -1) { 
				showlabel = true;
			}
		}
		if (showlabel == true) { 
			$('#speakersvalidation').css('display','block');
		} else {
			$('#speakersvalidation').css('display','none');
		}
	});

	function checkdate() {
		var returnval = true;

		//check sessiondates
		<?php if(strstr($event->datefrom,'1970-01-01')) : ?>
			return returnval;
		<?php endif; ?>
		var startparts = $('#startdate').val().split("-");
		var start = Date.parse(new Date(startparts[2], (startparts[1] - 1) ,startparts[0])) / 1000;

		var endparts = $('#enddate').val().split("-");
		var end = Date.parse(new Date(endparts[2], (endparts[1] - 1) ,endparts[0])) / 1000;

		var returnval;
		var eventstart = <?= strtotime($event->datefrom)?>;
		var eventend = <?= strtotime($event->dateto)?>;

		if (start < eventstart) { 
			returnval = confirm("<?= __('Are you sure you want to add a session that takes place before the event?')?>");
		}else if(end > eventend) {
			returnval = confirm("<?= __('Are you sure you want to add a session that takes place after the event?')?>");
		}

		return returnval;
	}

	$('#starttime').keyup(function() {
		var parts = this.value.split(":");
		if(parts[0].substring(0,1) != '_' && parts[0].substring(1,2) != '_') {
			var hours = parseInt(parts[0]);
			if(hours > 23) {
				this.value = '';
				this.text = '';
			}

			if(parts[1].substring(0,1) != '_' && parts[1].substring(1,2) != '_') {
				var minutes = parseInt(parts[1]);
				if(minutes > 59) {
					this.value = '';
					this.text = '';
				}
			}
		}
	});

	$('#endtime').keyup(function() {
		var parts = this.value.split(":");
		if(parts[0].substring(0,1) != '_' && parts[0].substring(1,2) != '_') {
			var hours = parseInt(parts[0]);
			if(hours > 23) {
				this.value = '';
				this.text = '';
			}

			if(parts[1].substring(0,1) != '_' && parts[1].substring(1,2) != '_') {
				var minutes = parseInt(parts[1]);
				if(minutes > 59) {
					this.value = '';
					this.text = '';
				}
			}
		}
	});
</script>
