<a href="<?= site_url('module/edit/5/event/'.$event->id) ?>" class="edit btn" style="float:right;"><i class="icon-pencil"></i> <?= __('Module Settings')?></a>
<?php
if ($this->uri->segment(4) == 'session') {
	$itemtype = 'session';
} elseif($this->uri->segment(4) == 'poi') {
	$itemtype = 'poi';
} elseif($this->uri->segment(4) == 'subfloorplan') {
	$itemtype = 'subfloorplan';
} else {
	$itemtype = 'exhibitor';
}
?>

<h1><?= __('Floorplan')?></h1>
<?php if(isset($_GET['error']) && $_GET['error'] == 'noimage') : ?>
<p style="color:#FF0000;"><?= __('Please select an image to upload.') ?></p>
<?php endif; ?>
<?php if(isset($_GET['error']) && $_GET['error'] == 'image') : ?>
<p style="color:#FF0000;"><?= __('Failed to upload floorplan. Please select an image smaller than 3000px width and height and filesize 2MB.') ?></p>
<?php endif; ?>
<p><?= __('Image must be in jpg or png format. Max width and height is 3000px. Minimum width is 800px, minimum height is 600px. Max filesize is 2MB.') ?></p>
<div class="note">
    <form action="<?= site_url('map/add/'.$event->id.'/event') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
        <fieldset>
		<?php if($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)) : ?>
        <p>
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)){ ?><span class="evtlogo" style="background:transparent url('<?= image_thumb($map->imageurl, 50, 50) ?>') no-repeat top left;"></span><span class="hintAppearance" style="margin-left:60px;"><?= __('If you just replace the floorplan your items will still be mapped on the same locations.')?><br /> <?= __('If you remove the floorplan the mapping will be lost as well.')?></span><?php } ?>
            <input type="file" name="w_map" id="w_map" value="" class="<?= (form_error("w_map") != '') ? 'error' : '' ?> logoupload" /><br clear="all" />
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)){ ?><span><a href="<?= site_url('event/removemap/'.$map->id.'/'.$event->id) ?>" class="deletemap" style="margin-left:55px;">Remove</a></span><?php } ?>
        </p>
        <div class="buttonbar">
            <input type="hidden" name="postback" value="postback" id="postback">
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)) : ?>
            <input type="hidden" name="replaceId" value="<?=$map->id?>" />
            <button type="submit" class="btn primary"><?= __('Replace Floorplan') ?></button>
       	 	<?php else: ?>
       	 	<button type="submit" class="btn primary"><?= __('Upload Floorplan') ?></button>
    		<?php endif; ?>
			<br clear="all" />
        </div>
		<br clear="all" />
        <?php else: ?>
		<p>
			<label for="floorplanname1"><?= __('Name') ?></label>
			<input type="text" name="floorplanname1" value="" id="floorplanname1">
		</p>
        <p>
            <label style="width: 200px;"><?= __('No map found, add a map') ?></label>
            <input type="file" name="w_map" id="w_map" value="" <?= (form_error("w_map") != '') ? 'class="error"' : '' ?> />
        </p>
        <div class="buttonbar">
            <input type="hidden" name="postback" value="postback" id="postback">
            <?php if($map != FALSE && file_exists($this->config->item('imagespath') . $map->imageurl)) : ?>
            <input type="hidden" name="replaceId" value="<?=$map->id?>" />
            <button type="submit" class="btn primary"><?= __('Replace Floorplan') ?></button>
       	 	<?php else: ?>
       	 	<button type="submit" class="btn primary"><?= __('Upload Floorplan') ?></button>
    		<?php endif; ?>
			<br clear="all" />
        </div>
		<br clear="all" />

        <?php endif; ?>
		</fieldset>
    </form>
</div>
<?php if ($map != FALSE && file_exists($this->config->item('imagespath') .$map->imageurl)): ?>
<br clear="all"/>
<div class="switch" style="width: auto;">
	<?php if($app->apptypeid != 10) : ?>
	<a href="<?= site_url('map/event/'.$event->id.'/exhibitor/'.$map->id) ?>" class="part <?= ($itemtype == 'exhibitor' ? "active" : "") ?>"><?= __('Add Exhibitor locations') ?></a>
	<a href="<?= site_url('map/event/'.$event->id.'/session/'.$map->id) ?>" class="part <?= ($itemtype == 'session' ? "active" : "") ?>"><?= __('Add Session locations') ?></a>
	<a href="<?= site_url('map/event/'.$event->id.'/subfloorplan/'.$map->id) ?>" class="part <?= ($itemtype == 'subfloorplan' ? "active" : "") ?>"><?= __('Add Sub floorplan') ?></a>
	<?php else :?>
	<?php if($itemtype != 'subfloorplan') {
		$itemtype = 'session';
		} ?>
	<a href="<?= site_url('map/event/'.$event->id.'/session/'.$map->id) ?>" class="part active"><?= __('Add Session locations') ?></a>
	<a href="<?= site_url('map/event/'.$event->id.'/subfloorplan/'.$map->id) ?>" class="part <?= ($itemtype == 'subfloorplan' ? "active" : "") ?>"><?= __('Add Sub floorplan') ?></a>
	<?php endif; ?>

</div>
	<script src="js/mobilymap.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {

			$('.map').mobilymap({
				position: 'top left',
				popupClass: 'bubble',
				markerClass: 'point',
				popup: true,
				cookies: true,
				caption: false,
				setCenter: true,
				outsideButtons: null,
				onMarkerClick: function(){},
				onPopupClose: function(){},
				onMapLoad: function(){
					content.bind({
						dblclick: function(e){
							var position = $(this).position();
							var offset = $(this).offset();
							var x = e.pageX - (offset.left);
							var y = e.pageY - (offset.top);
							if($('#sel_koppel option').size() <= 1){
								$('#saveadd').addClass('disabled');
							}
							$('#posx').val(x); $('#posy').val(y);
							$('#sel_koppel').removeClass('error');
							$('.map .imgContent').append('<div class="point added" id="p-' + x + '-' + y + '"><div class="markerContent"></div></div>');
							$('.mapform_bg').css({'opacity':0, 'display':'block'}).animate({'opacity':0.6});
							$('.mapform').fadeIn();

							reinitmap();
						}
					});
				}
			});

			// Cancel NEW MARKER
			$('#canceladd').click(function(e){
				e.preventDefault();
				$('.point.added').remove();
				$('.mapform_bg').fadeOut();
				$('.mapform').fadeOut();
			});

			// SAVE NEW MARKER
			$('#saveadd').click(function(e){
				e.preventDefault();
				if(!$(this).hasClass('disabled')) {
					if($('#sel_koppel').val() != ''){
						$.ajax({
							url: '<?= ($itemtype == "session") ? site_url("map/addSessionMarker") : (($itemtype == "poi") ? site_url('map/addPoi') : ($itemtype == "subfloorplan") ? site_url("map/addSubfloorplan") : site_url("map/addMarker")) ?>',
							type: 'POST',
							data: $('#frmMarkerDetails').serialize(),
							complete: function(xhr, textStatus) {
								$('.point.added .markerContent').html($("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").text() + '<br /><a href="<?= site_url("map/removemarker/".$map->eventid."/".$map->id) ?>/' + $("#sel_koppel").val() + '" class="delete"><?= __('DELETE')?></a>');
								$('.point.added').removeClass('added');

								$('.mapform_bg').fadeOut();
								$('.mapform').fadeOut();

								$("#sel_koppel option[value='"+ $("#sel_koppel").val() +"']").remove();
								$('#sel_koppel').val('');
							},
							success: function(data, textStatus, xhr) {

							},
							error: function(xhr, textStatus, errorThrown) {

							}
						});
					} else {
						// no exhibitor chosen
						$('#sel_koppel').addClass('error');
					}
				}
			});

			// $('#saveaddfloorplan').click(function(e){
			// 	e.preventDefault();
			// 	if(!$(this).hasClass('disabled')) {
			// 		if($('#sel_koppel').val() != ''){
			// 			$.ajax({
			// 				url: '<?= ($itemtype == "session") ? site_url("map/addSessionMarker") : (($itemtype == "poi") ? site_url('map/addPoi') : ($itemtype == "subfloorplan") ? site_url("map/addSubfloorplan") : site_url("map/addMarker")) ?>',
			// 				type: 'POST',
			// 				data: $('#frmMarkerDetailsFloorplan').serialize(),
			// 				complete: function(xhr, textStatus) {
			// 					$('.point.added .markerContent').html($("#floorplanname").val() + '<br /><a href="<?= site_url("map/removesubfloorplan/".$map->eventid."/".$map->id) ?>/' + $("#floorplanname").val() + '" class="delete">DELETE</a>');
			// 					$('.point.added').removeClass('added');

			// 					$('.mapform_bg').fadeOut();
			// 					$('.mapform').fadeOut();

			// 				},
			// 				success: function(data, textStatus, xhr) {

			// 				},
			// 				error: function(xhr, textStatus, errorThrown) {

			// 				}
			// 			});
			// 		} else {
			// 			// no exhibitor chosen
			// 			$('#sel_koppel').addClass('error');
			// 		}
			// 	}
			// });

		});

		function reinitmap(){
			var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
			var point=$this.find(".point");

			point.each(function(){
				var $this=$(this),pos=$this.attr("id").split("-");
				x = pos[1] - $this.width()/2, y = pos[2] - $this.height() + 4;
				$this.css({position:"absolute",zIndex:"2",top:y+"px",left:x+"px"});
				<?php if($itemtype == 'exhibitor' || $itemtype == '') : ?>
				$this.css('background-image', "url('../img/marker-exhibitor.png')");
				<?php elseif($itemtype == 'session') : ?>
				$this.css('background-image', "url('../img/marker-session.png')");
				<?php elseif($itemtype == 'subfloorplan') : ?>
				$this.css('background-image', "url('../img/marker-map.png')");
				<?php endif; ?>
				$this.css('width', '44px');
			});
			//.wrapInner($("<div />").addClass("markerContent").css({display:"none"}));
			point.click(function(){
				var $this=$(this),pointw=$this.width(),pointh=$this.height(),pos=$this.position(),py=pos.top,px=pos.left,wrap=$this.find(".markerContent").html();
				/*if(sets.setCenter){
					var center_y=-py+divh/2-pointh/2,center_x=-px+divw/2-pointw/2,center=map.check(center_x,center_y);
					content.animate({top:center.y+"px",left:center.x+"px"})
				}*/
				if(sets.popup){
					$("."+sets.popupClass).remove();
					$this.after($("<div />").addClass(sets.popupClass).css({position:"absolute",zIndex:"3"}).html(wrap).append($("<a />").addClass("close")));
					var popup=$this.next("."+sets.popupClass),popupw=popup.innerWidth(),popuph=popup.innerHeight(),y=py,x=px;popup.css({top:y+pointh+"px",left:x+"px",marginLeft:-(popupw/2-pointw/2)+"px"})
				}else{
					sets.onMarkerClick.call(this)
				}
				return false;
			});
			$this.find(".close").live("click",function(){
				var $this=$(this);
				$this.parents("."+sets.popupClass).remove();
				setTimeout(function(){
					sets.onPopupClose.call(this)
				},100);
				return false;
			});
			if(sets.outsideButtons!=null){
				$(sets.outsideButtons).click(function(){
					var $this=$(this),rel=$this.attr("rel");
					div=content.find("."+sets.markerClass).filter(function(){
						return $(this).attr("id")==rel
					});
					div.click();
					return false;
				});
			}
		}

	</script>
	<?php list($width, $height, $type, $attr) = getimagesize($this->config->item('imagespath') . $map->imageurl); ?>
	<?php if($map->parentId != 0) : ?>
	<a href="<?=site_url('map/event/'.$map->eventid.'/subfloorplan/'.$map->parentId);?>" class="btn" style="float:right;margin-top:-3px;"><?= __('Go to parent floorplan') ?></a>
	<?php endif; ?>
	<div class="note">
		<?= __('Double-click the map on the spot you would like to add a marker.') ?>
	</div>
	<div class="map">
		<img src="<?= $this->config->item('publicupload') .$map->imageurl ?>" width="<?= ($width) ?>" height="<?= ($height) ?>" style="left:0px !important;" />
		<?php if ($markers): ?>
			<?php if ($itemtype == 'session'): ?>
				<?php foreach ($markers as $marker): ?>
					<div class="point" id="p-<?= number_format($marker->xpos, 0, ".", "") ?>-<?= number_format($marker->ypos, 0, ".", "") ?>">
						<div class="markerContent">
							<?= $marker->location ?><br />
							<a href="<?= site_url('map/removeSessionmarker/'.$map->eventid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE') ?></a>
						</div>
					</div>
				<?php endforeach ?>
            <?php elseif($itemtype == 'poi'):  ?>
                <?php foreach ($markers as $marker): ?>
                    <div class="point" id="p-<?= ($marker->x2 != 0) ? $marker->x1+($marker->x2/2) : $marker->x1 ?>-<?= ($marker->y2 != 0) ? $marker->y1+($marker->y2/2) : $marker->y1 ?>">
                        <div class="markerContent">
                            <?= $marker->name . ' (' . $marker->poitypename . ')' ?><br />
                            <a href="<?= site_url('map/removepoi/'.$map->eventid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE') ?></a>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php elseif($itemtype == 'subfloorplan') : ?>
				<?php foreach ($markers as $marker): ?>
					<div class="point" id="p-<?= $marker->x ?>-<?= $marker->y ?>">
						<div class="markerContent">
							<a href="<?=site_url('map/event/'.$event->id.'/subfloorplan/'.$marker->id);?>"><?= $marker->title ?></a><br />
							<a href="<?= site_url('map/removefloorplan/'.$map->eventid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE') ?></a>
						</div>
					</div>
				<?php endforeach ?>
			<?php else: ?>
				<?php foreach ($markers as $marker): ?>
					<div class="point" id="p-<?= ($marker->x2 != 0) ? $marker->x1+($marker->x2/2) : $marker->x1 ?>-<?= ($marker->y2 != 0) ? $marker->y1+($marker->y2/2) : $marker->y1 ?>">
						<div class="markerContent">
							<?= $marker->name ?><br />
							<a href="<?= site_url('map/removemarker/'.$map->eventid.'/'.$map->id.'/'.$marker->id) ?>" class="delete"><?= __('DELETE') ?></a>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>
		<?php endif ?>
	</div>
	<div class="mapform_bg"></div>
	<?php if ($itemtype == 'session'): ?>
		<div class="mapform">
			<form action="<?= site_url('map/marker/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
				<h2><?= __('Choose Location') ?></h2>
				<p>
					<label for="sel_koppel"><?= __('Location')?></label>
					<select name="sel_koppel" id="sel_koppel">
						<option value=""><?= __('Select ...')?></option>
						<?php if ($sessionlocations != FALSE): ?>
						<?php foreach ($sessionlocations as $sl): ?>
							<option value="<?= $sl->id ?>"><?= $sl->location ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="hidden" name="markerid" value="" id="markerid">
					<input type="hidden" name="mapid" value="<?=$map->id?>" id="mapid">
					<input type="hidden" name="posx" value="" id="posx">
					<input type="hidden" name="posy" value="" id="posy">
					<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
					<a href="#" id="saveadd" class="btn primary <?= (isset($sl) && $sl != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
				</div>
			</form>
		</div>
    <?php elseif($itemtype == 'poi') : ?>
	<div class="mapform">
		<form action="<?= site_url('map/marker/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
			<h2><?= __('Choose Poi')?></h2>
			<p>
				<label for="name"><?= __('Name') ?></label>
				<input type="text" name="name" value="" id="name">
			</p>
			<p>
				<label for="sel_koppel"><?= __('Poitype') ?></label>
				<select name="sel_koppel" id="sel_koppel">
					<option value=""><?= __('Select ...') ?></option>
					<?php if ($poitypes != FALSE): ?>
					<?php foreach ($poitypes as $poitype): ?>
						<option value="<?= $poitype->id ?>"><?= $poitype->name ?></option>
					<?php endforeach ?>
					<?php endif ?>
				</select>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<input type="hidden" name="eventid" value="<?= $event->id ?>" id="eventid">
				<input type="hidden" name="venueid" value="" id="venueid">
				<input type="hidden" name="markerid" value="" id="markerid">
				<input type="hidden" name="posx" value="" id="posx">
				<input type="hidden" name="posy" value="" id="posy">
				<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
				<a href="#" id="saveadd" class="btn primary <?= ($poitype != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
			</div>
		</form>
	</div>
	<?php elseif($itemtype == 'subfloorplan' && isset($event)): ?>
		<div class="mapform" style="height:190px;">
			<form action="<?= site_url('map/addSubfloorplan/') ?>" method="POST" enctype="multipart/form-data" accept-charset="utf-8" id="frmMarkerDetailsFloorplan">
				<h2><?= __('Add Floorplan') ?></h2>
				<p>
					<label for="floorplanname"><?= __('Name') ?></label>
					<input type="text" name="floorplanname" value="" id="floorplanname">
				</p>
				<p>
					<input type="file" name="subfloorplan" id="subfloorplan" value="" <?= (form_error("subfloorplan") != '') ? 'class="error"' : '' ?> />
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="hidden" name="eventid" value="<?= $event->id ?>" id="eventid">
					<input type="hidden" name="parentid" value="<?= $this->uri->segment(5) ?>" id="parentid">
					<input type="hidden" name="markerid" value="" id="markerid">
					<input type="hidden" name="posx" value="" id="posx">
					<input type="hidden" name="posy" value="" id="posy">
					<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
					<input type="submit" id="saveaddfloorplan" class="btn primary" value="Save"/>
				</div>
			</form>
		</div>
	<?php elseif($itemtype == 'subfloorplan' && isset($venue)): ?>
		<div class="mapform" style="height:190px;">
			<form action="<?= site_url('map/addSubfloorplanVenue/') ?>" method="POST" enctype="multipart/form-data" accept-charset="utf-8" id="frmMarkerDetailsFloorplan">
				<h2><?= __('Add Floorplan') ?></h2>
				<p>
					<label for="floorplanname"><?= __('Name') ?></label>
					<input type="text" name="floorplanname" value="" id="floorplanname">
				</p>
				<p>
					<input type="file" name="subfloorplan" id="subfloorplan" value="" <?= (form_error("subfloorplan") != '') ? 'class="error"' : '' ?> />
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="hidden" name="venueid" value="<?= $venue->id ?>" id="venueid">
					<input type="hidden" name="parentid" value="<?= $this->uri->segment(5) ?>" id="parentid">
					<input type="hidden" name="markerid" value="" id="markerid">
					<input type="hidden" name="posx" value="" id="posx">
					<input type="hidden" name="posy" value="" id="posy">
					<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
					<input type="submit" id="saveaddfloorplan" class="btn primary" value="Save"/>
				</div>
			</form>
		</div>
	<?php else: ?>
		<div class="mapform">
			<form action="<?= site_url('map/marker/') ?>" method="POST" accept-charset="utf-8" id="frmMarkerDetails">
				<h2><?= __('Choose Exhibitor') ?></h2>
				<p>
					<label for="sel_koppel"><?= __('Exhibitor') ?></label>
					<select name="sel_koppel" id="sel_koppel">
						<option value=""><?= __('Select ...') ?></option>
						<?php if ($exhibitors != FALSE): ?>
						<?php foreach ($exhibitors as $exhibitor): ?>
							<option value="<?= $exhibitor->id ?>"><?= $exhibitor->booth . ' - ' . $exhibitor->name ?></option>
						<?php endforeach ?>
						<?php endif ?>
					</select>
				</p>
				<div class="buttonbar">
					<input type="hidden" name="postback" value="postback" id="postback">
					<input type="hidden" name="markerid" value="" id="markerid">
					<input type="hidden" name="mapid" value="<?=$map->id?>" id="mapid">
					<input type="hidden" name="posx" value="" id="posx">
					<input type="hidden" name="posy" value="" id="posy">
					<a href="#" id="canceladd" class="btn"><?= __('Cancel') ?></a>
					<a href="#" id="saveadd" class="btn primary <?= ($exhibitors != FALSE) ? "" : "disabled" ?>"><?= __('Save') ?></a>
				</div>
			</form>
		</div>
	<?php endif ?>
<?php endif ?>
<?php if($itemtype == 'exhibitor' || $itemtype == '') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/marker-exhibitor.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php elseif($itemtype == 'session') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/marker-session.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php elseif($itemtype == 'subfloorplan') : ?>
<script type="text/javascript">
$(document).ready(function() {
	var $this=$('.imgContent'), divw = $this.width(), divh = $this.height();
	var point=$this.find(".point");

	point.each(function(){
		var $this=$(this),pos=$this.attr("id").split("-");
		$this.css('background-image', "url('../img/marker-map.png')");
		$this.css('width', '44px');
	});
});
</script>
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function() {
	$('.deletemap').click(function(e) {
		e.preventDefault();
		var delurl = $(this).attr('href');
		jConfirm('If you remove the floorplan all items will be removed from the floorplan as well. Are you sure?', 'Remove floorplan', function(r) {
			if(r === true) {
				location.href = delurl;
			} else {
				return false;
			}
		});
	});
});
</script>
