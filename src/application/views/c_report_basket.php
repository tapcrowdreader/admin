<style type="text/css">	

	table
	{
		margin-top: 1em;
		border: 1px solid #AAAAAA;
	}
	
	table tr
	{
		width: 100%;
	}
	
	table td
	{
		border-bottom: 1px solid #DDDDDD;
	}
	
	table td.basketorder
	{
		background-color: whitesmoke;
	}
	
	table th
	{
		background-color: lightgrey;
		border-bottom: 1px solid #AAAAAA;
	}
	
</style>


<?php

function html_show_array($array){
  echo "<table>\n";
  echo "<tr><th>Order</th><th>Item Name</th><th>Category</th><th>Quantity</th>";
  show_array($array);
  echo "</table>\n";
}


function show_array($array){
	foreach($array as $order){
		echo '<tr>';
		echo '<td class="basketorder">'.$order['order'];
		echo "</td><td class=\"basketorder\"></td><td class=\"basketorder\"></td>";
		echo "<td class=\"basketorder\"></td>\n";
		show_element($order['items']);
		echo "</tr>\n";
	}
}

function show_element($array){
	foreach($array as $item){
		echo "<tr>\n";
		echo "<td> </td>\n";
		echo '<td>'.$item['element']."</td>\n";
		echo '<td>'.$item['category']."</td>\n";
		echo '<td>'.$item['quantity']."</td>\n";
		echo "<tr>\n";
	}
}

?>

<div id="report">

	<div class="header">
	<h1><?= __('Reports')?></h1>
	
	
	<input type="hidden" value="" id="name" name="name">
	<button class="add btn primary"><?=__("Download Report as CSV File")?></button>
	</div>
	<div class="listitems">
		<?php html_show_array($report)?>
	</div>
</div>

<script type="text/javascript">
$("#name").val("<?=$name?>");

$("div.header button.add").click(
	function()
	{
		window.location.href = "reports/view/basket/venue/1";
	}
)

</script>