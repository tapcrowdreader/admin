<?php
/**
 * Tapcrowd list view
 * @author Tom Van de Putte
 */
$browse_url = str_replace('--parentId--', $data->parentId, $settings->browse_url);
$browse_url = str_replace('--parentType--', $data->parentType, $browse_url);
?>
<form method="POST" action="<?= str_replace('--id--', $data->id, $settings->edit_url) ?>" enctype="multipart/form-data">

	<!-- Header -->
	<div class="control-group">
		<div class="pull-right" >
			<a type="button" class="btn" href="<?=$browse_url?>"><?=__('Cancel')?></a>
			<button type="submit" class="btn btn-primary"><?=__('Save')?></button>

		</div>
		<?php if($data->id != null) : ?>
			<h2><?=__('Edit %s', $data->title)?></h2>
		<?php else: ?>
			<h2><?=__('Add Item')?></h2>
		<?php endif; ?>
	</div>

	<!-- Title -->
	<?php foreach($settings->languages as $language) : ?>
	<div class="control-group">
    	<label class="control-label" for="input_title_<?=$language->key?>"><?=__('Title')?> <?= '(' . $language->name . ')'; ?></label>
		<div class="controls">
			<?php $trans = _getTranslation('place', $data->id, 'title', $language->key); ?>
			<input type="text" id="input_title_<?=$language->key?>" placeholder="" name="place_title_<?=$language->key?>" value="<?= $trans ? $trans : $data->title?>" class="span8" />
		</div>
	</div>
	<?php endforeach; ?>

	<!-- Address -->
	<div class="control-group">
		<label class="control-label" for="input_addr"><?=__('Address')?></label>
		<div class="controls">
			<div class="input-append">
				<input type="text" id="input_addr" name="place_addr" value="<?=$data->addr?>" class="span7" />
				<button class="btn" type="button" id="show_location_button"><i class="icon-map-marker"></i> Show</button>
			</div>
			<p class="muted"><?=__('Current location: %f, %f', $data->lat, $data->lng)?></p>
			<div id="map_canvas" class="map"></div>
			<input type="hidden" id="input_lat" name="place_lat" value="<?=$data->lat?>" />
			<input type="hidden" id="input_lng" name="place_lng" value="<?=$data->lng?>" />
		</div>
	</div>

	<!-- Info -->
	<?php foreach($settings->languages as $language) : ?>
	<div class="control-group">
		<label class="control-label" for="input_title_<?=$language->key?>"><?=__('Info')?> <?= '(' . $language->name . ')'; ?></label>
		<div class="controls">
			<?php $trans = _getTranslation('place', $data->id, 'info', $language->key); ?>
			<textarea id="input_info_<?=$language->key?>" name="place_info_<?=$language->key?>" class="span8" style="height:100px"><?= $trans ? $trans : $data->info?></textarea>
		</div>
	</div>
	<?php endforeach; ?>

	<div class="control-group">
		<label class="control-label" for="imageurl"><?=__('Image')?></label>
		<div class="controls">
			<?php if($data->imageurl != ''){ ?><span class="evtlogo"><img src="<?= $this->config->item('publicupload') . $data->imageurl ?>" width="50" /></span><?php } ?>
			<input type="file" name="imageurl" id="imageurl" value="" class="logoupload" value="<?= set_value('imageurl') ?>" style="width:360px;" /><br />
			<?php if($data->imageurl != ''){ ?><span><a href="<?= site_url('places/removeimage/'.$data->id) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
		</div>
	</div>

	<!-- Status -->
<!--	<div class="control-group">
		<label class="control-label" for="input_status"><?=__('Status')?></label>
		<div class="controls">
			<select id="input_status" name="<?=$prefix?>status" class="span8"><?php
			foreach($settings->statuses as $status) {
				$selected = ($status == $data->status)? ' selected="selected"' : ''; ?>
				<option value="<?=$status?>"<?=$selected?>><?=$status?></option><?php
			} ?>
			</select>
		</div>
	</div>-->

	<?php
	/**
	 * Metadata
	 */
	foreach($metadata as $m) {
		foreach($languages as $lang) {
			if(_checkMultilang($m, $lang->key, $app)) {
				$class = (form_error($m->qname.'_'.$lang->key) != '')? ' class="error"' : '';
				$input = _getInputField($m, $app, $lang, $languages, 'place', $data->id);
				echo "<p$class>$input</p>";
			}
		}
	}

	if(!empty($catshtml)) : ?>
		<br />
		<label style="margin-bottom:10px;"><?= __('Item groups') ?>:</label>
		<ul>
			<?php foreach($catshtml as $option) : ?>
			<?php $option = str_replace('placescategories', 'Places', $option); ?>
			<?= $option ?>
			<?php endforeach; ?>
		</ul><br />
	<?php endif; ?>

	<div class="row">
		<label for="tags">Tags:</label>
		<ul id="mytags" name="mytagsul"></ul>
	</div>

	<div class="pull-right">
		<input type="hidden" name="parentType" value="<?= $data->parentType ?>" />
		<input type="hidden" name="parentId" value="<?= $data->parentId ?>" />
		<input type="hidden" name="groupids" value="<?= $data->groupids ?>" />
		<input type="hidden" name="launcherid" value="<?= $settings->launcher->id ?>" />
		<a type="button" class="btn" href="javascript:history.go(-1);"><?=__('Cancel')?></a>
		<button type="submit" class="btn btn-primary"><?=__('Save')?></button>
	</div>
</form>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script>

/**
 * Google Maps integration
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.8/reference
 */
(function(){
	var map, geocoder, marker, input_lat, input_lng, input_addr;

	/**
	 * Initiate map, geocoder and marker
	 */
	function initialize()
	{
		var mapOptions = {
			zoom: 8,
			center: new google.maps.LatLng( 50.753887, 4.269936),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		geocoder = new google.maps.Geocoder();
		marker = new google.maps.Marker({map: map, draggable: true});
		google.maps.event.addListener(marker, 'dragend', updateInputFields);

		// Set DOMNodes
		input_lat = document.getElementById('input_lat');
		input_lng = document.getElementById('input_lng');
		input_addr = document.getElementById('input_addr');

		// Update map if any input fields are already set
		if(parseFloat(input_lat.value) != 0 && parseFloat(input_lng.value) != 0) {
			updateMarker( new google.maps.LatLng(input_lat.value, input_lng.value) );
		}
		else if(input_addr.value != '') {
			codeAddress();
		}
	}

	/**
	 * Show address on map
	 * @param string address
	 */
	function codeAddress( address )
	{
		var address = input_addr.value;
		geocoder.geocode( {'address': address}, function(results, status)
		{
			if(status == google.maps.GeocoderStatus.OK) {
				var loc = results[0].geometry.location;
				updateMarker(loc)
				updateInputFields(loc);
			} else {
				if(status == 'ZERO_RESULTS') {
					alert('<?=__('Could not find any results for given address')?>');
				} else {
					alert('<?=__('Geocode was not successful for the following reason: ')?>' + status);
				}
			}
		});
	}

	/**
	 * Update Marker
	 * @param google.maps.LatLng location
	 */
	function updateMarker( location )
	{
		map.setCenter(location);
		marker.setPosition(location);
		map.setZoom(16);
	}

	/**
	 * Method to update form input fields
	 */
	function updateInputFields()
	{
		if(typeof(marker) != 'object') return;
		var location = marker.getPosition();
		document.getElementById('input_lat').value = location.lat();
		document.getElementById('input_lng').value = location.lng();
	}

	// Append event listeners
	google.maps.event.addDomListener(window, 'load', initialize);
	document.getElementById('show_location_button').onclick = codeAddress;
})();

	$(document).ready(function(){
		$("#mytags").tagit({
			initialTags: [<?php if(isset($postedtags) && $postedtags != false && !empty($postedtags)) { 
				foreach($postedtags as $val){ 
					echo '"'.$val.'", ';
				} 
			} elseif (isset($tags) && $tags != false) {
				foreach($tags as $val){ 
					echo "\"".$val->tag."\"," ;
				}
			} ?>]
		});

		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = "<?=$tag->tag?>";
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
	});

</script>
