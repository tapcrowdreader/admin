<div>
	<!-- <a href="<?= site_url('formscreens/result/'.$formscreen->id) ?>?blank" class="btn" target="_blank" style="float:right;"><?= __('View page in new window')?></a> -->
	<h1><?=__('Layout results page')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("headerHtml") != '') ? 'class="error"' : '' ?>>
				<label for="headerHtml"><?= __('Header text (HTML)') ?>:</label>
				<textarea name="headerHtml" id="headerHtml"><?=set_value('headerHtml', $data->header)?></textarea>
			</p>
			<p <?= (form_error("bodyHtml") != '') ? 'class="error"' : '' ?>>
				<label for="bodyHtml"><?= __('Body text (HTML)') ?>:</label>
				<textarea name="bodyHtml" id="bodyHtml"><?=set_value('bodyHtml', $data->body)?></textarea>
			</p>
			<p <?= (form_error("footerHtml") != '') ? 'class="error"' : '' ?>>
				<label for="footerHtml"><?= __('Footer text (HTML)') ?>:</label>
				<textarea name="footerHtml" id="footerHtml"><?=set_value('footerHtml', $data->footer)?></textarea>
			</p>
			<p <?= (form_error("css") != '') ? 'class="error"' : '' ?>>
				<label for="css"><?= __('Extra CSS') ?>:</label>
				<textarea name="css" id="css"><?=set_value('css', $data->css)?></textarea>
			</p>

			<label>Color piecharts: </label><br />
			<div id="jPicker">
			<?php 
				$i = 0;
				while($i < $maxpossiblevalues) {
					$value = '';
					if(isset($colors[$i])) {
						$value = $colors[$i];
					}
					echo '<span style="text-align:left;margin-bottom:20px;">Color '.($i+1).': </span><input class="Expandable" style="display:none;" name="color'.$i.'" type="text" value="'.$value.'" /><br clear="all" />';
					$i++;
				}
			?>
			</div>

			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="javascript:history.go(-1);" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(
    function()
    {
      $('.Expandable').jPicker(
        {
          window:
          {
            expandable: true
          }
        });
    });
</script>