<div>
	<h1><?= __('Notify Users') ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("title") != '') ? 'class="error"' : '' ?>>
				<label for="title"><?= __('Title:')?></label>
				<input type="text" name="title" id="title" value="<?= set_value('title') ?>" />
			</p>
			<p <?= (form_error("text") != '') ? 'class="error"' : '' ?>>
				<label for="text"><?= __('Text: ')?></label>
				<textarea name="text" id="text"><?= set_value('text', "[userconferencebag] \n\n\n" . __('Kind regards,\nThe TapCrowd Team')) ?></textarea>
			</p>
			<p>
				<label></label><?= __('Use the tag [userconferencebag] to insert the conference bag in your mail.')?>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('favourites/event/'.$event->id) ?>" class="delete"><?= __('Cancel')?></a>
				<button type="submit" class="send email"><?= __('SEND NOTIFICATION')?></button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('button.send').click(function(e){
			e.preventDefault();
			var delurl = $(this).attr('href');
			jConfirm('<?= __('Are you sure you want to send this notification right now?')?>', '<?= __('Send notification')?>', function(r) {
				if(r == true) {
					$('form.edit').submit();
				}
			});
		});
	});
</script>