<div>
	<?php
	#
	# Module settings button
	#
	if(isset($settings->module_url)) { ?>
		<div class="btn-group pull-right">
			<a href="<?=$settings->module_url?>" class="btn"><i class="icon-wrench"></i> <?=__('Module Settings')?></a>
		</div><?php
	} ?>
	<h1><?= __('Edit Notes')?></h1>
	<div class="frmsessions">
		<?php if(_isAdmin()) : ?>
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<p <?= (form_error("modules") != '') ? 'class="error"' : '' ?>>
				<label><?= __('On which modules should the notes module be available?')?></label>
				<?php 

				# Display modules
				$activemoduleids = array();
				foreach($activemodules as $m) $activemoduleids[] = $m->launcherid;

				foreach($modules as $m) {
					$checked = in_array($m->id, $activemoduleids)? 'checked="checked"' : ''; ?>
						<input type="checkbox" class="checkboxClass" name="notemodules[]" <?=$checked?> value="<?=$m->id?>" />
						<?= $m->title ?><br />
				<?php
				} ?>
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="javascript:history.go(-1);" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
		<?php endif; ?>
	</div>
</div>