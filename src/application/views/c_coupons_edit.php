<style type="text/css">
	.hidden {
		display: none;
	}
	
	img.previewimg {
		 max-width: 600px;
		 max-height: 480px;
	}
</style>

<div>
	<h1><?= __('Edit Coupon')?></h1>
	<div class="frmsessions">
		<form name="editcoupon" action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			
            <?php if(isset($languages) && !empty($languages)) : ?>
	            <?php foreach($languages as $language) : ?>
	            <p>
	                <label for="title"><?=__('Title ')?> <?= '(' . $language->name . ')'; ?></label>
					<?php $trans = _getTranslation('coupons', $coupon->id, 'title', $language->key); ?>
					<input type="text" name="title_<?=$language->key?>" id="title_<?=$language->key?>" value="<?= set_value('title_'.$language->key, ($trans != null) ? $trans : $coupon->title) ?>" />
	            </p>
	            <?php endforeach; ?>
                <?php foreach($languages as $language) : ?>
		            <p>
		                <label for="text"><?= __('Text ') ?><?= '(' . $language->name . ')'; ?>:</label>
						<?php $trans = _getTranslation('coupons', $couopn->id, 'text', $language->key); ?>
		                <textarea name="text_<?php echo $language->key; ?>" id="text"><?= htmlspecialchars_decode(set_value('text_'.$language->key, ($trans != null) ? $trans : $coupon->content), ENT_NOQUOTES) ?></textarea>
		            </p>
            	<?php endforeach; ?>
			<?php else : ?>
	            <p>
	                <label for="title"><?= __('Title:') ?></label>
	                <input type="text" name="title_<?php echo $app->defaultlanguage; ?>" id="title" value="<?= htmlspecialchars_decode(set_value('title_'.$app->defaultlanguage, _getTranslation('coupons', $coupon->id, 'title', $app->defaultlanguage)), ENT_NOQUOTES) ?>" />
	            </p>
	            <p>
	                <label for="text"><?= __('Text:') ?></label>
	                <textarea name="text_<?php echo $app->defaultlanguage; ?>" id="text"><?= htmlspecialchars_decode(set_value('text_'. $app->defaultlanguage, _getTranslation('coupons', $coupon->id, 'content',  $app->defaultlanguage)), ENT_NOQUOTES) ?></textarea>
	            </p>
            <?php endif; ?>
            
 			<p align="center" <?= (form_error("image") != '') ? 'class="error"' : '' ?>>
 				<img class="previewimg" src="<?= $coupon->image?>">
				<label for="image"><?= __('Image:') ?></label>
				<input type="file" name="image" id="image" value="" class="imageupload"/>
			</p><br />
           
			<label for="group1"><?=__("Select for how long you want this coupon to be available:")?></label>
			
		
			<input type="radio" name="group1" value="1" <?= ($coupon->uses ==  1) ? 'checked' : ""?>> <?=__("One Time")?><br>
			<input type="radio" name="group1" value="-1" <?= ($coupon->uses !=  1) ? 'checked' : ""?>> <?=__("Unlimited")?><br>
	   
			
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="button" class="btn primary cancel"><?= __('Cancel') ?></button>
				<button type="button" class="btn primary pass"><?=__("Generate Passbook Coupon")?></button>
				<button type="submit" class="btn primary"><?= __('Submit') ?></button>
				<br clear="all" />
			</div>
			
			<br clear="all" />
		</form>
	</div>
</div>

<script type="text/javascript">

(function(){

$("form[name=editcoupon] button.cancel").click(
	function (){
		window.location.href = "<?php echo 'coupons/venue/'.$coupon->venueid?>";
	}
);	

$("form[name=editcoupon] button.pass").click(
	function (){
		window.location.href = "<?='coupons/createPass/'.$coupon->venueid.'/'.$coupon->id?>";
	}
);

// function readURL(input) {
//     if (input.files && input.files[0]) {
//         var reader = new FileReader();

//         reader.onload = function (e) {
//             $('.previewimg').attr('src', e.target.result);
//         }

//         reader.readAsDataURL(input.files[0]);
//     }
// }
    
})();

</script>