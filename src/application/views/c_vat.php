<h1><?= __('Submit app:') . ' ' . $app->name ?></h1>
<form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" id="form">
	<p>
		<label> <?= __('Please choose your country') ?> </label>
		<select name="country" id="country">
			<?php foreach(_getAllCountries() as $key => $value) : ?>
			<option name="<?= $key ?>" value="<?= $key ?>"><?= $value ?> </option>
			<?php endforeach; ?>
		</select>
	</p>
	<p>
		<label> <?= __('Do you have a VAT number?') ?> </label>
		<input type="checkbox" name="vatnumbercheckbox" class="checkboxClass" id="vatnumbercheckbox" /><br clear="all" />
	</p>
	<p id="vatnumberp" style="display:none;">
		<label> <?= __('VAT number') ?> <span id="vathint"></span> </label>
		<input type="text" name="vatnumber" id="vatnumber" />
	</p>
	<div class="buttonbar">
		<input type="hidden" name="postback" value="postback" id="postback">
		<button type="submit" id="submitbutton" class="btn primary"><?= __('Continue') ?></button>
		<br clear="all" />
	</div>
</form>
<script type="text/javascript">
$(document).ready(function() {
	$("#vatnumbercheckbox").change(function(event) {
		var isChecked = $('#vatnumbercheckbox').attr('checked')?true:false;
		if(isChecked) {
			$("#vatnumberp").css('display','block');
		} else {
			$("#vatnumberp").css('display','none');
		}
	});

	$("#country").change(function(event) {
		if($("#country").val() == 'BE') {
			$("#vathint").html('(BE0123456789)');
		} else if($("#country").val() == 'NL') {
			$("#vathint").html('(NL123456789ABC)');
		} else {
			$("#vathint").html('');
		}
	});

	$("#submitbutton").click(function(event) {
		event.preventDefault();
		var isChecked = $('#vatnumbercheckbox').attr('checked')?true:false;
		if(isChecked) {
			if(validateVat()) {
				$("#form").submit();
			}
		} else {
			$("#form").submit();
		}
	});
	$('#vatnumber').blur(function() {
		var isChecked = $('#vatnumbercheckbox').attr('checked')?true:false;
		if(isChecked) {
			validateVat();
		}
	});

	function validateVat() {
		var vat = $("#vatnumber").val();
		vat = $.trim(vat);
		if($("#country").val() == 'BE') {
			if(vat.length == 9 && vat.substring(0, 2) != 'BE') {
				vat = 'BE0'+vat;
			} else if(vat.length != 9 && vat.substring(0, 2) != 'BE') {
				vat = 'BE'+vat;
			}
			if(vat.length != 12 || isNaN(vat.substring(2,vat.length))) {
				alert('Please enter a valid VAT number');
				return false;
			} else {
				return true;
			}
		} else if($("#country").val() == 'NL') {
			if(vat.substring(0, 2) != 'NL') {
				vat = 'NL'+vat;
			}

			if(vat.length != 14) {
				alert('Please enter a valid VAT number');
				return false;
			} else {
				return true;
			}
		} else {
			if(vat != '') {
				return true;
			} else {
				alert('Please enter a valid VAT number');
				return false;
			}
		}
	}
});
</script>