<script type="text/javascript">
	$(document).ready(function(){
		// $(".metaEdit").click(function(event){
		// 	event.preventDefault();
		// 	var id = $(this).attr("id");
		// 	editMetadata(id);
		// });
		
		$("#mytags").tagit({
			initialTags: [<?php if(isset($tags) && $tags != null){foreach($tags as $val){ echo '"'.$val.'", '; }} ?>],
		});
		var tags = new Array();
		var i = 0;
		<?php foreach($apptags as $tag) : ?>
		tags[i] = '<?=$tag->tag?>';
		i++;
		<?php endforeach; ?>
		$("#mytags input.tagit-input").autocomplete({
			source: tags
		});
		
		
		// function editMetadata(id) {
		// 	if($('#imgEdit' + id).attr('src') == 'img/icons/accept.png') {
		// 		$.post("<?= 'metadata/edit/'?>"+id, { id: id, key: $('#txtkey'+ id).val(), value: $('#txtvalue'+ id).val(), metadata: 'metadata'}, function(data) {
		// 			$('#lblkey'+ id).text($('#txtkey'+ id).val());
		// 			$('#lblvalue'+ id).text($('#txtvalue'+ id).val());
		// 			$('#lblkey'+ id).css('display', '');
		// 			$('#lblvalue'+ id).css('display', '');

		// 			$('#txtkey'+ id).css('display', 'none');
		// 			$('#txtvalue'+ id).css('display', 'none');
		// 			$('#imgEdit' + id).attr('src', 'img/icons/pencil.png');
		// 		});
		// 	} else {
		// 		$('#lblkey'+ id).css('display', 'none');
		// 		$('#lblvalue'+ id).css('display', 'none');

		// 		$('#txtkey'+ id).css('display', '');
		// 		$('#txtvalue'+ id).css('display', '');
		// 		$('#imgEdit' + id).attr('src', 'img/icons/accept.png');
		// 	}
		// }
		
		// $(".txtvalue").keydown(function(event){
		//   if(event.keyCode == 13){
		// 	event.preventDefault();
		// 	var id = $(this).attr("id");
		// 	id = id.substr(8);
		// 	editMetadata(id);
		//   }
		// });

	});
</script>
<div>
	<h1><?= __('Edit %s', $artist->name) ?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<p <?= (form_error("name") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Name:') ?></label>
				<input type="text" name="name" value="<?= htmlspecialchars_decode(set_value('name', $artist->name), ENT_NOQUOTES) ?>" id="name">
			</p>
			<?php if(isset($languages) && !empty($languages)) : ?>
            <?php foreach($languages as $language) : ?>
            <p <?= (form_error("description_".$language->key) != '') ? 'class="error"' : '' ?>>
                <label for="description"><?= __('Description')?> <?= '(' . $language->name . ')'; ?>:</label>
				<?php $trans = _getTranslation('artist', $artist->id, 'description', $language->key); ?>
                <textarea name="description_<?php echo $language->key; ?>" id="description"><?= htmlspecialchars_decode(set_value('description_'.$language->key, ($trans != null) ? $trans : $artist->description ), ENT_NOQUOTES) ?></textarea>
            </p>
            <?php endforeach; ?>
            <?php else : ?>
            <?php endif; ?>
            <p <?= (form_error("imageurl") != '') ? 'class="error"' : '' ?>>
				<label for="imageurl"><?= (!isset($venue)) ? 'Logo' : 'Image'?>:</label>
				<span class="evtlogo" <?php if($artist->imageurl != ''){ ?>style="width:150px; height:150px; background:transparent url('<?= image_thumb($artist->imageurl, 150, 150) ?>') no-repeat center center"<?php } ?>>&nbsp;</span>
				<input type="file" name="imageurl" id="imageurl" value="" class="logoupload" style="width:360px;" /><br />
				<span class="note" style="width:150px;"><?= __('Max size %s px by %s px', 2000,2000) ?></span>
				<br clear="all" />
				<?php if($artist->imageurl != '' && file_exists($this->config->item('imagespath') . $artist->imageurl)){ ?><span><a href="<?= site_url('artists/removeimage/'.$artist->id.'/'.$app->id) ?>" class="deletemap"><?= __('Remove') ?></a></span><?php } ?>
			</p><br clear="all"/>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:') ?></label>
				<input type="text" name="order" value="<?= set_value('order', $artist->order) ?>" id="order">
			</p>
<!-- 			<p <?= (form_error("number") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Number:') ?></label>
				<input type="text" name="number" value="<?= set_value('number', $artist->number) ?>" id="number">
			</p><br clear="all"/> -->
			<?php if($app->id != 393) : ?>
			<div class="row">
				<p style="min-height:0px;">
				<label for="tags"><?= __('Tags:')?></label>
				</p>
				<ul id="mytags" name="mytagsul"></ul>
			</div>
			<?php else: ?>
				<p>
					<label for="tagdelbar"><?= __('Brands:') ?></label><br/>
					<select name="tagdelbar">
						<option value="Audi" <?= (in_array('Audi', $tags)) ? 'selected' : '' ?>><?= __('Audi')?></option>
						<option value="Volkswagen" <?= (in_array('Volkswagen', $tags)) ? 'selected' : '' ?>><?= __('Volkswagen')?></option>
						<option value="skoda & Tweedehandswagens" <?= (in_array('skoda &amp; Tweedehandswagens', $tags)) ? 'selected' : '' ?>><?= __('Skoda & Tweedehandswagens')?></option>
						<option value="Carrosserie" <?= (in_array('Carrosserie', $tags)) ? 'selected' : '' ?>><?= __('Carrosserie')?></option>
					</select>
				</p>
			<?php endif; ?>
			<?php foreach($metadata as $m) : ?>
				<?php foreach($languages as $lang) : ?>
					<p <?= (form_error($m->qname.'_'.$lang->key) != '') ? 'class="error"' : '' ?>>
						<?= _getInputField($m, $app, $lang, $languages, 'artist', $artist->id); ?>
					</p>
				<?php endforeach; ?>
			<?php endforeach; ?>
<!-- 			<?php foreach($usedMetadata as $meta) : ?>
			<p <?= (form_error('usedmeta_'.$meta->key) != '') ? 'class="error"' : '' ?>>
				<label><?=ucfirst($meta->key) ?>:</label>
				<?php foreach($metadata as $data) {
					if($data->key == $meta->key) {
						$value = $data->value; 
					}
				}?>
				<input type="text" name="usedmeta_<?=$meta->key ?>" value="<?= set_value('usedmeta_'.$meta->key,$value) ?>" id="<?='usedmeta_'.$meta->key?>">
			</p>
			<?php endforeach; ?> -->
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<a href="<?= site_url('artists/edit/'.$artist->id) ?>" class="btn"><?= __('Cancel') ?></a>
				<button type="submit" class="btn primary"><?= __('Save') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
<!-- 		<?php if($app->id != 393) : ?>
		<p style="border-bottom-color:#000; border-bottom-style: dotted; border-bottom-width: 2px; min-height:20px;">
			<br clear="all" />
		</p>
		
        <form action="<?= 'metadata/add/'.$artist->id ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="edit">
			<p>
                <label style="float: left; width: 40px;"><?= __('Key:') ?></label>
                <input type="text" name="key" value="" id="key" style="float: left; width: 200px; margin-right: 10px;"/>
                <label style="float: left; width: 40px;"><?= __('Value:') ?></label>
                <input type="text" name="value" value="" id="value" style="float: left; width: 200px;"/>
				<input type="hidden" name="metadata" value="metadata" id="postback" />	
				<input type="hidden" name="table" value="artist" />
				<input type="hidden" name="redirect" value="<?= $this->uri->uri_string() ?>" />
				<button type="submit" class="save"><?= __('Add metadata') ?></button>
            </p>
            <?php if(isset($metadata)) : ?>
			
            <?php foreach($metadata as $data) : ?>
				<p style="min-height:10px;">
                <label id="lblkey<?=$data->id ?>" name="lblkey<?=$data->id ?>" style="float: left; width: 280px; margin-right: 10px;">
                    <?php echo $data->key; ?>: 
                </label>
                <label id="lblvalue<?=$data->id ?>" name="lblvalue<?=$data->id ?>" style="text-align:left; float: left; width: 280px; margin-right: 10px;">
                    <?php echo $data->value; ?>
                </label>
				<input type="text" value="<?=$data->key?>" id="txtkey<?=$data->id ?>" name="key<?=$data->id ?>" style="float: left; width: 270px; display:none; margin-right: 10px;"/>
				<input type="text" class="txtvalue" value="<?=$data->value?>"id="txtvalue<?=$data->id ?>" name="value<?=$data->id ?>" style="float: left; width: 270px; display: none; margin-right: 10px;"/>
				<a href='' id="<?=$data->id ?>" class="aedit metaEdit"><img id="imgEdit<?=$data->id ?>" src="img/icons/pencil.png" width="16" height="16" title="')?>Edit')?> alt="<?= __('Edit')?>" class="png" /></a>
                <a href='<?= site_url("metadata/delete/artist/" . $artist->id . '/' . $data->id) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="<?= __('Delete')?>" alt="<?= __('Delete')?>" class="png" /></a>
				<br class="clear" />
				</p>
            <?php endforeach; ?>
			
            <?php endif; ?>
        </form>
        <?php endif; ?> -->
	</div>
</div>