<h1><?= __("Payment Cancelled:") .' ' . $app->name ?></h1>
<p>Your payment has been canceled. Please try again to proceed.</p>
<a href="http://<?= $_SERVER['SERVER_NAME'] ?>/price/app/<?= $app->id ?>" type="submit" class="btn primary" style="margin-top:20px;float:right;"><?= __('Try again') ?></a>
