<h1><?=__('City Information')?></h1>
<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<div class="frmsessions">
	<form action="<?= site_url($this->uri->uri_string()) ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data" class="addevent">
		<?php if($error != ''): ?>
		<div class="error"><?= $error ?></div>
		<?php endif ?>
		<p>
		<?php
		$data = array(
				'name'			=> 'txt_cityinfo',
				'id'			=> 'txt_cityinfo',
				'value' 		=> html_entity_decode(set_value('txt_cityinfo', _currentApp()->info)),
				'width'			=> '800px',
				'height'		=> '500'
			);
		
		echo form_fckeditor($data);
		?>
		</p>
		<div class="buttonbar">
			<input type="hidden" name="postback" value="postback" id="postback">
			<button type="submit" class="btn primary"><?= __('Save') ?></button>
			<br clear="all" />
		</div>
		<br clear="all" />
	</form>
</div>