<div>
	<h1><?=__('Import RSS')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>        
				<p <?= (form_error("cataloggroup") != '') ? 'class="error"' : '' ?>>
					<label><?= __('Catalog group') ?></label>
					<select name="cataloggroup" id="cataloggroup">
						<option value=""><?= __('Select ...') ?></option>
						<?php foreach ($cataloggroups as $cataloggroup): ?>
							<option value="<?= $cataloggroup->id ?>" <?= set_select('cataloggroup', $cataloggroup->id) ?>><?= $cataloggroup->name ?></option>
						<?php endforeach ?>
					</select>
					<a href="<?= site_url('cataloggroups/add/'.$this->uri->segment(3).'/'.$this->uri->segment(4) . '/') ?>" style="position:absolute; margin:-22px 0 0 350px;"><?= __('Add cataloggroup') ?> &rsaquo;&rsaquo;</a>
				</p>
                <p <?= (form_error("url") != '') ? 'class="error"' : '' ?>>
                    <label for="url"><?= __('Url :') ?></label>
                    <input type="text" name="url" id="url" value="<?= set_value('url') ?>" />
                </p>
                <p <?= (form_error("refreshrate") != '') ? 'class="error"' : '' ?>>
                    <label for="refreshrate"><?= __('Refresh rate (minutes) :') ?></label>
                    <input type="text" name="refreshrate" id="refreshrate" value="<?= set_value('refreshrate') ?>" />
                </p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('IMPORT') ?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
    
    <?php if($currentRSSfeeds != null && !empty($currentRSSfeeds)): ?>
	<h1><?= __('Remove RSS') ?></h1>
	<div id="listview_wrapper" class="dataTables_wrapper">
        <table id="listview" class="display">
            
			<?php if ($currentRSSfeeds != FALSE): ?>
            <thead>
                <tr>
                    <th class="data">
                        Url
                    </th>
                    <th>
                    </th>
                </tr>
            </thead>
            <tbody>
			<?php foreach($currentRSSfeeds as $feed): ?>
                <tr>
                    <td><?= $feed->url; ?></td>
                    <td class="icon"><a href='<?= site_url($this->uri->segment(1) . "/deletesource/" . $feed->id . '/' . $this->uri->segment(5)) ?>' class="adelete"><img src="img/icons/delete.png" width="16" height="16" title="Delete" alt="Delete" class="png" /></a></td>
                </tr>
			<?php endforeach ?>
            </tbody>
			<?php else: ?>
				
			<?php endif ?> 
        </table>
	</div>
    <?php endif; ?>
</div>