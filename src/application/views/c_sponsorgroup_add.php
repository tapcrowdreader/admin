<div>
	<?php if($this->session->flashdata('event_feedback') != ''): ?>
	<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
	<?php endif ?>
	<h1><?= __('Add Sponsorgroup')?></h1>
	<div class="frmsessions">
		<form action="<?= site_url($this->uri->uri_string()) ?>" method="post" accept-charset="utf-8" class="edit">
			<?php if($error != ''): ?>
			<div class="error"><?= $error ?></div>
			<?php endif ?>
			<?php if(!isset($venues)): ?>
				<p <?= (form_error("events") != '') ? 'class="error"' : '' ?> style="display:none;">
					<label><?= __('Event')?></label>
					<select name="events" id="events">
						<option value=""><?= __('Select ...')?></option>
						<?php foreach ($events as $evt): ?>
							<option value="<?= $evt->id ?>" <?= set_select('events', $evt->id) ?>><?= $evt->name ?></option>
						<?php endforeach ?>
					</select>
				</p>
			<?php else: ?>
				<p <?= (form_error("venues") != '') ? 'class="error"' : '' ?> style="display:none;">
					<label><?= __('Venue')?></label>
					<select name="venues" id="venues">
						<option value=""><?= __('Select ...')?></option>
						<?php foreach ($venues as $vnu): ?>
							<option value="<?= $vnu->id ?>" <?= set_select('venues', $vnu->id) ?>><?= $vnu->name ?></option>
						<?php endforeach ?>
					</select>
				</p>
			<?php endif; ?>
			<?php foreach($languages as $language) : ?>
			<p <?= (form_error("name_".$language->key) != '') ? 'class="error"' : '' ?>>
				<label><?= __('Name '.'(' . $language->name . ')'); ?></label>
				<input type="text" name="name_<?=$language->key?>" value="<?= set_value('name_'.$language->key) ?>" id="name_<?=$language->key?>">
			</p>
			<?php endforeach; ?>
			<p <?= (form_error("order") != '') ? 'class="error"' : '' ?>>
				<label><?= __('Order:')?></label>
				<input type="text" name="order" value="<?= set_value('order') ?>" id="order">
			</p>
			<div class="buttonbar">
				<input type="hidden" name="postback" value="postback" id="postback">
				<button type="submit" class="btn primary"><?= __('Add Group')?></button>
				<br clear="all" />
			</div>
			<br clear="all" />
		</form>
	</div>
</div>