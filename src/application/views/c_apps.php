<?php if($this->session->flashdata('event_feedback') != ''): ?>
<div class="feedback fadeout"><?= $this->session->flashdata('event_feedback') ?></div>
<?php endif ?>
<?php if($this->session->flashdata('tc_error') != ''): ?>
<div class="error fadeout"><?= $this->session->flashdata('tc_error') ?></div>
<?php endif ?>
<h1><?= __('My mobile apps') ?></h1>
<br>
<!-- <a href="<?= site_url('apps/wizard/') ?>" class="add btn primary">
	<i class="icon-plus-sign icon-white"></i> <?= __('Create App with wizard') ?>
</a>  -->

<?php

# Validate account permissions
$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.create', $account) !== false) { ?>
	<a href="<?= site_url('apps/add/') ?>" class="add btn primary" style="margin-right:5px;">
		<i class="icon-plus-sign icon-white"></i> <?= __('New Mobile App') ?>
	</a><?php
} ?>

<?php if(_isAdmin()) : ?>
<a href="<?= site_url('appclone/') ?>" class="add btn primary" style="margin-right:5px;">
	<?=__('Copy App')?>
</a>
<?php endif; ?>
<br><br>
<div class="apps">
<?php if ($apps != FALSE): ?>
	<?php
		$picsubmit = FALSE;
	?>
	<?php foreach ($apps as $app): ?>
		<div class="app">
			<?php if(!empty($app->app_icon)) : ?>
			<img src="<?= $app->app_icon ?>" width="25" height="25" alt="TapCrowd App" class="customAppIcon">
			<?php else: ?>
			<img src="img/tapcrowd.png" width="25" alt="TapCrowd App">
			<?php endif; ?>
			
			<h1><a href="<?= site_url('apps/view/'.$app->id) ?>"><?= $app->name ?></a></h1>

			<?php
			if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.create', $account) !== false) { ?>
				<a href="<?= site_url('apps/delete/'.$app->id) ?>" class="delete btn"><i class="icon-remove"></i> <!--/**/--><?= __('Remove') ?></a><?php
			} ?>
			<a href="<?= site_url('apps/view/'.$app->id) ?>" class="manage btn"><i class="icon-pencil"></i> <?= __('Edit') ?></a>
		</div>
	<?php endforeach ?>
<?php endif ?>
</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.delete').click(function(e) {
			delurl = $(this).attr('href');
			jConfirm('<?= __('This will <b>remove</b> this app!<br />! This cannot be undone!') ?>', '<?= __('Remove Application') ?>', function(r) {
				if(r == true) {
					window.location = delurl;
					return true;
				} else {
					jAlert('<?= __('App not removed!') ?>', '<?= __('Info') ?>');
					return false;
				}
			});
			return false;
		});
	});
</script>
