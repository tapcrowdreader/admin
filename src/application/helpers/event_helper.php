<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */

function _updateTimeStamp($eventid) {
	$obj =& get_instance();
	
	$data = array(
			'timestamp' => time()
		);
	$obj->db->where('id', $eventid);
	$obj->db->update('event', $data);
	//$this->updateTimeStamp($id);
}

function _updateVenueTimeStamp($venueid) {
	$obj =& get_instance();
	
	$data = array(
			'timestamp' => time()
		);
	$obj->db->where('id', $venueid);
	$obj->db->update('venue', $data);
	//$this->updateTimeStamp($id);
}

function _showTodo($eventid) {
	$obj =& get_instance();
	
	$obj->db->where('id', $eventid);
	$event = $obj->db->get('event')->row();
	$obj->db->flush_cache();
	
	$todo = array();
	// Check map
	$obj->db->where('eventid', $eventid);
	if($obj->db->get('map')->num_rows() == 0) { $todo['Upload a floorplan'] = 'event/edit/'.$eventid; }
	$obj->db->flush_cache();
	
	// Check logo
	if($event->eventlogo == '') $todo['Upload a logo'] = 'event/edit/'.$eventid;
	
	// Check modules
	
	
	// Return todolist
	return $todo;
}

function _currentEvent() {
	$obj =& get_instance();
	$obj->db->where('id', $obj->session->userdata('eventid'));
	return $obj->db->get('event')->row();
}

function _currentVenue() {
	$obj =& get_instance();
	$obj->db->where('id', $obj->session->userdata('venueid'));
	return $obj->db->get('venue')->row();
}

function _currentArtist() {
	$obj =& get_instance();
	$obj->db->where('id', $obj->session->userdata('artistid'));
	return $obj->db->get('artist')->row();
}