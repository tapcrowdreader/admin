<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @param object $m Metadata object (result row)
 * @return string html
 */
function _getInputField($m, $app, $lang = '', $languages = array(), $objectType = '', $objectId = '') {
	$str = '';
	$CI =& get_instance();

	$langstring = '';
	if(!empty($lang) && count($languages) > 1) {
		$langstring = ' (' . $lang->name . ')';
	}

	if($lang->key != $app->defaultlanguage) {
		if(!empty($objectType) && !empty($objectId)) {
			$trans = $CI->db->query("SELECT value FROM tc_metavalues WHERE metaId = $m->id AND parentType = '$objectType' AND parentId = '$objectId' AND language = '$lang->key' LIMIT 1");
			if($trans->num_rows() > 0) {
				$trans = $trans->row();
				$m->value = $trans->value;
			}
		}

		// $transname = _getTranslation('tc_metadata', $m->id, 'name', $lang->key);
		// if($transname) {
		// 	$m->name = $transname;
		// }
	}

	switch($m->type) {
		case 'text':
			$str = '<label>'.ucfirst($m->name).$langstring.':</label>
				<input type="text" value="'.set_value($m->qname.'_'.$lang->key, $m->value) .'" name="'.$m->qname .'_'.$lang->key.'" /> ';
			break;
		case 'email':
			$str = '<label>'.ucfirst($m->name).$langstring.':</label>
				<input type="email" value="'.set_value($m->qname.'_'.$lang->key, $m->value) .'" name="'.$m->qname .'_'.$lang->key.'" /> ';
			break;
		case 'phone':
			$str = '<label>'.ucfirst($m->name).$langstring.':</label>
				<input type="text" value="'.set_value($m->qname.'_'.$lang->key, $m->value) .'" name="'.$m->qname .'_'.$lang->key.'" /> ';
			break;
		case 'url':
			$str = '<label>'.ucfirst($m->name).$langstring.':</label>
				<input type="url" value="'.set_value($m->qname.'_'.$lang->key, $m->value) .'" name="'.$m->qname .'_'.$lang->key.'" /> ';
			break;
		case 'datetime':
			$date = date('d-m-Y', strtotime($m->value));
			$time = date('H:i', strtotime($m->value));

			if(empty($m->value)) {
				$date =  date('d-m-Y', time());
				$time = date('H:i', time());
			}

			$str = '<label>'.ucfirst($m->name).$langstring.':</label>
				<input type="text" name="'.$m->qname.'-date_'.$lang->key.'" value="'.set_value($m->qname.'-date_'.$lang->key, $date).'" id="'.$m->qname.'-date_'.$lang->key.'" class="datepicker datepickerMeta">&nbsp;
				<input type="text" name="'.$m->qname.'-time_'.$lang->key.'" value="'.set_value($m->qname.'-time_'.$lang->key, $time).'" id="'.$m->qname.'-time_'.$lang->key.'" class="fromtill fromtillMeta"><br clear="all"/>';
			break;
		case 'image':
			$str = '<label>'.ucfirst($m->name).$langstring.':</label>';
			if(!empty($m->value)) {
				$str .= '<span class="evtlogo"><img src="'.$CI->config->item('publicupload') . $m->value .'" width="50" /></span><br clear="all" />';
				$str .= '<span><a href="'.site_url('metadata/removefile/'.$m->id.'/'.$objectType.'/'.$objectId.'/'.md5('tcadm'.$objectType.$objectId)).'" class="deletemap deletemetaimage">'. __('Remove') .'</a></span>';
			}

			$str .= '<input type="file" value="'.set_value($m->qname.'_'.$lang->key, $m->value) .'" name="'.$m->qname .'_'.$lang->key.'" />';
			break;
		case 'location':
			$latlon = explode(',', $m->value);
			$str = '<label>'.ucfirst($m->name).$langstring.':</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" id="input_addr" name="input_addr" value="" class="span7" />
							<button class="btn" type="button" id="show_location_button"><i class="icon-map-marker"></i>'.__('Show').'</button>
						</div>
						<div id="map_canvas" class="map"></div>
						<input type="text" id="input_lat" name="'.$m->qname.'_input_lat_'.$lang->key.'" value="'.set_value($m->qname.'_input_lat', $latlon[0]).'" />
						<input type="text" id="input_lng" name="'.$m->qname.'_input_lng_'.$lang->key.'" value="'.set_value($m->qname.'_input_lng', $latlon[1]).'" />
					</div>';
			break;
		case 'pdf':
			$str = '<label>'.ucfirst($m->name).$langstring.':</label>';
			if(!empty($m->value)) {
				$str .= '<span style="text-align:left;">'. substr($m->value, strrpos($m->value, '/') + 1) .'</span><br clear="all" />';
				//$str .= '<span><a href="'.site_url('metadata/remove/'.$m->id).'" class="deletemap deletemetaimage">'. __('Remove') .'</a></span>';
			}

			$str .= '<input type="file" value="'.set_value($m->qname.'_'.$lang->key, $m->value) .'" name="'.$m->qname .'_'.$lang->key.'" />';
			break;
		case 'markericon':
			$basepath = $CI->config->item('imagespath');
			$imgpath = 'upload/markericons/' . $m->parentType . '_' . $m->parentId . '/';
			$markericons = array();
			if(file_exists($basepath.$imgpath)) {
				$iter = new DirectoryIterator($basepath.$imgpath);
				foreach($iter as $file) {
					if($file->isFile()) {
						$markericons[] = str_replace($basepath,'',$file->getPathname());
					}
				}
			}

			$qname = $m->qname . '_' . $lang->key;
			$resource_url = $CI->config->item('publicupload');
			$str .= '<label>'.ucfirst($m->name).$langstring.':</label><br />';
			foreach($markericons as $icon) {
				$sel = ($m->value == $icon)? ' checked="true"' : '';
				$str.= '<div class="pull-left">
					<label class="radio">
						<input type="radio" name="'.$qname.'" value="'.$icon.'"'.$sel.'>
						<img src="'.$resource_url.$icon.'" class="img-polaroid" width="50">
					</label>
					</div>';
			}
			$str.= '<div><input name="'.$qname.'" type="file" class="span8" /></div>';
			break;
		default:
			$str = '<label>'.ucfirst($m->name).$langstring.':</label>
				<input type="text" value="'.set_value($m->qname.'_'.$lang->key, $m->value) .'" name="'.$m->qname .'_'.$lang->key.'" /> ';
			break;
	}
	return $str;
}

function _validateInputField($m, $value) {
	$return = true;
	$CI =& get_instance();

	//possible extra validation for some metadata types
	switch($m->type) {
		case 'email':
			if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
				if(!empty($value)) {
					$return = false;
				}
			}
			break;
		case 'url':
			if(!filter_var($value, FILTER_VALIDATE_URL)) {
				if(!empty($value)) {
					$return = false;
				}
			}
			break;
		default:
			$return = true;
			break;
	}

	return $return;
}

function _saveInputField($m, $value, $parentType, $parentId, $app, $lang) {
	$CI =& get_instance();
	if($m->type == 'image') {
		if(!empty($value['name'])) {
			$uploadpath = $CI->config->item('imagespath') . "upload/metadata/".$app->id.'/'.$parentType .'/'.$parentId.'/';
			if(!is_dir($uploadpath)){
				mkdir($uploadpath, 0775, TRUE);
			}

			if(copy($value['tmp_name'], $uploadpath.$value['name'])) {
				if (move_uploaded_file($value['tmp_name'], $uploadpath.$value['name'])) {
					$value = "upload/metadata/".$app->id.'/'.$parentType .'/'.$parentId.'/'.$value['name'];
					$CI->metadata_model->removeFieldFromObject($parentType, $parentId, $m->id, $lang->key);
					$CI->general_model->insert('tc_metavalues', array(
						'metaId' => $m->id,
						'parentType' => $parentType,
						'parentId' => $parentId,
						'name' => $m->name,
						'type' => $m->type,
						'value' => $value,
						'language' => $lang->key
					));
				}
			}
		}
	} elseif($m->type == 'pdf') {
		// $CI->load->library('crocodoc'); // Loading Crocodoc API library
		if(!empty($value['name'])) {
			$uploadpath = $CI->config->item('imagespath') . "upload/metadata/".$app->id.'/'.$parentType .'/'.$parentId.'/';
			if(!is_dir($uploadpath)){
				mkdir($uploadpath, 0775, TRUE);
			}

			if(copy($value['tmp_name'], $uploadpath.$value['name'])) {
				if (move_uploaded_file($value['tmp_name'], $uploadpath.$value['name'])) {
					// echo $value['name'].' - move<br />';
					// Call Crocodoc API
					// $response = $CI->crocodoc->upload( $CI->config->item('publicupload') . "upload/metadata/".$app->id.'/'.$parentType .'/'.$parentId.'/'.$value['name'] );
					// $uuid = isset( $response['uuid'] ) ? $response['uuid'] : '';
					// $value = "upload/metadata/".$app->id.'/'.$parentType .'/'.$parentId.'/'.$value['name'] . '__' . $uuid;
					$value = "upload/metadata/".$app->id.'/'.$parentType .'/'.$parentId.'/'.$value['name'];
					$CI->metadata_model->removeFieldFromObject($parentType, $parentId, $m->id, $lang->key);
					$CI->general_model->insert('tc_metavalues', array(
						'metaId' => $m->id,
						'parentType' => $parentType,
						'parentId' => $parentId,
						'name' => $m->name,
						'type' => $m->type,
						'value' => $value,
						'language' => $lang->key
					));
				}
			}
		}
	} elseif($m->type == 'markericon') {

		# In case there is an HTTP Upload, upload the file
		if(isset($value['tmp_name']) && is_uploaded_file($value['tmp_name'])) {
			$basepath = $CI->config->item('imagespath');
			$imgpath = 'upload/markericons/' . $m->parentType . '_' . $m->parentId . '/';
			if(!is_dir($basepath.$imgpath)) {
				mkdir($basepath.$imgpath, 0775, true);
			}
			if(!move_uploaded_file($value['tmp_name'], $basepath.$imgpath.$value['name'])) {
				throw new \RuntimeException;
			}
			$value = $imgpath.$value['name'];
		}

		# Insert into the database
		$CI->metadata_model->removeFieldFromObject($parentType, $parentId, $m->id, $lang->key);
		$CI->general_model->insert('tc_metavalues', array(
			'metaId' => $m->id,
			'parentType' => $parentType,
			'parentId' => $parentId,
			'name' => $m->name,
			'type' => $m->type,
			'value' => $value,
			'language' => $lang->key
		));

	} else {
		if($value == ',') {
			$value = '';
		}

		$CI->metadata_model->removeFieldFromObject($parentType, $parentId, $m->id, $lang->key);
		if(!empty($value)) {
			$CI->general_model->insert('tc_metavalues', array(
				'metaId' => $m->id,
				'parentType' => $parentType,
				'parentId' => $parentId,
				'name' => $m->name,
				'type' => $m->type,
				'value' => $value,
				'language' => $lang->key
			));
		}
	}
}

function _getPostedField($m, $post, $files, $lang) {
	if($m->type == 'image' || $m->type == 'pdf') {
		if(!isset($files[$m->qname.'_'.$lang->key])) {
			return false;
		}
		$postfield = $files[$m->qname.'_'.$lang->key];
	} elseif($m->type == 'datetime') {
		if(!isset($post[$m->qname.'-date_'.$lang->key])) {
			return false;
		}
		$postfield = $post[$m->qname.'-date_'.$lang->key] . ' ' . $post[$m->qname.'-time_'.$lang->key] . ':00';
	} elseif($m->type == 'location') {
		if(!isset($post[$m->qname.'_input_lat_'.$lang->key])) {
			return false;
		}
		$postfield = $post[$m->qname.'_input_lat_'.$lang->key] . ',' . $post[$m->qname.'_input_lng_'.$lang->key];

	} elseif($m->type == 'markericon') {

		$qname = $m->qname.'_'.$lang->key;
		if($_FILES[$qname]['name'] != '') {
			$postfield = $_FILES[$qname];
		} else {
			$postfield = filter_input(INPUT_POST, $qname);
		}
		if(empty($postfield)) return false;

	} else {
		if(!isset($post[$m->qname.'_'.$lang->key])) {
			return false;
		}
		$postfield = $post[$m->qname.'_'.$lang->key];
	}

	return $postfield;
}
function _setRules($m, $lang) {
	$rules = array();
	$rule = array();

	$rule['qname'] = $m->qname.'_'.$lang->key;
	$rule['name'] = $m->name .' ('.$lang->name.')';
	$rule['validation'] = 'trim';
	$rule = (object)$rule;

	if($m->type == 'datetime') {
		$rule->qname = $m->qname.'-date_'.$lang->key;
		$rules[] = $rule;
		$rule2 = array();
		$rule2['qname'] = $m->qname.'-time_'.$lang->key;
		$rule2['name'] = $m->name .' ('.$lang->name.')';
		$rule2['validation'] = 'trim';
		$rule2 = (object)$rule2;
		$rules[] = $rule2;
	} elseif($m->type == 'location') {
		$rule->qname = $m->qname.'_input_lat_'.$lang->key;
		$rules[] = $rule;
		$rule2 = array();
		$rule2['qname'] = $m->qname.'_input_lng_'.$lang->key;
		$rule2['name'] = $m->name .' ('.$lang->name.')';
		$rule2['validation'] = 'trim';
		$rule2 = (object)$rule2;
		$rules[] = $rule2;
	} else {
		$rules[] = $rule;
	}

	return $rules;
}

function _checkMultilang($m, $lang, $app) {
	if($lang == $app->defaultlanguage) {
		return true;
	}

	$disabledtypes = array('image', 'date', 'datetime', 'location', 'map', 'markericon', 'fax', 'email', 'phone');
	if(in_array(strtolower($m->type), $disabledtypes)) {
		return false;
	}

	return true;
}
