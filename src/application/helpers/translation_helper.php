<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */

function _getTranslation($table, $tableid, $fieldname, $language, $object = '') {
    $obj =& get_instance();
	$str = "";

    $res = $obj->db->query("SELECT translation FROM translation WHERE `table` = '".$table."' AND `tableid` = '".$tableid."' AND `fieldname` = '".$fieldname."' AND `language` = '".$language."'");
    if($res->num_rows() == 0){
    	if(!empty($object)) {
    		return $object->{$fieldname};
    	}
    	return FALSE;
    } else {
    	$str = str_replace('$channelname', $obj->channel->name, $res->row()->translation);
    }

    return $str;
}

class Notifications implements Countable
{
	protected $alerts;

	public static function getInstance()
	{
		static $instance;
		if(!isset($instance)) {
			$class = __CLASS__;
			$instance = new $class;
		}
		return $instance;
	}

	protected function __construct()
	{
		$this->alerts =& $_SESSION['system_notifications'];
		if(empty($this->alerts)) {
			$types = array('info','success','error','warning','danger');
			$this->alerts = (object)array_fill_keys( $types, array());
		}
	}

	public function alert($msg, $severity = 'success')
	{
		$types = array('info','success','error','warning','danger');
		if(!in_array($severity, $types)) $severity = 'info';
		array_push($this->alerts->$severity, $msg);
	}

	public function __toString()
	{
		$fmt = '<div class="alert %s"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>%s:</strong> %s</div>';
		foreach($this->alerts as $severity => $messages) {
			foreach($messages as $message) {
				printf($fmt, "alert-$severity", ucfirst($severity), $message);
			}
		}
		unset($_SESSION['system_notifications']);
	}

	/**
	 * Returns the total alert count
	 *
	 * @return int
	 */
	public function count()
	{
		$cnt = 0;
		foreach($this->alerts as $messages) $cnt += count($messages);
		return $cnt;
	}
}

function alert($msg, $severity = 'success')
{
	return Notifications::getInstance()->alert($msg, $severity);
}


function __($key)
{
	$obj =& get_instance();
	static $_localisation_table;
	static $_userdata;
	static $debugmode;

	# Initiatiate static table
	if(!isset($_localisation_table)) {
		$_localisation_table = array();
	}

	# Get args, crc
	$args = func_get_args();
	$crc = crc32($key);

	# Set userdata
	if(!isset($_userdata)) {
		$language = filter_input(INPUT_COOKIE, 'language');

		if(!in_array($language, getAvailableLanguages())) {
			$language = 'en';
		}

		$_userdata = array(
			'language' => $language,
			'country' => '',
			'channel' => (int)\Tapcrowd\API::getCurrentChannel()->channelId,
		);

		# Set debugmode
		$debugmode = (filter_input(INPUT_GET, 'debug'))? true : false;
	}

	# Lookup in static table
	if(isset($_localisation_table[$crc])) {
		$args[0] = $_localisation_table[$crc];
		return call_user_func_array('sprintf', $args);
	}

	# Do query
	$sql = "SELECT v FROM localization WHERE language='%s' AND id=%d AND source='cms' AND channel=%d  AND country='%s' LIMIT 1";
	$sql = sprintf($sql, $_userdata['language'], $crc, $_userdata['channel'], $_userdata['country']);
	$res = $obj->db->query($sql);

	$toTranslate = "";
	# When no results are found, insert into the queue
	if($res->num_rows() == 0) {
		$local = array(
			'id' => $crc,
			'k'	=> $key,
			'language'	=> $_userdata['language'],
			'country'	=> $_userdata['country'],
			'channel'	=> $_userdata['channel'],
		);

		$sql = "INSERT INTO localization_queue (id, k, language, country, channel) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE cnt = cnt+1";
		$obj->db->query($sql,$local);

		if($_userdata['channel'] != 1) {
			// fallback on default channel translation
			$sql = "SELECT v FROM localization WHERE language='%s' AND id=%d AND source='cms' AND channel=%d  AND country='%s' LIMIT 1";
			$sql = sprintf($sql, $_userdata['language'], $crc, 1, $_userdata['country']);
			$res = $obj->db->query($sql);
			if ($res->num_rows() == 1) {
				$toTranslate = $res->row()->v;
			}
		}
	} else if ($res->num_rows() == 1) {
		$toTranslate = $res->row()->v;
	}

	#
	# BUG Fix empty tranlations @tomvdp 20120904
	$toTranslate = trim($toTranslate);
	if(empty($toTranslate)) $toTranslate = $key;

	# Store in static table
	$_localisation_table[$crc] = $toTranslate;

	# Update first function argument (key)
	$args[0] = $toTranslate;

	# Debug mode
	if($debugmode == true) {
		return '$$' . call_user_func_array('sprintf', $args) . '**';
	}

	return call_user_func_array('sprintf', $args);
}

function getAvailableLanguages() {
	// $obj =& get_instance();
	// $obj->load->model('translation_model');
	// return $obj->translation_model->getLanguages();
	return array('en', 'nl', 'pt');
}

function getUserInformation() {
	if (isset($_COOKIE['language'])) {
		return array('locale' => $_COOKIE['language'] . '_BE', 'channel' => '0');
	} else {
		return array('locale' => 'en_UK', 'channel' => '0');
	}
}