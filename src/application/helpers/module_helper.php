<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */

function _autocomplete($table, $field, $like) {
	$CI =& get_instance();
	$CI->db->like($field, $like, 'both');
	$res = $CI->db->get($table);
	if($res->num_rows() == 0) return array();
	return $res->result();
}

function _autocompletetags($appid, $like) {
	$CI =& get_instance();
	$CI->db->where('appid', $appid);
	$CI->db->like('tag', $like, 'both');
	$res = $CI->db->get('tag');
	if($res->num_rows() == 0) return array();
	return $res->result();
}

function _getSidebarEventModules($eventid) {
	$CI =& get_instance();
	$modules = $CI->db->query("SELECT mt.* FROM module m, moduletype mt WHERE m.event = $eventid AND mt.id = m.moduletype");
	if($modules->num_rows() > 0) {
		$str = '<ul id="sbmodules">';
		foreach($modules->result() as $module) {
			if($module->name == "Sessions") {
				if(_currentApp()->apptypeid == 3){
					//$str.= '<li><a href="'.site_url('sessiongroups/event/'.$eventid).'" '.($CI->uri->segment(1) == 'sessiongroups' ? "class='sel'" : "").'>Datums</a></li>';
					$str.= '<li><a href="'.site_url('sessions/event/'.$eventid).'" '.($CI->uri->segment(1) == 'sessions' ? "class='sel'" : "").'>Line-up</a></li>';
				} else {
					//$str.= '<li><a href="'.site_url('sessiongroups/event/'.$eventid).'" '.($CI->uri->segment(1) == 'sessiongroups' ? "class='sel'" : "").'>Sessiongroups</a></li>';
					$str.= '<li><a href="'.site_url('sessions/event/'.$eventid).'" '.($CI->uri->segment(1) == 'sessions' ? "class='sel'" : "").'>Sessions</a></li>';
				}
			} else if($module->name == "Products, brands & services catalog") {
				$str.= '<li><a href="'.site_url('brands/event/'.$eventid).'" '.($CI->uri->segment(1) == 'brands' || $CI->uri->segment(1) == 'categories' ? "class='sel'" : "").'>'.character_limiter($module->name, 19).'</a></li>';
			} else {
				$str.= '<li><a href="'.site_url($module->controller.'/event/'.$eventid).'" '.($CI->uri->segment(1) == $module->controller ? "class='sel'" : "").'>'.character_limiter($module->name, 19).'</a></li>';
			}
		}
		$str .= '</ul>';
		echo $str;
	}
}

function _getSidebarVenueModules($venueid) {
	if($venueid == FALSE || $venueid == 0 || $venueid == '') return '';
	$CI =& get_instance();
	$modules = $CI->db->query("SELECT mt.* FROM module m, moduletype mt WHERE m.venue = $venueid AND mt.id = m.moduletype");
	if($modules->num_rows() > 0) {
		$str = '<ul id="sbmodules">';
		foreach($modules->result() as $module) {
			if($module->name == "Agenda") {
				$str.= '<li><a href="'.site_url('sessiongroups/venue/'.$venueid).'" '.($CI->uri->segment(1) == 'sessiongroups' ? "class='sel'" : "").'>&nbsp;&nbsp;Agendagroups</a></li>';
				$str.= '<li><a href="'.site_url('sessions/venue/'.$venueid).'" '.($CI->uri->segment(1) == 'sessions' ? "class='sel'" : "").'>&nbsp;&nbsp;Agenda</a></li>';
			} else if($module->name == "Products, brands & services catalog") {
				$str.= '<li><a href="'.site_url('brands/venue/'.$venueid).'" '.($CI->uri->segment(1) == 'brands' || $CI->uri->segment(1) == 'categories' ? "class='sel'" : "").'>&nbsp;&nbsp;'.character_limiter($module->name, 19).'</a></li>';
			} else {
				$str.= '<li><a href="'.site_url($module->controller.'/venue/'.$venueid).'" '.($CI->uri->segment(1) == $module->controller ? "class='sel'" : "").'>&nbsp;&nbsp;'.character_limiter($module->name, 19).'</a></li>';
			}
		}
		$str .= '</ul>';
		echo $str;
	}
}