<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */

function _appType($appid) {
	$obj =& get_instance();
	
	$obj->db->where('id', $appid);
	$app = $obj->db->get('app')->$row();
	
	return $app->apptypeid;
}

function _currentApp() {
	$obj =& get_instance();
	//var_dump($obj->session->userdata);	

	// $obj->db->where('id', $obj->session->userdata('appid'));
	$appid = $obj->session->userdata('appid');
	if($appid) {
		$app = $obj->db->query("SELECT app.*, apptypefamily.id as familyid FROM app INNER JOIN apptype ON apptype.id = app.apptypeid INNER JOIN apptypefamily ON apptype.familyid = apptypefamily.id WHERE app.id = $appid LIMIT 1")->row();
		 $app->flavortitle = $obj->db->query("SELECT title FROM apptype WHERE id = $app->apptypeid LIMIT 1")->row()->title;
		 return $app;
	}
	return false;
}

function externalIds() {
  $obj =& get_instance();
  $appid = $obj->session->userdata('appid');
  if($appid) {
      $app = $obj->db->query("SELECT app.show_external_ids_in_cms FROM app WHERE app.id = $appid LIMIT 1")->row();
      return $app->show_external_ids_in_cms;
  }
  return false;
}

function _appID() {
	$obj =& get_instance();
	if($obj->session->userdata('appid') && $obj->session->userdata('appid') != ''){
		return $obj->session->userdata('appid');
	} else {
		//redirect('apps');
	}
}

function _appsToDo($id) {
	if($id == FALSE) return FALSE;
	$obj =& get_instance();
	$obj->db->where('id', $id);
	$res = $obj->db->get_where('app');
	if($res->num_rows() == 0) return FALSE;
	$app = $res->row();
	
	// check op aanmaak event
	$count = 0;
	$apptodo = "";
	switch($app->apptypeid) {
		case 1:
			$obj->db->where('appid', $app->id);
			$count = $obj->db->get('appevent')->num_rows();
			//$apptodo = "You have to add one or more event(s) to your app. <a href='".site_url('apps/manage').'/'.$app->id."'>Add event now</a>";
			break;
		case 2:
			$obj->db->where('appid', $app->id);
			$count = $obj->db->get('appvenue')->num_rows();
			//$apptodo = "You have to add one or more venue(s) to your app. <a href='".site_url('apps/manage').'/'.$app->id."'>Add venue now</a>";
			break;
		case 3:
			break;
		case 4:
			break;
	}
	
	if($count == 0) {
		return $apptodo;
	}
	return FALSE;
	// check op modules van events
	
	// check op data in modules van events
	
	// check op submit
	
}
function checkBreadcrumbsVenue($app, $array) {
	if($app->familyid != 2 && $app->apptypeid != 2) {
		return array_unshift_assoc($array, ($app->apptypeid == 5) ? __("POI's") : __("Venues"), "venues");
	} 
	return $array;
}
function array_unshift_assoc(&$arr, $key, $val)
{
    $arr = array_reverse($arr, true);
    $arr[$key] = $val;
    return array_reverse($arr, true);
} 

/**
 * restrictLength()
 *
 * Restricts length of a string, that will end on ... when restricted.
 *
 * @param string $s String to restrict in length.
 * @param int $max Max length of the string.
 * @param bool $encode Run the output trough textToHtml() ?
 * @return string The trimmed string.
 */
function restrictLength($s, $max = 40, $encode = true) {
	$s = $s;

	if(strlen($s) > $max) {
		$s = substr($s, 0, $max - 3) . "...";
	}

	if($encode) {
		$s = $s;
	}

	return $s;
}

function analyticsgrant($appid) {
  $obj =& get_instance();
  if($appid) {
    $app = $obj->db->query("SELECT * FROM analyticsgrant WHERE appid = ? LIMIT 1", $appid);
    if($app->num_rows() == 0) {
      return false;
    } else {
      return true;
    }
  }
  return false;
}

function getCurrentChannel() {
  $channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();
  return $channel;
}

function _getAllCountries() {
$countries = array(
  "AF" => "Afghanistan",
  "AL" => "Albania",
  "DZ" => "Algeria",
  "AS" => "American Samoa",
  "AD" => "Andorra",
  "AO" => "Angola",
  "AI" => "Anguilla",
  "AQ" => "Antarctica",
  "AG" => "Antigua And Barbuda",
  "AR" => "Argentina",
  "AM" => "Armenia",
  "AW" => "Aruba",
  "AU" => "Australia",
  "AT" => "Austria",
  "AZ" => "Azerbaijan",
  "BS" => "Bahamas",
  "BH" => "Bahrain",
  "BD" => "Bangladesh",
  "BB" => "Barbados",
  "BY" => "Belarus",
  "BE" => "Belgium",
  "BZ" => "Belize",
  "BJ" => "Benin",
  "BM" => "Bermuda",
  "BT" => "Bhutan",
  "BO" => "Bolivia",
  "BA" => "Bosnia And Herzegowina",
  "BW" => "Botswana",
  "BV" => "Bouvet Island",
  "BR" => "Brazil",
  "IO" => "British Indian Ocean Territory",
  "BN" => "Brunei Darussalam",
  "BG" => "Bulgaria",
  "BF" => "Burkina Faso",
  "BI" => "Burundi",
  "KH" => "Cambodia",
  "CM" => "Cameroon",
  "CA" => "Canada",
  "CV" => "Cape Verde",
  "KY" => "Cayman Islands",
  "CF" => "Central African Republic",
  "TD" => "Chad",
  "CL" => "Chile",
  "CN" => "China",
  "CX" => "Christmas Island",
  "CC" => "Cocos (Keeling) Islands",
  "CO" => "Colombia",
  "KM" => "Comoros",
  "CG" => "Congo",
  "CD" => "Congo, The Democratic Republic Of The",
  "CK" => "Cook Islands",
  "CR" => "Costa Rica",
  "CI" => "Cote D'Ivoire",
  "HR" => "Croatia (Local Name: Hrvatska)",
  "CU" => "Cuba",
  "CY" => "Cyprus",
  "CZ" => "Czech Republic",
  "DK" => "Denmark",
  "DJ" => "Djibouti",
  "DM" => "Dominica",
  "DO" => "Dominican Republic",
  "TP" => "East Timor",
  "EC" => "Ecuador",
  "EG" => "Egypt",
  "SV" => "El Salvador",
  "GQ" => "Equatorial Guinea",
  "ER" => "Eritrea",
  "EE" => "Estonia",
  "ET" => "Ethiopia",
  "FK" => "Falkland Islands (Malvinas)",
  "FO" => "Faroe Islands",
  "FJ" => "Fiji",
  "FI" => "Finland",
  "FR" => "France",
  "FX" => "France, Metropolitan",
  "GF" => "French Guiana",
  "PF" => "French Polynesia",
  "TF" => "French Southern Territories",
  "GA" => "Gabon",
  "GM" => "Gambia",
  "GE" => "Georgia",
  "DE" => "Germany",
  "GH" => "Ghana",
  "GI" => "Gibraltar",
  "GR" => "Greece",
  "GL" => "Greenland",
  "GD" => "Grenada",
  "GP" => "Guadeloupe",
  "GU" => "Guam",
  "GT" => "Guatemala",
  "GN" => "Guinea",
  "GW" => "Guinea-Bissau",
  "GY" => "Guyana",
  "HT" => "Haiti",
  "HM" => "Heard And Mc Donald Islands",
  "VA" => "Holy See (Vatican City State)",
  "HN" => "Honduras",
  "HK" => "Hong Kong",
  "HU" => "Hungary",
  "IS" => "Iceland",
  "IN" => "India",
  "ID" => "Indonesia",
  "IR" => "Iran (Islamic Republic Of)",
  "IQ" => "Iraq",
  "IE" => "Ireland",
  "IL" => "Israel",
  "IT" => "Italy",
  "JM" => "Jamaica",
  "JP" => "Japan",
  "JO" => "Jordan",
  "KZ" => "Kazakhstan",
  "KE" => "Kenya",
  "KI" => "Kiribati",
  "KP" => "Korea, Democratic People's Republic Of",
  "KR" => "Korea, Republic Of",
  "KW" => "Kuwait",
  "KG" => "Kyrgyzstan",
  "LA" => "Lao People's Democratic Republic",
  "LV" => "Latvia",
  "LB" => "Lebanon",
  "LS" => "Lesotho",
  "LR" => "Liberia",
  "LY" => "Libyan Arab Jamahiriya",
  "LI" => "Liechtenstein",
  "LT" => "Lithuania",
  "LU" => "Luxembourg",
  "MO" => "Macau",
  "MK" => "Macedonia, Former Yugoslav Republic Of",
  "MG" => "Madagascar",
  "MW" => "Malawi",
  "MY" => "Malaysia",
  "MV" => "Maldives",
  "ML" => "Mali",
  "MT" => "Malta",
  "MH" => "Marshall Islands",
  "MQ" => "Martinique",
  "MR" => "Mauritania",
  "MU" => "Mauritius",
  "YT" => "Mayotte",
  "MX" => "Mexico",
  "FM" => "Micronesia, Federated States Of",
  "MD" => "Moldova, Republic Of",
  "MC" => "Monaco",
  "MN" => "Mongolia",
  "MS" => "Montserrat",
  "MA" => "Morocco",
  "MZ" => "Mozambique",
  "MM" => "Myanmar",
  "NA" => "Namibia",
  "NR" => "Nauru",
  "NP" => "Nepal",
  "NL" => "Netherlands",
  "AN" => "Netherlands Antilles",
  "NC" => "New Caledonia",
  "NZ" => "New Zealand",
  "NI" => "Nicaragua",
  "NE" => "Niger",
  "NG" => "Nigeria",
  "NU" => "Niue",
  "NF" => "Norfolk Island",
  "MP" => "Northern Mariana Islands",
  "NO" => "Norway",
  "OM" => "Oman",
  "PK" => "Pakistan",
  "PW" => "Palau",
  "PA" => "Panama",
  "PG" => "Papua New Guinea",
  "PY" => "Paraguay",
  "PE" => "Peru",
  "PH" => "Philippines",
  "PN" => "Pitcairn",
  "PL" => "Poland",
  "PT" => "Portugal",
  "PR" => "Puerto Rico",
  "QA" => "Qatar",
  "RE" => "Reunion",
  "RO" => "Romania",
  "RU" => "Russian Federation",
  "RW" => "Rwanda",
  "KN" => "Saint Kitts And Nevis",
  "LC" => "Saint Lucia",
  "VC" => "Saint Vincent And The Grenadines",
  "WS" => "Samoa",
  "SM" => "San Marino",
  "ST" => "Sao Tome And Principe",
  "SA" => "Saudi Arabia",
  "SN" => "Senegal",
  "SC" => "Seychelles",
  "SL" => "Sierra Leone",
  "SG" => "Singapore",
  "SK" => "Slovakia (Slovak Republic)",
  "SI" => "Slovenia",
  "SB" => "Solomon Islands",
  "SO" => "Somalia",
  "ZA" => "South Africa",
  "GS" => "South Georgia, South Sandwich Islands",
  "ES" => "Spain",
  "LK" => "Sri Lanka",
  "SH" => "St. Helena",
  "PM" => "St. Pierre And Miquelon",
  "SD" => "Sudan",
  "SR" => "Suriname",
  "SJ" => "Svalbard And Jan Mayen Islands",
  "SZ" => "Swaziland",
  "SE" => "Sweden",
  "CH" => "Switzerland",
  "SY" => "Syrian Arab Republic",
  "TW" => "Taiwan",
  "TJ" => "Tajikistan",
  "TZ" => "Tanzania, United Republic Of",
  "TH" => "Thailand",
  "TG" => "Togo",
  "TK" => "Tokelau",
  "TO" => "Tonga",
  "TT" => "Trinidad And Tobago",
  "TN" => "Tunisia",
  "TR" => "Turkey",
  "TM" => "Turkmenistan",
  "TC" => "Turks And Caicos Islands",
  "TV" => "Tuvalu",
  "UG" => "Uganda",
  "UA" => "Ukraine",
  "AE" => "United Arab Emirates",
  "GB" => "United Kingdom",
  "US" => "United States",
  "UM" => "United States Minor Outlying Islands",
  "UY" => "Uruguay",
  "UZ" => "Uzbekistan",
  "VU" => "Vanuatu",
  "VE" => "Venezuela",
  "VN" => "Viet Nam",
  "VG" => "Virgin Islands (British)",
  "VI" => "Virgin Islands (U.S.)",
  "WF" => "Wallis And Futuna Islands",
  "EH" => "Western Sahara",
  "YE" => "Yemen",
  "YU" => "Yugoslavia",
  "ZM" => "Zambia",
  "ZW" => "Zimbabwe"
);

return $countries;

}
