<?php

/**
 * Validate an email address.
 *
 * Provide email address (raw input)
 * Returns true if the email address has the email
 * address format and the domain exists.
 *
 * @param string $email
 * @return bool
 */
function isValidEmail( $email )
{
	$isValid = true;
	$atIndex = strrpos($email, "@");
	if (is_bool($atIndex) && !$atIndex) {
		$isValid = false;
	} else {
		$domain = substr($email, $atIndex+1);
		$local = substr($email, 0, $atIndex);
		$localLen = strlen($local);
		$domainLen = strlen($domain);

		if($localLen < 1 || $localLen > 64) {
			# local part length exceeded
			$isValid = false;
		}
		elseif($domainLen < 1 || $domainLen > 255) {
			# domain part length exceeded
			$isValid = false;
		}
		elseif($local[0] == '.' || $local[$localLen-1] == '.') {
			# local part starts or ends with '.'
			$isValid = false;
		}
		elseif(preg_match('/\\.\\./', $local)) {
			# local part has two consecutive dots
			$isValid = false;
		}
		elseif(!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
			# character not valid in domain part
			$isValid = false;
		}
		elseif(preg_match('/\\.\\./', $domain)) {
			# domain part has two consecutive dots
			$isValid = false;
		}
		elseif(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
			# character not valid in local part unless
			# local part is quoted
			if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
				$isValid = false;
			}
		}

// var_dump($domain, $isValid,checkdnsrr($domain,"MX") ,checkdnsrr($domain,"A") );
// $mxhosts = array();
// if($isValid) var_dump($domain, !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")));

		if($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A"))) {
			# domain not found in DNS
			$isValid = false;
		}
	}
	return $isValid;
}


#
# Tests
#
// function __testIsValidEmail()
// {
// 	$input = array(
// 		'' => false,
// 		'test@test.be' => true,
// 		'test@gmailc.idjd' => false,
// 		'webdesign@contactpunt.be' => true,
// 		'test@test.com' => true,
// 		'Edwig.van.hooydonck@telent.be' => false,
// 		'sdDS\@fdg@f.be' => false,
// 	);
// 	while(list($email, $valid) = each($input)) {
// 		if($valid !== isValidEmail($email)) {
// 			printf("Input '%s' should have been %d\n", $email, $valid);
// 		}
// 	}
//
// }
// __testIsValidEmail();


