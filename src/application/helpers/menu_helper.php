<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
function _eventsOfApp($app) {
    //query goed zetten
	$str = '';
	$CI =& get_instance();
    $appid = $app->id; 
	$res = $CI->db->query("SELECT e.*, t.name as typename FROM appevent ae, event e, eventtype t WHERE ae.appid = '$appid' AND e.id = ae.eventid AND e.eventtypeid = t.id AND e.deleted = 0 ORDER BY id DESC");
	

    foreach($res->result() as $row) {
        //$eventids[] = $row->eventid;
		$str.= '<li><a href="'.site_url('event/view/'.$row->id).'">'.$row->name.'</a></li>';
    }
    

    
    return $str;
}

function _venuesOfApp($app) {
    //query goed zetten
	$str = '';
	$CI =& get_instance();
    $appid = $app->id; 
    $CI->db->where('appid', $appid);
    $res = $CI->db->get('appvenue');
	
    $venueids = array();
    foreach($res->result() as $row) {
        $venueids[] = $row->venueid;
    }
    
    if($venueids != null && !empty($venueids)) {
        $count = 0;
        $venuesQuery = $CI->db->where_in('id', $venueids);
		$CI->db->where('deleted', '0');
        $venues = $CI->db->get('venue');
		foreach($venues->result() as $row) {
			$str.= '<li><a href="'.site_url('venue/view/'.$row->id).'">'.$row->name.'</a></li>';
		}
    }
    
    return $str;
}
