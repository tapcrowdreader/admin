<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */

function _authed()
{
	static $authed;
	if(isset($authed)) return $authed;

	# Verify Session
	$session = \Tapcrowd\Model\Session::getInstance();
	try {
		$session->startSession();
	} catch(\Exception $e) {
		$session->closeSession();
		$CI =& get_instance();
		$CI->session->sess_destroy();
		redirect('/auth/login?errormsg=sessionexpired');
	}

	$account = \Tapcrowd\API::getCurrentUser();
	if($account === false || $account->accountId == 3) {
		\Tapcrowd\API::redirectToLogin();
	} else {
		# make shure CI session data isset
		$CI =& get_instance();
		$CI->session->set_userdata(array(
			'logged_in' => true,
			'name' => $account->fullname,
		));
		$authed = true;
	}

	return $authed;
}

function _currentUser()
{
	$account = \Tapcrowd\API::getCurrentUser();
	if($account === false) {
		\Tapcrowd\API::redirectToLogin();
	} else {
		return $account;
	}
}

/**
 * TODO: This function should be aware of the current channel to differentiate between reseller/tapcrowd admins
 */
function _isAdmin()
{
	$account = \Tapcrowd\API::getCurrentUser();
	if($account === false) \Tapcrowd\API::redirectToLogin();

// 	if($account->role == 'admin' && $account->channelId == 1) return true; else return false;

	$channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();
	if($account->role == 'admin' && ($account->channelId == 1 || $account->channelId == $channel->id))
		return true;
	else return false;
}

function _actionAllowed($obj, $type, $return = '')
{

// var_dump($account, $obj, $type, $return); exit;

	# Get and validate account
	$account = \Tapcrowd\API::getCurrentUser();
	if($account === false) \Tapcrowd\API::redirectToLogin();

	# Validate object type
	if(!in_array($type, array('app','event','venue'))) {
		throw new \InvalidArgumentException('Given object type is not supported in this context');
	}

	# No object set, Redirect to 404 page
	if(empty($obj) || !isset($obj->id) || !is_numeric($obj->id)) redirect(404); //redirect($type . 's');

	# Check access rights
	$access = \Tapcrowd\API::hasEntityAccess($account, $type, (int)$obj->id);

	if($access === false) {
		$CI =& get_instance();
		$CI->session->set_flashdata('tc_error', 'We would recommend to manage your own apps!');
		redirect('apps');
	}

	# Return app if requested
	if($return == 'returnApp') {
		if($type == 'app') {
			return $obj;
		} else {
			$CI =& get_instance();
			$method = 'getRealAppOf' . ucfirst($type);
			$appid = $CI->app_model->$method($obj->id);
			return $CI->app_model->get($appid->appid);
		}
	} else {
		return true;
	}
}
