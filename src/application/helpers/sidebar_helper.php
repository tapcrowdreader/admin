<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
function _countEventsOfApp() {
    //query goed zetten
	$CI =& get_instance();
    $appid = _currentApp()->id; 
    $CI->db->where('appid', $appid);
    $res = $CI->db->get('appevent');
    $eventids = array();
    foreach($res->result() as $row) {
        $eventids[] = $row->eventid;
    }
    
    if($eventids != null && !empty($eventids)) {
        $count = 0;
        $eventsQuery = $CI->db->where_in('id', $eventids);
        $events = $CI->db->get('event');
        $events = $events->result();

        foreach($events as $event) {
            if($event->deleted == 0) {
                $count++;
            }
        }
        return (int) $count;
    }
    return null;
}

function _countVenuesOfApp() {
    //query goed zetten
	$CI =& get_instance();
    $appid = _currentApp()->id; 
    $CI->db->where('appid', $appid);
    $res = $CI->db->get('appvenue');
    $venueids = array();
    foreach($res->result() as $row) {
        $venueids[] = $row->venueid;
    }
    
    if($venueids != null && !empty($venueids)) {
        $count = 0;
        $venuesQuery = $CI->db->where_in('id', $venueids);
        $venues = $CI->db->get('venue');
        $venues = $venues->result();

        foreach($venues as $venue) {
            if($venue->deleted == 0) {
                $count++;
            }
        }
        return (int) $count;
    }
    return null;
}
