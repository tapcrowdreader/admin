<?php
/**
 * Tapcrowd API Integration layer
 * @author Tom Van de Putte
 */

namespace Tapcrowd;

require_once dirname(__DIR__) . '/models/session_model.php';

if (!defined('BASEPATH')) exit('No direct script access allowed');



class AccessViolation extends \RuntimeException {}
class AccountException extends \RuntimeException {}


class API
{
	private static $currentAccount;

	/**
	 * Returns the account data for the current loggedin user
	 *
	 * @return \Tapcrowd\Account
	 */
	public static function getCurrentUser()
	{
		return \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();

		# Resolve staticly
// 		if(isset(self::$currentAccount)) return self::$currentAccount;

// 		$CI =& get_instance();

		# If user is NOT loggedin, check for the Tapcrowd Account session cookie
// 		if($CI->session->userdata('logged_in') !== true) {

// 			self::$currentAccount = \Tapcrowd\Session_Model::getInstance()->getCurrentAccount();


// 			if(SANDBOX_MODE) {
// 				$tcsess = 1;
// 			} else {
// 				$tcsess = filter_input(INPUT_COOKIE, 'tcsess', FILTER_VALIDATE_INT);
// 			}
// 			if(empty($tcsess)) {
// 				self::$currentAccount = false;
// 			} else {
// 				self::$currentAccount = DB::getAccountForSessionId($tcsess);

// 				# Update CI session when loggedin
// 				if(self::$currentAccount instanceof Account) {

// 					if(self::$currentAccount->role == 'admin' && self::$currentAccount->channelId == 0) {
// 						$logged_in_as_admin = true;
// 					} else {
// 						$logged_in_as_admin = false;
// 					}
//
// 					$CI->session->set_userdata(array(
// 						'current_session' => (int)$tcsess,
// 						'logged_in' => true,
// 						'name' => self::$currentAccount->fullname,
// 						'logged_in_as_admin' => $logged_in_as_admin
// 					));
// 				}
// 			}
// 		}

		# User is logged in, Update the sessions end time
// 		else {
// 			$tcsess = (int)$CI->session->userdata('current_session');
// 			self::$currentAccount = DB::getAccountForSessionId($tcsess);
// 			if(self::$currentAccount instanceof Account) {
// 				DB::updateSessionEndtime($tcsess);
// 			}
// 		}

		# Return our result
// 		return self::$currentAccount;
	}

	/**
	 * Returns the current channel
	 *
	 * The current channel is:
	 *
	 * @return \Tapcrowd\Channel
	 */
	public static function getCurrentChannel()
	{
		return \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();
	}

	/**
	 * Get application using appId or bundleid
	 *
	 * @param int|string $identifier Either appId or app bundle
	 * @return \Tapcrowd\Application
	 * @throws \InvalidArgumentException
	 */
	public static function getApplication( $identifier )
	{
		if(is_numeric($identifier)) {
			if(!($identifier > 0)) throw new \InvalidArgumentException('Invalid Application Identifier');
			$app = DB::getApplicationUsingId((int)$identifier);
		} else {
			if(substr($identifier, 'com.tapcrowd.') === false) {
				throw new \InvalidArgumentException('Invalid Application bundle');
			} else {
				$app = DB::getApplicationUsingBundle($identifier);
			}
		}

		if($app instanceof \Tapcrowd\Application) {
			return $app;
		} else {
			throw new \InvalidArgumentException('Application Not Found');
		}
	}

	/**
	 * Validates whether the current user has access to the given object
	 *
	 * - Event and Venue id's are translated to respective appId's.
	 * - TapCrowd admins are granted all access
	 * - Channel admins are grantes access to all content within their channel
	 *
	 * @param \Tapcrowd\Account $account The account to validate for
	 * @param string $objectType The type of the given object (app, event or venue)
	 * @param int $objectId The object identifier
	 * @return bool
	 */
	public static function hasEntityAccess( Account $account, $objectType, $objectId )
	{
		# Validate
		if(!in_array($objectType, array('app','event','venue'))) {
			throw new \InvalidArgumentException('Invalid objectType');
		}
		if(!is_numeric($objectId)) {
			throw new \InvalidArgumentException('Invalid objectId');
		}

		$channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();

		# Fact: Tapcrowd admins can access everything, Channel admins are granted access
		# to all content within their channel
		if($account->role == 'admin' && ($account->channelId == 1 || $account->channelId == $channel->id)) {
			return true;
		}

		# Translate event && venue id's to appid
		$appId = DB::selectAppId( $objectType, $objectId );

		#
		# Fail: Current app differs from objects app. Redirects to apps page.
		#
		$CI =& get_instance();
		$currentAppId = $CI->session->userdata('appid');
		if(!empty($currentAppId) && is_numeric($currentAppId) && $currentAppId != $appId) {
			redirect('apps');
		}

		# Check rights
		return DB::checkApplicationRights( $account->accountId, $appId );
	}

	/**
	 * Return all the given accounts apps (privileged)
	 *
	 * @param \Tapcrowd\Account $account The account
	 * @param string $query Optional search query
	 * @param int $offset Offset in result set
	 * @param int $limit Max. number of results
	 * @return Application[]
	 */
	public static function listApplications( Account $account, $query = null, $offset = 0, $limit = 0 )
	{
		# Validate
		if($query !== null) $query = filter_var($query, FILTER_SANITIZE_STRING);
		if(!is_numeric($offset) || !is_numeric($limit)) {
			throw new \InvalidArgumentException('Invalid offset/limit');
		}

		# List applications for account
		return DB::selectApps( $account, $query, $offset, $limit );
	}

	/**
	 * Returns all apps in the given channel
	 *
	 * @param \Tapcrowd\Channel $channel The channel
	 * @param string $query Optional search query
	 * @param int $offset Offset in result set
	 * @param int $limit Max. number of results
	 * @param array $extraparams Optional extra search parameters
	 * @return Application[]
	 */
	public static function listChannelApplications( Channel $channel, $query = null, $offset = 0, $limit = 0, $extraparams = array() )
	{
		# Validate
		if($query !== null) $query = filter_var($query, FILTER_SANITIZE_STRING);
		if(!is_numeric($offset) || !is_numeric($limit)) {
			throw new \InvalidArgumentException('Invalid offset/limit');
		}

		# If this current account is a tapcrowd admin, list apps for all channels
		if($channel->id == 1 && self::getCurrentUser()->role == 'admin') {
			return DB::selectChannelApps( null, $offset, $limit, $extraparams );
		}

		# List applications for account
		return DB::selectChannelApps( $channel, $offset, $limit );
	}

	/**
	 * Redirects to the login page and destroys the session (== logout)
	 *
	 * @param bool $logout Wether to logout first
	 * @return void
	 */
	public function redirectToLogin( $logout = false )
	{
		$CI =& get_instance();
		$CI->session->sess_destroy();
		header('HTTP/1.1 303 See Other');
// 		header('Location: /auth/' . self::getLocation(($logout? 'logout' : 'login'), 'admin') );
		header('Location: '.($logout? '/auth/logout' : '/auth/login'));
		exit;
	}

	/**
	 * Return the location of the requested path.
	 *
	 * This functions should contain all the logic for redirection, channel urls, etc...
	 *
	 * @param string $path The path
	 * @param string $domain The domain (admin, account, profiling ...)
	 */
	public function getLocation( $path = '', $domain = 'admin' )
	{
		$server = $_SERVER['SERVER_NAME'];

		# Belgacom exception
		if($domain == 'account' && strpos($server, 'myapp.bmapps.be') !== false) {
			$server = 'admin.tapcrowd.com';
		}

		if($domain != 'admin') $server = str_replace('admin',$domain,$server);

		return 'http://' . $server . '/' . $path;
	}
}

/**
 * Account Struct
 */
class Account
{
	public $id;
	public $accountId;
	public $parentId;
	public $channelId;
	public $organizerId;
	public $login;
	public $role;
	public $fullname;
	public $email;
}

/**
 * Balance Struct
 */
class Balance
{
	/**
	 * @var \Tapcrowd\Account
	 */
	public $account;

	public $apps;
	public $credits;

	public function __construct()
	{
		$this->credits = (object)array('basic' => 0, 'plus' => 0, 'pro' => 0);
	}
}

/**
 * Channel Struct
 */
class Channel
{
	public $channelId = 1;
	public $name = 'Tapcrowd';
	public $logo = '/img/header_logo.png';
}

/**
 * Application Struct
 */
class Application
{
	public $id;
	public $apptypeid;
	public $flavor;
//	public $organizerid;
	public $accountId;
	public $accountname;
	public $email;
	public $name;
	public $info;
	public $app_icon;
	public $subdomain;
	public $cname;
	public $creation;
	public $defaultlanguage;
	public $channel = null;
	public $languages = array();
	public $bundle;
}

/**
 * Flavor Struct
 */
class Flavor
{
	public $flavorId;
	public $title;
	public $icon;
}

class DB
{
	/**
	 * Returns either the account for the given session of false
	 *
	 * @param int $tcsess The tapcrowd core session id
	 * @return \Tapcrowd\Account|false
	 */
	public static function getAccountForSessionId( $tcsess )
	{
		$CI =& get_instance();

		# Get session interval
		$sess_interval = $CI->config->item('sess_expiration');
		if(empty($sess_interval)) $sess_interval = (20*60);

		# Query
		$sql = 'SELECT accountId, channelId, clientId AS organizerId, role, fullname, email FROM tc_sessions
			INNER JOIN tc_accounts USING(accountId)
			WHERE tc_sessions.end_time > (NOW() - INTERVAL ? SECOND)
			AND tc_sessions.sessId=? LIMIT 1';
		$data = $CI->db->query( $sql, array((int)$sess_interval, (int)$tcsess))->row_array();
		if(empty($data)) return false;

		# Create and fill account object
		$account = new \Tapcrowd\Account;
		foreach($data as $k => $v) {
			if(property_exists($account, $k)) {
				$account->$k = $v;
			}
		}
		return $account;
	}

	/**
	 * Update the session end time to the current time
	 *
	 * @param int $tcsess The session identifier
	 * @return void
	 */
	public static function updateSessionEndtime( $tcsess )
	{
		$sql = 'UPDATE tc_sessions SET end_time = CURRENT_TIMESTAMP WHERE sessId = ?';
		$CI =& get_instance();
		$CI->db->query($sql, (int)$tcsess);
	}

	/**
	 * Returns the app id for the given object (type + id)
	 *
	 * @param string $objectType The objects type (supported are app, venue and event)
	 * @param int $objectId The objects identifier
	 * @return int
	 */
	public static function selectAppId( $objectType, $objectId )
	{
		if($objectType == 'app') return (int)$objectId;
		elseif($objectType == 'venue') {
			$sql = 'SELECT appid FROM appvenue WHERE venueid=? AND appid <> 2 LIMIT 1';
		}
		elseif($objectType == 'event') {
			$sql = 'SELECT appid FROM appevent WHERE eventid=? AND appid <> 2 LIMIT 1';
		}
		else return false;

		$CI =& get_instance();
		$data = $CI->db->query( $sql, (int)$objectId )->row_array();
		return (empty($data) || empty($data['appid']))? false : $data['appid'];
	}

	/**
	 * Validates the given accounts rights to accessthe given app
	 *
	 * @param int $accountId The account identifier
	 * @param int $appId The application identifier
	 * @return bool
	 */
	public static function checkApplicationRights( $accountId, $appId )
	{
		$sql = "
		SELECT
		  (SELECT COUNT(*) AS cnt FROM tc_entityrights WHERE entityId=? AND entity='application' AND accountId=?) +
		  (SELECT COUNT(*) AS cnt FROM app WHERE id=? AND accountId=?)
		AS cnt";
		$CI =& get_instance();
		$data = $CI->db->query( $sql, array((int)$appId, (int)$accountId, (int)$appId, (int)$accountId) )->row_array();
// 		var_dump($data, ((empty($data) || empty($data['cnt']))? false : true)  ); exit;
		return (empty($data) || empty($data['cnt']))? false : true;
	}

	/**
	 * Returns the apps for the given account
	 *
	 * @param \Tapcrowd\Account $account
	 * @param string $query Optional search query
	 * @param int $offset Offset in result set
	 * @param int $limit Max. number of results
	 * @return \Tapcrowd\Application[]
	 */
	public static function selectApps( Account $account, $query, $offset, $limit )
	{
		$sql = "
		SELECT id, apptypeid, accountId, name, info, subdomain, cname, creation, defaultlanguage, app_icon, isdeleted, bundle
		FROM app
		WHERE id IN(
		  SELECT entityId as appId FROM tc_entityrights WHERE entity='application' AND accountId=?
		  UNION
		  SELECT id AS appId FROM app WHERE accountId=?
		)
		AND isdeleted != 1 ORDER BY creation ASC";
		$CI =& get_instance();
		return self::createAppArray($CI->db->query($sql, array($account->accountId, $account->accountId)));
	}

	/**
	 * Returns the app for the given bundle
	 *
	 * @param string $bundle
	 * @return \Tapcrowd\Application
	 */
	public static function getApplicationUsingBundle( $bundle )
	{
		$sql = 'SELECT * FROM app WHERE bundle=?';
		$CI =& get_instance();
		$apps = self::createAppArray($CI->db->query($sql, $bundle));
		if(is_array($apps) && !empty($apps)) return array_pop($apps);
		else return false;
	}

	/**
	 * Returns the app for the given bundle
	 *
	 * @param int $appId
	 * @return \Tapcrowd\Application
	 */
	public static function getApplicationUsingId( $appId )
	{
// 		$sql = 'SELECT * FROM app WHERE id=?';
		$sql = "SELECT * FROM app LEFT JOIN tc_accounts USING(accountId) WHERE app.id=?";
		$CI =& get_instance();
		$apps = self::createAppArray($CI->db->query($sql, (int)$appId));
		if(is_array($apps) && !empty($apps)) return array_pop($apps);
		else return false;
	}

	/**
	 * Returns the apps for the given channel
	 * ordered by descending timestamps
	 *
	 * @param \Tapcrowd\Channel $channel The channel
	 * @param int $offset Offset in result set
	 * @param int $limit Max. number of results
	 * @return \Tapcrowd\Application[]
	 */
	public static function selectChannelApps( Channel $channel, $offset, $limit, $extraparams = array() )
	{
		$sql = "SELECT * FROM app LEFT JOIN tc_accounts USING(accountId) WHERE app.isdeleted != 1";
		if($channel !== null) {
			$sql .= ' AND tc_accounts.channelId=?';
		}

		if(!empty($extraparams)) {
			foreach($extraparams as $k => $v) {
				if(!empty($k) && !empty($v)) {
					$sql .= " AND ".$k." LIKE '%".$v."%'";
				}
			}
		}

		$sql .= ' ORDER BY timestamp DESC';
		if(!empty($limit) && is_numeric($limit)) {
			$sql .= " LIMIT ".$limit;
		}

		$CI =& get_instance();
		return self::createAppArray($CI->db->query($sql, $channel->channelId));
	}

	/**
	 * Create Applicaion array from database result
	 * @param CI_DB_mysql_result $result
	 * @return \Tapcrowd\Application[]
	 */
	private static function createAppArray( CI_DB_mysql_result $result )
	{
		static $flavors;
		static $channels;

		$rows = $result->result_array();
		$apps = array();
		if(empty($rows)) return $apps;

		# Get flavors
		if(!isset($flavors)) {
			$flavors = array();
			$CI =& get_instance();
			$r = $CI->db->query('SELECT id, title, image_phone FROM apptype')->result_array();
			foreach($r as $d) {
				$flavor = new \Tapcrowd\Flavor;
				$flavor->flavorId = (int)$d['id'];
				$flavor->title = $d['title'];
				$flavor->icon = $d['image_phone'];
				$flavors[$flavor->flavorId] = $flavor;
			}
		}

		# Get channels
		if(!isset($channels)) {
			$channels = \Tapcrowd\Model\Channel::getInstance()->listChannels();
		}

		# Generate App structs
		$CI =& get_instance();
		$upload_url = $CI->config->item('publicupload');
		$upload_path = $CI->config->item('imagespath');
		foreach($rows as $row) {
			$app =& $apps[];
			$app = new \Tapcrowd\Application;
			foreach($row as $k => $v) {

				switch($k) {
					# Cleanup app_icon's
					case 'app_icon':
						if(!empty($v) && !strpos('://') && file_exists($upload_path . $v)) {
							$app->app_icon = $upload_url . $v;
						}
						// elseif(!empty($app->flavor->icon)) $app->app_icon = $app->flavor->icon;
						// else $app->app_icon = 'http://placehold.it/36&text=LOGO';
						break;
					# Set flavor
					case 'apptypeid':
						$app->apptypeid = $v;
						if(isset($flavors[$v])) $app->flavor = $flavors[$v];
						break;
					# Set fullname
					case 'fullname':
						$app->accountname = $v;
						break;
					# Set channel
					case 'channelId':
						$app->channel = isset($channels[$v])? $channels[$v] : $channels['TapCrowd'];
						break;
					default:
						if(property_exists($app, $k)) $app->$k = $v;
						break;
				}
			}
		}
		return $apps;
	}
}
