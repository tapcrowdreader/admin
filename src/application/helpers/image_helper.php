<?php

function image_thumb($image_path, $height, $width) {
	if($image_path == FALSE || $image_path == '' || $image_path == null) exit();


	// Get the CodeIgniter super object
	$CI =& get_instance();
	if(!is_dir($CI->config->item('imagespath') . 'cache/'.substr($image_path,0,strrpos($image_path, "/")+1))){
		mkdir($CI->config->item('imagespath') . 'cache/'.substr($image_path,0,strrpos($image_path, "/")+1), 0755,true);
	}
	$image_thumb = $CI->config->item('imagespath') . 'cache/'.$image_path;
	if(substr($image_path, 0, 3) != '../') {
	$image_path = $CI->config->item('imagespath') . $image_path;
	}

	//check if file already exists on server
	if(file_exists(str_replace("[", "%5B", str_replace("]", "%5D", str_replace(" ", "%20", $image_thumb))))) {
		return str_replace("[", "%5B", str_replace($CI->config->item('imagespath'), $CI->config->item('publicupload'),str_replace("]", "%5D", str_replace(" ", "%20", $image_thumb))));
	}
	
	// if $image_path is URL
	$delpath = '';
	if(strstr($image_path, 'http://')){
		file_put_contents('cache/temp/' . substr($image_path, strrpos($image_path , "/") + 1), file_get_contents($image_path));
		$image_path = 'cache/temp/' . substr($image_path, strrpos($image_path , "/") + 1);
		$delpath = $image_path;
	}
	
	if(file_exists($image_path)) {
		// Path to image thumbnail
		//$image_thumb = 'cache/' . $height . '_' . $width . '_' . substr($image_path, strrpos($image_path , "/") + 1);
		if(file_exists($image_thumb)) return str_replace($CI->config->item('imagespath'), $CI->config->item('publicupload'),str_replace("[", "%5B", str_replace("]", "%5D", str_replace(" ", "%20", $image_thumb))));

		if( ! file_exists($image_thumb) &&  file_exists($image_path)) {
			// LOAD LIBRARY
			$CI->load->library('image_lib');

			$img_prop = getimagesize($image_path);

			// CONFIGURE IMAGE LIBRARY
			$config['image_library']    = 'gd2';
			$config['source_image']     = $image_path;
			$config['new_image']        = $image_thumb;
			$config['maintain_ratio']   = TRUE;

			if($img_prop[0] > $width) { 
				$config['width']        = $width;
			} else {
				$config['width']        = $img_prop[0];
			}

			if($img_prop[1] > $height) { 
				$config['height']       = $height;
			} else {
				$config['height']       = $img_prop[1];
			}
			$CI->image_lib->initialize($config);
			if(!$CI->image_lib->resize()) {
				return "img/blank.gif";
			} else {
				
			}
			$CI->image_lib->clear();
			
			if($delpath != '') {
				unlink($delpath);
			}
			return str_replace("[", "%5B", str_replace($CI->config->item('imagespath'), $CI->config->item('publicupload'),str_replace("]", "%5D", str_replace(" ", "%20", $image_thumb))));
		} else {
			return "img/blank.gif";
		}
	} else {
		return "img/blank.gif";
	}
}

function eventlogo_thumb($image_path, $height, $width) {
	// Get the CodeIgniter super object
	$CI =& get_instance();
	if(substr($image_path, 0, 3) != '../') {
	$image_path = $CI->config->item('imagespath') . $image_path;
	}
	// if $image_path is URL
	$delpath = '';
	if(strstr($image_path, 'http://')){
		file_put_contents($CI->config->item('imagespath') . 'cache/temp/' . substr($image_path, strrpos($image_path , "/") + 1), file_get_contents($image_path));
		$image_path = $CI->config->item('imagespath') . 'cache/temp/' . substr($image_path, strrpos($image_path , "/") + 1);
		$delpath = $image_path;
	}
	
	// Path to image thumbnail
	$image_thumb = $CI->config->item('imagespath') . 'upload/eventlogos/' . substr($image_path, strrpos($image_path , "/") + 1);
	if(file_exists($image_thumb)) return $image_thumb;
	
	if(!file_exists($image_thumb)) {
		// LOAD LIBRARY
		$CI->load->library('image_lib');
		
		// CONFIGURE IMAGE LIBRARY
		$config['image_library']    = 'gd2';
		$config['source_image']     = $image_path;
		$config['new_image']        = $image_thumb;
		$config['maintain_ratio']   = TRUE;
		$config['width']            = $width;
		$config['height']           = $height;
			
		$CI->image_lib->initialize($config);
		$CI->image_lib->resize();
		$CI->image_lib->clear();
	}
	
	if($delpath != '') {
		unlink($delpath);
	}
	
	return 'upload/eventlogos/' . substr($image_path, strrpos($image_path , "/") + 1);
}

function split_map($map_path, $typeid = 0, $type='event') {
	if($type=='event') {
		$typeid = 'e'.$typeid;
	} elseif($type == 'venue') {
		$typeid = 'v'.$typeid;
	}
	// Get the CodeIgniter super object
	$CI =& get_instance();
	// $filename = $CI->config->item('imagespath') . $filename;
	
	$imagearray = array();
	
	if(file_exists($CI->config->item('imagespath') . $map_path)) {
		// File and new size
		$filename = $map_path;
		$percent = 0.5;
		
		// Get new sizes
		list($width, $height) = getimagesize($CI->config->item('imagespath') . $filename);
		$newwidth = $width * $percent;
		$newheight = $height * $percent;

		$sizes = array(
				array(
					'dst_x'	=> 0,
					'dst_y'	=> 0,
					'src_x'	=> 0,
					'src_y'	=> 0
				),
				array(
					'dst_x'	=> 0,
					'dst_y'	=> 0,
					'src_x'	=> $width/2,
					'src_y'	=> 0
				),
				array(
					'dst_x'	=> 0,
					'dst_y'	=> 0,
					'src_x'	=> 0,
					'src_y'	=> $height/2
				),
				array(
					'dst_x'	=> 0,
					'dst_y'	=> 0,
					'src_x'	=> $width/2,
					'src_y'	=> $height/2
				)
			);
		
		// Resize
		/*
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width/2, $height/2);
		imagecopyresized($thumb, $source, 0, 0, $width/2, 0, $newwidth, $newheight, $width/2, $height/2);
		imagecopyresized($thumb, $source, 0, 0, 0, $height/2, $newwidth, $newheight, $width/2, $height/2);
		imagecopyresized($thumb, $source, 0, 0, $width/2, $height/2, $newwidth, $newheight, $width/2, $height/2);
		*/

		if(!is_dir($CI->config->item('imagespath') . 'cache/maps/'.$typeid)){
			mkdir($CI->config->item('imagespath') . 'cache/maps/'.$typeid, 0755,true);
		}
		
		for ($i=0; $i < 4; $i++) { 
			if(!file_exists($CI->config->item('imagespath') .  'cache/maps/'.$typeid.'/' . $i . '_' . substr($map_path, strrpos($map_path , "/") + 1))){
				// Load
				$thumb = imagecreatetruecolor($newwidth, $newheight);
				switch (end(explode(".", $filename))) {
					case 'jpeg':
					case 'jpg':
					default:
						$source = imagecreatefromjpeg($CI->config->item('imagespath') . $filename);
						break;
					case 'png':
						$source = imagecreatefrompng($CI->config->item('imagespath') . $filename);
						break;
				}
				// Output
				imagecopyresized($thumb, $source, $sizes[$i]['dst_x'], $sizes[$i]['dst_y'], $sizes[$i]['src_x'], $sizes[$i]['src_y'], $newwidth, $newheight, $width/2, $height/2);
				imagejpeg($thumb, $CI->config->item('imagespath') . 'cache/maps/'.$typeid.'/' . $i . '_' . substr($map_path, strrpos($map_path , "/") + 1));
				imagedestroy($thumb);
			}
			$imagearray[] = $CI->config->item('publicupload') . 'cache/maps/'.$typeid.'/' . $i . '_' . substr($map_path, strrpos($map_path , "/") + 1);
		}
		
	}
	
	return $imagearray;
}

function image_crop($image_path, $width, $height) {
	if($image_path == FALSE || $image_path == '' || $image_path == null) return '';
	// Get the CodeIgniter super object
	$CI =& get_instance();
	$image_path2 = $image_path;
	$image_path = $CI->config->item('imagespath') . $image_path;
	
	if(!file_exists($image_path)) return "img/blank.gif";
	
	// Path to image thumbnail
	$image_thumb = $CI->config->item('imagespath') .'cache/cp_' . $height . '_' . $width . '_' . substr($image_path, strrpos($image_path , "/") + 1);
	$image_cache = $CI->config->item('imagespath') .'cache/temp/cp_' . $height . '_' . $width . '_' . substr($image_path, strrpos($image_path , "/") + 1);
	if(file_exists($image_thumb)) return str_replace("[", "%5B", str_replace("]", "%5D", str_replace(" ", "%20", $image_thumb)));
	
	if( ! file_exists($image_thumb) &&  file_exists($image_path)) {
		$sizes = getimagesize($image_path);
		$old_width = $sizes[0];
		$old_height = $sizes[1];
		$scale = max($width/$old_width, $height/$old_height);

		$config['image_library'] 	= 'gd2';
		$config['source_image']		= $image_path;
		$config['create_thumb'] 	= FALSE;
		$config['maintain_ratio'] 	= TRUE;
		$config['width']	 		= $old_width*$scale;
		$config['height']			= $old_height*$scale;
		$config['dynamic_output'] 	= FALSE;
		$config['new_image'] 		= $image_thumb;

		$CI->load->library('image_lib');
		$CI->image_lib->initialize($config);
		if ( ! $CI->image_lib->resize()) {
			echo $CI->image_lib->display_errors();
			return "img/blank.gif";
		} else {
			$CI->image_lib->clear();
			
			$config['image_library'] 	= 'gd2';
			$config['source_image']		= $image_thumb;
			$config['create_thumb'] 	= FALSE;
			$config['maintain_ratio'] 	= FALSE;
			$config['width']	 		= $width;
			$config['height']			= $height;
			$config['dynamic_output'] 	= FALSE;
			$config['source_image']		= $image_thumb;
			$config["x_axis"] 			= ($old_width*$scale - $width)/2;
			$config["y_axis"] 			= ($old_height*$scale - $height)/2;
			$CI->image_lib->initialize($config); 
			$CI->image_lib->crop();
			
			return  str_replace($CI->config->item('imagespath'), $CI->config->item('publicupload'),str_replace("[", "%5B", str_replace("]", "%5D", str_replace(" ", "%20", 'cache/cp_' . $height . '_' . $width . '_' . substr($image_path2, strrpos($image_path2 , "/") + 1)))));
		}
	} else {
		return "img/blank.gif";
	}
}

function myUrlEncode($string) {
	$entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
	$replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
	return str_replace($entities, $replacements, urlencode($string));
}

/* End of file image_helper.php */
/* Location: ./application/helpers/image_helper.php */
