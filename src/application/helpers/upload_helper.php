<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Upload the image and add to resources
 *
 * @param File $file The uploaded file
 * @param String $parenttype The parent type ex. Event
 * @param int $parentid The parent id
 * @param String $type The type of image ex thumb, image
 * @param Bool $imagecheck Check if file is image
 * @param String $filetype The type of file ex. jpg
 * @param int $maxwidth The maximum width of the image
 * @param int $maxheight The maximum height of the image
 * @param Bool $thumb Create a thumbnail
 * @param int $thumbwidth The width of the thumbnail
 * @param int $thumbheight The height of the thumbnail
 * @param Bool $thumbratio Maintain the ratio for the thumbnail
 * @return void
 */
function imageupload($args) {
	$imagecheck = TRUE;
	$filetype = '';
	$maxwidth = 0;
	$maxheight = 0;
	$thumb = FALSE;
	$thumbwidth = 50;
	$thumbheight = 50;
	$thumbratio = TRUE;

	foreach($args as $k => $v) {
		${$k} = $v;
	}

	$CI =& get_instance();
	$userdata = $CI->session->userdata;
	$appid = $userdata['appid'];

	if($imagecheck) {
		if(is_image($file) == FALSE) {
			return FALSE;
		}
	}

	if($filetype) {
		if(checkfiletype($file, $filetype) == FALSE) {
			return FALSE;
		}
	}

	if($maxwidth != 0 || $maxheight != 0) {
		if(is_allowed_dimensions($file, $maxwidth, $maxheight) == FALSE) {
			return FALSE;
		}
	}

	//for now upload to upload.tapcrowd later replace by api
	$uploadpath = $CI->config->item('imagespath') . "upload/".$appid.'/'.$parenttype .'/'.$parentid.'/';
	if(!is_dir($uploadpath)){
		mkdir($uploadpath, 0775, TRUE);
	}	

	if (!copy($file['tmp_name'], $uploadpath.$file['name'])) {
		if (!move_uploaded_file($file['tmp_name'], $uploadpath.$file['name'])) {
			return FALSE;
		}
	}

	$CI->general_model->insert(
			'tc_resources', 
			array(
				'appId' => $appid,
				'parentType' => $parenttype,
				'parentId' => $parentid,
				'sortorder' => 0,
				'type'	=> $type,
				'path' => "upload/".$appid.'/'.$parenttype .'/'.$parentid.'/'.$file['name']
			)
		);

	if($thumb) {
		$config['image_library'] = 'gd2';
		$config['source_image'] = $uploadpath.'/'.$file['name'];
		$config['new_image'] = $uploadpath.'/thumb_'.$file['name'];
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = $thumbratio;
		$config['width'] = $thumbwidth;
		$config['height'] = $thumbheight;

		$CI->load->library('image_lib', $config);

		$CI->image_lib->resize();

		$CI->general_model->insert(
				'tc_resources', 
				array(
					'appId' => $appid,
					'parentType' => $parenttype,
					'parentId' => $parentid,
					'sortorder' => 0,
					'type'	=> 'thumb',
					'path' => "upload/".$appid.'/'.$parenttype .'/'.$parentid.'/thumb_'.$file['name']
				)
			);
	}

	return true;
}

function is_image($file) {
	$png_mimes  = array('image/x-png');
	$jpeg_mimes = array('image/jpg', 'image/jpe', 'image/jpeg', 'image/pjpeg');

	if (in_array($file['type'], $png_mimes))
	{
		$file['type'] = 'image/png';
	}

	if (in_array($file['type'], $jpeg_mimes))
	{
		$file['type'] = 'image/jpeg';
	}

	$img_mimes = array(
						'image/gif',
						'image/jpeg',
						'image/png',
					);

	return (in_array($file['type'], $img_mimes, TRUE)) ? TRUE : FALSE;
}

function checkfiletype($file, $filetype) {
	$png_mimes  = array('image/x-png');
	$jpeg_mimes = array('image/jpg', 'image/jpe', 'image/jpeg', 'image/pjpeg');

	if (in_array($file['type'], $png_mimes))
	{
		$file['type'] = 'image/png';
	}

	if (in_array($file['type'], $jpeg_mimes))
	{
		$file['type'] = 'image/jpg';
	}

	$mime = 'image/'.$filetype;
	if(strtolower($file['type']) == strtolower($mime)) {
		return true;
	} else {
		return false;
	}
}

function is_allowed_dimensions($file, $width, $height)
{
	if (function_exists('getimagesize'))
	{
		$D = @getimagesize($file['tmp_name']);

		if ($width > 0 AND $D['0'] > $width)
		{
			return FALSE;
		}

		if ($height > 0 AND $D['1'] > $height)
		{
			return FALSE;
		}

		return TRUE;
	}

	return TRUE;
}