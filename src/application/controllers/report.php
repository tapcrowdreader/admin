<?php if(!defined('BASEPATH')) exit(__('No direct script access'));
class Report extends CI_Controller {

    //php 5 constructor
    function __construct() {
        parent::__construct();
        if(_authed()) { }
        $this->load->model('report_model');
    }

    function index(){
        $cdata['content']       = $this->load->view('reports_list', array('lists' => 'lists'), TRUE);
        $this->load->view('master', $cdata);
    }

    function users(){
        $users = $this->report_model->getNewUsers();        
        $totalUsers = $this->report_model->getTotalUsersUptoDate();


        $usersData = array ();
        $i=0;

        foreach($users as $rec){
            $channel = $this->report_model->getChannelName($rec->channelId);

            $usersData[$i]['cdate'] = $rec->cdate;
            $usersData[$i]['fullname'] = $rec->fullname;
            $usersData[$i]['email'] = $rec->email;
            $usersData[$i]['channelId'] = $channel;
            $usersData[$i]['parentId'] = $rec->parentId;                        
            $i++;
        }
        
        $serverpath = $this->config->item('base_url');
        $cdata['content']       = $this->load->view('reports_list', array('users' => $usersData,'totalUsers' => $totalUsers, 'serverpath' => $serverpath),TRUE);
        $this->load->view('master', $cdata);
    }

    function apps(){
        $newApps = $this->report_model->getTotalAppsUptoDate();
        $month ="12"; 
        $appData = array ();
        $i=0;
        if($this->input->post('mymonth') !=0 ){
            $month = $this->input->post('mymonth');
            $year = $this->input->post('myyear');
            $apps = $this->report_model->getNewApps($month,$year);   
            foreach($apps as $rec){
                $owner = $this->report_model->getOwnerName($rec->ownerid);
                $appData[$i]['creation'] = $rec->creation;
                $appData[$i]['appname'] = $rec->appname;
                $appData[$i]['owner'] = $owner;
                $appData[$i]['flavor'] = $rec->flavor;
                $appData[$i]['channel'] = $rec->channel;                        
                $i++;
            }
        }
      
        $unique = array_map("unserialize", array_unique(array_map("serialize", $appData)));
        $serverpath = $this->config->item('base_url');
        $cdata['content']       = $this->load->view('reports_list', array('appData' => $unique, 'newApps' => $newApps, 'month'=>$month, 'serverpath' => $serverpath),TRUE);
        $this->load->view('master', $cdata);        
    }

    function funnel(){
        $newApps = $this->report_model->getTotalAppsUptoDate();
		$appDates = $this->report_model->allDates();
        $funnelData = array();
        $rev = array();
        $tAmount = 0;
        $revAmountO = 0;
        $revAmount = 0;
        $i=0;
        
        foreach($appDates as $value){
            $revAmount = $this->report_model->getRevenueByAppDate($value['creation']);
            $rev[$value['creation']]=$revAmount;
        }
        foreach($appDates as $appdate){
            $appCount = $this->report_model->getTotalAppsByDate($appdate['creation']);
            $paidApps = $this->report_model->getFunnelPaidAppsByDate($appdate['creation']);
            $totalUsersByDate = $this->report_model->getTotalUsersByDate($appdate['creation']);
            $reveByDate = $this->report_model->getRevnueByDate($appdate['creation']);

            $funnelData[$appdate['creation']]['paid'] = $paidApps;
            $funnelData[$appdate['creation']]['revenue'] = $reveByDate;;
            $funnelData[$appdate['creation']]['appcount'] = count($appCount);
            $funnelData[$appdate['creation']]['totalusers'] = $totalUsersByDate;
            $i++;
        }
        krsort($funnelData);
        $serverpath = $this->config->item('base_url');
        $cdata['content'] = $this->load->view('reports_list', array('funnelData' => $funnelData, 'appCount' => $appCount, 'rev'=>$rev, 'serverpath' => $serverpath),TRUE);
        $this->load->view('master', $cdata); 
    }

    function revenue(){
        $revenueToDate = $this->report_model->revenueToDate();
        $serverpath = $this->config->item('base_url');
        $cdata['content'] = $this->load->view('reports_list', array('revenuetodate'=>$revenueToDate, 'serverpath' => $serverpath),TRUE); 
        $this->load->view('master', $cdata); 
    }
}