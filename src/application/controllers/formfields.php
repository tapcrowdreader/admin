<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Formfields extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('forms_model');
		$this->load->model('artist_model');
	}
	
	//php 4 constructor
	function Formfields() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('forms_model');
	}
	
	function index() {
		$this->form();
	}
	
	function formscreen($id) {
		if($id == FALSE || $id == 0) redirect('events');
		
		$formscreen = $this->forms_model->getFormScreenByID($id);
		if($formscreen == FALSE) redirect('forms');
		
		$masterPageTitle = 'Fields in screen '.$formscreen->title;
		
		$form = $this->forms_model->getFormByID($formscreen->formid);
		$singleScreen = $form->singlescreen;
		
		$settingsBtnUrl = 'forms/editlauncher/'.$form->launcherid.'/';
		$reportBtnUrl	= 'forms/export/'.$form->id;
		$deleteBtnUrl	= 'forms/delete/'.$form->id.'/';
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$settingsBtnUrl .= 'event/'.$eventid;
			$deleteBtnUrl .= 'event';
			
		}else if($form->venueid){
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$settingsBtnUrl .= 'venue/'.$venueid;
			$deleteBtnUrl .= 'venue';
			
		}else{
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
			$settingsBtnUrl .= 'app/'.$app->id;
		}
		
		$launcher = $this->module_mdl->getLauncherById($form->launcherid);
		//--- Setting Form Buttons --- ///
		if($singleScreen):
			$formSettingBtns = '';
		
			$formSettingBtns .= '<a class="btn" href="'.site_url($settingsBtnUrl).'">
								<img width="16" height="16" alt="TapCrowd App" src="img/Settings.png">
								Form Settings
							</a>';
			if($launcher->moduletypeid != 61 && $launcher->moduletypeid != 62) {
				$formSettingBtns .= '<a class="btn" href="'.site_url('forms/results/'.$form->id).'" id="viewresults">
								<img width="16" height="16" alt="TapCrowd App" src="img/icons/application_cascade.png">
								View Results
							</a>';
			}			
							
		endif;
		
		if($launcher->moduletypeid == 61) {
			//ask a question module
			$questionsurl = 'askaquestion/event/'.$form->eventid;
			// redirect($questionsurl);
			$formSettingBtns .= '<a class="add btn" href="'.site_url($questionsurl).'" id="questions">
								<img width="16" height="16" alt="TapCrowd App" src="img/icons/application_cascade.png"> '.
								__('Questions').
							'</a>';
		}

		if($launcher->moduletypeid == 62) {
			$formSettingBtns .= '<a class="add btn" href="'.site_url('formscreens/result/'.$formscreen->id).'" id="questions">
								<img width="16" height="16" alt="TapCrowd App" src="img/icons/application_cascade.png"> '.
								__('View results').
							'</a>';
		}
		
		// Get Form Event
		$formfields = $this->forms_model->getFormFieldsByFormScreenID($id);

		if($launcher->moduletypeid == 60 || $launcher->moduletypeid == 61 || $launcher->moduletypeid == 62) {
			foreach($formfields as $key => $f) {
				if($f->formfieldtypeid == 16) {
					unset($formfields[$key]);
				}
			}
		}
		
		$this->iframeurl = 'formfields/formscreen/'.$id.'?r=1&width=247';
		
		$headers = array('Label' => 'label', 'Order' => 'order');
		
		if($type == 'venue'){
			$cdata['content'] 		= $this->load->view('c_listview', array('formscreen' => $formscreen, 'data' => $formfields, 'headers' => $headers, 'masterPageTitle' => $masterPageTitle, 'formSettingBtns' => $formSettingBtns, 'launcherid' => $form->launcherid, 'venue' => $venue, 'form' => $form), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			if($form->singlescreen == 1) {
				$cdata['crumb']			= checkBreadcrumbsVenue($app->apptypeid, array($venue->name => "venue/view/".$venue->id, $form->title => "formscreens/form/".$form->id, __("Fields") => $this->uri->uri_string()));
			} else {
				$cdata['crumb']			= checkBreadcrumbsVenue($app->apptypeid, array($venue->name => "venue/view/".$venue->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Fields") => $this->uri->uri_string()));
			}
			
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
		}else if($type == 'event'){
			$cdata['content'] 		= $this->load->view('c_listview', array('formscreen' => $formscreen, 'data' => $formfields, 'headers' => $headers, 'masterPageTitle' => $masterPageTitle, 'formSettingBtns' => $formSettingBtns, 'launcherid' => $form->launcherid, 'event' => $event, 'form' => $form), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			if($app->apptypeid != 3 && $app->apptypeid != 10) {
				if($form->singlescreen == 1) {
					$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id, __("Fields") => $this->uri->uri_string());
				} else {
					$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Fields") => $this->uri->uri_string());
				}
			} else {
				if($form->singlescreen == 1) {
					$cdata['crumb']			= array($event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id, __("Fields") => $this->uri->uri_string());
				} else {
					$cdata['crumb']			= array($event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Fields") => $this->uri->uri_string());
				}
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
		}else{
			$cdata['content'] 		= $this->load->view('c_listview', array('formscreen' => $formscreen, 'data' => $formfields, 'headers' => $headers, 'typeapp' => 'app', 'masterPageTitle' => $masterPageTitle, 'formSettingBtns' => $formSettingBtns, 'launcherid' => $form->launcherid, 'app' => $app, 'form' => $form), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$cdata['crumb']			= array($form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Fields") => $this->uri->uri_string());
		}
		if($singleScreen){
			// unset($cdata['crumb'][$formscreen->title]);
			// $cdata['crumb'][$form->title] = $cdata['crumb'][__('Forms')];
		}
		
		$this->load->view('master', $cdata);
	}
	
	function add($id) {
		$this->load->library('form_validation');
		$error = "";
	
		$formscreen = $this->forms_model->getFormScreenByID($id);
		if($formscreen == FALSE) redirect('forms');
		$masterPageTitle = 'Fields in screen '.$formscreen->title;
		
		$formscreenorder = $this->forms_model->getFormFieldsMaxOrderByFormScreenID($id);
		$formscreen->max_order = $formscreenorder->order + 1;
		
		$form = $this->forms_model->getFormByID($formscreen->formid);
		$singleScreen = $form->singlescreen;
		if($form->useflows){
			$formscreens_flows = $this->forms_model->getFormScreensByFormIdExcludeOne($form->id, $formscreen->id);
		}

		$launcher = $this->module_mdl->getLauncherById($form->launcherid);
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
		}else if($form->venueid){
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			
		}else{
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
		}
		
		$this->iframeurl = 'formfields/formscreen/'.$id.'?r=1&width=247';;
		
		//Get all field Types
		$fieldtypes = $this->forms_model->getFormsFieldTypes();
				
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		
		//for football event get artists of own team
		if($app->apptypeid == 6) {
			$artists = $this->artist_model->getArtistsByAppID($app->id);
		} else {
			$artists = array();
		}
	
		if($this->input->post('postback') == "postback") {
			
			$optionsErr = FALSE;
			
			if( $this->input->post('formfieldtypeid') == 15 && $this->general_model->rowExists('formfield', array('formscreenid' => $id, 'formfieldtypeid'  => 15)) > 0 ):
            	$error = __('Image field already exists (One image field is allowed per screen).');
			else:
				
				$field_options = array();
				if( $this->input->post('formfieldtypeid') == 3 || $this->input->post('formfieldtypeid') == 6 ) :
					$field_options = $this->input->post('fieldvalue');
					$flowscreens = $this->input->post('flowscreen');
					$flowscreens = isset( $flowscreens[$app->defaultlanguage] ) ? $flowscreens[$app->defaultlanguage] : array();
				elseif($this->input->post('formfieldtypeid') == 19) : 
					$field_options_button = $_POST['flowscreen2'][$app->defaultlanguage];
					$flowscreens = $this->input->post('flowscreen');
					$flowscreens = isset( $flowscreens[$app->defaultlanguage] ) ? $flowscreens[$app->defaultlanguage] : array();
				endif;
				foreach($languages as $language) {
					$this->form_validation->set_rules('label_'.$language->key, 'label ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('possiblevalues_'.$language->key, 'possiblevalues', 'trim');
                    $this->form_validation->set_rules('defaultvalue_'.$language->key, 'defaultvalue', 'trim');
						
						foreach($field_options[$language->key] as $val){
							if( ! trim( $val ) )
								$optionsErr = TRUE;
						}
				
            	}
				
				$this->form_validation->set_rules('formfieldtypeid', 'Field Type', 'trim|numeric');
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				$this->form_validation->set_rules('xpos', 'xpos', 'trim|numeric');
				$this->form_validation->set_rules('ypos', 'ypos', 'trim|numeric');
				$this->form_validation->set_rules('width', 'width', 'trim|numeric');
				$this->form_validation->set_rules('height', 'height', 'trim|numeric');
				$this->form_validation->set_rules('customproperty', 'customproperty', 'trim');
			endif;

			if(!$error && $this->form_validation->run() == FALSE){
				if( $optionsErr ) $error .= '<p>Option(s) are empty or incorrect!</p>';
			} else {
				if( $optionsErr ) $error .= '<p>Option(s) are empty or incorrect!</p>';
				if($error == '') {
					
					$required = $this->input->post('required');
					if($required == '')
						$required = "no";
					
					$order = $this->input->post('order');
					if($order == ''){
						$order = $formscreen->max_order;
					}
					
					$xpos = 0;
					$ypos = 0;
					$width = 0;
					$height = 0;
					if($this->input->post('xpos') != '') {
						$xpos = $this->input->post('xpos');
					}	
					if($this->input->post('ypos') != '') {
						$ypos = $this->input->post('ypos');
					}
					if($this->input->post('width') != '') {
						$width = $this->input->post('width');
					}	
					if($this->input->post('height') != '') {
						$height = $this->input->post('height');
					}
					$data = array(
						"formscreenid"				 => $formscreen->id,
						"formfieldtypeid"			 => set_value('formfieldtypeid'),
						"label"             		 => set_value('label_'.  $app->defaultlanguage),
						"possiblevalues"		 	 => isset( $field_options[$app->defaultlanguage] ) ? implode(',', $field_options[$app->defaultlanguage]) : '',
						"defaultvalue" 				 => set_value('defaultvalue_'.  $app->defaultlanguage),
						"required"			 		 => set_value('required'),
						"xpos"						 => $xpos,
						"ypos"						 => $ypos,
						"width"						 => $width,
						"height"					 => $height,
						"required"					 => $required,
						"order"						 => $order,
						"sticky"					 => $this->input->post('sticky'),
						'customproperty'			 => $this->input->post('customproperty')
					);
					
					if($newid = $this->general_model->insert('formfield', $data)){
						if( $this->input->post('formfieldtypeid') == 3 || $this->input->post('formfieldtypeid') == 6 ) :
							foreach( $field_options[$app->defaultlanguage] as $optionIndex=>$option ){
								if( trim( $option ) ):
									$foData = array(
													'formfieldid' => $newid,
													'value' => $option,
													'isdefault' => 0,
													'formflow_nextscreenid' => ( $form->useflows && isset( $flowscreens[$optionIndex] ) ) ? intval( $flowscreens[$optionIndex] ) : 0
												);
									$fieldoptionid = $this->general_model->insert('formfieldoption', $foData);
									foreach($languages as $language){
										$this->language_model->addTranslation('formfieldoption', $fieldoptionid, 'value', $language->key, $field_options[$language->key][$optionIndex]);
									}
								endif;
							}
						elseif($this->input->post('formfieldtypeid') == 19) :
							$foData = array(
								'formfieldid' => $newid,
								'value' => '',
								'isdefault' => 1,
								'formflow_nextscreenid' => ($form->useflows) ? intval($field_options_button) : 0
							);
							$fieldoptionid = $this->general_model->insert('formfieldoption', $foData);
						endif;
						
						//add translated fields to translation table
						foreach($languages as $language) {
							$this->language_model->addTranslation('formfield', $newid, 'label', $language->key, $this->input->post('label_'.$language->key));
							if($this->input->post('possiblevalues_'.$language->key)) {
								$this->language_model->addTranslation('formfield', $newid, 'possiblevalues', $language->key, $this->input->post('possiblevalues_'.$language->key));
							}
							
							$this->language_model->addTranslation('formfield', $newid, 'defaultvalue', $language->key, $this->input->post('defaultvalue_'.$language->key));
						}
						
						$this->session->set_flashdata('event_feedback', __('The field has successfully been add!'));
						_updateTimeStamp($formscreen->id);
						if($error == '') {
							redirect('formfields/formscreen/'.$formscreen->id);
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}						
				}

			}
		}
		
		if($type == 'venue'){
			$cdata['content'] 		= $this->load->view('c_formfield_add', array('formscreen' => $formscreen, 'fieldtypes'=>$fieldtypes, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle, 'formscreens_flows' => $formscreens_flows, 'launcher' => $launcher, 'form' => $form), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app->apptypeid, array($venue->name => "venue/view/".$venue->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Add Field") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
		}else if($type == 'event'){
			$cdata['content'] 		= $this->load->view('c_formfield_add', array('formscreen' => $formscreen, 'fieldtypes'=>$fieldtypes, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle, 'formscreens_flows' => $formscreens_flows, 'launcher' => $launcher, 'form' => $form), TRUE);
			if($app->apptypeid != 3 && $app->apptypeid != 10) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Add Field") => $this->uri->uri_string());
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Add Field") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
		}else{
			$cdata['content'] 		= $this->load->view('c_formfield_add', array('formscreen' => $formscreen, 'fieldtypes'=>$fieldtypes, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle, 'formscreens_flows' => $formscreens_flows, 'launcher' => $launcher, 'form' => $form), TRUE);
			$cdata['crumb']			= array(__("Apps") => "apps", $app->name => "apps/view/".$app->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Add Field") => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		}
		
		if($singleScreen){
			unset($cdata['crumb'][$formscreen->title]);
			// $cdata['crumb'][$form->title] = $cdata['crumb'][__('Forms')];
		}
		
		$this->load->view('master', $cdata);
	}

	function edit($id) {
		$this->load->library('form_validation');
		$error = "";
	
		// EDIT Formscreen
		$formfield = $this->forms_model->getFormFieldByID($id);
		if($formfield == FALSE) redirect('formscreens');
		
		$formscreen = $this->forms_model->getFormScreenByID($formfield->formscreenid);
		if($formscreen == FALSE) redirect('forms');
		
		if( $formfield->formfieldtypeid == 3 || $formfield->formfieldtypeid == 6 || $formfield->formfieldtypeid == 19){
			 $formfield->fieldOptions = $this->forms_model->getFieldOptions($formfield->id);
		}
		
		$masterPageTitle = 'Fields in screen '.$formscreen->title;
		
		$formscreenorder = $this->forms_model->getFormFieldsMaxOrderByFormScreenID($formfield->formscreenid);
		$formscreen->max_order = $formscreenorder->order + 1;
		
		$form = $this->forms_model->getFormByID($formscreen->formid);
		$singleScreen = $form->singlescreen;

		$formscreens = FALSE;
		if($form->useflows){
			$formscreens_flows = $this->forms_model->getFormScreensByFormIdExcludeOne($form->id, $formscreen->id);
		}

		$launcher = $this->module_mdl->getLauncherById($form->launcherid);
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
		}else if($form->venueid){
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			
		}else{
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
		}
				
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		
		//Get all field Types
		$fieldtypes = $this->forms_model->getFormsFieldTypes();
		$this->iframeurl = 'formfields/formscreen/'.$formscreen->id.'?r=1&width=247';;
		
		if($this->input->post('postback') == "postback") {
			
			$optionsErr = FALSE;
			
			if( $this->input->post('formfieldtypeid') == 15 && $this->general_model->rowExists('formfield', array('formscreenid' => $formscreen->id, 'formfieldtypeid'  => 15, 'id !=' => $id)) > 0 ):
            	$error = __('Image field already exists (One image field is allowed per screen).');
			else:
				$field_options = array();
				$newoptions = array();
				if( $this->input->post('formfieldtypeid') == 3 || $this->input->post('formfieldtypeid') == 6 ) :
					$field_options = $this->input->post('fieldvalue');
					$newoptions =  $this->input->post('new_field_option');
					$flowscreens = $this->input->post('flowscreen');
					$new_flowscreens = $this->input->post('new_flow_screen');
					$flowscreens = isset($flowscreens[$app->defaultlanguage]) ? $flowscreens[$app->defaultlanguage] : array();
					$new_flowscreens = isset($new_flowscreens[$app->defaultlanguage]) ? $new_flowscreens[$app->defaultlanguage] : array();
					
				endif;
				
				foreach($languages as $language) {
					$this->form_validation->set_rules('label_'.$language->key, 'Label ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('possiblevalues_'.$language->key, 'possiblevalues', 'trim');
                    $this->form_validation->set_rules('defaultvalue_'.$language->key, 'defaultvalue', 'trim');
					
					foreach($field_options[$language->key] as $val){
						if( ! trim( $val[0] ) )
							$optionsErr = TRUE;
					}
					$ii = 0;
					foreach($newoptions[$language->key] as $val){
						if($ii == 0 ) continue;
						$ii++;
						if( ! trim( $val[0] ) )
							$optionsErr = TRUE;
					}
					
				}
				
				$this->form_validation->set_rules('formfieldtypeid', 'Field Type', 'trim|numeric');
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				$this->form_validation->set_rules('xpos', 'xpos', 'trim|numeric');
				$this->form_validation->set_rules('ypos', 'ypos', 'trim|numeric');
				$this->form_validation->set_rules('width', 'width', 'trim|numeric');
				$this->form_validation->set_rules('height', 'height', 'trim|numeric');
				$this->form_validation->set_rules('customproperty', 'customproperty', 'trim');
				
			endif;
            
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
				if( $optionsErr ) $error .= '<p>Option(s) are empty or incorrect!</p>';
			} else {
				if( $optionsErr ) $error .= '<p>Option(s) are empty or incorrect!</p>';
				
				if($error == '') {
					
					$required = $this->input->post('required');
					if($required == '')
						$required = "no";
					
					$order = $this->input->post('order');
					if($order == ''){
						$order = $formscreen->max_order;
					}

					$xpos = $formfield->xpos;
					$ypos = $formfield->ypos;
					$width = $formfield->width;
					$height = $formfield->height;
					if($this->input->post('xpos') != '') {
						$xpos = $this->input->post('xpos');
					}	
					if($this->input->post('ypos') != '') {
						$ypos = $this->input->post('ypos');
					}
					if($this->input->post('width') != '') {
						$width = $this->input->post('width');
					}	
					if($this->input->post('height') != '') {
						$height = $this->input->post('height');
					}
					$data = array(
						"formfieldtypeid"			 => set_value('formfieldtypeid'),
						"label"             		 => set_value('label_'.  $app->defaultlanguage),
						"possiblevalues"		 	 => set_value('possiblevalues_'.  $app->defaultlanguage),
						"defaultvalue" 				 => set_value('defaultvalue_'.  $app->defaultlanguage),
						"required"			 		 => set_value('required'),
						"xpos"						 => $xpos,
						"ypos"						 => $ypos,
						"width"						 => $width,
						"height"					 => $height,
						"required"					 => $required,
						"order"						 => $order,
						"sticky"					 => $this->input->post('sticky'),
						'customproperty'			 => $this->input->post('customproperty')
					);
							
					if($this->forms_model->edit_table($id, $data, 'formfield')){
						
						if( $this->input->post('formfieldtypeid') == 3 || $this->input->post('formfieldtypeid') == 6 ) :
							foreach( $newoptions[$app->defaultlanguage] as $optionIndex=>$option ){
								if( trim( $option ) ):
									$foData = array(
													'formfieldid' => $id,
													'value' => $option,
													'isdefault' => 0,
													'formflow_nextscreenid' => ( $form->useflows && isset( $new_flowscreens[$optionIndex-1] ) ) ? intval($new_flowscreens[$optionIndex-1]) : 0
												);
									$fieldoptionid = $this->general_model->insert('formfieldoption', $foData);
									foreach($languages as $language){
										$this->language_model->addTranslation('formfieldoption', $fieldoptionid, 'value', $language->key, $newoptions[$language->key][$optionIndex]);
									}
								endif;
								
							}
							foreach($field_options[$app->defaultlanguage] as $optionid=>$optionvalue){
								if( trim( $optionvalue[0] ) ):
									$foData = array('value' => $optionvalue[0]);
									if(array_key_exists($optionid, $flowscreens)) $foData['formflow_nextscreenid'] = intval($flowscreens[$optionid][0]);
									
									$this->forms_model->edit_table($optionid, $foData, 'formfieldoption');
								endif;
							}
						elseif($this->input->post('formfieldtypeid') == 19) :
							$foData = array(
								'formfieldid' => $id,
								'value' => '',
								'isdefault' => 1,
								'formflow_nextscreenid' => ($form->useflows) ? intval($_POST['flowscreen2'][$app->defaultlanguage]) : 0
							);
							$exists = $this->db->query("SELECT id FROM formfieldoption WHERE formfieldid = ? AND formflow_nextscreenid != 0 LIMIT 1", array($id));
							if($exists->num_rows() == 0) {
								$fieldoptionid = $this->general_model->insert('formfieldoption', $foData);
							} else {
								$fieldoptionid = $this->general_model->update('formfieldoption', $exists->row()->id, $foData);
							}
							
						endif;
						
						foreach($languages as $language) {
	                        $label = $this->input->post('label_'.$language->key);
							$nameSuccess = $this->language_model->updateTranslation('formfield', $id, 'label', $language->key, $label);
							if($nameSuccess == false || $nameSuccess == null) {
								$this->language_model->addTranslation('formfield', $id, 'label', $language->key, $label);
							}
							
							if($this->input->post('possiblevalues_'.$language->key)) {
								$descrSuccess = $this->language_model->updateTranslation('formfield', $id, 'possiblevalues', $language->key, $this->input->post('possiblevalues_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('formfield', $id, 'possiblevalues', $language->key, $this->input->post('possiblevalues_'.$language->key));
								}
							}
							
							$descrSuccess = $this->language_model->updateTranslation('formfield', $id, 'defaultvalue', $language->key, $this->input->post('defaultvalue_'.$language->key));
							if($descrSuccess == false || $descrSuccess == null) {
								$this->language_model->addTranslation('formfield', $id, 'defaultvalue', $language->key, $this->input->post('defaultvalue_'.$language->key));
							}
							if( $this->input->post('formfieldtypeid') == 3 || $this->input->post('formfieldtypeid') == 6 ) :
							foreach($field_options[$language->key] as $optionid=>$optionvalue){
								$translation = is_array( $optionvalue ) ? $optionvalue[0] : $optionvalue;
								if( trim( $translation ) ):
									$nameSuccess = $this->language_model->updateTranslation('formfieldoption', $optionid, 'value', $language->key, $translation);
									if( $nameSuccess == FALSE || $nameSuccess == null ){
										$this->language_model->addTranslation('formfieldoption', $optionid, 'value', $language->key, $translation);
									}
								endif;
							}
							endif;
												
						}
						
						$this->session->set_flashdata('event_feedback', __('The field has successfully been updated'));
						_updateTimeStamp($formscreen->id);
						redirect('formfields/formscreen/'.$formscreen->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
		}
		
		if($type == 'venue'){
			$cdata['content'] 		= $this->load->view('c_formfield_edit', array('formfield' => $formfield, 'formscreen' => $formscreen, 'fieldtypes'=>$fieldtypes,'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle, 'formscreens_flows' => $formscreens_flows, 'launcher' => $launcher, 'form' => $form), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app->apptypeid, array($venue->name => "venue/view/".$venue->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Edit: ") . $formfield->label  => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
		}else if($type == 'event'){
			$cdata['content'] 		= $this->load->view('c_formfield_edit', array('formfield' => $formfield, 'formscreen' => $formscreen, 'fieldtypes'=>$fieldtypes,'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle, 'formscreens_flows' => $formscreens_flows, 'launcher' => $launcher, 'form' => $form), TRUE);
			if($app->apptypeid != 3 && $app->apptypeid != 10) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Edit: ") . $formfield->label  => $this->uri->uri_string());
			} else {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Edit: ") . $formfield->label  => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
		}else{
			$cdata['content'] 		= $this->load->view('c_formfield_edit', array('formfield' => $formfield, 'formscreen' => $formscreen, 'fieldtypes'=>$fieldtypes,'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle, 'formscreens_flows' => $formscreens_flows, 'launcher' => $launcher, 'form' => $form), TRUE);
			$cdata['crumb']			= array(__("Apps") => "apps", $event->name => "apps/view/".$app->id, $form->title => "formscreens/form/".$form->id, $formscreen->title =>"formfields/formscreen/".$formscreen->id ,__("Edit: ") . $formfield->label  => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		}
		if($singleScreen){
			unset($cdata['crumb'][$formscreen->title]);
			// $cdata['crumb'][$form->title] = $cdata['crumb'][__('Forms')];
		}
		$this->load->view('master', $cdata);
	}
	
	function delete($id, $type = "event") {		
		$formfield = $this->forms_model->getFormFieldByID($id);
		if($formfield == FALSE) redirect('formscreens');
		$fieldOptions = array();
		if( $formfield->formfieldtypeid == 3 || $formfield->formfieldtypeid == 6 ){
			 $fieldOptions = $this->forms_model->getFieldOptions($formfield->id);
		}
		
		$formscreen = $this->forms_model->getFormScreenByID($formfield->formscreenid);
		if($formscreen == FALSE) redirect('forms');
		
		$form = $this->forms_model->getFormByID($formscreen->formid);
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
		}else if($form->venueid){
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			
		}else{
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
		}
		
		$this->language_model->removeTranslations('formfield', $id);
		if($this->general_model->remove('formfield', $id)){
			
			foreach( $fieldOptions as $option ){
				$this->language_model->removeTranslations('formfieldoption', $option->id);
			}
			$this->general_model->removeByParentId('formfieldid', $id, 'formfieldoption');
			
			$this->session->set_flashdata('event_feedback', __('The field has successfully been deleted'));
			_updateTimeStamp($formfield->id);
			redirect('formfields/formscreen/'.$formscreen->id);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('formfields/formscreen/'.$formscreen->id);
		}
	}
		
	function sort($type = '') {
		if($type == '' || $type == FALSE) redirect('events');
		
		switch($type){
			case "formfield":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);
					
					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('formfield', $data);
					$this->db->last_query();
				}
				break;			
			default:
				break;
		}
	}
	
	function deleteoption($id){
		$fieldoption = $this->forms_model->getFieldOptionById($id);
		if($fieldoption == FALSE) redirect('formscreens');
		
		$formfield = $this->forms_model->getFormFieldByID($fieldoption->formfieldid);
		if($formfield == FALSE) redirect('formscreens');
		
		$formscreen = $this->forms_model->getFormScreenByID($formfield->formscreenid);
		if($formscreen == FALSE) redirect('forms');
		
		$form = $this->forms_model->getFormByID($formscreen->formid);
		$successUrl = 'formfields/edit/'.$formfield->id;
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$successUrl .= '/event';
		}else if($form->venueid){
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$successUrl .= '/venue';
		}else{
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
		}
		
		$this->language_model->removeTranslations('formfieldoption', $id);
		if($this->general_model->remove('formfieldoption', $id)){
			
			$this->session->set_flashdata('event_feedback', __('The field option has successfully been deleted'));
			_updateTimeStamp($formfield->id);
			redirect($successUrl);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('formfields/formscreen/'.$formscreen->id);
		}
	}
}