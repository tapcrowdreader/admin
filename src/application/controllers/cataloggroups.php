<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Cataloggroups extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('catalog_model');
	}

	//php 4 constructor
	function Sessiongroups() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('catalog_model');
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		// Get Sessions for event per group
		$cataloggroups = $this->catalog_model->getCataloggroupsByEventID($id);
		$headers = array(__('Name') => 'name', 'Order' => 'order');

		$cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => $cataloggroups, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Catalog Groups") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->iframeurl = 'catalogs/venue/'.$id;

		// Get Sessions for event per group
		$cataloggroups = $this->catalog_model->getCataloggroupsByVenueID($id);
		$headers = array(__('Name') => 'name', 'Order' => 'order');

		$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'data' => $cataloggroups, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Catalog Groups") => $this->uri->uri_string()));
		$this->load->view('master', $cdata);
	}

	function add($id, $type = 'event') {
		$config = array();
		$this->load->library('image_lib', $config);
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');

			$venues = $this->venue_model->allfromorganizer(_currentUser()->organizerId);

			$this->iframeurl = 'catalogs/venue/'.$id;

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND cataloggroupid <> 0 GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('venues', 'venues', 'trim');
				$this->form_validation->set_rules('image', 'image', 'trim');


				if($app->id == 393) {
					$this->form_validation->set_rules('tagdelbar', 'merk', 'trim|required');
				}

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array(
						"name" => set_value('name'),
						//"venueid" => set_value('venues'),
						"venueid" => $id,
						"order" => set_value('order')
					);

					if($categoryid = $this->general_model->insert('cataloggroup', $data)){
						// *** SAVE TAGS *** //
						if($app->id != 393) {
							if($this->input->post('mytagsulselect') != null && count($this->input->post('mytagsulselect')) > 0) {
							foreach ($this->input->post('mytagsulselect') as $tag) {
								$tags = array(
										'appid' 	=> $app->id,
										'tag' 		=> $tag,
										'cataloggroupid' => $categoryid
									);
								$this->db->insert('tag', $tags);
							}
							}
						} else {
							$tags = array(
									'appid' 	=> $app->id,
									'tag' 		=> $this->input->post('tagdelbar'),
									'cataloggroupid' => $categoryid
								);
							$this->db->insert('tag', $tags);
						}

						// *** //

						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/cataloggroupimages/".$categoryid)){
							mkdir($this->config->item('imagespath') . "upload/cataloggroupimages/".$categoryid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/cataloggroupimages/'.$categoryid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = ""; //No image uploaded!
							$error = $this->upload->display_errors();
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/cataloggroupimages/'. $categoryid . '/' . $returndata['file_name'];

							//resize
							$newname = 'upload/cataloggroupimages/'.$categoryid.'/groupimage'.$returndata['raw_name'].'@2x.png';
							$config['image_library'] = 'gd2';
							$config['source_image'] = $this->config->item('imagespath') . $image;
							$config['maintain_ratio'] = TRUE;
							$config['master_dim'] = 'width';
							$config['width'] = 960;
							$config['height'] = 640;
							$config['new_image'] = $this->config->item('imagespath') . $newname;

							$this->image_lib->initialize($config);

							if (!$this->image_lib->resize())
							{
								echo $this->image_lib->display_errors();
							}

							$this->general_model->update('cataloggroup', $categoryid, array('image' => $newname));
						}

						$this->session->set_flashdata('event_feedback', __('The cataloggroup has successfully been added!'));
						_updateVenueTimeStamp($venue->id);
						redirect('cataloggroups/venue/'.$venue->id);
					} else {
						$error = validation_errors();
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_cataloggroup_add', array('venue' => $venue, 'venues' => $venues, 'error' => $error, 'app' => $app, 'apptags' => $apptags), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Catalogs") => "catalog/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');

			$events = $this->event_model->allfromorganizer(_currentUser()->organizerId);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('events', 'events', 'trim');


				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array(
						"name" => set_value('name'),
						//"eventid" => set_value('events'),
						"eventid" => $event->id,
						"order" => set_value('order')
					);

					if($this->general_model->insert('cataloggroup', $data)){
						$this->session->set_flashdata('event_feedback', __('The cataloggroup has successfully been added!'));
						_updateTimeStamp($event->id);
						redirect('cataloggroups/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_cataloggroup_add', array('event' => $event, 'events' => $events, 'error' => $error), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Catalogs") => "catalog/event/".$event->id, __("Add new") => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function edit($id, $type = 'event') {
		$config = array();
		$this->load->library('image_lib', $config);
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			// EDIT SESSIONGROUP
			$cataloggroup = $this->catalog_model->getCataloggroupByID($id);
			if($cataloggroup == FALSE) redirect('venues');

			$venue = $this->venue_model->getById($cataloggroup->venueid);
			$venues = $this->venue_model->allfromorganizer(_currentUser()->organizerId);
			$app = _actionAllowed($venue, 'venue','returnApp');

			$this->iframeurl = 'catalogs/group/'.$id;

			// TAGS
			$tags = $this->db->query("SELECT tag FROM tag WHERE cataloggroupid = $id");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag->tag;
				}
				$tags = $tagz;
			}

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND cataloggroupid <> 0 GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('venues', 'venues', 'trim');
				$this->form_validation->set_rules('image', 'image', 'trim');


				if($app->id == 393) {
					$this->form_validation->set_rules('tagdelbar', 'merk', 'trim|required');
				}

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/cataloggroupimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/cataloggroupimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/cataloggroupimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = ""; //No image uploaded!
						$error = $this->upload->display_errors();
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/cataloggroupimages/'. $id . '/' . $returndata['file_name'];

						//resize
						$newname = 'upload/cataloggroupimages/'.$id.'/groupimage'.$returndata['raw_name'].'@2x.png';
						$config['image_library'] = 'gd2';
						$config['source_image'] = $this->config->item('imagespath') . $image;
						$config['maintain_ratio'] = TRUE;
						$config['master_dim'] = 'width';
						$config['width'] = 960;
						$config['height'] = 640;
						$config['new_image'] = $this->config->item('imagespath') . $newname;

						$this->image_lib->initialize($config);

						if (!$this->image_lib->resize())
						{
							echo $this->image_lib->display_errors();
						}

						$this->general_model->update('cataloggroup', $id, array('image' => $newname));
					}

					$data = array(
						"name" => set_value('name'),
						//"venueid" => set_value('venues'),
						"venueid" => $venue->id,
						"order" => set_value('order')
					);

					if($this->catalog_model->edit_cataloggroup($id, $data)){
						// *** SAVE TAGS *** //
						if(!empty($id)) {
							$this->db->where('cataloggroupid', $id);
							$this->db->where('appid', $app->id);
							if($this->db->delete('tag')){
								if ($this->input->post('mytagsulselect') != '' && count($this->input->post('mytagsulselect')) > 0) {
									foreach ($this->input->post('mytagsulselect') as $tag) {
										$tags = array(
												'appid' 	=> $app->id,
												'tag' 		=> $tag,
												'cataloggroupid' => $id
											);
										$this->db->insert('tag', $tags);
									}
								}
							}
						}
						// *** //
						$this->session->set_flashdata('event_feedback', __('The catalog group has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						redirect('cataloggroups/venue/'.$venue->id);
					} else {
						$error = validation_errors();
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_cataloggroup_edit', array('venue' => $venue, 'cataloggroup' => $cataloggroup, 'venues' => $venues, 'error' => $error, 'tags' => $tags, 'app' => $app, 'apptags' => $apptags), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Catalogs") => "catalog/venue/".$venue->id, __("Edit: ") . $cataloggroup->name => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			// EDIT SESSIONGROUP
			$cataloggroup = $this->catalog_model->getCataloggroupByID($id);
			if($cataloggroup == FALSE) redirect('events');

			$event = $this->event_model->getbyid($cataloggroup->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			$events = $this->event_model->allfromorganizer(_currentUser()->organizerId);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('events', 'events', 'trim');


				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array(
						"name" => set_value('name'),
						//"eventid" => set_value('events'),
						"eventid" => $event->id,
						"order" => set_value('order')
					);

					if($this->catalog_model->edit_cataloggroup($id, $data)){
						$this->session->set_flashdata('event_feedback', __('The catalog group has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('cataloggroups/event/'.$event->id);
					} else {
						$error =__( "Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_catalogroup_edit', array('event' => $event, 'cataloggroup' => $cataloggroup, 'events' => $events, 'error' => $error), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Catalogs") => "catalog/event/".$event->id, __("Edit: ") . $cataloggroup->name => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function delete($id, $type = 'event') {
		if($type == 'venue') {
			$cataloggroup = $this->catalog_model->getCataloggroupByID($id);
			if($this->general_model->remove('cataloggroup', $id)){
				$this->session->set_flashdata('event_feedback', __('The catalog group has successfully been deleted'));
				_updateVenueTimeStamp($cataloggroup->venueid);
				redirect('cataloggroups/venue/'.$cataloggroup->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('cataloggroups/venue/'.$cataloggroup->venueid);
			}
		} else {
			$cataloggroup = $this->catalog_model->getCataloggroupByID($id);
			if($this->general_model->remove('cataloggroup', $id)){
				$this->session->set_flashdata('event_feedback', __('The catalog group has successfully been deleted'));
				_updateTimeStamp($cataloggroup->eventid);
				redirect('cataloggroups/event/'.$cataloggroup->eventid);
			} else {
				$this->catalog->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('cataloggroups/event/'.$cataloggroup->eventid);
			}
		}
	}

	function sort($type = '') {
		if($type == '' || $type == FALSE) redirect('events');

		switch($type){
			case "catalog":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('catalog', $data);
					$this->db->last_query();
				}
				break;
			case "groups":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('cataloggroup', $data);
					$this->db->last_query();
				}
				break;
			default:
				break;
		}
	}

	function removeimage($id, $type = 'event') {
		$item = $this->catalog_model->getCataloggroupByID($id);
		if($type == 'venue') {
			if($item == FALSE) redirect('venues');
			$venue = $this->venue_model->getbyid($item->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($app->organizerid != _currentUser()->organizerId) redirect('venues');

			if(!file_exists($this->config->item('imagespath') . $item->image)) redirect('venues');

			// Delete image + generated thumbs

			unlink($this->config->item('imagespath') . $item->image);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('image' => '');
			$this->general_model->update('cataloggroup', $id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('cataloggroups/venue/'.$venue->id);
		} else {
			$event = $this->event_model->getbyid($item->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->image)) redirect('events');

			// Delete image + generated thumbs

			unlink($this->config->item('imagespath') . $item->image);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('image' => '');
			$this->general_model->update('caztaloggroup', $id, $data);

			_updateTimeStamp($event->id);
			redirect('cataloggroups/event/'.$event->id);
		}
	}

    function crop($id, $type = 'event', $selection = '') {
		if($selection == '') {
			$selectionName = __('Product Catalog');
		} else {
			$selectionName = $selection;
		}
        $error = '';
        $cataloggroup = $this->catalog_model->getCataloggroupByID($id);
        if($cataloggroup == FALSE) redirect($type . 's');
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($cataloggroup->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$appid = $app->id;
		} else {
            $event = $this->event_model->getbyid($cataloggroup->eventid);
            $app = _actionAllowed($event, 'event','returnApp');
		}

		$this->iframeurl = 'catalogs/groups/'.$cataloggroup->venueid.'/'.$selection;

        $cataloggrouppicwidth = $this->general_model->get_meta_data_by_key('app', $app->id, 'cataloggrouppicwidth');
        $cataloggrouppicheight = $this->general_model->get_meta_data_by_key('app', $app->id, 'cataloggrouppicheight');
        if(isset($cataloggrouppicwidth) && $cataloggrouppicwidth != false) {
        	$cataloggrouppicwidth = $cataloggrouppicwidth->value;
        } else {
        	$cataloggrouppicwidth = 0;
        }
        if(isset($cataloggrouppicheight) && $cataloggrouppicheight != false) {
        	$cataloggrouppicheight = $cataloggrouppicheight->value;
        } else {
        	$cataloggrouppicheight = 0;
        }

		if($app->apptypeid == 4 || $app->apptypeid == 11) {
			$catalogpicwidth = 960;
		}

        if($this->input->post('postback') == "postback") {
           $w = $this->input->post('w');
           $h = $this->input->post('h');
           $x = $this->input->post('x');
           $y = $this->input->post('y');

		   $newname = 'upload/cataloggroupimages/'.$cataloggroup->id.'/cropped_'.time().'.jpg';
           $config['image_library']  = 'gd2';
           $config['source_image']  = $this->config->item('imagespath') . $cataloggroup->image;
           $config['create_thumb']  = FALSE;
           $config['maintain_ratio']  = FALSE;
           $config['width']    = $w;
           $config['height']   = $h;
           $config['x_axis']   = $x;
           $config['y_axis']   = $y;
           $config['dynamic_output']  = FALSE;
           $config['new_image'] = $this->config->item('imagespath') . $newname;

           $this->load->library('image_lib', $config);
           $this->image_lib->initialize($config);

           if (!$this->image_lib->crop())
           {
			   $newname = $cataloggroup->image;
			   echo $this->image_lib->display_errors();
           }

			$data = array(
					"image"	=> $newname
				);

			$this->general_model->update('cataloggroup', $cataloggroup->id, $data);
			$cataloggroup->image = $newname;
        }

        if($type == 'venue') {
            $venue = $this->venue_model->getbyid($cataloggroup->venueid);
            $cdata['content'] 		= $this->load->view('c_cataloggroup_crop', array('venue' => $venue, 'cataloggroup' => $cataloggroup, 'error' => $error, 'cataloggrouppicheight' => $cataloggrouppicheight, 'cataloggrouppicwidth' => $cataloggrouppicwidth), TRUE);
            $cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $selectionName => "cataloggroups/venue/".$venue->id."/0/".$selection, __("Edit Picture: ") . $cataloggroup->name => $this->uri->uri_string()));
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
            $this->load->view('master', $cdata);
        }
    }


}