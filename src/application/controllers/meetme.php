<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Meetme extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Meet me',
			'plural' => 'Meet me',
			'title' => __('Meet me'),
			'parentType' => 'event',
			'parentId' => null,
			'headers' => array(
			),
			'actions' => array(
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false,
		);
	}
	
	function event($id) {
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		
		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array()), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'meetme');
		$this->load->view('master', $cdata);
	}	
}