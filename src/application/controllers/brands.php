<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Brands extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('brands_model');
	}
	
	//php 4 constructor
	function Brands() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('brands_model');
	}
	
	function index() {
		$this->event();
	}
	
	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');
		
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
        $this->iframeurl = "exhibitors/event/" . $id;
		
		$brands = $this->brands_model->getBrandsByEventID($id);
		$headers = array(__('Name') => 'name');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => $brands, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Brands") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}
	
	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		
		$brands = $this->brands_model->getBrandsByVenueID($id);
		$headers = array(__('Name') => 'name');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'data' => $brands, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Brands") => $this->uri->uri_string()));
		$this->load->view('master', $cdata);
		
	}
	
	function add($id, $type = "event") {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
			
			$venue = $this->venue_model->getbyid($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
            
            $this->iframeurl = "exhibitor/brands/" . $id;
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				
				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					// Save exhibitor
					$data = array( 
							"venueid"		=> $id,
							"name" 			=> set_value('name')
						);
					$res = $this->general_model->insert('exhibitorbrand', $data);
					
					if($res != FALSE){
						$this->session->set_flashdata('event_feedback', __('The brand has successfully been added'));
						redirect('brands/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
					
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_brand_add', array('venue' => $venue, 'error' => $error), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Brands") => "brands/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
            
            $this->iframeurl = "exhibitor/brands/" . $id;

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					// Save exhibitor
					$data = array( 
							"eventid"		=> $id,
							"name" 			=> set_value('name')
						);
					$res = $this->general_model->insert('exhibitorbrand', $data);

					if($res != FALSE){
						$this->session->set_flashdata('event_feedback', __('The brand has successfully been added'));
						_updateTimeStamp($event->id);
						redirect('brands/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

				}
			}

			$cdata['content'] 		= $this->load->view('c_brand_add', array('event' => $event, 'error' => $error), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Brands") => "brands/event/".$event->id, __("Add new") => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}
	
	function edit($id, $type = "event") {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
			
			// EDIT EXHIBITOR
			$brand = $this->brands_model->getBrandByID($id);
			if($brand == FALSE) redirect('venues');
			
			$venue = $this->venue_model->getbyid($brand->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
            
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array( 
							"venueid"		=> $venue->id,
							"name" 			=> set_value('name')
						);
					
					if($this->general_model->update('exhibitorbrand', $id, $data)){
						$this->session->set_flashdata('event_feedback', __('The brand has successfully been updated'));
						redirect('brands/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_brand_edit', array('venue' => $venue, 'brand' => $brand, 'error' => $error), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Brand") => "brands/venue/".$venue->id, __("Edit: ") . $brand->name => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			// EDIT EXHIBITOR
			$brand = $this->brands_model->getBrandByID($id);
			if($brand == FALSE) redirect('events');

			$event = $this->event_model->getbyid($brand->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
            $this->iframeurl = "exhibitor/by-brand/" . $event->id . "/" .$id;
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array( 
							"eventid"		=> $event->id,
							"name" 			=> set_value('name')
						);

					if($this->general_model->update('exhibitorbrand', $id, $data)){
						$this->session->set_flashdata('event_feedback', __('The brand has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('brands/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_brand_edit', array('event' => $event, 'brand' => $brand, 'error' => $error), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Brand") => "brands/event/".$event->id, __("Edit: ") . $brand->name => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}
	
	function delete($id, $type = "event") {
		if($type == 'venue') {
			$brand = $this->brands_model->getBrandByID($id);
			$venue = $this->venue_model->getbyid($brand->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($this->general_model->remove('exhibitorbrand', $id)){
				$this->db->delete('exhibrand', array('exhibitorbrandid' => $id));
				$this->session->set_flashdata('event_feedback', __('The brand has successfully been deleted'));
				redirect('brands/venue/'.$brand->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('brands/venue/'.$brand->venueid);
			}
		} else {
			$brand = $this->brands_model->getBrandByID($id);
			$event = $this->event_model->getbyid($brand->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			if($this->general_model->remove('exhibitorbrand', $id)){
				$this->db->delete('exhibrand', array('exhibitorbrandid' => $id));
				$this->session->set_flashdata('event_feedback', __('The brand has successfully been deleted'));
				_updateTimeStamp($brand->eventid);
				redirect('brands/event/'.$brand->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('brands/event/'.$brand->eventid);
			}
		}
	}

}