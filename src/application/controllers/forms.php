<?php if(!defined('BASEPATH')) exit('No direct script access');

class Forms extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('forms_model');
		$this->load->model('module_mdl');
		$this->load->helper('excelexport');
		$this->load->model('appearance_model');
	}
	
	//php 4 constructor
	function Forms() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('forms_model');
	}
	
	function index() {
		$this->event();
	}
	
	function event($id, $launcherid, $groupid = '') {
		if($id == FALSE || $id == 0) redirect('events');
		if($launcherid == FALSE || $launcherid == 0) redirect('events');
		
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');  
		
		$masterPageTitle = 'Forms in '.$app->name;
		
		// Get Form Event
		$forms = $this->forms_model->getFormsByLauncherID($launcherid);
		if($forms[0]->singlescreen == 1):
			$this->db->select('id');
            $this->db->where('formid', $forms[0]->id);
            $this->db->order_by('order', 'asc');
            $q = $this->db->get('formscreen');
            $row = $q->result();
            if(isset($row[0])) {
            	redirect('formfields/formscreen/'.$row[0]->id);
            } else {
            	redirect('formscreens/form/'.$forms[0]->id);
            }
		else:
			redirect('formscreens/form/'.$forms[0]->id);
		endif;
                
        $this->iframeurl = 'forms/event/'.$id.'/'.$launcherid;
        
		$childcon = "formscreens";
		$confunc = "form";
		
		$headers = array('Title' => 'title', 'Order' => 'order');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => $forms, 'headers' => $headers, 'childcon' => $childcon, 'confunc' => $confunc, 'masterPageTitle' => $masterPageTitle, 'launcherid' => $launcherid), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array("Events" => "events", $event->name => "event/view/".$id, "Forms" => $this->uri->uri_string());
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $launcherid);
		$this->load->view('master', $cdata);
	}
	
	function venue($id, $launcherid, $groupid = '') {
		if($id == FALSE || $id == 0) redirect('venues');
		if($launcherid == FALSE || $launcherid == 0) redirect('venues');
		
		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$masterPageTitle = 'Forms in '.$app->name;
		
		// Get Launcher Forms
		$forms = $this->forms_model->getFormsByLauncherID($launcherid);
		
		if($forms[0]->singlescreen == 1):
			$this->db->select('id');
            $this->db->where('formid', $forms[0]->id);
            $this->db->order_by('order', 'asc');
            $q = $this->db->get('formscreen');
            $row = $q->result();
            if(isset($row[0])) {
            	redirect('formfields/formscreen/'.$row[0]->id);
            } else {
            	redirect('formscreens/form/'.$forms[0]->id);
            }
		else:
			redirect('formscreens/form/'.$forms[0]->id);
		endif;
		
		$this->iframeurl = 'forms/venue/'.$id.'/'.$launcherid;
                
		$childcon = "formscreens";
		$confunc = "form";
		
		$headers = array('Title' => 'title', 'Order' => 'order');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'data' => $forms, 'headers' => $headers, 'childcon' => $childcon, 'confunc' => $confunc, 'masterPageTitle' => $masterPageTitle, 'launcherid' => $launcherid), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, "Forms" => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $launcherid);
		$this->load->view('master', $cdata);
	}
	
	function app($id, $launcherid, $groupid = '') {
		if($id == FALSE || $id == 0) redirect('apps');
		if($launcherid == FALSE || $launcherid == 0) redirect('apps');
		
		$app = $this->app_model->get($id);
		if($app == FALSE) redirect('apps');
		
		$masterPageTitle = 'Forms in '.$app->name;
		
		// Get Launcher Forms
		$forms = $this->forms_model->getFormsByLauncherID($launcherid);
		if($forms[0]->singlescreen == 1):
			$this->db->select('id');
            $this->db->where('formid', $forms[0]->id);
            $this->db->order_by('order', 'asc');
            $q = $this->db->get('formscreen');
            $row = $q->result();
            if(isset($row[0])) {
            	redirect('formfields/formscreen/'.$row[0]->id);
            } else {
            	redirect('formscreens/form/'.$forms[0]->id);
            }
		else:
			redirect('formscreens/form/'.$forms[0]->id);
		endif;
		
		$this->iframeurl = 'forms/app/'.$id.'/'.$launcherid;
        
		$childcon = "formscreens";
		$confunc = "form";
		
		$headers = array('Title' => 'title', 'Order' => 'order');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('app' => $app, 'data' => $forms, 'headers' => $headers, 'childcon' => $childcon, 'confunc' => $confunc, 'masterPageTitle' => $masterPageTitle, 'launcherid' => $launcherid), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$cdata['crumb']			= array("Apps" => "apps", $app->name => "apps/view/".$id, "Forms" => $this->uri->uri_string());
		
		$this->load->view('master', $cdata);
	}
	
	function add($id, $type = "event") {
		
		$moduletypeid = 44;
		$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = $moduletypeid LIMIT 1")->row();
		
		if($type == 'venue') {
                    
			$this->load->library('form_validation');
			$error = "";
			
			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			
			$masterPageTitle = 'Forms in '.$app->name;
			
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
                        //Get Forms by Venue Id
			$forms = $this->forms_model->getFormsByVenueID($id);
			
			//If total form is not zero
			//if(count($forms) > 0 || $id == 0) redirect('venues');
			
			$singlescreen = 1;
			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('submissionbuttonlabel_'.$language->key, 'submissionbuttonlabel', 'trim');
					$this->form_validation->set_rules('submissionconfirmationtext_'.$language->key, 'submissionconfirmationtext', 'trim');
				}
				$emailsendresult  = $this->input->post('emailsendresult');
				$chkCustomURL  = $this->input->post('chkcustomurl');
				if( $chkCustomURL )
					$this->form_validation->set_rules('customurl', 'Custom URL', 'trim|required|callback_url_check');				
				//If form results are to be sent by email?
				if($emailsendresult == "yes"){
					$this->form_validation->set_rules('email', 'email', 'trim|required|callback_email_check');
				}
				
				$singlescreen = $this->input->post('multiscreen') == 1 ? 0 : 1;
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				
				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					if($error == '') {
						
						$customurl 		  = $this->input->post('customurl');
						$order = $this->input->post('order');
                        $getGeoLocation = $this->input->post('geolocation');
                                                
						if($order == '')
							$order = 0;

                        if($getGeoLocation == '' or $getGeoLocation != 1)
                            $getGeoLocation = 0;
                                                
						$emailsendresult 		  = $this->input->post('emailsendresult');
						$allowmutliplesubmissions = $this->input->post('allowmutliplesubmissions');
								
						
						//-------------- Addd to Launcher --------------//
						$launchericon = "";
						if($app->themeid != 0) {
							$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = $moduletypeid LIMIT 1");
							if($icon->num_rows() != 0) {
								$icon = $icon->row();
								$icon = $icon->icon;
								$image = $this->config->item('imagespath') . $icon;
								$newpath = 'upload/appimages/'.$appid .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
								if(file_exists($image)) {
									if(copy($image, $this->config->item('imagespath') . $newpath)) {
										$launchericon = $newpath;
									}
								}
							}
						}
						$data = array(
							'venueid'	=> $id,
							'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
							'module'	=> 'forms',
							'url'		=> '',
							'moduletypeid' => $moduletypeid,
							'icon'		=> $launchericon,
							'order'		=> $order,
							'active'	=> 1
						);
						
						if($launcherid = $this->general_model->insert('launcher', $data)) 
						{
							$this->form_validation->set_rules('image', 'image', 'trim');
							//Uploads Images
							if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
								mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0755, true);
							}

							$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
							$configexlogo['allowed_types'] = 'png';
							$configexlogo['max_width']  = '200';
							$configexlogo['max_height']  = '200';
							$this->load->library('upload', $configexlogo);
							$this->upload->initialize($configexlogo);
							if (!$this->upload->do_upload('image')) {
								$image = $launcher->icon; //No image uploaded!
								if($_FILES['image']['name'] != '') {
									$error = $this->upload->display_errors();
								}
							} else {
								//successfully uploaded
								$returndata = $this->upload->data();
								$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
								$this->general_model->update('launcher', $launcherid, array('icon' => $image));
							}

							// Adding to Module							
							$activeForm = $this->forms_model->getActiveForms('venueid', $id, 1);
							if( $activeForm == 1 ){
								$this->general_model->insert('module', array('moduletype' => 44,'venue'	=> $id));
							}
							//-------------------------------------------//
							
							foreach($languages as $language) {
								$this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));					
							}
							
							//-------------- Addd to Forms --------------//																	
							$data = array(
									"appid"					 	 => $app->id,
									"eventid"					 => 0,
									"venueid"					 => $id,
									"launcherid"				 => $launcherid,
									"title"             		 => set_value('title_'.  $app->defaultlanguage),
									"submissionbuttonlabel"		 => set_value('submissionbuttonlabel_'.  $app->defaultlanguage),
									"submissionconfirmationtext" => set_value('submissionconfirmationtext_'.  $app->defaultlanguage),
									"email" 					 => set_value('email'),
									"emailsendresult"			 => $emailsendresult,
									"allowmutliplesubmissions"	 => $allowmutliplesubmissions,
									"order"						 => $order,
                                    "getgeolocation"			 => $getGeoLocation,
									'singlescreen'				 => $singlescreen,
									'customurl'					 => $customurl,
									'useflows'					 => intval($this->input->post('useflows'))
								);
							
								if($newid = $this->general_model->insert('form', $data)){
								
								//add translated fields to translation table
								foreach($languages as $language) {
									//description
									
									$this->language_model->addTranslation('form', $newid, 'title', $language->key, $this->input->post('title_'.$language->key));
									$this->language_model->addTranslation('form', $newid, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
									$this->language_model->addTranslation('form', $newid, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								}						
								
								// ---------- Add Screen -----------------//
								if($singlescreen == 1):									
									
									$this->general_model->insert('formscreen', array(
										'formid'	=> $newid,
										'title'		=> 'Default Screen',
										'order'		=> 0,
										'visible'	=> 1
									));
									
								endif;
								
								//-------------- Addd to Tags --------------//
								
								$data = array(
									'appid'		=> $app->id,
									'venueid'	=> $id,
									'formid'	=> $newid,
									'tag'		=> $this->input->post('title_'.  $app->defaultlanguage),
									'active'	=> 1
								);
								
								$tagid = $this->general_model->insert('tag', $data);
								foreach($languages as $language) {
									$this->language_model->addTranslation('tag', $tagid, 'tag', $language->key, $this->input->post('title_'.$language->key));					
								}
								
								//-------------- --------------- --------------//
								
								$this->session->set_flashdata('event_feedback', __('The form has been successfully added!'));
								_updateTimeStamp($venue->id);
								if($error == '') {
									redirect('formscreens/form/'.$newid);
								}
							} 
								else {
							$error = __("Oops, something went wrong. Please try again.");
						}
								
						}
						else {
							$error = __("Oops, something went wrong. Please try again.");
						}				
					}
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_form_add', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle, 'singlescreen' => $singlescreen, 'defaultlauncher' => $defaultlauncher), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, "Forms" => "forms/venue/".$venue->id, "Add new" => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms');
			$this->load->view('master', $cdata);
		} 
		else if($type == 'event') {
			$this->load->library('form_validation');
			$error = "";
		
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$masterPageTitle = 'Forms in '.$app->name;
			
			//$this->iframeurl = 'sessions/event/'.$id;

			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			//Get Forms by Venue Id
			$forms = $this->forms_model->getFormsByEventID($id);
			
			//If total form is not zero
			//if(count($forms) > 0 || $id == 0) redirect('events');
			$singlescreen = 1;
			if($this->input->post('postback') == "postback") {
				
				foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('submissionbuttonlabel_'.$language->key, 'submissionbuttonlabel', 'trim');
					$this->form_validation->set_rules('submissionconfirmationtext_'.$language->key, 'submissionconfirmationtext', 'trim');
				}
				
				$emailsendresult  = $this->input->post('emailsendresult');
				$chkCustomURL  = $this->input->post('chkcustomurl');
				if( $chkCustomURL )
					$this->form_validation->set_rules('customurl', 'Custom URL', 'trim|required|callback_url_check');
				
				//If form results are to be sent by email?
				if($emailsendresult == "yes"){
					$this->form_validation->set_rules('email', 'email', 'trim|required|callback_email_check');
				}
				$singlescreen = $this->input->post('multiscreen') == 1 ? 0 : 1;
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				
				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {
									
					if($error == '') {	
						
						$customurl 		  = $this->input->post('customurl');
						$order 		  = $this->input->post('order');
                        $getGeoLocation = $this->input->post('geolocation');
                                                
						if($order == '')
							$order = 0;
						
                        if($getGeoLocation == '' or $getGeoLocation != 1)
                            $getGeoLocation = 0;
						
						$emailsendresult 		  = $this->input->post('emailsendresult');
						$allowmutliplesubmissions = $this->input->post('allowmutliplesubmissions');
						
						
						//-------------- Addd to Launcher --------------//
						$launchericon = "";
						if($app->themeid != 0) {
							$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = $moduletypeid LIMIT 1");
							if($icon->num_rows() != 0) {
								$icon = $icon->row();
								$icon = $icon->icon;
								$image = $this->config->item('imagespath') . $icon;
								$newpath = 'upload/appimages/'.$appid .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
								if(file_exists($image)) {
									if(copy($image, $this->config->item('imagespath') . $newpath)) {
										$launchericon = $newpath;
									}
								}
							}
						}
						$data = array(
							'eventid'	=> $id,
							'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
							'module'	=> 'forms',
							'url'		=> '',
							'moduletypeid' => $moduletypeid,
							'icon'		=> $launchericon,
							'order'		=> $order,
							'active'	=> 1
						);
						
						if($launcherid = $this->general_model->insert('launcher', $data)) 
						{
							$this->form_validation->set_rules('image', 'image', 'trim');
							//Uploads Images
							if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
								mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0755, true);
							}

							$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
							$configexlogo['allowed_types'] = 'png';
							$configexlogo['max_width']  = '200';
							$configexlogo['max_height']  = '200';
							$this->load->library('upload', $configexlogo);
							$this->upload->initialize($configexlogo);
							if (!$this->upload->do_upload('image')) {
								$image = $launcher->icon; //No image uploaded!
								if($_FILES['image']['name'] != '') {
									$error = $this->upload->display_errors();
								}
							} else {
								//successfully uploaded
								$returndata = $this->upload->data();
								$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
								$this->general_model->update('launcher', $launcherid, array('icon' => $image));
							}
							
							$this->form_validation->set_rules('image', 'image', 'trim');
							//Uploads Images
							if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
								mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0755, true);
							}

							$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
							$configexlogo['allowed_types'] = 'png';
							$configexlogo['max_width']  = '200';
							$configexlogo['max_height']  = '200';
							$this->load->library('upload', $configexlogo);
							$this->upload->initialize($configexlogo);
							if (!$this->upload->do_upload('image')) {
								$image = $launcher->icon; //No image uploaded!
								if($_FILES['image']['name'] != '') {
									$error = $this->upload->display_errors();
								}
							} else {
								//successfully uploaded
								$returndata = $this->upload->data();
								$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
								$this->general_model->update('launcher', $launcherid, array('icon' => $image));
							}

							// Adding to Module							
							$activeForm = $this->forms_model->getActiveForms('eventid', $id, 1);
							if( $activeForm == 1 ){
								$this->general_model->insert('module', array('moduletype' => 44,'event'	=> $id));
							}
							//-------------------------------------------//
							
							foreach($languages as $language) {
								$this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));					
							}
							
							//-------------- Addd to Forms --------------//																	
							$data = array(
									"appid"					 	 => $app->id,
									"eventid"					 => $id,
									"venueid"					 => 0,
									"launcherid"				 => $launcherid,
									"title"             		 => set_value('title_'.  $app->defaultlanguage),
									"submissionbuttonlabel"		 => set_value('submissionbuttonlabel_'.  $app->defaultlanguage),
									"submissionconfirmationtext" => set_value('submissionconfirmationtext_'.  $app->defaultlanguage),
									"email" 					 => set_value('email'),
									"emailsendresult"			 => $emailsendresult,
									"allowmutliplesubmissions"	 => $allowmutliplesubmissions,
									"order"						 => $order,
									"getgeolocation" 			 => $getGeoLocation,
									'singlescreen'				 => $singlescreen,
									'customurl'					 => $customurl,
									'useflows'					 => intval($this->input->post('useflows'))
								);
							
								if($newid = $this->general_model->insert('form', $data)){
								
								//add translated fields to translation table
								foreach($languages as $language) {
									//description
									
									$this->language_model->addTranslation('form', $newid, 'title', $language->key, $this->input->post('title_'.$language->key));
									$this->language_model->addTranslation('form', $newid, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
									$this->language_model->addTranslation('form', $newid, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								}						
								
								// ---------- Add Screen -----------------//
								if($singlescreen == 1):									
									
									$this->general_model->insert('formscreen', array(
										'formid'	=> $newid,
										'title'		=> 'Default Screen',
										'order'		=> 0,
										'visible'	=> 1
									));
									
								endif;
								
								//-------------- Addd to Tags --------------//
								
								$data = array(
									'appid'		=> $app->id,
									'eventid'	=> $id,
									'formid'	=> $newid,
									'tag'		=> $this->input->post('title_'.  $app->defaultlanguage),
									'active'	=> 1
								);
								
								$tagid = $this->general_model->insert('tag', $data);
								foreach($languages as $language) {
									$this->language_model->addTranslation('tag', $tagid, 'tag', $language->key, $this->input->post('title_'.$language->key));					
								}
								
								//-------------- --------------- --------------//
								
								$this->session->set_flashdata('event_feedback', __('The form has been successfully added!'));
								_updateTimeStamp($event->id);
								if($error == '') {
									redirect('formscreens/form/'.$newid);
								}
							} 
								else {
							$error = __("Oops, something went wrong. Please try again.");
						}
								
						}
						else {
							$error = __("Oops, something went wrong. Please try again.");
						}				
					}

				}
			}
			
			$cdata['content'] 		= $this->load->view('c_form_add', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle, 'singlescreen' => $singlescreen, 'defaultlauncher' => $defaultlauncher), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, "Forms" => "forms/event/".$event->id.'/'.$form->launcherid, "Add new" => $this->uri->uri_string());
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, "Forms" => "forms/event/".$event->id.'/'.$form->launcherid, "Add new" => $this->uri->uri_string());
			}
		
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms');
			$this->load->view('master', $cdata);
		}
		else {
			$this->load->library('form_validation');
			$error = "";
		
			$app = $this->app_model->get($id);
			if($app == FALSE) redirect('apps');
			$masterPageTitle = 'Forms in '.$app->name;
			//$this->iframeurl = 'sessions/event/'.$id;

			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			//Get Forms by Venue Id
			$forms = $this->forms_model->getFormsByEventID($id);
			
			//If total form is not zero
			//if(count($forms) > 0 || $id == 0) redirect('events');
			$singlescreen = 1;
			if($this->input->post('postback') == "postback") {
				
				foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('submissionbuttonlabel_'.$language->key, 'submissionbuttonlabel', 'trim');
					$this->form_validation->set_rules('submissionconfirmationtext_'.$language->key, 'submissionconfirmationtext', 'trim');
				}
				
				$emailsendresult  = $this->input->post('emailsendresult');
				//If form results are to be sent by email?
				if($emailsendresult == "yes"){
					$this->form_validation->set_rules('email', 'email', 'trim|required|callback_email_check');
				}
				$singlescreen = $this->input->post('multiscreen') == 1 ? 0 : 1;
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				
				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {
									
					if($error == '') {	
						
						$order 		  = $this->input->post('order');
                        if($order == '')
							$order = 0;
						
                        $getGeoLocation = $this->input->post('geolocation');
						if($getGeoLocation == '' or $getGeoLocation != 1)
							$getGeoLocation = 0;
						
						$emailsendresult 		  = $this->input->post('emailsendresult');
						$allowmutliplesubmissions = $this->input->post('allowmutliplesubmissions');
								
						
						//-------------- Addd to Launcher --------------//
						$launchericon = "";
						if($app->themeid != 0) {
							$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = $moduletypeid LIMIT 1");
							if($icon->num_rows() != 0) {
								$icon = $icon->row();
								$icon = $icon->icon;
								$image = $this->config->item('imagespath') . $icon;
								$newpath = 'upload/appimages/'.$appid .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
								if(file_exists($image)) {
									if(copy($image, $this->config->item('imagespath') . $newpath)) {
										$launchericon = $newpath;
									}
								}
							}
						}

						$data = array(
							'appid'	=> $id,
							'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
							'module'	=> 'forms',
							'url'		=> '',
							'moduletypeid' => $moduletypeid,
							'icon'		=> $launchericon,
							'order'		=> $order,
							'active'	=> 1
						);
						
						if($launcherid = $this->general_model->insert('launcher', $data)) 
						{
							
							$this->form_validation->set_rules('image', 'image', 'trim');
							//Uploads Images
							if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
								mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0755, true);
							}

							$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
							$configexlogo['allowed_types'] = 'png';
							$configexlogo['max_width']  = '200';
							$configexlogo['max_height']  = '200';
							$this->load->library('upload', $configexlogo);
							$this->upload->initialize($configexlogo);
							if (!$this->upload->do_upload('image')) {
								$image = $launcher->icon; //No image uploaded!
								if($_FILES['image']['name'] != '') {
									$error = $this->upload->display_errors();
								}
							} else {
								//successfully uploaded
								$returndata = $this->upload->data();
								$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
								$this->general_model->update('launcher', $launcherid, array('icon' => $image));
							}
							
							// Adding to Module							
							$activeForm = $this->forms_model->getActiveForms('appid', $app->id, 1);
							if( $activeForm == 1 ){
								$this->general_model->insert('module', array('moduletype' => 44,'app' => $app->id));
							}
							//-------------------------------------------//
							
							foreach($languages as $language) {
								$this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));					
							}
							
							//-------------- Addd to Forms --------------//																	
							$data = array(
									"appid"					 	 => $app->id,
									"eventid"					 => 0,
									"venueid"					 => 0,
									"launcherid"				 => $launcherid,
									"title"             		 => set_value('title_'.  $app->defaultlanguage),
									"submissionbuttonlabel"		 => set_value('submissionbuttonlabel_'.  $app->defaultlanguage),
									"submissionconfirmationtext" => set_value('submissionconfirmationtext_'.  $app->defaultlanguage),
									"email" 					 => set_value('email'),
									"emailsendresult"			 => $emailsendresult,
									"allowmutliplesubmissions"	 => $allowmutliplesubmissions,
									"order"						 => $order,
                                    "getgeolocation" 			 => $getGeoLocation,
									'singlescreen'				 => $singlescreen,
									'useflows'					 => intval($this->input->post('useflows'))
								);
							
								if($newid = $this->general_model->insert('form', $data)){
								
								//add translated fields to translation table
								foreach($languages as $language) {
									//description
									
									$this->language_model->addTranslation('form', $newid, 'title', $language->key, $this->input->post('title_'.$language->key));
									$this->language_model->addTranslation('form', $newid, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
									$this->language_model->addTranslation('form', $newid, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								}						
								
								// ---------- Add Screen -----------------//
								if($singlescreen == 1):									
									
									$this->general_model->insert('formscreen', array(
										'formid'	=> $newid,
										'title'		=> 'Default Screen',
										'order'		=> 0,
										'visible'	=> 1
									));
									
								endif;
								
								//-------------- Addd to Tags --------------//
								
								$data = array(
									'appid'		=> $app->id,
									'eventid'	=> 0,
									'formid'	=> $newid,
									'tag'		=> $this->input->post('title_'.  $app->defaultlanguage),
									'active'	=> 1
								);
								
								$tagid = $this->general_model->insert('tag', $data);
								foreach($languages as $language) {
									$this->language_model->addTranslation('tag', $tagid, 'tag', $language->key, $this->input->post('title_'.$language->key));					
								}
								
								//-------------- --------------- --------------//
								
								$this->session->set_flashdata('event_feedback', __('The form has been successfully added!'));
								_updateTimeStamp($app->id);
								if($error == '') {
									redirect('formscreens/form/'.$newid);
								}
							} 
								else {
							$error = __("Oops, something went wrong. Please try again.");
						}
								
						}
						else {
							$error = __("Oops, something went wrong. Please try again.");
						}				
					}

				}
			}
			
			$cdata['content'] 		= $this->load->view('c_form_add', array('app' => $app, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle, 'singlescreen' => $singlescreen, 'defaultlauncher' => $defaultlauncher), TRUE);
			$cdata['crumb']			= array("Apps" => "apps", $app->name => "apps/view/".$app->id, "Forms" => "forms/app/".$app->id.'/'.$form->launcherid, "Add new" => $this->uri->uri_string());
		
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function editlauncher($launcherid, $type = "event") {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
		
			$form = $this->forms_model->getFormByLauncherID($launcherid);			 
			if($form == FALSE) redirect('venues');
			$id = $form->id;
		
			$venue = $this->venue_model->getById($form->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$masterPageTitle = 'Forms in '.$app->name;
			
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			$launcher = $this->module_mdl->getLauncherById($launcherid);
			
			$this->iframeurl = 'forms/venue/'.$venue->id.'/'.$launcherid;
			$singlescreen = $form->singlescreen;

			$launcherbackgroundcolor = $this->appearance_model->getLauncherBackgroundColor($app->id);
			
			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('submissionbuttonlabel_'.$language->key, 'submissionbuttonlabel', 'trim');
					$this->form_validation->set_rules('submissionconfirmationtext_'.$language->key, 'submissionconfirmationtext', 'trim');
				}
				$emailsendresult  = $this->input->post('emailsendresult');
				$chkCustomURL  = $this->input->post('chkcustomurl');
				if( $chkCustomURL )
					$this->form_validation->set_rules('customurl', 'Custom URL', 'trim|required|callback_url_check');
				
				//If form results are to be sent by email?
				if($emailsendresult == "yes"){
					$this->form_validation->set_rules('email', 'email', 'trim|required|callback_email_check');
				}
				$singlescreen = $this->input->post('multiscreen') == 1 ? 0 : 1;
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				
				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					
					$customurl 		  = $this->input->post('customurl');
					$emailsendresult 		  = $this->input->post('emailsendresult');
					$allowmutliplesubmissions = $this->input->post('allowmutliplesubmissions');
					$getGeoLocation = $this->input->post('geolocation');
					if($getGeoLocation == '' or $getGeoLocation != 1)
						$getGeoLocation = 0;

					$emailconfirmation = $form->emailconfirmation;
					if($this->input->post('emailconfirmation') == '' || $this->input->post('emailconfirmation') != 1) {
						$emailconfirmation = 0;
					} else {
						$emailconfirmation = 1;
					}

					$data = array(
								"title"             		 => set_value('title_'.  $app->defaultlanguage),
								"submissionbuttonlabel"		 => set_value('submissionbuttonlabel_'.  $app->defaultlanguage),
								"submissionconfirmationtext" => set_value('submissionconfirmationtext_'.  $app->defaultlanguage),
								"email" 					 => set_value('email'),
								"emailsendresult"			 => $emailsendresult,
								"allowmutliplesubmissions"	 => $allowmutliplesubmissions,
								"order"						 => set_value('order'),
								'singlescreen'				 => $singlescreen,
								'getgeolocation'			 => $getGeoLocation,
								'emailconfirmation'			 => $emailconfirmation,
								'customurl'					 => $customurl,
								'useflows'					 => intval($this->input->post('useflows'))
							);
					
					if($this->forms_model->edit_form($id, $data)){
						
                        //add translated fields to translation table
	                        foreach($languages as $language) {
	                            
								$title = $this->input->post('title_'.$language->key);
	                            $nameSuccess = $this->language_model->updateTranslation('form', $id, 'title', $language->key, $title);
								if($nameSuccess == false || $nameSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'title', $language->key, $title);
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								}								
							}
						
							//-------------- Edit Launcher --------------//
							$displaytype = $launcher->displaytype;
							if($this->input->post('displaytype') == '' || $this->input->post('displaytype') != 1) {
								$displaytype = 'none';
							} else {
								$displaytype = '';
							}
							$data = array(
								'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
								'order'		=> $this->input->post('order'),
								'active'	=> 1,
								'displaytype' => $displaytype
							);
							
							if($this->forms_model->edit_table($launcherid, $data,'launcher'))
							{
								$descrSuccess = $this->language_model->updateTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								}
								
								$this->form_validation->set_rules('image', 'image', 'trim');
								//Uploads Images
								if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
									mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0755, true);
								}
	
								$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
								$configexlogo['allowed_types'] = 'png';
								$configexlogo['max_width']  = '200';
								$configexlogo['max_height']  = '200';
								$this->load->library('upload', $configexlogo);
								$this->upload->initialize($configexlogo);
								if (!$this->upload->do_upload('image')) {
									$image = $launcher->icon; //No image uploaded!
									if($_FILES['image']['name'] != '') {
										$error = $this->upload->display_errors();
									}
								} else {
									//successfully uploaded
									$returndata = $this->upload->data();
									$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
									$this->general_model->update('launcher', $launcherid, array('icon' => $image));
								}
							}
							
							//-------------- --------------- --------------//
							
							
							//----------------- Edit Tags -----------------//
							// $this->load->model('tag_model');
							
							// $data = array(
							// 	'appid'		=> $app->id,
							// 	'venueid'	=> $form->venueid,
							// 	'formid'	=> $id,
							// 	'tag'		=> $this->input->post('title_'.  $app->defaultlanguage),
							// 	'active'	=> 1
							// );
							
							// $tag = $this->tag_model->getTagIdbyFormID($id);
							
							// if($this->forms_model->edit_table($tag->id, $data,'tag'))
							// {
							// 	$descrSuccess = $this->language_model->updateTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
							// 	if($descrSuccess == false || $descrSuccess == null) {
							// 		$this->language_model->addTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
							// 	}
							// }
														
							//-------------- --------------- --------------//
							
						$this->session->set_flashdata('event_feedback', __('The form has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						if($error == '') {
							redirect('forms/venue/'.$venue->id.'/'.$form->launcherid);
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
		
			$cdata['content'] 		= $this->load->view('c_form_edit', array('venue' => $venue, 'form' => $form, 'error' => $error, 'languages' => $languages, 'app' => $app, 'launcher' => $launcher, 'masterPageTitle' => $masterPageTitle, 'singlescreen' => $singlescreen, 'launcherbackgroundcolor' => $launcherbackgroundcolor), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= array("Venues" => "venues", $venue->name => "venue/view/".$venue->id, "Edit: " . $form->title => $this->uri->uri_string());
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
			$this->load->view('master', $cdata);
		} 
		else if($type == 'event'){
			$this->load->library('form_validation');
			$error = "";
		
			$form = $this->forms_model->getFormByLauncherID($launcherid);			 
			if($form == FALSE) redirect('events');
			$id = $form->id;
			
		
			$event = $this->event_model->getbyid($form->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$masterPageTitle = 'Forms in '.$app->name;
			
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			$launcher = $this->module_mdl->getLauncherById($launcherid);
			
			$this->iframeurl = 'forms/event/'.$event->id.'/'.$launcherid;
			$singlescreen = $form->singlescreen;

			$launcherbackgroundcolor = $this->appearance_model->getLauncherBackgroundColor($app->id);
			
			if($this->input->post('postback') == "postback") {
				
				foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('submissionbuttonlabel_'.$language->key, 'submissionbuttonlabel', 'trim');
					$this->form_validation->set_rules('submissionconfirmationtext_'.$language->key, 'submissionconfirmationtext', 'trim');
				}
				$emailsendresult  = $this->input->post('emailsendresult');
				$chkCustomURL  = $this->input->post('chkcustomurl');
				if( $chkCustomURL )
					$this->form_validation->set_rules('customurl', 'Custom URL', 'trim|required|callback_url_check');
				
				//If form results are to be sent by email?
				if($emailsendresult == "yes"){
					$this->form_validation->set_rules('email', 'email', 'trim|required|callback_email_check');
				}
				$singlescreen = $this->input->post('multiscreen') == 1 ? 0 : 1;
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				
				if($this->form_validation->run() == FALSE){
					$error = validation_errors();				
				} else {										
					if($error == '') {
									
					if($this->form_validation->run() == FALSE || $error != ''){
						$error .= validation_errors();
					}else {					
						
						$customurl 		  = $this->input->post('customurl');
						$emailsendresult 		  = $this->input->post('emailsendresult');
						$allowmutliplesubmissions = $this->input->post('allowmutliplesubmissions');
						$getGeoLocation = $this->input->post('geolocation');
						if($getGeoLocation == '' or $getGeoLocation != 1)
							$getGeoLocation = 0;

						$emailconfirmation = $form->emailconfirmation;
						if($this->input->post('emailconfirmation') == '' || $this->input->post('emailconfirmation') != 1) {
							$emailconfirmation = 0;
						} else {
							$emailconfirmation = 1;
						}

						$opts = explode(',', $form->opts);
						if($launcher->moduletypeid == 61) {
							$key = 'moderation';
							$moderation = $this->input->post($key);
							if($moderation && !in_array($key, $opts)) {
								$opts[] = $key;
							} elseif(!$moderation && in_array($key, $opts)) {
								$opts = array_diff($opts, array($key));
							}
						}

						$opts = array_diff($opts, array(''));
						$opts = implode(',', $opts);
						
						$data = array(
								"title"             		 => set_value('title_'.  $app->defaultlanguage),
								"submissionbuttonlabel"		 => set_value('submissionbuttonlabel_'.  $app->defaultlanguage),
								"submissionconfirmationtext" => set_value('submissionconfirmationtext_'.  $app->defaultlanguage),
								"email" 					 => set_value('email'),
								"emailsendresult"			 => $emailsendresult,
								"allowmutliplesubmissions"	 => $allowmutliplesubmissions,
								"order"						 => set_value('order'),
								'singlescreen'				 => $singlescreen,
								'getgeolocation'			 => $getGeoLocation,
								'emailconfirmation'			 => $emailconfirmation,
								'opts' 						 => $opts,
								'customurl'					 => $customurl,
								'useflows'					 => intval($this->input->post('useflows'))
							);
						
						if($this->forms_model->edit_form($id, $data)){
								                        
	                        //add translated fields to translation table
	                        foreach($languages as $language) {
	                            
								$title = $this->input->post('title_'.$language->key);
	                            $nameSuccess = $this->language_model->updateTranslation('form', $id, 'title', $language->key, $title);
								if($nameSuccess == false || $nameSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'title', $language->key, $title);
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								}								
							}
							
							
							//-------------- Edit Launcher --------------//
							$displaytype = $launcher->displaytype;
							if($this->input->post('displaytype') == '' || $this->input->post('displaytype') != 1) {
								$displaytype = 'none';
							} else {
								$displaytype = '';
							}
							$data = array(
								'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
								'order'		=> $this->input->post('order'),
								'active'	=> 1,
								'displaytype' => $displaytype
							);
							
							if($this->forms_model->edit_table($launcherid, $data,'launcher'))
							{
								$descrSuccess = $this->language_model->updateTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								}

								//Uploads Images
								if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
									mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0775, true);
								}
								
								$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
								$configexlogo['allowed_types'] = 'png';
								$configexlogo['max_width']  = '200';
								$configexlogo['max_height']  = '200';
								$this->load->library('upload', $configexlogo);
								$this->upload->initialize($configexlogo);
								if (!$this->upload->do_upload('image')) {
									$image = $launcher->icon; //No image uploaded!
									if($_FILES['image']['name'] != '') {
										$error = $this->upload->display_errors();
									}
								} else {
									//successfully uploaded
									$returndata = $this->upload->data();
									$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
									$this->general_model->update('launcher', $launcherid, array('icon' => $image));
								}
							}
							
							//-------------- --------------- --------------//
							
							
							//----------------- Edit Tags -----------------//
							// $this->load->model('tag_model');
							
							// $data = array(
							// 	'appid'		=> $app->id,
							// 	'eventid'	=> $form->eventid,
							// 	'formid'	=> $id,
							// 	'tag'		=> $this->input->post('title_'.  $app->defaultlanguage),
							// 	'active'	=> 1
							// );
							
							// $tag = $this->tag_model->getTagIdbyFormID($id);
							
							// if($this->forms_model->edit_table($tag->id, $data,'tag'))
							// {
							// 	$descrSuccess = $this->language_model->updateTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
							// 	if($descrSuccess == false || $descrSuccess == null) {
							// 		$this->language_model->addTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
							// 	}
							// }
														
							//-------------- --------------- --------------//
							
							
							$this->session->set_flashdata('event_feedback', __('The form has successfully been updated'));
							_updateTimeStamp($event->id);
							if($error == '') {
								redirect('forms/event/'.$event->id.'/'.$form->launcherid);
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
				}
				}
			}
			
			
			$cdata['content'] 		= $this->load->view('c_form_edit', array('event' => $event, 'form' => $form, 'error' => $error, 'languages' => $languages, 'app' => $app, 'launcher' => $launcher, 'masterPageTitle' => $masterPageTitle, 'singlescreen' => $singlescreen, 'launcherbackgroundcolor' => $launcherbackgroundcolor), TRUE);
			$cdata['crumb']			= array("Events" => "events", $event->name => "event/view/".$event->id, "Edit: " . $form->title => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $launcherid);
			$this->load->view('master', $cdata);
		}
		else {
			$this->load->library('form_validation');
			$error = "";
		
			$form = $this->forms_model->getFormByLauncherID($launcherid);			 
			if($form == FALSE) redirect('events');
			$id = $form->id;
			
		
			$app = $this->app_model->get($form->appid);
			if($app == FALSE) redirect('apps');
			$masterPageTitle = 'Forms in '.$app->name;
			
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			$this->iframeurl = 'forms/app/'.$app->id.'/'.$launcherid;
			$singlescreen = $form->singlescreen;
			
			$launcher = $this->module_mdl->getLauncherById($launcherid);

			$launcherbackgroundcolor = $this->appearance_model->getLauncherBackgroundColor($app->id);
			
			if($this->input->post('postback') == "postback") {
				
				foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('submissionbuttonlabel_'.$language->key, 'submissionbuttonlabel', 'trim');
					$this->form_validation->set_rules('submissionconfirmationtext_'.$language->key, 'submissionconfirmationtext', 'trim');
				}
				
				$emailsendresult  = $this->input->post('emailsendresult');
				//If form results are to be sent by email?
				if($emailsendresult == "yes"){
					$this->form_validation->set_rules('email', 'email', 'trim|required|callback_email_check');
				}
				$singlescreen = $this->input->post('multiscreen') == 1 ? 0 : 1;
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');

				if( $chkCustomURL )
					$this->form_validation->set_rules('customurl', 'Custom URL', 'trim|required|callback_url_check');
				
				if($this->form_validation->run() == FALSE){
					$error = validation_errors();				
				} else {										
					if($error == '') {
									
					if($this->form_validation->run() == FALSE || $error != ''){
						$error .= validation_errors();
					}else {					
						
						$customurl 		  = $this->input->post('customurl');
						$emailsendresult 		  = $this->input->post('emailsendresult');
						$allowmutliplesubmissions = $this->input->post('allowmutliplesubmissions');
						$getGeoLocation = $this->input->post('geolocation');
						if($getGeoLocation == '' or $getGeoLocation != 1)
							$getGeoLocation = 0;

						$emailconfirmation = $form->emailconfirmation;
						if($this->input->post('emailconfirmation') == '' || $this->input->post('emailconfirmation') != 1) {
							$emailconfirmation = 0;
						} else {
							$emailconfirmation = 1;
						}
						
						$data = array(
								"title"             		 => set_value('title_'.  $app->defaultlanguage),
								"submissionbuttonlabel"		 => set_value('submissionbuttonlabel_'.  $app->defaultlanguage),
								"submissionconfirmationtext" => set_value('submissionconfirmationtext_'.  $app->defaultlanguage),
								"email" 					 => set_value('email'),
								"emailsendresult"			 => $emailsendresult,
								"allowmutliplesubmissions"	 => $allowmutliplesubmissions,
								"order"						 => set_value('order'),
								'singlescreen'				 => $singlescreen,
								'getgeolocation'			 => $getGeoLocation,
								'emailconfirmation'			 => $emailconfirmation,
								'customurl'					 => $customurl,
								'useflows'					 => intval($this->input->post('useflows'))
							);
						
						if($this->forms_model->edit_form($id, $data)){
								                        
	                        //add translated fields to translation table
	                        foreach($languages as $language) {
	                            
								$title = $this->input->post('title_'.$language->key);
	                            $nameSuccess = $this->language_model->updateTranslation('form', $id, 'title', $language->key, $title);
								if($nameSuccess == false || $nameSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'title', $language->key, $title);
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								}								
							}
							
							
							//-------------- Edit Launcher --------------//
							$displaytype = $launcher->displaytype;
							if($this->input->post('displaytype') == '' || $this->input->post('displaytype') != 1) {
								$displaytype = 'none';
							} else {
								$displaytype = '';
							}
							$data = array(
								'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
								'order'		=> $this->input->post('order'),
								'active'	=> 1,
								'displaytype' => $displaytype
							);
							
							if($this->forms_model->edit_table($launcherid, $data,'launcher'))
							{
								$descrSuccess = $this->language_model->updateTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								}

								//Uploads Images
								if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
									mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0775, true);
								}
								
								$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
								$configexlogo['allowed_types'] = 'png';
								$configexlogo['max_width']  = '200';
								$configexlogo['max_height']  = '200';
								$this->load->library('upload', $configexlogo);
								$this->upload->initialize($configexlogo);
								if (!$this->upload->do_upload('image')) {
									$image = $launcher->icon; //No image uploaded!
									if($_FILES['image']['name'] != '') {
										$error = $this->upload->display_errors();
									}
								} else {
									//successfully uploaded
									$returndata = $this->upload->data();
									$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
									$this->general_model->update('launcher', $launcherid, array('icon' => $image));
								}
							}
							
							//-------------- --------------- --------------//
							
							
							//----------------- Edit Tags -----------------//
							// $this->load->model('tag_model');
							
							// $data = array(
							// 	'appid'		=> $app->id,
							// 	'eventid'	=> $form->eventid,
							// 	'formid'	=> $id,
							// 	'tag'		=> $this->input->post('title_'.  $app->defaultlanguage),
							// 	'active'	=> 1
							// );
							
							// $tag = $this->tag_model->getTagIdbyFormID($id);
							
							// if($this->forms_model->edit_table($tag->id, $data,'tag'))
							// {
							// 	$descrSuccess = $this->language_model->updateTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
							// 	if($descrSuccess == false || $descrSuccess == null) {
							// 		$this->language_model->addTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
							// 	}
							// }
														
							//-------------- --------------- --------------//
							
							
							$this->session->set_flashdata('event_feedback', __('The form has successfully been updated'));
							_updateTimeStamp($app->id);
							if($error == '') {
								redirect('forms/app/'.$app->id.'/'.$form->launcherid);
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
				}
				}
			}
			
			
			$cdata['content'] 		= $this->load->view('c_form_edit', array('app' => $app, 'form' => $form, 'error' => $error, 'languages' => $languages, 'app' => $app, 'launcher' => $launcher, 'masterPageTitle' => $masterPageTitle, 'singlescreen' => $singlescreen, 'launcherbackgroundcolor' => $launcherbackgroundcolor), TRUE);
			$cdata['crumb']			= array("Apps" => "apps", $app->name => "apps/view/".$app->id, "Edit: " . $form->title => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$this->load->view('master', $cdata);
		}
	}
	
	function edit($id, $type = "event") {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
		
			$form = $this->forms_model->getFormByID($id);			 
			if($form == FALSE) redirect('venues');
			
			$venue = $this->venue_model->getById($form->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$masterPageTitle = 'Forms in '.$app->name;
			
			$languages = $this->language_model->getLanguagesOfApp($app->id);
						
			
			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('submissionbuttonlabel_'.$language->key, 'submissionbuttonlabel', 'trim');
					$this->form_validation->set_rules('submissionconfirmationtext_'.$language->key, 'submissionconfirmationtext', 'trim');
					
					$emailsendresult  = $this->input->post('emailsendresult');
					//If form results are to be sent by email?
					if($emailsendresult == "yes"){
						$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
					}
                }
				
				$emailsendresult  = $this->input->post('emailsendresult');
				$chkCustomURL  = $this->input->post('chkcustomurl');
				if( $chkCustomURL )
					$this->form_validation->set_rules('customurl', 'Custom URL', 'trim|required|callback_url_check');
				
				//If form results are to be sent by email?
				if($emailsendresult == "yes"){
					$this->form_validation->set_rules('email', 'email', 'trim|required|callback_email_check');
				}
								
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				
				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					
                    $customurl 		  = $this->input->post('customurl');
					$getGeoLocation = $this->input->post('geolocation');
                    if($getGeoLocation == '' or $getGeoLocation != 1)
                                            $getGeoLocation = 0;
                                    
                                        $emailsendresult 		  = $this->input->post('emailsendresult');
					$allowmutliplesubmissions = $this->input->post('allowmutliplesubmissions');
					
					$data = array(
								"title"             		 => set_value('title_'.  $app->defaultlanguage),
								"submissionbuttonlabel"		 => set_value('submissionbuttonlabel_'.  $app->defaultlanguage),
								"submissionconfirmationtext" => set_value('submissionconfirmationtext_'.  $app->defaultlanguage),
								"email" 					 => set_value('email'),
								"emailsendresult"			 => $emailsendresult,
								"allowmutliplesubmissions"	 => $allowmutliplesubmissions,
								"order"						 => set_value('order'),
                                "getgeolocation" 			 => $getGeoLocation,
								'customurl'					 => $customurl,
								'useflows'					 => intval($this->input->post('useflows'))
							);
					
					if($this->forms_model->edit_form($id, $data)){
						
                        //add translated fields to translation table
	                        foreach($languages as $language) {
	                            
								$title = $this->input->post('title_'.$language->key);
	                            $nameSuccess = $this->language_model->updateTranslation('form', $id, 'title', $language->key, $title);
								if($nameSuccess == false || $nameSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'title', $language->key, $title);
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								}								
							}
						
							//-------------- Edit Launcher --------------//
							
							$data = array(
								'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
								'order'		=> $order,
								'active'	=> 1
							);
							
							if($this->forms_model->edit_table($form->launcherid, $data,'launcher'))
							{
								$descrSuccess = $this->language_model->updateTranslation('launcher', $form->launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('launcher', $form->launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								}

								//Uploads Images
								if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
									mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0775, true);
								}
								
								$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
								$configexlogo['allowed_types'] = 'png';
								$configexlogo['max_width']  = '200';
								$configexlogo['max_height']  = '200';
								$this->load->library('upload', $configexlogo);
								$this->upload->initialize($configexlogo);
								if (!$this->upload->do_upload('image')) {
									$image = $launcher->icon; //No image uploaded!
									if($_FILES['image']['name'] != '') {
										$error = $this->upload->display_errors();
									}
								} else {
									//successfully uploaded
									$returndata = $this->upload->data();
									$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
									$this->general_model->update('launcher', $launcherid, array('icon' => $image));
								}
							}
							
							//-------------- --------------- --------------//
							
							
							//----------------- Edit Tags -----------------//
							$this->load->model('tag_model');
							
							$data = array(
								'appid'		=> $app->id,
								'venueid'	=> $form->venueid,
								'formid'	=> $id,
								'tag'		=> $this->input->post('title_'.  $app->defaultlanguage),
								'active'	=> 1
							);
							
							$tag = $this->tag_model->getTagIdbyFormID($id);
							
							if($this->forms_model->edit_table($tag->id, $data,'tag'))
							{
								$descrSuccess = $this->language_model->updateTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
								}
							}
														
							//-------------- --------------- --------------//
							
						$this->session->set_flashdata('event_feedback', __('The form has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						redirect('forms/venue/'.$venue->id.'/'.$form->launcherid);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
		
			$cdata['content'] 		= $this->load->view('c_form_edit', array('venue' => $venue, 'form' => $form, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= array("Venues" => "venues", $venue->name => "venue/view/".$venue->id, "Edit: " . $form->title => $this->uri->uri_string());
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
			$this->load->view('master', $cdata);
		} 
		else if($type == 'event'){
			$this->load->library('form_validation');
			$error = "";
		
			$form = $this->forms_model->getFormByID($id);			 
			if($form == FALSE) redirect('events');
			
		
			$event = $this->event_model->getbyid($form->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$masterPageTitle = 'Forms in '.$app->name;
			
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			// $this->iframeurl = 'forms/view/'.$id.'?r=1&width=247';
			$this->iframeurl = 'formscreens/form/'.$id.'?r=1&width=247';
			
			if($this->input->post('postback') == "postback") {
				
				foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('submissionbuttonlabel_'.$language->key, 'submissionbuttonlabel', 'trim');
					$this->form_validation->set_rules('submissionconfirmationtext_'.$language->key, 'submissionconfirmationtext', 'trim');
					
				}
				
				$emailsendresult  = $this->input->post('emailsendresult');
				$chkCustomURL  = $this->input->post('chkcustomurl');
				if( $chkCustomURL )
					$this->form_validation->set_rules('customurl', 'Custom URL', 'trim|required|callback_url_check');
				
				//If form results are to be sent by email?
				if($emailsendresult == "yes"){
					$this->form_validation->set_rules('email', 'email', 'trim|required|callback_email_check');
				}
									
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				
				if($this->form_validation->run() == FALSE){
					$error = validation_errors();				
				} else {										
					if($error == '') {
									
					if($this->form_validation->run() == FALSE || $error != ''){
						$error .= validation_errors();
					}else {					
						
						$customurl 		  = $this->input->post('customurl');
						$getGeoLocation = $this->input->post('geolocation');
                                                
                        if($getGeoLocation == '' or $getGeoLocation != 1)
                            $getGeoLocation = 0;
                
                        $emailsendresult 		  = $this->input->post('emailsendresult');
						$allowmutliplesubmissions = $this->input->post('allowmutliplesubmissions');
						
						$data = array(
								"title"             		 => set_value('title_'.  $app->defaultlanguage),
								"submissionbuttonlabel"		 => set_value('submissionbuttonlabel_'.  $app->defaultlanguage),
								"submissionconfirmationtext" => set_value('submissionconfirmationtext_'.  $app->defaultlanguage),
								"email" 					 => set_value('email'),
								"emailsendresult"			 => $emailsendresult,
								"allowmutliplesubmissions"	 => $allowmutliplesubmissions,
								"order"						 => set_value('order'),
                                "getgeolocation" 			 => $getGeoLocation,
								'customurl'					 => $customurl,
								'useflows'					 => intval($this->input->post('useflows'))
							);
						
						if($this->forms_model->edit_form($id, $data)){
								                        
	                        //add translated fields to translation table
	                        foreach($languages as $language) {
	                            
								$title = $this->input->post('title_'.$language->key);
	                            $nameSuccess = $this->language_model->updateTranslation('form', $id, 'title', $language->key, $title);
								if($nameSuccess == false || $nameSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'title', $language->key, $title);
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								}								
							}
							
							
							//-------------- Edit Launcher --------------//
							
							$data = array(
								'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
								'order'		=> $order,
								'active'	=> 1
							);
							
							if($this->forms_model->edit_table($form->launcherid, $data,'launcher'))
							{
								$descrSuccess = $this->language_model->updateTranslation('launcher', $form->launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('launcher', $form->launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								}

								//Uploads Images
								if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
									mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0775, true);
								}
								
								$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
								$configexlogo['allowed_types'] = 'png';
								$configexlogo['max_width']  = '200';
								$configexlogo['max_height']  = '200';
								$this->load->library('upload', $configexlogo);
								$this->upload->initialize($configexlogo);
								if (!$this->upload->do_upload('image')) {
									$image = $launcher->icon; //No image uploaded!
									if($_FILES['image']['name'] != '') {
										$error = $this->upload->display_errors();
									}
								} else {
									//successfully uploaded
									$returndata = $this->upload->data();
									$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
									$this->general_model->update('launcher', $launcherid, array('icon' => $image));
								}
							}
							
							//-------------- --------------- --------------//
							
							
							//----------------- Edit Tags -----------------//
							$this->load->model('tag_model');
							
							$data = array(
								'appid'		=> $app->id,
								'eventid'	=> $form->eventid,
								'formid'	=> $id,
								'tag'		=> $this->input->post('title_'.  $app->defaultlanguage),
								'active'	=> 1
							);
							
							$tag = $this->tag_model->getTagIdbyFormID($id);
							
							if($this->forms_model->edit_table($tag->id, $data, 'tag'))
							{
								$descrSuccess = $this->language_model->updateTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
								}
							}
														
							//-------------- --------------- --------------//
							
							
							$this->session->set_flashdata('event_feedback', __('The form has successfully been updated'));
							_updateTimeStamp($event->id);
							redirect('forms/event/'.$event->id.'/'.$form->launcherid);
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
				}
				}
			}
			
			
			$cdata['content'] 		= $this->load->view('c_form_edit', array('event' => $event, 'form' => $form, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle), TRUE);
			$cdata['crumb']			= array("Events" => "events", $event->name => "event/view/".$event->id, "Edit: " . $form->title => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
			$this->load->view('master', $cdata);
		}
		else {
			$this->load->library('form_validation');
			$error = "";
		
			$form = $this->forms_model->getFormByID($id);			 
			if($form == FALSE) redirect('events');
			
			$app = $this->app_model->get($form->appid);
			if($app == FALSE) redirect('apps');
			$masterPageTitle = 'Forms in '.$app->name;
			
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			$this->iframeurl = 'forms/view/'.$id.'?r=1&width=247';
			
			if($this->input->post('postback') == "postback") {
				
				foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('submissionbuttonlabel_'.$language->key, 'submissionbuttonlabel', 'trim');
					$this->form_validation->set_rules('submissionconfirmationtext_'.$language->key, 'submissionconfirmationtext', 'trim');
					
					$emailsendresult  = $this->input->post('emailsendresult');
					//If form results are to be sent by email?
					if($emailsendresult == "yes"){
						$this->form_validation->set_rules('email', 'email', 'trim|required|callback_email_check');
					}
                }				
				$this->form_validation->set_rules('order', 'order', 'trim|numeric');
				$chkCustomURL  = $this->input->post('chkcustomurl');
				if( $chkCustomURL )
					$this->form_validation->set_rules('customurl', 'Custom URL', 'trim|required|callback_url_check');
				
				if($this->form_validation->run() == FALSE){
					$error = validation_errors();				
				} else {										
					if($error == '') {
									
					if($this->form_validation->run() == FALSE || $error != ''){
						$error .= validation_errors();
					}else {					
						
						$customurl 		  = $this->input->post('customurl');
						$getGeoLocation = $this->input->post('geolocation');
                                                
                        if($getGeoLocation == '' or $getGeoLocation != 1)
                            $getGeoLocation = 0;
                
                        $emailsendresult 		  = $this->input->post('emailsendresult');
						$allowmutliplesubmissions = $this->input->post('allowmutliplesubmissions');
						
						$data = array(
								"title"             		 => set_value('title_'.  $app->defaultlanguage),
								"submissionbuttonlabel"		 => set_value('submissionbuttonlabel_'.  $app->defaultlanguage),
								"submissionconfirmationtext" => set_value('submissionconfirmationtext_'.  $app->defaultlanguage),
								"email" 					 => set_value('email'),
								"emailsendresult"			 => $emailsendresult,
								"allowmutliplesubmissions"	 => $allowmutliplesubmissions,
								"order"						 => set_value('order'),
                                "getgeolocation" 			 => $getGeoLocation,
                                'customurl'					 => $customurl,
								'useflows'					 => intval($this->input->post('useflows'))
							);
						
						if($this->forms_model->edit_form($id, $data)){
								                        
	                        //add translated fields to translation table
	                        foreach($languages as $language) {
	                            
								$title = $this->input->post('title_'.$language->key);
	                            $nameSuccess = $this->language_model->updateTranslation('form', $id, 'title', $language->key, $title);
								if($nameSuccess == false || $nameSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'title', $language->key, $title);
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionbuttonlabel', $language->key, $this->input->post('submissionbuttonlabel_'.$language->key));
								}
								
								$descrSuccess = $this->language_model->updateTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('form', $id, 'submissionconfirmationtext', $language->key, $this->input->post('submissionconfirmationtext_'.$language->key));
								}								
							}
							
							
							//-------------- Edit Launcher --------------//
							
							$data = array(
								'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
								'order'		=> $order,
								'active'	=> 1
							);
							
							if($this->forms_model->edit_table($form->launcherid, $data,'launcher'))
							{
								$descrSuccess = $this->language_model->updateTranslation('launcher', $form->launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('launcher', $form->launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
								}
							}
							
							//-------------- --------------- --------------//
							
							
							//----------------- Edit Tags -----------------//
							$this->load->model('tag_model');
							
							$data = array(
								'appid'		=> $app->id,
								'eventid'	=> $form->eventid,
								'formid'	=> $id,
								'tag'		=> $this->input->post('title_'.  $app->defaultlanguage),
								'active'	=> 1
							);
							
							$tag = $this->tag_model->getTagIdbyFormID($id);
							
							if($this->forms_model->edit_table($tag->id, $data, 'tag'))
							{
								$descrSuccess = $this->language_model->updateTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('tag', $tag->id, 'title', $language->key, $this->input->post('title_'.$language->key));
								}
							}
														
							//-------------- --------------- --------------//
							
							
							$this->session->set_flashdata('event_feedback', __('The form has successfully been updated'));
							_updateTimeStamp($app->id);
							redirect('forms/app/'.$app->id.'/'.$form->launcherid);
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
				}
				}
			}
			
			
			$cdata['content'] 		= $this->load->view('c_form_edit', array('app' => $app, 'form' => $form, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle), TRUE);
			$cdata['crumb']			= array("Apps" => "apps", $app->name => "apps/view/".$app->id, "Edit: " . $form->title => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$this->load->view('master', $cdata);
		}
	}
	
	function module($trigger= '', $id, $launcherid, $type = "event") {
		$launcher = $this->module_mdl->getLauncherById($launcherid);
        if($type == "venue") {
			$venueid = $id;
			$this->load->model('venue_model');
			$venue = $this->venue_model->getById($venueid);
                        
            $this->load->model('module_mdl');
            $app = _actionAllowed($venue, 'venue','returnApp');
                        
			switch($trigger){
				case 'activate':
					// Add module to event
					$this->load->model('forms_model');
					$form = $this->forms_model->getFormByLauncherID($launcherid);                    
					if($form == FALSE) 
					{
						$title = $launcher->title;
						
						$data = array(
							'title'		=> $launcher->title,
							'appid'		=> $launcher->appid,
							'venueid'	=> $launcher->venueid,
							'eventid'	=> $launcher->eventid
						);
						
						$formid = $this->general_model->insert('form', $data);
					}
                                         
                    //-------------- Update Launcher --------------//							
					$data = array(
						'active'	=> 1
					);
					
					$this->forms_model->edit_table($launcherid, $data, 'launcher');					
					//-------------- --------------- --------------//
					$activeForm = $this->forms_model->getActiveForms('venueid', $id, 1, $launcher->moduletypeid);
					if( $activeForm == 1 ){
						$this->general_model->insert('module', array('moduletype' => $launcher->moduletypeid,'venue'	=> $id));
					}
					break;
				case 'deactivate':
					//-------------- Update Launcher --------------//							
					$data = array(
						'active'	=> 0
					);
					
					$this->forms_model->edit_table($launcherid, $data, 'launcher');
					$activeForm = $this->forms_model->getActiveForms('venueid', $id, 1, $launcher->moduletypeid);
					if( $activeForm == 0 ){
						$this->db->where('moduletype', $launcher->moduletypeid);
						$this->db->where('venue', $id);
						$this->db->delete('module');
					}
					break;
				default:
					break;
			}
			_updateTimeStamp($venueid);
			redirect('venue/view/'.$venueid);
		} else if($type == 'event'){
			$eventid = $id;
			$this->load->model('event_model');
			$event = $this->event_model->getbyid($eventid);
                        
			$this->load->model('module_mdl');
			$app = _actionAllowed($event, 'event','returnApp');
			switch($trigger){
				case 'activate':
					// Add module to event
					$this->load->model('forms_model');
					$form = $this->forms_model->getFormByLauncherID($launcherid);			 
					if($form == FALSE) 
					{
						$title = $launcher->title;
						
						$data = array(
							'title'		=> $launcher->title,
							'appid'		=> $launcher->appid,
							'venueid'	=> $launcher->venueid,
							'eventid'	=> $launcher->eventid
						);

						if($launcher->moduletypeid == 61) {
							$data['opts'] = 'moderation';
						}
						
						$formid = $this->general_model->insert('form', $data);
						
					} else {
						if($launcher->moduletypeid == 61) {
							$data['opts'] = 'moderation';
							$this->general_model->update('form', $form->id, $data);
						}
					}
					
					//-------------- Update Launcher --------------//							
					$data = array(
						'active'	=> 1
					);
					
					$this->forms_model->edit_table($launcherid, $data, 'launcher');
					//-------------- --------------- --------------//
					$activeForm = $this->forms_model->getActiveForms('eventid', $id, 1, $launcher->moduletypeid);
					if( $activeForm == 1 ){
						$this->general_model->insert('module', array('moduletype' => $launcher->moduletypeid,'event'	=> $id));
					}
					break;
				case 'deactivate':
					//-------------- Update Launcher --------------//							
					$data = array(
						'active'	=> 0
					);
					
					$this->forms_model->edit_table($launcherid, $data, 'launcher');
					$activeForm = $this->forms_model->getActiveForms('eventid', $id, 1, $launcher->moduletypeid);
					if( $activeForm == 0 ){
						$this->db->where('moduletype', $launcher->moduletypeid);
						$this->db->where('event', $id);
						$this->db->delete('module');
					}
					break;
				default:
					break;
			}
			_updateTimeStamp($eventid);
			redirect('event/view/'.$eventid);
		} else {
			$appid = $id;
			$this->load->model('module_mdl');
			$app = $this->app_model->get($appid);
			if($app == FALSE) redirect('apps');
			_actionAllowed($app, 'app','');
			
			switch($trigger){
				case 'activate':
					// Add module to event
					$this->load->model('forms_model');
					$form = $this->forms_model->getFormByLauncherID($launcherid);			 
					if($form == FALSE) {
						$title = $launcher->title;
						
						$data = array(
							'title'		=> $launcher->title,
							'appid'		=> $launcher->appid,
							'venueid'	=> $launcher->venueid,
							'eventid'	=> $launcher->eventid
						);
						
						$formid = $this->general_model->insert('form', $data);
					}
					
					//-------------- Update Launcher --------------//							
					$data = array(
						'active'	=> 1
					);
					
					$this->forms_model->edit_table($launcherid, $data, 'launcher');
					//-------------- --------------- --------------//
					$activeForm = $this->forms_model->getActiveForms('appid', $id, 1, $launcher->moduletypeid);
					if( $activeForm == 1 ){
						$this->general_model->insert('module', array('moduletype' => $launcher->moduletypeid,'app'	=> $id));
					}
					break;
				case 'deactivate':
					//-------------- Update Launcher --------------//							
					$data = array(
						'active'	=> 0
					);
					
					$this->forms_model->edit_table($launcherid, $data, 'launcher');
					$activeForm = $this->forms_model->getActiveForms('appid', $id, 1, $launcher->moduletypeid);
					if( $activeForm == 0 ){
						$this->db->where('moduletype', $launcher->moduletypeid);
						$this->db->where('app', $id);
						$this->db->delete('module');
					}
					break;
				default:
					break;
			}
			_updateTimeStamp($appid);
			redirect('apps/view/'.$appid);
		}
	}
	
	function delete($id, $type = "event") {
		$moduletypeid = 44;
		if($type == "venue") {
			$form = $this->forms_model->getFormByID($id);
            $this->language_model->removeTranslations('form', $id);
			if($this->general_model->remove('form', $id)){
				
				//Remove Tags....
				$this->load->model('tag_model');
				
				$tag = $this->tag_model->getTagIdbyFormID($id);
				if(!empty($tag->id)) {
					$this->general_model->remove('tag', $tag->id);
				}
				
				
				//Remove launcher
				$this->general_model->remove('launcher', $form->launcherid);
				
				// Count Venue Launchers of this ModuleType...
				$venuelaunchers = $this->venue_model->countLauncherFromVenueModule($venue->id, $moduletypeid);
				if(!$venuelaunchers){
					
					$totalmodule = $this->module_mdl->countModuleFromVenueModule($venue->id, $moduletypeid);
					if($totalmodule){
						//Add to Module
						$this->module_mdl->removeModuleFromVenue($venue->id, $moduletypeid);
					}
					
				}
				//-------------------------------------------//
							
				$this->session->set_flashdata('event_feedback', __('The form has successfully been deleted'));
				_updateVenueTimeStamp($form->venueid);
				redirect('venue/view/'.$form->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('forms/venue/'.$form->venueid.'/'.$form->launcherid);
			}
		} 
		else if($type == 'event'){
			$form = $this->forms_model->getFormByID($id);
			$event = $this->event_model->getbyid($form->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
            $this->language_model->removeTranslations('form', $id);
			if($this->general_model->remove('form', $id)){
				
				//Remove Tags....
				$this->load->model('tag_model');
				
				$tag = $this->tag_model->getTagIdbyFormID($id);
				if(!empty($tag->id)) {
					$this->general_model->remove('tag', $tag->id);
				}
				
				//Remove launcher
				$this->general_model->remove('launcher', $form->launcherid);
				
				// Count Venue Launchers of this ModuleType...
				$eventlaunchers = $this->event_model->countLauncherFromEventModule($event->id, $moduletypeid);
				if(!$eventlaunchers){
					
					$totalmodule = $this->module_mdl->countModuleFromEventModule($event->id, $moduletypeid);
					if($totalmodule){
						//Add to Module
						$this->module_mdl->removeModuleFromEvent($event->id, $moduletypeid);
					}
					
				}
				
				//-------------------------------------------//
				
				$this->session->set_flashdata('event_feedback', __('The form has successfully been deleted'));
				_updateTimeStamp($form->eventid);
				redirect('event/view/'.$form->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('forms/event/'.$form->eventid.'/'.$form->launcherid);
			}
		}
		else {
			$form = $this->forms_model->getFormByID($id);
			
			$app = $this->app_model->get($form->appid);
			if($app == FALSE) redirect('apps');
			
            $this->language_model->removeTranslations('form', $id);
			if($this->general_model->remove('form', $id)){
				
				//Remove Tags....
				$this->load->model('tag_model');
				
				$tag = $this->tag_model->getTagIdbyFormID($id);
				if(!empty($tag->id)) {
					$this->general_model->remove('tag', $tag->id);
				}
				
				//Remove launcher
				$this->general_model->remove('launcher', $form->launcherid);
				
				// Count Venue Launchers of this ModuleType...
				$applaunchers = $this->app_model->countLauncherFromAppModule($app->id, $moduletypeid);
				if(!$applaunchers){
					
					$totalmodule = $this->module_mdl->countModuleFromAppModule($app->id, $moduletypeid);
					if($totalmodule){
						//Add to Module
						$this->module_mdl->removeModuleFromApp($app->id, $moduletypeid);
					}
					
				}
				
				//-------------------------------------------//
				
				$this->session->set_flashdata('event_feedback', __('The form has successfully been deleted'));
				_updateTimeStamp($app->id);
				redirect('apps/view/'.$app->id);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('forms/app/'.$app->id.'/'.$form->launcherid);
			}
		}
	}
		
	function sort($type = '') {
		if($type == '' || $type == FALSE) redirect('events');
		
		switch($type){
			case "form":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);
					
					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('form', $data);
					$this->db->last_query();
				}
				break;			
			default:
				break;
		}
	}
        
    function export($formId){
		$formSubmissions = array();
		$this->db->select('id');
		$this->db->where('formid', $formId);
		$res = $this->db->get('formsubmission');
		if( $res->num_rows()>0 )
			$formSubmissions = $res->result();
		
		$formscreens = $this->forms_model->getFormScreensByFormID($formId);
		$form = $this->forms_model->getFormByID($formId);

		$launcher = $this->module_mdl->getLauncherById($form->launcherid);
		$ffIds 		= array();
		$ffHeadings = array();
		$ffValues 	= array();
		
		foreach($formscreens as $formscreen){
			$this->db->select('id, formfieldtypeid, label');
			$this->db->where('formfieldtypeid !=', 10);
			$this->db->where('formscreenid', $formscreen->id);
			$res = $this->db->get('formfield');
			if( $res->num_rows() > 0 )
				$rows = $res->result();
				foreach($rows as $row){
					$ffIds[$row->id] = 	$row->id;
					$ffHeadings[$row->id] = $row->label;
				}
		}
		if($form->getgeolocation == 1){
        	$ffIds[] = -1;
			$ffHeadings['-1'] = 'Geo Location';
		}
		
		$inClz = implode(',', $ffIds);
		$tmp = array();
		$prv = '';
		
		if(count( $formSubmissions > 0 ) && count( $ffIds ) > 0 )
		foreach($formSubmissions as $formsubmissionid){
			$tmp = array();
			foreach( $ffIds as $id ){
				$tmp[$ffHeadings[$id]] = '';
				$qr 	= 'SELECT * FROM formsubmissionfield WHERE formfieldid IN ( ' . $inClz . ' ) AND formsubmissionid = ' . $formsubmissionid->id;
				$res 	= $this->db->query($qr);
				$rows 	= $res->result();
				foreach( $rows as $row ){
					if( $id == $row->formfieldid ){
						$tmp[$ffHeadings[$id]] = $row->value;
					}
				}
			}
			array_push($ffValues, $tmp);
		}
		
		array_unshift($ffValues, $ffHeadings);

		if($launcher->moduletypeid == 60) {
			//RATING MODULE
			foreach($ffValues as &$value) {
				if(isset($value['rateobject']) && isset($value['rateobjectid'])) {
					$objecttable = $value['rateobject'];
					$objectid = (int)$value['rateobjectid'];
					if($objecttable == 'session' || $objecttable == 'speaker') {
						$object = $this->db->query("SELECT * FROM $objecttable WHERE id = $objectid LIMIT 1");
						if($object->num_rows() > 0) {
							$object = $object->row();

							if(isset($object->name)) {
								if(!in_array('Name of rated item', $ffHeadings)) {
									$ffHeadings[] = 'Name of rated item';
								}
								
								$value['name'] = $object->name;
							} elseif(isset($object->title)) {
								if(!in_array('title', $ffHeadings)) {
									$ffHeadings[] = 'title';
								}
								$value['title'] = $object->title;
							}	

							if($objecttable == 'session') {
								// JOIN SS... OR NOT :)
								$speakers = $this->db->query("SELECT s.name FROM speaker s INNER JOIN speaker_session ss ON ss.speakerid = s.id WHERE ss.sessionid = $objectid");
								if($speakers->num_rows() > 0) {
									$speakers = $speakers->result();
									$speakersArr = array();
									foreach($speakers as $s) {
										$speakersArr[] = $s->name; 
									}
									$value['speakers'] = implode(',', $speakersArr);
									if(!in_array('speakers', $ffHeadings)) {
										$ffHeadings[] = 'speakers';
									}
								}
							}	
						}
					}

					unset($value['rateobject']);
					unset($value['rateobjectid']);
				} 
			}

			array_shift($ffValues);

			foreach($ffHeadings as $key => $heading) {
				if($heading == 'rateobject' || $heading == 'rateobjectid') {
					unset($ffHeadings[$key]);
				}
			}

			$filename = 'Form-' . $formId . '.xls';
			$this->load->library('excel');
			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setTitle('Form');
			$rowCounter = 1;
			$fieldletter = 'A';
			foreach($ffHeadings as $row) {
				$this->excel->getActiveSheet()->setCellValue($fieldletter . $rowCounter, $row);
				$fieldletter++;		
			}
			$rowCounter++;
			foreach($ffValues as $row){
				$fieldletter = 'A'; // Excel header counter
				foreach($row as $key=>$rowvalue ){
					$this->excel->getActiveSheet()->setCellValue($fieldletter . $rowCounter, $rowvalue);
					$fieldletter++;
				}
				$rowCounter++;
			}
		} else {
			$filename = 'Form-' . $formId . '.xls';
			$this->load->library('excel');
			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setTitle('Form');
			$rowCounter = 1;
			foreach($ffValues as $row){
				$fieldletter = 'A'; // Excel header counter
				foreach( $row as $key=>$value ){
					$value = ($value == '0') ? '' : $value;
					$this->excel->getActiveSheet()->setCellValue($fieldletter . $rowCounter, $value);
					$fieldletter++;		
				}
				$rowCounter++;
			}
		}
		
		header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
		
    }

    function results($formid) {
    	$formSubmissions = array();
		$this->db->select('id');
		$this->db->where('formid', $formid);
		$res = $this->db->get('formsubmission');
		if( $res->num_rows()>0 ) $formSubmissions = $res->result();
		
		$formscreens = $this->forms_model->getFormScreensByFormID($formid);
		$form = $this->forms_model->getFormByID($formid);
		
		if($form->eventid != 0) {
    		$event = $this->event_model->getById($form->eventid);
    		$app = _actionAllowed($event, 'event', 'returnApp');
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $form->title => 'formscreens/form/'.$form->id, __('Results') => $this->uri->uri_string());
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$id, $form->title => 'formscreens/form/'.$form->id, __('Results') => $this->uri->uri_string());
			}
    		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
    	} elseif($form->venueid != 0) {
    		$venue = $this->venue_model->getById($form->venueid);
    		$app = _actionAllowed($venue, 'venue', 'returnApp');
    		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $form->title => 'formscreens/form/'.$form->id, __('Results') => $this->uri->uri_string()));
    		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
    	}
		$launcher = $this->module_mdl->getLauncherById($form->launcherid);
		$ffIds 		= array();
		$ffHeadings = array();
		$ffValues 	= array();
		
		foreach($formscreens as $formscreen){
			$this->db->select('id, formfieldtypeid, label');
			$this->db->where('formfieldtypeid !=', 10);
			$this->db->where('formscreenid', $formscreen->id);
			$res = $this->db->get('formfield');
			if( $res->num_rows() > 0 )
				$rows = $res->result();
				foreach($rows as $row){
					$ffIds[$row->id] = 	$row->id;
					$ffHeadings[$row->id] = $row->label;
				}
		}
		if($form->getgeolocation == 1){
        	$ffIds[] = -1;
			$ffHeadings['-1'] = 'Geo Location';
		}
		
		$inClz = implode(',', $ffIds);
		$tmp = array();
		$prv = '';
		
		if(count( $formSubmissions > 0 ) && count( $ffIds ) > 0 )
		foreach($formSubmissions as $formsubmissionid){
			$tmp = array();
			foreach( $ffIds as $id ){
				$tmp[$ffHeadings[$id]] = '';
				$qr 	= 'SELECT * FROM formsubmissionfield WHERE formfieldid IN ( ' . $inClz . ' ) AND formsubmissionid = ' . $formsubmissionid->id;
				$res 	= $this->db->query($qr);
				$rows 	= $res->result();
				foreach( $rows as $row ){
					if( $id == $row->formfieldid ){
						$tmp[$ffHeadings[$id]] = $row->value;
					}
				}
			}
			array_push($ffValues, $tmp);
		}
		
		//array_unshift($ffValues, $ffHeadings);
		
		$cdata['content'] 		= $this->load->view('c_forms_results', array('results' => $ffValues, 'headings' => $ffHeadings, 'form' => $form), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
    }
	
	function url_check($url){
		if(filter_var($url, FILTER_VALIDATE_URL) === FALSE){
			$this->form_validation->set_message('url_check', '%s is not a valid url!');
			return false;
		}
		return true;
	}
	
	function email_check($emails){
		if( strpos($emails, ',') === false ){
			if(filter_var($emails, FILTER_VALIDATE_EMAIL) === FALSE){
				$this->form_validation->set_message('email_check', '%s is not a valid email!');
				return false;
			}
		}else{
			$emails = explode(',', $emails);
			foreach( $emails as $email)
				if(filter_var(trim($email), FILTER_VALIDATE_EMAIL) === FALSE){
					$this->form_validation->set_message('email_check', '%s is not a valid email!');
					return false;
				}
			
			return true;
		}
		
		return true;
	}

}