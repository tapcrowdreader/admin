<?php
// namespace Tapcrowd;

if(!defined('BASEPATH')) exit(__('No direct script access'));

require_once dirname(__DIR__) . '/models/bread_model.php';
session_start();

/**
 * TC_Controller
 * @author Tom Van de Putte
 */
class TC_Controller extends CI_Controller
{
	protected $_struct_class;
	protected $_struct_table;
	protected $_struct_model;
	protected $_struct_controller;
	protected $_module_settings;

	private $_pdo;

	public function __construct( $struct_class, $struct_table )
	{
		parent::__construct();

		# Validate struct
		if(!class_exists($struct_class)) {
			throw new \Tapcrowd\UnknownStructException($struct_class);
		} else {
			$this->_struct_class = $struct_class;
			$this->_struct_table = $struct_table;
		}

		# Create PDO Connection
		$pdo = new \PDO('mysql:dbname='.$this->db->database.';host='.$this->db->hostname, $this->db->username, $this->db->password);
		if(!$pdo instanceOf \PDO) {
			throw new \Exception('Unable to create PDO object for ' . __CLASS__);
		}
		$this->_pdo =& $pdo;

		# Load model
		$this->_struct_model = new \Tapcrowd\Bread_model($this->_struct_table, $pdo, $this->_struct_class);
	}

	/**
	 * Try to get the current launcher/module
	 *
	 * @return stdclass Launcher object
	 */
	protected function _getLauncher()
	{
		# Parse URI
		$uri = explode('/',$_SERVER['PATH_INFO']);
		if(empty($uri) || count($uri) < 2) {
			throw new \RuntimeException(__('Unsupported request (%s)', $_SERVER['PATH_INFO']));
		}

		# Get parent data
		array_shift($uri);
		$controller = array_shift($uri);
		$method = array_shift($uri);
		$parentId = (int)array_shift($uri);

		# Build sql
		if($method == 'venue' || $method == 'event') {
			$sql = "SELECT * FROM launcher WHERE {$method}id=$parentId AND module='".__CLASS__."'";
		} else {
			throw new \RuntimeException(__('Unsupported parent type (%s) in %s', $method, $controller));
		}

		# Fetch launcher
		$stat = $this->_pdo->query($sql);
		$data = $stat->fetchAll(\PDO::FETCH_OBJ);
		if(empty($data) || empty($data[0])) {
			throw new \RuntimeException(__('No launcher for parent (%s #%d)', $parentType, $parentId));
		}
		else {
			return $data[0];
		}
	}

	/**
	 * Validate and return parent object
	 *
	 * @param string $parentType The parent type
	 * @param string $parentId The parent identifier
	 * @return mixed the parent object
	 */
	protected function _getParentObject( $parentType, $parentId )
	{
		# Parent !== event/venue
		if($parentType != 'venue' || $parentType != 'event') {

			# Get venue/event id from database
			try {
				$sql = 'SELECT venueid, eventid FROM %s WHERE id=%d LIMIT 1';
				$table = filter_var($parentType, FILTER_SANITIZE_STRING);
				$stat = $this->_pdo->query(sprintf($sql, $table, (int)$parentId));
				$data = $stat->fetchAll(\PDO::FETCH_ASSOC);
			} catch(\Exception $e) {
				throw new \RuntimeException(__('Unknown parent type (%s)', $parentType));
			}

			if(empty($data) || empty($data[0])) {
				throw new \RuntimeException(__('Unknown parent type (%s)', $parentType));
			}

			# Get parent
			if($data[0]['venueid'] != 0) {;
				$parent = $this->venue_model->getbyid( $data[0]['venueid'] );
				_actionAllowed($parent,'venue');
			} else {
				$parent = $this->event_model->getbyid( $data[0]['eventid'] );
				_actionAllowed($parent,'event');
			}
		} else {
			$parent_model = $parentType.'_model';
			if(!isset($this->$parent_model) || method_exists('getbyid', $this->$parent_model)) {
				throw new RuntimeException(__('Unknown parent type (%s)', $parentType));
			}
			$parent = $this->$parent_model->getbyid($parentId);
			_actionAllowed($parent,$parentType);
		}

		return $parent;
	}

	/**
	 * Validate and return current Struct
	 *
	 * @param int $structId The struct identifier
	 * @return \Tapcrowd\Struct
	 */
	protected function _getStruct( $structId )
	{
		$struct_class = $this->_struct_class;
		$struct = new $struct_class( (int)$structId );
		if($this->_struct_model->read($struct) === false) {
			$_SESSION['error'] = __('%s #%d not found', $struct_class, $structId);
			return false;
		}
		return $struct;
	}

	/**
	 * Shortcut for venue apps (calls browse method)
	 * @param int $venueId The venue identifier
	 */
	public function venue( $venueId )
	{
		if(!is_numeric($venueId)) {
			exit;
		}
		$this->browse( $this->appid, 'venue', $venueId );
	}

	/**
	 * Shortcut for event apps (calls browse method)
	 * @param int $eventId The event identifier
	 */
	public function event( $eventId )
	{
		if(!is_numeric($eventId)) {
			exit;
		}
		$this->browse( $this->appid, 'event', $eventId );
	}

	/**
	 * Lists structures from database
	 *
	 * @param string $parentType The parentType
	 * @param int $parentId The parent identifier
	 * @param int $groupId Optional group identifier
	 * @param string $tags Optional comma separated lists of tags
	 * @param string $query Optional search query
	 * @param int $offset Optional offset in the resultset, defaults to 0 (zero)
	 * @param int $limit Optional max. number of results, defaults to 500
	 */
	public function browse( $appId, $parentType, $parentId, $groupId = null, $tags = null, $query = null, $offset = 0, $limit = 500 )
	{
		$parent = $this->_getParentObject( $parentType, $parentId );

		# Set mobilesite url
// 		$this->iframeurl = 'places/'.$parentType.'/'.$parentId;
// 		$listview = $this->_module_settings->views['list'];
// 		$this->_module_settings->add_url = 'places/add/'.$parentType.'/'.$parentId;

		# Get places
		$query = new \Tapcrowd\Query($appId, $parentType, $parentId, $groupId, $tags, $query, $offset, $limit);
		$structs = $this->_struct_model->browse($query);

		# Get and set output
		$cdata = array(
			'content' => $this->load->view(
				$this->_module_settings->views['list'],
				array('settings' => $this->_module_settings, 'data' => $structs),
				true),
			'sidebar' => $this->load->view('c_sidebar', array($parentType => $parent), true),
			'crumb' => array($this->_module_settings->plural => __CLASS__.'/'.$parentType.'/'.$parentId),
		);
		$this->load->view('master', $cdata);
	}

	/**
	 * Display structure in display or read mode
	 */
	public function read( $structId )
	{
		$struct = $this->_getStruct( $structId );
		if($struct === false) {
			$content = __('Struct %d not found', $structId);
		} else {
			# Validate parent
			$parent = $this->_getParentObject( $struct->parentType, $struct->parentId );
			$this->iframeurl = __CLASS__.'/'.$struct->parentType.'/'.$struct->parentId;

			# Get view
			$view = filter_input(INPUT_GET, 'view');
			if(!isset($this->_module_settings->views[$view])) $view = 'read';
			$view = $this->_module_settings->views[$view];

			$content = $this->load->view($view, array('settings' => $this->_module_settings, 'data' => $struct), true);
		}

		$cdata = array(
			'content' => $content,
			'sidebar' => $this->load->view('c_sidebar', array($parentType => $parent), true),
			'crumb' => array($this->_module_settings->plural => __CLASS__.'/'.$parentType.'/'.$parentId),
		);
		$this->load->view('master', $cdata);
	}

	/**
	 * Edit page
	 */
	public function edit( $structId )
	{
		# Get $struct and parent object
		$struct = $this->_getStruct( $structId );
		if($struct === false) { redirect('/'); }
		$parent = $this->_getParentObject( $struct->parentType, $struct->parentId );

		# Parse input
		$prefix = strtolower(get_class($struct)) . '_';
		$params = array_diff_key(get_object_vars($struct), get_class_vars('\Tapcrowd\Struct'));

		foreach($params as $param => $value) {
			$input = filter_input(INPUT_POST, $prefix.$param);
			if($input !== null) $struct->$param = $input;
		}

		# Validate/Set status
		$status = filter_input(INPUT_POST, $prefix.'status');
		if(isset($status)) {
			if(!in_array($status, $this->_module_settings->statuses)) {
				$error = __('Invalid status code "%s"', $status);
			} else {
				$struct->status = $status;
			}
		}

		# Resolve
		if(isset($error)) {
			$_SESSION['error'] = $error;
		} else {
			try {
				$this->_struct_model->edit($struct);
				$_SESSION['message'] = __('Update successful');
			} catch(\Exception $e) {
				$_SESSION['error'] = $e->getMessage();
			}
		}
		redirect('places/'.$struct->parentType.'/'.$struct->parentId);
	}

	/**
	 * Add page, redirects to edit page on success
	 *
	 * @param string $parentType The parent type
	 * @param int $parentId The parent identifier
	 */
	public function add( $parentType, $parentId )
	{
		$parent = $this->_getParentObject( $parentType, $parentId );

		# Create default object
		$struct = new $this->_struct_class;
		$struct->appId = (int)$this->appid;
		$struct->parentType = $parentType;
		$struct->parentId = (int)$parentId;
		$struct->status = 'created';

		# Resolve
		$this->_struct_model->add($struct);

		$href = $this->_module_settings->actions['edit']->href;
		redirect(str_replace('--id--', $struct->id, $href));
	}

	/**
	 * Delete request (redirects to browse)
	 */
	public function delete( $structId )
	{
		$struct = $this->_getStruct( $structId );
		if($struct === false) { redirect('/'); }
		$parent = $this->_getParentObject( $struct->parentType, $struct->parentId );

		$singular =& $this->_module_settings->singular;
		if(!$this->_struct_model->delete($struct)) {
			$_SESSION['error'] = __('Could not delete %s #%d', $singular, $struct->id);
		} else {
			$_SESSION['message'] = __('%s #%d successfully deleted', $singular, $struct->id);
		}
		redirect('places/'.$struct->parentType.'/'.$struct->parentId);
	}
}

