<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Social extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Social() {
		parent::__construct();
	}
	
	function index() {
		$this->load->view('app/social/master');
	}
	
	function action($key = '', $type = '', $facebookid = '', $appid = '', $eventid = '') {
		if($key == '' || $type == '' || $facebookid == '' || $appid == '') {
			$this->error();
		} else {
			$secret = md5('tcadm' . $facebookid . $type);
			if($key == $secret) {
				// GET ACTION AND SCORE FOR APPID
				$action_data = $this->db->query("SELECT * FROM socialscore_types WHERE action = '$type' AND appid = $appid LIMIT 1");
				if($action_data->num_rows() == 0) $this->error(__("Unknown action"));
				$action_data = $action_data->row();
				$action_total = $action_data->score;
				
				// INSERT NEW ACTION
				$this->db->insert("socialscore", array(
					'facebookid' => $facebookid,
					'appid' => $appid,
					'eventid' => $eventid,
					'action' => $action_data->id,
					'score' => $action_total,
					'creation' => date("Y-m-d H:i:s", time())
				));
				
				$userscore_app = $this->db->query("SELECT SUM(score) as total FROM socialscore WHERE appid = $appid AND facebookid = '$facebookid'" . ($eventid != '' ? " AND eventid = ".$eventid : ""))->row()->total; 
				$userscore_tot = $this->db->query("SELECT SUM(score) as total FROM socialscore WHERE facebookid = '$facebookid'")->row()->total; 
				
				// TOP 3
				$top3 = array();
				$topscores = $this->db->query("SELECT facebookid, SUM(score) as total FROM socialscore GROUP BY facebookid ORDER BY total DESC LIMIT 3")->result(); 
				
				$ch = curl_init();
				foreach ($topscores as $topscore) {
					curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/'.$topscore->facebookid);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_POST, 0);
					$data = curl_exec($ch);
					$json = json_decode($data);
					$top3[] = array('name' => $json->name, 'score' => $topscore->total);
				}
				curl_close($ch);
				
				$cdata['content'] = $this->load->view('app/social/new_score', array("top3" => $top3, "action_data" => $action_data, "action_total" => $action_total, "userscore_app" => $userscore_app, "userscore_tot" => $userscore_tot), TRUE);
				$this->load->view('app/social/master', $cdata);
			} else {
				$this->error(__("Invalid authentication"));
			}
			
		}
	}
	
	function ranking($key = '', $facebookid = '', $appid = '') {
		if($key == '') { 
			$this->error(); 
		} else {
			$secret = md5('tcadm' . $facebookid . $appid);
			if($facebookid == '' && $appid == '') $secret = 'ok';
			if($key == $secret || $secret = 'ok') {
				
				$leaderboard = array();
				$topscores = $this->db->query("SELECT facebookid, SUM(score) as total FROM socialscore GROUP BY facebookid ORDER BY total DESC")->result(); 
				
				// SAVE TOP 20 and personal score
				$teller = 1;
				$leaders = array();
				foreach ($topscores as $score) {
					if ($teller < 21 && $score->facebookid != $facebookid) {
						$score->user = FALSE;
						$score->rank = $teller;
						$leaders[] = $score;
					}
					
					if ($score->facebookid == $facebookid) {
						$score->user = TRUE;
						$score->rank = $teller;
						$leaders[] = $score;
					}
					$teller++;
				}
				
				$ch = curl_init();
				
				foreach ($leaders as $topscore) {
					curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/'.$topscore->facebookid);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_POST, 0);
					$data = curl_exec($ch);
					$json = json_decode($data);
					$leaderboard[] = array('name' => $json->name, 'score' => $topscore->total, 'rank' => $topscore->rank, "user" => $topscore->user);
				}
				curl_close($ch);
				
				$cdata['content'] = $this->load->view('app/social/ranking', array("leaderboard" => $leaderboard), TRUE);
				$this->load->view('app/social/master', $cdata);
			} else {
				$this->error(__("Invalid authentication"));
			}
		}
	}
	
	function fbranking($appid = '', $facebookid = '') {
		define("APP_ID", "219927378041648");
		define("APP_SECRET", "a33aaa52b45a846e6a4f723ba1eab6da");
		define("REDIRECT", site_url("app/social/authentication/"));
		define("APP_IFRAME", "http://www.facebook.com/pages/Festiviteiten/220485121316059?sk=app_219927378041648");
		
		if(!isset($_REQUEST["signed_request"])) {
			$this->error("Only accessible on Facebook!");
		} else {
			$signed_request = $_REQUEST["signed_request"];
			
			$fbdata = $this->parse_signed_request($signed_request, APP_SECRET);
			$auth_url = "http://www.facebook.com/dialog/oauth?client_id=" . APP_ID . "&redirect_uri=" . urlencode(REDIRECT);
			
			if($this->session->userdata("user_denied")) {
				if (empty($fbdata["user_id"])) {
					echo "... <script>function authorize() { top.location.href='" . $auth_url . "'; }</script><a href='#' onClick='authorize()'>".__("Show my actions")."</a>";
				}
			} else {
				if (empty($fbdata["user_id"])) {
					echo("<script> top.location.href='" . $auth_url . "'</script>");
					die;
				}
			}
			
			// print_r($fbdata);
			$facebookid = (array_key_exists('user_id', $fbdata) ? $fbdata['user_id'] : '');
			
			$leaderboard = array();
			$topscores = $this->db->query("SELECT facebookid, SUM(score) as total FROM socialscore GROUP BY facebookid ORDER BY total DESC")->result(); 
			
			$userscore_app = $this->db->query("SELECT SUM(score) as total FROM socialscore WHERE facebookid = '$facebookid'")->row()->total; 
			$userscore_tot = $this->db->query("SELECT SUM(score) as total FROM socialscore WHERE facebookid = '$facebookid'")->row()->total; 
			
			$latest_actions = $this->db->query("SELECT ss.facebookid, st.description, ss.score FROM socialscore ss LEFT JOIN socialscore_types st ON (ss.action = st.id) WHERE ss.facebookid = '$facebookid' ORDER BY ss.creation DESC LIMIT 3");
			if($latest_actions->num_rows() == 0) {
				$latest_actions = FALSE;
			} else {
				$latest_actions = $latest_actions->result();
			}
			
			$personal_ranking = "";
			// SAVE TOP 20 and personal score
			$teller = 1;
			$leaders = array();
			foreach ($topscores as $score) {
				if ($teller < 221 && $score->facebookid != $facebookid) {
					$score->user = FALSE;
					$score->rank = $teller;
					$leaders[] = $score;
				}
				
				if ($score->facebookid == $facebookid) {
					$score->user = TRUE;
					$score->rank = $teller;
					$leaders[] = $score;
					$personal_ranking = $teller;
				}
				$teller++;
			}
			
			$ch = curl_init();
			
			foreach ($leaders as $topscore) {
				curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/'.$topscore->facebookid."?access_token=". $this->config->item('FACEBOOK_TOKEN'));
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_POST, 0);
				$data = curl_exec($ch);
				$json = json_decode($data);
				if($json != null && isset($topscore) && $topscore != null && $topscore != false) {
					$leaderboard[] = array('name' => $json->name, 'score' => $topscore->total, 'rank' => $topscore->rank, "user" => $topscore->user);
				}
			}
			curl_close($ch);
			
			$cdata['content'] = $this->load->view('app/social/fbranking', array("personal_ranking" => $personal_ranking, "fbdata" => $fbdata, "latest_actions" => $latest_actions, "leaderboard" => $leaderboard, "userscore_app" => $userscore_app, "userscore_tot" => $userscore_tot), TRUE);
			$this->load->view('app/social/master', $cdata);
		}
	}
	
	function authentication() {
		$query = explode("&", $_SERVER['QUERY_STRING']);
		$qstring = array();
		foreach ($query as $item) {
			$split = explode("=", $item);
			$qstring[$split[0]] = $split[1];
		}
		
		if(isset($qstring['error_reason']) && $qstring['error_reason'] == "user_denied") {
			$this->session->set_userdata("user_denied", 1);
		}
		
		header("Location: http://www.facebook.com/pages/Festiviteiten/220485121316059?sk=app_219927378041648");
	}
	
	function parse_signed_request($signed_request, $secret) {
		list($encoded_sig, $payload) = explode('.', $signed_request, 2); 
		
		// decode the data
		$sig = $this->base64_url_decode($encoded_sig);
		$data = json_decode($this->base64_url_decode($payload), true);
		
		if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
			error_log(__('Unknown algorithm. Expected HMAC-SHA256'));
			return null;
		}
		
		// check sig
		$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
		if ($sig !== $expected_sig) {
			error_log(__('Bad Signed JSON signature!'));
			return null;
		}
		
		return $data;
	}

	function base64_url_decode($input) {
		return base64_decode(strtr($input, '-_', '+/'));
	}
	
	function error($error = '404 Page Not Found') {
		$this->load->view('app/social/error', array('error' => $error));
	}
	 
	function dev_fbranking($appid = '', $facebookid = '') {
		define("APP_ID", "205040276204944");
		define("APP_SECRET", "8667261ef69d2e116a86cb5130460c86");
		define("REDIRECT", site_url("app/social/dev_authentication/"));
		define("APP_IFRAME", "http://www.facebook.com/pages/Floatleft/139833556052038?sk=app_205040276204944");
		
		// *** BATCH CALL *** //
		$app_id = APP_ID;
		$app_secret = APP_SECRET; 
		$my_url = REDIRECT;
		
		$code = $_REQUEST["code"];
		
		if(empty($code)) {
			$dialog_url = "http://www.facebook.com/dialog/oauth?client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url);
			echo("<script> top.location.href='" . $dialog_url . "'</script>");
		}
		
		$token_url = "https://graph.facebook.com/oauth/access_token?client_id="
			. $app_id . "&redirect_uri=" . urlencode($my_url) 
			. "&client_secret=" . $app_secret 
			. "&code=" . $code;
		
		$access_token = file_get_contents($token_url);
		$batched_request = '[{"method":"GET","relative_url":"me"},' . '{"method":"GET","relative_url":"me/friends?limit=50"}]';
		
		$post_url = "https://graph.facebook.com/" . "?batch=" . $batched_request . "&" . $access_token . "&method=post";
		echo $post_url;
		
		$post = file_get_contents($post_url);
		echo '<p>'.__('Response: ').'<pre>' . $post . '</pre></p>';
		// *** //
		
		
		/*
		if(!isset($_REQUEST["signed_request"])) {
			$this->error("Only accessible on Facebook!");
		} else {
			$signed_request = $_REQUEST["signed_request"];
			
			$fbdata = $this->parse_signed_request($signed_request, APP_SECRET);
			$auth_url = "http://www.facebook.com/dialog/oauth?client_id=" . APP_ID . "&redirect_uri=" . urlencode(REDIRECT);
			
			if($this->session->userdata("user_denied")) {
				if (empty($fbdata["user_id"])) {
					echo "... <script>function authorize() { top.location.href='" . $auth_url . "'; }</script><a href='#' onClick='authorize()'>Show my actions</a>";
				}
			} else {
				if (empty($fbdata["user_id"])) {
					echo("<script> top.location.href='" . $auth_url . "'</script>");
					die;
				}
			}
			
			// print_r($fbdata);
			$facebookid = (array_key_exists('user_id', $fbdata) ? $fbdata['user_id'] : '');
			
			$leaderboard = array();
			$topscores = $this->db->query("SELECT facebookid, SUM(score) as total FROM socialscore GROUP BY facebookid ORDER BY total DESC")->result(); 
			
			$userscore_app = $this->db->query("SELECT SUM(score) as total FROM socialscore WHERE facebookid = '$facebookid'")->row()->total; 
			$userscore_tot = $this->db->query("SELECT SUM(score) as total FROM socialscore WHERE facebookid = '$facebookid'")->row()->total; 
			
			$latest_actions = $this->db->query("SELECT ss.facebookid, st.description, ss.score FROM socialscore ss LEFT JOIN socialscore_types st ON (ss.action = st.id) WHERE ss.facebookid = '$facebookid' ORDER BY ss.creation DESC LIMIT 3");
			if($latest_actions->num_rows() == 0) {
				$latest_actions = FALSE;
			} else {
				$latest_actions = $latest_actions->result();
			}
			
			$personal_ranking = "";
			// SAVE TOP 20 and personal score
			$teller = 1;
			$leaders = array();
			foreach ($topscores as $score) {
				if ($teller < 221 && $score->facebookid != $facebookid) {
					$score->user = FALSE;
					$score->rank = $teller;
					$leaders[] = $score;
				}
				
				if ($score->facebookid == $facebookid) {
					$score->user = TRUE;
					$score->rank = $teller;
					$leaders[] = $score;
					$personal_ranking = $teller;
				}
				$teller++;
			}
			
			$ch = curl_init();
			
			foreach ($leaders as $topscore) {
				curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/'.$topscore->facebookid);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_POST, 0);
				$data = curl_exec($ch);
				$json = json_decode($data);
				$leaderboard[] = array('name' => $json->name, 'score' => $topscore->total, 'rank' => $topscore->rank, "user" => $topscore->user);
			}
			curl_close($ch);
			
			$cdata['content'] = $this->load->view('app/social/fbranking', array("personal_ranking" => $personal_ranking, "fbdata" => $fbdata, "latest_actions" => $latest_actions, "leaderboard" => $leaderboard, "userscore_app" => $userscore_app, "userscore_tot" => $userscore_tot), TRUE);
			$this->load->view('app/social/master', $cdata);
		}
		*/
	}
	
	
	function dev_authentication() {
		$query = explode("&", $_SERVER['QUERY_STRING']);
		$qstring = array();
		foreach ($query as $item) {
			$split = explode("=", $item);
			$qstring[$split[0]] = $split[1];
		}
		
		if($qstring['error_reason'] == "user_denied") {
			$this->session->set_userdata("user_denied", 1);
		}
		
		header("Location: http://www.facebook.com/pages/Festiviteiten/220485121316059?sk=app_219927378041648");
	}
	
	
}