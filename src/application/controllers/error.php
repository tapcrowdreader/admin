<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Error extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}
	
	function index() {
		$cdata['content'] 		= $this->load->view('c_error', array(), TRUE);
		$cdata['crumb']			= array();
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}

}