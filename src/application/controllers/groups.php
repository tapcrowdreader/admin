<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Groups extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('module_mdl');
		$this->load->model('group_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Group',
			'plural' => 'Groups',
			'parentType' => 'event',
			'browse_url' => 'groups/view/--currentid--/--parentType--/--parentId--/--object--',
			'add_url' => 'groups/add/--parentId--/--parentType--/--currentid--/--object--/--basegroupid--',
			'edit_url' => 'groups/edit/--id--/--parentType--/--parentId--/--currentid--/--object--',
			'view_url' => 'groups/view/--id--/--parentType--/--parentId--/--object--/--basegroupid--',
			'item_view_url' => '--object--/edit/--itemid--/--parentType--/--object--/--currentid--/--basegroupid--',
			'module_url' => 'module/editLauncher/--launcherid--',
			'headers' => array(
				__('Title') => 'title'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'groups/edit/--id--/--parentType--/--parentId--/--currentid--/--object--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--/--object--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'groups/delete/--id--/--parentType--/--currentid--/--object--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'itemactions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => '--object--/edit/--itemid--/--parentType--/--object--/--currentid--/--basegroupid--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--itemid--/--object--/--parentType--/--currentid--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'groupitems/delete/--id--/--parentType--/--parentId--/--object--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger btn-groupitem-delete',
				),
			),
			'views' => array(
				'list' => 'tc_groupview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false,
			'deletecheckedurl' => 'groups/removemany/--parentId--/--parentType--'
		);
	}
	function add($id, $type = 'event', $parentid = 0, $object = '', $basegroupid = '') {
		$error = '';
		$this->load->library('form_validation');
		$languages = array();
		if(!empty($basegroupid)) {
			$basegroup = $this->group_model->getById($basegroupid);
		}
		
		if($type == 'event') {
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('name_'.$language->key, 'name_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('displaytype', 'displaytype', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {

					if($parentid != 0) {
						$parent = $this->group_model->getById($parentid);
						$tree = $parent->tree . '/' . $parentid;
					} else {
						$tree = 0;
					}


					//insert group
					$order = 0;
					$orderpost = $this->input->post('order');
					if(!empty($orderpost) && is_numeric($orderpost)) {
						$order = $this->input->post('order');
					}
					$data = array(
						'appid'		=> $app->id,
						'eventid'	=> $id,
						'name'		=> $this->input->post('name_'.  $app->defaultlanguage),
						'parentid'	=> $parentid,
						'tree'		=> $tree,
						'order'		=> $order,
						'displaytype' => $this->input->post('displaytype')
					);

					// if($parent != null) {
					// 	$data['displaytype'] = $parent->displaytype;
					// }

					$groupid = $this->general_model->insert('group', $data);

	                foreach($languages as $language) {
	                    $this->language_model->addTranslation('group', $groupid, 'name', $language->key, $this->input->post('name_'.$language->key));
	                }

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/groupimages/".$groupid)){
						mkdir($this->config->item('imagespath') . "upload/groupimages/".$groupid, 0755,true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/groupimages/'.$groupid;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('imageurl')) {
						$image = ""; //No image uploaded!
						if($_FILES['imageurl']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/groupimages/'. $groupid . '/' . $returndata['file_name'];
					}


					//insert launcher if parentid = 0
					if($parentid == 0) {
						$image2 = 'l_group';
						if($image != '') {
							$image2 = $image;
						}
						$launcherdata = array(
							'eventid'	=> $id,
							'moduletypeid'	=> 33,
							'module'	=> 'groups',
							'title'		=> $this->input->post('name_'.  $app->defaultlanguage),
							'icon'		=> $image2,
							'active'	=> 1,
							'groupid'	=> $groupid
							);
						$this->general_model->insert('launcher',$launcherdata);
					}

					_updateTimeStamp($id);
					if($error == '') {
						$this->general_model->update('group', $groupid, array('imageurl' => $image));
						if($parentid != 0) {
							redirect('groups/view/'.$parentid.'/event/'.$id.'/'.$object);
						} else {
							redirect('groups/view/'.$groupid.'/event/'.$id.'/'.$object);
						}
						
					} else {
						//image error
						redirect('groups/edit/'.$groupid.'/'.$object.'?error=image');
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_groups_add', array('event' => $event, 'error' => $error, 'languages'=>$languages, 'basegroupid' => $basegroupid), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__('Events') => 'events', $event->name => 'event/view/'.$event->id, __('Group') => 'groups/add/'.$id.'/event/'.$object);
			} else {
				$cdata['crumb']			= array($event->name => 'event/view/'.$event->id, __('Group') => 'groups/add/'.$id.'/event/'.$object);
			}
			
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$this->load->view('master', $cdata);
		} elseif($type == 'venue') {
			$venue = $this->venue_model->getbyid($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($app->apptypeid == 8 || $app->apptypeid == 7) {
				$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($parentid, $venue->id);
				if(isset($launcher) && $launcher->groupid != 0) {
					$this->iframeurl = 'catalogs/resto/'.$venue->id.'/'.$launcher->groupid;
				}
			}

			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('name_'.$language->key, 'name_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('displaytype', 'displaytype', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {

					//insert group
					$data = array(
						'appid'		=> $app->id,
						'venueid'	=> $id,
						'name'		=> $this->input->post('name_'.  $app->defaultlanguage),
						'parentid'	=> $parentid,
						'order'		=> $this->input->post('order'),
						'displaytype' => $this->input->post('displaytype')
					);

					if($parentid != 0) {
						$parent = $this->group_model->getById($parentid);
						$tree = $parent->tree . '/' . $parentid;
					} else {
						$tree = 0;
					}

					// if($parent != null) {
					// 	$data['displaytype'] = $parent->displaytype;
					// }

					$groupid = $this->general_model->insert('group', $data);

	                foreach($languages as $language) {
	                    $this->language_model->addTranslation('group', $groupid, 'name', $language->key, $this->input->post('name_'.$language->key));
	                }

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/groupimages/".$groupid)){
						mkdir($this->config->item('imagespath') . "upload/groupimages/".$groupid, 0755,true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/groupimages/'.$groupid;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('imageurl')) {
						$image = ""; //No image uploaded!
						if($_FILES['imageurl']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/groupimages/'. $groupid . '/' . $returndata['file_name'];
					}


					//insert launcher if parentid = 0
					if($parentid == 0) {
						$image2 = 'l_group';
						if($image != '') {
							$image2 = $image;
						}
						$launcherdata = array(
							'venueid'	=> $id,
							'moduletypeid'	=> 33,
							'module'	=> 'groups',
							'title'		=> $this->input->post('name_'.  $app->defaultlanguage),
							'icon'		=> $image2,
							'active'	=> 1,
							'groupid'	=> $groupid
							);
						$this->general_model->insert('launcher',$launcherdata);
					}

					_updateVenueTimeStamp($id);
					if($error == '') {
						$this->general_model->update('group', $groupid, array('imageurl' => $image));
						if($parentid != 0) {
							redirect('groups/view/'.$parentid.'/venue/'.$id.'/'.$object);
						} else {
							redirect('groups/view/'.$groupid.'/venue/'.$id.'/'.$object);
						}
					} else {
						//image error
						redirect('groups/edit/'.$groupid.'/'.$object.'?error=image');
					}
				}
			}

			$parents = $this->group_model->getParents($parentid);
			$group = $this->group_model->getById($parentid);
			$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($basegroupid, $venue->id);
			$cdata['content'] 		= $this->load->view('c_groups_add', array('venue' => $venue, 'error' => $error, 'languages'=>$languages, 'v' => $basegroupid), TRUE);
			if($parents) {
				$crumbarray = array($venue->name => 'venue/view/'.$venue->id);
				$i = 0;
				foreach($parents as $p) {
					if($i == 0) {
						$crumbarray[$launcher->title] = 'groups/view/'.$basegroup->id.'/venue/'.$venue->id.'/'.$object;
					} elseif($i > 0) {
						$crumbarray[$p->name] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/'.$object;
					}
					$i++;
				}

				$crumbarray[$group->name] = 'groups/view/'.$group->id.'/venue/'.$venue->id.'/'.$object;

				$cdata['crumb']			= checkBreadcrumbsVenue($app, $crumbarray);
			} else {
				$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => 'venue/view/'.$venue->id));
			}

			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, strtolower($launcher->module));
			$this->load->view('master', $cdata);
		}
	}

	function edit($id, $type, $typeid, $parentid = 0, $object = '', $basegroupid = '') {
		$error = '';
		$this->load->library('form_validation');
		$languages = array();
		$group = $this->group_model->getById($id);
		if($type == 'event') {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('name_'.$language->key, 'name_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('displaytype', 'displaytype', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {
					if($parentid != 0) {
						$parent = $this->group_model->getById($parentid);
						$tree = $parent->tree . '/' . $parentid;
					} else {
						$tree = 0;
					}

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/groupimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/groupimages/".$id, 0755, true);
					} 

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/groupimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('imageurl')) {
						$image = $group->imageurl; //No image uploaded!
						if($_FILES['imageurl']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/groupimages/'. $id . '/' . $returndata['file_name'];
					}


					//update group
					$data = array(
						'name'		=> $this->input->post('name_'.  $app->defaultlanguage),
						'imageurl'	=> $image,
						'order'		=> $this->input->post('order'),
						'displaytype' => $this->input->post('displaytype')
					);

					$groupid = $this->general_model->update('group', $id, $data);

	                foreach($languages as $language) {
	                    $this->language_model->addTranslation('group', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
	                }


					_updateTimeStamp($typeid);
					if($error == '') {
						redirect('groups/view/'.$group->id.'/event/'.$typeid.'/'.$object.'/'.$basegroupid);
					} else {
						//image error
						redirect('groups/edit/'.$group->id.'/'.$object.'?error=image');
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_groups_edit', array('event' => $event, 'error' => $error, 'languages'=>$languages, 'group'=>$group, 'type' => $type, 'object' => $object, 'basegroupid' => $basegroupid), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__('Events') => 'events', $event->name => 'event/view/'.$event->id, $group->name => 'groups/edit/'.$id.'/event/'.$typeid.'/'.$parentid.'/'.$object);
			} else {
				$cdata['crumb']			= array($event->name => 'event/view/'.$event->id, $group->name => 'groups/edit/'.$id.'/event/'.$typeid.'/'.$parentid.'/'.$object);
			}
			$cdata['crumb']			= array(__('Events') => 'events', $event->name => 'event/view/'.$event->id, $group->name => 'groups/edit/'.$id.'/event/'.$typeid.'/'.$parentid.'/'.$object);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$this->load->view('master', $cdata);
		} elseif($type == 'venue') {
			$venue = $this->venue_model->getById($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($app->apptypeid == 8) {
				$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($id, $venue->id);
				$this->iframeurl = 'catalogs/resto/'.$venue->id.'/'.$id;
			}

			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('name_'.$language->key, 'name_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('displaytype', 'displaytype', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {
					if($parentid != 0) {
						$parent = $this->group_model->getById($parentid);
						$tree = $parent->tree . '/' . $parentid;
					} else {
						$tree = 0;
					}

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/groupimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/groupimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/groupimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('imageurl')) {
						$image = $group->imageurl; //No image uploaded!
						if($_FILES['imageurl']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/groupimages/'. $id . '/' . $returndata['file_name'];
					}

					//update group
					$data = array(
						'name'		=> $this->input->post('name_'.  $app->defaultlanguage),
						'imageurl'	=> $image,
						'order'		=> $this->input->post('order'),
						'displaytype' => $this->input->post('displaytype')
					);

					$groupid = $this->general_model->update('group', $id, $data);

	                foreach($languages as $language) {
	                    $this->language_model->addTranslation('group', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
	                }

					_updateTimeStamp($typeid);
					if($error == '') {
						redirect('groups/view/'.$group->parentid.'/venue/'.$typeid.'/'.$object);
					} else {
						//image error
						redirect('groups/edit/'.$group->id.'/'.$object.'?error=image');
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_groups_edit', array('venue' => $venue, 'error' => $error, 'languages'=>$languages, 'group'=>$group, 'type' => $type, 'object' => $object, 'basegroupid' => $basegroupid), TRUE);
			$parents = $this->group_model->getParents($id);
			if(isset($parents[1]) && (strtolower($parents[1]->name) == 'team' || strtolower($parents[1]->name) == 'menu' || strtolower($parents[1]->name) == 'gallery')) {
				array_shift($parents);
			}
			$crumbarray = array($venue->name => 'venue/view/'.$venue->id);
			$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($parents[0]->id, $venue->id);
			$i = 0;
			foreach($parents as $p) {
				if($i == 0) {
					$crumbarray[$launcher->title] = 'groups/view/'.$basegroup->id.'/venue/'.$venue->id.'/'.$object;
				} elseif($i > 0) {
					$crumbarray[$p->name] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/'.$object;
				}
				$i++;
			}

			$crumbarray[$group->name] = 'groups/view/'.$group->id.'/venue/'.$venue->id.'/'.$object;

			$cdata['crumb']			= checkBreadcrumbsVenue($app, $crumbarray);
			// $cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => 'venue/view/'.$venue->id, $group->name => 'groups/edit/'.$id.'/venue/'.$typeid.'/'.$parentid.'/'.$object));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'groups');
			$this->load->view('master', $cdata);
		}
	}

	function image_rules($str){
		$filename = 'imageurl';
		return image_check($filename, $_FILES);
	}

	function view($id, $type = 'event', $typeid, $object = '', $basegroupid2 = '') {
		if(substr($object, strlen($object) -1) != 's') {
			$objectPlural = $object.'s';
		} else {
			$objectPlural = $object;
		}
		
		$error = '';
		if($type == 'event') {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'groups/view/'.$id.'/'.$type.'/'.$typeid.'/'.$object;

			$group = $this->group_model->getById($id);
			$children = $this->group_model->getChildren($id);
			$parents = $this->group_model->getParents($id);
			$groupitems = $this->group_model->getItemsOfGroup($id);
			if($groupitems != null) {
				foreach($groupitems as $item) {
					$children[] = $item;
				}
			}

			foreach($children as $child) {
				if((!isset($child->title) || $child->title == null) && $child->name != null) {
					$child->title = $child->name;
				}
				if(!isset($child->groupitem) || $child->groupitem == null) {
					$child->groupitem = false;
				}
			}

			$children = $this->group_model->sortChildren($children);

			$headers = array(__('Title') => 'title');

			$i = 0;
			if($object != '') {
				if($app->familyid != 1) {
					$crumbarray			= array(__('Events') => 'events', $event->name => 'event/view/'.$event->id, ucfirst($objectPlural) => $objectPlural. '/event/'.$typeid);
				} else {
					$crumbarray		= array($event->name => 'event/view/'.$event->id, ucfirst($objectPlural) => $objectPlural. '/event/'.$typeid);
				}
			} else {
				if($app->familyid != 1) {
					$crumbarray			= array(__('Events') => 'events', $event->name => 'event/view/'.$event->id, __('Groups') => 'groups/view/'.$id.'/'.$typeid.'/event');
				} else {
					$crumbarray		= array($event->name => 'event/view/'.$event->id, __('Groups') => 'groups/view/'.$id.'/'.$typeid.'/event');
				}
			}
			foreach($parents as $parent) {
				if($parent->name == "exhibitorcategories" || $parent->name == 'catalogcategories' || $parent->name == 'placescategories' || $parent->name == 'attendeecategories') {
					$parent->name = __('Categories');
					if($i == 0) {
					$buttonname = __('Add Category');
					$catbrand = __('Category');
					$i++;
					} 
				}
				if(isset($object)) {
					$crumbarray[$parent->name] = 'groups/view/'.$parent->id.'/'.$type.'/'.$typeid.'/'.$object;
				}
			}
			if($group->name == "exhibitorbrands") {
				$group->name = __('Brands');
				$buttonname = __('Add Brand');
				$catbrand = __('Brand');
			} elseif($group->name == "exhibitorcategories"|| $group->name == 'catalogcategories' || $group->name == 'placescategories' || $group->name == 'attendeecategories') {
				$group->name = __('Categories');
				$buttonname = __('Add Category');
				$catbrand = __('Category');
			} 
			if(isset($object)) {
				$crumbarray[$group->name] = 'groups/view/'.$group->id.'/'.$type.'/'.$typeid.'/'.$object;
			} else {
				$crumbarray[$group->name] = 'groups/view/'.$group->id.'/'.$type.'/'.$typeid;
			}
			$lastparent = $group->id;
			if(isset($parents[1])) {
				$basegroupid = $parents[1]->id;
			} else {
				$basegroupid = $group->id;
			}

			if($object == 'places'&& isset($parents[0])) {
				$basegroupid = $parents[0]->id;
			}

			if(!empty($basegroupid2)) {
				$basegroupid = $basegroupid2;
			}

			$launcher = $this->module_mdl->getLauncherOfGroupAndEvent($basegroupid, $event->id);

			$this->_module_settings->parentType = 'event';
			# Module edit url
			$module_url =& $this->_module_settings->module_url;
			$module_url = str_replace(array('--parentType--','--parentId--', '--launcherid--'), array('event', $event->id, $launcher->id), $module_url);
			if(!$launcher) {
				$module_url = 'module/editbycontroller/'.$object.'/event/'.$event->id;
			}

			if(!$launcher && $object == 'attendees') {
				$launcher = $this->module_mdl->getLauncher(14, 'event', $event->id);
			}
			# Module add url
			$add_url =& $this->_module_settings->add_url;
			$add_url = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('event', $event->id, $id, $objectPlural, $basegroupid), $add_url);
			# Edit href
			$edit_href =& $this->_module_settings->actions['edit']->href;
			$edit_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('event', $event->id, $id, $objectPlural, $basegroupid), $edit_href);
			# View href
			$view_href =& $this->_module_settings->view_url;
			$view_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('event', $event->id, $id, $objectPlural, $basegroupid), $view_href);

			if($object == 'places') {
				$this->_module_settings->item_view_url = 'places/read/--itemid--/--currentid--/--basegroupid--?view=edit';
			}
			$item_view_href =& $this->_module_settings->item_view_url;
			$item_view_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('event', $event->id, $id, $objectPlural, $basegroupid), $item_view_href);
			# Duplicate href
			$duplicate_href =& $this->_module_settings->actions['duplicate']->href;
			$duplicate_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('event', $event->id, $id, $objectPlural, $basegroupid), $duplicate_href);
			# Delete href
			$delete_href =& $this->_module_settings->actions['delete']->href;
			$delete_href = str_replace(array('--parentType--', '--currentid--', '--object--'), array('event', $id, $objectPlural), $delete_href);
			# delete many
			$this->_module_settings->checkboxes = true;
			$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
			$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $event->id), $deletecheckedurl);

			# Edit href
			if($object == 'places') {
				$this->_module_settings->itemactions['edit']->href = 'places/read/--itemid--/--currentid--/--basegroupid--?view=edit';
			}
			$item_edit_href =& $this->_module_settings->itemactions['edit']->href;
			$item_edit_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('event', $event->id, $id, $objectPlural, $basegroupid), $item_edit_href);
			# Duplicate href
			$item_duplicate_href =& $this->_module_settings->itemactions['duplicate']->href;
			$item_duplicate_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('event', $event->id, $id, $object, $basegroupid), $item_duplicate_href);
			# Delete href
			$item_delete_href =& $this->_module_settings->itemactions['delete']->href;
			$item_delete_href = str_replace(array('--parentType--', '--currentid--', '--object--', '--parentId--'), array('event', $id, $object, $event->id), $item_delete_href);

			$btnItems = (object)array(
							'title' => __('Add Item'),
							'href' => $objectPlural.'/add/'.$event->id.'/event/'.$object.'/'.$id.'/'.$basegroupid,
							'icon_class' => 'icon-plus-sign',
							'btn_class' => 'add btn primary',
							);
			$this->_module_settings->extrabuttons['items'] = $btnItems;

			$listview = $this->_module_settings->views['list'];
			$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $children, 'title' => $group->name), true);
			// $cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'type'=>'event', 'typeid'=>$typeid, 'parents' => $parents, 'group' => $group, 'data' => $children, 'headers' => $headers, 'object' => $object), TRUE);
			$cdata['crumb'] = $crumbarray;
			$menucontroller = 'groups';

			if(strtolower($group->name) == __('Categories') || (isset($parents[1]) && strtolower($parents[1]->name == 'exhibitorcategories')) || strtolower($group->name) == __('Brands') || (isset($parents[1]) && strtolower($parents[1]->name == 'exhibitorbrands'))) {
				$menucontroller = 'exhibitors';
			} elseif($launcher->module == 'attendees') {
				$menucontroller = 'attendees';
			}

			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, $menucontroller);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$this->load->view('master', $cdata);
		} elseif($type == 'venue') {
			if($object == 'catalogs') {
				$object = 'catalog';
			}
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'groups/view/'.$id.'/'.$type.'/'.$typeid.'/'.$object;

			$crumbarray = array($venue->name => 'venue/view/'.$venue->id);

			$group = $this->group_model->getById($id);
			if($app->apptypeid == 8 || $app->apptypeid == 7) {
				// $launcher = $this->module_mdl->getLauncherOfGroupAndVenue($id, $venue->id);
				$this->iframeurl = 'catalogs/resto/'.$venue->id.'/'.$group->id;
			}
			//echo '<pre>';
			$children = $this->group_model->getChildren($id); //SELECT * FROM `group` WHERE parentid = 15350
			$parents = $this->group_model->getParents($id);
			//echo $this->db->last_query();
			//print_r($parents);
			//exit;
			$groupitems = $this->group_model->getItemsOfGroup($id);
			if($groupitems != null) {
				$i = 0;
				foreach($groupitems as $item) {
					if($item->type == '') {
						$item->type = 'catalog';
					}

					if(!empty($object) && $object != $item->type) {
						unset($groupitems[$i]);
					} else {
						$children[] = $item;
					}
					$i++;
				}
			}

			//get the right launcher, try parent 0 and 1 or the current groupid
			if(isset($parents[0])) {
				$basegroupid = $parents[0]->id;
				$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($basegroupid, $venue->id);
			} 

			if(isset($parents[1]) && ($launcher == false || empty($launcher))) {
				$basegroupid = $parents[1]->id;
				$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($basegroupid, $venue->id);
			} 

			if($launcher == false || empty($launcher)) {
				$basegroupid = $group->id;
				$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($basegroupid, $venue->id);
			}

			$menucontroller = 'groups';

			if(strtolower($group->name) == 'menu' || (isset($parents[1]) && strtolower($parents[1]->name) == 'menu') || strtolower($launcher->module) == 'menu') {
				$menucontroller = 'menu';
				$launcher = $this->module_mdl->getLauncher(50, 'venue', $venue->id);
				$basegroupid = $launcher->groupid;
			} elseif(strtolower($group->name) == 'gallery' || strtolower($group->name) == 'pictures' || (isset($parents[1]) && strtolower($parents[1]->name) == 'gallery' || strtolower($parents[1]->name) == 'pictures') || strtolower($launcher->module) == 'gallery' || strtolower($launcher->module) == 'pictures') {
				$menucontroller = 'gallery';
				$launcher = $this->module_mdl->getLauncher(51, 'venue', $venue->id);
				$basegroupid = $launcher->groupid;
			} elseif(strtolower($group->name) == 'team' || (isset($parents[1]) && strtolower($parents[1]->name) == 'team') || strtolower($launcher->module) == 'team') {
				$menucontroller = 'team';
				$launcher = $this->module_mdl->getLauncher(52, 'venue', $venue->id);
				$basegroupid = $launcher->groupid;
			} elseif(strtolower($group->name) == 'placescategories' || (isset($parents[1]) && strtolower($parents[1]->name) == 'placescategories') || strtolower($launcher->module) == 'places') {
				$menucontroller = 'places';
				$launcher = $this->module_mdl->getLauncher(54, 'venue', $venue->id);
				$basegroupid = $launcher->groupid;
			} elseif(strtolower($group->name) == 'catalog' || (isset($parents[0]) && strtolower($parents[0]->name) == 'catalogcategories') || (isset($parents[1]) && strtolower($parents[1]->name) == 'catalog') || strtolower($launcher->module) == 'gallery' || strtolower($launcher->module) == 'catalog') {
				$menucontroller = 'catalog';
				$launcher = $this->module_mdl->getLauncher(15, 'venue', $venue->id);
				if(strtolower($parents[1]->name) == 'catalog') {
					$basegroupid = $parents[1]->id;
				} elseif(strtolower($parents[0]->name) == 'catalog') {
					$basegroupid = $parents[0]->id;
				}
			} elseif(strtolower($object) == 'catalog') {
				$menucontroller = 'catalog';
			}

			# Module import url
			if($launcher->moduletypeid != 51) {
				$this->_module_settings->import_url = 'import/catalog/'.$venue->id.'/'.$id;
			}

			# Module edit url
			$module_url =& $this->_module_settings->module_url;
			$module_url = str_replace(array('--parentType--','--parentId--', '--launcherid--'), array('venue', $venue->id, $launcher->id), $module_url);
			if(!$launcher) {
				$module_url = 'module/editbycontroller/'.strtolower($object).'/venue/'.$venue->id;
			}

			foreach($children as $child) {
				if((!isset($child->title) || $child->title == null) && $child->name != null) {
					$child->title = $child->name;
				}
				if(!isset($child->groupitem) || $child->groupitem == null) {
					$child->groupitem = false;
				}
			}

			
			if(isset($parents[1]) && ($parents[1]->name == 'catalogcategories' || $parents[1]->name == 'placescategories' || strtolower($parents[1]->name) == 'menu' || strtolower($parents[1]->name) == 'team' || strtolower($parents[1]->name) == 'gallery' || strtolower($parents[1]->name) == 'pictures') && $launcher) {
				$parents[1]->name = $launcher->title;
			}

			if(($group->name == 'catalogcategories' || $group->name == 'placescategories' || strtolower($group->name) == 'menu' || strtolower($group->name) == 'team' || strtolower($group->name) == 'gallery' || strtolower($p->name) == 'pictures') && $launcher) {
				$group->name = $launcher->title;
			}

			$i = 0;

			if($object != '' && strtolower($object) != 'catalog') {
				$crumbarray = array($venue->name => 'venue/view/'.$venue->id, __(ucfirst($object)) => strtolower($object).'/venue/'.$venue->id);
			} else {
				$crumbarray = array($venue->name => 'venue/view/'.$venue->id);
			}

			if($launcher) {
				$crumbarray[$launcher->title] = $launcher->module.'/venue/'.$launcher->venueid;
			}

			foreach($parents as $parent) {
				if($parent->name == "exhibitorcategories" || $parent->name == 'catalogcategories' || $parent->name == 'placescategories') {
					$parent->name = __('Categories');
					if($i == 0) {
					$buttonname = __('Add Category');
					$catbrand = __('Category');
					$i++;
					} 
				}
				if(isset($object)) {
					if($parent->name !=  __('Brands') && $parent->name !=  __('Categories')) {
						$crumbarray[$parent->name] = 'groups/view/'.$parent->id.'/'.$type.'/'.$typeid.'/'.strtolower($object);
					} else {
						// $crumbarray[$parent->name] = 'groups/view/'.$parent->id.'/'.$type.'/'.$typeid;
					}
				}
			}

			if(isset($object)) {
				if(!empty($parents)) {
					$crumbarray[$group->name] = 'groups/view/'.$group->id.'/'.$type.'/'.$typeid.'/'.strtolower($object);
				}
			} else {
				$crumbarray[$group->name] = 'groups/view/'.$group->id.'/'.$type.'/'.$typeid;
			}

			$lastparent = $group->id;

			if(!empty($basegroupid2)) {
				$basegroupid = $basegroupid2;
			}

			$this->_module_settings->parentType = 'venue';
			# Module add url
			$add_url =& $this->_module_settings->add_url;
			$add_url = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('venue', $venue->id, $id, strtolower($object), $basegroupid), $add_url);
			# Edit href
			$edit_href =& $this->_module_settings->actions['edit']->href;
			$edit_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('venue', $venue->id, $id, strtolower($object), $basegroupid), $edit_href);
			# View href
			$view_href =& $this->_module_settings->view_url;
			$view_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('venue', $venue->id, $id, strtolower($object), $basegroupid), $view_href);
			if($object == 'places') {
				$this->_module_settings->item_view_url = 'places/read/--itemid--/--currentid--/--basegroupid--?view=edit';
			}
			$item_view_href =& $this->_module_settings->item_view_url;
			$item_view_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('venue', $venue->id, $id, strtolower($object), $basegroupid), $item_view_href);
			# Duplicate href
			$duplicate_href =& $this->_module_settings->actions['duplicate']->href;
			$duplicate_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('venue', $venue->id, $id, strtolower($object), $basegroupid), $duplicate_href);
			# Delete href
			$delete_href =& $this->_module_settings->actions['delete']->href;
			$delete_href = str_replace(array('--parentType--', '--currentid--', '--object--'), array('venue', $id, strtolower($object)), $delete_href);
			# delete many
			$this->_module_settings->checkboxes = true;
			$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
			$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('venue', $venue->id), $deletecheckedurl);

			if(strtolower($object) == 'catalog') {
				$this->_module_settings->premium = 'catalog';
				$this->_module_settings->parentId = $venue->id;
			}
			# Edit href
			if($object == 'places') {
				$this->_module_settings->itemactions['edit']->href = 'places/read/--itemid--/--currentid--/--basegroupid--?view=edit';
			}
			$item_edit_href =& $this->_module_settings->itemactions['edit']->href;
			$item_edit_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('venue', $venue->id, $id, strtolower($object), $basegroupid), $item_edit_href);
			# Duplicate href
			$item_duplicate_href =& $this->_module_settings->itemactions['duplicate']->href;
			$item_duplicate_href = str_replace(array('--parentType--','--parentId--', '--currentid--', '--object--', '--basegroupid--'), array('venue', $venue->id, $id, strtolower($object), $basegroupid), $item_duplicate_href);
			# Delete href
			$item_delete_href =& $this->_module_settings->itemactions['delete']->href;
			$item_delete_href = str_replace(array('--parentType--', '--currentid--', '--object--', '--parentId--'), array('venue', $id, strtolower($object), $venue->id), $item_delete_href);

			$btnItems = (object)array(
							'title' => __('Add Item'),
							'href' => strtolower($object).'/add/'.$venue->id.'/venue/'.strtolower($object).'/'.$id.'/'.$basegroupid,
							'icon_class' => 'icon-plus-sign',
							'btn_class' => 'add btn primary',
							);
			$this->_module_settings->extrabuttons['items'] = $btnItems;

			$listview = $this->_module_settings->views['list'];
			$cdata['content'] = $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $children, 'title' => $group->name), true);

			foreach($crumbarray as $k => $v) {
				$crumbarray[$k] = strtolower($v);
			}

			if($object != '') {
				$cdata['crumb']	= checkBreadcrumbsVenue($app, $crumbarray);
			} else {
				$cdata['crumb']	= checkBreadcrumbsVenue($app, $crumbarray);
			}
			$cdata['sidebar'] = $this->load->view('c_sidebar', array(), TRUE);

			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, $menucontroller);
			$this->load->view('master', $cdata);

		}
	}

	function module($trigger= '', $typeid, $type, $launcherid) {
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$this->load->model('module_mdl');
			$launcher = $this->db->query("SELECT * FROM launcher WHERE id = $launcherid LIMIT 1")->row();
			switch($trigger){
				case 'activate':
					// Add module to venue
					$this->general_model->update('launcher', $launcher->id, array('active'=>1));
					$this->module_mdl->addModuleToVenue($typeid, $launcher->moduletypeid);
					break;
				case 'deactivate':
					// TODO Remove module from event
					$this->general_model->update('launcher', $launcher->id, array('active'=>0));
					break;
				default:
					break;
			}
			_updateVenueTimeStamp($typeid);
			redirect('venue/view/'.$typeid);
		}

	}

	function delete($id, $type, $returngroupid = '', $objecttype = '') {
		if($type == 'venue') {
			$group = $this->group_model->getById($id);
			$venue = $this->venue_model->getbyid($group->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
            $this->language_model->removeTranslations('group', $id);
			if($this->general_model->remove('group', $id)){
				$this->db->delete('groupitem', array('groupid' => $id));
				$this->db->delete('group', array('parentid' => $id));
				$this->session->set_flashdata('event_feedback', __('The group has successfully been deleted'));
				_updateVenueTimeStamp($group->venueid);
				if(!empty($returngroupid) && !empty($objecttype)) {
					redirect('groups/view/'.$returngroupid.'/venue/'.$group->venueid.'/'.$objecttype);
				}
				redirect('venue/view/'.$group->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('groups/view/'.$group->id.'/venue/'.$group->venueid);
			}
		} else {
			$group = $this->group_model->getById($id);
			$event = $this->event_model->getbyid($group->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
            $this->language_model->removeTranslations('group', $id);
			//remove exhibitors
			// $this->load->model('exhibitor_model');
			// $this->exhibitor_model->removeExhibitorsOfGroup($id);
			if($this->general_model->remove('group', $id)){
				$this->db->delete('groupitem', array('groupid' => $id));
				$this->db->delete('group', array('parentid' => $id));
				$this->session->set_flashdata('event_feedback', __('The group has successfully been deleted'));
				_updateTimeStamp($group->eventid);
				if(!empty($returngroupid) && !empty($objecttype)) {
					redirect('groups/view/'.$returngroupid.'/event/'.$group->eventid.'/'.$objecttype);
				}
				redirect('event/view/'.$group->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('groups/view/'.$group->id.'/event/'.$group->eventid);
			}
		}
	}

	function removeimage($id, $type = 'event', $object) {
		$item = $this->group_model->getById($id);
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($item->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->imageurl)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('group', $id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('groups/edit/'.$item->id.'/venue/'.$venue->id.'/'.$item->parentid.'/'.$object);
		} else {
			$event = $this->event_model->getbyid($item->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->imageurl)) redirect('events');
			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('group', $id, $data);

			_updateTimeStamp($event->id);
			redirect('groups/edit/'.$item->id.'/event/'.$event->id.'/'.$item->parentid.'/'.$object);
		}
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$groupitemids = array();
		$i = 0;
		foreach($selectedids as $id) {
			if(stristr($id, 'item_')) {
				unset($selectedids[$i]);
				$groupitemids[] = substr($id, 5);
				$i++;
			}
		}
		
		$this->general_model->removeMany($selectedids, '`group`');

		if(!empty($groupitemids)) {
			$groupitemids2 = implode(',', $groupitemids);
			$items = $this->db->query("SELECT * FROM groupitem WHERE id IN ($groupitemids2)")->result();
			foreach($items as $item) {
				$this->db->query("DELETE FROM $item->itemtable WHERE id = $item->itemid");
			}
			$this->general_model->removeMany($groupitemids, 'groupitem');
		}
    } 

}