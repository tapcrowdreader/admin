<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Dataimport extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('import_model');
	}
	
	//php 4 constructor
	function Dataimport() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('import_model');
	}
	
	function index() {
		$this->event();
	}
	
	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');
		
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		
		$error = '';
		
		$beurzen = $this->import_model->getXpoList();
		
		$cdata['content'] 		= $this->load->view('c_import', array('event' => $event, 'error' => $error, 'beurzen' => $beurzen), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Import") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function expotest($id) {
		$xml = $this->import_model->getXpoBeurs($id);
		echo '<pre>';
		print_r($xml);
		echo '</pre>';
	}

	function beurs2($eventid) {
		if($this->input->post('postback') == 'postback'){
		$appidquery = $this->db->query("SELECT * FROM appevent WHERE eventid = $eventid AND appid != 2 LIMIT 1");
		if($appidquery->num_rows() != 0) {
			$appid = $appidquery->row()->appid;

		$xml = $this->import_model->getXpoBeurs($this->input->post('sel_beurs'));
		// $xml = $this->import_model->getXpoBeurs(16);

		//change this to the main group for categories (should be created when creating event)
		$parentid = 0;
		$parentidquerycategories = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name='exhibitorcategories' LIMIT 1");
		$parentidquerybrands = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name='exhibitorbrands' LIMIT 1");

		if($parentidquerybrands->num_rows() != 0 && $parentidquerycategories->num_rows() != 0) {
			$launcherdata = array(
					'eventid'	=> $eventid,
					'moduletypeid'	=> 33,
					'module'	=> 'groups',
					'title'		=> 'Brands',
					'icon'		=> '',
					'groupid'	=> $parentidquerybrands->row()->id
				);
			$this->general_model->insert('launcher', $launcherdata);

			$launcherdata = array(
					'eventid'	=> $eventid,
					'moduletypeid'	=> 33,
					'module'	=> 'groups',
					'title'		=> 'Categories',
					'icon'		=> '',
					'groupid'	=> $parentidquerycategories->row()->id
				);
			$this->general_model->insert('launcher', $launcherdata);
		}


		// $eventid = 2727;
		// $appid = 2727;
		//categorien => groepen
		$categories = $xml->Productgroepen->Productgroep;
		$groups = array();

		foreach($categories as $category) {
			$i = 1;
			while(isset($category->{'ProductgroepNL'.$i}) && !empty($category->{'ProductgroepNL'.$i})) {
				$group->parentname = '';
				$group->naam = ''.$category->{'ProductgroepNL'.$i};


				$j = $i + 1;
				if(!isset($category->{'ProductgroepNL'.$j}) || empty($category->{'ProductgroepNL'.$j})) {
					//no children => save
					if($i > 1) {
						$n = $i - 1;
						$group->parentname = ''.$category->{'ProductgroepNL'.$n};
					} 


					if(isset($group->parentname) && $group->parentname != '') {
						$parentidquery = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name='$group->parentname' LIMIT 1");
						if($parentidquery->num_rows() != 0) {
							$parentid = $parentidquery->row()->id;
						} 
					} else {
						$parentid = $parentidquerycategories->row()->id;
					}


					$groupdata = array(
						'appid'		=> $appid,
						'eventid'	=> $eventid,
						'name'		=> $group->naam,
						'parentid'	=> $parentid
						);

					$groupid = $this->general_model->insert('group',$groupdata);
					$groups[''.$category->UniekID] = $groupid;
					// var_dump($group);
				}
				$i++;
			}
		}

		//merken => brands
		$brands = $xml->Merken->Merk;
		$brands_new = array();

		$brandparentid = 0;
		$parentidquerybrands = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name='exhibitorbrands' LIMIT 1");
		if($parentidquerybrands->num_rows() != 0) {
			$brandparentid = $parentidquerybrands->row()->id;
		} 
		
		foreach($brands as $brand) {
			//Add to database
			// $data = array(
			// 		'name' => ''.$brand->Merk,
			// 		'eventid' => $eventid
			// 	);
			
			// $brandid = $this->general_model->insert('exhibitorbrand', $data);



			$groupdata = array(
				'appid'		=> $appid,
				'eventid'	=> $eventid,
				'name'		=> ''.$brand->Merk,
				'parentid'	=> $brandparentid
				);

			$groupid = $this->general_model->insert('group',$groupdata);

			$brands_new[''.$brand->UniekID] = $groupid;
		}



		// ### Exhibitors ###
		$exhibitors = $xml->Exposanten->Exposant;


		
		foreach($exhibitors as $exhibitor) {
			// Add Exhibitor to database
			$data = array(
					'eventid' => $eventid,
					'name' => ''.$exhibitor->Naam,
					'booth' => ''.$exhibitor->Standnummer,
					'tel' => ''.$exhibitor->Telefoon,
					'web' => ''.$exhibitor->Website,
					'username' => ''.$exhibitor->Login,
					'password' => ''.$exhibitor->Paswoord
				);
			
			$new_id = $this->general_model->insert('exhibitor', $data);
			//
			// Set Categories for Exhibitor
			if(count($exhibitor->Productgroepen->Productgroep) > 1){
				$cats = $exhibitor->Productgroepen->Productgroep;
				foreach($cats as $category) {
					//insert groupitem
					$groupitemdata = array(
						'appid'	=> $appid,
						'eventid'	=> $eventid,
						'groupid'	=> $groups[''.$category],
						'itemtable'	=> 'exhibitor',
						'itemid'	=> $new_id
						);
					$categoryid = $this->general_model->insert('groupitem', $groupitemdata);
				}
			} else {
				if($exhibitor->Productgroepen->Productgroep != '') {
					//insert groupitem
					$groupitemdata = array(
						'appid'	=> $appid,
						'eventid'	=> $eventid,
						'groupid'	=> $groups[''.$exhibitor->Productgroepen->Productgroep],
						'itemtable'	=> 'exhibitor',
						'itemid'	=> $new_id
						);
					$categoryid = $this->general_model->insert('groupitem', $groupitemdata);
				}
			}
			
			//
			// Set Brands for Exhibitor
			if(count($exhibitor->Merken->Merk) > 1){
				$brandz = $exhibitor->Merken->Merk;
				foreach($brandz as $brand) {
					$groupitemdata = array(
						'appid'	=> $appid,
						'eventid'	=> $eventid,
						'groupid'	=> $brands_new[''.$brand],
						'itemtable'	=> 'exhibitor',
						'itemid'	=> $new_id
						);
					$categoryid = $this->general_model->insert('groupitem', $groupitemdata);
					// $data = array(
					// 		'exhibitorid' => $new_id,
					// 		'exhibitorbrandid' => $brands_new[''.$brand]
					// 	);
					// $brandid = $this->general_model->insert('exhibrand', $data);
				}
			} else {
				if($exhibitor->Merken->Merk != '') {
					$groupitemdata = array(
						'appid'	=> $appid,
						'eventid'	=> $eventid,
						'groupid'	=> $brands_new[''.$exhibitor->Merken->Merk],
						'itemtable'	=> 'exhibitor',
						'itemid'	=> $new_id
						);
					$categoryid = $this->general_model->insert('groupitem', $groupitemdata);
					// $data = array(
					// 		'exhibitorid' => $new_id,
					// 		'exhibitorbrandid' => $brands_new[''.$exhibitor->Merken->Merk]
					// 	);
					// $brandid = $this->general_model->insert('exhibrand', $data);
				}
			}
		}
		}
		}
	}
	
	function beurs($eventid) {
		if(stristr($_SERVER['HTTP_REFERER'], base_url()) != FALSE) {
			if($this->input->post('postback') == 'postback'){
				
				$xml = $this->import_model->getXpoBeurs($this->input->post('sel_beurs'));
				
				// ### Categories ###
				$categories = $xml->Productgroepen->Productgroep;
				$categories_new = array();
				
				foreach($categories as $category) {
					//Add to database
					$data = array(
							'name' => ''.$category->ProductgroepNL1,
							'eventid' => $eventid
						);
					
					$categoryid = $this->general_model->insert('exhibitorcategory', $data);
					$categories_new[''.$category->UniekID] = $categoryid;
				}
				
				// ### Brands ###
				$brands = $xml->Merken->Merk;
				$brands_new = array();
				
				foreach($brands as $brand) {
					//Add to database
					$data = array(
							'name' => ''.$brand->Merk,
							'eventid' => $eventid
						);
					
					$brandid = $this->general_model->insert('exhibitorbrand', $data);
					$brands_new[''.$brand->UniekID] = $brandid;
				}
				
				// ### Exhibitors ###
				$exhibitors = $xml->Exposanten->Exposant;
				
				foreach($exhibitors as $exhibitor) {
					// Add Exhibitor to database
					$data = array(
							'eventid' => $eventid,
							'name' => "$exhibitor->Naam",
							'booth' => "$exhibitor->Standnummer",
							'tel' => "$exhibitor->Telefoon",
							'web' => "$exhibitor->Website",
							'login' => "$exhibitor->Login",
							'password' => "$exhibitor->Paswoord"
						);
					
					$new_id = $this->general_model->insert('exhibitor', $data);
					//
					// Set Categories for Exhibitor
					if(count($exhibitor->Productgroepen->Productgroep) > 1){
						$cats = $exhibitor->Productgroepen->Productgroep;
						foreach($cats as $category) {
							$data = array(
									'exhibitorid' => $new_id,
									'exhibitorcategoryid' => $categories_new[''.$category]
								);
							$categoryid = $this->general_model->insert('exhicat', $data);
						}
					} else {
						if($exhibitor->Productgroepen->Productgroep != '') {
							$data = array(
									'exhibitorid' => $new_id,
									'exhibitorcategoryid' => $categories_new[''.$exhibitor->Productgroepen->Productgroep]
								);
							$categoryid = $this->general_model->insert('exhicat', $data);
						}
					}
					
					//
					// Set Brands for Exhibitor
					if(is_array($exhibitor->Merken->Merk)){
						$brandz = $exhibitor->Merken->Merk;
						foreach($brandz as $brand) {
							$data = array(
									'exhibitorid' => $new_id,
									'exhibitorbrandid' => $brands_new[''.$brand]
								);
							$brandid = $this->general_model->insert('exhibrand', $data);
						}
					} else {
						if($exhibitor->Merken->Merk != '') {
							$data = array(
									'exhibitorid' => $new_id,
									'exhibitorbrandid' => $brands_new[''.$exhibitor->Merken->Merk]
								);
							$brandid = $this->general_model->insert('exhibrand', $data);
						}
					}
				}
				echo TRUE;
			} else {
				echo FALSE;
			}
		} else {
			echo "False, intruder!";
		}
	}
    
    function ibbtxml() {
        libxml_use_internal_errors(true); 
        $res = $this->import_model->readIbbtXml('IEEE ICIP2.xml', 541, 4);
//        $res = $this->import_model->readIbbtXml('QoMEX.xml', 540, 4);
        $cdata['content'] 		= $this->load->view('c_dataimport_ibbtxml', TRUE);
        $this->load->view('master', $cdata);
    }
	
    function editibbt() {
//		$res = $this->db->query("SELECT * FROM session WHERE sessiongroupid = 714 AND `description` = ''");
//		if($res->num_rows() == 0) return FALSE;
//        
//        foreach($res->result() as $session) {
//            $time = substr($session->name,0,11);
//            $name = substr($session->name, 11);
//            $timeArray = explode('-',$time);
//            $starttime = date("Y-m-d H:i:s",strtotime($timeArray[0]));
//            $endtime = date("Y-m-d H:i:s",strtotime($timeArray[1]));
//            
//            $data = array(
//              "name"      => trim($name),
//              "starttime" => $starttime,
//              "endtime"   => $endtime
//            );
//            
//            $this->db->where('id', $session->id);
//            $this->db->update('session', $data);
//        }
//        
//        $cdata['content'] 		= $this->load->view('c_dataimport_editibbt', TRUE);
//        $this->load->view('master', $cdata);
    }
	/*
	function importt($eventid, $beursid) {
		
		$xml = $this->import_model->getXpoBeurs($beursid);
		print_r($xml);
		print "<br /><br /><br />";
		
		// ### Categories ###
		$categories = $xml->Productgroepen->Productgroep;
		$categories_new = array();
		
		print ' --- CATEGORIES ---<br />';
		foreach($categories as $category) {
			//Add to database
			$data = array(
					'name' => ''.$category->ProductgroepNL1,
					'eventid' => $eventid
				);
			
			$categoryid = $this->general_model->insert('exhibitorcategory', $data);
			print 'added ' . $category->ProductgroepNL1 . '<br />' . "\n";
			$categories_new[''.$category->UniekID] = $categoryid;
		}
		print ' --- --- ---<br />';
		print_r($categories_new);
		print '<br />--- END CATEGORIES ---<br /><br />';
		
		// ### Brands ###
		$brands = $xml->Merken->Merk;
		$brands_new = array();
		
		print " --- BRANDS ---<br />";
		foreach($brands as $brand) {
			//Add to database
			$data = array(
					'name' => ''.$brand->Merk,
					'eventid' => $eventid
				);
			
			$brandid = $this->general_model->insert('exhibitorbrand', $data);
			print 'added ' . $brand->Merk . '<br />' . "\n";
			$brands_new[''.$brand->UniekID] = $brandid;
		}
		print ' --- --- ---<br />';
		print_r($brands_new);
		print '<br />--- END BRANDS ---<br /><br />';
		
		// ### Exhibitors ###
		$exhibitors = $xml->Exposanten->Exposant;
		
		print " --- EXHIBITORS ---<br />";
		foreach($exhibitors as $exhibitor) {
			print "<br />\n";
			// Add Exhibitor to database
			$data = array(
					'eventid' => $eventid,
					'name' => "$exhibitor->Naam",
					'booth' => "$exhibitor->Standnummer",
					'tel' => "$exhibitor->Telefoon",
					'web' => "$exhibitor->Website"
				);
			
			$new_id = $this->general_model->insert('exhibitor', $data);
			print 'added ' . $exhibitor->Naam . '<br />' . "\n";
			//
			// Set Categories for Exhibitor
			//print_r($exhibitor->Productgroepen->Productgroep);
			if(count($exhibitor->Productgroepen->Productgroep) > 1){
				echo "Productgroep is array<br />\n";
				$cats = $exhibitor->Productgroepen->Productgroep;
				print_r($cats);
				foreach($cats as $category) {
					echo "new categoryid : " . $categories_new[''.$category] . "<br />\n";
					$data = array(
							'exhibitorid' => $new_id,
							'exhibitorcategoryid' => $categories_new[''.$category]
						);
					$categoryid = $this->general_model->insert('exhicat', $data);
					print 'added cat ' . $categories_new[''.$category] . '<br />' . "\n";
				}
			} else {
				if($exhibitor->Productgroepen->Productgroep != '') {
					echo "new categoryid : " . $categories_new[''.$exhibitor->Productgroepen->Productgroep] . "<br />\n";
					$data = array(
							'exhibitorid' => $new_id,
							'exhibitorcategoryid' => $categories_new[''.$exhibitor->Productgroepen->Productgroep]
						);
					$categoryid = $this->general_model->insert('exhicat', $data);
					print 'added category ' . $categories_new[''.$exhibitor->Productgroepen->Productgroep] . '<br />' . "\n";
				}
			}
			
			//
			// Set Brands for Exhibitor
			//print_r($exhibitor->Merken->Merk);
			if(is_array($exhibitor->Merken->Merk)){
				echo "Merken is array<br />\n";
				$brandz = $exhibitor->Merken->Merk;
				print_r($brandz);
				foreach($brandz as $brand) {
					echo "new brandid : " . $brands_new[''.$brand] . "<br />\n";
					$data = array(
							'exhibitorid' => $new_id,
							'exhibitorbrandid' => $brands_new[''.$brand]
						);
					$brandid = $this->general_model->insert('exhibrand', $data);
					print 'added brand ' . $brands_new[''.$brand] . '<br />' . "\n";
				}
			} else {
				if($exhibitor->Merken->Merk != '') {
					echo "new brandid : " . $brands_new[''.$exhibitor->Merken->Merk] . "<br />\n";
					$data = array(
							'exhibitorid' => $new_id,
							'exhibitorbrandid' => $brands_new[''.$exhibitor->Merken->Merk]
						);
					$brandid = $this->general_model->insert('exhibrand', $data);
					print 'added brand ' . $brands_new[''.$exhibitor->Merken->Merk] . '<br />' . "\n";
				}
			}
		}
		print '--- END EXHIBITORS ---<br /><br />';
		
	}
	*/
	
	public function joburg() {
// 		//load eventlist
// //		$this->load->model('event_model');
// //		$this->load->model('app_model');
// 		$link = mysql_connect('localhost', 'root', '');
// 		$dbname = 'tapcrowd';
// 		mysql_select_db($dbname);
		
// 		$page = 'http://api.oxynade.com/event/list?apikey=mobilejuice&format=json&channelid=142&pagelength=2000';

// 		$result = file_get_contents($page);
// 		$result = json_decode($result);
		
// 		if(strtolower($result->response->status) == 'ok') {
// 			$eventlist = $result->result;
// 		} else {
// 			echo "ERROR";
// 		}
		
// 		//foreach event get eventdetail from api
// 		foreach($eventlist as $event) {
// 			$eventpage = 'http://api.oxynade.com/event/detail?apikey=mobilejuice&eventid='.$event->eventid.'&format=json';
// 			$eventResult = file_get_contents($eventpage);
// 			$eventResult = json_decode($eventResult);
			
// 			if(strtolower($eventResult->response->status) == 'ok') {
// 				$eventDetails = $eventResult->result;
// 				$eventDetails = $eventDetails[0];
// 			} else {
// 				echo "ERROR";
// 			}
			
// 			//save event image to server
// 			$path = '';
// 			if($eventDetails->image != null && $eventDetails->image != '') {
// //				$path = $this->config->item('imagespath') .'upload/eventlogos/event'.$eventDetails->eventid.'.jpg';
// 				$path = 'upload/eventlogos/event'.$eventDetails->eventid.'.jpg';
// 				$file = file_get_contents($eventDetails->image);
// 				if($file) {
// 					$image = file_put_contents($path, $file);
// 				}
// 			}
			
// 			//replace the dates
// 			$datefrom = str_replace('Z','',$eventDetails->datefrom);
// 			$datefrom = str_replace('T',' ',$eventDetails->datefrom);
			
// 			$dateto = str_replace('Z','',$eventDetails->dateto);
// 			$dateto = str_replace('T',' ',$eventDetails->dateto);
			
			
// 			//build event data
// //			$data = array(
// //				'organizerid' => '568',
// //				'venueid' => '797',
// //				'eventtypeid' => '4',
// //				'name' => $eventDetails->title,
// //				'datefrom' => $datefrom,
// //				'dateto' => $dateto,
// //				'description' => $eventDetails->content,
// //				'eventlogo'	=> 'upload/eventlogos/event'.$eventDetails->eventid.'.jpg',
// //				'active'	=> '1'
// //			);
			
			
// 			//check if event exists
// //			$eventExists = $this->event_model->getbyNameAndDatefrom($eventDetails->title, $datefrom);
// 			$res = mysql_query("SELECT e.* FROM event e WHERE e.name = '$eventDetails->title' AND e.datefrom = '$datefrom' LIMIT 1");
// 			if(mysql_num_rows($res) == 0) {
// 				$eventExists = false;
// 			}
// 			$eventExists = mysql_fetch_row($res);
// 			$eventExists = $eventExists[0];
			
// 			$eventlogo = 'upload/eventlogos/event'.$eventDetails->eventid.'.jpg';
// 			if($eventExists) {
// 				//update
// //				$id = $this->general_model->update('event', $eventExists->id, $data);
// 				mysql_query("UPDATE event SET organizerid = 568, venueid = 797, eventtypeid = 4, name = '$eventDetails->title', datefrom = '$datefrom', dateto = '$dateto', description = '$eventDetails->content', eventlogo = '$eventlogo', active = 1 WHERE id = $eventExists");
// 				$id = $eventExists;
// 			} else {
// 				//insert
// //				$id = $this->general_model->insert('event', $data);
// 				$eventDetails->title = mysql_real_escape_string($eventDetails->title);
// 				$eventDetails->content = mysql_real_escape_string($eventDetails->content);
// 				$query = "INSERT INTO event (organizerid,venueid,eventtypeid,name,datefrom,dateto,active,eventlogo,description) VALUES(568,797,4,'$eventDetails->title','$datefrom','$dateto',1,'$eventlogo','$eventDetails->content')";
// 				mysql_query($query);
// 				$id = mysql_insert_id();
// //				$this->app_model->addEventToApp('314', $id);
// 				mysql_query("INSERT INTO appevent (appid,eventid) VALUES (314,$id)");
// 			}
			
// 			//schedule inserten of updaten
// 			$schedules = $eventDetails->dayinfo;
// 			foreach($schedules as $schedule) {
// 				$arraySchedule = explode(" ", $schedule);
// 				$day = $arraySchedule[0] . ' ' . $arraySchedule[1];
// //				$data = array(
// //					'eventid'	=> $id,
// //					'date'		=> $day,
// //					'caption'	=> $arraySchedule[3]
// //				);
				
				
// //				$existingSchedule = $this->db->query("SELECT * FROM schedule WHERE eventid = $id AND date = '$day' AND caption = '$arraySchedule[3]'");
// 				$existingSchedule = mysql_query("SELECT * FROM schedule WHERE eventid = $id AND date = '$day' AND caption = '$arraySchedule[3]'");
// 				if($existingSchedule != false && mysql_num_rows($existingSchedule) == 0) {
// //					$this->general_model->insert('schedule', $data);
// 					mysql_query("INSERT INTO schedule (eventid,date,caption) VALUES ($id,'$day','$arraySchedule[3]')");
// 				} else {
// //					$this->general_model->update('schedule', $existingSchedule->row()->id, $data);
// 					$existingSchedule = mysql_fetch_row($existingSchedule);
// //					$existingSchedule = $existingSchedule[0];
// 					mysql_query("UPDATE schedule SET eventid = $id, date = '$day', caption = '$arraySchedule[3]' WHERE id = $existingSchedule[0]");
// 				}
// 			}
			
// //			$moduleExists = $this->db->query("SELECT * FROM module WHERE event = $id AND moduletype = 12");
// 			$moduleExists = mysql_query("SELECT * FROM module WHERE event = $id AND moduletype = 12");
// 			if($moduleExists != false && mysql_num_rows($moduleExists) == 0) {
// //				$moduledata = array(
// //					'moduletype'	=> 12,
// //					'event'			=> $id
// //				);
// //				$this->general_model->insert('module', $moduledata);
// 				mysql_query("INSERT INTO module (moduletype,event) VALUES (12,$id)");
// 			}
			
// //			_updateTimeStamp($id);
// //			_updateVenueTimeStamp('797');
// 			$time = time();
// 			mysql_query("UPDATE event SET timestamp = $time WHERE id = $id");
// 			mysql_query("UPDATE venue SET timestamp = $time WHERE id = 797");
			
// 			echo 'succes';
// 		}
	}
	
	public function joburg_backup() {
		// //load eventlist
		// $this->load->model('event_model');
		// $this->load->model('app_model');
		// $page = 'http://api.oxynade.com/event/list?apikey=mobilejuice&format=json&channelid=142&pagelength=2000';

		// $result = file_get_contents($page);
		// $result = json_decode($result);
		
		// if(strtolower($result->response->status) == 'ok') {
		// 	$eventlist = $result->result;
		// } else {
		// 	echo "ERROR";
		// }
		
		// //foreach event get eventdetail from api
		// foreach($eventlist as $event) {
		// 	$eventpage = 'http://api.oxynade.com/event/detail?apikey=mobilejuice&eventid='.$event->eventid.'&format=json';
		// 	$eventResult = file_get_contents($eventpage);
		// 	$eventResult = json_decode($eventResult);
			
		// 	if(strtolower($eventResult->response->status) == 'ok') {
		// 		$eventDetails = $eventResult->result;
		// 		$eventDetails = $eventDetails[0];
		// 	} else {
		// 		echo "ERROR";
		// 	}
			
		// 	//save event image to server
		// 	$path = '';
		// 	if($eventDetails->image != null && $eventDetails->image != '') {
		// 		$path = $this->config->item('imagespath') .'upload/eventlogos/event'.$eventDetails->eventid.'.jpg';
		// 		$image = file_put_contents($path, file_get_contents($eventDetails->image));
		// 	}
			
		// 	//replace the dates
		// 	$datefrom = str_replace('Z','',$eventDetails->datefrom);
		// 	$datefrom = str_replace('T',' ',$eventDetails->datefrom);
			
		// 	$dateto = str_replace('Z','',$eventDetails->dateto);
		// 	$dateto = str_replace('T',' ',$eventDetails->dateto);
			
			
		// 	//build event data
		// 	$data = array(
		// 		'organizerid' => '568',
		// 		'venueid' => '797',
		// 		'eventtypeid' => '4',
		// 		'name' => $eventDetails->title,
		// 		'datefrom' => $datefrom,
		// 		'dateto' => $dateto,
		// 		'description' => $eventDetails->content,
		// 		'eventlogo'	=> 'upload/eventlogos/event'.$eventDetails->eventid.'.jpg',
		// 		'active'	=> '1'
		// 	);
			
			
		// 	//check if event exists
		// 	$eventExists = $this->event_model->getbyNameAndDatefrom($eventDetails->title, $datefrom);
			
		// 	if($eventExists) {
		// 		//update
		// 		$id = $this->general_model->update('event', $eventExists->id, $data);
		// 		$id = $eventExists->id;
				
		// 	} else {
		// 		//insert
		// 		$id = $this->general_model->insert('event', $data);
		// 		$this->app_model->addEventToApp('314', $id);
		// 	}
			
		// 	//schedule inserten of updaten
		// 	$schedules = $eventDetails->dayinfo;
		// 	foreach($schedules as $schedule) {
		// 		$arraySchedule = explode(" ", $schedule);
		// 		$day = $arraySchedule[0] . ' ' . $arraySchedule[1];
		// 		$data = array(
		// 			'eventid'	=> $id,
		// 			'date'		=> $day,
		// 			'caption'	=> $arraySchedule[3]
		// 		);
				
				
		// 		$existingSchedule = $this->db->query("SELECT * FROM schedule WHERE eventid = $id AND date = '$day' AND caption = '$arraySchedule[3]'");
		// 		if($existingSchedule->num_rows() == 0) {
		// 			$this->general_model->insert('schedule', $data);
		// 		} else {
		// 			$this->general_model->update('schedule', $existingSchedule->row()->id, $data);
		// 		}
		// 	}
			
		// 	$moduleExists = $this->db->query("SELECT * FROM module WHERE event = $id AND moduletype = 12");
		// 	if($moduleExists->num_rows() == 0) {
		// 		$moduledata = array(
		// 			'moduletype'	=> 12,
		// 			'event'			=> $id
		// 		);
		// 		$this->general_model->insert('module', $moduledata);
		// 	}
			
		// 	_updateTimeStamp($id);
		// 	_updateVenueTimeStamp('797');
		// 	echo 'succes';
		// }
	}
}