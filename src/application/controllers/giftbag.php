<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Giftbag extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Giftbag() {
		parent::__construct();
	}
	
	function index() {
		$this->sign();
	}
	
	function sign($phoneid = '', $eventid = '') {
		$this->load->library('form_validation');
		if ($phoneid != '' && $eventid != '') {
			// INIT CONFIG
			$error = '';
			$completioncode = 1234;
			
			$aantal = $this->db->query("SELECT * FROM giftbag WHERE phoneid = '$phoneid'")->num_rows();
			
			$complete = FALSE;
			if ($this->db->query("SELECT * FROM giftbag WHERE phoneid = '$phoneid' AND exhibitor = $completioncode LIMIT 1")->num_rows() == 1) {
				$complete = TRUE;
			}
			
			if ($this->input->post('postback') == 'postback') {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
				$this->form_validation->set_rules('code', 'code', 'trim|required');
				
				if($this->form_validation->run() == FALSE){
					$error = __("Please make sure all fields are filled in correctly.");
				} else {
					// CHECK IF CODE EXISTS
					$exhibitor = $this->db->query("SELECT id FROM exhibitor WHERE code = '" . set_value('code') . "' AND eventid = $eventid  LIMIT 1");
					if ($exhibitor->num_rows() > 0) {
						$checkindata = array(
							"name"		=> set_value('name'),
							"email"		=> set_value('email'),
							"phoneid"	=> $phoneid,
							"exhibitor"	=> $exhibitor->row()->id
						);
						
						if($this->db->insert('giftbag', $checkindata)){
							$this->session->set_flashdata('feedback_giftbag',__('Congratulations! You completed your first check-in!'));
							redirect("giftbag/sign/".$phoneid."/".$eventid."/");
						} else {
							$error = __("Validation failed, please try again?");
						}
					} else {
						$error = __("Unknown code, nice try!");
					}
				}
			}
			
			if ($this->input->post('checkin') == 'checkin') {
				$this->form_validation->set_rules('code', 'code', 'trim|required');
				
				if($this->form_validation->run() == FALSE){
					$error = "Code must be filled in.";
				} else {
					// CHECK IF CODE EXISTS
					$exhibitor = $this->db->query("SELECT id FROM exhibitor WHERE code = '" . set_value('code') . "' AND eventid = $eventid LIMIT 1");
					if ($exhibitor->num_rows() > 0) {
						$codes = $this->db->query("SELECT * FROM giftbag WHERE exhibitor = '" . $exhibitor->row()->id . "' AND phoneid = '$phoneid' LIMIT 1");
						if ($codes->num_rows() > 0) {
							$error = __("Too bad, you already checked in here before!");
						} else {
							$checkindata = array(
								"phoneid"	=> $phoneid,
								"exhibitor"	=> $exhibitor->row()->id
							);
							
							if($this->db->insert('giftbag', $checkindata)){
								$this->session->set_flashdata('feedback_giftbag',__('Check-in completed. You earned another point!'));
								redirect("giftbag/sign/".$phoneid."/".$eventid."/");
							} else {
								$error = __("Validation failed, please try again?");
							}
						}
					} else {
						$error = __("Unknown code, nice try!");
					}
				}
			}
			
			if ($this->input->post('collect') == 'collect') {
				$this->form_validation->set_rules('code', 'code', 'trim|required');
				
				if($this->form_validation->run() == FALSE){
					$error = "Invalid validation code.";
				} else {
					if (set_value('code') == $completioncode) {
						$checkindata = array(
							"phoneid"	=> $phoneid,
							"exhibitor"	=> $completioncode
						);
						
						if($this->db->insert('giftbag', $checkindata)){
							$this->session->set_flashdata('feedback_giftbag',__('Congratulations! You have succesfull collected your giftbag.'));
							redirect("giftbag/sign/".$phoneid."/".$eventid."/");
						} else {
							$error = __("Validation failed, please try again?");
						}
					} else {
						$error = __("Unknown code, nice try!");
					}
				}
			}
			
			$this->load->view('giftbag/master', array('aantal' => $aantal, 'error' => $error, 'complete' => $complete));
		} else {
			die(__("INVALID AUTHENTICATION"));
		}
	}
	
}