<?php if (!defined('BASEPATH')) exit(__('No direct script access'));

class Import extends CI_Controller {
	//php 5 constructor
	function __construct() {
    	parent::__construct();
		if (_authed()) {}
		$this->load->helper('email');
		$this->load->model('sessions_model');
		$this->load->model('group_model');
		$this->load->model('speaker_model');
    }

	function index() {
		//$this->event();
    }
    
	function sessions_speakers($eventid) {
    	$datafile = '';
		$view_data = '';
		$confirm = false;
		$inputvalid = true;
		$error = '';
		$add_social_media = false;
        
		$this->load->model('general_model');
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event', 'returnApp');
        
		if($app->apptypeid == 10){
			$template_file = 'import/download/sessions';
			$template_example = $this->config->item('publicupload') . 'excelfiles/examples/sessions.png';
			
			$supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");
	
			$add_social_media = true;
			
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			$totalLang = count($languages);
			
			$defaultLangPrefix = '';
			if($totalLang > 1)
				$defaultLangPrefix = '_'.$app->defaultlanguage;
			
			$this->load->library('excel'); // Load PHPExcel Library
			
			if ($this->input->post('postback') == 'postback') {           
			//Uploads excel
				if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid)) {
					mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid, 0755, true);
				}
				/* Zip File Upload Code Start
				if (!is_dir($this->config->item('imagespath') . "upload/sessionsimages/")) {
					mkdir($this->config->item('imagespath') . "upload/sessionsimages/", 0755, true);
				}
	
				if (!is_dir($this->config->item('imagespath') . "upload/sessionsimages/" . $eventid)) {
					mkdir($this->config->item('imagespath') . "upload/sessionsimages/" . $eventid, 0755, true);
				}
				
				//Upload Zip File
				if($_FILES["zip_file"]["name"]) {
						$filename = $_FILES["zip_file"]["name"];
						$source = $_FILES["zip_file"]["tmp_name"];
						$type = $_FILES["zip_file"]["type"];
	
						$name = explode(".", $filename);
						$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
						foreach($accepted_types as $mime_type) {
								if($mime_type == $type) {
										$okay = true;
										break;
								} 
						}
	
						$continue = strtolower($name[1]) == 'zip' ? true : false;
						if(!$continue) {
								$error .= "The file you are trying to upload is not a .zip file. Please try again.";
						}
	
						$target_path = $this->config->item('imagespath') . "upload/sessionsimages/" . $eventid.$filename;  // change this to the correct site path
						if(move_uploaded_file($source, $target_path)) {
								$zip = new ZipArchive();
								$x = $zip->open($target_path);
								if ($x === true) {
										$zip->extractTo($this->config->item('imagespath') . "upload/sessionsimages/" . $eventid); // change this to the correct site path
										$zip->close();
	
										unlink($target_path);
								}
								$error .= "Your .zip file was uploaded and unpacked.";
						} else {	
								$error .= "There was a problem with the upload. Please try again.";
						}
				}
				///////////
				
				// Zip File Upload Code End 
				*/
				
				$config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $eventid;
				//$config['allowed_types'] = 'csv|xl|xls|xlsx'; // local
				$config['allowed_types'] = '*'; // live            
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
	
				$file_type = $_FILES['excelfile']['type'];
	
				if (!in_array($file_type, $supported_types)) {
					$error .="<p>The file type is not supported.</p>";
				} else if (!$this->upload->do_upload('excelfile')) {
	
					if ($_FILES['excelfile']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					$returndata = $this->upload->data();
					
					$headers = TRUE;
					$heading = array();
	
					$row = 1;
					$error = '';
	
					$pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $eventid . "/" . $returndata['file_name'];
	
					$datafile = $pathtofile; //important
	
					if (file_exists($pathtofile)) {
						
						$view_data = $this->_readExcelFile($datafile);                    
						$toalRows = count($view_data);
						
						$name_errors = array();
						$url_errors = array();
						$group_errors = array();
						$dats_errors = array();
						$start_date_errors = array();
						$end_date_errors = array();                    
						$input_error = false;                    
						
						$alloweImageExt = array('jpg', 'jpeg', 'png', 'bmp', 'gif');
						
						for($k=1; $k<$toalRows; $k++){
							$start_datetime = $this->set_datetime($view_data[$k]['starttime']);
							$end_datetime   = $this->set_datetime($view_data[$k]['endtime']);
							
							$enddate    = strtotime($end_datetime);
							$startdate  = strtotime($start_datetime);
							
							if(!$view_data[$k]['sessiongroup']):
								$group_errors[] = $k + 1;
								$input_error = true;
							endif;
							if(!$view_data[$k]['name'.$defaultLangPrefix]):
								$name_errors[] = $k + 1;
								$input_error = true;
							endif;
							if (!$start_datetime || !$end_datetime):
								$dats_errors[] = $k + 1; 
								$input_error = true;
							endif;
							if ($enddate < $startdate):	
									$end_date_errors[] = $k + 1; //add to array of end dates errors.
									$input_error = true;
							elseif ($enddate == $startdate):
									if ($endtime < $starttime):
										$end_date_errors[] = $k + 1; //add to array of end dates errors.
										$input_error = true;
									endif;
							endif;
							
							( $input_error == false )? $confirm = true :  $inputvalid = false;
							
							//------------ Format Error for View ------------//
							if (count($group_errors) > 0) {
								$group_error_str = implode(", ", $group_errors);
								$error .="Incomplete Or Incorrect Session group at rows [" . $group_error_str . "]<br />";
							}
	
							if (count($name_errors) > 0) {
								$name_error_str = implode(", ", $name_errors);
								$error .="Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
							}
	
							if (count($dats_errors) > 0) {
								$dats_error_str = implode(", ", $dats_errors);
								$error .="Incomplete Or Incorrect Date at rows [" . $dats_error_str . "]<br />";
							}
	
							if (count($end_date_errors) > 0) {
								$end_date_error_str = implode(", ", $end_date_errors);
								$error .="End date & time should be greater than Start date & time at rows [" . $end_date_error_str . "]<br />";
							}
	
							if (count($url_errors) > 0) {
								$urle_error_str = implode(", ", $url_errors);
								$error .="Incomplete Or Incorrect url at rows [" . $urle_error_str . "]<br />";
							}
						}                    
					//----------------------------------------------//
					} else {
						echo "no file";
					}
				}
			} else if ($this->input->post('postback') == 'saveconfirm') {
	
				$datafile = $this->input->post('datafile');            
				if ($datafile != '') {
					
					$pathtofile = $datafile;
	
					if (file_exists($pathtofile)) {
	
						$file_data = $this->_readExcelFile($datafile);
						$toalRows = count($file_data);
						
						$firstRow = true;
						$parents = array();
						foreach($file_data as $dataRow){
							if($firstRow):
								$firstRow = false;
								continue;
							endif;
							
							// Check if a session group exist ?
							$sessiongroupExists = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid AND name = ? LIMIT 1", $dataRow['sessiongroup']);
	
							if ($sessiongroupExists->num_rows() > 0) {
								$sessiongroupid = $sessiongroupExists->row()->id;
							} else {
								$sessiongroupid = $this->general_model->insert('sessiongroup', array('eventid' => $eventid, 'name' => $dataRow['sessiongroup']));
								if ($sessiongroupid) {
									$this->language_model->addTranslation('sessiongroup', $sessiongroupid, 'name', $app->defaultlanguage, $dataRow['sessiongroup']);
								}
							}
							
							$order = 0;
							
							unset($dataRow['sessiongroup']);
							
							$dataRow['sessiongroupid']  = $sessiongroupid;
							$dataRow['eventid']         = $eventid;
							$dataRow['order']           = $order;
							$dataRow['starttime']       = $this->set_datetime($dataRow['starttime']);
							$dataRow['endtime']         = $this->set_datetime($dataRow['endtime']);

							$location = $dataRow['location'];

							$locationExists = $this->db->query("SELECT xpos, ypos FROM session WHERE eventid = $eventid AND location = ? LIMIt 1", array($location));
							if($locationExists->num_rows() > 0) {
								$locationExists = $locationExists->row();
								$dataRow['xpos'] = $locationExists->xpos;
								$dataRow['ypos'] = $locationExists->ypos;
							}
							
							$imageurl = '';
							if($sessionrow['imageurl']) {
								$imageurl = trim($sessionrow['imageurl']);
								$dataRow['imageurl'] = $imageurl;
							}
							
								
							/*--------------Social Media Information--------------------*/
							$twitter  = checkhttp($dataRow['twitter']);
							$facebook = checkhttp($dataRow['facebook']);
							$youtube  = checkhttp($dataRow['youtube']);
							unset($dataRow['twitter']);
							unset($dataRow['facebook']);
							unset($dataRow['youtube']);
							/*----------------------------------------------------------*/                        
							
							$externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
							
							if($externalId){

								$whrArr = array(
									'external_id' => $externalId,
									'eventid'     => $eventid
								);
		
								$updId = $this->general_model->rowExists('session', $whrArr);
									
								if($updId):
									$sessionid = $this->_importExcelToDatabase('session', $dataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $updId);
								else:
									$sessionid = $this->_importExcelToDatabase('session', $dataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);
								endif;
							
							}else{
								
								$whrArr = array(
									'eventid'	=> $eventid,
									'name'		=> $dataRow['name'.$defaultLangPrefix],
									'starttime'	=> $dataRow['starttime']
								);
		
								$updId = $this->general_model->rowExists('session', $whrArr);
									
								if($updId):
									$sessionid = $this->_importExcelToDatabase('session', $dataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $updId);
								else:
									$sessionid = $this->_importExcelToDatabase('session', $dataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);
								endif;
							}

							if(isset($dataRow['parent_sessions_id']) && !empty($dataRow['parent_sessions_id'])) {
								$parents[$sessionid] = $dataRow['parent_sessions_id'];
							}
							
							if($add_social_media){
								if($twitter)
									$this->general_model->insert('metadata', array(
										'appid'         => $app->id,
										'table'         => 'session',
										'identifier'    => $sessionid,
										'key'           => 'twitter',
										'value'         => $twitter
										));
								// End if($twitter)
										
								if($facebook)
									$this->general_model->insert('metadata', array(
										'appid'         => $app->id,
										'table'         => 'session',
										'identifier'    => $sessionid,
										'key'           => 'facebook',
										 'value'         => $facebook
										));
								 // End if($twitter)
										
								 if($youtube)
									$this->general_model->insert('metadata', array(
										'appid'         => $app->id,
										'table'         => 'session',
										'identifier'    => $sessionid,
										'key'           => 'youtube',
										'value'         => $youtube
									 ));
								  // End if($youtube)
										
							} // End if($add_social_media)
							
						}

						// parentids
						foreach($parents as $sessionid => $external_parentid) {
							$parentsession = $this->sessions_model->getByExternalId($external_parentid, $eventid);
							if($parentsession) {
								$this->general_model->update('session', $sessionid, array(
										'parentid' => $parentsession->id
									));
							}
						}
						
						unlink($pathtofile);                    
						$this->session->set_flashdata('event_feedback', 'The session has successfully been added!');
						_updateTimeStamp($eventid);
						redirect('sessions/event/' . $eventid);
					} else {
						echo "no file";
					}
				}
			} else if ($this->input->post('postback') == 'cancelimport') {
	
				$datafile = $this->input->post('datafile');
	
				if ($datafile != '') {
					$pathtofile = $datafile;
	
					if (file_exists($pathtofile)) {
						unlink($pathtofile);
					}
				}
	
				$this->session->set_flashdata('event_feedback', 'The session import has been cancelled!');
				_updateTimeStamp($eventid);
				redirect('sessions/event/' . $eventid);
			}
	
			if ($confirm == true && $inputvalid == true) {
				$cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
			} else {
				if ($datafile != '') {
					$pathtofile = $datafile;
	
					if (file_exists($pathtofile)) {
						unlink($pathtofile);
					}
				}
				$cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example, 'zip_field_label' => 'Speaker Zip File'), TRUE);
			}
			
			$cdata['crumb'] = array("Events" => "events", $event->name => "event/view/" . $event->id, "Sessions" => "sessions/event/" . $event->id, "Excel import" => "import/sessions/" . $event->id);
			$cdata['sidebar'] = $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
			
				
		} else {
			//$template_file = $this->config->item('publicupload') . 'excelfiles/templates/sessions.xls';
			$template_file = 'import/download/sessions';
			$template_example = $this->config->item('publicupload') . 'excelfiles/examples/sessions.png';
			
			
			$supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");
	
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			$totalLang = count($languages);
			
			$defaultLangPrefix = '';
			if($totalLang > 1)            
				$defaultLangPrefix = '_'.$app->defaultlanguage;
			
			$this->load->library('excel'); // Load PHPExcel Library
			
			if ($this->input->post('postback') == 'postback') {   
			    
				$sheetsName = array('1' => 'Sessions', '2' => 'Speakers');
				
				//Uploads excel
				if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid)) {
					mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid, 0755, true);
				}
				/* Zip File Upload Code Start
				if (!is_dir($this->config->item('imagespath') . "upload/sessionsimages/")) {
					mkdir($this->config->item('imagespath') . "upload/sessionsimages/", 0755, true);
				}
	
				if (!is_dir($this->config->item('imagespath') . "upload/sessionsimages/" . $eventid)) {
					mkdir($this->config->item('imagespath') . "upload/sessionsimages/" . $eventid, 0755, true);
				}
				
				//Upload Zip File
				if($_FILES["zip_file"]["name"]) {
						$filename = $_FILES["zip_file"]["name"];
						$source = $_FILES["zip_file"]["tmp_name"];
						$type = $_FILES["zip_file"]["type"];
	
						$name = explode(".", $filename);
						$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
						foreach($accepted_types as $mime_type) {
								if($mime_type == $type) {
										$okay = true;
										break;
								} 
						}
	
						$continue = strtolower($name[1]) == 'zip' ? true : false;
						if(!$continue) {
								$error .= "The file you are trying to upload is not a .zip file. Please try again.";
						}
	
						$target_path = $this->config->item('imagespath') . "upload/sessionsimages/" . $eventid.$filename;  // change this to the correct site path
						if(move_uploaded_file($source, $target_path)) {
								$zip = new ZipArchive();
								$x = $zip->open($target_path);
								if ($x === true) {
										$zip->extractTo($this->config->item('imagespath') . "upload/sessionsimages/" . $eventid); // change this to the correct site path
										$zip->close();
	
										unlink($target_path);
								}
								$error .= "Your .zip file was uploaded and unpacked.";
						} else {	
								$error .= "There was a problem with the upload. Please try again.";
						}
				}
				///////////
				
				// Zip File Upload Code End 
				*/
				
				$config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $eventid;
				//$config['allowed_types'] = 'csv|xl|xls|xlsx'; // local
				$config['allowed_types'] = '*'; // live
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
	
				$file_type = $_FILES['excelfile']['type'];
	
				if (!in_array($file_type, $supported_types)) {
					$error .="<p>The file type is not supported.</p>";
				} else if (!$this->upload->do_upload('excelfile')) {
	
					if ($_FILES['excelfile']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					$returndata = $this->upload->data();
					
					$headers = TRUE;
					$heading = array();
	
					$row = 1;
					$error = '';
	
					$pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $eventid . "/" . $returndata['file_name'];
	
					$datafile = $pathtofile; //important
	
					if (file_exists($pathtofile)) {
						$tmp = $this->_readExcelFile($datafile);
						$view_data = $tmp[0];
						$view_data2 = $tmp[1];
						
						$toalRows = count($view_data);
	
						$name_errors = array();
						$url_errors = array();
						$group_errors = array();
						$dats_errors = array();
						$start_date_errors = array();
						$end_date_errors = array();
						$speaker_imageurl_errors = array();
						$input_error = false;
						
						$alloweImageExt = array('jpg', 'jpeg', 'png', 'bmp', 'gif');
						for($k=1; $k<$toalRows; $k++){
							$start_datetime = $this->set_datetime($view_data[$k]['starttime']);
							$end_datetime   = $this->set_datetime($view_data[$k]['endtime']);
							$enddate    = strtotime($end_datetime);
							$startdate  = strtotime($start_datetime);
							
							if(!$view_data[$k]['sessiongroup_name']):
								$group_errors[] = $k + 1;
								$input_error = true;
							endif;							
							if(!$view_data[$k]['session_name'.$defaultLangPrefix]):
								$name_errors[] = $k + 1;
								$input_error = true;
							endif;
							if (!$start_datetime || !$end_datetime):
								$dats_errors[] = $k + 1; 
								$input_error = true;
							endif;
							if ($enddate < $startdate):	
									$end_date_errors[] = $k + 1; //add to array of end dates errors.
									$input_error = true;
							elseif ($enddate == $startdate):
									if ($endtime < $starttime):
										$end_date_errors[] = $k + 1; //add to array of end dates errors.
										$input_error = true;
									endif;
							endif;
							
							( $input_error == false )? $confirm = true :  $inputvalid = false;
							
							//------------ Format Error for View ------------//
							if (count($group_errors) > 0) {
								$group_error_str = implode(", ", $group_errors);
								$error .="Incomplete Or Incorrect Session group at rows [" . $group_error_str . "]<br />";
								$group_errors = array();
							}
	
							if (count($name_errors) > 0) {
								$name_error_str = implode(", ", $name_errors);
								$error .="Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
								$name_errors = array();
							}
	
							if (count($dats_errors) > 0) {
								$dats_error_str = implode(", ", $dats_errors);
								$error .="Incomplete Or Incorrect Date at rows [" . $dats_error_str . "]<br />";
								$dats_errors = array();
							}
	
							if (count($end_date_errors) > 0) {
								$end_date_error_str = implode(", ", $end_date_errors);
								$error .="End date & time should be greater than Start date & time at rows [" . $end_date_error_str . "]<br />";
								$end_date_errors = array();
							}
	
							if (count($url_errors) > 0) {
								$urle_error_str = implode(", ", $url_errors);
								$error .="Incomplete Or Incorrect url at rows [" . $urle_error_str . "]<br />";
								$url_errors = array();
							}
						}
						
	//----------------------------------------------//
					} else {
						echo "no file";
					}
				}
			} else if ($this->input->post('postback') == 'saveconfirm') {
	
				$datafile = $this->input->post('datafile');
	
				if ($datafile != '') {
					
					$pathtofile = $datafile;
	
					if (file_exists($pathtofile)) {
						$sessionlauncher = $this->module_mdl->getLauncher(10, 'event', $eventid);
						$parents = array();
						
						$tmp = $this->_readExcelFile($datafile);
						$file_data		= $tmp[0];
						$speakerData 	= $tmp[1];
						
						$toalRows = count($file_data);
						
						$firstRow = true;
						foreach($file_data as $dataRow){
							if($firstRow):
								$firstRow = false;
								continue;
							endif;
							
							// Check if a session group exist ?
							$sessiongroupExists = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid AND name = ? LIMIT 1", $dataRow['sessiongroup_name']);
	
							if ($sessiongroupExists->num_rows() > 0) {
								$sessiongroupid = $sessiongroupExists->row()->id;
							} else {
								$sessiongroupid = $this->general_model->insert('sessiongroup', array('eventid' => $eventid, 'name' => $dataRow['sessiongroup_name']));
								if ($sessiongroupid) {
									$this->language_model->addTranslation('sessiongroup', $sessiongroupid, 'name', $app->defaultlanguage, $dataRow['sessiongroup_name']);
								}
							}
									
						   $order = 0;							
							
							$sessionDataRow = array();
							
							if($totalLang > 1){
								foreach($languages as $langrow){
									$sessionDataRow['name_'.$langrow->key]          = trim($dataRow['session_name_'.$langrow->key]);
									$sessionDataRow['description_'.$langrow->key]   = trim($dataRow['description_'.$langrow->key]);									
								}
							}else {
								$sessionDataRow['name']         = trim($dataRow['session_name']);
								$sessionDataRow['description']  = trim($dataRow['description']);
							}
							
							$sessionDataRow['sessiongroupid']  = $sessiongroupid;
							$sessionDataRow['eventid']         = $eventid;
							$sessionDataRow['order']           = $order;
							$sessionDataRow['starttime']       = $this->set_datetime($dataRow['starttime']);
							$sessionDataRow['endtime']         = $this->set_datetime($dataRow['endtime']);
							$sessionDataRow['location']        = $dataRow['location'];
							$sessionDataRow['url']             = $dataRow['url'];
							
							$externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
							$sessionDataRow['external_id']  = $externalId;
							
							if($externalId){

								$whrArr = array(
									'external_id' => $externalId,
									'eventid'     => $eventid
								);
		
								$updId = $this->general_model->rowExists('session', $whrArr);
									
								if($updId):
									$sessionid = $this->_importExcelToDatabase('session', $sessionDataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $updId);
								else:
									$sessionid = $this->_importExcelToDatabase('session', $sessionDataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);
								endif;
							}
							else{
								
								$whrArr = array(
									'eventid'	=> $eventid,
									'name'		=> $sessionDataRow['name'.$defaultLangPrefix],
									'starttime'	=> $sessionDataRow['starttime']
								);
		
								$updId = $this->general_model->rowExists('session', $whrArr);
									
								if($updId):
									$sessionid = $this->_importExcelToDatabase('session', $sessionDataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $updId);
								else:
									$sessionid = $this->_importExcelToDatabase('session', $sessionDataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);
								endif;
							}

							if(isset($dataRow['parent_sessions_id']) && !empty($dataRow['parent_sessions_id'])) {
								$parents[$sessionid] = $dataRow['parent_sessions_id'];
							}

							// metadata
							if($updId) {
								$this->metadata_model->removeFieldsFromObject('session', $sessionid);
							}
							$metadata = $this->metadata_model->getFieldsByParent('launcher', $sessionlauncher->id);
							foreach($metadata as $m) {
								if(isset($dataRow[$m->name]) && !empty($dataRow[$m->name])) {
									$this->general_model->insert('tc_metavalues', array(
											'metaId' => $m->id,
											'parentType' => 'session',
											'parentId' => $sessionid,
											'name' => $m->name,
											'type' => $m->type,
											'value' => $dataRow[$m->name],
											'language' => $app->defaultlanguage
										));
								}
							}
						
						}//foreach($file_data as $dataRow)

						// parentids
						foreach($parents as $sessionid => $external_parentid) {
							$parentsession = $this->sessions_model->getByExternalId($external_parentid, $eventid);
							if($parentsession) {
								$this->general_model->update('session', $sessionid, array(
										'parentid' => $parentsession->id
									));
							}
						}
						
						$firstRow = true;
						foreach($speakerData as $dataRow){
							if($firstRow):
								$firstRow = false;
								continue;
							endif;
							
							$speakerDataRow = array();
							
							if($totalLang > 1){
								foreach($languages as $langrow){
									$speakerDataRow['name_'.$langrow->key]          = trim($dataRow['speaker_name_'.$langrow->key]);
									$speakerDataRow['description_'.$langrow->key]   = trim($dataRow['speaker_bio_description_'.$langrow->key]);
								}
							}else {
								$speakerDataRow['name']         = trim($dataRow['speaker_name']);
								$speakerDataRow['description']  = trim($dataRow['speaker_bio_description']);
							}
							
							$externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
							$speakerDataRow['external_id']  = $externalId;
							$speakerDataRow['eventid']   = $eventid; 
							//$speakerDataRow['sessionid'] = $sessiongroupid;
							$speakerDataRow['company']   = $dataRow['speaker_company'];
							$speakerDataRow['function']  = $dataRow['speaker_function'];
							$speakerDataRow['order']     = 0;
							
							$sessionExtIds;
							if( stripos($dataRow['session_id'], ',') !== false ):
								$sessionExtIds = explode(',', $dataRow['session_id']);
							elseif( stripos($dataRow['session_id'], '.') !== false ):
								$sessionExtIds = explode('.', $dataRow['session_id']);
							else:
								$sessionExtIds = $dataRow['session_id'];
							endif;							
							
							if($speakerDataRow['name'.$defaultLangPrefix]){
								
								if($externalId){

									$whrArr = array(
										'external_id' => $externalId,
										'eventid'     => $eventid
									);
		
									$updId = $this->general_model->rowExists('speaker', $whrArr);
		
									if($updId): // external id already exists in table, update record
		
										$speakerid = $this->_importExcelToDatabase('speaker', $speakerDataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $updId);
		
									else: // external id don't exists in table, insert record
		
										$speakerid = $this->_importExcelToDatabase('speaker', $speakerDataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);										
										
									endif;
								
								}else{
									$whrArr = array(
										'name' 		=> $speakerDataRow['name'.$defaultLangPrefix],
										'eventid' 	=> $eventid
									);
		
									$updId = $this->general_model->rowExists('speaker', $whrArr);
		
									if($updId): // external id already exists in table, update record
		
										$speakerid = $this->_importExcelToDatabase('speaker', $speakerDataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $updId);
		
									else: // external id don't exists in table, insert record
		
										$speakerid = $this->_importExcelToDatabase('speaker', $speakerDataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);										
										
									endif;
								}

								if(isset($dataRow['speakergroup_name']) && !empty($dataRow['speakergroup_name'])) {
									$group = $this->group_model->getByName($dataRow['speakergroup_name'], 'event', $eventid);
									if($group == false) {
										$parentgroupid = $this->speaker_model->getMainCategorieGroup($eventid);
										$groupid = $this->general_model->insert('group', array(
												'appid' => $app->id,
												'eventid' => $eventid,
												'name' => $dataRow['speakergroup_name'],
												'parentid' => $parentgroupid,
											));
									} else {
										$groupid = $group->id;
									}

									$this->general_model->insert('groupitem', array(
										'appid' => $app->id,
										'eventid' => $eventid,
										'groupid' => $groupid,
										'itemtable' => 'speaker',
										'itemid' => $speakerid
									));
								}


								$inClz = '';
								if( is_array( $sessionExtIds ) ):
									
									foreach( $sessionExtIds as $sessExternalId ):
										$sessExternalId = trim($sessExternalId);
										$whrArr = array(
											'external_id' => $sessExternalId,
											'eventid'     => $eventid
										);
				
										$sessionId = $this->general_model->rowExists('session', $whrArr);
										if(!empty($sessionId)) {
											$inClz .= $sessionId.',';
										}
										
										if( $sessionId ){
											$whrArr = array(
												'speakerid' => $speakerid,
												'eventid'   => $eventid,
												'sessionid'	=> $sessionId
											);
					
											if( ! $this->general_model->rowExists('speaker_session', $whrArr) ) {
												$session_speaker_id = $this->general_model->insert('speaker_session', array(
													'eventid' => $eventid,
													'speakerid' => $speakerid,
													'sessionid' => $sessionId
												));													
											}
										}
											
									endforeach;											
								elseif( $sessionExtIds && $sessionExtIds != 'session_id'):
									$sessionExtIds = trim($sessionExtIds);
									$whrArr = array(
										'external_id' => $sessionExtIds,
										'eventid'     => $eventid
									);
				
									$sessionId = $this->general_model->rowExists('session', $whrArr);
									$inClz = $sessionId;
																					
									if( $sessionId ){
										$whrArr = array(
											'speakerid' => $speakerid,
											'eventid'   => $eventid,
											'sessionid'	=> $sessionId
										);
					
										if( ! $this->general_model->rowExists('speaker_session', $whrArr) ) {
											$session_speaker_id = $this->general_model->insert('speaker_session', array(
												'eventid' => $eventid,
												'speakerid' => $speakerid,
												'sessionid' => $sessionId
											));													
										}
									}
											
								endif;								
								
								if(strpos($inClz, ',') !== false)
									$inClz = substr(trim($inClz), 0, -1);
									
								if(!empty($inClz)):
									$this->db->query( 'DELETE FROM speaker_session WHERE eventid = ' . $eventid . ' AND  speakerid = ' . $speakerid . ' AND sessionid NOT IN (' . $inClz . ')' );
								endif;
							}
						}
						
						unlink($pathtofile);
						
						$this->session->set_flashdata('event_feedback', 'The session has successfully been added!');
						_updateTimeStamp($eventid);
						redirect('sessions/event/' . $eventid);
					} else {
						echo "no file";
					}
				}
			} else if ($this->input->post('postback') == 'cancelimport') {
	
				$datafile = $this->input->post('datafile');
	
				if ($datafile != '') {
					$pathtofile = $datafile;
	
					if (file_exists($pathtofile)) {
						unlink($pathtofile);
					}
				}
	
				$this->session->set_flashdata('event_feedback', 'The session import has been cancelled!');
				_updateTimeStamp($eventid);
				redirect('sessions/event/' . $eventid);
			}
	
			if ($confirm == true && $inputvalid == true) {
				$cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'view_data2' => $view_data2, 'total_cols' => $totalNumberOfCols, 'sheetsName' => $sheetsName), TRUE);
			} else {
				if ($datafile != '') {
					$pathtofile = $datafile;
	
					if (file_exists($pathtofile)) {
						unlink($pathtofile);
					}
				}
				$cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example, 'zip_field_label' => 'Speaker Zip File'), TRUE);
			}
	
			$cdata['crumb'] = array("Events" => "events", $event->name => "event/view/" . $event->id, "Sessions" => "sessions/event/" . $event->id, "Excel import" => "import/sessions/" . $event->id);
			$cdata['sidebar'] = $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

    
//----------------------------- Exhibitors -----------------------------//
//----------------------------------------------------------------------//

    function exhibitors($eventid) {
        
        $datafile = '';
        $view_data = '';
        $confirm = false;
        $inputvalid = true;        
        
        //$template_file = $this->config->item('publicupload') . 'excelfiles/templates/exhibitors.xls';
        $template_file = 'import/download/exhibitor'; // Create Template file dynamically
        
        $template_example = $this->config->item('publicupload') . 'excelfiles/examples/exhibitors.png';
        $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

        $this->load->model('general_model');
        $this->load->model('exhibitor_model');
        $this->load->helper('email_helper');

        $event = $this->event_model->getbyid($eventid);
        
        $app = _actionAllowed($event, 'event', 'returnApp');
        $languages = $this->language_model->getLanguagesOfApp($app->id);
        
        $totalLang = count($languages);
        $defaultLangPostfix = '';
        if($totalLang > 1)
            $defaultLangPostfix = '_'.$app->defaultlanguage;
        
        
        $this->load->library('excel'); // Load PHPExcel Library
        
        if ($this->input->post('postback') == 'postback') {

		//Uploads excel
            if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid)) {
                mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid, 0755, true);
            }
            /* Zip File Upload Code Start
            if (!is_dir($this->config->item('imagespath') . "upload/exhibitorimages/")) {
                mkdir($this->config->item('imagespath') . "upload/exhibitorimages/", 0755, true);
            }

            if (!is_dir($this->config->item('imagespath') . "upload/exhibitorimages/" . $eventid)) {
                mkdir($this->config->item('imagespath') . "upload/exhibitorimages/" . $eventid, 0755, true);
            }
            
            //Upload Zip File
            if($_FILES["zip_file"]["name"]) {
                    $filename = $_FILES["zip_file"]["name"];
                    $source = $_FILES["zip_file"]["tmp_name"];
                    $type = $_FILES["zip_file"]["type"];

                    $name = explode(".", $filename);
                    $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
                    foreach($accepted_types as $mime_type) {
                            if($mime_type == $type) {
                                    $okay = true;
                                    break;
                            } 
                    }

                    $continue = strtolower($name[1]) == 'zip' ? true : false;
                    if(!$continue) {
                            $error .= "The file you are trying to upload is not a .zip file. Please try again.";
                    }

                    $target_path = $this->config->item('imagespath') . "upload/exhibitorimages/" . $eventid.$filename;  // change this to the correct site path
                    if(move_uploaded_file($source, $target_path)) {
                            $zip = new ZipArchive();
                            $x = $zip->open($target_path);
                            if ($x === true) {
                                    $zip->extractTo($this->config->item('imagespath') . "upload/exhibitorimages/" . $eventid); // change this to the correct site path
                                    $zip->close();

                                    unlink($target_path);
                            }
                            $error .= "Your .zip file was uploaded and unpacked.";
                    } else {	
                            $error .= "There was a problem with the upload. Please try again.";
                    }
            }
            ///////////
            // Zip File Upload Code End
            */
            $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $eventid;
            //$config['allowed_types'] = 'csv|xl|xls|xlsx';
            $config['allowed_types'] = '*'; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $file_type = $_FILES['excelfile']['type'];

//echo($file_type); //To see the type..

            if (!in_array($file_type, $supported_types)) {
                $error .="<p>The file type is not supported.</p>";
            } else if (!$this->upload->do_upload('excelfile')) {

                if ($_FILES['excelfile']['name'] != '') {
                    $error .= $this->upload->display_errors();
                }
            } else {

//successfully uploaded
                $returndata = $this->upload->data();

                $headers = TRUE;
                $heading = array();

                $row = 1;
                $error = '';

                $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $eventid . "/" . $returndata['file_name'];

                $datafile = $pathtofile; //important

                if (file_exists($pathtofile)) {
                    
                    $view_data = $this->_readExcelFile($datafile);
                    
                    $name_errors = array();
                    $email_errors = array();
                    $input_error = false;
                    $toalRows = count($view_data);
                    for($k=1; $k<$toalRows; $k++){
                        if(!$view_data[$k]['name'.$defaultLangPostfix]):
                            $name_errors[] = $k + 1; //add to array of name errors.
                            $input_error = true;
                        endif;
                        if ($view_data[$k]['email'] != ''):
                            if (!$this->valid_email($view_data[$k]['email'])):
                                $email_errors[] = $k + 1; //add to array of email errors.
                                $input_error = true;
                            endif;
                        endif;
                    }
                    //------------ Format Error for View ------------//
                    if (count($name_errors) > 0) {
                        $name_error_str = implode(", ", $name_errors);
                        $error .="Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
                    }

                    if (count($email_errors) > 0) {
                        $email_error_str = implode(", ", $email_errors);
                        $error .="Incomplete Or Incorrect Email at rows [" . $email_error_str . "]<br />";
                    }
                    
                    ( $input_error == false )? $confirm = true :  $inputvalid = false;                    
                    
                    //----------------------------------------------//
                } else {
                    echo "no file";
                }
            }
        } else if ($this->input->post('postback') == 'saveconfirm') {

            $datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $error = '';

                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {

                    $file_data = $this->_readExcelFile($datafile);
                    $toalRows = count($file_data);
                    
                    $exhibitorcategoriesgroupid = $this->exhibitor_model->getMainCategorieGroup($eventid);
                    
                    if ( $error == '' && $exhibitorcategoriesgroupid ) {
                        $firstRow = true;
                        $catIndex = 0;
                        foreach($file_data as $dataRow){
                            if($firstRow):
                                foreach($dataRow as $key=>$val){
                                    if(strstr($key, 'category'))
                                        $catIndex++;
                                }
                                $firstRow = false;
                                continue;
                            endif;                            
                            
                            $externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
                            $dataRow['eventid'] = $eventid;
                            
                            $catDataRow = $dataRow;
                            for($cind = 1; $cind <=$catIndex; $cind++){                                    
                                unset($dataRow['category'.$cind]);
                            }
                            // external id exists in excel file
                            if($externalId):                                    
                                $whrArr = array(
                                    'external_id' => $externalId,
                                    'eventid'     => $eventid
                                );
                                    
                                $updId = $this->general_model->rowExists('exhibitor', $whrArr);
                                    
                                if($updId): // external id already exists in table, update record
                                        
                                    $exhibitorid = $this->_importExcelToDatabase('exhibitor', $dataRow, 'update', $languages, array('name' => 'name' , 'description' => 'description'), $app, $updId);
                                    
                                 else: // external id don't exists in table, insert record 
                                        
                                    $exhibitorid = $this->_importExcelToDatabase('exhibitor', $dataRow, 'add', $languages, array('name' => 'name' , 'description' => 'description'), $app);
                                        
                                 endif;
                                    
                            else: // external id don't exits in excel file
                                    
                                $whrArr = array(
                                    'name'      => $dataRow['name'.$defaultLangPostfix],
                                    'eventid'   => $eventid
                                );
                                    
                                $updId = $this->general_model->rowExists('exhibitor', $whrArr);
                                    
                                if($updId): // external id already exists in table, update record
                                        
                                    $exhibitorid = $this->_importExcelToDatabase('exhibitor', $dataRow, 'update', $languages, array('name' => 'name' , 'description' => 'description'), $app, $updId);
                                    
                                else: // external id don't exists in table, insert record 
                                        
                                    $exhibitorid = $this->_importExcelToDatabase('exhibitor', $dataRow, 'add', $languages, array('name' => 'name' , 'description' => 'description'), $app);
                                        
                                endif;
                                    
                                    
                            endif;
                            
                            // Removing groupitems for current exhibitor
                            $this->db->query("DELETE FROM `groupitem` WHERE `appid` = '" . $app->id . "' AND `eventid` ='" . $eventid . "' AND `itemtable` = 'exhibitor' AND `itemid` = '".$exhibitorid."'");
                            
                            // Start: Inserting Groups / Categories
                            $parentid = $exhibitorcategoriesgroupid;
                                
                            for($cind = 1; $cind <=$catIndex; $cind++){                                    
                                $gpName = trim($catDataRow['category'.$cind]);
                                
                                if($gpName){
                                    $groupExists = $this->db->query("SELECT * FROM `group` WHERE `appid` = '" . $app->id . "' AND `parentid` ='" . $parentid . "' AND `name` = ? LIMIT 1", $gpName);

                                    if ($groupExists->num_rows() > 0) {
                                        $groupid = $groupExists->row()->id;
                                            
                                        if ($groupid) {
                                                
                                            $groupItemExists = $this->db->query("SELECT * FROM `groupitem` WHERE `itemid` = '".$exhibitorid."' AND `itemtable` = 'exhibitor' AND `appid` = '" . $app->id . "' AND `eventid` ='" . $eventid . "' AND `groupid` = ? LIMIT 1", $groupid);

                                            if($groupItemExists->num_rows() == 0)
                                            {
                                                // Inserting Group Items
                                                $giData = array();
                                                $giData['appid']        = $app->id;
                                                $giData['eventid']      = $eventid;
                                                $giData['groupid']      = $groupid;
                                                $giData['itemtable']    = 'exhibitor';
                                                $giData['itemid']       = $exhibitorid;

                                                $this->general_model->insert('groupitem', $giData);
                                            }
                                        } // if ($groupid)
                                    } else {
                                        //insert ist group/category

                                        $gpData = array();
                                        $gpData['appid']    = $app->id; 
                                        $gpData['eventid']  = $eventid;
                                        $gpData['name']     = $gpName;
                                        $gpData['parentid'] = $parentid;
                                        $gpData['tree']     = 0;

                                        $groupid = $this->general_model->insert('group', $gpData);

                                        if ($groupid) {
                                            $this->language_model->addTranslation('group', $groupid, 'name', $app->defaultlanguage, $gpName);

                                            // Inserting Group Items
                                            $giData = array();
                                            $giData['appid']        = $app->id;
                                            $giData['eventid']      = $eventid;
                                            $giData['groupid']      = $groupid;
                                            $giData['itemtable']    = 'exhibitor';
                                            $giData['itemid']       = $exhibitorid;

                                            $this->general_model->insert('groupitem', $giData);

                                       } // if ($groupid)
                                    }
                                        
                                }// End: if($gpName)
                                    
                            } // End: for($cind = 1; $cind <=$catIndex; $cind++)
                                
                            // End: Inserting Groups / Categories                                
                                                            
                        } // foreach($file_data[$k] as $dataRow)
                            
                    } // if ($error == '' && $exhibitorcategoriesgroupid != '')
                    
                    unlink($pathtofile); //Remove Excel File

                    $this->session->set_flashdata('event_feedback', 'The exhibitors has successfully been added!');
                    _updateTimeStamp($eventid);
                    redirect('exhibitors/event/' . $eventid);
                } else {
                    echo "no file";
                }
            }
        } else if ($this->input->post('postback') == 'cancelimport') {

            $datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }

            $this->session->set_flashdata('event_feedback', 'The exhibitor import has been cancelled!');
            _updateTimeStamp($eventid);
            redirect('exhibitors/event/' . $eventid);
        }
        if ($confirm == true && $inputvalid == true) {
            $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
        } else {
            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }
            $cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
        }

        $cdata['crumb'] = array("Events" => "events", $event->name => "event/view/" . $event->id, "Exhibitors" => "exhibitors/event/" . $event->id, "Excel import" => "import/exhibitors/" . $event->id);
        $cdata['sidebar'] = $this->load->view('c_sidebar', array('event' => $event), TRUE);
        $this->load->view('master', $cdata);
    }

    
//----------------------------- Attendees ------------------------------//
//----------------------------------------------------------------------//

    function attendees($eventid) {

        $datafile = '';
        $view_data = '';
        $confirm = false;
        $inputvalid = true;       
        
        //$template_file = $this->config->item('publicupload') . 'excelfiles/templates/attendees.xls';
        $template_file = 'import/download/attendee'; // Create Template file dynamically
        
        $template_example = $this->config->item('publicupload') . 'excelfiles/examples/attendees.png';
        $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

        $this->load->model('general_model');
        $this->load->helper('email_helper');

        $event = $this->event_model->getbyid($eventid);
        
        $app = _actionAllowed($event, 'event', 'returnApp');
        $languages = $this->language_model->getLanguagesOfApp($app->id);
        
        $totalLang =  count($languages);
        
        $this->load->library('excel'); // Load PHPExcel Library
        
        if ($this->input->post('postback') == 'postback') {
            
            if($_FILES['excelfile']['name']){            
                //Uploads excel
                if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid)) {
                    mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid, 0755, true);
                }
                /* Zip File Upload Code Start
                 
                if (!is_dir($this->config->item('imagespath') . "upload/attendeesimages/")) {
                    mkdir($this->config->item('imagespath') . "upload/attendeesimages/", 0755, true);
                }
                
                if (!is_dir($this->config->item('imagespath') . "upload/attendeesimages/" . $eventid)) {
                    mkdir($this->config->item('imagespath') . "upload/attendeesimages/" . $eventid, 0755, true);
                }

                //Upload Zip File
                if($_FILES["zip_file"]["name"]) {
                        $filename = $_FILES["zip_file"]["name"];
                        $source = $_FILES["zip_file"]["tmp_name"];
                        $type = $_FILES["zip_file"]["type"];

                        $name = explode(".", $filename);
                        $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
                        foreach($accepted_types as $mime_type) {
                                if($mime_type == $type) {
                                        $okay = true;
                                        break;
                                } 
                        }

                        $continue = strtolower($name[1]) == 'zip' ? true : false;
                        if(!$continue) {
                                $error .= "The file you are trying to upload is not a .zip file. Please try again.";
                        }

                        $target_path = $this->config->item('imagespath') . "upload/attendeesimages/" . $eventid.$filename;  // change this to the correct site path                    
                        if(move_uploaded_file($source, $target_path)) {
                                $zip = new ZipArchive();
                                $x = $zip->open($target_path);
                                if ($x === true) {
                                        $zip->extractTo($this->config->item('imagespath') . "upload/attendeesimages/" . $eventid); // change this to the correct site path
                                        $zip->close();

                                        unlink($target_path);
                                }
                                //$error .= "Your .zip file was uploaded and unpacked.";
                        } else {	
                                $error .= "There was a problem with the upload. Please try again.";
                        }
                }
                ///////////
                
                // Zip File Upload Code End
                */
                
                $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $eventid;
                //$config['allowed_types'] = 'csv|xl|xls|xlsx';
                $config['allowed_types'] = '*';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

    //echo($_FILES['excelfile']['type']);

                $file_type = $_FILES['excelfile']['type'];

                if (!in_array($file_type, $supported_types)) {
                    $error .="<p>The file type is not supported.</p>";
                } else if (!$this->upload->do_upload('excelfile')) {

                    if ($_FILES['excelfile']['name'] != '') {
                        $error .= $this->upload->display_errors();
                    }
                } else {

    //successfully uploaded
                    $returndata = $this->upload->data();

                    $headers = TRUE;
                    $heading = array();

                    $row = 1;
                    $error = '';

                    $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $eventid . "/" . $returndata['file_name'];

                    $datafile = $pathtofile; //important

                    if (file_exists($pathtofile)) {
                        $view_data = $this->_readExcelFile($datafile);
                        $toalRows = count($view_data);
                        
                        $name_errors = array();
                        $firstname_errors = array();
                        $email_errors = array();
                        $input_error = false;
                        
                        for($k=1; $k<$toalRows; $k++){
                            if(!$view_data[$k]['name']):
                                $name_errors[] = $k + 1; //add to array of name errors.
                                $input_error = true;
                            endif;
                            if(!$view_data[$k]['firstname']):
                                $name_errors[] = $k; //add to array of name errors.
                                $input_error = true;
                            endif;
                            if (trim($view_data[$k]['email']) != ''):
                                if (!$this->valid_email(trim($view_data[$k]['email']))):
                                    $email_errors[] = $k + 1; //add to array of email errors.
                                    $input_error = true;
                                endif;
                            endif;
                        }
                        
                        ( $input_error == false )? $confirm = true :  $inputvalid = false;

                        //------------ Format Error for View ------------//
                        if (count($name_errors) > 0) {
                            $name_error_str = implode(", ", $name_errors);
                            $error .="Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
                        }

                        if (count($firstname_errors) > 0) {
                            $firstname_error_str = implode(", ", $firstname_errors);
                            $error .="Incomplete Or Incorrect First Name at rows [" . $firstname_error_str . "]<br />";
                        }

                        if (count($email_errors) > 0) {
                            $email_error_str = implode(", ", $email_errors);
                            $error .="Incomplete Or Incorrect Email at rows [" . $email_error_str . "]<br />";
                        }
    //----------------------------------------------//	
                    } else {
                        echo "no file";
                    }
                }
            }
            else {
                $error .= "Please select xls file.";
            }
        } else if ($this->input->post('postback') == 'saveconfirm') {

            $datafile = $this->input->post('datafile');
           
            if ($datafile != '') {
                $error = '';

                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {

                    $file_data = $this->_readExcelFile($datafile);
                    $toalRows = count($file_data);
                    
                    $firstRow = true;
                    foreach($file_data as $dataRow){
                        if($firstRow):
                            $firstRow = false;
                            continue;
                        endif;
                            
                        $externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
                        $dataRow['eventid'] = $eventid;
                        $dataRow['phonenr'] = $dataRow['phone'];
                        $dataRow['linkedin'] = checkhttp( $dataRow['public_linkedin_url'] );
                        $facebook = !empty( $dataRow['facebook'] ) ? checkhttp( $dataRow['facebook'] ) : '';
						$twitter = !empty( $dataRow['twitter'] ) ? checkhttp( $dataRow['twitter'] ) : '';
						
						
						unset($dataRow['phone']);
                        unset($dataRow['public_linkedin_url']);
                        unset($dataRow['facebook']);
						unset($dataRow['twitter']);
                        // external id exists in excel file
                        if($externalId):
                            
                            $whrArr = array(
                                'external_id' => $externalId,
                                'eventid'     => $eventid
                            );
                                    
                            $updId = $this->general_model->rowExists('attendees', $whrArr);
                                    
                            if($updId): // external id already exists in table, update record
                                        
                                $attendeesid = $this->_importExcelToDatabase('attendees', $dataRow, 'update', $languages, array('description' => 'description'), $app, $updId);
                                    
                            else: // external id don't exists in table, insert record 
                                        
                                $attendeesid = $this->_importExcelToDatabase('attendees', $dataRow, 'add', $languages, array('description' => 'description'), $app);
                                        
                            endif;
                                    
                        else: // external id don't exits in excel file
                                    
                            $whrArr = array(
                                'name'      => $dataRow['name'],
                                'firstname'      => $dataRow['firstname'],
                                'eventid'   => $eventid
                            );

                            $updId = $this->general_model->rowExists('attendees', $whrArr);

                            if($updId): // external id already exists in table, update record

                                $attendeesid = $this->_importExcelToDatabase('attendees', $dataRow, 'update', $languages, array('description' => 'description'), $app, $updId);

                            else: // external id don't exists in table, insert record 

                                $attendeesid = $this->_importExcelToDatabase('attendees', $dataRow, 'add', $languages, array('description' => 'description'), $app);

                            endif;
                        
                        endif; // End if external id exists in excel file
						
						
						/*--------------Social Media Information--------------------*/
						if($twitter){
							$metadata_social = $this->general_model->rowExists('metadata', array('appid' => $app->id, 'table' => 'attendees', 'identifier' => $attendeesid, 'key' => 'twitter'), 1);
							if( count( $metadata_social ) > 0 ){
								$this->general_model->update('metadata', $metadata_social->id, array(
									'value'         => $twitter
								));
							}else{
								$this->general_model->insert('metadata', array(
									'appid'         => $app->id,
									'table'         => 'attendees',
									'identifier'    => $attendeesid,
									'key'           => 'twitter',
									'value'         => $facebook
								));
							}
						}// End if($twitter)
										
						if($facebook){
							$metadata_social = $this->general_model->rowExists('metadata', array('appid' => $app->id, 'table' => 'attendees', 'identifier' => $attendeesid, 'key' => 'facebook'), 1);
							if( count( $metadata_social ) > 0 ){
								$this->general_model->update('metadata', $metadata_social->id, array(
									'value'         => $twitter
								));
							}else{
								$this->general_model->insert('metadata', array(
									'appid'         => $app->id,
									'table'         => 'attendees',
									'identifier'    => $attendeesid,
									'key'           => 'facebook',
									'value'         => $facebook
								));
							}
						}// End if($facebook)
					}
                    unlink($pathtofile); //Remove Excel File

                    $this->session->set_flashdata('event_feedback', 'The attendees has successfully been added!');
                    _updateTimeStamp($eventid);
                    redirect('attendees/event/' . $eventid);
                } else {
                    echo "no file";
                }
            }
        } else if ($this->input->post('postback') == 'cancelimport') {

            $datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }

            $this->session->set_flashdata('event_feedback', 'The attendees import has been cancelled!');
            _updateTimeStamp($eventid);
            redirect('attendees/event/' . $eventid);
        }
        if ($confirm == true && $inputvalid == true) {
            $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
        } else {
            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }
            $cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
        }

        $cdata['crumb'] = array("Events" => "events", $event->name => "event/view/" . $event->id, "Attendees" => "attendees/event/" . $event->id, "Excel import" => "import/attendees/" . $event->id);
        $cdata['sidebar'] = $this->load->view('c_sidebar', array('event' => $event), TRUE);
        $this->load->view('master', $cdata);
    }

    
//----------------------------- Sponsors ------------------------------//
//---------------------------------------------------------------------//
    function _sponsorGroup($type, $sponsorgroup, $evVnArr, $defaultLang, $updId = 0){
        
        if( $type == 'add' ):
            $sponsorgroupExists = $this->db->query("SELECT * FROM sponsorgroup WHERE " . $evVnArr[0] . " = " . $evVnArr[1] . " AND name = ? LIMIT 1", $sponsorgroup);
            if ($sponsorgroupExists->num_rows() > 0):
                return $sponsorgroupExists->row()->id;            
            endif;
        elseif( $type == 'update' ):
            
            $sponsorgroupExists = $this->db->query("SELECT * FROM sponsorgroup WHERE " . $evVnArr[0] . " = " . $evVnArr[1] . " AND id = ? LIMIT 1", $updId);
            if ($sponsorgroupExists->num_rows() > 0):
                
                $this->db->where('id', $updId);
                $this->db->update('sponsorgroup', array('name' => $sponsorgroup));
                
                return $updId;
                
            endif;
        
        endif;
        
        $order = 0;
        
        $sponsorgroupid = $this->general_model->insert('sponsorgroup', array($evVnArr[0] => $evVnArr[1], 'name' => $sponsorgroup, 'order' => $order));
        if ($sponsorgroupid) {
            $this->language_model->addTranslation('sponsorgroup', $sponsorgroupid, 'name', $defaultLang, $sponsorgroup);
        }
                
        return $sponsorgroupid;

    }
    
    function sponsors($id, $type = 'event') {

//--------------------- For Venuee ------------------------//

        if ($type == 'event') {

            $eventid = $id;
            $datafile = '';
            $view_data = '';
            $confirm = false;
            $inputvalid = true;            
            
            //$template_file = $this->config->item('publicupload') . 'excelfiles/templates/sponsors.xls';
            $template_file = 'import/download/sponsor'; // Create Template file dynamically
            
            $template_example = $this->config->item('publicupload') . 'excelfiles/examples/sponsors.png';
            $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

            $this->load->model('general_model');

            $event = $this->event_model->getbyid($eventid);
            if($event->eventtypeid == 6)
                $add_social_media = true;
            
            $app = _actionAllowed($event, 'event', 'returnApp');
            $languages = $this->language_model->getLanguagesOfApp($app->id);
            
            $totalLang = count($languages);
            $defaultLangPrefix = '';
            
            if($totalLang > 1)
                $defaultLangPrefix = '_'.$app->defaultlanguage;
            
            $this->load->library('excel'); // Load PHPExcel Library
            
            if ($this->input->post('postback') == 'postback') {
//Uploads excel
                if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid)) {
                    mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $eventid, 0755, true);
                }
                /* Zip File Upload Code Start
                if (!is_dir($this->config->item('imagespath') . "upload/sponsorsimages/")) {
                    mkdir($this->config->item('imagespath') . "upload/sponsorsimages/", 0755, true);
                }

                if (!is_dir($this->config->item('imagespath') . "upload/sponsorsimages/" . $eventid)) {
                    mkdir($this->config->item('imagespath') . "upload/sponsorsimages/" . $eventid, 0755, true);
                }

                //Upload Zip File
                if($_FILES["zip_file"]["name"]) {
                        $filename = $_FILES["zip_file"]["name"];
                        $source = $_FILES["zip_file"]["tmp_name"];
                        $type = $_FILES["zip_file"]["type"];

                        $name = explode(".", $filename);
                        $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
                        foreach($accepted_types as $mime_type) {
                                if($mime_type == $type) {
                                        $okay = true;
                                        break;
                                } 
                        }

                        $continue = strtolower($name[1]) == 'zip' ? true : false;
                        if(!$continue) {
                                $error .= "The file you are trying to upload is not a .zip file. Please try again.";
                        }

                        $target_path = $this->config->item('imagespath') . "upload/sponsorsimages/" . $eventid.$filename;  // change this to the correct site path
                        if(move_uploaded_file($source, $target_path)) {
                                $zip = new ZipArchive();
                                $x = $zip->open($target_path);
                                if ($x === true) {
                                        $zip->extractTo($this->config->item('imagespath') . "upload/sponsorsimages/" . $eventid); // change this to the correct site path
                                        $zip->close();

                                        unlink($target_path);
                                }
                                $error .= "Your .zip file was uploaded and unpacked.";
                        } else {	
                                $error .= "There was a problem with the upload. Please try again.";
                        }
                }
                ///////////
                
                // Zip File Upload Code End
                */
                $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $eventid;
                //$config['allowed_types'] = 'csv|xl|xls|xlsx';
                $config['allowed_types'] = '*';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                $file_type = $_FILES['excelfile']['type'];

                if (!in_array($file_type, $supported_types)) {
                    $error .="<p>The file type is not supported.</p>";
                } else if (!$this->upload->do_upload('excelfile')) {

                    if ($_FILES['excelfile']['name'] != '') {
                        $error .= $this->upload->display_errors();
                    }
                } else {

//successfully uploaded
                    $returndata = $this->upload->data();

                    $headers = TRUE;
                    $heading = array();

                    $row = 1;
                    $error = '';

                    $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $eventid . "/" . $returndata['file_name'];

                    $datafile = $pathtofile; //important

                    if (file_exists($pathtofile)) {

                        $view_data = $this->_readExcelFile($datafile);
                        $toalRows = count($view_data);
                        
                        $group_errors = array();
                        $name_errors = array();
                        $input_error = false;

                        for($k=1; $k<$toalRows; $k++){
                            if(!$view_data[$k]['name'.$defaultLangPrefix]):
                                $name_errors[] = $k + 1; //add to array of name errors.
                                $input_error = true;
                            endif;
                            if(!$view_data[$k]['sponsorgroup']):
                                $group_errors[] = $k + 1; //add to array of name errors.
                                $input_error = true;
                            endif;                            
                        }
                        
                        ( $input_error == false )? $confirm = true :  $inputvalid = false;
                         
                        //------------ Format Error for View ------------//										
                        if (count($group_errors) > 0) {
                            $group_error_str = implode(", ", $group_errors);
                            $error .="Incomplete Or Incorrect Sponsor Group at rows [" . $group_error_str . "]<br />";
                        }

                        if (count($name_errors) > 0) {
                            $name_error_str = implode(", ", $name_errors);
                            $error .="Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
                        }
//----------------------------------------------//			
                    } else {
                        echo "no file";
                    }
                }
            } else if ($this->input->post('postback') == 'saveconfirm') {

                $datafile = $this->input->post('datafile');

                if ($datafile != '') {
                    
                    $pathtofile = $datafile;

                    if (file_exists($pathtofile)) {
                        $file_data = $this->_readExcelFile($datafile);
                        $toalRows = count($file_data);
                        
                        $firstRow = true;
                        foreach($file_data as $dataRow){
                            if($firstRow):
                                $firstRow = false;
                                continue;
                            endif;
                            
                            $externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
                            
                            $order = 0;
                            
                            $sponsorgroup = $dataRow['sponsorgroup'];
                            unset($dataRow['sponsorgroup']);
                            
                            $dataRow['eventid']         = $eventid;
                            $dataRow['order']           = $order;
                            
                            if($externalId):
                                    
                                $whrArr = array(
                                    'external_id' => $externalId,
                                    'eventid'     => $eventid
                                );
                                    
                                $row = $this->general_model->rowExists('sponsor', $whrArr, 1);                                
                                if($row->id): // external id already exists in table, update record
                                    $dataRow['sponsorgroupid']  = $this->_sponsorGroup('update', $sponsorgroup, array('eventid', $eventid), $app->defaultlanguage, $row->sponsorgroupid);
                                    $sponsorid = $this->_importExcelToDatabase('sponsor', $dataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $row->id);                                

                                else: // external id don't exists in table, insert record 
                                    $dataRow['sponsorgroupid']  = $this->_sponsorGroup('add', $sponsorgroup, array('eventid', $eventid), $app->defaultlanguage);
                                    $sponsorid = $this->_importExcelToDatabase('sponsor', $dataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);

                                endif;

                            else: // external id don't exits in excel file

                                $whrArr = array(
                                    'name'      => $dataRow['name'.$defaultLangPrefix],
                                    'eventid'   => $eventid
                                );

                                $row = $this->general_model->rowExists('sponsor', $whrArr, 1);

                                if($row->id): // external id already exists in table, update record
                                    $dataRow['sponsorgroupid']  = $this->_sponsorGroup('update', $sponsorgroup, array('eventid', $eventid), $app->defaultlanguage, $row->sponsorgroupid);
                                    $sponsorid = $this->_importExcelToDatabase('sponsor', $dataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $row->id);                                

                                else: // external id don't exists in table, insert record 
                                    $dataRow['sponsorgroupid']  = $this->_sponsorGroup('add', $sponsorgroup, array('eventid', $eventid), $app->defaultlanguage);
                                    $sponsorid = $this->_importExcelToDatabase('sponsor', $dataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);

                                endif;

                            endif; // End if external id exists in excel file                      
                        
                        }
                        
                        unlink($pathtofile); //Remove Excel File

                        $this->session->set_flashdata('event_feedback', 'The sponsors has successfully been added!');
                        _updateTimeStamp($eventid);
                        redirect('sponsors/event/' . $eventid);
                    } else {
                        echo "no file";
                    }
                }
            } else if ($this->input->post('postback') == 'cancelimport') {

                $datafile = $this->input->post('datafile');

                if ($datafile != '') {
                    $pathtofile = $datafile;

                    if (file_exists($pathtofile)) {
                        unlink($pathtofile);
                    }
                }

                $this->session->set_flashdata('event_feedback', 'The sponsors import has been cancelled!');
                _updateTimeStamp($eventid);
                redirect('sponsors/event/' . $eventid);
            }

            if ($confirm == true && $inputvalid == true) {
                $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
            } else {
                if ($datafile != '') {
                    $pathtofile = $datafile;

                    if (file_exists($pathtofile)) {
                        unlink($pathtofile);
                    }
                }
                $cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
            }

            $cdata['crumb'] = array("Events" => "events", $event->name => "event/view/" . $event->id, "Sponsors" => "sponsors/event/" . $event->id, "Excel import" => "import/sponsors/" . $event->id . "/" . $type);
            $cdata['sidebar'] = $this->load->view('c_sidebar', array('event' => $event), TRUE);
            $this->load->view('master', $cdata);
        } else {

//--------------------- For Venuee ------------------------//

            $venueid = $id;
            $datafile = '';
            $view_data = '';
            $confirm = false;
            $inputvalid = true;
            //$template_file = $this->config->item('publicupload') . 'excelfiles/templates/sponsors.xls';
            $template_file = 'import/download/sponsor';
            $template_example = $this->config->item('publicupload') . 'excelfiles/examples/sponsors.png';
            $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

            $this->load->model('general_model');

            $venue = $this->venue_model->getById($venueid);
            $app = _actionAllowed($venue, 'venue', 'returnApp');
            $languages = $this->language_model->getLanguagesOfApp($app->id);
            
            $totalLang = count($languages);
            $defaultLangPrefix = '';
            
            if($totalLang > 1)
                $defaultLangPrefix = '_'.$app->defaultlanguage;
            
            $this->load->library('excel'); // Load PHPExcel Library
            
            if ($this->input->post('postback') == 'postback') {
//Uploads excel
                if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $venueid)) {
                    mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $venueid, 0755, true);
                }

                $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $venueid;
                //$config['allowed_types'] = 'csv|xl|xls|xlsx';
                $config['allowed_types'] = '*';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                $file_type = $_FILES['excelfile']['type'];

                if (!in_array($file_type, $supported_types)) {
                    $error .="<p>The file type is not supported.</p>";
                } else if (!$this->upload->do_upload('excelfile')) {

                    if ($_FILES['excelfile']['name'] != '') {
                        $error .= $this->upload->display_errors();
                    }
                } else {

//successfully uploaded
                    $returndata = $this->upload->data();

                    $headers = TRUE;
                    $heading = array();

                    $row = 1;
                    $error = '';

                    $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $venueid . "/" . $returndata['file_name'];

                    $datafile = $pathtofile; //important

                    if (file_exists($pathtofile)) {
                        
                        $view_data = $this->_readExcelFile($datafile);
                        $toalRows = count($view_data);
                        
                        $group_errors = array();
                        $name_errors = array();
                        $input_error = false;

                        for($k=1; $k<$toalRows; $k++){
                            if(!$view_data[$k]['name'.$defaultLangPrefix]):
                                $name_errors[] = $k + 1; //add to array of name errors.
                                $input_error = true;
                            endif;
                            if(!$view_data[$k]['sponsorgroup']):
                                $group_errors[] = $k + 1; //add to array of name errors.
                                $input_error = true;
                            endif;     
                        }
                        
                        ( $input_error == false )? $confirm = true :  $inputvalid = false;
                         
                        //------------ Format Error for View ------------//										
                        if (count($group_errors) > 0) {
                            $group_error_str = implode(", ", $group_errors);
                            $error .="Incomplete Or Incorrect Sponsor Group at rows [" . $group_error_str . "]<br />";
                        }

                        if (count($name_errors) > 0) {
                            $name_error_str = implode(", ", $name_errors);
                            $error .="Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
                        }
                        
//----------------------------------------------//			
                    } else {
                        echo "no file";
                    }
                }
            } else if ($this->input->post('postback') == 'saveconfirm') {

                $datafile = $this->input->post('datafile');

                if ($datafile != '') {
                    
                    $pathtofile = $datafile;

                    if (file_exists($pathtofile)) {
                        
                        $file_data = $this->_readExcelFile($datafile);
                        $toalRows = count($file_data);
                        
                        $firstRow = true;
                        foreach($file_data as $dataRow){
                            if($firstRow):
                                $firstRow = false;
                                continue;
                            endif;
                        
                            $externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
                            
                            $order = 0;
                            
                            $sponsorgroup = $dataRow['sponsorgroup'];
                            unset($dataRow['sponsorgroup']);
                            
                            $dataRow['venueid']         = $venueid;
                            $dataRow['order']           = $order;
                            
                            if($externalId):
                                    
                                $whrArr = array(
                                    'external_id' => $externalId,
                                    'venueid'     => $venueid
                                );
                                    
                                $row = $this->general_model->rowExists('sponsor', $whrArr, 1);                                
                                if($row->id): // external id already exists in table, update record
                                    $dataRow['sponsorgroupid']  = $this->_sponsorGroup('update', $sponsorgroup, array('venueid', $venueid), $app->defaultlanguage, $row->sponsorgroupid);
                                    $sponsorid = $this->_importExcelToDatabase('sponsor', $dataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $row->id);                                

                                else: // external id don't exists in table, insert record 
                                    $dataRow['sponsorgroupid']  = $this->_sponsorGroup('add', $sponsorgroup, array('venueid', $venueid), $app->defaultlanguage);
                                    $sponsorid = $this->_importExcelToDatabase('sponsor', $dataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);

                                endif;

                            else: // external id don't exits in excel file

                                $whrArr = array(
                                    'name'      => $dataRow['name'.$defaultLangPrefix],
                                    'venueid'   => $venueid
                                );

                                $row = $this->general_model->rowExists('sponsor', $whrArr, 1);

                                if($row->id): // external id already exists in table, update record
                                    $dataRow['sponsorgroupid']  = $this->_sponsorGroup('update', $sponsorgroup, array('venueid', $venueid), $app->defaultlanguage, $row->sponsorgroupid);
                                    $sponsorid = $this->_importExcelToDatabase('sponsor', $dataRow, 'update', $languages, array('name' => 'name', 'description' => 'description'), $app, $row->id);                                

                                else: // external id don't exists in table, insert record 
                                    $dataRow['sponsorgroupid']  = $this->_sponsorGroup('add', $sponsorgroup, array('venueid', $venueid), $app->defaultlanguage);
                                    $sponsorid = $this->_importExcelToDatabase('sponsor', $dataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);

                                endif;

                            endif; // End if external id exists in excel file
                        
                        }
                        
                        unlink($pathtofile); //Remove Excel File

                        $this->session->set_flashdata('event_feedback', 'The sponsors has successfully been added!');
                        _updateTimeStamp($venueid);
                        redirect('sponsors/venue/' . $venueid);
                    } else {
                        echo "no file";
                    }
                }
            } else if ($this->input->post('postback') == 'cancelimport') {

                $datafile = $this->input->post('datafile');

                if ($datafile != '') {
                    $pathtofile = $datafile;

                    if (file_exists($pathtofile)) {
                        unlink($pathtofile);
                    }
                }

                $this->session->set_flashdata('event_feedback', 'The sponsors import has been cancelled!');
                _updateTimeStamp($venueid);
                redirect('sponsors/venue/' . $venueid);
            }

            if ($confirm == true && $inputvalid == true) {
                $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
            } else {
                if ($datafile != '') {
                    $pathtofile = $datafile;

                    if (file_exists($pathtofile)) {
                        unlink($pathtofile);
                    }
                }
                $cdata['content'] = $this->load->view('c_import_excel', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
            }

            $cdata['crumb'] = array("Venues" => "venues", $venue->name => "venue/view/" . $venue->id, "Sponsors" => "sponsors/venue/" . $venue->id, "Excel import" => "import/sponsors/" . $venue->id . "/" . $type);
            $cdata['sidebar'] = $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
            $this->load->view('master', $cdata);
        }
    }


//------------------------------- Events -------------------------------//
//----------------------------------------------------------------------//

    function events($launcherid = '') {
    	$redirect = '';
        $datafile = '';
        $view_data = '';
        $confirm = false;
        $inputvalid = true;
        //$template_file = $this->config->item('publicupload') . 'excelfiles/templates/events.xls';
		$template_file = 'import/download/events';
        $template_example = $this->config->item('publicupload') . 'excelfiles/examples/events.png';
        $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

        $this->load->model('general_model');
        $this->load->helper('email_helper');

        $app = _currentApp();
        if ($app == FALSE)
            redirect('apps');

        $languages = $this->language_model->getLanguagesOfApp($app->id);
		$totalLang = count($languages);
        
        $defaultLangPrefix = '';
        if($totalLang > 1):            
            $defaultLangPrefix = '_'.$app->defaultlanguage;        
        endif;
		
		$this->load->library('excel'); // Load PHPExcel Library
		
        if ($this->input->post('postback') == 'postback') {
        //Uploads excel
            if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id)) {
                mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id, 0755, true);
            }

            $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $app->id;
            //$config['allowed_types'] = 'csv|xl|xls|xlsx';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            //echo($_FILES['excelfile']['type']);

            $file_type = $_FILES['excelfile']['type'];

            if (!in_array($file_type, $supported_types)) {
                $error .="<p>The file type is not supported.</p>";
            } else if (!$this->upload->do_upload('excelfile')) {

                if ($_FILES['excelfile']['name'] != '') {
                    $error .= $this->upload->display_errors();
                }
            } else {

            //successfully uploaded
                $returndata = $this->upload->data();

                $headers = TRUE;
                $heading = array();

                $row = 1;
                $error = '';

                $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $app->id . "/" . $returndata['file_name'];

                $datafile = $pathtofile; //important

                if (file_exists($pathtofile)) {

                    $view_data = $this->_readExcelFile($datafile);
                    $toalRows = count($view_data);
					
                    $name_errors = array();
                    $dats_errors = array();
                    $email_errors = array();
                    $end_date_errors = array();
                    $input_error = false;
					
					for($k=1; $k<$toalRows; $k++){
                        if(!$view_data[$k]['name'.$defaultLangPrefix]):
                            $name_errors[] = $k + 1; //add to array of name errors.
                            $input_error = true;
                        endif;
						if($view_data[$k]['datefrom']):
							$start_datetime = $this->set_date($view_data[$k]['datefrom']);
						else:
							$start_datetime = '1970-01-01';
                        endif;
						if($view_data[$k]['dateto']!=''):
							$end_datetime = $this->set_date($view_data[$k]['dateto']);
						else:
							$end_datetime = '1970-01-01';
                        endif;
						
						if (!$start_datetime || !$end_datetime):
							$dats_errors[] = $k + 1;
                            $input_error = true;
						else:
							$startdate = strtotime($start_datetime);
                            $enddate = strtotime($end_datetime);
							if ($enddate < $startdate):
                            	$end_date_errors[] = $k + 1;
                                $input_error = true;
                            endif;
                        endif;
						
						if(trim($view_data[$k]['email'])):
                            if (!$this->valid_email(trim($view_data[$k]['email']))) {
                                $email_errors[] = $k + 1; //add to array of email errors.
                                $input_error = true;
                            }
                        endif;
                    }
					
					( $input_error == false )? $confirm = true :  $inputvalid = false;
					
					//------------ Format Error for View ------------//

                    if (count($name_errors) > 0) {
                        $name_error_str = implode(", ", $name_errors);
                        $error .="Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
                    }
					if (count($dats_errors) > 0) {
                        $dats_error_str = implode(", ", $dats_errors);
                        $error .="Incomplete Or Incorrect Dates at rows [" . $dats_error_str . "]<br />";
                    }

                    if (count($end_date_errors) > 0) {
                        $end_date_error_str = implode(", ", $end_date_errors);
                        $error .="To Date Should be grater than From Date at rows [" . $end_date_error_str . "]<br />";
                    }
                    if (count($email_errors) > 0) {
                        $email_error_str = implode(", ", $email_errors);
                        $error .="Incorrect Email at rows [" . $email_error_str . "]<br />";
                    }
					//----------------------------------------------//
				
                } else {
                    echo "no file";
                }
            }
        } else if ($this->input->post('postback') == 'saveconfirm') {
            $datafile = $this->input->post('datafile');
            if ($datafile != '') {
                $error = '';

                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
					$file_data = $this->_readExcelFile($datafile);
                    $toalRows = count($file_data);
                    
                    $firstRow = true;
					foreach($file_data as $dataRow){
                        if($firstRow):
                            $firstRow = false;
                            continue;
                        endif;
						$externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
						
						$dataRow['datefrom'] =  isset( $dataRow['datefrom'] ) ? $this->set_date( $dataRow['datefrom'] ) : '1970-01-01';
						$dataRow['dateto'] =  isset( $dataRow['dateto'] ) ? $this->set_date( $dataRow['dateto'] ) : '1970-01-01';
						
						$vdata['eventid'] = 0;
						$vdata['name'] = $dataRow['location_name'];
						$vdata['address'] = $dataRow['location_address'];
						$vdata['active'] = 1;
						
						$venueid = 0;
						
						$vQry = "SELECT venue.id as id FROM venue, appvenue WHERE venue.name = '" . $vdata['name'] . "' AND  appvenue.appid = " . $app->id . " AND venue.id = appvenue.venueid LIMIT 1";
						$res = $this->db->query($vQry);
						if( $res->num_rows() > 0 ) $venueid = $res->row()->id;
						
						if( ! $venueid )
							$venueid = $this->_importExcelToDatabase('venue', $vdata, 'add', $languages, array('name' => 'name'), $app);
						
						if( $venueid ){
							unset( $dataRow['location_name'] );
							unset( $dataRow['location_address'] );
							
							$dataRow['organizerid'] = _currentUser()->id;
							$dataRow['venueid'] = $venueid;
							$dataRow['phonenr'] = $dataRow['telephone'];
							unset( $dataRow['telephone'] );
							$dataRow['ticketlink'] = checkhttp($dataRow['ticketingurl']);
							unset($dataRow['ticketingurl']);
							$dataRow['website'] = checkhttp($dataRow['website']);
							$dataRow['eventtypeid'] = 3;
							$dataRow['active'] = 1;
							        
                            $eventid = $this->_importExcelToDatabase('event', $dataRow, 'add', $languages, array('name' => 'name', 'description' => 'description'), $app);
							
							$this->app_model->addEventToApp($app->id, $eventid);
                            if (_currentUser()->id == 1) {
								//$this->app_model->addEventToApp(1, $eventid); //Add to XpoPlus App!
                            } else {
                             	$this->app_model->addEventToApp(2, $eventid); //Add to TAPCROWD App!
                            }
							
							//insert info launcher
							$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE id = 3")->row();

							$launcherdata = array(
								'moduletypeid' => 21,
								'eventid' => $eventid,
								'module' => $defaultlauncher->module,
								'title' => $defaultlauncher->title,
								'icon' => $defaultlauncher->icon,
								'order' => $defaultlauncher->order,
								'active' => 1
							);
							$this->db->insert('launcher', $launcherdata);
							
							$groupdata = array(
								'appid' => $app->id,
                                'eventid' => $eventid,
                                'name' => 'exhibitorcategories',
                                'parentid' => 0
							);
                            $categoriegroupid = $this->general_model->insert('group', $groupdata);

                            $groupdata2 = array(
                            	'appid' => $app->id,
                                'eventid' => $eventid,
                                'name' => 'exhibitorbrands',
                                'parentid' => 0
                                );
							$brandgroupid = $this->general_model->insert('group', $groupdata2);
							
							if(!empty($launcherid)) {
								$launcher = $this->module_mdl->getLauncherById($launcherid);
								$this->general_model->insert('tag', array(
										'appid' => $app->id,
										'tag' => $launcher->tag,
										'eventid' => $eventid
										));
								
								$redirect = 'events/launcher/'.$launcherid;
							}
							
						}
						
					}
                    

                    unlink($pathtofile); //Remove Excel File

                    $this->session->set_flashdata('event_feedback', 'The event has successfully been added!');
                    _updateTimeStamp($app->id);
                    if(isset($redirect) && !empty($redirect)) {
                    	redirect($redirect);
                    }
                    redirect('events/');
                } else {
                    echo "no file";
                }
            }
        } else if ($this->input->post('postback') == 'cancelimport') {

            $datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }

            $this->session->set_flashdata('event_feedback', 'The event import has been cancelled!');
            _updateTimeStamp($app->id);
            redirect('events/');
        }
        if ($confirm == true && $inputvalid == true) {
            $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
        } else {
            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }
            $cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
        }

        $cdata['crumb'] = array("Events" => "events", "Excel import" => "import/events/");
        $cdata['sidebar'] = $this->load->view('c_sidebar', array('event' => $event), TRUE);
        $this->load->view('master', $cdata);
    }

//------------------------------- Venues -------------------------------//
//----------------------------------------------------------------------//

    function venues($launcherid = '') {
    	$geo_service = new \Tapcrowd\Models\GeoLocationService;
        $datafile = '';
        $view_data = '';
        $confirm = false;
        $inputvalid = true;
        //$template_file = $this->config->item('publicupload') . 'excelfiles/templates/pois.xls';
        $template_file = 'import/download/pois';
        $template_example = $this->config->item('publicupload') . 'excelfiles/examples/pois.png';
        $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

        $this->load->model('general_model');
        $this->load->helper('email_helper');

        $app = _currentApp();
        if ($app == FALSE)
            redirect('apps');
        
        $languages = $this->language_model->getLanguagesOfApp($app->id);
        $totalLang = count($languages);
        
        $defaultLangPrefix = '';
        if($totalLang > 1):            
            $defaultLangPrefix = '_'.$app->defaultlanguage;        
        endif;
        
        $this->load->library('excel'); // Load PHPExcel Library
        
        $venues = $this->venue_model->getAppVenue($app->id);
		
		if ($app->subdomain != "" && $venues != false) {
            $this->iframeurl = "venues/app/" . $app->id;
        }

        $apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND venueid <> 0 GROUP BY tag");
        if ($apptags->num_rows() == 0) {
            $apptags = array();
        } else {
            $apptags = $apptags->result();
        }
        
        if ($this->input->post('postback') == 'postback') {
//Uploads excel
            if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id)) {
                mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id, 0755, true);
            }

            $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $app->id;
            //$config['allowed_types'] = 'csv|xl|xls|xlsx';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

//echo($_FILES['excelfile']['type']);

            $file_type = $_FILES['excelfile']['type'];

            if (!in_array($file_type, $supported_types)) {
                $error .="<p>The file type is not supported.</p>";
            } else if (!$this->upload->do_upload('excelfile')) {

                if ($_FILES['excelfile']['name'] != '') {
                    $error .= $this->upload->display_errors();
                }
            } else {

//successfully uploaded
                $returndata = $this->upload->data();

                $headers = TRUE;
                $heading = array();

                $row = 1;
                $error = '';

                $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $app->id . "/" . $returndata['file_name'];

                $datafile = $pathtofile; //important

                if (file_exists($pathtofile)) {
                    $view_data = $this->_readExcelFile($datafile);
                    $toalRows = count($view_data);
                    
                    $name_errors = array();
                    $email_errors = array();
                    $input_error = false;
                    
                    for($k=1; $k<$toalRows; $k++){
                        if(!$view_data[$k]['name'.$defaultLangPrefix]):
                            $name_errors[] = $k + 1; //add to array of name errors.
                            $input_error = true;
                        endif;
                        if(trim($view_data[$k]['email'])):
                            if (!$this->valid_email(trim($view_data[$k]['email']))) {
                                $email_errors[] = $k + 1; //add to array of email errors.
                                $input_error = true;
                            }
                        endif;
                    }
                        
                    ( $input_error == false )? $confirm = true :  $inputvalid = false;
                    
                    //------------ Format Error for View ------------//

                    if (count($name_errors) > 0) {
                        $name_error_str = implode(", ", $name_errors);
                        $error .="Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
                    }

                    if (count($email_errors) > 0) {
                        $email_error_str = implode(", ", $email_errors);
                        $error .="Incorrect Email at rows [" . $email_error_str . "]<br />";
                    }

                    //----------------------------------------------//	
                } else {
                    echo "no file";
                }
            }
        } else if ($this->input->post('postback') == 'saveconfirm') {

            $datafile = $this->input->post('datafile');
            
            $this->load->library('GoogleGeocode');
            $geo = new GoogleGeocode();
            $redirect = '';
            
            if ($datafile != '') {
                
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    $file_data = $this->_readExcelFile($datafile);
					$geo_service->getCoordinatesForLocations($file_data, 'address', 'latitude', 'longitude', 2);
                    $toalRows = count($file_data);
                    
                    $firstRow = true;
                    foreach($file_data as $dataRow){
                        if($firstRow):
                            $firstRow = false;
                            continue;
                        endif;
                        
                        $externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
                        $dataRow['active'] =  1;
                        $dataRow['timestamp'] =  time();
                        
						/*
                        if(empty($data['lon']) || empty($data['lat'])):
                            $res = $geo->geocode($dataRow['address']);
                            $dataRow['lon'] = isset($res['Placemarks'][0]['Longitude']) ? $res['Placemarks'][0]['Longitude'] : '';
                            $dataRow['lat'] = isset($res['Placemarks'][0]['Latitude']) ? $res['Placemarks'][0]['Latitude'] : '';
                        else:
                            $dataRow['lon'] = $dataRow['longitude'];
                            $dataRow['lat'] = $dataRow['latitude'];                            
                        endif;
                        */
						$dataRow['lat'] = isset( $dataRow['latitude'] ) ? $dataRow['latitude'] : '';
						$dataRow['lon'] = isset( $dataRow['longitude'] ) ? $dataRow['longitude'] : '';
						if(isset($dataRow['longitude'])) {
							unset($dataRow['longitude']);
							unset($dataRow['latitude']);
						}
							
						if( !empty( $dataRow['lat'] ) && !empty( $dataRow['lng'] ) ){
							$dataRow['isgeocoded'] = 1;
							
							$this->general_model->insert('geocoding_cache', array(
								'address' => $dataRow['address'],
								'lat' => $dataRow['lat'],
								'lng' => $dataRow['lon']
								)
							);
						}
						
						unset($dataRow['longitude(optional)']);
						unset($dataRow['latitude(optional)']);
							
                        $tags = $dataRow['tag'];
                        unset($dataRow['tag']);
                        
                        $isUpdate = FALSE;
						
						 // external id exists in excel file
                        if($externalId):
                            
							$updIds = $this->general_model->PoisExists($app->id, 'external_id', $externalId);
							
							if($updIds[0]->id): // external id already exists in table, update record
                                $isUpdate = TRUE;
								$dataRow['deleted']	= 0;
                                $venueid = $this->_importExcelToDatabase('venue', $dataRow, 'update', $languages, array('name' => 'name', 'info' => 'info'), $app, $updIds[0]->id);
                                    
                            else: // external id don't exists in table, insert record 
        
                                $venueid = $this->_importExcelToDatabase('venue', $dataRow, 'add', $languages, array('name' => 'name', 'info' => 'info'), $app);
								$this->general_model->insert('module', array('venue' => $venueid, 'moduletype' => 21));
                            
                            endif;
                                    
                        else: // external id don't exits in excel file
                                    
                            $updIds = $this->general_model->PoisExists($app->id, 'name', $dataRow['name'.$defaultLangPrefix]);
							
							if(count($updIds)>0): // external id already exists in table, update record
								$isUpdate = TRUE;
								$dataRow['deleted']	= 0;
								foreach($updIds as $updId)
                                	$venueid = $this->_importExcelToDatabase('venue', $dataRow, 'update', $languages, array('name' => 'name', 'info' => 'info'), $app, $updId->id);

                            else: // external id don't exists in table, insert record 

                                $venueid = $this->_importExcelToDatabase('venue', $dataRow, 'add', $languages, array('name' => 'name', 'info' => 'info'), $app);
								$this->general_model->insert('module', array('venue' => $venueid, 'moduletype' => 21));

                            endif;
                        
                            endif; // End if external id exists in excel file
						
						if ($venueid && !$isUpdate) {
                            //insert info launcher
                            $defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE id = 3")->row();
                            $launcherdata = array(
                                'moduletypeid'  => 21,
                                'venueid'       => $venueid,
                                'module'        => $defaultlauncher->module,
                                'title'         => $defaultlauncher->title,
                                'icon'          => $defaultlauncher->icon,
                                'order'         => $defaultlauncher->order,
                                'active'        => 1
                            );
                            $this->db->insert('launcher', $launcherdata);

                            //save main categorie and brands group for exhibitors
                            $groupdata = array(
                                'appid'     => $app->id,
                                'venueid'   => $venueid,
                                'name'      => 'exhibitorcategories',
                                'parentid'  => 0
                            );
                            $categoriegroupid = $this->general_model->insert('group', $groupdata);
							
							
							$this->general_model->insert('appvenue', array('venueid' => $venueid, "appid" => $app->id));
							
						}
						
						// *** SAVE TAGS *** //
						if($isUpdate){
							foreach($updIds as $venue){
								if(!empty($venue->id)) {
								$this->db->where('venueid', $venue->id);
								$this->db->where('appid', $app->id);
								if ($this->db->delete('tag')) {
									// Code to Insert tag was here initially;                            
									if ($tags != '') {
										$tagsArray = explode(",", $tags);
											if ($tagsArray != '' && count($tagsArray) > 0) {
												foreach ($tagsArray as $tag) {
													$tData = array(
														'appid'     => $app->id,
														'tag'       => $tag,
														'venueid'   => $venue->id
													);
													$this->db->insert('tag', $tData);
												}// foreach ($tagsArray as $tag)
											} // if ($tagsArray != '' && count($tagsArray) > 0)
										}
									} // if ($this->db->delete('tag'))
								}
							} // foreach($updIds as $venueid)
						}
						else
						{
							if(!empty($venueid)) {
								$this->db->where('venueid', $venueid);
								$this->db->where('appid', $app->id);
								if ($this->db->delete('tag')) {
									// Code to Insert tag was here initially;                            
									if ($tags != '') {
										$tagsArray = explode(",", $tags);
										if ($tagsArray != '' && count($tagsArray) > 0) {
											foreach ($tagsArray as $tag) {
												$tData = array(
													'appid'     => $app->id,
													'tag'       => $tag,
													'venueid'   => $venueid
												);
												$this->db->insert('tag', $tData);
											}// foreach ($tagsArray as $tag)
										} // if ($tagsArray != '' && count($tagsArray) > 0)
									}
								} // if ($this->db->delete('tag'))
							}
						}

						if(!empty($launcherid)) {
							$launcher = $this->module_mdl->getLauncherById($launcherid);
							$this->general_model->insert('tag', array(
									'appid' => $app->id,
									'tag' => $launcher->tag,
									'venueid' => $venueid
								));

							$redirect = 'venues/launcher/'.$launcherid;
						}

					}
                }
				
                unlink($pathtofile); //Remove Excel File

                $this->session->set_flashdata('event_feedback', 'The venues has successfully been added!');
                _updateTimeStamp($app->id);
                if(isset($redirect) && !empty($redirect)) {
                	redirect($redirect);
                }
                redirect('venues/');
            } else {
                    echo "no file";
            }
        }
        else if ($this->input->post('postback') == 'cancelimport') {

            $datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }

            $this->session->set_flashdata('event_feedback', 'The event import has been cancelled!');
            _updateTimeStamp($app->id);
            redirect('venues/');
        }
        if ($confirm == true && $inputvalid == true) {
            $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
        } else {
            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }
            $cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
        }

        $cdata['crumb'] = array("Venues" => "venues", "Excel import" => "import/venues/");
        $cdata['sidebar'] = $this->load->view('c_sidebar', array('venue' => $event), TRUE);
        $this->load->view('master', $cdata);
    }

//------------------------------- Places -------------------------------//
//----------------------------------------------------------------------//
function places($parentType, $parentId) {		
		$datafile = '';
        $view_data = '';
        $confirm = false;
        $inputvalid = true;
       
        $template_file = 'import/download/places';
        $template_example = $this->config->item('publicupload') . 'excelfiles/examples/places.png';
        $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

        $this->load->model('general_model');
		$this->load->model('exhibitor_model');
        $this->load->helper('email_helper');
		
		$geo_service = new \Tapcrowd\Models\GeoLocationService;
		
        $app = _currentApp();
        if ($app == FALSE)
            redirect('apps');
        
        $languages = $this->language_model->getLanguagesOfApp($app->id);
        $totalLang = count($languages);
        
        $defaultLangPrefix = '';
        if($totalLang > 1):            
            $defaultLangPrefix = '_'.$app->defaultlanguage;        
        endif;
		
        $this->load->library('excel'); // Load PHPExcel Library
        if( $parentType == 'event' ):
			$event = $this->event_model->getbyid($parentId);
		elseif( $parentType == 'venue' ):
			$venue = $this->venue_model->getById($parentId);
		endif;
        
        if ($this->input->post('postback') == 'postback') {
			
			//Uploads excel
            if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id)) {
                mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id, 0755, true);
            }

            $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $app->id;
            //$config['allowed_types'] = 'csv|xl|xls|xlsx';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $file_type = $_FILES['excelfile']['type'];

            if (!in_array($file_type, $supported_types)) {
                $error .="<p>The file type is not supported.</p>";
            } else if (!$this->upload->do_upload('excelfile')) {
                if ($_FILES['excelfile']['name'] != '') {
                    $error .= $this->upload->display_errors();
                }
            } else {
				//successfully uploaded
                $returndata = $this->upload->data();

                $headers = TRUE;
                $heading = array();

                $row = 1;
                $error = '';

                $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $app->id . "/" . $returndata['file_name'];

                $datafile = $pathtofile; //important

                if (file_exists($pathtofile)) {
                    $view_data = $this->_readExcelFile($datafile);
                    $toalRows = count($view_data);
                    
                    $name_errors = array();
                    $input_error = false;
                    
                    for($k=1; $k<$toalRows; $k++){
                        if(!$view_data[$k]['title'.$defaultLangPrefix]):
                            $name_errors[] = $k + 1; //add to array of name errors.
                            $input_error = true;
                        endif;                        
                    }
                        
                    ( $input_error == false )? $confirm = true :  $inputvalid = false;
                    
                    //------------ Format Error for View ------------//

                    if (count($name_errors) > 0) {
                        $name_error_str = implode(", ", $name_errors);
                        $error .="Incomplete Or Incorrect Title at rows [" . $name_error_str . "]<br />";
                    }
					                  
                } else {
                    echo "no file";
                }
            }
        } else if ($this->input->post('postback') == 'saveconfirm') {
			
			$translationArray = array('title' => 'title', 'info' => 'info');

            $datafile = $this->input->post('datafile');
            
            $this->load->library('GoogleGeocode');
            $geo = new GoogleGeocode();
            $redirect = 'places/'.$parentType.'/'.$parentId;
            
            if ($datafile != '') {
                
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
					$file_data = $this->_readExcelFile($datafile);
					$geo_service->getCoordinatesForLocations($file_data, 'address', 'latitude(optional)', 'longitude(optional)', 2);
                    $toalRows = count($file_data);
                    
					$categoriesgroupid = $this->exhibitor_model->getPlacesMainCategorieGroup($parentId, $parentType);
					if ( $categoriesgroupid ){
						$firstRow = true;
						$catIndex = 0;
						foreach($file_data as $dataRow){
							if($firstRow):
                                foreach($dataRow as $key=>$val){
                                    if(strstr($key, 'category'))
                                        $catIndex++;
                                }
                                $firstRow = false;
                                continue;
                            endif;
							
							$catDataRow = $dataRow;
                            for($cind = 1; $cind <=$catIndex; $cind++){                                    
                                unset($dataRow['category'.$cind]);
                            }
							
							$externalId = isset( $dataRow['external_id'] ) ? trim($dataRow['external_id']) : '';
							$dataRow['appId'] =  $app->id;
							$dataRow['status'] =  'active';
							$dataRow['parentType'] =  $parentType;
							$dataRow['parentId'] =  $parentId;                        
							$dataRow['addr'] = trim( $dataRow['address'] );
							unset($dataRow['address']);
							/*
							if(empty($dataRow['latitude(optional)']) || empty($dataRow['longitude(optional)'])):
								/*
								$res = $geo->geocode($dataRow['addr']);
								$dataRow['lat'] = isset($res['Placemarks'][0]['Latitude']) ? $res['Placemarks'][0]['Latitude'] : '';
								$dataRow['lng'] = isset($res['Placemarks'][0]['Longitude']) ? $res['Placemarks'][0]['Longitude'] : '';
								*/
								/*
								$geo_json	= file_get_contents( "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode( $dataRow['addr'] ) . "&sensor=false" ); // get json content
								$json_res 	= json_decode($geo_json, true); //json decoder
								$dataRow['lat'] = $json_res['results'][0]['geometry']['location']['lat'];
								$dataRow['lng'] = $json_res['results'][0]['geometry']['location']['lng'];
							else:
								$dataRow['lat'] = $dataRow['latitude(optional)'];
								$dataRow['lng'] = $dataRow['longitude(optional)'];								
							endif;
							*/
							
							$dataRow['lat'] = isset( $dataRow['latitude(optional)'] ) ? $dataRow['latitude(optional)'] : '';
							$dataRow['lng'] = isset( $dataRow['longitude(optional)'] ) ? $dataRow['longitude(optional)'] : '';
							
							if( !empty( $dataRow['lat'] ) && !empty( $dataRow['lng'] ) ){
								$dataRow['isgeocoded'] = 1;
								
								$this->general_model->insert('geocoding_cache', array(
															'address' => $dataRow['addr'],
															'lat' => $dataRow['lat'],
															'lng' => $dataRow['lng']
															)
														);
							}
							
							unset($dataRow['longitude(optional)']);
							unset($dataRow['latitude(optional)']);
							
							$isUpdate = FALSE;
							
							// external id exists in excel file
							if($externalId):
								$row = $this->general_model->rowExists('tc_places', array('external_id' => $externalId, 'appId' => $app->id, 'parentType' => $parentType, 'parentId' => $parentId), 1);
								
								if($row->id): // external id already exists in table, update record
									$isUpdate = TRUE;
									$placeid = $this->_importExcelToDatabase('tc_places', $dataRow, 'update', $languages, $translationArray, $app, $row->id);                            
								else: // external id don't exists in table, insert record         
									$placeid = $this->_importExcelToDatabase('tc_places', $dataRow, 'add', $languages, $translationArray, $app);							
								endif;
							
							else: // external id don't exits in excel file
										
								$row = $this->general_model->rowExists('tc_places', array('title' => $dataRow['title'.$defaultLangPrefix], 'appId' => $app->id, 'parentType' => $parentType, 'parentId' => $parentId), 1);
								if($row->id): // external id already exists in table, update record
									$isUpdate = TRUE;
									$placeid = $this->_importExcelToDatabase('tc_places', $dataRow, 'update', $languages, $translationArray, $app, $row->id);
								else: // external id don't exists in table, insert record 
									$placeid = $this->_importExcelToDatabase('tc_places', $dataRow, 'add', $languages, $translationArray, $app);							
								endif;
							
							endif; // End if external id exists in excel file
							
							// Removing groupitems for current exhibitor
							$whrClzCol = ( $parentType == 'event' ) ? 'eventid' : 'venueid';
                            $this->db->query("DELETE FROM `groupitem` WHERE `appid` = '" . $app->id . "' AND `" . $whrClzCol . "` ='" . $parentId . "' AND `itemtable` = 'tc_places' AND `itemid` = '" . $placeid . "'");
                            
                            // Start: Inserting Groups / Categories
                            $groupitem_parentid = $categoriesgroupid;
                                
                            for($cind = 1; $cind <=$catIndex; $cind++){                                    
                                $gpName = trim($catDataRow['category'.$cind]);
                                
                                if($gpName){
                                    $groupExists = $this->db->query("SELECT * FROM `group` WHERE `appid` = '" . $app->id . "' AND `parentid` ='" . $groupitem_parentid . "' AND `name` = ? LIMIT 1", $gpName);

                                    if ($groupExists->num_rows() > 0) {
                                        $groupid = $groupExists->row()->id;
                                            
                                        if ($groupid) {
                                                
                                            $groupItemExists = $this->db->query("SELECT * FROM `groupitem` WHERE `itemid` = '".$placeid."' AND `itemtable` = 'tc_places' AND `appid` = '" . $app->id . "' AND `".$whrClzCol."` ='" . $parentId . "' AND `groupid` = ? LIMIT 1", $groupid);

                                            if($groupItemExists->num_rows() == 0)
                                            {
                                                // Inserting Group Items
                                                $giData = array();
                                                $giData['appid']        = $app->id;
                                                $giData[$whrClzCol]		= $parentId;
                                                $giData['groupid']      = $groupid;
                                                $giData['itemtable']    = 'tc_places';
                                                $giData['itemid']       = $placeid;

                                                $this->general_model->insert('groupitem', $giData);
                                            }
                                        } // if ($groupid)
                                    } else {
                                        //insert ist group/category

                                        $gpData = array();
                                        $gpData['appid']    = $app->id; 
                                        $giData[$whrClzCol]	= $parentId;
                                        $gpData['name']     = $gpName;
                                        $gpData['parentid'] = $groupitem_parentid;
                                        $gpData['tree']     = 0;

                                        $groupid = $this->general_model->insert('group', $gpData);

                                        if ($groupid) {
                                            $this->language_model->addTranslation('group', $groupid, 'name', $app->defaultlanguage, $gpName);

                                            // Inserting Group Items
                                            $giData = array();
                                            $giData['appid']        = $app->id;
                                            $giData[$whrClzCol]		= $parentId;
                                            $giData['groupid']      = $groupid;
                                            $giData['itemtable']    = 'tc_places';
                                            $giData['itemid']       = $placeid;

                                            $this->general_model->insert('groupitem', $giData);

                                       } // if ($groupid)
                                    }
                                        
                                }// End: if($gpName)
                                    
                            } // End: for($cind = 1; $cind <=$catIndex; $cind++)
                                
                            // End: Inserting Groups / Categories
											
						}
					}
                }
				
                unlink($pathtofile); //Remove Excel File
				
                $this->session->set_flashdata('event_feedback', 'The places have successfully been added!');
                _updateTimeStamp($app->id);
                if(isset($redirect) && !empty($redirect)) {
                	redirect($redirect);
                }
                redirect('places/'.$parentType.'/'.$parentId);
            } else {
                    echo "no file";
            }
        }
        else if ($this->input->post('postback') == 'cancelimport') {
			
			$datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }

            $this->session->set_flashdata('event_feedback', 'The place import has been cancelled!');
            _updateTimeStamp($app->id);
            redirect('places/'.$parentType.'/'.$parentId);
        }
        if ($confirm == true && $inputvalid == true) {
            $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
        } else {
			
            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }
            $cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
        }
		if( $parentType == 'event' ):
			$cdata['crumb'] = array($event->name => "event/view/" . $event->id, 'Places' => 'places/'.$parentType.'/'.$parentId, 'Excel import' => 'import/places/'.$parentType.'/'.$parentId);
		elseif( $parentType == 'venue' ):
			$cdata['crumb'] = array($venue->name => 'venues', 'Places' => 'places/'.$parentType.'/'.$parentId, 'Excel import' => 'import/places/'.$parentType.'/'.$parentId);
		endif;
		
        $cdata['sidebar'] = $this->load->view('c_sidebar', array('venue' => $event), TRUE);
        $this->load->view('master', $cdata);
    }


//------------------------------- Users -------------------------------//
//----------------------------------------------------------------------//
function users($parentType, $parentId) {
		$datafile = '';
        $view_data = '';
        $confirm = false;
        $inputvalid = true;
       
        $template_file = 'import/download/users';
        // $template_example = $this->config->item('publicupload') . 'excelfiles/examples/users.png';
        $template_example = '';
        $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

        $this->load->model('general_model');
		$this->load->model('users_model');
		$this->load->helper('email');
		
		$app = _currentApp();
        if ($app == FALSE)
            redirect('apps');
        
        $languages = $this->language_model->getLanguagesOfApp($app->id);
		$totalLang = count($languages);
        $defaultLangPostfix = '';
        if($totalLang > 1)
            $defaultLangPostfix = '_'.$app->defaultlanguage;
        
        $this->load->library('excel'); // Load PHPExcel Library
        if( $parentType == 'event' ):
			$event = $this->event_model->getbyid($parentId);
		elseif( $parentType == 'venue' ):
			$venue = $this->venue_model->getById($parentId);
		endif;
        
        if ($this->input->post('postback') == 'postback') {
			
			//Uploads excel
            if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id)) {
                mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id, 0755, true);
            }

            $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $app->id;
            //$config['allowed_types'] = 'csv|xl|xls|xlsx';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $file_type = $_FILES['excelfile']['type'];

            if (!in_array($file_type, $supported_types)) {
                $error .="<p>The file type is not supported.</p>";
            } else if (!$this->upload->do_upload('excelfile')) {
                if ($_FILES['excelfile']['name'] != '') {
                    $error .= $this->upload->display_errors();
                }
            } else {
				//successfully uploaded
                $returndata = $this->upload->data();

                $headers = TRUE;
                $heading = array();

                $row = 1;
                $error = '';

                $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $app->id . "/" . $returndata['file_name'];

                $datafile = $pathtofile; //important

                if (file_exists($pathtofile)) {
                    $view_data = $this->_readExcelFile($datafile);
                    $toalRows = count($view_data);
                    
                    $name_errors = array();
					$login_errors = array();
					$password_errors = array();
					$email_errors = array();
					$attd_name_errors = array();
					$attd_firstname_errors = array();
					$attd_email_errors = array();
                    $input_error = false;
                    
                    for($k=1; $k<$toalRows; $k++){
                        if(!$view_data[$k]['name']):
                            $name_errors[] = $k + 1; 
                            $input_error = true;
                        endif;
						if(!$view_data[$k]['login']):
                            $login_errors[] = $k + 1; 
                            $input_error = true;
                        endif;
						if(!$view_data[$k]['password']):
                            $password_errors[] = $k + 1;
                            $input_error = true;
                        endif;
						if(!$view_data[$k]['email']):
                            $email_errors[] = $k + 1;
                            $input_error = true;
						elseif (!$this->valid_email($view_data[$k]['email'])):
							$email_errors[] = $k + 1;
                            $input_error = true;
                        endif;
						if( $app->apptypeid == 3 && strtolower($view_data[$k]['create_attendee']) == 'yes' ):
							if(!$view_data[$k]['attendee_name']):
								$attd_name_errors[] = $k + 1; 
								$input_error = true;
							endif;
							if(!$view_data[$k]['attendee_firstname']):
								$attd_firstname_errors[] = $k + 1; 
								$input_error = true;
							endif;
							if(!$view_data[$k]['attendee_email']):
								$attd_email_errors[] = $k + 1;
								$input_error = true;
							elseif (!$this->valid_email($view_data[$k]['attendee_email'])):
								$attd_email_errors[] = $k + 1;
								$input_error = true;
							endif;
						endif;
                    }
                        
                    ( $input_error == false )? $confirm = true :  $inputvalid = false;
                    
                    //------------ Format Error for View ------------//

                    if (count($name_errors) > 0) {
                        $name_error_str = implode(", ", $name_errors);
                        $error .= "Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
                    }
					if (count($login_errors) > 0) {
                        $name_error_str = implode(", ", $login_errors);
                        $error .= "Incomplete Or Incorrect Login at rows [" . $name_error_str . "]<br />";
                    }
					if (count($password_errors) > 0) {
                        $name_error_str = implode(", ", $password_errors);
                        $error .= "Incomplete Or Incorrect Password at rows [" . $name_error_str . "]<br />";
                    }
					if (count($email_errors) > 0) {
                        $name_error_str = implode(", ", $email_errors);
                        $error .= "Incomplete Or Incorrect Email at rows [" . $name_error_str . "]<br />";
                    }
					if (count($attd_name_errors) > 0) { 
                        $name_error_str = implode(", ", $attd_name_errors);
                        $error .= "Incomplete Or Incorrect Attendee Name at rows [" . $name_error_str . "]<br />";
                    }
					if (count($attd_firstname_errors) > 0) {
                        $name_error_str = implode(", ", $attd_firstname_errors);
                        $error .= "Incomplete Or Incorrect Attendee First Name at rows [" . $name_error_str . "]<br />";
                    }
					if (count($attd_email_errors) > 0) {
                        $name_error_str = implode(", ", $attd_email_errors);
                        $error .= "Incomplete Or Incorrect Attendee Email at rows [" . $name_error_str . "]<br />";
                    }
					                  
                } else {
                    echo "no file";
                }
            }
		} else if ($this->input->post('postback') == 'saveconfirm') {
			$datafile = $this->input->post('datafile');
            
            $redirect = 'users/'.$parentType.'/'.$parentId;
            
            if ($datafile != '') {
                
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
					$file_data = $this->_readExcelFile($datafile);
					$toalRows = count($file_data);
                    
					$firstRow = true;
					foreach($file_data as $dataRow){
						if($firstRow):
                        	$firstRow = false;
                            continue;
                        endif;
						
						$userData = array(
										'name' => $dataRow['name'],
										'login' => $dataRow['login'],
										'password' => $dataRow['password'],
										'email' => $dataRow['email'],
										);
						
						$updUserId = 0;
						$updUserId = $this->users_model->checkUserExists($app->id, $userData['login'], $parentType, $parentId);
						$userId = 0;
						
						if( $updUserId ):
							unset( $userData['login'] );
							$userId = $this->users_model->updateUser($updUserId, $app->id, $userData, $parentType, $parentId);
						else:
							$userId = $this->users_model->insertUser($app->id, $userData, $parentType, $parentId);
						endif;
						
						if( $app->apptypeid == 3 && strtolower( $dataRow['create_attendee'] ) == 'yes' ):
							$facebook = !empty( $dataRow['attendee_facebook'] ) ? checkhttp( $dataRow['attendee_facebook'] ) : '';
							$twitter = !empty( $dataRow['attendee_twitter'] ) ? checkhttp( $dataRow['attendee_twitter'] ) : '';
						
							$this->session->set_flashdata('user_create_attendee', 'Yes');
							$attendeeData = array(
										'name' => $dataRow['attendee_name'],
										'firstname' => $dataRow['attendee_firstname'],
										'company' => $dataRow['attendee_company'],
										'country' => $dataRow['attendee_country'],
										'function' => $dataRow['attendee_function'],
										'email' => $dataRow['attendee_email'],
										'phonenr' => $dataRow['attendee_phone'],
										'description'.$defaultLangPostfix => $dataRow['attendee_description'.$defaultLangPostfix],
										'linkedin' => checkhttp( $dataRow['attendee_public_linkedin_url'] )
										);
							foreach($languages as $language) {                            
								$attendeeData['description_'.$language->key] = $dataRow['attendee_description_'.$language->key];
							}
							
							if( $parentType == 'event')
								$attendeeData['eventid'] = $parentId;
							else
								$attendeeData['venueid'] = $parentId;
							
							$userRow = $this->general_model->rowExists('user', array('id' => $userId), 1);
							if( count( $userRow ) > 0 &&  $userRow->attendeeid ){
								$attendeesid = $this->_importExcelToDatabase('attendees', $attendeeData, 'update', $languages, array('description' => 'description'), $app, $userRow->attendeeid);
							}else{
								$attendeesid = $this->_importExcelToDatabase('attendees', $attendeeData, 'add', $languages, array('description' => 'description'), $app);
								$this->general_model->update('user', $userId, array('attendeeid' => $attendeesid));
							}
							
							
							/*--------------Social Media Information--------------------*/
						if($twitter){
							$metadata_social = $this->general_model->rowExists('metadata', array('appid' => $app->id, 'table' => 'attendees', 'identifier' => $attendeesid, 'key' => 'twitter'), 1);
							if( count( $metadata_social ) > 0 ){
								$this->general_model->update('metadata', $metadata_social->id, array(
									'value'         => $twitter
								));
							}else{
								$this->general_model->insert('metadata', array(
									'appid'         => $app->id,
									'table'         => 'attendees',
									'identifier'    => $attendeesid,
									'key'           => 'twitter',
									'value'         => $facebook
								));
							}
						}// End if($twitter)
										
						if($facebook){
							$metadata_social = $this->general_model->rowExists('metadata', array('appid' => $app->id, 'table' => 'attendees', 'identifier' => $attendeesid, 'key' => 'facebook'), 1);
							if( count( $metadata_social ) > 0 ){
								$this->general_model->update('metadata', $metadata_social->id, array(
									'value'         => $twitter
								));
							}else{
								$this->general_model->insert('metadata', array(
									'appid'         => $app->id,
									'table'         => 'attendees',
									'identifier'    => $attendeesid,
									'key'           => 'facebook',
									'value'         => $facebook
								));
							}
						}// End if($facebook)
							
							
							
						endif; // if( $dataRow['create_attendee'] == 'Yes' )
					}
				
                }
				unlink($pathtofile); //Remove Excel File
				
                $this->session->set_flashdata('event_feedback', 'The users have successfully been added!');
                _updateTimeStamp($app->id);
                if(isset($redirect) && !empty($redirect)) {
                	redirect($redirect);
                }
                redirect('users/'.$parentType.'/'.$parentId);
            } else {
                    echo "no file";
            }
        }
        else if ($this->input->post('postback') == 'cancelimport') {
			
			$datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }

            $this->session->set_flashdata('event_feedback', 'The user import has been cancelled!');
            _updateTimeStamp($app->id);
            redirect('users/'.$parentType.'/'.$parentId);
        }
        if ($confirm == true && $inputvalid == true) {
            $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
        } else {
			
            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }
            $cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
        }
		if( $parentType == 'event' ):
			$cdata['crumb'] = array($event->name => "event/view/" . $event->id, 'Users' => 'users/'.$parentType.'/'.$parentId, 'Excel import' => 'import/places/'.$parentType.'/'.$parentId);
		elseif( $parentType == 'venue' ):
			$cdata['crumb'] = array($venue->name => 'venues', 'Users' => 'Users/'.$parentType.'/'.$parentId, 'Excel import' => 'import/places/'.$parentType.'/'.$parentId);
		endif;
		
        $cdata['sidebar'] = $this->load->view('c_sidebar', array('venue' => $event), TRUE);
        $this->load->view('master', $cdata);
    }

//----------------------------------------------------------------------//


function _catalogGroup($app, $venueid, $parentid, $totalCatagories, $catDataRow){
	
	for($cind = 1; $cind <=$totalCatagories; $cind++){                                    
    	$gpName = trim($catDataRow['category'.$cind]);
        if( $gpName ){
        	$groupExists = $this->db->query("SELECT * FROM `group` WHERE `appid` = '" . $app->id . "' AND `parentid` ='" . $parentid . "' AND `name` = ? LIMIT 1", $gpName);
			if ($groupExists->num_rows() > 0) {
            	$parentid = $groupExists->row()->id;
			}else{
				$gpData = array();
           		$gpData['appid']    = $app->id; 
                $gpData['venueid']  = $venueid;
                $gpData['name']     = $gpName;
                $gpData['parentid'] = $parentid;
                $gpData['tree']     = 0;

                $groupid = $this->general_model->insert('group', $gpData);
				if ( $groupid ) {
                	$this->language_model->addTranslation('group', $groupid, 'name', $app->defaultlanguage, $gpName);
					$parentid = $groupid;
				}
			}
		}// End: if($gpName)
                   
	} // End: for($cind = 1; $cind <=$catIndex; $cind++)

	return $parentid;
}
//----------------------------- Catalog -----------------------------//
//----------------------------------------------------------------------//

    function catalog($venueid, $Group_Id) {
        
        $datafile = '';
        $view_data = '';
        $confirm = false;
        $inputvalid = true;        
        
        $template_file = 'import/download/catalog/'.$venueid; // Create Template file dynamically
        
        $template_example = $this->config->item('publicupload') . 'excelfiles/examples/catalog.png';
        $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

        $this->load->model('general_model');
        $this->load->model('catalog_model');
        $this->load->helper('email_helper');
		
		$this->load->model('module_mdl');
		$this->load->model('metadata_model');
		$launcher = $this->module_mdl->getLauncher(15, 'venue', $venueid);
		$metaFields = $this->metadata_model->getFieldsByParent('launcher', $launcher->id);
		$forbidTypes = array('pdf', 'image', 'markericon');
		$langMetaFieldTypes = array('text', 'header');
		$addLangPostFixToMetaData = TRUE;
		
        $venue = $this->venue_model->getById($venueid);
        
        $app = _actionAllowed($venue, 'venue', 'returnApp');
        $languages = $this->language_model->getLanguagesOfApp($app->id);
        
        $totalLang = count($languages);
        $defaultLangPostfix = '';
        if($totalLang > 1)
            $defaultLangPostfix = '_'.$app->defaultlanguage;
		else $addLangPostFixToMetaData = FALSE;
        
        
        $this->load->library('excel'); // Load PHPExcel Library
        
        if ($this->input->post('postback') == 'postback') {

		//Uploads excel
            if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $venueid)) {
                mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $venueid, 0755, true);
            }
            
            $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $venueid;
            //$config['allowed_types'] = 'csv|xl|xls|xlsx';
            $config['allowed_types'] = '*'; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $file_type = $_FILES['excelfile']['type'];

//echo($file_type); //To see the type..

            if (!in_array($file_type, $supported_types)) {
                $error .="<p>The file type is not supported.</p>";
            } else if (!$this->upload->do_upload('excelfile')) {

                if ($_FILES['excelfile']['name'] != '') {
                    $error .= $this->upload->display_errors();
                }
            } else {

//successfully uploaded
                $returndata = $this->upload->data();

                $headers = TRUE;
                $heading = array();

                $row = 1;
                $error = '';

                $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $venueid . "/" . $returndata['file_name'];

                $datafile = $pathtofile; //important

                if (file_exists($pathtofile)) {
                    
                    $view_data = $this->_readExcelFile($datafile);
                    
                    $name_errors = array();
                    $email_errors = array();
                    $input_error = false;
                    $toalRows = count($view_data);
                    for($k=1; $k<$toalRows; $k++){
                        if(!$view_data[$k]['name'.$defaultLangPostfix]):
                            $name_errors[] = $k + 1; //add to array of name errors.
                            $input_error = true;
                        endif;
                    }
                    //------------ Format Error for View ------------//
                    if (count($name_errors) > 0) {
                        $name_error_str = implode(", ", $name_errors);
                        $error .="Incomplete Or Incorrect Name at rows [" . $name_error_str . "]<br />";
                    }

                    ( $input_error == false )? $confirm = true :  $inputvalid = false;                    
                    
                    //----------------------------------------------//
                } else {
                    echo "no file";
                }
            }
        } else if ($this->input->post('postback') == 'saveconfirm') {

            $datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $error = '';

                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
					
					$languageFields = array(
									'name' => 'name',
									'description' => 'description',
									'urltitle' => 'urltitle',
									'url' => 'url');
					
                    $file_data = $this->_readExcelFile($datafile);
                    $toalRows = count($file_data);
                    
                    if( $Group_Id > 0 ):
						$catalogcategoriesgroupid = $Group_Id;
                    else:
						$catalogcategoriesgroupid = $this->catalog_model->getMainCategorieGroup($venueid);
					endif;
                    
					if ( $error == '' && $catalogcategoriesgroupid ) {
                        $firstRow = true;
                        $catIndex = 0;
						$metaKeys = array();
                        foreach($file_data as $dataRow){							
                            if($firstRow):
                                foreach($dataRow as $key=>$val){
                                    if(strstr($key, 'category'))
                                        $catIndex++;
                                	
									if(strstr($key, 'metadata:'))
										$metaKeys[] = $key;
								}
                                $firstRow = false;
                                continue;
                            endif;
							
                            $dataRow['externalid'] = $dataRow['external_id'];
							unset($dataRow['external_id']);
							
                            $externalId = isset( $dataRow['externalid'] ) ? trim($dataRow['externalid']) : '';
                            $dataRow['venueid'] = $venueid;
                            
                            $catDataRow = $dataRow;
                            for($cind = 1; $cind <=$catIndex; $cind++){                                    
                                unset($dataRow['category'.$cind]);
                            }
							
							$metaDataFields = array();
							foreach( $metaKeys as $metakey ){
								$new_key = str_replace('metadata:', 'meta_catalog_', $metakey);
								$metaDataFields[$new_key] = $dataRow[$metakey];
								unset( $dataRow[$metakey] );
							}
							
							// external id exists in excel file
                            if($externalId):                                    
                                $whrArr = array(
                                    'externalid' => $externalId,
                                    'venueid'     => $venueid
                                );
                                    
                                $updId = $this->general_model->rowExists('catalog', $whrArr);
                                    
                                if($updId): // external id already exists in table, update record
                                        
                                    $catalogid = $this->_importExcelToDatabase('catalog', $dataRow, 'update', $languages, $languageFields, $app, $updId);
                                    
                                 else: // external id don't exists in table, insert record 
                                        
                                    $catalogid = $this->_importExcelToDatabase('catalog', $dataRow, 'add', $languages, $languageFields, $app);
                                        
                                 endif;
                                    
                            else: // external id don't exits in excel file
                                    
                                $whrArr = array(
                                    'name'      => $dataRow['name'.$defaultLangPostfix],
                                    'venueid'   => $venueid
                                );
                                    
                                $updId = $this->general_model->rowExists('catalog', $whrArr);
                                    
                                if($updId): // external id already exists in table, update record
                                        
                                    $catalogid = $this->_importExcelToDatabase('catalog', $dataRow, 'update', $languages, $languageFields, $app, $updId);
                                    
                                else: // external id don't exists in table, insert record 
                                        
                                    $catalogid = $this->_importExcelToDatabase('catalog', $dataRow, 'add', $languages, $languageFields, $app);
                                        
                                endif;
                                    
                                    
                            endif;
                            
                            $groupid = $this->_catalogGroup($app, $venueid, $catalogcategoriesgroupid, $catIndex, $catDataRow);
                            $groupItemExists = $this->db->query("SELECT id FROM `groupitem` WHERE `appid` = '" . $app->id . "' AND `venueid` = ".$venueid." AND `groupid` ='" . $groupid . "' AND `itemtable` = 'Catalog' and `itemid` = ".$catalogid." LIMIT 1");
								if ($groupItemExists->num_rows() == 0) {
									 // Inserting Group Items
									 $giData = array();
									 $giData['appid']        = $app->id;
									 $giData['venueid']      = $venueid;
									 $giData['groupid']      = $groupid;
									 $giData['itemtable']    = 'Catalog';
									 $giData['itemid']       = $catalogid;

									 $this->general_model->insert('groupitem', $giData);
								}
							
							// -- MetaData
							foreach($metaFields as $field){
								$mData = array();
								if( ! in_array( $field->type, $forbidTypes ) ){
									if( in_array( $field->type, $langMetaFieldTypes ) ){
										$langPostFix = '';
										foreach( $languages as $language ){
											if( $addLangPostFixToMetaData )
												$langPostFix = '_' . $language->key;
											if( $metaDataFields[$field->qname . $langPostFix] ):
												$metaValueRow = $this->db->query("SELECT metaId FROM `tc_metavalues` WHERE `metaId` = '" . $field->id . "' AND `parentId` = ".$catalogid." AND `language` ='" . $language->key . "' AND `parentType` = 'catalog' LIMIT 1");
												if( $metaValueRow->num_rows() > 0 ){
													$mData = array();
													$whr = array();
													
													$mData['value']	= $metaDataFields[$field->qname . $langPostFix];
													
													$whr['metaId']      = $field->id;
													$whr['parentType']	= 'catalog';
													$whr['parentId']    = $catalogid;
													$whr['type']      	= $field->type;
													$whr['language']    = $language->key;
													
													$this->metadata_model->updateMetaDataValues($mData, $whr);
													
												}else{
													$mData = array();
													$mData['metaId']        = $field->id;
													$mData['parentType']	= 'catalog';
													$mData['parentId']      = $catalogid;
													$mData['name']      	= $field->name;
													$mData['type']      	= $field->type;
													$mData['value']      	= $metaDataFields[$field->qname . $langPostFix];
													$mData['language']      = $language->key;
													
													$this->general_model->insert('tc_metavalues', $mData);
												}
											endif;
										}
									}else if($metaDataFields[$field->qname]){
										$metaValueRow = $this->db->query("SELECT metaId FROM `tc_metavalues` WHERE `metaId` = '" . $field->id . "' AND `parentId` = ".$catalogid." AND `language` ='" . $app->defaultlanguage . "' AND `parentType` = 'catalog' LIMIT 1");
										if( $metaValueRow->num_rows() > 0 ){
											$mData = array();
											$whr = array();
												
											$mData['value']	= $metaDataFields[$field->qname];
												
											$whr['metaId']      = $field->id;
											$whr['parentType']	= 'catalog';
											$whr['parentId']    = $catalogid;
											$whr['type']      	= $field->type;
											$whr['language']    = $app->defaultlanguage;
												
											$this->metadata_model->updateMetaDataValues($mData, $whr);
												
										}else{
											$mData = array();
											$mData['metaId']        = $field->id;
											$mData['parentType']	= 'catalog';
											$mData['parentId']      = $catalogid;
											$mData['name']      	= $field->name;
											$mData['type']      	= $field->type;
											$mData['value']      	= $metaDataFields[$field->qname];
											$mData['language']      = $app->defaultlanguage;
											
											$this->general_model->insert('tc_metavalues', $mData);
										}
									}
								}
							}
							
						} // foreach($file_data[$k] as $dataRow)
                            
                    } // if ($error == '' && $catalogcategoriesgroupid != '')
                    
                    unlink($pathtofile); //Remove Excel File

                    $this->session->set_flashdata('event_feedback', 'The catalog has successfully been added!');
                    _updateTimeStamp($eventid);
                    redirect('catalog/venue/' . $venueid);
                } else {
                    echo "no file";
                }
            }
        } else if ($this->input->post('postback') == 'cancelimport') {

            $datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }

            $this->session->set_flashdata('event_feedback', 'The catalog import has been cancelled!');
            _updateTimeStamp($venueid);
            redirect('catalog/venue/'.$venueid);
        }
        if ($confirm == true && $inputvalid == true) {
            $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('venue' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
        } else {
            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }
            $cdata['content'] = $this->load->view('c_import_excel', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
        }
        $cdata['crumb'] = array("Venues" => "venue", $venue->name => "venue/view/" . $venue->id, "Catalog" => "catalog/venue/" . $venue->id, "Excel import" => "import/catalog/" . $venue->id);
        $cdata['sidebar'] = $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
        $this->load->view('master', $cdata);
    }




    function set_datetime($date_value) {
        $date = '';
        $dateTime = explode(" ", $date_value);


        if (count($dateTime) < 2) {
            return false;
        }

        $newrow['Date'] = $dateTime[0];
        $newrow['Time'] = $dateTime[1];

        if (stristr($newrow['Date'], '-')) {
//date with -
            $datearray = explode('-', $newrow['Date']);
            if (strlen($datearray[0]) == 4) {
                $date = $datearray[0] . '-' . $datearray[1] . '-' . $datearray[2];
            } elseif (strlen($datearray[2]) == 4) {
                $date = $datearray[2] . '-' . $datearray[1] . '-' . $datearray[0];
            }
        } elseif (stristr($newrow['Date'], '/')) {
//date with /
            $datearray = explode('/', $newrow['Date']);
            if (strlen($datearray[0]) == 4) {
                $date = $datearray[0] . '-' . $datearray[1] . '-' . $datearray[2];
            } elseif (strlen($datearray[2]) == 4) {
                $date = $datearray[2] . '-' . $datearray[0] . '-' . $datearray[1];
            }
        } elseif (stristr($newrow['Date'], ' ')) {
//date with spaces
            $newrow['Date'] = strtolower($newrow['Date']);
            if ($timestamp = strtotime($newrow['Date'])) {
                $date = date('Y-m-d', $timestamp);
            }
        } else {
            return false;
        }

        $datetime = $date . " " . $newrow['Time'];
        return $datetime;
    }

    function set_date($date_value) {
        $date = '';
		$newrow['Date'] = $date_value;
		
		if (stristr($newrow['Date'], '-')) {
//date with -
            $datearray = explode('-', $newrow['Date']);
            if (strlen($datearray[0]) == 4) {
                $date = $datearray[0] . '-' . $datearray[1] . '-' . $datearray[2];
            } elseif (strlen($datearray[2]) == 4) {
                $date = $datearray[2] . '-' . $datearray[1] . '-' . $datearray[0];
            }
        } elseif (stristr($newrow['Date'], '/')) {
//date with /
            $datearray = explode('/', $newrow['Date']);
            if (strlen($datearray[0]) == 4) {
                $date = $datearray[0] . '-' . $datearray[1] . '-' . $datearray[2];
            } elseif (strlen($datearray[2]) == 4) {
                $date = $datearray[2] . '-' . $datearray[1] . '-' . $datearray[0];
            }
        } elseif (stristr($newrow['Date'], ' ')) {
//date with spaces
            $newrow['Date'] = strtolower($newrow['Date']);
            if ($timestamp = strtotime($newrow['Date'])) {
                $date = date('Y-m-d', $timestamp);
            }
        } else {
            return false;
        }

        return $date;
    }
    
    function download($type, $event_venue_id = 0){
        
        $app = _currentApp();
        if ($app == FALSE)
            redirect('apps');
        
        $addSocialMediaFields = 0;
		$addAttendeeFields = 0;
        if($app->apptypeid == 10):
            $addSocialMediaFields = 1;
		elseif($app->apptypeid == 3):
			$addAttendeeFields = 1;
        endif;
        
        $fieldsArray = array();
        $fileName = '';
		$multisheets = 0;
        
        switch($type){
            
            case 'attendee':
                $fieldsArray = array(
                'external_id'   => '',
                'name'          => 'Attendee X',
                'firstname'     => 'First Name X',
                'company'       => 'Company X',
				'country'		=> 'BE',
                'function'      => 'Function X',
                'email'         => 'info@tabcrowd.com',
                'phone'         => '3292980192',
                '_description'  => 'Here comes the description of the attendee',
                'public_linkedin_url' => 'http://www.linkedin.com/in/profilename',
				'facebook'		=> 'http://www.facebook.com/username',
				'twitter'		=> 'http://www.twitter.com/username'
                );
                    
                $fileName = 'Attendees';
                
            break;
            
            case 'exhibitor':
                $fieldsArray = array(
                'external_id'    => '',
                '_name'         => 'Exhibitor X',
                'booth'         => 'B001',
                '_description'  => 'Here comes the description of the exhibitor',
                'tel'           => '3292980192',                
                'address'       => 'grauwpoort 1, 9000 gent',
                'email'         => 'info@tabcrowd.com',
                'web'           => 'www.tapcrowd.com',                
                'category1'     => 'Mobile',
                'category2'     => 'Mobile Applications',
                'category3'     => '',
                'category4'     => '',
                'category5'     => '',
				'category6'     => '',
				'category7'     => '',
				'category8'     => '',
				'category9'     => '',
				'category10'     => '',
				'category11'     => '',
				'category12'     => '',
				'category13'     => '',
				'category14'     => '',
				'category15'     => '',
				'category16'     => '',
				'category17'     => '',
				'category18'     => '',
				'category19'     => '',
				'category20'     => ''
                );
                
                $fileName = 'Exhibitors';
                
            break;
            
            case 'sponsor':
                $fieldsArray = array(
                'external_id'   => '',
                'sponsorgroup'  => 'Group X',
                '_name'         => 'Sponsor X',
                '_description'  => 'Here comes the description of the sponsor',
                'website'       => 'www.tapcrowd.com'                
                );
                
                $fileName = 'Sponsors';
                
            break;
        
            case 'sessions':
                $fieldsArray = array('sessions' => array(
														array('external_id' => '1', 'sessiongroup_name'	=> '5-Nov','_session_name' => 'Opening', '_description' => '','starttime' => '31-10-2012 14:00','endtime' => '31-10-2012 14:30', 'location' => 'Room 1', 'url' => 'www.tapcrowd.com'),
														array('external_id' => '2', 'sessiongroup_name'	=> '5-Nov','_session_name' => 'Introduction', '_description' => 'Keynote','starttime' => '31-10-2012 14:30','endtime' => '31-10-2012 15:00', 'location' => 'Room 1', 'url' => 'www.tapcrowd.com'),
														array('external_id' => '3', 'sessiongroup_name'	=> '5-Nov','_session_name' => 'The future of mobile', '_description' => 'The future of mobile …','starttime' => '31-10-2012 15:00','endtime' => '31-10-2012 15:50', 'location' => 'Room 1', 'url' => ''),
														array('external_id' => '4', 'sessiongroup_name'	=> '5-Nov','_session_name' => 'Coffee break', '_description' => '','starttime' => '31-10-2012 15:50','endtime' => '31-10-2012 16:00', 'location' => 'Room 1', 'url' => 'www.tapcrowd.com'),
														array('external_id' => '5', 'sessiongroup_name'	=> '5-Nov','_session_name' => 'Mobile solutions', '_description' => 'Keynote: AR, QR-CODES, …','starttime' => '31-10-2012 16:00','endtime' => '31-10-2012 17:00', 'location' => 'Room 1', 'url' => ''),
														array('external_id' => '6', 'sessiongroup_name'	=> '5-Nov','_session_name' => 'Q&A', '_description' => '','starttime' => '31-10-2012 17:00','endtime' => '31-10-2012 17:30', 'location' => 'Room 1', 'url' => '')
													),
									 'speakers' => array(
									 					array('external_id' => '1', '_speaker_name' => 'Jan Raman', 'speaker_company' => 'TapCrowd NV', 'speaker_function' => 'Project Manager', '_speaker_bio_description' => 'Jan joined TapCrowd in….', 'session_id'=> '2'),
														array('external_id' => '2', '_speaker_name' => 'Tim Vermeulen', 'speaker_company' => 'TapCrowd NV', 'speaker_function' => 'Managing partner', '_speaker_bio_description' => '', 'session_id'=> '2,3'),
														array('external_id' => '3', '_speaker_name' => 'Peter Heyman', 'speaker_company' => 'Heyman NV', 'speaker_function' => 'Art director', '_speaker_bio_description' => '', 'session_id'=> '5'),
														array('external_id' => '4', '_speaker_name' => 'Timmy Hendrix', 'speaker_company' => 'Oxynade NV', 'speaker_function' => 'Managing partner', '_speaker_bio_description' => '', 'session_id'=> '5'),
														array('external_id' => '5', '_speaker_name' => 'Emma Daems', 'speaker_company' => 'Oxynade NV', 'speaker_function' => 'Managing partner', '_speaker_bio_description' => '', 'session_id'=> '6')
													)
                				);
                
                $fileName = 'Sessions';
                $multisheets = 1;
                
                if($addSocialMediaFields == 1){
                    $fieldsArray = array(
					'external_id'	=> '',
                    'sessiongroup'  => '31-10-2012',
                    '_name'         => 'Session Name X',
                    '_description'  => 'Here comes the description of the session',
                    'starttime'     => '31-10-2012 14:00',
                    'endtime'       => '31-10-2012 15:00',
                    'location'      => 'Location X',
                    'url'           => 'www.tapcrowd.com',
                    'twitter'       => 'http://www.twitter.com/tapcrowd',
                    'facebook'      => 'http://www.facebook.com/tapcrowd',
                    'youtube'       => 'http://www.youtube.com/tapcrowd'
                    );
                    
                    $fileName = 'Festival';
					$multisheets = 0;
                }
                
            break;
            
            case 'pois':
                $fieldsArray = array(
				'external_id'	=> '',
                '_name'     	=> 'POI X',
                'address'   	=> 'Address X',
                'telephone' 	=> '3292980192',
                'fax'       	=> '3292980192',
                'email'     	=> 'info@tapcrowd.com',
                'website'   	=> 'www.tapcrowd.com',
                '_info'     	=> 'Info X',
                'tag'       	=> 'tag1,tag2,tag3',
                'longitude' 	=> '',
                'latitude'  	=> ''
                );
                
                $fileName = 'Pois';
                
            break;
        	
			case 'events':
                $fieldsArray = array(
				'_name'				=> 'Event X2',
                'datefrom'			=> '02-05-2012',
                'dateto'			=> '02-05-2012',
                '_description'		=> 'Here comes the description of the event',
				'telephone'     	=> '3292980192',
                'email'				=> 'info@tapcrowd.com',
                'website'			=> 'www.tapcrowd.com',
                'ticketingurl'      => 'www.tapcrowd.com',
                'location_name'		=> 'Venue X',
                'location_address'	=> 'Address X'
                );
                
                $fileName = 'Events';
                
            break;
			
			case 'places':
                $fieldsArray = array(
				'external_id'			=> '',
				'_title'				=> 'Place X',
                '_info'					=> 'Info about the Place X',
				'address'     			=> 'grauwpoort 1 gent',
                'latitude(optional)'	=> '2.33823',
                'longitude(optional)'	=> '50.09093',
				'category1'     		=> 'Shop',
                'category2'     		=> 'Resto',
                'category3'     		=> '',
                'category4'     		=> '',
                'category5'     		=> '',
				'category6'     		=> '',
				'category7'     		=> '',
				'category8'     		=> '',
				'category9'     		=> '',
				'category10'     		=> '',
				'category11'     		=> '',
				'category12'     		=> '',
				'category13'     		=> '',
				'category14'     		=> '',
				'category15'     		=> '',
				'category16'     		=> '',
				'category17'    		=> '',
				'category18'     		=> '',
				'category19'     		=> '',
				'category20'     		=> ''
                );
			
				$fileName = 'Places';
			
			break;
			
			case 'users':
                $fieldsArray = array(
				'name'		=> 'User Name X',
				'login'		=> 'login.name',
                'password'	=> 'Pa$$w0rd123',
				'email'		=> 'login.name@tapcrowd.com'
				);
				
				if( $addAttendeeFields ):
					$fieldsArray['create_attendee']		= 'Yes/No (If yes, attendee_name, attendee_firstname, and attendee_email are mandatory.)';
					$fieldsArray['attendee_name']		= 'Attendee X';
					$fieldsArray['attendee_firstname']	= 'First Name X';
					$fieldsArray['attendee_company']	= 'Company X';
					$fieldsArray['attendee_country']	= 'BE';
					$fieldsArray['attendee_function']	= 'Function X';
					$fieldsArray['attendee_email']		= 'info@tabcrowd.com';
					$fieldsArray['attendee_phone']		= '3292980192';
					$fieldsArray['_attendee_description'] = 'Here comes the description of the attendee';
					$fieldsArray['attendee_public_linkedin_url'] = 'http://www.linkedin.com/in/profilename';
					$fieldsArray['attendee_facebook']	= 'http://www.facebook.com/username';
					$fieldsArray['attendee_twitter']	= 'http://www.twitter.com/username';
				endif;
				
				$fileName = 'Users';
			
			break;
			
			case 'catalog':
				$fieldsArray = array(
                'external_id'   => '',
                '_name'         => 'Catalog Item X',
                '_description'  => 'Here comes the description of the catalog',
				'_urltitle'     => 'Title X',
                '_url'          => 'http://www.tapcrowd.com',                
                'category1'     => 'Mobile',
                'category2'     => 'Mobile Applications',
                'category3'     => '',
                'category4'     => '',
                'category5'     => '',
				'category6'     => '',
				'category7'     => '',
				'category8'     => '',
				'category9'     => '',
				'category10'    => ''
                );
                
				// -- Geting metadata fields
				$this->load->model('metadata_model');
				$this->load->model('module_mdl');
				$launcher = $this->module_mdl->getLauncher(15, 'venue', $event_venue_id);
				$metaDataFields = $this->metadata_model->getFieldsByParent('launcher', $launcher->id);
				$forbidTypes = array('pdf', 'image', 'markericon');
				$multiLingualTypes = array('text', 'header');
				
				foreach( $metaDataFields as $metadata ):
					if( ! in_array($metadata->type, $forbidTypes ) ):
						$key = str_replace('meta_catalog_', 'metadata:', $metadata->qname);
						if( in_array( $metadata->type, $multiLingualTypes )  ) $key = '_'.$key;
						
						
						if( $metadata->type == 'location' ):
							$fieldsArray[$key] = '51.05972449999999,3.7235209000000395';
						else:
							$fieldsArray[$key] = '';
						endif;
						
						
					endif;
				endforeach;
				$fileName = 'Catalogs';
            
			break;
			
            default:
                redirect('apps');
        }
        
        
        $this->_exportExcelTemplate($app, $fieldsArray, $fileName, $multisheets);
        
    }
    
    function _exportExcelTemplate($app, $excelfieldarray, $modulename, $multisheets = 0){
        
        $filename = $modulename.'.xls'; //save our workbook as this file name
        
        $languages = $this->language_model->getLanguagesOfApp($app->id); // Get all languages
        $totallang = count($languages);
        
        //load our PHPExcel library
        $this->load->library('excel');
		
        if($multisheets){
			$sheetIndex = 0;			
			foreach( $excelfieldarray as $sheetName => $fieldsArray ){
				//activate worksheet number 1
				if( $sheetIndex > 0 )
					$this->excel->createSheet();
					
				$this->excel->setActiveSheetIndex($sheetIndex);
				//name the worksheet
				$this->excel->getActiveSheet()->setTitle($sheetName);
				$sheetIndex++;
				
				$totalRows = count ($fieldsArray);
				$rowCounter = 2;
				for($i=0; $i < $totalRows; $i++){
					$fieldletter = 'A'; // Excel header counter
					foreach($fieldsArray[$i] as $key=>$value){
						if($key[0] == '_')
						{
							if($totallang>1){
								foreach($languages as $lang){ 
									if($i == 0)
										$this->excel->getActiveSheet()->setCellValue($fieldletter . 1, substr($key, 1). '_' . $lang->key);
										
									$this->excel->getActiveSheet()->setCellValue($fieldletter . $rowCounter, $value);
			
									$fieldletter++;
								}
							}
							else{
								if($i == 0)
									$this->excel->getActiveSheet()->setCellValue($fieldletter . 1, substr($key, 1));
									
								$this->excel->getActiveSheet()->setCellValue($fieldletter . $rowCounter, $value);
								$fieldletter++;
							}
								
						}
						else
						{
							if($i == 0)
								$this->excel->getActiveSheet()->setCellValue($fieldletter . 1, $key);
							
							$this->excel->getActiveSheet()->setCellValue($fieldletter . $rowCounter, $value);
							$fieldletter++;
						}						
					}
					$rowCounter++;				
				}
			}
			$this->excel->setActiveSheetIndex(0);
		}
		else
		{
			//activate worksheet number 1
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle($modulename);
			
			$fieldletter = 'A'; // Excel header counter
			
			foreach($excelfieldarray as $key=>$value){
				
				if($key[0] == '_')
				{
					if($totallang>1){
						foreach($languages as $lang){                
							$this->excel->getActiveSheet()->setCellValue($fieldletter . '1', substr($key, 1). '_' . $lang->key);
							$this->excel->getActiveSheet()->setCellValue($fieldletter . '2', $value);
	
							$fieldletter++;
						}
					}
					else{
						$this->excel->getActiveSheet()->setCellValue($fieldletter . '1', substr($key, 1));
						$this->excel->getActiveSheet()->setCellValue($fieldletter . '2', $value);
						$fieldletter++;
					}
						
				}
				else
				{
					$this->excel->getActiveSheet()->setCellValue($fieldletter . '1', $key);
					$this->excel->getActiveSheet()->setCellValue($fieldletter . '2', $value);
					$fieldletter++;
				}
				
			}
		}
        
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        
    }
    
    function _readExcelFile($fileName){
		
		$objPHPExcel        = PHPExcel_IOFactory::load($fileName);
		$toalWorkSheets 	= $objPHPExcel->getSheetCount();
				
		$rtnArray = array();
		
		for($ws = 0; $ws < $toalWorkSheets; $ws++){
			$worksheet          = $objPHPExcel->getSheet($ws);		
			$highestRow         = $worksheet->getHighestRow();
			$highestColumn      = $worksheet->getHighestColumn();
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
	
			$rowsArray = array();
			$colsArray = array();
	
			$datetime_cols = array();
			$date_cols = array();
			
			for ($row = 1; $row <= $highestRow; ++$row){
				$curRow = array();
	
				for ($col = 0; $col < $highestColumnIndex; ++$col){
					$cell = $worksheet->getCellByColumnAndRow($col, $row);
					# Gather time indicating columns and rewrite their cell display styles.
					if($row == 1 && strpos($cell->getValue(), 'time') !== false) {
						$datetime_cols[] = $col;
						$val = $cell->getValue();
					}elseif($row == 1 && strpos($cell->getValue(), 'date') !== false) {
						$date_cols[] = $col;
						$val = $cell->getValue();
					}elseif($row != 1 && in_array($col, $datetime_cols)) {
						$worksheet->getStyle($cell->getCoordinate())->getNumberFormat()->setFormatCode('yyyy-mm-dd hh:mm:ss');
						$val = $cell->getFormattedValue();
					}elseif($row != 1 && in_array($col, $date_cols)) {
						$worksheet->getStyle($cell->getCoordinate())->getNumberFormat()->setFormatCode('yyyy-mm-dd');
						$val = $cell->getFormattedValue();
					}
					else {
						$val = $cell->getFormattedValue();
					}
					
					if($row == 1):
						$colsArray[] = $val;
						$curRow[$colsArray[$col]] = $val;
					else:
						$curRow[$colsArray[$col]] = $val;
					 endif;
				}
	
				array_push($rowsArray, $curRow);
			}
			
			if( $toalWorkSheets > 1 )
				$rtnArray[] =	$rowsArray;
			else
				$rtnArray =	$rowsArray;
						
		}
		return $rtnArray;
    }
    
	function _importExcelToDatabase($table, $row, $type, $languages, $languageFields, $app, $updId = 0){
        
        $totalLang = count($languages);
        
        $defaultLangPostfix = '';
        if($totalLang > 1) $defaultLangPostfix = '_'.$app->defaultlanguage;
        
        $data = array();
        foreach($row as $key => $value):
			if(! trim( $key )) continue;
			
            $langField = FALSE;
            foreach($languageFields as $langkey => $val):
                if(strstr($key, $langkey)):
                    $data[$langkey] = isset($row[$langkey.$defaultLangPostfix]) ? $row[$langkey.$defaultLangPostfix] : '';
                    $langField      = true;
                endif;
             endforeach;
                    
             if(!$langField)
                $data[$key] = isset( $row[$key] ) ? $row[$key] : '';
             
        endforeach;       
        switch($type){
            
            case 'add':
            
                $insertId = $this->general_model->insert($table, $data);
				
				$table = ( $table == 'tc_places' ) ? 'place' : $table;
                if ($insertId) {
                    if($totalLang > 1){
                        foreach($languageFields as $key => $val):
                            foreach($languages as $langrow){
                                if($row[$key . '_' .$langrow->key])
                                    $this->language_model->addTranslation($table, $insertId, $val, $langrow->key, $row[$key . '_' .$langrow->key]);
                             }
                                
                        endforeach; //foreach($languageFields as $key => $val)
                            
                    }
                    else{
                        foreach($languageFields as $key => $val):
                            if($row[$key])
                                $this->language_model->addTranslation($table, $insertId, $val, $app->defaultlanguage, $row[$key]);
                        endforeach; //foreach($languageFields as $key => $val)
                    }
                        
                    return $insertId;
                }
                else
                    return false;
            break;
            
            case 'update':
                
                unset($data['external_id']);
                    
                $this->general_model->update($table, $updId, $data);
                
				$table = ( $table == 'tc_places' ) ? 'place' : $table;				               
                if($totalLang > 1):
                    foreach($languageFields as $key => $val):
                        
                        foreach($languages as $language) {                            
                            $descrSuccess = $this->language_model->updateTranslation($table, $updId, $val, $language->key, $row[$key . '_' . $language->key]);
                            if($descrSuccess == false || $descrSuccess == null):
                                if($row[$key . '_' . $language->key])
                                    $this->language_model->addTranslation($table, $updId, $val, $language->key, $row[$key . '_' . $language->key]);
                            endif;
                        }
                           
                    endforeach;
                        
                else:
                    foreach($languageFields as $key => $val):
                        $descrSuccess = $this->language_model->updateTranslation($table, $updId, $val, $app->defaultlanguage, $row[$key]);						
                        if($descrSuccess == false || $descrSuccess == null):
                            if($row[$key])
                                $this->language_model->addTranslation($table, $updId, $val, $app->defaultlanguage, $row[$key]);
                        endif;
                            
                    endforeach;

                endif;
                
                return $updId;
                
            break;            
        }
    
    } // End function _importExcelToDatabase

	function valid_email($address)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $address)) ? FALSE : TRUE;
	}


//-------------------------------- Quiz --------------------------------//
//----------------------------------------------------------------------//
function quiz($parentType, $parentId) {
		$datafile = '';
        $view_data = '';
        $confirm = false;
        $inputvalid = true;
       
        $template_file = 'import/download/questions';
        $template_example = '';
        $supported_types = array("application/excel", "application/ms-excel", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.msexcel", "application/vnd.excel", "application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template", "application/octet-stream", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetheader");

        $this->load->model('general_model');
		$this->load->model('users_model');
		$this->load->helper('email');
		
		$app = _currentApp();
        if ($app == FALSE)
            redirect('apps');
        
        $languages = $this->language_model->getLanguagesOfApp($app->id);
		$totalLang = count($languages);
        $defaultLangPostfix = '';
        if($totalLang > 1)
            $defaultLangPostfix = '_'.$app->defaultlanguage;
        
        $this->load->library('excel'); 
        if( $parentType == 'event' ):
			$event = $this->event_model->getbyid($parentId);
		elseif( $parentType == 'venue' ):
			$venue = $this->venue_model->getById($parentId);
		endif;

		$quizid = $this->db->query("SELECT id FROM quiz WHERE appid = ? LIMIT 1", array($app->id))->row()->id;
        
        if ($this->input->post('postback') == 'postback') {
            if (!is_dir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id)) {
                mkdir($this->config->item('imagespath') . "upload/excelfiles/" . $app->id, 0755, true);
            }

            $config['upload_path'] = $this->config->item('imagespath') . 'upload/excelfiles/' . $app->id;
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $file_type = $_FILES['excelfile']['type'];

            if (!in_array($file_type, $supported_types)) {
                $error .="<p>The file type is not supported.</p>";
            } else if (!$this->upload->do_upload('excelfile')) {
                if ($_FILES['excelfile']['name'] != '') {
                    $error .= $this->upload->display_errors();
                }
            } else {
                $returndata = $this->upload->data();

                $headers = TRUE;
                $heading = array();

                $row = 1;
                $error = '';

                $pathtofile = $this->config->item('imagespath') . "upload/excelfiles/" . $app->id . "/" . $returndata['file_name'];

                $datafile = $pathtofile; 

                if (file_exists($pathtofile)) {
                    $view_data = $this->_readExcelFile($datafile);
                    $toalRows = count($view_data);
                    
                    $name_errors = array();
					$login_errors = array();
					$password_errors = array();
					$email_errors = array();
					$attd_name_errors = array();
					$attd_firstname_errors = array();
					$attd_email_errors = array();
                    $input_error = false;
                    
                    for($k=1; $k<$toalRows; $k++){
                        if(!$view_data[$k]['type']):
                            $type_errors[] = $k + 1; 
                            $input_error = true;
                        endif;
						if(!$view_data[$k]['text']):
                            $text_errors[] = $k + 1; 
                            $input_error = true;
                        endif;
						if(!$view_data[$k]['correct answer']):
                            $correct_answer_errors[] = $k + 1;
                            $input_error = true;
                        endif;
						if(!$view_data[$k]['score']):
                            $score_errors[] = $k + 1;
                            $input_error = true;
                        endif;
                    }
                        
                    ( $input_error == false )? $confirm = true :  $inputvalid = false;
                    
                    //------------ Format Error for View ------------//

                    if (count($type_errors) > 0) {
                        $type_error_str = implode(", ", $type_errors);
                        $error .= "Incomplete Or Incorrect Type at rows [" . $type_error_str . "]<br />";
                    }
					if (count($text_errors) > 0) {
                        $text_error_str = implode(", ", $text_errors);
                        $error .= "Incomplete Or Incorrect Text at rows [" . $text_error_str . "]<br />";
                    }
					if (count($correct_answer_errors) > 0) {
                        $correct_answer_error_str = implode(", ", $correct_answer_errors);
                        $error .= "Incomplete Or Incorrect Correct answer at rows [" . $correct_answer_error_str . "]<br />";
                    }
					if (count($score_errors) > 0) {
                        $score_error_str = implode(", ", $score_errors);
                        $error .= "Incomplete Or Incorrect Score at rows [" . $score_error_str . "]<br />";
                    }					                  
                } else {
                    echo "no file";
                }
            }
		} else if ($this->input->post('postback') == 'saveconfirm') {
			$datafile = $this->input->post('datafile');
            
            $redirect = 'quiz/'.$parentType.'/'.$parentId;
            
            if ($datafile != '') {
                $pathtofile = $datafile;
                if (file_exists($pathtofile)) {
					$file_data = $this->_readExcelFile($datafile);
					$toalRows = count($file_data);
                    
					$firstRow = true;
					foreach($file_data as $dataRow){
						if($firstRow):
                        	$firstRow = false;
                            continue;
                        endif;

                        $types = array('free text' => 1, 'multiple choice' => 2);
                        $answers = explode(';', $dataRow['options ; seperated']);
						
						$data = array(
										'quizid' => $quizid,
										'quizquestiontypeid' => $types[strtolower($dataRow['type'])],
										'questiontext' => $dataRow['text'],
										'correctanswer_score' => $dataRow['score'],
										'explanationtext' => $dataRow['explanation text']
										);

						if(!in_array($dataRow['correct answer'], $answers)) {
							$data['correctanswer'] = $dataRow['correct answer'];
						}
						
						$quizquestionid = $this->general_model->insert('quizquestion', $data);

						foreach($answers as $answer) {
							$answerid = $this->general_model->insert('quizquestionoption', array(
									'quizid' => $quizid,
									'quizquestionid' => $quizquestionid,
									'optiontext' => $answer
								));

							if($answer == $dataRow['correct answer']) {
								$this->general_model->update('quizquestion', $quizquestionid, array('correctanswer_quizquestionoptionid' => $answerid));
							}
						}
					}
                }

				unlink($pathtofile); //Remove Excel File
				
                $this->session->set_flashdata('event_feedback', 'The questions have successfully been added!');
                _updateTimeStamp($app->id);
                if(isset($redirect) && !empty($redirect)) {
                	redirect($redirect);
                }
                redirect('quiz/'.$parentType.'/'.$parentId);
            } else {
                echo "no file";
            }
        }
        else if ($this->input->post('postback') == 'cancelimport') {
			
			$datafile = $this->input->post('datafile');

            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }

            $this->session->set_flashdata('event_feedback', 'The import has been cancelled!');
            _updateTimeStamp($app->id);
            redirect('quiz/'.$parentType.'/'.$parentId);
        }
        if ($confirm == true && $inputvalid == true) {
            $cdata['content'] = $this->load->view('c_import_excel_confirmation', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'datafile' => $datafile, 'view_data' => $view_data, 'total_cols' => $totalNumberOfCols), TRUE);
        } else {
            if ($datafile != '') {
                $pathtofile = $datafile;

                if (file_exists($pathtofile)) {
                    unlink($pathtofile);
                }
            }
            $cdata['content'] = $this->load->view('c_import_excel', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'template_file' => $template_file, 'template_example' => $template_example), TRUE);
        }
		if( $parentType == 'event' ):
			$cdata['crumb'] = array($event->name => "event/view/" . $event->id, 'Quiz' => 'quiz/'.$parentType.'/'.$parentId, 'Excel import' => 'import/places/'.$parentType.'/'.$parentId);
		elseif( $parentType == 'venue' ):
			$cdata['crumb'] = array($venue->name => 'venues', 'Quiz' => 'quiz/'.$parentType.'/'.$parentId, 'Excel import' => 'import/quiz/'.$parentType.'/'.$parentId);
		endif;
		
        $cdata['sidebar'] = $this->load->view('c_sidebar', array('venue' => $event), TRUE);
        $this->load->view('master', $cdata);
    }

}