<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class dev extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function dev() {
		parent::__construct();
	}
	
	function index() {
		die('dev');
	}
	
	function getBeurs($beursid) {
		ini_set('max_execution_time', 0);
		
		$client = new soapclient($this->config->item('kxpo_url'));
		$result = $client->getBeurs(array("sPaswoord" => $this->config->item('kxpo_password'), 'iBeursID'=> $beursid));
		$xml = simplexml_load_string($result->getBeursResult);
		$xml = $xml->beurs;
		echo $result->getBeursResult;
		
		/*
		<br />
		<b>Fatal error</b>:  Uncaught SoapFault exception: [HTTP] Error Fetching http headers in C:\websites\xpoplus.com\application\controllers\dev.php:25
		Stack trace:
		#0 [internal function]: SoapClient-&gt;__doRequest('&lt;?xml version=&quot;...', 'http://catalogu...', 'http://tempuri....', 1, 0)
		#1 [internal function]: SoapClient-&gt;__call('getBeurs', Array)
		#2 C:\websites\xpoplus.com\application\controllers\dev.php(25): SoapClient-&gt;getBeurs(Array)
		#3 [internal function]: Dev-&gt;getBeurs('26')
		#4 C:\websites\xpoplus.com\system\codeigniter\CodeIgniter.php(236): call_user_func_array(Array, Array)
		#5 C:\websites\xpoplus.com\index.php(115): require_once('C:\websites\xpo...')
		#6 {main}
		  thrown in <b>C:\websites\xpoplus.com\application\controllers\dev.php</b> on line <b>25</b><br />
		*/
	}
	
	function getList() {
		ini_set('max_execution_time', 0);
		
		$client = new soapclient($this->config->item('kxpo_url'));
		$result = $client->getBeurzen(array("sPaswoord" => $this->config->item('kxpo_password')));
		$xml = simplexml_load_string($result->getBeurzenResult);
		
		foreach($xml as $item) {
			if($item->NaamNL[0] != "") {
				$event->id = $item->ID[0];
				$event->name = $item->NaamNL[0];
			}else if($item->NaamFR[0] != "") {
				$event->id = $item->ID[0];
				$event->name = $item->NaamFR[0];
			}else if($item->NaamEN[0] != "") {
				$event->id = $item->ID[0];
				$event->name = $item->NaamEN[0];
			}
			$returnlist[] = $event;
			$event = null;
		}
		if(isset($returnlist)) {
			print_r($returnlist);
		}else {
			die('NULL');
		}
	}

	function info() {
		phpinfo();
	}

	function sessionsapi($eventid) {
		$this->load->model('sessions_model');
		$sess = $this->sessions_model->getSessionByEventID($eventid);
		print "-----<br />";
		print_r(json_encode($sess));
		print "<br />-----";
	}
	
	function striptxt($text = "<d334fd5e d6ff807f dd4ed1f1 9e2aeb68 92feef42 99f1c8e9 9d86ff28 87ecd18a>") {
		echo str_replace(" ","",substr(substr($text, 0, -1), 1));
	}
	
	/* ----------- */
	/* ARTBRUSSELS */
	/* ----------- */
	function importcsv() {
		/*
		$headers = TRUE;
		$heading = array();
		$dataarray = array();
		$row = 1;
		if (($handle = fopen("./artbrussels.csv", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);
				$row++;
				$newrow = array();
				for ($c=0; $c < $num; $c++) {
					if($headers) {
						$heading[] = $data[$c];
					} else {
						$newrow[$heading[$c]] = $data[$c];
					}
				}
				
				if(!$headers) {
					// data in $dataarray toevoegen;
					$dataarray[] = $newrow;
				}
				print "Add row [" . $newrow['id'] . "]<br />";
				$headers = FALSE;
			}
			fclose($handle);
		}
		print "Aantal galleries : " . count($dataarray) . "<br />";
		//print_r($dataarray);
		*/
		die();
	}
	
	function importcsv2() {
		// document : root/artbrussels.csv
		
		$a = mb_convert_encoding(file_get_contents('./artbrussels.csv'), 'UTF-8');
		//print '<table>';
		$headers = TRUE;
		$heading = array();
		$inserts = array();
		$updates = array();
		foreach(explode("\r\n", $a) as $lines) {
			//print "<tr>"; 
			$count = 0;
			$row = array();
			foreach(explode("\t", $lines) as $li) {
				if($headers){
					$heading[] = $li;
				} else {
					$row[$heading[$count]] = $li;
				}
				//print "<td>$li</td>";
				$count++;
			}
			//print "</tr>";
			if(!$headers) {
				
				// Format data to database-structure
				
				// checken in database of deze al bestaat of als er wijzingen zijn aangebracht op entry
				/*
				$check = $this->db->query("SELECT fr.*, fr.id as regel_id, fd.*, fd.id as doc_id FROM fact_regels fr, fact_documenten fd
					WHERE fd.id = fr.document_id 
					AND fd.soort = 'factuur' 
					AND fd.advertentie_id = '".$row['Code klant']."' 
					AND fd.docnummer = '".$row['Nummer verrichting']."' 
					AND fd.datum = '".$row['Datum']."' 
					AND fd.vervaldatum = '".$row['Vervaldatum']."' 
					AND fd.status = '".$row['Betaald (j/n)']."'
					AND fr.prijs = '".$row['Bedrag debet']."'
					");
				$log .= $this->db->last_query();
				$log .= "<br /><br />";
				
				$log .= print_r($check->result_array(), true);
				$log .= "<br /><br />";
				if($check->num_rows() == 0) {
					// No data found, prepare for insert
					$inserts[] = $row;
					
					$this->db->insert('fact_documenten', array(
						'advertentie_id'				=> $row['Code klant'],
						'docnummer'						=> $row['Nummer verrichting'],
						'soort'							=> $row['Type verrichting'],
						'datum'							=> $row['Datum'],
						'vervaldatum'					=> $row['Vervaldatum'],
						'status'						=> $row['Betaald (j/n)'],
						'opmerkingen'					=> $row['Omschrijving verrichting'],
						'manual_bedrijfsnaam'			=> $row['Naam klant'],
						'manual_postcodeengemeente'		=> $row['Postcode klant'] . ' '. $row['Gemeente klant']
					));
					$insert_id = $this->db->insert_id();
					$log .= $this->db->last_query()  . "<br />";
					
					$this->db->insert('fact_regels', array(
						'document_id'	=> $insert_id,
						'titel'			=> 'factuur ' . $row['Nummer verrichting'],
						'prijs'			=> $row['Bedrag debet'],
						'aantal'		=> '1',
						'beschrijving'	=> $row['Omschrijving verrichting'],
						'btw21'			=> 'n',
						'milieutaks'	=> 'n'
					));
					$log .= $this->db->last_query()  . "<br /><br />----------<br /><br />";
				}
				*/
			}
			$headers = FALSE;
		}
		//print '</table>';
		$log .= "Aantal lijnen : " . count(explode("\r\n", $a)) . "<br />";
		//$log .= "Aantal inserts : " . count($inserts) . "<br />";
		
		echo $log;
		//write_file('./logs/import/'. date('Ymd_His') .'_facturatie.txt', str_replace('<br />', "\n", $log));
		
	}
	
	function updatecsvimport() {
		$headers = TRUE;
		$heading = array();
		$dataarray = array();
		$row = 1;
		if (($handle = fopen("./artbrussels_new.csv", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);
				$row++;
				$newrow = array();
				for ($c=0; $c < $num; $c++) {
					if($headers) {
						$heading[] = $data[$c];
					} else {
						$newrow[$heading[$c]] = $data[$c];
					}
				}
				
				if(!$headers) {
					// data in $dataarray toevoegen;
					$dataarray[] = $newrow;
					
					// gallery descriptions (address, phone, fax, ...)
					$rows = $this->db->query("SELECT * FROM exhibitor_ WHERE shortname LIKE '%".$newrow['ShortName']."%'");
					//print_r($row);
					print '<br /><br />';
					
					// map images from tempupload
					if ($rows->num_rows() > 0) {
						$row = $rows->row();
						
						$newdir = "upload/exhibitorimages/".$row->id;
						if(!is_dir($newdir)){
							mkdir("upload/exhibitorimages/".$row->id, 0755);
						}
						
						for ($i=1; $i < 21; $i++) { 
							if ($newrow['ImagePress'.$i]) {
								// *** IMAGE FILE ***
								$image = '';
								if(file_exists("tempupload/".$newrow['ImagePress'.$i])){
									if (file_exists("upload/exhibitorimages/".$row->id."/".$newrow['ImagePress'.$i])) {
										unlink("upload/exhibitorimages/".$row->id."/".$newrow['ImagePress'.$i]);
									}
									if(rename("tempupload/".$newrow['ImagePress'.$i], "upload/exhibitorimages/".$row->id."/".$newrow['ImagePress'.$i])){
										echo "- " . $newrow['ImagePress'.$i] . " -> " . "upload/exhibitorimages/".$row->id."/".$newrow['ImagePress'.$i] . "<br />";
										$image = "upload/exhibitorimages/".$row->id."/".$newrow['ImagePress'.$i];
									}
								} else {
									$image = "upload/exhibitorimages/".$row->id."/".$newrow['ImagePress'.$i];
								}
								
								// *** DESCRIPTION ***
								$newstring = '';
								if($newrow['PressArtist'.$i] != '') { $newstring .= $newrow['PressArtist'.$i] . " <br />" ; }
								if($newrow['PressTitle'.$i] != '') { 
									$newstring .= $newrow['PressTitle'.$i]; 
									if($newrow['PressYear'.$i] != '') { 
										$newstring .= ' - ' . $newrow['PressYear'.$i] . " <br />" ; 
									} else {
										$newstring .= " <br />" ;
									}
								}
								if($newrow['PressTechnic'.$i] != '') { $newstring .= $newrow['PressTechnic'.$i] . " <br />" ; }
								if($newrow['PressMeasures'.$i] != '') { $newstring .= $newrow['PressMeasures'.$i]; }
								
								// replacement
								$newstring = str_replace(array('$doubleQuote$', '$singleQuote$', '<br />'), array('"', "'", "\n"), $newstring);
								
								$newdata = array('imagedescription'.$i => $newstring);
								if($image != '') {
									$newdata['image'.$i] = $image;
								}
								
								$this->db->where('id', $row->id);
								if($this->db->update('exhibitor_', $newdata)){
									echo "Update in database complete<br /><br />";
								}
							}
							
						}
					}
					
					// image descriptions
					/*
					for ($i=1; $i < 20; $i++) { 
						if ($newrow{'ImagePress'.$i} != '') {
							print $newrow{'ImagePress'.$i} . '<br />';
							$row = $this->db->query("SELECT * FROM exhibitor_ WHERE image".$i." LIKE '%".$newrow{'ImagePress'.$i}."%'")->row();
							print_r($row);
							print '<br /><br />';
							// compile new string
							// "ImagePress1","PressName1","PressArtist1","PressTitle1","PressTechnic1","PressYear1","PressMeasures1","PressCopyright1"
							$newstring = '';
							//if($newrow{'PressName'.$i} != '') { $newstring .= $newrow{'PressName'.$i} . " <br />" ; }
							if($newrow{'PressArtist'.$i} != '') { $newstring .= $newrow{'PressArtist'.$i} . " <br />" ; }
							if($newrow{'PressTitle'.$i} != '') { 
								$newstring .= $newrow{'PressTitle'.$i}; 
								if($newrow{'PressYear'.$i} != '') { 
									$newstring .= ' - ' . $newrow{'PressYear'.$i} . " <br />" ; 
								} else {
									$newstring .= " <br />" ;
								}
							}
							if($newrow{'PressTechnic'.$i} != '') { $newstring .= $newrow{'PressTechnic'.$i} . " <br />" ; }
							if($newrow{'PressMeasures'.$i} != '') { $newstring .= $newrow{'PressMeasures'.$i}; }
							//if($newrow{'PressCopyright'.$i} != '') { $newstring .= $newrow{'PressCopyright'.$i}; }
							
							// replacement
							$newstring = str_replace(array('$doubleQuote$', '$singleQuote$', '<br />'), array('"', "'", "\n"), $newstring);
							
							$this->db->where('id', $row->id);
							$this->db->update('exhibitor_', array('imagedescription'.$i => $newstring));
							print $newstring . "<br /><br />";
							print "Update gebeurd<br /><br />";
							$newstring = '';
						}
					}
					*/
					
					// compile new string
					// "Street1","Postalcode1","City1","Country1","Tel1","Fax1","Mobile1","Email1","Website1","Director1"
					$newstring = "Address<br />";
					if($newrow['Street1'] != '') { $newstring .= $newrow['Street1'] . " <br />" ; }
					if($newrow['City1'] != '') { 
						$newstring .= $newrow['City1'] . " " ; 
						if($newrow['Postalcode1'] != '') { 
							$newstring .= $newrow['Postalcode1'] . " <br />" ; 
						} else {
							$newstring .= "<br />" ;
						}
					}
					if($newrow['Country1'] != '') { $newstring .= $newrow['Country1'] . " <br /><br />" ; }
					if($newrow['Website1'] != '') { $newstring .= "Web  " . (substr($newrow['Website1'], 0, 7) != 'http://' ? 'http://'.$newrow['Website1'] : '') . " <br /><br />" ; }
					if($newrow['Tel1'] != '') { $newstring .= "Phone  " . $newrow['Tel1'] . " <br /><br />" ; }
					if($newrow['Mobile1'] != '') { $newstring .= "Mobile  " . $newrow['Mobile1'] . " <br /><br />" ; }
					if($newrow['Fax1'] != '') { $newstring .= "Fax  " . $newrow['Fax1'] . " <br />" ; }
					
					// replacement
					$newstring = str_replace(array('$doubleQuote$', '$singleQuote$', '<br />', '$singlequote$', '$doublequote$'), array('"', "'", "\n", "'", '"'), $newstring);
					
					//$this->db->where('id', $row->id);
					$this->db->where('shortname', $newrow['ShortName']);
					if($this->db->update('exhibitor_', array('description' => $newstring, 'booth' => $newrow['Booth']))){
						print $newstring . "<br /><br />";
						print "Update gebeurd<br /><br />";
					} else {
						print "Update FAILED<br /><br />";
					}
					$newstring = '';
				}
				//print "Add row [" . $newrow['id'] . "]<br />";
				$headers = FALSE;
			}
			fclose($handle);
		}
		print "Aantal galleries : " . count($dataarray) . "<br />";
		print_r($dataarray);
		die();
	}
	
	function remapGalleries() {
		/*
		* image1 => image20
		*/
		
		/*
		$log = '';
		$galleries = $this->db->query("SELECT * FROM exhibitor WHERE eventid = 293");
		
		foreach ($galleries->result() as $row) {
			$images = array();
			for($i=1; $i<21; $i++){
				if($row->{'image'.$i} != ''){
					$images['image'.$i] = $row->{'image'.$i};
				}
				unset($row->{'image'.$i});
				$row->images = $images;
			}
			
			if(count($images) > 0) {
				if(!is_dir("upload/exhibitorimages/".$row->id)){
					mkdir("upload/exhibitorimages/".$row->id, 0755);
				}
				
				foreach($images as $image => $url) {
					// foto's verplaatsen in correcte map
					if(file_exists("tempupload/".$url)){
						if(rename("tempupload/".$url, "upload/exhibitorimages/".$row->id."/".$url)){
							$log .= "- " . $url . " -> " . "upload/exhibitorimages/".$row->id."/".$url . "<br />";
							$images[$image] = "upload/exhibitorimages/".$row->id."/".$url;
						}
					} else {
						unset($images[$image]);
					}
				}
				
				if(count($images) > 0){
					// foto's updaten in database
					$this->db->where('id', $row->id);
					if($this->db->update('exhibitor', $images)){
						$log .= "Update in database complete<br /><br />";
					}
				}
			}
		}
		
		print $log;
		*/
		
		/*
		* imageurl
		*/
		
		$log = '';
		$galleries = $this->db->query("SELECT * FROM exhibitor WHERE eventid = 293");
		
		foreach ($galleries->result() as $row) {
			
			if($row->imageurl != '') {
				if(file_exists($row->imageurl)){
					$this->db->where('id', $row->id);
					if($this->db->update('exhibitor', array('imageurl' => ""))){
						unlink($row->imageurl);
						$log .= "Update in database complete<br /><br />";
					}
				}
			}
			
			/*$url = $row->imageurl;
			if($url != ''){
				if(file_exists("tempupload/".$url)){
					if(rename("tempupload/".$url, "upload/exhibitorimages/".$url)){
						$log .= "- tempupload/" . $url . " -> " . "upload/exhibitorimages/".$url . "<br />";
						
						$this->db->where('id', $row->id);
						if($this->db->update('exhibitor', array('imageurl' => "upload/exhibitorimages/".$url))){
							$log .= "Update in database complete<br /><br />";
						}
					}
				}
			}*/
		}
		
		print $log;
	}
	
	function importArtists() {
		/*
		$log = '';
		
		$brands = $this->db->query("SELECT * FROM importtable");
		//
		// CONNECT BRANDS TO EXHIBITORS
		//
		foreach ($brands->result() as $row) {
			// koppel-kolommen overlopen
			for ($i=1; $i<4; $i++) { 
				if($row->{'koppel'.$i} != '') {
					$this->db->where('eventid', 293);
					$this->db->where('shortname', mysql_escape_string($row->{'koppel'.$i}));
					$exhibitorres = $this->db->get('exhibitor', 1);
					if($exhibitorres->num_rows() > 0) {
						$exhibitor = $exhibitorres->row();
						
						$this->db->where('exhibitorid', $exhibitor->id);
						$this->db->where('exhibitorbrandid', $row->brandid);
						$select = $this->db->get('exhibrand');
						if($select->num_rows() == 0) {
							if($this->db->insert('exhibrand', array('exhibitorid' => $exhibitor->id, 'exhibitorbrandid' => $row->brandid))) {
								$log .= "koppel exhibitor " . $exhibitor->id . " (" . $row->{'koppel'.$i} . ") aan " . $row->brandid . "<br />";
							}
						} else {
							$log .= "REEDS GEKOPPELD exhibitor " . $exhibitor->id . " (" . $row->{'koppel'.$i} . ") aan " . $row->brandid . "<br />";
						}
					} else {
						$log .= "--- exhibitor " . $row->{'koppel'.$i} . " bestaat niet<br />";
					}
				}
			}
		}
		*/
		
		/*
		* ADD BRANDS AND RENUMBER BRANDID IN TABLE
		*/
		/*
		$prev = '';
		$newadds = 0;
		$updates = 0;
		foreach($brands->result() as $row) {
			$brandid = 0;
			if($row->brandname == $prev) {
				// id updaten
				$this->db->where('name', mysql_escape_string($row->brandname));
				$brand = $this->db->get('exhibitorbrand', 1)->row();
				$brandid = $brand->id;
				
				$log .= "existing brand : " . $row->brandname . " ($brandid)<br />";
				$updates++;
			} else {
				// brand nakijken en/of toevoegen en id opslaan
				$this->db->where('name', mysql_escape_string($row->brandname));
				$brand = $this->db->get('exhibitorbrand', 1);
				if($brand->num_rows() > 0) {
					$brandid = $brand->row()->id;
				} else {
					// nieuwe input
					$brand_array = array(
						"eventid" 	=> 293,
						"name" 		=> $row->brandname
					);
					if($this->db->insert('exhibitorbrand', $brand_array)) {
						$brandid = $this->db->insert_id();
						$log .= "new brand : " . $row->brandname . " ($brandid)<br />";
					}
				}
				
				$newadds++;
			}
			
			$this->db->where('id', $row->id);
			$this->db->update('importtable', array('brandid' => $brandid));
			
			$prev = $row->brandname;
		}
		
		$log .= "<br />Nieuwe Brands : " . $newadds;
		$log .= "<br />Update Brands : " . $updates . "<br />";
		*/
		
		echo $log;
		
	}
	
	// MAP SPLIT RESIZE
	function testmapsplit() {
		//print_r(split_map('upload/maps/artbrussels_floorplan11SMALL.png'));
	}
	
	// LINKED IN INTEGRATION
	function saveLinkedin() {
		/*
		$name = $this->input->post('name');
		$firstname = $this->input->post('firstname');
		$linkedin = $this->input->post('linkedin');
		
		if($name != '' && $firstname != '' && $linkedin != '') {
			if($this->db->query("UPDATE attendees SET linkedin = '$linkedin' WHERE firstname = '$firstname' AND name = '$name'")){
				echo "GELUKT";
			} else {
				echo "FAIL";
			}
		} else {
			echo "BAD REQUEST";
		}
		*/
	}
	
	function userlogin($login = '', $token = '') {
		if($token == '123456') {
			$this->session->sess_destroy();
			$this->db->where('login', $login);
			$query = $this->db->get_where('organizer');
			$user_data = $query->row_array();
			$user_data['logged_in'] = true;
			$this->session->set_userdata($user_data);
			redirect('apps');
		} else {
			redirect('auth/login');
		}
	}
	
	function csvimport($eventid = '') {
		if($eventid == '') die('Invalid eventid');
		
		// document : root/TC-Sessions-import V3.csv
		$log = '';
		$headers = TRUE;
		$heading = array();
		$dataarray = array();
		$row = 1;
		if (($handle = fopen("./TC-Sessions-import V3.csv", "r")) !== FALSE) {
			$sessiongroup = "";
			$sessiongroupid = "";
			while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
				$num = count($data);
				$row++;
				$newrow = array();
				for ($c=0; $c < $num; $c++) {
					if($headers) {
						$heading[] = $data[$c];
					} else {
						$newrow[$heading[$c]] = $data[$c];
					}
				}
				
				if(!$headers) {
					if ($newrow['groepsnaam'] != '' && $newrow['groepsnaam'] != $sessiongroup) {
						$sessiongroup = $newrow['groepsnaam'];
						$log .= "Create group '" . $sessiongroup . "' and return ID<br />";
						$this->db->insert("sessiongroup", array(
							"eventid" 	=> $eventid,
							"name" 		=> $sessiongroup
						));
						$sessiongroupid = $this->db->insert_id();
					}
					// dubbele images
					$images = array('dimitridelvaux.jpg', 'ericlegnini.jpg', 'laurentdoumont.jpg', 'marclelangue.jpg', 'philipcatherine.jpg', 'rawkandinsky.jpg', 'verginiyapopova.jpg', 'winchovski.jpg');
					if (in_array($newrow['Foto'], $images)) {
						$image = 'upload/sessionimages/jazz_' . $newrow['Foto'];
					} else {
						$image = 'upload/sessionimages/' . $newrow['Foto'];
					}
					
					if($this->db->insert("session", array(
						'sessiongroupid' 	=> $sessiongroupid, 
						'name' 				=> $newrow['artistnaam'], 
						'description' 		=> $newrow['artistomschrijving'], 
						'starttime' 		=> $newrow['starttijd'], 
						'endtime' 			=> $newrow['eindtijd'], 
						'location' 			=> $newrow['locatie'], 
						'imageurl' 			=> $image
					))) {
						$log .= "&nbsp;&nbsp; + Added session to group " . $sessiongroup . "<br />";
					} else {
						$log .= "&nbsp;&nbsp; ! Failed to ad session to group " . $sessiongroup . "<br />";
					}
					// data in $dataarray toevoegen;
					$dataarray[] = $newrow;
				}
				$headers = FALSE;
			}
			fclose($handle);
		}

		$log .= "<br />Aantal lijnen : " . count($dataarray) . "<br />";
		//$log .= "Aantal inserts : " . count($inserts) . "<br />";
		
		echo $log;
		
	}
	
	function specialimport() {
		$a = mb_convert_encoding(file_get_contents('./gal2020_exhi.csv'), 'UTF-8');
		$eventid = 260;
		$log = '';
		$prev = '';
		$count = 0;
		foreach(explode("\r\n", $a) as $lines) {
			$splitdata = explode(';', $lines);
			/*
			// STEP 1 : INSERT ALL UNIQUE BRANDS
			if($this->db->query("SELECT * FROM exhibitorbrand WHERE eventid = $eventid AND name = '".addslashes($splitdata[0])."'")->num_rows() == 0) {
				$this->db->insert("exhibitorbrand", array('eventid' => $eventid, "name" => trim($splitdata[0])));
				$count++;
			}
			$log .= $count . ' new brands inserted<br />';
			*/
			
			/*
			// STEP 2 : INSERT ALL UNIQUE EXHIBITORS
			if($this->db->query("SELECT * FROM exhibitor WHERE eventid = $eventid AND name = '".addslashes($splitdata[1])."'")->num_rows() == 0) {
				$this->db->insert("exhibitor", array('eventid' => $eventid, "name" => trim($splitdata[1])));
				$count++;
			}
			$log .= $count . ' new exhibitors inserted<br />';
			*/
			
			/*
			// STEP 3 : CONNECT BRANDS TO EXHIBITORS
			foreach($this->db->query("SELECT * FROM exhibitor WHERE eventid = $eventid")->result() as $exhibitor) {
				// NAME EQUALS TO THIS LINE'S EXHIBITOR
				if($exhibitor->name == trim($splitdata[1])){
					// CHECK FOR BRANDID IN DATABASE
					$brand = $this->db->query("SELECT * FROM exhibitorbrand WHERE eventid = $eventid AND name = '".addslashes($splitdata[0])."' LIMIT 1");
					if($brand->num_rows() > 0) {
						// CHECK IF BRAND IS ALREADY LINKED TO EXHIBITOR ?
						if($this->db->query("SELECT * FROM exhibrand WHERE exhibitorid = $exhibitor->id AND exhibitorbrandid = ".$brand->row()->id)->num_rows() == 0) {
							// INSERT LINK BETWEEN EXHIBITOR AND BRAND
							$this->db->insert("exhibrand", array("exhibitorid" => $exhibitor->id, "exhibitorbrandid" => $brand->row()->id));
							$log .= " - insert exhibrand exhibitorid: $exhibitor->id and exhibitorbrandid: ".$brand->row()->id . "<br />";
						} // already linked, next
					} // fail, next
				} // next
			}
			*/
		}
		$log .= "Aantal lijnen : " . count(explode("\r\n", $a)) . "<br />";
		
		echo $log;
	}
	
}