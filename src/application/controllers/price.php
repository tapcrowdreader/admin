<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Price extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('price_model');
		$this->load->model('order_model','order');
	}

	function app($id) {
		$app = $this->app_model->get($id);
		if($app == FALSE) redirect('apps');

		_actionAllowed($app, 'app');


		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.submit', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to Submit Application', 'error');
			redirect('apps/view/'.$app->id);
		}

		if($app->subflavorpaidtype == 9999) {
			redirect('submit/app/'.$app->id);
		}

		$price = $this->price_model->getPriceForApp($app);
		$flavor = $this->app_model->getFlavorOfId($app->apptypeid);
		$sflavor = $this->app_model->getSubflavor($price->subflavorid);
		$options = $this->price_model->getOptions($app);
		$paymentinfo = $this->price_model->getAppPaymentInfo($app->id);

		$priceApp = 0;
		$altOptions = array();
		foreach($options as $o) {
			if($o->id == 1) {
				$priceApp = (int)$o->price_setup + (int)$o->price_permonth;
			}
			if($o->price_permonth != 0 || $o->price_setup != 0) {
				$altOptions[] = $o;
			}
		}
		//Only options with payment infomation
		$options = $altOptions;


		$paidsubflavors = $this->order->getPaidSubflavorForApp($id);
		foreach ($paidsubflavors as $paidsubflavor) {
			// - /price_setup
			$paid = $paid + (float)$paidsubflavor->price_setup;
		}

		//check if payment is available on this channel
		// $allowpayment = $this->price_model->allowPayment();

		//var_dump($alreadypaid);
		if($this->input->post('postback') == 'postback') {
			$prices = array();
			$prices[] = $price->id;
			$totalprice = (int)$price->price_setup + (int)$price->price_permonth * $price->billingperiod;
			//var_dump($options);
			foreach($options as $o) {
				if($this->input->post($o->id)) {
					$op = $this->price_model->getOptionPrice($o->id, $app);
					$prices[] = $op->price_id;
					$totalprice += (int)$o->price_setup + (int)$o->price_permonth;
				}
			}
			//$this->price_model->addPriceOfApp($app->id, $totalprice, $price->id);
			//var_dump($prices);
			//DEV:
			$isPaid = $this->order->isAppPaid($id, $prices);
			//var_dump($isPaid);
			if($isPaid === true) {
				//TODO: App is paid! -> redirect to page that displays this! 
				redirect('pay/ok/'.$id);
			}else if($isPaid === false) {
				//If false: no payments found, create new order
				$order = $this->order->createOrderForPrices($prices, $id, $paymentinfo);
				redirect('pay/order/'.$order->id);
			}else {
				//List of items that are not paid!
				$order = $this->order->createOrderForPrices($isPaid, $id, $paymentinfo);
				redirect('pay/order/'.$order->id);
			}
			//
			//also save options somewhere!
			
		}


		//var_dump($subflavor);

		$cdata['content'] 		= $this->load->view('c_price', array('app' => $app, 'price' => $price,'paid' => $paid,  'flavor' => $flavor, 'options' => $options, 'priceApp' => $priceApp, 'sflavor' => $sflavor, 'paymentinfo' => $paymentinfo), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array( $app->name => "apps/view/".$id,__("Price") => "price/app/".$id);
		$this->load->view('master', $cdata);
	}	

	function vat($id) {
		$app = $this->app_model->get($id);
		if($app == FALSE) redirect('apps');

		_actionAllowed($app, 'app');

		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.submit', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to Submit Application', 'error');
			redirect('apps/view/'.$app->id);
		}

		if($app->subflavorpaidtype == 9999) {
			redirect('submit/app/'.$app->id);
		}

		if($this->input->post('postback') == 'postback') {
			$country = $this->input->post('country');
			$vatCheck = $this->input->post('vatnumbercheckbox');
			if($vatCheck === false) {
				$vatCheck = 0;
			} else {
				$vatCheck = 1;
			}
			$vatnumber = $this->input->post('vatnumber');

			$this->general_model->insert_unique('apppaymentinfo', array(
					'appid' => $app->id,
					'country' => $country,
					'vat' => $vatCheck,
					'vatnumber' => $vatnumber
				), 'appid', $app->id);

			redirect('price/app/'.$app->id);
		}

		$cdata['content'] 		= $this->load->view('c_vat', array('app' => $app), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array( $app->name => "apps/view/".$id,__("VAT") => "price/vat/".$id);
		$this->load->view('master', $cdata);	
	}
}
