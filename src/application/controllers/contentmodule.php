<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Contentmodule extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('contentmodule_model');
	}
	function image_rules($str){
		$filename = 'image';
		return image_check($filename, $_FILES);
	}  
	
	function add($appid) {
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$error = '';
		$this->load->library('form_validation');

		$this->iframeurl = 'contentmodule/app/'.$app->id;

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND contentmoduleid <> 0 GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}	

		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
            }
			$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('tags', 'tags', 'trim');
			$this->form_validation->set_rules('hideinmenu', 'hideinmenu', 'trim');
			$this->form_validation->set_rules('homemodule', 'homemodule', 'trim');
			
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {	

				$hideinmenu = 0;
				$homemodule = 0;
				if($this->input->post('hideinmenu')) {
					$hideinmenu = 1;
				}
				if($this->input->post('homemodule')) {
					$homemodule = 1;
				}
				$data = array(
						'appid'		=> $app->id,
						'title'		=> $this->input->post('title_'.$app->defaultlanguage),
						'homemodule' => $homemodule,
						'hideinmenu' => $hideinmenu,
						'order'		=> $this->input->post('order')
					);
				$id = $this->general_model->insert('contentmodule', $data);

				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/contentmoduleimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/contentmoduleimages/".$id, 0755, TRUE);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/contentmoduleimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('imageurl')) {
					$image = "upload/l_careers.png"; //No image uploaded!
					if($_FILES['imageurl']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/contentmoduleimages/'. $id . '/' . $returndata['file_name'];
				}

				// *** SAVE TAGS *** //
				$tags = $this->input->post('mytagsulselect');
				foreach ($tags as $tag) {
					$tags = array(
							'appid' 	=> $app->id,
							'tag' 		=> htmlspecialchars_decode(html_entity_decode($tag)),
							'contentmoduleid' => $id
						);
					$this->db->insert('tag', $tags);
				}
				
				$this->general_model->update('contentmodule', $id, array('icon' => $image));
				$this->general_model->update('app', $app->id, array('timestamp' => time()));
				if($error == '') {
					redirect('apps/view/'.$app->id);
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_contentmodule_add', array('app' => $app, 'error' => $error, 'languages' => $languages, 'apptags' => $apptags), TRUE);
		$cdata['crumb']			= array(__("Add Module") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}

	function edit($id) {
		$contentmodule = $this->contentmodule_model->getById($id);
		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$error = '';
		$this->load->library('form_validation');
		$this->iframeurl = 'contentmodule/view/'.$id;

		// TAGS
		$tags = $this->db->query("SELECT * FROM tag WHERE contentmoduleid = $id");
		if($tags->num_rows() == 0) {
			$tags = array();
		} else {
			$tagz = array();
			foreach ($tags->result() as $tag) {
				$tagz[] = $tag;
			}
			$tags = $tagz;
		}
		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND contentmoduleid <> 0 GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}	

		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
            }
			$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('tags', 'tags', 'trim');
			$this->form_validation->set_rules('hideinmenu', 'hideinmenu', 'trim');
			$this->form_validation->set_rules('homemodule', 'homemodule', 'trim');
			
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {	

				$hideinmenu = 0;
				$homemodule = 0;
				if($this->input->post('hideinmenu')) {
					$hideinmenu = 1;
				}
				if($this->input->post('homemodule')) {
					$homemodule = 1;
				}

				if($homemodule == 1) {
					$this->contentmodule_model->removeHomeModule($app->id);
				}

				$data = array(
						'title'		=> $this->input->post('title_'.$app->defaultlanguage),
						// 'homemodule' => $homemodule,
						'hideinmenu' => $hideinmenu,
						'order'		=> $this->input->post('order')
					);
				$this->general_model->update('contentmodule', $id, $data);

				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/contentmoduleimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/contentmoduleimages/".$id, 0755, TRUE);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/contentmoduleimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('imageurl')) {
					$image = "upload/l_careers.png"; //No image uploaded!
					if($_FILES['imageurl']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/contentmoduleimages/'. $id . '/' . $returndata['file_name'];
				}

				// *** SAVE TAGS *** //
				if(!empty($id)) {
					$this->db->where('contentmoduleid', $id);
					$this->db->where('appid', $app->id);
	                $this->db->delete('tag');
					$tags = $this->input->post('mytagsulselect');
					foreach ($tags as $tag) {
						$tags = array(
								'appid' 	=> $app->id,
								'tag' 		=> htmlspecialchars_decode(html_entity_decode($tag)),
								'contentmoduleid' => $id
							);
						$this->db->insert('tag', $tags);
					}
				}
				
				if($_FILES['imageurl']['name'] != '') {
					$this->general_model->update('contentmodule', $id, array('icon' => $image));
				}
				
				$this->general_model->update('app', $app->id, array('timestamp' => time()));
				if($error == '') {
					redirect('apps/view/'.$app->id);
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_contentmodule_edit', array('app' => $app, 'error' => $error, 'languages' => $languages, 'contentmodule' => $contentmodule, 'tags' => $tags, 'apptags' => $apptags), TRUE);
		$cdata['crumb']			= array($contentmodule->title => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}

	public function view($id) {
		$this->iframeurl = 'contentmodule/view/'.$id;
		$contentmodule = $this->contentmodule_model->getById($id);

		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');

		$sections = $this->contentmodule_model->getSectionsOfModuleWithTypes($id);


		$cdata['content'] 		= $this->load->view('c_contentmodule_view', array('app' => $app, 'sections' => $sections, 'contentmodule' => $contentmodule), TRUE);
		$cdata['crumb']			= array($contentmodule->title => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}

	public function delete($id) {
		$contentmodule = $this->contentmodule_model->getById($id);

		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');

		$this->general_model->remove('contentmodule',$id);
		redirect('apps/view/'.$app->id);
	}
        
    public function importrss($id) {
        $contentmodule = $this->contentmodule_model->getById($id);
        if ($this->input->post('postback') == "postback") {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('url', 'url', 'trim|required');
            $this->form_validation->set_rules('refreshrate', 'refreshrate', 'trim|numeric');
            $this->form_validation->set_rules('tag', 'tag', 'trim|required');            
            if ($this->form_validation->run() == FALSE) {
                $error = __("Some fields are missing.");
            } else {
                $tag = $this->input->post('tag');
                $url = $this->input->post('url');
                $content = file_get_contents($url);
                try {
                    $rss = new SimpleXmlElement($content);
                } catch (Exception $e) {
                    $rss = null;
                }
                
                $urlExists = $this->contentmodule_model->urlExists($contentmodule->appid,$url);
                
                if ($content != null && $rss != null && !empty($content) && !empty($rss)) {
                    $newssourceData = array(
                        "appid" => $contentmodule->appid,
                        "type" => "rss",
                        "url" => $url,
                        "timestamp" => date("Y-m-d H:i:s", time()),
                        "refreshrate" => $this->input->post('refreshrate')
                    );
                    
                    if($urlExists > 0){
                        $error = __("<p>RSS Feed URL already exists.</p>");
                        $this->session->set_flashdata('error', __("<p>RSS Feed URL already exists.</p>"));                        
                        redirect('contentmodule/importrss/' . $contentmodule->id);
                    }else{
                        if($sourceid = $this->general_model->insert('newssource', $newssourceData)){
                            $tagData = array(
                                "contentmoduleid" => $contentmodule->id,
                                "tag" => (string) $tag                                
                            );                            
                            $this->contentmodule_model->insert('tag', $tagData);                            
                        }
                    }
                    foreach ($rss->channel->item as $post) {
                        $description = (string) $post->description;
                        $namespace = $post->children('http://purl.org/rss/1.0/modules/content/');
                        $content = (string) $namespace->encoded;
                        if ($content != null) {
                            $description = $content;
                        }
                        $data = array(
                            "title" => (string) $post->title,
                            "appid" => $contentmodule->appid,
                            "txt" => $description,
                            "url" => (string) $post->link,
                            "sourceid" => $sourceid,
                            "datum" => ((string) $post->pubDate != null && (string) $post->pubDate != '') ? date("Y-m-d H:i:s", strtotime((string) $post->pubDate)) : date("Y-m-d H:i:s", time())
                        );
                        if ($newsid = $this->general_model->insert('newsitem', $data)) {
                            //add translated fields to translation table
                            if ($languages != null) {
                                foreach ($languages as $language) {
                                    $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, (string) $post->title);
                                    $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $description);
                                }
                            }
                            $tagData = array(
                                "newsitemid" => (string) $newsid,
                                "tag" => (string) $tag                                
                            );
                            $this->contentmodule_model->insert('tag', $tagData);
                            
                        } else {
                            $error = __("<p>Oops, something went wrong. Please try again.</p>");
                        }
                    }
                } else {
                    $error = __("<p>Oops, something went wrong. Please try again.</p>");
                }

                if ($error == '') {
                    $this->session->set_flashdata('event_feedback', __('The rss has successfully been imported'));
                    _updateTimeStamp($contentmodule->appid);
                    redirect('apps/view/' . $contentmodule->appid);
                }
            }
        }

        $cdata['content'] = $this->load->view('c_importrss_view', array('name' => $error,'error' => $error, 'app' => $app, 'sections' => $sections, 'contentmodule' => $contentmodule), TRUE);
        $this->load->view('master', $cdata);
    }

}