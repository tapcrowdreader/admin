<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Metadata extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('metadata_model');
	}
	
	//php 4 constructor
	function Metadata() {
		parent::__construct();
		if(_authed()) { }
	}
    
	function index() {
		$this->app(_currentApp()->id);
	}
	
	function add($tableid, $type = '') {
		if($type == '') {
			if($this->input->post('metadata') == "metadata") {
				$key = $this->input->post('key');
				$value = $this->input->post('value');
				$table = $this->input->post('table');

				$data = array(
						"appid"			=> _currentApp()->id,
						"table"			=> 'artist',
						"identifier"	=> $tableid,
						"key"           => $key,
						"value"			=> $value
					);

				$this->general_model->insert('metadata', $data);
				redirect($table.'s/edit/'.$tableid);
			}
		} elseif($type=='event') {
			$event = $this->event_model->getbyid($tableid);
			$app = _actionAllowed($event, 'event','returnApp');
			$key = $this->input->post('key');
			$value = $this->input->post('value');

			$data = array(
					"appid"			=> $app->id,
					"table"			=> 'event',
					"identifier"	=> $tableid,
					"key"           => $key,
					"value"			=> $value
				);

			$this->general_model->insert('metadata', $data);

			$metadata = $this->metadata_model->get($app->id, 'event', $tableid);

			echo $this->load->view('c_festival_info', array('event' => $event, 'metadata' => $metadata, 'app' => $app), TRUE);
		}
	}
	
	function edit($id) {
		if($this->input->post('metadata') == "metadata") {
			$key = $this->input->post('key');
			$value = $this->input->post('value');

			$data = array(
					"key"           => $key,
					"value"			=> $value
				);

			$this->general_model->update('metadata', $id, $data);
		}
	}
	
	function delete($table, $tableid, $id) {
		if($table == 'artist') {
	        if($this->general_model->remove('metadata', $id) ){
	            $this->session->set_flashdata('artist_feedback', __('The metadata has successfully been deleted'));
	            $redirect = str_replace("@","/",$redirect);
	            redirect($table . 's/edit/' . $tableid);
	        } else {
	            $this->session->set_flashdata('artist_error', __('Oops, something went wrong. Please try again.'));
	            redirect($table . 's/edit/' . $tableid);
	        }
	    } elseif($table == 'event') {
			$event = $this->event_model->getbyid($tableid);
			$app = _actionAllowed($event, 'event','returnApp');
	    	$this->general_model->remove('metadata', $id);
			$metadata = $this->metadata_model->get($app->id, 'event', $tableid);

			echo $this->load->view('c_festival_info', array('event' => $event, 'metadata' => $metadata, 'app' => $app), TRUE);
	    }
	}
	
	function addtemp() {
		$app = _currentApp();
		if($this->input->post('metadata') == "metadata") {
			$key = $this->input->post('key');
			$value = $this->input->post('value');
			$table = $this->input->post('table');

			$data = array(
					"table"			=> 'artist',
					"key"           => $key,
					"value"			=> $value,
					"appid"			=> $app->id
				);

			$this->general_model->insert('metadatatemp', $data);
			redirect($table.'s/add/'.$app->id);
		}
	}
	
	function edittemp($id) {
		if($this->input->post('metadata') == "metadata") {
			$key = $this->input->post('key');
			$value = $this->input->post('value');

			$data = array(
					"key"           => $key,
					"value"			=> $value
				);

			$this->general_model->update('metadatatemp', $id, $data);
		}
	}
	
	function deletetemp($table, $id) {
        if($this->general_model->remove('metadatatemp', $id) ){
            $this->session->set_flashdata('artist_feedback', __('The metadata has successfully been deleted'));
            $redirect = str_replace("@","/",$redirect);
            redirect($table . 's/add');
        } else {
            $this->session->set_flashdata('artist_error', __('Oops, something went wrong. Please try again.'));
            redirect($table . 's/add');
        }
	}




	//new metadata functions
	function remove($id) {
		$meta = $this->metadata_model->getFieldById($id);
		$app = $this->app_model->get($meta->appId);
		_actionAllowed($app, 'app','');

		$this->general_model->remove('tc_metadata', $id);
		unlink($this->config->item('imagespath') . $meta->value);
	}

	function removefile($metaId, $objectType, $objectId, $hash) {
		if($hash != md5('tcadm'.$objectType.$objectId)) {
			return false;
		}
		$res = $this->db->query("SELECT value FROM tc_metavalues WHERE metaId = $metaId AND parentType = '$objectType' AND parentId = '$objectId' LIMIT 1");
		if($res->num_rows() > 0) {
			$res = $res->row();
			if(file_exists($this->config->item('imagespath') . $res->value)) {
				unlink($this->config->item('imagespath') . $res->value);
			}

			$this->db->query("DELETE FROM tc_metavalues WHERE metaId = $metaId AND parentType = '$objectType' AND parentId = '$objectId'");
		}
	}


	function copy($launcherid) {
		$launcher = $this->module_mdl->getLauncherById($launcherid);
		$appid = $launcher->appid;
		$tag = $launcher->tag;
		if($launcher->appid != 0) {
			$app = $this->app_model->get($launcher->appid);
			_actionAllowed($app, 'app','');

			if($launcher->moduletypeid == 30) {
				$objects = 'events';
			} elseif($launcher->moduletypeid == 31) {
				$objects = 'venues';
			}
		}
		$appid = (int) $appid;
		$addmetadata = array();
		if($objects == 'events') {
			$metadata = $this->db->query("SELECT m.*, l.eventid, l.moduletypeid, l.module, l.title, l.icon, l.displaytype, l.order, l.url FROM tc_metadata m INNER JOIN launcher l ON l.id = m.parentId WHERE m.appid = $appid AND m.parentType = 'launcher' AND l.eventid > 0")->result();

			$events = $this->db->query("SELECT e.*, t.tag FROM appevent ae INNER JOIN event e ON e.id = ae.eventid INNER JOIN tag t ON t.eventid = e.id WHERE ae.appid = $appid AND deleted = 0 AND t.tag = ?", array($tag))->result();

			foreach($metadata as $m) {
				foreach($events as $e) {
					if($e->id != $m->eventid) {
						$newlauncherid = $this->db->query("SELECT id FROM launcher WHERE eventid = $e->id AND moduletypeid = $m->moduletypeid LIMIT 1");
						if($newlauncherid->num_rows == 0) {
							$newlauncherid = $this->general_model->insert('launcher', array(
								'eventid' => $e->id,
								'moduletypeid' => $m->moduletypeid,
								'module' => $m->module,
								'title' => $m->title,
								'icon' => $m->icon,
								'displaytype' => $m->displaytype,
								'order' => $m->order,
								'url' => $m->url,
								'active' => 0
								));
						} else {
							$newlauncherid = $newlauncherid->row()->id;
						}
						$checkExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $newlauncherid AND qname = '$m->qname' LIMIT 1");
						if($checkExists->num_rows() == 0) {
							if(!isset($addmetadata[$newlauncherid.'_'.$m->qname])) {
								$addmetadata[$newlauncherid.'_'.$m->qname] = array(
									'appId' => $appid,
									'parentType' => 'launcher',
									'parentId' => $newlauncherid,
									'sortorder' => $m->sortorder,
									'status' => $m->status,
									'type' => $m->type,
									'opts' => $m->opts,
									'qname' => $m->qname,
									'name' => $m->name,
									'value' => $m->value
									);
							}
						}
					}
				}
			}
		} elseif($objects == 'venues') {
			$metadata = $this->db->query("SELECT m.*, l.venueid, l.moduletypeid, l.module, l.title, l.icon, l.displaytype, l.order, l.url FROM tc_metadata m INNER JOIN launcher l ON l.id = m.parentId WHERE m.appid = $appid AND m.parentType = 'launcher' AND l.venueid > 0")->result();

			$venues = $this->db->query("SELECT e.*, t.tag FROM appvenue ae INNER JOIN venue e ON e.id = ae.venueid INNER JOIN tag t ON t.venueid = e.id WHERE ae.appid = $appid AND deleted = 0 AND t.tag = ?", array($tag))->result();

			foreach($metadata as $m) {
				foreach($venues as $e) {
					if($e->id != $m->venueid) {
						$newlauncherid = $this->db->query("SELECT id FROM launcher WHERE venueid = $e->id AND moduletypeid = $m->moduletypeid LIMIT 1");
						if($newlauncherid->num_rows == 0) {
							$newlauncherid = $this->general_model->insert('launcher', array(
								'venueid' => $e->id,
								'moduletypeid' => $m->moduletypeid,
								'module' => $m->module,
								'title' => $m->title,
								'icon' => $m->icon,
								'displaytype' => $m->displaytype,
								'order' => $m->order,
								'url' => $m->url,
								'active' => 0
								));
						} else {
							$newlauncherid = $newlauncherid->row()->id;
						}
						$checkExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $newlauncherid AND qname = '$m->qname' LIMIT 1");
						if($checkExists->num_rows() == 0) {
							if(!isset($addmetadata[$newlauncherid.'_'.$m->qname])) {
								$addmetadata[$newlauncherid.'_'.$m->qname] = array(
									'appId' => $appid,
									'parentType' => 'launcher',
									'parentId' => $newlauncherid,
									'sortorder' => $m->sortorder,
									'status' => $m->status,
									'type' => $m->type,
									'opts' => $m->opts,
									'qname' => $m->qname,
									'name' => $m->name,
									'value' => $m->value
									);
							}
						}
					}
				}
			}
		}

		$sql = array();
		foreach($addmetadata as $am) {
			$sql[] = "('".implode("','", $am)."')";
		}

		$sql = implode(',',$sql);

		if(!empty($sql)) {
			$this->db->query("INSERT INTO tc_metadata (appId, parentType, parentId, sortorder, status, type, opts, qname, name, value) VALUES $sql");
		}

		if($objects == 'events') {
			redirect('events/launcher/'.$launcherid);
		} elseif($objects == 'venues') {
			redirect('venues/launcher/'.$launcherid);
		}
		
	}
}