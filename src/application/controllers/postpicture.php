<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Postpicture extends CI_Controller
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if(_authed()) { }

		# Set settings
		$this->_module_settings = (object)array(
			'singular' => 'Picture',
			'plural' => 'Pictures',
			'title' => __('Photo Sharing'),
			'add_url' => 'postpicture/add/--parentId--/--parentType--',
			'browse_url' => 'postpicture/--parentType--/--parentId--',
			'module_url' => 'module/editByController/postpicture/--parentType--/--parentId--/0',
			'headers' => array(
				__('Image') => 'imageurl',
				__('Description') => 'description',
			),
			'actions' => array(
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'postpicture/remove/--parentType--/--parentId--/--id--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger confirm',
				),
			),
			'views' => array(
				'list' => 'tc_listview'
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'launcher' => null,
			'checkboxes' => false,
			'deletecheckedurl' => 'postpicture/remove/--parentType--/--parentId--'
		);
	}

	/**
	 * Default access point
	 */
	public function index()
	{
		$this->event();
	}

	/**
	 * Venue access point
	 *
	 * @param int $venueId
	 */
	public function venue( $venueId )
	{
		$query = filter_input(INPUT_GET, 'query');
		$this->search('venue', $venueId, $query);
	}

	/**
	 * Event access point
	 *
	 * @param int $eventId
	 */
	public function event( $eventId )
	{
		$query = filter_input(INPUT_GET, 'query');
		$this->search('event', $eventId, $query);
	}

	/**
	 * Lists the Pictures
	 *
	 * @param string $parentType (event or venue)
	 * @param int $parentId
	 * @param string $query
	 */
	public function search( $parentType, $parentId, $query )
	{
		# Get parent, app, launcher objects
		if(empty($parentId)) redirect('events');
		list($app, $parent, $launcher) = $this->_getParents($parentType, $parentId);

		# Gather data
		$path = sprintf('thumb/app_%d/%s_%d/postpicture/', $app->id, $parentType, $parentId);
		$pictures = $this->_streamRequest('GET',$path);

		# Setup
		$settings =& $this->_module_settings;
		$replace = array(array('--parentType--','--parentId--'), array($parentType, $parentId));
		foreach(array('module_url','add_url','deletecheckedurl') as $url) {
			$settings->$url = str_replace($replace[0], $replace[1], $settings->$url);
		}
		$this->_module_settings->checkboxes = true;
		$this->_module_settings->title = $launcher->title;
		$this->iframeurl = "photostream/view/" . $app->id;

		$remove_url =& $this->_module_settings->actions['delete']->href;
		$remove_url = str_replace($replace[0], $replace[1], $remove_url);

		# Output
		$data = array('settings' => $this->_module_settings, 'data' => $pictures);
		$this->_renderOutput($this->_module_settings->views['list'], $app, $parentType, $parent, $data);
	}

	/**
	 * Add a picture to the Stream
	 *
	 * @param int $parentId
	 * @param string $parentType (event or venue)
	 */
	public function add( $parentId, $parentType )
	{
		# Validate
		if(empty($parentId)) redirect('events');

		# Get parent, app, launcher objects
		list($app, $parent, $launcher) = $this->_getParents($parentType, $parentId);

		# Handle request
		$method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
		if($method == 'POST') {
			try {
				if(!isset($_FILES['image']) || !is_uploaded_file($_FILES['image']['tmp_name'])) {
					throw new \InvalidArgumentException('No image uploaded');
				}

				# Rename tmp file ro original name (Original filename will be POSTed)
				$filepath = dirname($_FILES['image']['tmp_name']) . '/' . $_FILES['image']['name'];
				rename($_FILES['image']['tmp_name'], $filepath);

				$path = "app_{$app->id}/{$parentType}_$parentId/postpicture/";
				$data = array(
					'appid' => $app->id,
					$parentType.'id' => $parentId,
					'description' => filter_input(INPUT_POST, 'description'),
					'photo_jpg' => '@'.$filepath,
				);
				$this->_streamRequest( 'POST', $path, $data);
			} catch(\Exception $e) {
				Notifications::getInstance()->alert($e->getMessage(), 'error');
			}
			redirect("postpicture/$parentType/$parentId");
		} else {
			$this->_module_settings->title = $launcher->title;
			$data = array($parentType => $parent, 'error' => $error, 'languages' => $languages, 'app' => $app);
			$this->_renderOutput('c_postpicture_add', $app, $parentType, $parent, $data);
		}
	}

	/**
	 * Remove Picture(s)
	 *
	 * @param string $parentType
	 * @param int $parentId
	 * @param int $pictureId (optional, may also be POSTed as 'selectedids')
	 */
	public function remove( $parentType, $parentId, $pictureId = null )
	{
		# Validate
		if(empty($parentId)) redirect('events');

		# Get parent, app, launcher objects
		list($app, $parent, $launcher) = $this->_getParents($parentType, $parentId);

		# Gather data
		$path = sprintf('thumb/app_%d/%s_%d/postpicture/', $app->id, $parentType, $parentId);
		$pictures = $this->_streamRequest('GET',$path);

		# POST pictureIds
		$opts = array('flags' => FILTER_REQUIRE_ARRAY);
		$pictureIds = filter_input(INPUT_POST, 'selectedids', FILTER_VALIDATE_INT, $opts);

		# GET pictureId
		if(is_numeric($pictureId)) {
			if(!is_array($pictureIds)) $pictureIds = array();
			$pictureIds[] = $pictureId;
		}

		# Match paths
		foreach($pictures as $picture) {
			if(in_array($picture->id, $pictureIds)) {
				$filepath = substr($picture->imageurl, strpos($picture->imageurl, $path));
				//var_Dump("DELETE $filepath");
				$data = $this->_streamRequest('DELETE', $filepath);
			}
		}
		redirect("postpicture/$parentType/$parentId");
	}

	/**
	 * Renders the output
	 *
	 * @param string $view
	 * @param object $app
	 * @param string $parentType
	 * @param object $parent
	 * @param array $data
	 */
	protected function _renderOutput( $view, $app, $parentType, $parent, $data )
	{
		$crumb = array();
		if($app->familyid == 3) $crumb[__($parentType.'s')] = $parentType;
		$crumb = array(
			$parent->name => "$parentType/view/".$parent->id,
			$this->_module_settings->title => "postpicture/$parentType/".$parent->id,
			__("Add new") => $this->uri->uri_string(),
		);
		$this->load->view('master', array(
			'sidebar' => $this->load->view('c_sidebar', array($parentType => $parent), true),
			'modules' => $this->module_mdl->getModulesForMenu($parentType, $parent->id, $app, 'postpicture'),
			'content' => $this->load->view($view, $data, true),
			'crumb' => $crumb,
		));
	}

	/**
	 * Returns the app, parent and launcher objects
	 *
	 * @param string $parentType (event or venue)
	 * @param int $parentId
	 * @return array ($app, $parent, $launcher)
	 */
	protected function _getParents( $parentType, $parentId )
	{
		switch($parentType) {
			case 'event':
				$parent = $this->event_model->getbyid($parentId); break;
			case 'venue':
				$parent = $this->venue_model->getById($parentId); break;
			default:
				Notifications::getInstance()->alert('Unsupported Parent Type', 'error'); break;
		}
		if(empty($parent)) Notifications::getInstance()->alert('Parent not found', 'error');
		$app = _actionAllowed($parent, $parentType, 'returnApp');
		$launcher = $this->module_mdl->getLauncher(39, $parentType, $parent->id);
		if($launcher->newLauncherItem) $launcher->title = __($launcher->title);
		return array($app, $parent, $launcher);
	}

	/**
	 * Internal method to call the Stream API
	 *
	 * @param string $method HTTP Method
	 * @param string $filter
	 * @param string $path
	 * @param array $data Optional POST/GET data
	 */
	protected function _streamRequest( $method, $path, $data = array() )
	{
		$stream_url = $this->config->item('stream_url');

		# Setup cUrl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Accept: application/json',
		));

		# Set data
		$url = "{$stream_url}$path";
		$url = $stream_url . preg_replace('/\s+/', '%20', $path);

		curl_setopt($ch, CURLOPT_URL, $url);
		switch($method) {
			case 'GET':
				curl_setopt($ch, CURLOPT_HTTPGET, true);
				break;
			case 'POST':
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				break;
			case 'DELETE':
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			default:
				throw new \InvalidArgumentException('Method Not Supported');
		}

		# Parse and return
		$out = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($out);

		# Handle errors
		if(isset($data->error)) {
			Notifications::getInstance()->alert('TapCrowd Stream: ' . $data->error, 'error');
		}

		return $data;
	}
}
