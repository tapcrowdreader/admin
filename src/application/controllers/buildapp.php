<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Buildapp extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('buildqueue_model');
	}

	function iphone($appid) {
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');
		$this->load->model('user_mdl');
		$organizer = $this->user_mdl->getOrganizerById($app->organizerid);
		$this->load->library('form_validation');

		$status = $this->buildqueue_model->getStatusios($app->id, 'ios');

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('email', 'email', 'trim');

			$emailtosend = $organizer->email;
			if(trim($this->input->post('email')) != '') {
				$emailtosend = $this->input->post('email');
			}

			$this->buildqueue_model->insert($app, 'ios', $emailtosend);
			$status = 'queue';
		} elseif($this->input->post('postback2') == "postback2") {
			$url = urlencode('http://clients.tapcrowd.com/demo/demoplist.php?appid='.$appid);
			$email = $this->input->post('email2');
			//define the receiver of the email
			$to = $email;
			//define the subject of the email
			$subject = __("App Ready: ").$app->name; 
			//create a boundary string. It must be unique 
			//so we use the MD5 algorithm to generate a random hash

			//define the headers we want passed. Note that they are separated with \r\n
			$headers = "From: appbuilder@tapcrowd.com\r\nReply-To: info@tapcrowd.com";
			$headers .= "\r\nBcc: appbuilder@tapcrowd.com";

			//add boundary string and mime type specification
			//$headers .= "\r\nContent-Type: multipart/alternative; boundary=\"PHP-alt-".$random_hash."\""; 
			$headers .=  "\r\n".'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 

			//define the body of the message.

			$message = __("<h2>TapCrowd</h2>
			Hi, <br/><br/>Your demo app %s (iPhone) is ready to be downloaded.
			You can download %s <a href='http://clients.tapcrowd.com/demo/iphoneredirect.php?appid=%d'> here</a>.<br/>
			Note: this link only works on your iPhone, please open this email on your iPhone and click the link.

			<br />
			<br />
			The TapCrowd Team", $app->name, $app->name, $app->id);
			
			//send the email
			$mail_sent = @mail( $to, $subject, $message, $headers );
			$this->session->set_flashdata('event_feedback', 'The mail has been sent');
			$status = 'mailsent';
		}

		$cdata['content'] 		= $this->load->view('c_buildapp', array('app'=>$app, 'organizer' => $organizer, 'status' => $status, 'env' => 'ios'), TRUE);
		// $cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__("Build App") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function android($appid) {
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');
		$this->load->model('user_mdl');
		$organizer = $this->user_mdl->getOrganizerById($app->organizerid);
		$this->load->library('form_validation');

		$status = $this->buildqueue_model->getStatusAndroid($app->id, 'android');

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('email', 'email', 'trim');

			$emailtosend = $organizer->email;
			if(trim($this->input->post('email')) != '') {
				$emailtosend = $this->input->post('email');
			}

			$this->buildqueue_model->insert($app, 'android', $emailtosend);
			$status = 'queue';
		} elseif($this->input->post('postback2') == "postback2") {
			$email = $this->input->post('email2');
			//define the receiver of the email
			$to = $email;
			//define the subject of the email
			$subject = __("App Ready: "). $app->name; 
			//create a boundary string. It must be unique 
			//so we use the MD5 algorithm to generate a random hash

			//define the headers we want passed. Note that they are separated with \r\n
			$headers = "From: appbuilder@tapcrowd.com\r\nReply-To: info@tapcrowd.com";
			$headers .= "\r\nBcc: appbuilder@tapcrowd.com";

			//add boundary string and mime type specification
			//$headers .= "\r\nContent-Type: multipart/alternative; boundary=\"PHP-alt-".$random_hash."\""; 
			$headers .=  "\r\n".'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 

			//define the body of the message.

			$message = __("<h2>TapCrowd</h2>
			Hi, <br/><br/>Your demo app %s (Android) is ready to be downloaded.
			You can download %s <a href='http://clients.tapcrowd.com/demo/androidapp?id=%d'> here</a>.<br/>
			Note: this link only works on your android phone, please open this email on your phone and click the link.

			<br />
			<br />
			The TapCrowd Team", $app->name, $app->name, $app->id);
			
			//send the email
			$mail_sent = @mail( $to, $subject, $message, $headers );
			$this->session->set_flashdata('event_feedback', 'The mail has been sent');
			$status = 'ended';
		}

		$cdata['content'] 		= $this->load->view('c_buildapp', array('app'=>$app, 'organizer' => $organizer, 'status' => $status, 'env' => 'android'), TRUE);
		// $cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__("Build App") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}
}
