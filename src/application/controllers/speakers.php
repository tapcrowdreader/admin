<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Speakers extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('sessions_model');
		$this->load->model('speaker_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Speaker',
			'plural' => 'Speakers',
			'title' => __('Speakers'),
			'parentType' => 'event',
			'browse_url' => 'speakers/--parentType--/--parentId--',
			'add_url' => 'speakers/add/--parentId--/--parentType--',
			'edit_url' => 'speakers/edit/--id--',
			'module_url' => 'module/editByController/speakers/--parentType--/--parentId--/0',
			// 'import_url' => 'import/speakers/--parentId--/--parentType--',
			'headers' => array(
				__('Name') => 'name',
				__('Order') => 'order'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'speakers/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'speakers/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'tabs' => array(

			),
			'checkboxes' => false,
			'deletecheckedurl' => 'speakers/removemany/--parentId--/--parentType--'
		);
	}
	
	function index() {
		$this->event();
	}
	
	function event($id, $groupid = '') {
		if($id == FALSE || $id == 0) redirect('events');
		
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');  

		$this->iframeurl = 'speakers/event/'.$id;

		$speakers = $this->speaker_model->getByEventId($id);

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# Module import url
		$import_url =& $this->_module_settings->import_url;
		$import_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $import_url);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		$speakercategoriesgroupid = $this->speaker_model->getMainCategorieGroup($id);
		if($speakercategoriesgroupid) {
			$btnCategories = (object)array(
							'title' => __('Edit Categories'),
							'href' => 'groups/view/'.$speakercategoriesgroupid.'/event/'.$id.'/speakers',
							'icon_class' => 'icon-pencil',
							'btn_class' => 'edit btn',
							);
			$this->_module_settings->extrabuttons['categories'] = $btnCategories;
		}

		$tabs =& $this->_module_settings->tabs;
		$tabs[] = (object)array(
				'active' => false,
				'url' => 'sessions/event/'.$id,
				'title' => __('Sessions')
			);
		$tabs[] = (object)array(
				'active' => true,
				'url' => 'speakers/event/'.$id,
				'title' => __('Speakers')
			);

		$launcher = $this->module_mdl->getLauncher(40, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($speakers)), true);

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']	= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'speakers');
		$this->load->view('master', $cdata);
	}
	
	function add($id, $type = "event", $object = 'speaker', $groupid = '', $basegroupid = '') {
		$this->load->library('form_validation');
		$error = "";
	
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		
		$this->iframeurl = 'speakers/event/'.$id;

		$languages = $this->language_model->getLanguagesOfApp($app->id);	

		$launcher = $this->module_mdl->getLauncher(40, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;
		$metadata = $this->metadata_model->getMetadata($launcher, '', '', $app);

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'speaker' GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}
		//validation
		if($this->input->post('mytagsulselect') != null) {
			$postedtags = $this->input->post('mytagsulselect');
		} else {
			$postedtags = array();
		}			
	
		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                $this->form_validation->set_rules('description_'.$language->key, 'description', 'trim');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}
            }
            $this->form_validation->set_rules('company', 'company', 'trim');
            $this->form_validation->set_rules('function', 'function', 'trim');
            $this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
            $this->form_validation->set_rules('order', 'order', 'trim');
		
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {							
				$data = array(
						"eventid"			=> $id,
						"name"              => set_value('name_'.$app->defaultlanguage),
						"company"			=> $this->input->post('company'),
						"function"			=> set_value('function'),
						"description"		=> set_value('description_'.$app->defaultlanguage),
						"order"				=> set_value('order')
					);
				
				if($newid = $this->general_model->insert('speaker', $data)){                  
					foreach($languages as $langrow){
					    $this->language_model->addTranslation('speaker', $newid, 'name', $langrow->key, $this->input->post('name_'.$langrow->key));
					    $this->language_model->addTranslation('speaker', $newid, 'description', $langrow->key, $this->input->post('description_'.$langrow->key));
						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								$postfield = _getPostedField($m, $_POST, $_FILES, $language);
								if($postfield !== false) {
									if(_validateInputField($m, $postfield)) {
										_saveInputField($m, $postfield, 'speaker', $newid, $app, $language);
									}
								}
							}
						}
					}

					if(!empty($groupid)) {
						$group = $this->group_model->getById($groupid);
						$groupitemdata = array(
								'appid'	=> $app->id,
								'eventid'	=> $event->id,
								'groupid'	=> $group->id,
								'itemtable'	=> 'speaker',
								'itemid'	=> $newid,
								'displaytype' => $group->displaytype
							);
						$this->general_model->insert('groupitem', $groupitemdata);
					} 
                                        
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/speakerimages/".$newid)){
						mkdir($this->config->item('imagespath') . "upload/speakerimages/".$newid, 0775, TRUE);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/speakerimages/'.$newid;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('imageurl')) {
						$image = ""; //No image uploaded!
						if($_FILES['imageurl']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/speakerimages/'. $newid . '/' . $returndata['file_name'];
					}

					$tags = $this->input->post('mytagsulselect');
					foreach ($tags as $tag) {
						$this->general_model->insert('tc_tag', array(
							'appid' => $app->id,
							'itemtype' => 'speaker',
							'itemid' => $newid,
							'tag' => $tag
						));
					}

					$data = array(
							"eventid"			=> $event->id,
							"name"              => set_value('name_'.$app->defaultlanguage),
							"company"			=> $this->input->post('company'),
							"function"			=> set_value('function'),
							"description"		=> set_value('description_'.$app->defaultlanguage),
							"imageurl"			=> $image,
							"order"				=> set_value('order')
						);
					if($newid = $this->general_model->update('speaker', $newid, $data)){
						$this->session->set_flashdata('event_feedback', 'The speaker has successfully been add!');
						_updateTimeStamp($event->id);
						if($error == '') {
							if($groupid != '') {
								redirect('groups/view/'.$groupid.'/event/'.$event->id.'/speakers');
							}
							redirect('speakers/event/'.$event->id);
						} else {
							//image error
							redirect('speakers/edit/'.$newid.'/event?error=image');
						}
					} else {
						//image error
						redirect('speakers/edit/'.$newid.'/event?error=image');
					}
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
			}
		}
	
		$cdata['content'] 		= $this->load->view('c_speaker_add', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "speakers/event/".$event->id, __("Add new") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']	= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "speakers/event/".$event->id, __("Add new") => $this->uri->uri_string());
		}
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'speakers');
		$this->load->view('master', $cdata);
	}

	function image_rules($str){
		$filename = 'image';
		return image_check($filename, $_FILES);
	}  
	
	function edit($id, $type = "event", $object = 'speaker', $groupid = '', $basegroupid = '') {
		$this->load->library('form_validation');
		$error = "";
		$speaker = $this->speaker_model->getById($id);
		$event = $this->event_model->getbyid($speaker->eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		
		$this->iframeurl = 'speakers/event/'.$event->id;

		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$launcher = $this->module_mdl->getLauncher(40, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;
		$metadata = $this->metadata_model->getMetadata($launcher, 'speaker', $speaker->id, $app);		

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'speaker' GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}
		$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'speaker'");
		if($tags->num_rows() == 0) {
			$tags = array();
		} else {
			$tagz = array();
			foreach ($tags->result() as $tag) {
				$tagz[] = $tag;
			}
			$tags = $tagz;
		}

		//validation
		if($this->input->post('mytagsulselect') != null) {
			$postedtags = $this->input->post('mytagsulselect');
		} else {
			$postedtags = array();
		}

		$maincat = $this->speaker_model->getMainCategorieGroup($event->id);

		if(empty($basegroupid)) {
			$basegroupid = $maincat;
		}

		$catshtml = '';
		$categories = array();
		$currentCats = '';
		if($maincat != false) {
			$this->speaker_model->htmlarray = array();
			$catshtml = $this->speaker_model->display_children($basegroupid,0,$speaker->eventid, $speaker->id);
			$currentCats = $this->speaker_model->groupids;
			$this->speaker_model->groupids = '';
		}

		if(!empty($groupid)) {
			$group = $this->group_model->getById($groupid);
			$parents = $this->group_model->getParents($groupid);
		}
	
		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                $this->form_validation->set_rules('description_'.$language->key, 'description', 'trim');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}
            }
            $this->form_validation->set_rules('company', 'company', 'trim');
            $this->form_validation->set_rules('function', 'function', 'trim');
            $this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
            $this->form_validation->set_rules('order', 'order', 'trim');
		
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {
				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/speakerimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/speakerimages/".$id, 0755, TRUE);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/speakerimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('imageurl')) {
					$image = $speaker->imageurl; //No image uploaded!
					if($_FILES['imageurl']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/speakerimages/'. $id . '/' . $returndata['file_name'];
				}

				$data = array(
						"eventid"			=> $event->id,
						"name"              => set_value('name_'.$app->defaultlanguage),
						"company"			=> $this->input->post('company'),
						"function"			=> set_value('function'),
						"description"		=> set_value('description_'.$app->defaultlanguage),
						"imageurl"			=> $image,
						"order"				=> set_value('order')
					);
				
				if($newid = $this->general_model->update('speaker', $id, $data)){
					$this->session->set_flashdata('event_feedback', 'The speaker has successfully been add!');                       
					foreach($languages as $language){
						$nameSuccess = $this->language_model->updateTranslation('speaker', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
						if($nameSuccess == false || $nameSuccess == null) {
							$this->language_model->addTranslation('speaker', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
						}
						$descrSuccess = $this->language_model->updateTranslation('speaker', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
						if($descrSuccess == false || $descrSuccess == null) {
							$this->language_model->addTranslation('speaker', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
						}
						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								$postfield = _getPostedField($m, $_POST, $_FILES, $language);
								if($postfield !== false) {
									if(_validateInputField($m, $postfield)) {
										_saveInputField($m, $postfield, 'speaker', $id, $app, $language);
									}
								}
							}
						}
					}

					$this->db->where('itemtype', 'speaker');
					$this->db->where('itemid', $id);
					$this->db->delete('tc_tag');
					$tags = $this->input->post('mytagsulselect');
					foreach ($tags as $tag) {
						$this->general_model->insert('tc_tag', array(
							'appid' => $app->id,
							'itemtype' => 'speaker',
							'itemid' => $id,
							'tag' => $tag
						));
					}

					if((isset($groupid) && $groupid != 0) || $maincat != false) {
						//groupitems
						if($this->input->post('groups') != null && $this->input->post('groups') != '') {
							$groups = $this->input->post('groups');
							$this->db->query("DELETE FROM groupitem WHERE itemid = $speaker->id AND itemtable = 'speaker'");
							foreach($groups as $group) {
								if($group != '') {
									$group2 = $this->group_model->getById($group);
									$groupitemdata = array(
											'appid'	=> $app->id,
											'eventid'	=> $speaker->eventid,
											'groupid'	=> $group2->id,
											'itemtable'	=> 'speaker',
											'itemid'	=> $speaker->id	,
											'displaytype' => $group2->displaytype
										);
									$this->general_model->insert('groupitem', $groupitemdata);
								}
							}
						}
					}

					_updateTimeStamp($event->id);
					if($error == '') {
						if($groupid != '') {
							redirect('groups/view/'.$groupid.'/event/'.$event->id.'/speakers');
						}
						redirect('speakers/event/'.$event->id);
					} else {
						//image error
						redirect('speakers/edit/'.$id.'/event?error=image');
					}
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
			}
		}
	
		$cdata['content'] 		= $this->load->view('c_speaker_edit', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'speaker' => $speaker, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags, 'maincat' => $maincat, 'catshtml' => $catshtml, 'currentCats' => $currentCats), TRUE);
		if(!empty($groupid)) {
			$crumbarray = array($event->name => 'event/view/'.$event->id);
			$currentCats = explode('/', $currentCats);
			foreach($parents as $p) {
				$p->name = str_replace('speakercategories', 'Speakers', $p->name);
				if(strtolower($p->name) != 'speakercategories') {
					$crumbarray[$p->name] = 'groups/view/'.$p->id.'/event/'.$event->id.'/speakers/'.$basegroupid;
				}
			}
			$group->name = str_replace('speakercategories', 'Speakers', $group->name);
			$crumbarray[$group->name] = 'groups/view/'.$group->id.'/event/'.$event->id.'/speakers/'.$basegroupid;
			$crumbarray[__("Edit: ") . $speaker->name] = $this->uri->uri_string();
			$cdata['crumb']			= $crumbarray;
		} else {
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "speakers/event/".$event->id, $speaker->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "speakers/event/".$event->id, $speaker->name => $this->uri->uri_string());
			}
		}
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'speakers');
		$this->load->view('master', $cdata);
	}
	
	function delete($id, $type = "event") {
		$speaker = $this->speaker_model->getById($id);
		$event = $this->event_model->getbyid($speaker->eventid);
		$app = _actionAllowed($event, 'event','returnApp');
        $this->language_model->removeTranslations('speaker', $id);
		if($this->general_model->remove('speaker', $id)){
			if($speaker->imageurl != '') {
				$this->deleteimage($speaker->imageurl);
			}
			
			$this->session->set_flashdata('event_feedback', __('The speaker has successfully been deleted'));
			_updateTimeStamp($speaker->eventid);
			redirect('speakers/event/'.$speaker->eventid);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('speakers/event/'.$speaker->eventid);
		}
	}
	
	function deleteimage($imageurl) {
		// unlink file itself
		unlink($imageurl);
		// unlink cache-files
		$info = pathinfo($imageurl);
		$file_name =  basename($imageurl,'.'.$info['extension']);
		array_map("unlink", glob("../cache/*".$file_name));
	}
	
	function autocomplete($field) {
		//In controller
		$keyword = $this->input->post('term');
		
		$data['response'] = 'false'; //Set default response
		
		$query = _autocomplete('speaker', $field, $keyword);
		
		if(count($query) > 0){
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach($query as $row){
				$data['message'][] = array('label'=> $row->{$field}, 'value'=> $row->{$field}); //Add a row to array
			}
		}
		echo json_encode($data);
	}
	
	function autocompletetags() {
		//In controller
		$keyword = $this->input->post('term');
		
		$data['response'] = 'false'; //Set default response
		
		$query = _autocompletetags(_currentApp()->id, $keyword);
		$keys = array();
		if(count($query) > 0){
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach($query as $row){
				if(!in_array($row->tag,$keys)) {
					$data['message'][] = array('label'=> $row->tag, 'value'=> $row->tag); //Add a row to array
					$keys[] = $row->tag;
				}
			}
		}

		echo json_encode($data);
	}
	
	function removeimage($id, $type = 'event') {
		$item = $this->speaker_model->getById($id);
		$event = $this->event_model->getbyid($item->eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		if(!file_exists($this->config->item('imagespath') . $item->imageurl)) redirect('events');

		// Delete image + generated thumbs
		unlink($this->config->item('imagespath') . $item->imageurl);
		$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

		$data = array('imageurl' => '');
		$this->general_model->update('speaker', $id, $data);

		_updateTimeStamp($event->id);
		redirect('speakers/event/'.$event->id);
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'speaker');
    }   
}