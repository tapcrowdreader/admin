<?php
/**
 * Tapcrowd Account Controller
 *
 * Manage account data. Implements authentication subsystem
 * @author Tom Van de Putte
 * @date 2012-11-29
 */

if(!defined('BASEPATH')) exit(__('No direct script access'));

/**
 * Account Controller Class
 */
class Account extends CI_Controller
{
	private $_model;
	private $_module_settings;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		_authed();		# Check for authentication

		# Load  account model
		require_once dirname(__DIR__) . '/models/account_model.php';
		$this->_account_model = \Tapcrowd\Model\Account::getInstance();
		$this->_session_model = \Tapcrowd\Model\Session::getInstance();

		# Module settings
		$this->_module_settings = (object)array(
			'singular' => 'Account',
			'plural' => 'Accounts',
			'add_url' => 'account/add',
			'headers' => array(
				__('Fullname') => 'fullname',
				__('Email') => 'email',
				__('Channel') => 'channel',
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'account/edit/--id--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'account/delete/--id--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'toolbar' => array(
				(object)array(
					'title' => __('My Account'),
					'href' => '/account/read',
					'icon_class' => 'icon-user',
					'btn_class' => '',
				),
			),
		);
	}

	/**
	 * Default method (when no specific method is given)
	 */
	public function index()
	{
		$this->read();
	}

	/**
	 * Lists Accounts for the given user
	 *
	 * Filters on accounts depending on role: channel admin see all accounts
	 * (Tapcrowd Channel admins see all accounts in the system)
	 */
	public function browse( $accountId, $query = null, $offset = 0, $limit = 1000 )
	{
		$args['data'] = array();
		try {
			if(empty($accountId)) $accountId = _currentUser()->accountId;
			$args['data'] = $this->_account_model->getAccounts($accountId);
		} catch(\Tapcrowd\AccessViolation $e) {
			alert(__('You cannot access this account'), 'error');
		} catch(\Exception $e) {
			alert(__('Something went wrong'), 'error');
		}
		$this->_outputHTML( 'tc_listview', $args, null);
	}

	/**
	 * Displays the Account Info screen; If no accountId given, calls currentUser()
	 *
	 * @param int $accountId The account identifier
	 */
	public function read( $accountId )
	{
		$account = $this->_getAccount($accountId);
		$this->_outputHTML( 'c_account', array('account' => $account), null);
	}

	/**
	 * Displays the Create Account screen
	 */
	public function add()
	{
		if(filter_input(INPUT_SERVER,'REQUEST_METHOD') == 'POST') {

			# Sanitize input
			$input = (object)$this->_validateFormInput();

			# Save Account
			if(count(Notifications::getInstance()) === 0) {
				try {
					# Create new Account/login
					$this->_session_model->saveLogin( $input->login, $input->pass, $input->fullname, $input->email);

					$edit_url = 'account/edit/'.$account->accountId;

					alert(__('Created new Account %s', $input->login), 'success');
					redirect('/account/browse');
				} catch(\Exception $e) {
					alert(__('Could not create new Account'), 'error');
				}
			}

			#
			# In HTML Mode return account form
			#
			$account = new \Tapcrowd\Account();
			foreach($input as $k => $v) if(property_exists($account, $k)) $account->$k = $v;
			$rights = $input->apprights;
		}
		elseif(filter_input(INPUT_SERVER,'REQUEST_METHOD') == 'GET') {
			$account = new \Tapcrowd\Account();
			$rights = array();
		}
		else {
			header('HTTP/1.1 405 Method Not Allowed');
			exit;
		}

		$apps = $this->_account_model->getApplications( _currentUser() );

		$params = array(
			'account' => $account,
			'apps' => $this->_account_model->getApplications( _currentUser() ),
			'rights' => $rights,
			'channels' => \Tapcrowd\Model\Channel::getInstance()->listChannels(),
		);
		$this->_outputHTML( 'c_account_form', $params, null);
	}

	/**
	 * Displays Edit Account screen for given account
	 *
	 * @param int $accountId The account identifier
	 */
	public function edit( $accountId )
	{
		if(filter_input(INPUT_SERVER,'REQUEST_METHOD') == 'POST') {

			# Verify access rights && Sanitize input
			$account = $this->_getAccount($accountId);
			$input = (object)$this->_validateFormInput($account);

			# Save Account
			if(count(Notifications::getInstance()) === 0) {
				try {
					$pass = $input->pass;

					# If login has changed, update login
					if($input->login != $account->login) {
						$this->_session_model->renameLogin( $account->login, $input->login);
						$account->login = $input->login;
					}

					# Get changed parameters and update if any...
					foreach(array('pass','email','fullname') as $k) {
						$$k = (empty($input->$k) || ($input->$k == $account->$k))? null : $input->$k;
					}
					if($pass || $fullname || $email) {
						$this->_session_model->saveLogin( $account->login, $pass, $input->fullname, $input->email);
					}

					# Update apprights
					if($input->apprights !== null) {
						$this->_account_model->setEntityRights( $account, 'application', $input->apprights);
					}

					$edit_url = 'account/edit/'.$account->accountId;
					alert(__('Updated Account <a href="%s">%s</a>', $edit_url, $account->login), 'success');
					redirect('/account/browse');
				} catch(\InvalidArgumentException $e) {
					alert(__('Could not update Account; %s', $e->getMessage()), 'error');
				} catch(\Tapcrowd\AccessViolation $e) {
					alert(__('Access Rights Violation : %s', $e->getMessage()), 'error');
				} catch(\Exception $e) {
					alert(__('Could not update Account'), 'error');
				}
			}

			#
			# In HTML Mode return account form
			#
			foreach($input as $k => $v) if(property_exists($account, $k)) $account->$k = $v;
		}
		elseif(filter_input(INPUT_SERVER,'REQUEST_METHOD') == 'GET') {
			$account = $this->_getAccount($accountId);
		}
		else {
			header('HTTP/1.1 405 Method Not Allowed');
			exit;
		}

		$params = array(
			'account' => $account,
			'apps' => $this->_account_model->getApplications( _currentUser() ),
			'rights' => $this->_account_model->getEntityRights( $account ),
			'channels' => \Tapcrowd\Model\Channel::getInstance()->listChannels(),
		);
		$this->_outputHTML( 'c_account_form', $params, null);
	}

	/**
	 * Removes the given account
	 * @param int $accountId The account identifier
	 */
	public function delete( $accountId )
	{
		try {
			$this->_account_model->removeAccount($accountId);
			alert(__('Account #%d has been removed', $accountId), 'success');
		} catch(\InvalidArgumentException $e) {
			alert(__('Unknown Account #%d', $accountId), 'error');
		} catch(\Tapcrowd\AccessViolation $e) {
			alert(__('You cannot remove this account'), 'error');
		} catch(\Exception $e) {
			alert(__('Account #%d could not be removed', $accountId), 'error');
		}
		redirect('/account/browse');
	}

	/**
	 * Private Method to output html
	 * @param string $view The html view
	 * @param array $params Optional; The parameters to pass through
	 * @param array $crumb Optional; The breadcrumb stages
	 */
	private function _outputHTML( $view, $params = null, $crumb = null )
	{
		# Add default parameters
		$params['settings'] = $this->_module_settings;
		$params['currentAccount'] = _currentUser();

		$this->load->view('master', array(
			'content' => $this->load->view($view, $params, true),
			'sidebar' => null,
			'crumb' => $crumb,
			'modules' => '',
		));
	}

	/**
	 * Tries to get the account object
	 *
	 * @return \Tapcrowd\Account|null
	 */
	private function _getAccount( $accountId = null )
	{
		$account = null;
		if(empty($accountId)) {
			$account = _currentUser();
		} else {
			# Get account
			try {
				$account = $this->_account_model->getAccount($accountId);
			} catch(\Tapcrowd\AccessViolation $e) {
				alert(__('You cannot access this account'), 'error');
			} catch(\Exception $e) {
				alert(__('Invalid Account Identifier'), 'error');
			}
		}
		return $account;
	}

	/**
	 * Validates the POST input for the Account form
	 *
	 * @param \Tapcrowd\Account $account Optional account to match with
	 * @return array
	 */
	private function _validateFormInput( \Tapcrowd\Account &$account = null )
	{
		# Sanitize input
		$passfilter = array('filter' => FILTER_UNSAFE_RAW, 'flags' => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		$input = filter_input_array(INPUT_POST, array(
			'account_fullname' => FILTER_SANITIZE_STRING,
			'account_login' => FILTER_SANITIZE_STRING,
			'account_email' => FILTER_SANITIZE_EMAIL,
			'account_pass' => $passfilter,
			'account_pass2' => $passfilter,
			'account_apprights' => array('filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY),
		));

		# Password
		if(isset($input['account_pass']) && $input['account_pass'] != $input['account_pass2']) {
			alert(__('Passwords do not match'), 'error');
		}

		# Empty password
		if($account === null && empty($input['account_pass'])) {
			alert(__('Password is a required field for new Accounts'), 'error');
		}

		# Name, email and login
		foreach(array('fullname','login','email') as $f) {
			if(empty($input['account_'.$f])) {
				alert(__('Field %s should not be empty', ucfirst($f)), 'error');
			}
		}

		# Duplicate Login : If no account is given or login is already used by another account
		if(!empty($input['account_login']) && (!isset($account) || ($input['account_login'] != $account->login)))
		{
			$accountId = $this->_session_model->getAccountForLogin( $input['account_login'] );
			if((isset($account) && $accountId && $accountId !== $account->accountId) || ($accountId !== null)) {
				alert(__('Login "%s" already taken', $input['account_login']), 'error');
			}
		}

		# Collect output
		$output = array();
		foreach($input as $k => $v) $output[substr($k,8)] = $v;
		return $output;
	}
}
