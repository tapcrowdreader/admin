<?php
class getTimestamp extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $moduleid = '', $deviceid = '', $devicetype = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$moduleid = ($this->input->post('moduleid') ? $this->input->post('moduleid') : 0);
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $moduleid);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $moduleid);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function event($obj, $eventid, $moduleid) {		
		$res = $obj->db->query("SELECT * FROM module WHERE event = $eventid AND moduletype = $moduleid");
		if($res->num_rows() == 0) return array();
		return array('timestamp' => $res->row()->timestamp);
	}
	
	function venue($obj, $venueid, $moduleid) {
		$res = $obj->db->query("SELECT * FROM module WHERE venue = $venueid AND moduletype = $moduleid");
		if($res->num_rows() == 0) return array();
		return array('timestamp' => $res->row()->timestamp);
	}
}