<?php 

class getAppVenues extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getAppVenues() {
		parent::MY_Controller();
	}
	
	function index($appid = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		// GET APP INFO
		$qry_app = $this->db->query("SELECT app.id, app.organizerid, app.apptypeid, at.name as apptype, app.name, app.info, app.advancedFilter, defaultlanguage
			FROM app 
			LEFT JOIN apptype at ON (app.apptypeid = at.id)
			WHERE app.id = $appid");
		if($qry_app->num_rows() == 0) {
			// UNKNOWN APPID
			echo "ERROR";
		} else {
			$appinfo = $qry_app->row();
			$data['app'] = $appinfo;
			
			// GET APP VENUES
			$qry_venues = $this->db->query("SELECT vnu.* 
				FROM venue vnu 
				LEFT JOIN appvenue av ON (vnu.id = av.venueid) 
				WHERE av.appid = $appid AND vnu.deleted = 0 
				ORDER BY id DESC");
			if($qry_venues->num_rows() == 0) {
				// NO EVENTS
				$data['venues'] = array();
			} else {
				foreach($qry_venues->result() as $row) {
					if($lang != '' && $lang != $appinfo->defaultlanguage) {
						$transname = _getTranslation($this,'venue',$row->id,'name', $lang);
						if($transname != null) {
						    $row->name = $transname->translation;
						}
					    $transdescription = _getTranslation($this,'venue',$row->id,'info', $lang);
					    if($transdescription != null) {
					        $row->info= $transdescription->translation;
					    }
					}
				}
				$data['venues'] = $qry_venues->result();
			}
			
			header('Content-type: application/json');
			echo json_encode($data);
		}
	}

}