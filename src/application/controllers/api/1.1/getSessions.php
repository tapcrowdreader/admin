<?php 

class getSessions extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getSessions() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '', $timestamp = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		$timestamp = ($this->input->post('timestamp') ? $this->input->post('timestamp') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang, $timestamp);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '', $timestamp = '') {
		$latest = 0;
		if($timestamp != '' && $timestamp != null) {
			$qry_timestamp = $obj->db->query("SELECT timestamp FROM module WHERE event = $eventid AND moduletype = 10");
			$latest = $qry_timestamp->row()->timestamp;
		}
		
		if($timestamp != null && $timestamp != '' && $timestamp >= $latest) {
			return array('timestamp' => $latest);
		} else {
		$app = _getAppFromEvent($obj, $eventid);
		$checkArtists = _checkArtistsForApp($obj, $app->id);
//		$checkMetadata = _checkMetadataForApp($obj, $app->id);
		$languages = _getLanguagesOfApp($obj, $app->id);
//		$res = $obj->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid ORDER BY sessiongroup.order ASC");
//		if($res->num_rows() == 0) return array();
//		
//		// GET SESSIONS PER SESSIONGROUP
//		foreach ($res->result() as $group) {
			$sess = $obj->db->query("SELECT id, eventid, name, session.order, description, starttime, endtime, speaker, location, sessiongroupid, mapid, xpos, ypos, imageurl, allowAddToFavorites, allowAddToAgenda, url, twitter FROM session WHERE eventid = $eventid ORDER BY session.order ASC");
//			$group->name = htmlspecialchars_decode($group->name, ENT_NOQUOTES);
			foreach($sess->result() as $sessie){
				// ADD EVENTID FROM GROUP
				//$sessie->eventid = $group->eventid;
				
				$sessie->date = date('d/m/Y', strtotime($sessie->starttime));
				$sessie->starttime = date('H', strtotime($sessie->starttime)) . 'h' . date('i', strtotime($sessie->starttime));
				$sessie->endtime = date('H', strtotime($sessie->endtime)) . 'h' . date('i', strtotime($sessie->endtime));
				$sessie->name = htmlspecialchars_decode($sessie->name, ENT_NOQUOTES);
				$sessie->description = htmlspecialchars_decode($sessie->description, ENT_NOQUOTES);
				$sessie->conferencebag = $sessie->allowAddToFavorites;
				unset($sessie->allowAddToFavorites);
				
				if($sessie->imageurl != '') {
					$sessie->imagethumb = base_url() . _api_image_crop($obj, $sessie->imageurl, 70, 70);
					$sessie->imageurl = base_url() . $sessie->imageurl;
				} else {
					$sessie->imagethumb = '';
				}
				
				if($lang != '') {
					$transname = _getTranslation($obj, 'session',$sessie->id,'name', $lang);
					if($transname != null) {
						$sessie->name = $transname->translation;
					}
					$transdescription = _getTranslation($obj, 'session',$sessie->id,'description', $lang);
					if($transdescription != null) {
						$sessie->description = $transdescription->translation;
					}
				}
				
				if($checkArtists) {
					$artists = $obj->db->query("SELECT artist.id, artist.name FROM artistsessions ass JOIN artist ON (ass.artistid = artist.id) WHERE ass.sessionid = $sessie->id ORDER BY artist.name");
					if($artists->result() != null && $artists->result() != false && $artists->num_rows() > 0) {
						$sessie->artists = $artists->result();
					}
				}
				
//				if($checkMetadata) {
//					$metadata = _get_meta_data($obj, "session", "$sessie->id");
//					if($metadata && count($metadata) > 0) $sessie->metadata = $metadata;
//				}
				
			}
//			$group->sessions = $sess->result();
//		}
		
		return $sess->result();
		}
	}
	
	function venue($obj, $venueid, $lang = '') {
		$res = $obj->db->query("SELECT * FROM sessiongroup WHERE venueid = $venueid ORDER BY sessiongroup.order ASC");
		if($res->num_rows() == 0) return array();
		$app = _getAppFromEvent($obj, $eventid);
		
		// GET SESSIONS PER SESSIONGROUP
		foreach ($res->result() as $group) {
			$sess = $obj->db->query("SELECT id, venueid, name, description, starttime, endtime, speaker, location, sessiongroupid, imageurl, allowAddToFavorites, allowAddToAgenda, url, twitter FROM session WHERE sessiongroupid = $group->id ORDER BY session.order ASC");
			$group->name = htmlspecialchars_decode($group->name, ENT_NOQUOTES);
			foreach($sess->result() as $sessie){
				$sessie->date = date('d/m/Y', strtotime($sessie->starttime));
				$sessie->starttime = date('H', strtotime($sessie->starttime)) . 'h' . date('i', strtotime($sessie->starttime));
				$sessie->endtime = date('H', strtotime($sessie->endtime)) . 'h' . date('i', strtotime($sessie->endtime));
				$sessie->name = htmlspecialchars_decode($sessie->name, ENT_NOQUOTES);
				$sessie->description = htmlspecialchars_decode($sessie->description, ENT_NOQUOTES);
				$sessie->conferencebag = $sessie->allowAddToFavorites;
				unset($sessie->allowAddToFavorites);
				
				if($sessie->imageurl != '') {
					$sessie->imagethumb = base_url() . _api_image_crop($sessie->imageurl, 110, 110);
					$sessie->imageurl = base_url() . $sessie->imageurl;
				} else {
					$sessie->imagethumb = '';
				}
			}
			$group->sessions = $sess->result();
		}
		
		return $res->result();
	}
	
}