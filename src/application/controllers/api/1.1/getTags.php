<?php
class getTags extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($appid = '', $deviceid = '', $devicetype = '') {
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		$data = array();
		$tags = $this->db->query("SELECT * FROM tag WHERE appid = $appid")->result();
		$data = $tags;
		
		header('Content-type: application/json');
		echo json_encode($data);
	}
}