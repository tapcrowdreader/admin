<?php 

class pushFavourite extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function pushFavourite() {
		parent::MY_Controller();
	}
	
	function index($key = '', $email = '', $exhibitorid = '', $sessionid = '', $extra = '') {
		// $key, $email, $eventid, $exhibitorid, $sessionid
		$key = ($this->input->post('key') ? $this->input->post('key') : '');
		$email = ($this->input->post('email') ? $this->input->post('email') : '');
		$eventid = ($this->input->post('eventid') ? $this->input->post('eventid') : 0);
		$exhibitorid = ($this->input->post('exhibitorid') ? $this->input->post('exhibitorid') : 0);
		$sessionid = ($this->input->post('sessionid') ? $this->input->post('sessionid') : 0);
		$extra = ($this->input->post('extra') ? $this->input->post('extra') : 0);
		
		if($key != "" && $email != '' && $eventid != ""){
			// Secret string opbouwen 
			$secret = md5("tcadm" . $eventid);
			
			// Secret vergelijken met meegestuurde
			if($key == $secret) {
				
				// Insert favourite
				$userdata = array(
						'useremail' 	=> $email,
						'eventid' 		=> $eventid,
						'exhibitorid' 	=> $exhibitorid,
						'sessionid' 	=> $sessionid,
						'extra'			=> $extra
					);
				
				if($this->db->insert('favorites', $userdata)){
					echo "OK";
				} else {
					echo "ERROR, insert failed";
				}
			} else {
				echo "ERROR, key != secret";
			}
		} else {
			echo "ERROR key email or eventid is empty";
		}
	}
	
}