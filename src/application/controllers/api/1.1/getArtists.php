<?php 

class getArtists extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getArtists() {
		parent::MY_Controller();
	}
	
	function index($appid = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		if($appid == '' || $appid == null) { echo "ERROR"; exit(); }
		
		$res = $this->db->query("SELECT * FROM artist WHERE appid = $appid ORDER BY name ASC");
		if($res->num_rows() == 0) return array();
		$artistids = array();
		foreach ($res->result() as $row) {
			$row->name = str_replace('&#39;', "'",htmlspecialchars_decode($row->name, ENT_NOQUOTES));
			
			// * GET ARSIST SESSIONS * //
			$artistids[] = $row->id;
			
			if($lang != '' && $lang != $app->defaultlanguage) {
				$transdescription = _getTranslation($obj, 'artist',$row->id,'description', $lang);
				if($transdescription != null) {
					$row->description = $transdescription->translation;
				}
			}
		}
		
		header('Content-type: application/json');

		$data = array();
		$data['artists'] = $res->result();
		
		$sessions = $this->_getArtistSessions($this, $artistids, $lang);
		$data['sessions'] = $sessions;
		
		//metadata
		$metadata = $this->db->query("SELECT * FROM metadata WHERE appid = $appid AND `table` = 'artist'")->result();
		$data['metadata'] = $metadata;
		
		echo json_encode($data);
		
	}
	
	protected function _getArtistSessions($obj, $artistids, $lang = '') {
		$artistids = implode($artistids, ',');
		$res = $obj->db->query("SELECT sess.*, artist.id as artistid FROM artistsessions ases LEFT JOIN artist ON ases.artistid = artist.id LEFT JOIN session sess ON ases.sessionid = sess.id WHERE ases.artistid IN ($artistids) ORDER BY sess.order asc, sess.name asc");
		if($res->num_rows() == 0) return array();
		foreach($res->result() as $row) {
			$transname = _getTranslation($obj, 'session',$row->id,'name', $lang);
			if($transname != null) {
				$row->name = $transname->translation;
			}
			$transdescription = _getTranslation($obj, 'session',$row->id,'description', $lang);
			if($transdescription != null) {
				$row->description = $transdescription->translation;
			}
		}
		return $res->result();
	}
	/*
	protected function _getArtistMetaData($obj, $artistid) {
		$res = $obj->db->query("SELECT md.key, md.value FROM metadata md WHERE md.table = 'artist' AND md.identifier = $artistid");
		$result = array();
		foreach ($res->result() as $row) {
			$result[$row->key] = $row->value;
		}
		return $result;
	}
	*/
}