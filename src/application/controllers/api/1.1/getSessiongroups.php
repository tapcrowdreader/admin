<?php 

class getSessiongroups extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '') {
		$res = $obj->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid ORDER BY sessiongroup.order ASC");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
	function venue($obj, $venueid, $lang = '') {
		$res = $obj->db->query("SELECT * FROM sessiongroup WHERE venueid = $venueid ORDER BY sessiongroup.order ASC");
		if($res->num_rows() == 0) return array();
		//$app = _getAppFromEvent($obj, $eventid);
		
		// GET SESSIONS PER SESSIONGROUP
		foreach ($res->result() as $group) {
			$sess = $obj->db->query("SELECT id, name, description, starttime, endtime, speaker, location, sessiongroupid, imageurl, allowAddToFavorites, allowAddToAgenda, url, twitter FROM session WHERE sessiongroupid = $group->id ORDER BY session.order ASC");
			$group->name = htmlspecialchars_decode($group->name, ENT_NOQUOTES);
			foreach($sess->result() as $sessie){
				$sessie->date = date('d/m/Y', strtotime($sessie->starttime));
				$sessie->starttime = date('H', strtotime($sessie->starttime)) . 'h' . date('i', strtotime($sessie->starttime));
				$sessie->endtime = date('H', strtotime($sessie->endtime)) . 'h' . date('i', strtotime($sessie->endtime));
				$sessie->name = htmlspecialchars_decode($sessie->name, ENT_NOQUOTES);
				$sessie->description = htmlspecialchars_decode($sessie->description, ENT_NOQUOTES);
				$sessie->conferencebag = $sessie->allowAddToFavorites;
				unset($sessie->allowAddToFavorites);
				
				if($sessie->imageurl != '') {
					$sessie->imagethumb = base_url() . _api_image_crop($sessie->imageurl, 110, 110);
					$sessie->imageurl = base_url() . $sessie->imageurl;
				} else {
					$sessie->imagethumb = '';
				}
			}
			$group->sessions = $sess->result();
		}
		
		return $res->result();
	}
	
}