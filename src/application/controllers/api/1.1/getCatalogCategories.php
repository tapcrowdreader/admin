<?php 

class getCatalogCategories extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '') {
		$eventCategories = $obj->db->query("SELECT * FROM catalogcategory WHERE eventid = $eventid");
		if($eventCategories->num_rows() == 0) return array();
		return $eventCategories->result();
	}
	
	function venue($obj, $venueid, $lang = '') {
		$venueCategories = $obj->db->query("SELECT * FROM catalogcategory WHERE venueid = $venueid");
		if($venueCategories->num_rows() == 0) return array();
		return $venueCategories->result();
	}
	
}