<?php 

class getAppEvents extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getAppEvents() {
		parent::MY_Controller();
	}
	
	function index($appid = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		// GET APP INFO
		$qry_app = $this->db->query("SELECT app.id, app.organizerid, app.apptypeid, at.name as apptype, app.name, app.info, app.advancedFilter, app.defaultlanguage
			FROM app 
			LEFT JOIN apptype at ON (app.apptypeid = at.id)
			WHERE app.id = $appid");
		if($qry_app->num_rows() == 0) {
			// UNKNOWN APPID
			echo "ERROR";
		} else {
			$appinfo = $qry_app->row();
			$data['app'] = $appinfo;
			
			// GET APP EVENTS
			$qry_events = $this->db->query("SELECT evt.id, evt.organizerid, evt.venueid, evt.name, typ.name as eventtype, evt.datefrom, evt.dateto, evt.eventlogo, evt.description, evt.qrshare, evt.timestamp
				FROM event evt 
				LEFT JOIN appevent ae ON (evt.id = ae.eventid) 
				LEFT JOIN eventtype typ ON (typ.id = evt.eventtypeid) 
				WHERE ae.appid = $appid AND evt.active = 1 AND evt.deleted = 0 
				ORDER BY id DESC");
			if($qry_events->num_rows() == 0) {
				// NO EVENTS
				$data['events'] = array();
			} else {
				$languages = _getLanguagesOfApp($this, $appid);
				foreach($qry_events->result() as $event){
					$event->venue = $this->db->query("SELECT id, name, address, lat, lon, telephone, fax, email, info, travelinfo, timestamp FROM venue WHERE id = ".$event->venueid)->row();
					
					//translations
					if($lang != '' && $lang != $appinfo->defaultlanguage) {
					    $transname = _getTranslation($this,'event',$event->id,'name', $lang);
					    if($transname != null) {
					        $event->name = $transname->translation;
					    }
					    $transdescription = _getTranslation($this,'event',$event->id,'description', $lang);
					    if($transdescription != null) {
					        $event->description = $transdescription->translation;
					    }
					}
				}
				$data['events'] = $qry_events->result();
			}
			
			header('Content-type: application/json');
			echo json_encode($data);
		}
	}

}