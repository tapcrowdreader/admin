<?php 

class getExhibitorCategories extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '') {
		$eventBrands = $obj->db->query("SELECT * FROM exhibitorcategory WHERE eventid = $eventid");
		if($eventBrands->num_rows() == 0) return array();
		return $eventBrands->result();
	}
	
	function venue($obj, $venueid, $lang = '') {
		$venueBrands = $obj->db->query("SELECT * FROM exhibitorcategory WHERE venueid = $venueid");
		if($venueBrands->num_rows() == 0) return array();
		return $venueBrands->result();
	}
	
}