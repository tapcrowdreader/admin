<?php 

class getSocialmedia extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getSocialmedia() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id);
				break;
			case 'venue':
				$data = $this->venue($this, $id);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid) {
		$res = $obj->db->query("SELECT id, eventid, twitter, twithash, RSS, facebookid, facebookappid, postorwall FROM socialmedia WHERE eventid = $eventid LIMIT 1");
		if($res->num_rows() == 0) return array();
		$res->row()->twitfrom = $res->row()->twitter;
		$res->row()->twithash = $res->row()->twithash;
		$res->row()->attribute = $res->row()->postorwall;
		return array($res->row());
	}
	
	function venue($obj, $venueid) {
		$res = $obj->db->query("SELECT id, venueid, twitter, twithash, RSS, facebookid, facebookappid, postorwall FROM socialmedia WHERE venueid = $venueid LIMIT 1");
		if($res->num_rows() == 0) return array();
		$res->row()->twitfrom = $res->row()->twitter;
		$res->row()->twithash = $res->row()->twithash;
		$res->row()->attribute = $res->row()->postorwall;
		return array($res->row());
	}
	
}