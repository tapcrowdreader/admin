<?php 
/**
 * CheckStores returns a list of URLs showing availability in App-stores
 */
class checkStores extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function checkStores() {
		parent::MY_Controller();
	}
	
	function index($appid = '', $secret = '') {
		
		$secret = ($this->input->post('secret') ? $this->input->post('secret') : '');
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		
		$mysecret = md5("tcadm" . $appid); //'123456';
		if($mysecret == $secret) {
			$res = $this->db->query("SELECT * FROM appstores WHERE appid = $appid");
			if($res->num_rows() == 0) {
				$data = '';
			} else {
				$stores = $res->row();
				$data->stores = $stores;
			}
			
			header('Content-type: application/json');
			echo json_encode($data);
		} else {
			header('Content-type: application/json');
			echo json_encode('');
		}
	}
	
}