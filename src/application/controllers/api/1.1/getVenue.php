<?php 

class getVenue extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getVenue() {
		parent::MY_Controller();
	}
	
	function index($appid = '', $venueid = '', $deviceid = '', $devicetype = '', $lang = '') {
		$data = array();
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0); //'10';
		$venueid = ($this->input->post('venueid') ? $this->input->post('venueid') : 0); //'333';
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($appid == '' || $appid == null || $venueid == '' || $venueid == null) { echo "ERROR"; exit(); }
		
		// GET APP INFO
		$qry_app = $this->db->query("SELECT app.id, app.organizerid FROM app WHERE app.id = $appid");
		if($qry_app->num_rows() == 0) {
			// UNKNOWN APPID
			echo "ERROR";
		} else {
			$app = _getAppFromVenue($this, $venueid);
			$languages = _getLanguagesOfApp($this, $appid);
			// GET VENUE
			$qry_venue = $this->db->query("SELECT vnu.id, vnu.name, vnu.address, vnu.lat, vnu.lon, vnu.telephone, vnu.fax, vnu.email, vnu.info, vnu.travelinfo, vnu.timestamp, vnu.image1, vnu.image2, vnu.image3, vnu.image4, vnu.image5, vnu.website, vnu.twitterurl, vnu.facebookurl, vnu.toururl, vnu.vimeourl, vnu.order
				FROM venue vnu
				WHERE vnu.id = $venueid AND vnu.active = 1 AND vnu.deleted = 0");
			if($qry_venue->num_rows() == 0) {
				// UNKNOWN VENUE
				echo "ERROR";
			} else {
				
				// *** VENUE DATA *** //
				$venue = $qry_venue->row();
				for ($i=1; $i < 6; $i++) { 
					if($venue->{'image'.$i} != '') { 
						$venue->{'image'.$i} = base_url() . image_thumb($venue->{'image'.$i}, 427, 640); 
					}
				}
				
				//translations
				if($lang != '' && $lang != $app->defaultlanguage) {
				    $transname = _getTranslation($this,'venue',$venueid,'name', $lang);
				    if($transname != null) {
				        $venue->name = $transname->translation;
				    }
				    $transdescription = _getTranslation($this,'venue',$venueid,'info', $lang);
				    if($transdescription != null) {
				        $venue->info = $transdescription->translation;
				    }
				}
				
				$data['venue'] = $venue;
				// *** //
				
				// GET MODULES
				// *** ACTIVATED MODULES *** //
                $this->load->model('catalog_model');
				$modules = $this->db->query("SELECT moduletype.* FROM module LEFT JOIN moduletype ON (module.moduletype = moduletype.id) WHERE module.venue = $venueid")->result();
				// *** GET DATA PER MODULE *** //
				foreach($modules as $module) {
					if($module->controller == 'sponsors') {
						include($module->apicall . '.php');
						include('getSponsorgroups.php');
						//groups
						$sponsorgroupsController = new getSponsorgroups();
						$data['sponsorgroups'] = $sponsorgroupsController->venue($this, $venueid);

						//sessions
						$modcont = new $module->apicall();
						$moddata = $modcont->venue($this, $venueid);
						$data['sponsors'] = $moddata;

					} else {
						if ($module->apicall != '' && $module->apicall != 'getAbout'  && $module->name != 'Careers' && $module->name != 'Services' && $module->name != 'Projects'  && $module->apicall != 'getEvent') {
							require_once($module->apicall . '.php');
							$modcont = new $module->apicall();
							$moddata = $modcont->venue($this, $venueid, $lang);
							if($moddata != null) {
								$data[$module->controller] = $moddata;
							}
						}
					}
				}
				
                $tags = $this->db->query("SELECT * FROM tag WHERE venueid = $venueid")->result();
                $data['tags'] = $tags;
				
				header('Content-type: application/json');
				echo json_encode($data);
			}
		}
	}
	
}