<?php 

class getSponsorgroups extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '') {
		$res = $obj->db->query("SELECT * FROM sponsorgroup WHERE eventid = $eventid ORDER BY sponsorgroup.order ASC");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
	function venue($obj, $venueid, $lang = '') {
		$res = $obj->db->query("SELECT * FROM sponsorgroup WHERE venueid = $venueid ORDER BY sponsorgroup.order ASC");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
}