<?php 

class getAppBoekenbeurs extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($appid = '', $deviceid = '', $devicetype = '', $lang = '', $timestamp = '') {
		$data = array();
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		$timestamp = ($this->input->post('timestamp') ? $this->input->post('timestamp') : '');
		$eventvenues = array();
						
		// GET APP INFO
		$qry_app = $this->db->query("SELECT app.id, app.apptypeid, at.name as apptype, app.name, app.info, app.advancedFilter, app.defaultlanguage
			FROM app 
			LEFT JOIN apptype at ON (app.apptypeid = at.id)
			WHERE app.id = $appid");
		if($qry_app->num_rows() == 0) {
			// UNKNOWN APPID
			echo "ERROR";
		} else {
			$appinfo = $qry_app->row();
			$data['app'] = array($appinfo);

			$languages = _getLanguagesOfApp($this, $appid);
			$latestEvent = 0;
			$latestVenue = 0;
			if($timestamp != '' && $timestamp != null) {
				$qry_timestampEvent = $this->db->query("SELECT evt.id as id, evt.timestamp as timestamp
					FROM event evt 
					LEFT JOIN appevent ae ON (evt.id = ae.eventid)  
					WHERE ae.appid = $appid AND evt.active = 1 AND evt.deleted = 0 
					ORDER BY evt.timestamp DESC");
				$res = $qry_timestampEvent->row();
				if($res != false && $res != null) {
					$latestEvent = $res->timestamp;
				}

				$qry_timestampVenue = $this->db->query("SELECT vnu.id, vnu.timestamp 
					FROM venue vnu 
					LEFT JOIN appvenue av ON (vnu.id = av.venueid) 
					WHERE av.appid = $appid AND vnu.deleted = 0 
					ORDER BY vnu.timestamp DESC");
				$res = $qry_timestampVenue->row();
				if($res != false && $res != null) {
					$latestVenue = $res->timestamp;
				}

				$latest = $latestVenue;
				if($latestEvent > $latestVenue) {
					$latest = $latestEvent;
				}
			}
					
			if($timestamp != null && $timestamp != '' && $timestamp >= $latest) {
				header('Content-type: application/json');
				echo json_encode(array('timestamp' => $latest));
			} else {
			// GET APP EVENTS
			$qry_events = $this->db->query("SELECT evt.id, evt.venueid, evt.name, typ.name as eventtype, evt.datefrom, evt.dateto, evt.eventlogo, evt.description, evt.timestamp
				FROM event evt 
				LEFT JOIN appevent ae ON (evt.id = ae.eventid) 
				LEFT JOIN eventtype typ ON (typ.id = evt.eventtypeid) 
				WHERE ae.appid = $appid AND evt.active = 1 AND evt.deleted = 0 
				ORDER BY id DESC");
			$events = $qry_events->result();
			$eventids = array();
			foreach($events as $event) {
				$eventids[] = $event->id;
			}
			if($qry_events->num_rows() == 0) {
				// NO EVENTS
				$data['events'] = array();
			} else {
                if($qry_events->num_rows() > 1) {
				foreach($events as $event){
					$eventvenue = $this->db->query("SELECT id, name, address, lat, lon, telephone, fax, email, info FROM venue WHERE id = ".$event->venueid)->row();
					$eventvenue->eventid = $event->id;
					$eventvenues[] = $eventvenue;
					$event->eventlogo = ($event->eventlogo != '') ? base_url() . $event->eventlogo : '';
					$event->thumblogo = ($event->eventlogo != '' ? base_url() . image_crop(str_replace('upload/eventlogos', 'upload/eventlogos/big', $event->eventlogo), 128, 128) : '');
					
					//translations
					if($lang != '' && $lang != $appinfo->defaultlanguage) {
					    $transname = _getTranslation($this,'event',$event->id,'name', $lang);
					    if($transname != null) {
					        $event->name = $transname->translation;
					    }
					    $transdescription = _getTranslation($this,'event',$event->id,'description', $lang);
					    if($transdescription != null) {
					        $event->description = $transdescription->translation;
					    }
					}					
				}
				$data['events'] = $qry_events->result();
                } elseif($qry_events->num_rows() == 1) {
                    //get all of event
                    $event = $qry_events->row();
                    $eventid = $event->id;
                    // GET EVENT
                    $qry_event = $this->db->query("SELECT evt.id, evt.eventtypeid, evt.venueid, typ.name as eventtypename, evt.name, evt.datefrom, evt.dateto, evt.eventlogo, evt.description, evt.phonenr, evt.website 
                        FROM event evt
                        LEFT JOIN eventtype typ ON (typ.id = evt.eventtypeid) 
                        WHERE evt.id = $eventid AND evt.active = 1 AND evt.deleted = 0");
                    if($qry_event->num_rows() == 0) {
                        // UNKNOWN EVENT
                        echo "ERROR";
                    } else {
                        // *** EVENT DATA *** //
                        $event = $qry_event->row();
                        if($event->eventlogo != '') $event->eventlogo = base_url() . $event->eventlogo;
                        $event->eventlogobig = str_replace('upload/eventlogos', 'upload/eventlogos/big', $event->eventlogo);
                        // ADD VENUEDETAILS TO EVENT
                        $vnu = $this->db->query("SELECT id, name, address, lat, lon, travelinfo FROM venue WHERE id = $event->venueid");
                        if($vnu->num_rows() > 0) {
                            $eventvenue = $vnu->row();
							$eventvenue->eventid = $event->id;
                            $eventvenues[] = $eventvenue;
                            $event->thumblogo = ($event->eventlogo != '' ? base_url() . image_crop(str_replace('upload/eventlogos', 'upload/eventlogos/big', $event->eventlogo), 128, 128) : '');

                            $metadata = _get_meta_data($this, 'event', $event->id);
                            if($metadata && count($metadata) > 0) $event->metadata = $metadata;

                        } else {
                            $event->venue = array();
                        }
                        
                        //translations
                        if($lang != '' && $lang != $appinfo->defaultlanguage) {
                            $transname = _getTranslation($this,'event',$event->id,'name', $lang);
                            if($transname != null) {
                                $event->name = $transname->translation;
                            }
                            $transdescription = _getTranslation($this,'event',$event->id,'description', $lang);
                            if($transdescription != null) {
                                $event->description = $transdescription->translation;
                            }
                        }

                        $data['event'] = array($event);
                        // *** //

                        // GET MODULES
                        // *** ACTIVATED MODULES *** //
                        $modules = $this->db->query("SELECT moduletype.* FROM module LEFT JOIN moduletype ON (module.moduletype = moduletype.id) WHERE module.event = $eventid")->result();
                        // *** GET DATA PER MODULE *** //
                        foreach($modules as $module) {
							//load every module except schedule
							if($module->id != 12) {
								if ($module->apicall == 'getBrandsCategories'){
									include('getBrands.php');
									$modcont = new getBrands();
									$moddata = $modcont->event($this, $eventid);
									if($moddata != null) {
										//$data['brands']['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => 'getBrands');
										$data['brands'] = $moddata;
									}

									include('getCategories.php');
									$modcont = new getCategories();
									$moddata = $modcont->event($this, $eventid);
									if($moddata != null) {
										//$data['categories']['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => 'getCategories');
										$data['categories'] = $moddata;
									}
								} else if($module->controller == 'sessions') {
									include($module->apicall . '.php');
									include('getSessiongroups.php');
									//groups
									$sessiongroupsController = new getSessiongroups();
									$data['sessiongroups'] = $sessiongroupsController->event($this, $eventid);
									
								} else if ($module->apicall != '') {
									include($module->apicall . '.php');
									$modcont = new $module->apicall();
									$moddata = $modcont->event($this, $eventid);
									if($moddata != null) {
										if($module->controller == 'sessions') {
											$itemname = 'sessiongroups';
										} else {
											$itemname = $module->controller;
										}

										//$data[$itemname]['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => $module->apicall);
										$data[$itemname] = $moddata;
									}
								}
							}
                        }
						

						
						//get launcher
//						$launcherdata = $getLauncher->event($this, $eventid, $lang);
//						if($launcherdata != null) {
//							$data['launcher'] = $launcherdata;
//						}
                    }
                }
			}
			
			// GET APP VENUES
			$qry_venues = $this->db->query("SELECT vnu.* 
				FROM venue vnu 
				LEFT JOIN appvenue av ON (vnu.id = av.venueid) 
				WHERE av.appid = $appid AND vnu.deleted = 0 
				ORDER BY id DESC");
			//$languages = _getLanguagesOfApp($this, $appid);
			if($qry_venues->num_rows() == 0) {
				// NO EVENTS
				$data['venues'] = array();
			} else {
				foreach ($qry_venues->result() as $row) {
					//translations
					if($lang != '' && $lang != $appinfo->defaultlanguage) {
					    $transname = _getTranslation($this,'venue',$row->id,'name', $lang);
					    if($transname != null) {
					        $row->name = $transname->translation;
					    }
					    $transdescription = _getTranslation($this,'venue',$row->id,'info', $lang);
					    if($transdescription != null) {
					        $row->info = $transdescription->translation;
					    }
					}
					
					for ($i=1; $i < 6; $i++) { 
						if($row->{'image'.$i} != '') { 
							$row->{'image'.$i} = base_url() . image_thumb($row->{'image'.$i}, 427, 640); 
						}
					}
					$tags = $this->db->query("SELECT * FROM tag WHERE venueid = $row->id")->result();
					$row->tags = $tags;
				}
				$data['venues'] = $qry_venues->result();
			}
			
			foreach($eventvenues as $eventvenue) {
				$data['venues'][] = $eventvenue;
			}
            
			// GET APP ARTISTS
			$qry_artists = $this->db->query("SELECT * 
				FROM artist 
				WHERE appid = $appid
				ORDER BY id DESC");
			if($qry_artists->num_rows() == 0) {
				// NO EVENTS
				$data['artists'] = array();
			} else {
                $artists = $qry_artists->result();
                foreach ($artists as $artist) {
                    $qry_metadata = $this->db->query("SELECT `key`, `value` FROM metadata WHERE `table` = 'artist' AND `identifier` = $artist->id ");
                    $metadata = array();
                    foreach( $qry_metadata->result() as $row) {
                        $metadata[$row->key] = $row->value;
                    }
                    if($metadata != null && !empty($metadata)) {
                        $artist->metadata = $metadata;
                    }
                }
				$data['artists'] = $artists;
			}
			
			//get launcher
			include('getLauncher.php');
			$launchercont = new getLauncher();
			
			if($qry_events->num_rows() == 0) {
				//tags
				$tags = $this->db->query("SELECT * FROM tag WHERE appid = $appid AND eventid = 0 AND venueid = 0 AND sessionid = 0 AND exhibitorid = 0 AND catalogitemid = 0 AND newsitemid = 0")->result();
				$data['tags'] = $tags;
				
				//metadata
				$metadata = $this->db->query("SELECT * FROM metadata WHERE appid = $appid AND `table` = 'app'")->result();
				$data['metadata'] = $metadata;
				
				//launchers
				$launcherdata = $launchercont->app($this, $appid, $lang);
			} elseif($qry_events->num_rows() == 1) {
				$event = $qry_events->row();
				//tags
				$tags = $this->db->query("SELECT * FROM tag WHERE appid = $appid")->result();
				$data['tags'] = $tags;
				
				//metadata
				$metadata = $this->db->query("SELECT * FROM metadata WHERE appid = $appid")->result();
				$data['metadata'] = $metadata;
				
				//launchers
				$launcherdata = $launchercont->event($this, $event->id, $lang);
			} elseif($qry_events->num_rows() > 1) {
				//tags
				$tags = $this->db->query("SELECT * FROM tag WHERE appid = $appid AND venueid = 0 AND sessionid = 0 AND exhibitorid = 0 AND catalogitemid = 0 AND newsitemid = 0")->result();
				$data['tags'] = $tags;
				
				//metadata
				$metadata = $this->db->query("SELECT * FROM metadata WHERE appid = $appid AND `table` = 'app'")->result();
				$data['metadata'] = $metadata;
				
				//launchers
				$launcherdata = $launchercont->app($this, $appid, $lang);
			}
			
			//launcher
			if($launcherdata != null) {
				$data['launchers'] = $launcherdata;
			} else {
				$data['launchers'] = array();
			}
			
			//schedules
			if($qry_events->num_rows() == 1) {			
				$eventidsString = implode(",", $eventids);
				$schedules = $this->db->query("SELECT id, eventid, schedule.key, schedule.caption FROM schedule WHERE eventid IN ($eventidsString)");
				$schedules = $schedules->result();

				$data['schedules'] = $schedules;
			}
			
			
			header('Content-type: application/json');
			echo json_encode($data);
			} 
		}
	}
	
}