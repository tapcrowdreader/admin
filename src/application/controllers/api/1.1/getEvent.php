<?php 

class getEvent extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getEvent() {
		parent::MY_Controller();
	}
	
	function index($appid = '', $eventid = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : ''); //'8'; 
		$eventid = ($this->input->post('eventid') ? $this->input->post('eventid') : 0); //'266'; 
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($appid == '' || $appid == null || $eventid == '' || $eventid == null) { echo "ERROR"; exit(); }
		
		// GET APP INFO
		$qry_app = $this->db->query("SELECT app.id, app.organizerid FROM app WHERE app.id = $appid");
		if($qry_app->num_rows() == 0) {
			// UNKNOWN APPID
			echo "ERROR";
		} else {
			// GET EVENT
			$qry_event = $this->db->query("SELECT evt.id, evt.eventtypeid, evt.venueid, typ.name as eventtypename, evt.name, evt.datefrom, evt.dateto, evt.eventlogo, evt.description, evt.phonenr, evt.website, evt.ticketlink, evt.qrshare, evt.timestamp, evt.email 
				FROM event evt
				LEFT JOIN eventtype typ ON (typ.id = evt.eventtypeid) 
				WHERE evt.id = $eventid AND evt.active = 1 AND evt.deleted = 0");
			if($qry_event->num_rows() == 0) {
				// UNKNOWN EVENT
				echo "ERROR";
			} else {
				// *** EVENT DATA *** //
				$app = _getAppFromEvent($this, $eventid);
				$languages = _getLanguagesOfApp($this, $app->id);
				$event = $qry_event->row();
				
				if($event->eventlogo != '') $event->eventlogo = base_url() . $event->eventlogo;
				$event->eventlogobig = str_replace('upload/eventlogos', 'upload/eventlogos/big', $event->eventlogo);
				$event->thumblogo = ($event->eventlogo != '' ? base_url() . image_crop(str_replace('upload/eventlogos', 'upload/eventlogos/big', $event->eventlogo), 128, 128) : '');
                
                //translations
                if($lang != '' && $lang != $app->defaultlanguage) {
                    $transname = _getTranslation($this,'event',$eventid,'name', $lang);
                    if($transname != null) {
                        $event->name = $transname->translation;
                    }
                    $transdescription = _getTranslation($this,'event',$eventid,'description', $lang);
                    if($transdescription != null) {
                        $event->description = $transdescription->translation;
                    }
                }
                
				$data['events'][] = $event;
				
				// ADD VENUEDETAILS TO EVENT
				$vnu = $this->db->query("SELECT id, name, address, lat, lon, travelinfo, email FROM venue WHERE id = $event->venueid");
				if($vnu->num_rows() > 0) {
					$venue = $vnu->row();
					$venue->eventid = $eventid;
					$data['venue'] = $venue;					
				} else {
					$data['venue'] = array();
				}
				
				// *** //
				
				// GET MODULES
				// *** ACTIVATED MODULES *** //
				$modules = $this->db->query("SELECT moduletype.* FROM module LEFT JOIN moduletype ON (module.moduletype = moduletype.id) WHERE module.event = $eventid")->result();
				// *** GET DATA PER MODULE *** //
				foreach($modules as $module) {
					if ($module->apicall == 'getBrandsCategories'){
						include('getBrands.php');
						$modcont = new getBrands();
						$moddata = $modcont->event($this, $eventid);
						if($moddata != null) {
							$data['brands']['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => 'getBrands');
							$data['brands']['data'] = $moddata;
						}
						
						include('getCategories.php');
						$modcont = new getCategories();
						$moddata = $modcont->event($this, $eventid);
						if($moddata != null) {
							$data['categories']['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => 'getCategories');
							$data['categories']['data'] = $moddata;
						}
					} else if($module->controller == 'sessions') {
						include($module->apicall . '.php');
						include('getSessiongroups.php');
						//groups
						$sessiongroupsController = new getSessiongroups();
						$data['sessiongroups'] = $sessiongroupsController->event($this, $eventid);

						//sessions
						$modcont = new $module->apicall();
						$moddata = $modcont->event($this, $eventid);
						$data['sessions'] = $moddata;

					} else if($module->controller == 'sponsors') {
						include($module->apicall . '.php');
						include('getSponsorgroups.php');
						//groups
						$sponsorgroupsController = new getSponsorgroups();
						$data['sponsorgroups'] = $sponsorgroupsController->event($this, $eventid);

						//sessions
						$modcont = new $module->apicall();
						$moddata = $modcont->event($this, $eventid);
						$data['sponsors'] = $moddata;
					} else if ($module->apicall != '') {
						include($module->apicall . '.php');
						$modcont = new $module->apicall();
						$moddata = $modcont->event($this, $eventid, $lang);
						if($moddata != null) {
							if($module->controller == 'sessions') {
								$itemname = 'sessiongroups';
							} else {
								$itemname = $module->controller;
							}

							$data[$itemname] = $moddata;
						}
					}
				}
				
				//get launcher
				include('getLauncher.php');
				$getLauncher = new getLauncher();
				$launcherdata = $getLauncher->event($this, $eventid, $lang);
				if($launcherdata != null) {
					$data['launcher'] = $launcherdata;
				}
				
                $tags = $this->db->query("SELECT * FROM tag WHERE eventid = $eventid")->result();
                $data['tags'] = $tags;
				
				$metadata = _get_meta_data($this, 'event', $event->id);
				$data['metadata'] = $metadata;
				
				header('Content-type: application/json');
				echo json_encode($data);
			}
		}
	}
	
}