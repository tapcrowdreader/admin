<?php 

class getSponsors extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '') {
		$app = _getAppFromEvent($obj, $eventid);
		$languages = _getLanguagesOfApp($obj, $app->id);
		$res = $obj->db->query("SELECT id,eventid, name, description, image, website FROM sponsor WHERE eventid = $eventid ORDER BY id ASC");
		if($res->num_rows() == 0) return array();
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = base_url() . $row->image;
			}
			
			if($lang != '' && $lang != $app->defaultlanguage) {
		        $transname =_getTranslation($obj,'sponsor',$row->id,'name', $lang);
		        if($transname != null) {
		            $row->name = $transname->translation;
		        }
		        $transdescription = _getTranslation($obj,'sponsor',$row->id,'description', $lang);
		        if($transdescription != null) {
		            $row->description = $transdescription->translation;
		        }
			}
		}
		return $res->result();
	}
	
	function venue($obj, $venueid, $lang = '') {
		$app = _getAppFromVenue($obj, $venueid);
		$languages = _getLanguagesOfApp($obj, $app->id);
		$res = $obj->db->query("SELECT id, venueid, name, description, image, website FROM sponsor WHERE venueid = $venueid ORDER BY id ASC");
		if($res->num_rows() == 0) return array();
		foreach($res->result() as $row){
			if($row->image != ''){
				$row->image = base_url() . $row->image;
			}
			
			if($lang != '' && $lang != $app->defaultlanguage) {
		        $transname =_getTranslation($obj,'sponsor',$row->id,'name', $lang);
		        if($transname != null) {
		            $row->name = $transname->translation;
		        }
		        $transdescription = _getTranslation($obj,'sponsor',$row->id,'description', $lang);
		        if($transdescription != null) {
		            $row->description = $transdescription->translation;
		        }
			}
		}
		return $res->result();
	}
	
}