<?php 

class getBrands extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getBrands() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id);
				break;
			case 'venue':
				$data = $this->venue($this, $id);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid) {
		$res = $obj->db->query("SELECT id, eventid, name FROM exhibitorbrand WHERE eventid = $eventid");
		if($res->num_rows() == 0) return array();
		/*foreach($res->result() as $row){}*/
		return $res->result();
	}
	
	function venue($obj, $venueid) {
		$res = $obj->db->query("SELECT id, name FROM exhibitorbrand WHERE venueid = $venueid");
		if($res->num_rows() == 0) return array();
		/*foreach($res->result() as $row){}*/
		return $res->result();
	}
	
}