<?php 

class pushAuthentication extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function pushAuthentication() {
		parent::MY_Controller();
	}
	
	function index($key = '', $phoneid = '', $appid = '') {
		// $key, $phoneid, $appid
		$key = ($this->input->post('key') ? $this->input->post('key') : '');
		$phoneid = ($this->input->post('phoneid') ? $this->input->post('phoneid') : '');
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		
		if($key != "" && $phoneid != '' && $appid != ""){
			
			// Secret string opbouwen 
			$secret = md5("tcadm" . $appid);
			
			// Secret vergelijken met meegestuurde
			if($key == $secret) {
				
				// Insert favourite
				$userdata = array(
						'appid' 	=> $appid,
						'phoneid' 	=> $phoneid
					);
				if($this->db->insert('authstats', $userdata)){
					echo "OK";
				} else {
					echo "ERROR";
				}
			} else {
				echo "ERROR";
			}
		} else {
			echo "ERROR";
		}
	}
	
}