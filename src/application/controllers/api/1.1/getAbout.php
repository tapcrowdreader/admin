<?php 

class getAbout extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getAbout() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '', $selection = '') {
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		$selection = ($this->input->post('selection') ? $this->input->post('selection') : '');
		
		 
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang, $selection);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang, $selection);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function venue($obj, $venueid, $lang = '', $selection = '') {
		$data = array();
		$qry_venue = $obj->db->query("SELECT vnu.id, vnu.name, vnu.address, vnu.lat, vnu.lon, vnu.telephone, vnu.fax, vnu.email, vnu.info, vnu.travelinfo, vnu.timestamp, vnu.image1, vnu.image2, vnu.image3, vnu.image4, vnu.image5, vnu.website, vnu.twitterurl, vnu.facebookurl, vnu.toururl, vnu.vimeourl 
			FROM venue vnu
			WHERE vnu.id = $venueid AND vnu.active = 1 AND vnu.deleted = 0");
		if($selection == 'location') {
			$qry_venue = $obj->db->query("SELECT vnu.id, vnu.name, vnu.address, vnu.lat, vnu.lon, vnu.travelinfo, vnu.timestamp
					FROM venue vnu
					WHERE vnu.id = $venueid AND vnu.active = 1 AND vnu.deleted = 0");
		}
		
		if($selection == 'contact') {
			$qry_venue = $obj->db->query("SELECT vnu.id, vnu.name, vnu.address, vnu.lat, vnu.lon, vnu.telephone, vnu.fax, vnu.email, vnu.travelinfo, vnu.timestamp, vnu.website 
				FROM venue vnu
				WHERE vnu.id = $venueid AND vnu.active = 1 AND vnu.deleted = 0");
		}
		
		if($selection == 'info') {
			$qry_venue = $obj->db->query("SELECT vnu.id, vnu.name, vnu.info, vnu.image1, vnu.timestamp 
				FROM venue vnu
				WHERE vnu.id = $venueid AND vnu.active = 1 AND vnu.deleted = 0");
		}

			if($qry_venue->num_rows() == 0) {
				// UNKNOWN VENUE
				echo "ERROR";
			} else {				
				$venue = $qry_venue->row();
				//translations
				if($lang != '' && $lang != $app->defaultlanguage) {
				    $transname = _getTranslation($this,'venue',$venueid,'name', $lang);
				    if($transname != null) {
				        $venue->name = $transname->translation;
				    }
				    $transdescription = _getTranslation($this,'venue',$venueid,'info', $lang);
				    if($transdescription != null) {
				        $venue->info = $transdescription->translation;
				    }
				}
				
				if($selection == 'contact') {
					$res = $obj->db->query("SELECT id, schedule.key, schedule.caption FROM schedule WHERE venueid = $venueid");
					if($res->num_rows() == 0) $schedule = '';
					if($lang != '' && $lang != 'en') {
						foreach($res->result() as $row) {
							$transkey = _getTranslation($obj, '', 0, strtolower($row->key), $lang);
							if($transkey != null && $transkey != false) {
								$row->key = ucfirst($transkey->translation);
							}
						}
					}
					/*foreach($res->result() as $row){}*/
					$schedule = $res->result();
				}
				
				$data['venue'] = $venue;
				
				$tags = $obj->db->query("SELECT * FROM tag WHERE venueid = $venueid")->result();
                $data['tags'] = $tags;
				
				if($selection == 'contact') {
					$data['schedule'] = $schedule;
				}
			}
		return $data;
	}
	
}