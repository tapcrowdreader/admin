<?php 

class updateUser extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function updateUser() {
		parent::MY_Controller();
	}
	
	function index($apikey = '', $external_id = '', $credits = '') {
		
		$data = array();
		
		$apikey = ($this->input->post('apikey') ? $this->input->post('apikey') : '');
		$credits = ($this->input->post('credits') ? $this->input->post('credits') : 0);
		$external_id = ($this->input->post('external_id') ? $this->input->post('external_id') : 0);
		
		// TODO : CHECK OP HOST ($this->input->post('host'))
		if($this->db->query("SELECT * FROM apikey WHERE apikey.key = '$apikey'")->num_rows() > 0) {
			
			$this->db->where('external_id', $external_id);
			$organizer = $this->db->get('organizer');
			
			if($organizer->num_rows() == 1) {
				$this->db->where('id', $organizer->row()->id);
				if($this->db->update('organizer', array('credits' => $credits))){
					header('Content-type: application/json');
					echo json_encode('UPDATED');
				} else {
					echo "ERROR";
				}
			} else {
				echo "NO SUCH USER";
			}
		} else {
			echo "API AUTHENTICATION FAILED";
		}
	}
	
}