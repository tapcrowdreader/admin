<?php 

class getMap extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getMap() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id);
				break;
			case 'venue':
				$data = $this->venue($this, $id);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid) {
		$res = $obj->db->query("SELECT id, eventid, imageurl, timestamp FROM map WHERE eventid = $eventid");
		if($res->num_rows() == 0) return array();
		
		foreach ($res->result() as $row) {
			if($row->imageurl != '') {
				$row->imageurl =  base_url() . $row->imageurl;
				if ($row->imageurl != '') {
					for ($i=0; $i < 4; $i++) { 
						if(file_exists('cache/maps/' . $i . '_' . substr($row->imageurl, strrpos($row->imageurl , "/") + 1))){
							$row->{'image'.($i+1)} = base_url() . 'cache/maps/' . $i . '_' . substr($row->imageurl, strrpos($row->imageurl , "/") + 1);
						} else {
							$row->{'image'.($i+1)} = '';
						}
					}
				}
			}
		}
		
		return array($res->row());
	}
	
	function venue($obj, $venueid) {
		$res = $obj->db->query("SELECT venueid, imageurl, timestamp FROM map WHERE venueid = $venueid LIMIT 1");
		if($res->num_rows() == 0) return array();
		if($res->row()->imageurl != '') $res->row()->imageurl =  base_url() . $res->row()->imageurl;
		return array($res->row());
	}
	
}