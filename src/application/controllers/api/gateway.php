<?php if(!defined('BASEPATH')) exit('No direct script access');

class Gateway extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Gateway() {
		parent::__construct();
	}
	
	function index() {
		$this->load->view('api/master');
	}
	
	function call($version = '', $function = '') {
		if(!$version || $version == '' || !$function || $function == '') die('Error! Bad connection!');
		
		$fields_string = '';
		if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != '') {
			parse_str($_SERVER['QUERY_STRING'],$_GET); 
			extract($_GET);
			foreach($_GET as $key=>$value) { 
				if($key == 'devicetype') {
					$fields_string .= $key.'='.str_replace(' ', '_',$value).'&'; 
				} else {
					$fields_string .= $key.'='.$value.'&';
				}
			}
		} else {
			foreach($_POST as $key=>$value) { 
				if($key == 'devicetype') {
					$fields_string .= $key.'='.str_replace(' ', '_',$value).'&'; 
				}else {
					$fields_string .= $key.'='.$value.'&';
				}
			}
		}
		rtrim($fields_string,'&');
		if($fields_string == '') die('Error! No parameters found!');
		
		//set POST variables
		// $url = base_url() . 'api/' . $version . '/' . $function;
		$url = 'http://api.tapcrowd.com/api/'.$version.'/'.$function;
		
		// *** Load tracking-page *** //
		$var_utmac = 'UA-23619195-3'; //enter the new urchin code
		$var_utmhn = 'admin.tapcrowd.com'; //enter your domain
		$var_utmn = rand(1000000000,9999999999);//random request number
		$var_cookie = rand(10000000,99999999);//random cookie number
		$var_random = rand(1000000000,2147483647); //number under 2147483647
		$var_today = time(); //today
		$var_referer = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''); //referer url
		$var_uservar = $fields_string; //enter your own user defined variable
		
		$var_utmp = '/api/' . $version . '/' . $function . "?" . $var_uservar; //this example adds a fake page request to the (fake) rss directory (the viewer IP to check for absolute unique RSS readers)
		
		$urchinUrl='http://www.google-analytics.com/__utm.gif?utmwv=1&utmn='.$var_utmn.'&utmsr=-&utmsc=-&utmul=-&utmje=0&utmfl=-&utmdt=-&utmhn='.$var_utmhn.'&utmr='.$var_referer.'&utmp='.$var_utmp.'&utmac='.$var_utmac.'&utmcc=__utma%3D'.$var_cookie.'.'.$var_random.'.'.$var_today.'.'.$var_today.'.'.$var_today.'.2%3B%2B__utmb%3D'.$var_cookie.'%3B%2B__utmc%3D'.$var_cookie.'%3B%2B__utmz%3D'.$var_cookie.'.'.$var_today.'.2.2.utmccn%3D(direct)%7Cutmcsr%3D(direct)%7Cutmcmd%3D(none)%3B%2B__utmv%3D'.$var_cookie.'.'.$var_uservar.'%3B';
		
		// $handle = fopen ($urchinUrl, "r");
		// $test = fgets($handle);
		// fclose($handle);
		// *** //
		
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);

		curl_setopt($ch,CURLOPT_POST,count($_POST));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		
		$result = curl_exec($ch);
		if($result === false) {
			echo 'Gateway error: ' . curl_error($ch);
		} else {
			// Operation completed without any errors
		}
		
		curl_close($ch);
		
	}
	
}