<?php if(!defined('BASEPATH')) exit('No direct script access');

class Browser extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Browser() {
		parent::__construct();
	}
	
	function index() {
		if($this->input->post('postback') == 'postback') {
			$version 	= end(explode('/', $this->input->post('sel_version')));
			$function	= $this->input->post('sel_function');
			$max = (count($_POST['key']) > count($_POST['value'])) ? count($_POST['key']) : count($_POST['value']);
			
			extract($_POST);
			
			//set POST variables
			$url = base_url() . 'api/' . $version . '/' . $function;
			
			$fields_string = '';
			for($i=0; $i<$max; $i++){
				$fields_string .= strtolower($_POST['key'][$i]).'='.strtolower($_POST['value'][$i]).'&'; 
			}
			rtrim($fields_string,'&');
			
			//open connection
			$ch = curl_init();
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_POST,count($_POST));
			curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
			
			if(curl_exec($ch) === false) {
				echo 'Gateway error: ' . curl_error($ch);
			} else {
				// Operation completed without any errors
			}
			
			$info = curl_getinfo($ch);
			echo "|||||<i>Time : </i> " . $info['total_time'] . " sec<br />\n<i>Download : </i>" . $this->_formatBytes($info['size_download']) . " at " . $this->_formatBytes($info['speed_download']) . "/sec" . "<br />\n<i>Upload : </i>" . $this->_formatBytes($info['size_upload']) . " at " . $this->_formatBytes($info['speed_upload']) . '/sec';
			
			//close connection
			curl_close($ch);
			
		} else {
			// versies ophalen
			$path = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']) . 'application/SITE/controllers/api';
			$scanres = scandir($path);
			$folders = array();
			
			foreach ($scanres as $result) {
				if ($result === '.' or $result === '..' or $result === '.svn') continue;
				
				if (is_dir($path . '/' . $result)) {
					//echo $path . '/' . $result . '<br />';
					$folders[$result] = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']) . 'application/SITE/controllers/api/' . $result;
					
					// functies per versie
					//$this->getDirectoryList(str_replace('api/index.php', '', $_SERVER['SCRIPT_FILENAME']) . 'application/API/controllers/' . $result)
					
				}
			}
			
			$this->load->view('api/form', array("versions" => $folders));
		}
	}
	
	function getFunctions() {
		echo json_encode($this->getDirectoryList($this->input->post('path')));
	}
	
	function getDirectoryList($directory) {
		$results = array();
		$handler = opendir($directory);
		while ($file = readdir($handler)) {
			if (preg_match("([^\s]+(?=\.(php)))", $file)) {
				$results[] = str_replace('.php', '', $file);
			}
		}
		closedir($handler);
		return $results;
	}
	
	function _formatBytes($size, $precision = 2) {
		$base = log($size) / log(1024);
		$suffixes = array('bytes', 'kb', 'Mb', 'Gb', 'Tb');
		
		return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
	}

}