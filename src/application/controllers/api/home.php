<?php if(!defined('BASEPATH')) exit('No direct script access');

class Home extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Home() {
		parent::__construct();
	}
	
	function index() {
		die('TapCrowd API is currently private, for any further information please contact <a href="mailto:support@tapcrowd.com">support@tapcrowd.com</a>');
	}
	
}