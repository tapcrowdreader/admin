<?php
class getFacebook extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id);
				break;
			case 'venue':
				$data = $this->venue($this, $id);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function event($obj, $eventid) {
		include('getSocialmedia.php');
		$modcont = new getSocialmedia();
		$socialmedia = $modcont->event($obj, $eventid);
		
		$facebook = $obj->db->query("SELECT * FROM facebook WHERE eventid = $eventid");
		$time = time();
		
		if($facebook->num_rows() > 0) {
			$facebook = $facebook->row();
			if($facebook->timestamp < ($time - 600)) {
				$messages = file_get_contents("https://graph.facebook.com/".$socialmedia->facebookid."/".$socialmedia->attribute."?access_token=". $this->config->item('FACEBOOK_TOKEN'));
				$data = array(
					"eventid"	=> $eventid,
					"json"		=> $messages,
					"timestamp"	=> $time
				);

				_update($obj, 'facebook', $facebook->id, $data);
			}
		} else {
			$messages = file_get_contents("https://graph.facebook.com/".$socialmedia->facebookid."/".$socialmedia->attribute."?access_token=". $this->config->item('FACEBOOK_TOKEN'));
			$data = array(
				"eventid"	=> $eventid,
				"json"		=> $messages,
				"timestamp"	=> $time
			);

			_insert($obj, 'facebook', $data);
		}
		
		$res = $obj->db->query("SELECT * FROM facebook WHERE eventid = $eventid");
		if($res->num_rows() == 0) return array();
		foreach($res->result() as $row) {
		return json_decode($row->json);
		}
		//return $res->result();
	}
	
	function venue($obj, $venueid) {
		include('getSocialmedia.php');
		$modcont = new getSocialmedia();
		$socialmedia = $modcont->venue($obj, $venueid);
		
		$facebook = $obj->db->query("SELECT * FROM facebook WHERE venueid = $venueid");
		$time = time();
		
		if($facebook->num_rows() > 0) {
			$facebook = $facebook->row();
			if($facebook->timestamp < ($time - 600)) {
				$messages = file_get_contents("https://graph.facebook.com/".$socialmedia->facebookid."/".$socialmedia->attribute."?access_token=". $this->config->item('FACEBOOK_TOKEN'));
				$data = array(
					"venueid"	=> $venueid,
					"json"		=> $messages,
					"timestamp"	=> $time
				);

				_update($obj, 'facebook', $facebook->id, $data);
			}
		} else {
			$messages = file_get_contents("https://graph.facebook.com/".$socialmedia->facebookid."/".$socialmedia->attribute."?access_token=". $this->config->item('FACEBOOK_TOKEN'));
			$data = array(
				"venueid"	=> $venueid,
				"json"		=> $messages,
				"timestamp"	=> $time
			);

			_insert($obj, 'facebook', $data);
		}
		
		$res = $obj->db->query("SELECT * FROM facebook WHERE venueid = $venueid");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
}