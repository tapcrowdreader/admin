<?php 

class getExhibitors extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getExhibitors() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '') {
		$eventBrands = $obj->db->query("SELECT id FROM exhibitorbrand WHERE eventid = $eventid");
		$eventBrands = $eventBrands->num_rows();
		$eventCategories = $obj->db->query("SELECT id FROM exhibitorcategory WHERE eventid = $eventid");
		$eventCategories = $eventCategories->num_rows();
		$app = _getAppFromEvent($obj, $eventid);
		$languages = _getLanguagesOfApp($obj, $app->id);
		$res = $obj->db->query("SELECT * FROM exhibitor WHERE eventid = $eventid ORDER BY name ASC");
		if($res->num_rows() == 0) return array();
		foreach ($res->result() as $row) {
			$row->name = str_replace('&#39;', "'",htmlspecialchars_decode($row->name, ENT_NOQUOTES));
			$row->web = (substr($row->web, 0, 7) != 'http://') ? 'http://'.$row->web : $row->web;
			$row->web = ($row->web == 'http://') ? '' : $row->web;
			
			// * PROCESS IMAGES * //
			if($row->imageurl != '') { $row->imageurl = base_url() . _api_image_crop($obj, $row->imageurl, 50, 50); }
			for ($i = 1; $i < 21; $i++) { 
				$image = $row->{'image'.$i};
				unset($row->{'image'.$i});
				if($i == 1 && $image != ''){ $row->image1 = base_url() . _api_image_crop($obj, $image, 70, 70); }
				unset($row->{'imagedescription'.$i});
			}
			
			// * GET EXHIBITOR CATEGORIES * //
			if($eventCategories != 0) {
			$categories = $this->_getExhibitorCats($obj, $row->id);
			if($categories && count($categories) > 0) $row->categories = $categories;
			}
			
			if($eventBrands != 0) {
			// * GET EXHIBITOR BRANDS * //
			$brands = $this->_getExhibitorBrands($obj, $row->id);
			if($brands && count($brands) > 0) $row->brands = $brands;
			}
			if($lang != '' && $lang != $app->defaultlanguage) {
			    $transname = _getTranslation($obj,'exhibitor',$row->id,'name', $lang);
			    if($transname != null) {
			        $row->name = $transname->translation;
			    }
			    $transdescription = _getTranslation($obj,'exhibitor',$row->id,'description', $lang);
			    if($transdescription != null) {
			        $row->description = $transdescription->translation;
			    }
			}
		}
		
		return $res->result();
	}
	
	function venue($obj, $venueid, $lang = '') {
		$venueBrands = $obj->db->query("SELECT id FROM exhibitorbrand WHERE venueid = $venueid");
		$venueBrands = $venueBrands->num_rows();
		$venueCategories = $obj->db->query("SELECT id FROM exhibitorcategory WHERE venueid = $venueid");
		$venueCategories = $venueCategories->num_rows();
		$res = $obj->db->query("SELECT * FROM exhibitor WHERE venueid = $venueid ORDER BY name ASC");
		if($res->num_rows() == 0) return array();
		foreach ($res->result() as $row) {
			$row->name = str_replace('&#39;', "'",htmlspecialchars_decode($row->name, ENT_NOQUOTES));
			$row->web = (substr($row->web, 0, 7) != 'http://') ? 'http://'.$row->web : '';
			
			// * PROCESS IMAGES * //
			if($row->imageurl != '') { $row->imageurl = base_url() . image_thumb($row->imageurl, 50, 50); }
			for ($i = 1; $i < 21; $i++) { 
				$image = $row->{'image'.$i};
				unset($row->{'image'.$i});
				if($i == 1 && $image != ''){ $row->image1 = base_url() . image_thumb($image, 70, 70); }
				unset($row->{'imagedescription'.$i});
			}
			
			// * GET EXHIBITOR CATEGORIES * //
			if($venueBrands != 0) {
			$categories = $this->_getExhibitorCats($obj, $row->id);
			if($categories && count($categories) > 0) $row->categories = $categories;
			}
			
			// * GET EXHIBITOR BRANDS * //
			if($venueCategories != 0) {
			$brands = $this->_getExhibitorBrands($obj, $row->id);
			if($brands && count($brands) > 0) $row->brands = $brands;
			}
		}
		
		return $res->result();
	}
	
	protected function _getExhibitorBrands($obj, $exhibitorid) {
		$res = $obj->db->query("SELECT eb.exhibitorbrandid as id, br.name FROM exhibrand eb LEFT JOIN exhibitorbrand br ON eb.exhibitorbrandid = br.id WHERE eb.exhibitorid = $exhibitorid ORDER BY br.name asc");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
	protected function _getExhibitorCats($obj, $exhibitorid) {
		$res = $obj->db->query("SELECT eb.exhibitorcategoryid as id, br.name FROM exhicat eb LEFT JOIN exhibitorcategory br ON eb.exhibitorcategoryid = br.id WHERE eb.exhibitorid = $exhibitorid  ORDER BY br.name asc");
		if($res->num_rows()  == 0) return array();
		return $res->result();
    }
	
}