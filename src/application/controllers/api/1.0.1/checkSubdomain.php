<?php 
/**
 * checkSubdomain gets app-data for subdomain given to the app.
 */
class checkSubdomain extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function checkSubdomain() {
		parent::MY_Controller();
	}
	
	function index($domain = '', $secret = '') {
		
		$domain = ($this->input->post('domain') ? $this->input->post('domain') : '');
		$secret = ($this->input->post('secret') ? $this->input->post('secret') : '');
		
		$mysecret = md5("tcadm" . $domain);
		if($mysecret == $secret) {
			$res = $this->db->query("SELECT * FROM app WHERE subdomain = '$domain' AND isdeleted = 0 LIMIT 1");
			if($res->num_rows() == 0) {
				$data = '';
			} else {
				$app = $res->row();
				$data->app = $app;
				// GET EVENTS FROM APP
				$this->load->model('event_model');
				$data->events = $this->event_model->allFromApp($app->id);
				
				// GET VENUES FROM APP
				$this->load->model('venue_model');
				$data->venues = $this->venue_model->getAppVenue($app->id);
			}
			
			header('Content-type: application/json');
			echo json_encode($data);
		} else {
			header('Content-type: application/json');
			echo json_encode('');
		}
	}
	
}