<?php 

class getCatalogs extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getCatalogs() {
		parent::MY_Controller();
	}
	
    function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '', $catalogtype = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		$catalogtype = ($this->input->post('catalogtype') ? $this->input->post('catalogtype') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang, $catalogtype);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang, $catalogtype);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '', $catalogtype = '') {
		$res = $obj->db->query("SELECT * FROM cataloggroup WHERE eventid = $eventid ORDER BY cataloggroup.order ASC");
		if($res->num_rows() == 0) return array();
		
		// GET SESSIONS PER SESSIONGROUP
		foreach ($res->result() as $group) {
			$cats = $obj->db->query("SELECT id, name, catalog.order, description, cataloggroupid, imageurl, sourceid, type, date FROM catalog WHERE cataloggroupid = $group->id AND type = '$catalogtype' ORDER BY catalog.order ASC");
			$group->name = htmlspecialchars_decode($group->name, ENT_NOQUOTES);
			foreach($cats->result() as $catalog){
				// ADD EVENTID FROM GROUP
				$catalog->eventid = $group->eventid;
				
				$catalog->name = htmlspecialchars_decode($catalog->name, ENT_NOQUOTES);
				$catalog->description = htmlspecialchars_decode($catalog->description, ENT_NOQUOTES);
				
				if($catalog->imageurl != '') {
					$catalog->imagethumb = base_url() . image_thumb($catalog->imageurl, 70, 70);
					$catalog->imageurl = base_url() . image_thumb($catalog->imageurl, 960, 640);
				} else {
					$catalog->imagethumb = '';
				}
                
                // * GET CATALOG CATEGORIES * //
                $categories = $this->_getCatalogCats($obj, $catalog->id);
                if($categories && count($categories) > 0) $catalog->categories = $categories;

                // * GET CATALOG BRANDS * //
                $brands = $this->_getCatalogBrands($obj, $catalog->id);
                if($brands && count($brands) > 0) $catalog->brands = $brands;
			}
			$group->catalog = $cats->result();
		}
		
		return $res->result();
	}
	
	function venue($obj, $venueid, $lang = '', $catalogtype = '') {
		$res = $obj->db->query("SELECT * FROM cataloggroup WHERE venueid = $venueid ORDER BY `order` ASC");

		if($res->num_rows() == 0) return array();
		
		$app = _getAppFromVenue($obj, $venueid);
		$languages = _getLanguagesOfApp($obj, $app->id);
		$sourcesdone = array();
		
		// GET CATALOGS PER CATALOGGROUP
		foreach ($res->result() as $group) {
			$cats = $obj->db->query("SELECT id, name, `order`, description, cataloggroupid, imageurl, sourceid, type, date FROM catalog WHERE cataloggroupid = $group->id AND type = '$catalogtype' ORDER BY `order` ASC");
			if($app->apptypeid == 4) {
				$cats = $obj->db->query("SELECT id, name, `order`, description, cataloggroupid, imageurl, sourceid, type, date FROM catalog WHERE cataloggroupid = $group->id ORDER BY `order` ASC");
			}
			$group->name = htmlspecialchars_decode($group->name, ENT_NOQUOTES);
            
			foreach($cats->result() as $catalog){
				//source
				$groupid = $catalog->cataloggroupid;
				if($catalog->sourceid != null && $catalog->sourceid != 0 && !in_array($catalog->sourceid, $sourcesdone)) {
					$source = $obj->db->query("SELECT * FROM catalogsource WHERE id = $catalog->sourceid");
					$source = $source->row();
					$sourcesdone[] = $source->id;
					if($source->refreshrate!= null && $source->refreshrate != 0) {
						$refreshrate = $source->refreshrate;
					} else {
						$refreshrate = 20;
					}
					if($source->timestamp < date("Y-m-d H:i:s",time() - $refreshrate * 60)) {
						//remove translations
//						$catalogsOfSource = _getCatalogsBySourceId($obj,$source->id);
//						foreach($catalogsOfSource as $c) {
//							_removeTranslations($obj,'catalog', $c->id);
//						}

						//remove catalogs of source
						_removeCatalogsOfSourceId($obj, $source->id);
						
						//insert new news items of source
						$content = file_get_contents($source->url);  
						$rss = new SimpleXmlElement($content);

						foreach($rss->channel->item as $post) {                    
							$data = array( 
									"cataloggroupid"=> $groupid,
									"name" 	=> (string)$post->title,
									"venueid" 	=> $venueid,
									"description" => (string)$post->description,
									"url"		=> (string)$post->link,
									"sourceid"  => $source->id,
									"type"		=> $catalog->type,
									"date"     => ((string) $post->pubDate!= null && (string) $post->pubDate != '') ? date("Y-m-d H:i:s",strtotime((string) $post->pubDate)) : date("Y-m-d H:i:s",time())
								);

							if($catalogid = _insert($obj, 'catalog', $data)) {
								//add translated fields to translation table
								if($languages != null) {
									foreach($languages as $language) {
										_addTranslation($obj, 'catalog', $catalogid, 'name', $language->key, (string)$post->title);
										_addTranslation($obj, 'catalog', $catalogid, 'description', $language->key, (string)$post->description);
									}
								} 
							} else {
								$error = "Oops, something went wrong. Please try again.";
							}
						}
						
						//update timestamp
						$catalogsourceData = array(
							"timestamp" => date("Y-m-d H:i:s",time())
						);
						_update($obj, 'catalogsource', $source->id, $catalogsourceData);
					}
				}
			}
			$group->catalog = $cats->result();
		}
		//GET NEW DATA
		$res = $obj->db->query("SELECT * FROM cataloggroup WHERE venueid = $venueid ORDER BY `order` ASC");

		if($res->num_rows() == 0) return array();
		foreach ($res->result() as $group) {
			$cats = $obj->db->query("SELECT id, name, `order`, description, cataloggroupid, imageurl, sourceid, type, date FROM catalog WHERE cataloggroupid = $group->id AND type = '$catalogtype' ORDER BY `order` ASC");
			if($app->apptypeid == 4) {
				$cats = $obj->db->query("SELECT id, name, `order`, description, cataloggroupid, imageurl, sourceid, type, date FROM catalog WHERE cataloggroupid = $group->id ORDER BY `order` ASC");
			}
			$group->name = htmlspecialchars_decode($group->name, ENT_NOQUOTES);
            
			foreach($cats->result() as $catalog){
				// ADD EVENTID FROM GROUP
				$catalog->venueid = $group->venueid;
				
				$catalog->name = htmlspecialchars_decode($catalog->name, ENT_NOQUOTES);
				$catalog->description = htmlspecialchars_decode($catalog->description, ENT_NOQUOTES);
                
				if($catalog->imageurl != '') {
                    
                    $catalog->image320 = base_url() . _api_image_crop($obj, $catalog->imageurl, 320, 480);
					$catalog->imagethumb = base_url() . _api_image_crop($obj, $catalog->imageurl, 70, 70);
					$catalog->imageurl = base_url() . _api_image_crop($obj, $catalog->imageurl, 640, 960);
                    
				} else {
					$catalog->imagethumb = '';
				}
				
				if($lang != '' && $lang != $app->defaultlanguage) {
					$transname = _getTranslation($obj,'catalog',$catalog->id,'name', $lang);
					if($transname != null) {
					    $catalog->name = $transname->translation;
					}
				    $transdescription = _getTranslation($obj,'catalog',$catalog->id,'description', $lang);
				    if($transdescription != null) {
				        $catalog->description= $transdescription->translation;
				    }
				}
				
				// * GET CATALOG CATEGORIES * //
				$categories = $this->_getCatalogCats($obj, $catalog->id);
				if($categories && count($categories) > 0) $catalog->categories = $categories;
				
				// * GET CATALOG BRANDS * //
				$brands = $this->_getCatalogBrands($obj, $catalog->id);
				if($brands && count($brands) > 0) $catalog->brands = $brands;
			}
			$group->catalog = $cats->result();
		}
		
		return $res->result();
	}
	
	protected function _getCatalogBrands($obj, $catalogid) {
		$res = $obj->db->query("SELECT eb.catalogbrandid as id, br.name FROM catebrand eb LEFT JOIN catalogbrand br ON eb.catalogbrandid = br.id WHERE eb.catalogid = $catalogid ORDER BY br.name asc");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
	protected function _getCatalogCats($obj, $catalogid) {
		$res = $obj->db->query("SELECT eb.catalogcategoryid as id, br.name FROM catecat eb LEFT JOIN catalogcategory br ON eb.catalogcategoryid = br.id WHERE eb.catalogid = $catalogid  ORDER BY br.name asc");
		if($res->num_rows()  == 0) return array();
		return $res->result();
    }
}