<?php 

class insertSocialScore extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function insertSocialScore() {
		parent::MY_Controller();
	}
	
	function index($key = '', $facebookid = '', $appid = '', $eventid = '', $action = '') {
		// $key, $email, $eventid, $exhibitorid, $sessionid
		$key = ($this->input->post('key') ? $this->input->post('key') : '');
		$facebookid = ($this->input->post('facebookid') ? $this->input->post('facebookid') : '');
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		$eventid = ($this->input->post('eventid') ? $this->input->post('eventid') : 0);
		$action = ($this->input->post('action') ? $this->input->post('action') : 0);
		
		if($key != "" && $facebookid != '' && $appid != "" && $eventid != "" && $action != ""){
			// Secret string opbouwen 
			$secret = md5("tcadm" . $facebookid);
			
			// Secret vergelijken met meegestuurde
			if($key == $secret) {
				$actie = $this->db->query("SELECT * FROM socialscore_types WHERE action = '$action' LIMIT 1");
				if($actie->num_rows() > 0) {
					
					$actie = $actie->row();
					$score = $actie->score;
					$actions = array();
					$actions[] = array("action" => $actie->action, "score" => "+" . $actie->score);
					
					$userdata = array(
							'facebookid' 	=> ''.$facebookid,
							'appid' 		=> $appid,
							'eventid' 		=> $eventid,
							'action' 		=> $actie->id,
							'score' 		=> $score
						);
					
					if($this->db->insert('socialscore', $userdata)){
						$data['saldo'] 		= $this->db->query("SELECT SUM(score) as score FROM socialscore WHERE facebookid = '$facebookid' AND appid = $appid")->row()->score;
						$data['action'] 	= $actions;
						$data['points'] 	= "$score";
						
						header('Content-type: application/json');
						echo json_encode($data);
					} else {
						echo "ERROR";
					}
				} else {
					echo "ERROR";
				}
			} else {
				echo "ERROR";
			}
		} else {
			echo "ERROR";
		}
	}
	
}