<?php 

class getArtistsByEvent extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
        $this->load->model('artist_model');
	}
	
	//php 4 constructor
	function getArtists() {
		parent::MY_Controller();
	}
	
	function index($eventid = '', $deviceid = '', $devicetype = '') {
		
		$eventid = ($this->input->post('eventid') ? $this->input->post('eventid') : 0);
		if($eventid == '' || $eventid == null) { echo "ERROR"; exit(); }
		$artists = $this->artist_model->getArtistsOfEvent($eventid);

		foreach ($artists as $row) {
			$row->name = str_replace('&#39;', "'",htmlspecialchars_decode($row->name, ENT_NOQUOTES));
			
			// * GET ARSIST SESSIONS * //
			$sessions = $this->_getArtistSessions($this, $row->id);
			if($sessions && count($sessions) > 0) $row->sessions = $sessions;
			
			
			$metadata = _get_meta_data($this, 'artist', $row->id);
			if($metadata && count($metadata) > 0) $row->metadata = $metadata;
			
			/*
			// * GET ARSIST METADATA * //
			$metadata = $this->_getArtistMetaData($this, $row->id);
			if($metadata && count($metadata) > 0) $row->metadata = $metadata;
			*/
		}
		
		header('Content-type: application/json');
		/*
			TODO : wegwerken
		*/
		$data = array();
		$data['artists']['data'] = $artists;
		
		echo json_encode($data);
		
	}
	
	protected function _getArtistSessions($obj, $artistid) {
		$res = $obj->db->query("SELECT artist.*, sess.sessiongroupid as sessiongroupid, sess.id as sessionid, sess.name as sessionname FROM artistsessions ases LEFT JOIN artist ON ases.artistid = artist.id LEFT JOIN session sess ON ases.sessionid = sess.id WHERE ases.artistid = $artistid ORDER BY sess.order asc, sess.name asc");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	/*
	protected function _getArtistMetaData($obj, $artistid) {
		$res = $obj->db->query("SELECT md.key, md.value FROM metadata md WHERE md.table = 'artist' AND md.identifier = $artistid");
		$result = array();
		foreach ($res->result() as $row) {
			$result[$row->key] = $row->value;
		}
		return $result;
	}
	*/
}