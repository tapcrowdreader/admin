<?php 
/**
 * createUser makes it possible for 3rd party applications to create a user within TapCrowd.
 */
class createUser extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function createUser() {
		parent::MY_Controller();
	}
	
	function index($apikey = '', $external_id = '', $name = '', $login = '', $password = '', $email = '', $credits = '') {
		
		$data = array();
		
		$apikey = ($this->input->post('apikey') ? $this->input->post('apikey') : '');
		
		// TODO : CHECK OP HOST ($this->input->post('host'))
		if($this->db->query("SELECT * FROM apikey WHERE apikey.key = '$apikey'")->num_rows() > 0) {
			
			$update_data = array();
			$qry_columns = $this->db->query("SHOW COLUMNS FROM organizer")->result();
			$columns = array();
			$nogos = array('id', 'token', 'activation', 'lastlogin');
			foreach ($qry_columns as $column) {
				if(!in_array($column->Field, $nogos)){
					$columns[] = $column->Field;
				}
			}
			
			$user_data = array();
			foreach($_POST as $key => $value) { 
				if ($key != 'apikey' && in_array($key, $columns)) {
					$user_data[$key] = $value;
				}
			}
			
			if(count($user_data) > 0) {
				if($newuser = $this->db->insert('organizer', $user_data)){
					header('Content-type: application/json');
					echo json_encode($user_data);
				} else {
					echo "ERROR";
				}
			} else {
				echo "ERROR";
			}
		} else {
			echo "API AUTHENTICATION FAILED";
		}
	}
	
}