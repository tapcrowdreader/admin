<?php 

class getApps extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($user = '', $pass = '', $deviceid = '', $devicetype = '') {
		$data = array();
		$username = ($this->input->post('user') ? $this->input->post('user') : 0);
		$pass = ($this->input->post('pass') ? $this->input->post('pass') : '');
		
		$user = $this->db->query("Select * from organizer Where login = '$username'")->row();
		$login = true;
			if(substr($user->password, 0, 4) == '$P$7'){
				// USE HASHER
				$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
				
				if(!$hasher->CheckPassword($pass, $user->password)) {
					$login =  false;
				}
			} else {
				// USE MD5
				if(md5($pass) != $user->password) {
					$login = false;
				}
			}

		$data = array();
		
		if(strtolower($username) == 'mobilejuice' && strtolower($pass) == 'juice7566') {
			$qry_apps = $this->db->query("SELECT app.id, app.organizerid, app.apptypeid, at.name as apptype, app.name, app.info, app.advancedFilter, app.defaultlanguage, app.app_icon
				FROM app 
				LEFT JOIN apptype at ON (app.apptypeid = at.id) AND app.isdeleted = 0");
			
			$apps = $qry_apps->result();
			$data['apps'] = $apps;
			header('Content-type: application/json');
			echo json_encode($data);
		} else {
			if(!$login) {
				echo "ERROR Wrong username/password combination";
			} else {
				$qry_apps = $this->db->query("SELECT app.id, app.organizerid, app.apptypeid, at.name as apptype, app.name, app.info, app.advancedFilter, app.defaultlanguage, app.app_icon
					FROM app 
					LEFT JOIN apptype at ON (app.apptypeid = at.id)
					WHERE app.organizerid = $user->id AND app.isdeleted = 0");

				if($qry_apps->num_rows() == 0) {
					// UNKNOWN APPID
					echo "ERROR NO APPS FOUND";
				} else {
					$apps = $qry_apps->result();

					$data['apps'] = $apps;
					header('Content-type: application/json');
					echo json_encode($data);
				}
			}
		}
	}
	
}