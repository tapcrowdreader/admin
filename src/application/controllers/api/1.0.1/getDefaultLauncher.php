<?php 

class getDefaultLauncher extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	function index($deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		$res = $this->db->query("SELECT * FROM defaultlauncher");
		if($res->num_rows() == 0) return array();
		
		$data = $res->result();
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
}