<?php 

class getBrandsCategories extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getBrandsCategories() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id);
				break;
			case 'venue':
				$data = $this->venue($this, $id);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid) {
		$res = array();
		$cats = $obj->db->query("SELECT id, eventid, name FROM exhibitorcategory WHERE eventid = $eventid");
		$brands = $obj->db->query("SELECT id, eventid, name FROM exhibitorbrand WHERE eventid = $eventid");
		if($cats->num_rows() != 0) $res['categories'] = $cats->result();
		if($brands->num_rows() != 0) $res['brands'] = $brands->result();
		/*foreach($res->result() as $row){}*/
		return $res;
	}
	
	function venue($obj, $venueid) {
		$res = array();
		$cats = $obj->db->query("SELECT id, name FROM exhibitorcategory WHERE venueid = $venueid");
		$brands = $obj->db->query("SELECT id, name FROM exhibitorbrand WHERE venueid = $venueid");
		if($cats->num_rows() != 0) $res['categories'] = $cats->result();
		if($brands->num_rows() != 0) $res['brands'] = $brands->result();
		/*foreach($res->result() as $row){}*/
		return $res;
	}
	
}