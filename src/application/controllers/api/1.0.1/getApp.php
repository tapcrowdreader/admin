<?php 

class getApp extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getApp() {
		parent::MY_Controller();
	}
	
	function index($appid = '', $deviceid = '', $devicetype = '', $lang = '', $timestamp = '') {
		$data = array();
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		$timestamp = ($this->input->post('timestamp') ? $this->input->post('timestamp') : '');
		
		// GET APP INFO
		$qry_app = $this->db->query("SELECT app.id, app.organizerid, app.apptypeid, at.name as apptype, app.name, app.info, app.advancedFilter, app.defaultlanguage
			FROM app 
			LEFT JOIN apptype at ON (app.apptypeid = at.id)
			WHERE app.id = $appid");
		if($qry_app->num_rows() == 0) {
			// UNKNOWN APPID
			echo "ERROR";
		} else {
			$appinfo = $qry_app->row();
			//get launcher
			include('getLauncher.php');
			$getLauncher = new getLauncher();
			$launcherdata = $getLauncher->app($this, $appid, $lang);
			if($launcherdata != null) {
				$appinfo->launcher = $launcherdata;
			}
			$data['app'] = $appinfo;

			$languages = _getLanguagesOfApp($this, $appid);
			$latestEvent = 0;
			$latestVenue = 0;
			if($timestamp != '' && $timestamp != null) {
				$qry_timestampEvent = $this->db->query("SELECT evt.id as id, evt.timestamp as timestamp
					FROM event evt 
					LEFT JOIN appevent ae ON (evt.id = ae.eventid)  
					WHERE ae.appid = $appid AND evt.active = 1 AND evt.deleted = 0 
					ORDER BY evt.timestamp DESC");
				$res = $qry_timestampEvent->row();
				if($res != false && $res != null) {
					$latestEvent = $res->timestamp;
				}

				$qry_timestampVenue = $this->db->query("SELECT vnu.id, vnu.timestamp 
					FROM venue vnu 
					LEFT JOIN appvenue av ON (vnu.id = av.venueid) 
					WHERE av.appid = $appid AND vnu.deleted = 0 
					ORDER BY vnu.timestamp DESC");
				$res = $qry_timestampVenue->row();
				if($res != false && $res != null) {
					$latestVenue = $res->timestamp;
				}

				$latest = $latestVenue;
				if($latestEvent > $latestVenue) {
					$latest = $latestEvent;
				}
			}
					
			if($timestamp != null && $timestamp != '' && $timestamp >= $latest) {
				header('Content-type: application/json');
				echo json_encode(array('timestamp' => $latest));
			} else {
			// GET APP EVENTS
			$qry_events = $this->db->query("SELECT evt.id, evt.organizerid, evt.venueid, evt.name, typ.name as eventtype, evt.datefrom, evt.dateto, evt.eventlogo, evt.description, evt.qrshare, evt.timestamp
				FROM event evt 
				LEFT JOIN appevent ae ON (evt.id = ae.eventid) 
				LEFT JOIN eventtype typ ON (typ.id = evt.eventtypeid) 
				WHERE ae.appid = $appid AND evt.active = 1 AND evt.deleted = 0 
				ORDER BY id DESC");
			if($qry_events->num_rows() == 0) {
				// NO EVENTS
				$data['events'] = array();
			} else {
                if($qry_events->num_rows() > 1) {
				foreach($qry_events->result() as $event){
					$event->venue = $this->db->query("SELECT id, name, address, lat, lon, telephone, fax, email, info, timestamp FROM venue WHERE id = ".$event->venueid)->row();
					$event->venue->eventid = $event->id;
					$event->eventlogo = ($event->eventlogo != '') ? base_url() . $event->eventlogo : '';
					$event->thumblogo = ($event->eventlogo != '' ? base_url() . image_crop(str_replace('upload/eventlogos', 'upload/eventlogos/big', $event->eventlogo), 128, 128) : '');
					
					//translations
					if($lang != '' && $lang != $appinfo->defaultlanguage) {
					    $transname = _getTranslation($this,'event',$event->id,'name', $lang);
					    if($transname != null) {
					        $event->name = $transname->translation;
					    }
					    $transdescription = _getTranslation($this,'event',$event->id,'description', $lang);
					    if($transdescription != null) {
					        $event->description = $transdescription->translation;
					    }
					}
					
					$metadata = _get_meta_data($this, 'event', $event->id);
					if($metadata && count($metadata) > 0) $event->metadata = $metadata;	
					
					//schedule
					$res = $this->db->query("SELECT id, eventid, schedule.key, schedule.caption FROM schedule WHERE eventid = $event->id");

					/*foreach($res->result() as $row){}*/
					$event->schedule = $res->result();
					
				}
				$data['events'] = $qry_events->result();
                } elseif($qry_events->num_rows() == 1) {
                    //get all of event
                    $event = $qry_events->row();
                    $eventid = $event->id;
                    // GET EVENT
                    $qry_event = $this->db->query("SELECT evt.id, evt.eventtypeid, evt.venueid, typ.name as eventtypename, evt.name, evt.datefrom, evt.dateto, evt.eventlogo, evt.description, evt.phonenr, evt.website, evt.ticketlink, evt.qrshare, evt.timestamp 
                        FROM event evt
                        LEFT JOIN eventtype typ ON (typ.id = evt.eventtypeid) 
                        WHERE evt.id = $eventid AND evt.active = 1 AND evt.deleted = 0");
                    if($qry_event->num_rows() == 0) {
                        // UNKNOWN EVENT
                        echo "ERROR";
                    } else {
                        // *** EVENT DATA *** //
                        $event = $qry_event->row();
                        if($event->eventlogo != '') $event->eventlogo = base_url() . $event->eventlogo;
                        $event->eventlogobig = str_replace('upload/eventlogos', 'upload/eventlogos/big', $event->eventlogo);
                        // ADD VENUEDETAILS TO EVENT
                        $vnu = $this->db->query("SELECT id, name, address, lat, lon, email travelinfo FROM venue WHERE id = $event->venueid");
                        if($vnu->num_rows() > 0) {
                            $venue = $vnu->row();
							$venue->eventid = $event->id;
                            $event->venue = $venue;
                            $event->thumblogo = ($event->eventlogo != '' ? base_url() . image_crop(str_replace('upload/eventlogos', 'upload/eventlogos/big', $event->eventlogo), 128, 128) : '');

                            $metadata = _get_meta_data($this, 'event', $event->id);
                            if($metadata && count($metadata) > 0) $event->metadata = $metadata;

                        } else {
                            $event->venue = array();
                        }
                        $tags = $this->db->query("SELECT * FROM tag WHERE eventid = $eventid")->result();
                        $event->tags = $tags;
                        
                        //translations
                        if($lang != '' && $lang != $appinfo->defaultlanguage) {
                            $transname = _getTranslation($this,'event',$event->id,'name', $lang);
                            if($transname != null) {
                                $event->name = $transname->translation;
                            }
                            $transdescription = _getTranslation($this,'event',$event->id,'description', $lang);
                            if($transdescription != null) {
                                $event->description = $transdescription->translation;
                            }
                        }
						
						//schedule
						$res = $this->db->query("SELECT id, eventid, schedule.key, schedule.caption FROM schedule WHERE eventid = $event->id");

						/*foreach($res->result() as $row){}*/
						$event->schedule = $res->result();

                        $data['event'] = $event;
                        // *** //

                        // GET MODULES
                        // *** ACTIVATED MODULES *** //
                        $modules = $this->db->query("SELECT moduletype.* FROM module LEFT JOIN moduletype ON (module.moduletype = moduletype.id) WHERE module.event = $eventid")->result();
                        // *** GET DATA PER MODULE *** //
                        foreach($modules as $module) {
							if ($module->apicall == 'getBrandsCategories'){
								include('getBrands.php');
								$modcont = new getBrands();
								$moddata = $modcont->event($this, $eventid);
								if($moddata != null) {
									$data['brands']['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => 'getBrands');
									$data['brands']['data'] = $moddata;
								}

								include('getCategories.php');
								$modcont = new getCategories();
								$moddata = $modcont->event($this, $eventid);
								if($moddata != null) {
									$data['categories']['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => 'getCategories');
									$data['categories']['data'] = $moddata;
								}
							} else if ($module->apicall != '') {
								include($module->apicall . '.php');
								$modcont = new $module->apicall();
								$moddata = $modcont->event($this, $eventid);
								if($moddata != null) {
									if($module->controller == 'sessions') {
										$itemname = 'sessiongroups';
									} else {
										$itemname = $module->controller;
									}

									$data[$itemname]['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => $module->apicall);
									$data[$itemname]['data'] = $moddata;
								}
							}
                        }
						

						
						//get launcher
						$launcherdata = $getLauncher->event($this, $eventid, $lang);
						if($launcherdata != null) {
							$data['launcher'] = $launcherdata;
						}
                    }
                }
			}
			
			// GET APP VENUES
			$qry_venues = $this->db->query("SELECT vnu.* 
				FROM venue vnu 
				LEFT JOIN appvenue av ON (vnu.id = av.venueid) 
				WHERE av.appid = $appid AND vnu.deleted = 0 
				ORDER BY id DESC");
			//$languages = _getLanguagesOfApp($this, $appid);
			if($qry_venues->num_rows() == 0) {
				// NO EVENTS
				$data['venues'] = array();
			} else {
				if($qry_venues->num_rows() > 1) {
					foreach ($qry_venues->result() as $row) {
						//translations
						if($lang != '' && $lang != $appinfo->defaultlanguage) {
							$transname = _getTranslation($this,'venue',$row->id,'name', $lang);
							if($transname != null) {
								$row->name = $transname->translation;
							}
							$transdescription = _getTranslation($this,'venue',$row->id,'info', $lang);
							if($transdescription != null) {
								$row->info = $transdescription->translation;
							}
						}

						for ($i=1; $i < 6; $i++) { 
							if($row->{'image'.$i} != '') { 
								$row->{'image'.$i} = base_url() . image_thumb($row->{'image'.$i}, 427, 640); 
							}
						}
						$tags = $this->db->query("SELECT * FROM tag WHERE venueid = $row->id")->result();
						$row->tags = $tags;
					}
					$data['venues'] = $qry_venues->result();
				} elseif($qry_venues->num_rows() == 1) {
					$venue = $qry_venues->row();
					$venueid = $venue->id;

					for ($i=1; $i < 6; $i++) { 
						if($venue->{'image'.$i} != '') { 
							$venue->{'image'.$i} = base_url() . image_thumb($venue->{'image'.$i}, 427, 640); 
						}
					}

					//translations
					if($lang != '' && $lang != $app->defaultlanguage) {
						$transname = _getTranslation($this,'venue',$venueid,'name', $lang);
						if($transname != null) {
							$venue->name = $transname->translation;
						}
						$transdescription = _getTranslation($this,'venue',$venueid,'info', $lang);
						if($transdescription != null) {
							$venue->info = $transdescription->translation;
						}
					}

					$tags = $this->db->query("SELECT * FROM tag WHERE venueid = $venueid")->result();
					$venue->tags = $tags;

					$data['venue'] = $venue;
					$app = $appinfo;
					
					$this->load->model('catalog_model');
					$modules = $this->db->query("SELECT moduletype.* FROM module LEFT JOIN moduletype ON (module.moduletype = moduletype.id) WHERE module.venue = $venueid")->result();
					// *** GET DATA PER MODULE *** //
					foreach($modules as $module) {
						if($app->apptypeid == 4) {
							if ($module->apicall != '' && $module->apicall != 'getAbout' && $module->name != 'Careers' && $module->name != 'Services' && $module->name != 'Projects' && $module->apicall != 'getEvent') {
								//get events not implemented yet
								require_once($module->apicall . '.php');
								$modcont = new $module->apicall();
								$moddata = $modcont->venue($this, $venueid, $lang);
								if($moddata != null) {
									$data[$module->controller]['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => $module->apicall);
									$data[$module->controller]['data'] = $moddata;
								}
							}
						} else {
							if ($module->apicall != '') {
								require_once($module->apicall . '.php');
								$modcont = new $module->apicall();
								$moddata = $modcont->venue($this, $venueid, $lang);
								if($moddata != null) {
									$data[$module->controller]['details'] = array('name' => $module->name, 'title' => $module->name, 'call' => $module->apicall);
									$data[$module->controller]['data'] = $moddata;
								}
							}
						}
					}
					
					if($app->apptypeid == 4) {
						require_once('getAbout.php');
						$modcont = new getAbout();

						$data['info']['data'] = $modcont->venue($this, $venueid, $lang, 'info');
						$data['contact']['data'] = $modcont->venue($this, $venueid, $lang, 'contact');
						$data['location']['data'] = $modcont->venue($this, $venueid, $lang, 'location');
					}
					
					//get launcher
					$launcherdata = $getLauncher->venue($this, $venueid, $lang);
					if($launcherdata != null) {
						$data['launcher'] = $launcherdata;
					}
				}
			}
            
			// GET APP ARTISTS
			$qry_artists = $this->db->query("SELECT * 
				FROM artist 
				WHERE appid = $appid
				ORDER BY id DESC");
			if($qry_artists->num_rows() == 0) {
				// NO EVENTS
				$data['artists'] = array();
			} else {
                $artists = $qry_artists->result();
                foreach ($artists as $artist) {
                    $qry_metadata = $this->db->query("SELECT `key`, `value` FROM metadata WHERE `table` = 'artist' AND `identifier` = $artist->id ");
                    $metadata = array();
                    foreach( $qry_metadata->result() as $row) {
                        $metadata[$row->key] = $row->value;
                    }
                    if($metadata != null && !empty($metadata)) {
                        $artist->metadata = $metadata;
                    }
                }
				$data['artists'] = $artists;
			}
			
			header('Content-type: application/json');
			echo json_encode($data);
			} 
		}
	}
	
}