<?php 

class getPOIs extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getPOIs() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id);
				break;
			case 'venue':
				$data = $this->venue($this, $id);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid) {
		$res = $obj->db->query("SELECT poi.name, poi.eventid, poi.x1, poi.x2, poi.y1, poi.y2, poitype.name as typename, poitype.icon FROM poi LEFT JOIN poitype ON (poi.poitypeid = poitype.id) WHERE poi.eventid = $eventid");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
	function venue($obj, $venueid) {
		$res = $obj->db->query("SELECT poi.name, poi.x1, poi.x2, poi.y1, poi.y2, poitype.name as typename, poitype.icon FROM poi LEFT JOIN poitype ON (poi.poitypeid = poitype.id) WHERE poi.venueid = $venueid");
		if($res->num_rows() == 0) return array();
		return $res->result();
	}
	
}