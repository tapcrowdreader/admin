<?php if(!defined('BASEPATH')) exit('No direct script access');

class Browser extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Browser() {
		parent::__construct();
	}
	
	function index() {
		if($this->input->post('postback') == 'postback') {
			$version 	= end(explode('/', $this->input->post('sel_version')));
			$function	= $this->input->post('sel_function');
			$max = (count($_POST['key']) > count($_POST['value'])) ? count($_POST['key']) : count($_POST['value']);
			
			//set POST variables
			$url = base_url() . 'api/' . $version . '/' . $function;
			
			$fields_string = ''; //'host='.$_SERVER['HTTP_REFERER'].'&';
			for($i=0; $i<$max; $i++){
				$fields_string .= strtolower($_POST['key'][$i]).'='.strtolower($_POST['value'][$i]).'&'; 
			}
			rtrim($fields_string,'&');
			
			// *** Load tracking-page *** //
			$var_utmac = 'UA-23619195-3'; //enter the new urchin code
			$var_utmhn = 'admin.tapcrowd.com'; //enter your domain
			$var_utmn = rand(1000000000,9999999999);//random request number
			$var_cookie = rand(10000000,99999999);//random cookie number
			$var_random = rand(1000000000,2147483647); //number under 2147483647
			$var_today = time(); //today
			$var_referer = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''); //referer url
			
			$var_uservar = '-'; //enter your own user defined variable
			$var_utmp = '/api/' . $version . '/' . $function; //this example adds a fake page request to the (fake) rss directory (the viewer IP to check for absolute unique RSS readers)
			
			$urchinUrl='http://www.google-analytics.com/__utm.gif?utmwv=1&utmn='.$var_utmn.'&utmsr=-&utmsc=-&utmul=-&utmje=0&utmfl=-&utmdt=-&utmhn='.$var_utmhn.'&utmr='.$var_referer.'&utmp='.$var_utmp.'&utmac='.$var_utmac.'&utmcc=__utma%3D'.$var_cookie.'.'.$var_random.'.'.$var_today.'.'.$var_today.'.'.$var_today.'.2%3B%2B__utmb%3D'.$var_cookie.'%3B%2B__utmc%3D'.$var_cookie.'%3B%2B__utmz%3D'.$var_cookie.'.'.$var_today.'.2.2.utmccn%3D(direct)%7Cutmcsr%3D(direct)%7Cutmcmd%3D(none)%3B%2B__utmv%3D'.$var_cookie.'.'.$var_uservar.'%3B';
			
			$handle = fopen ($urchinUrl, "r");
			$test = fgets($handle);
			fclose($handle);
			// *** //
			
			//open connection
			$ch = curl_init();
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_POST,count($_POST));
			curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
			
			if(curl_exec($ch) === false) {
				echo 'Gateway error: ' . curl_error($ch);
			} else {
				// Operation completed without any errors
			}
			
			$info = curl_getinfo($ch);
			echo "|||||<i>Time : </i> " . $info['total_time'] . " sec<br />\n" . 
			"<i>Download : </i>" . $this->_formatBytes($info['size_download']) . " at " . $this->_formatBytes($info['speed_download']) . "/sec" . "<br />\n" . 
			"<i>Upload : </i>" . $this->_formatBytes($info['size_upload']) . " at " . $this->_formatBytes($info['speed_upload']) . '/sec';
			
			//close connection
			curl_close($ch);
			
		} else {
			// versies ophalen
			$path = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']) . 'application/SITE/controllers/api';
			$scanres = scandir($path);
			$folders = array();
			
			foreach ($scanres as $result) {
				if ($result === '.' or $result === '..' or $result === '.svn') continue;
				
				if (is_dir($path . '/' . $result)) {
					//echo $path . '/' . $result . '<br />';
					$folders[$result] = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']) . 'application/SITE/controllers/api/' . $result;
					
					// functies per versie
					//$this->getDirectoryList(str_replace('api/index.php', '', $_SERVER['SCRIPT_FILENAME']) . 'application/API/controllers/' . $result)
					
				}
			}
			
			$this->load->view('api/form', array("versions" => $folders));
		}
	}
	
	function getFunctions() {
		echo json_encode($this->getDirectoryList($this->input->post('path')));
	}
	
	function getDirectoryList($directory) {
		$results = array();
		$handler = opendir($directory);
		while ($file = readdir($handler)) {
			if (preg_match("([^\s]+(?=\.(php)))", $file)) {
				$results[] = str_replace('.php', '', $file);
			}
		}
		closedir($handler);
		return $results;
	}
	
	function _formatBytes($size, $precision = 2) {
		$base = log($size) / log(1024);
		$suffixes = array('bytes', 'kb', 'Mb', 'Gb', 'Tb');
		
		return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
	}
	
}