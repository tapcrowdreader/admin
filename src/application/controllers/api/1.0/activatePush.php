<?php 

/**
 * This function activates a phone in database to receive push-messages.
 */
class activatePush extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function activatePush() {
		parent::MY_Controller();
	}
	
	function index($key = '', $phoneid = '', $appid = '') {
		// $key, $phoneid, $appid
		$key = ($this->input->post('key') ? $this->input->post('key') : '');
		$phoneid = ($this->input->post('phoneid') ? $this->input->post('phoneid') : '');
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		
		if($key != "" && $phoneid != '' && $appid != ""){
			$phoneid = str_replace(" ","",substr(substr($phoneid, 0, -1), 1));
			
			// Secret string opbouwen 
			$secret = md5("tcadm" . $appid);
			
			// Secret vergelijken met meegestuurde
			if($key == $secret) {
				
				// Insert favourite
				$userdata = array(
						'appid' 	=> $appid,
						'token' 		=> $phoneid
					);
				if($this->db->get_where('push', array('appid' => $appid, 'token' => $phoneid))->num_rows() == 0){
					if($this->db->insert('push', $userdata)){
						echo "OK";
					} else {
						echo "ERROR";
					}
				} else {
					echo "OK";
				}
			} else {
				echo "ERROR";
			}
		} else {
			echo "ERROR";
		}
	}
	
}