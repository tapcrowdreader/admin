<?php 

class getAttendees extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getAttendees() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '') {
	$app = _getAppFromEvent($obj, $eventid);
	$languages = _getLanguagesOfApp($obj, $app->id);

		$res = $obj->db->query("SELECT id, eventid, name, firstname, company, function, email, linkedin, phonenr, description FROM attendees WHERE eventid = $eventid ORDER BY firstname ASC");
		if($res->num_rows() == 0) return array();
		foreach($res->result() as $row){
			$row->linkedin = "?search_term=" . ($row->firstname != '' ? urlencode($row->firstname)."+" : "") 
												. ($row->name != '' ? urlencode($row->name) : "") 
												. ($row->company != '' ? "+".urlencode($row->company) : "") . "&filter=keywords&commit=Search";
												
			if($lang != '' && $lang != $app->defaultlanguage) {
			    $transdescription = _getTranslation($obj,'attendees',$row->id,'description', $lang);
			    if($transdescription != null) {
			        $row->description = $transdescription->translation;
			    }
			}
		}
		return $res->result();
	}
	
	function venue($obj, $venueid, $lang = '') {
		$res = $obj->db->query("SELECT id, eventid, name, firstname, company, function, email, linkedin, phonenr FROM attendees WHERE venueid = $venueid ORDER BY firstname ASC");
		if($res->num_rows() == 0) return array();
		/*foreach($res->result() as $row){}*/
		return $res->result();
	}
	
}