<?php 

class getSchedule extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getSchedule() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '') {
		$res = $obj->db->query("SELECT id, eventid, schedule.key, schedule.caption FROM schedule WHERE eventid = $eventid");
		if($res->num_rows() == 0) return array();
		/*foreach($res->result() as $row){}*/
		if($lang != '' && $lang != 'en') {
			foreach($res->result() as $row) {
				$transkey = _getTranslation($obj, '', 0, strtolower($row->key), $lang);
				if($transkey != null && $transkey != false) {
					$row->key = ucfirst($transkey->translation);
				}
			}
		}
		return $res->result();
	}
	
	function venue($obj, $venueid) {
		$res = $obj->db->query("SELECT id, venueid, schedule.key, schedule.caption FROM schedule WHERE venueid = $venueid");
		if($res->num_rows() == 0) return array();
		/*foreach($res->result() as $row){}*/
		return $res->result();
	}
	
}