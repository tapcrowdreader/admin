<?php 

class getSocialRanking extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getSocialRanking() {
		parent::MY_Controller();
	}
	
	function index($facebookid = '', $appid = '', $eventid = '', $deviceid = '', $devicetype = '') {
		$data = array();
		
		// RANKING BY APPID
		$facebookid = ($this->input->post('facebookid') ? $this->input->post('facebookid') : '');
		$appid = ($this->input->post('appid') ? $this->input->post('appid') : 0);
		$eventid = ($this->input->post('eventid') ? $this->input->post('eventid') : 0);
		
		if ($facebookid != '') {
			$data['user_saldo'] = $this->db->query("SELECT SUM(score) as totaal FROM socialscore WHERE facebookid = '$facebookid' AND appid = $appid LIMIT 1")->row()->totaal;
		} else {
			$data['user_saldo'] = 0;
		}
		
		// RANKING BY FACEBOOKID
		$data['ranking'] = $this->db->query("SELECT facebookid, SUM(score) as total_score FROM socialscore WHERE appid = $appid GROUP BY facebookid ORDER BY score DESC LIMIT 10")->result();
		
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
}