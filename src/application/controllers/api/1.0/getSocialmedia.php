<?php 

class getSocialmedia extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getSocialmedia() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id);
				break;
			case 'venue':
				$data = $this->venue($this, $id);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid) {
		$res = $obj->db->query("SELECT eventid, twitter, twithash, RSS, facebookid, facebookappid, postorwall FROM socialmedia WHERE eventid = $eventid LIMIT 1");
		if($res->num_rows() == 0) return array();
		$res->row()->twitfrom = "http://search.twitter.com/search.json?result_type=mixed&q=".$res->row()->twitter."&rpp=50";
		$res->row()->twithash = "http://search.twitter.com/search.json?result_type=mixed&q=".$res->row()->twithash."&rpp=50";
		$res->row()->attribute = $res->row()->postorwall;
		return $res->row();
	}
	
	function venue($obj, $venueid) {
		$res = $obj->db->query("SELECT venueid, twitter, twithash, RSS, facebookid, facebookappid, postorwall FROM socialmedia WHERE venueid = $venueid LIMIT 1");
		if($res->num_rows() == 0) return array();
		$res->row()->twitfrom = "http://search.twitter.com/search.json?result_type=mixed&q=".$res->row()->twitter."&rpp=50";
		$res->row()->twithash = "http://search.twitter.com/search.json?result_type=mixed&q=".$res->row()->twithash."&rpp=50";
		$res->row()->attribute = $res->row()->postorwall;
		return $res->row();
	}
	
}