<?php 

class getNews extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function getNews() {
		parent::MY_Controller();
	}
	
	function index($type = '', $id = '', $deviceid = '', $devicetype = '', $lang = '') {
		
		$data = array();
		$type = ($this->input->post('type') ? $this->input->post('type') : '');
		$id = ($this->input->post('id') ? $this->input->post('id') : 0);
		$lang = ($this->input->post('lang') ? $this->input->post('lang') : '');
		 
		if($type == '' || $type == null || $id == '' || $id == null) { echo "ERROR"; exit(); }
		
		switch ($type) {
			case 'event':
				$data = $this->event($this, $id, $lang);
				break;
			case 'venue':
				$data = $this->venue($this, $id, $lang);
				break;
			default:
				$data = array();
				break;
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
		
	}
	
	function event($obj, $eventid, $lang = '') {
		$app = _getAppFromEvent($obj, $eventid);
		$languages = _getLanguagesOfApp($obj, $app->id);
		$res = $obj->db->query("SELECT * FROM newssource WHERE eventid = $eventid");
	    if($res->num_rows() != 0) {
	        $sources = $res->result();
	        foreach($sources as $source) {
	            if($source->refreshrate!= null && $source->refreshrate != 0) {
	                $refreshrate = $source->refreshrate;
	            } else {
	                $refreshrate = 20;
	            }
	            if($source->timestamp < date("Y-m-d H:i:s",time()  - $refreshrate * 60)) {
	                //remove translations
	                $news = _getNewsBySourceId($obj,$source->id);
					if($news != null) {
	                foreach($news as $newsitem) {
	                    _removeTranslations($obj,'newsitem', $newsitem->id);
	                }
					}
	                
	                //remove newsitems
	                _removeNewsOfSourceId($obj, $source->id);
	                
	                //insert new news items of source
					try {
	                $content = file_get_contents($source->url); 
					if(!$content) {
						throw new Exception('news import failed');
					}
					} catch (Exception $e) {
						$content = null;
					}
					if($content != null && $content != false) {
						$rss = new SimpleXmlElement($content);

						foreach($rss->channel->item as $post) {
							$description = trim(strip_tags((string)$post->description));
							$date = ((string) $post->pubDate!= null && (string) $post->pubDate != '') ? date("Y-m-d H:i:s",strtotime((string) $post->pubDate)) : '';
							if($eventid == 517) {
								$data = array( 
										"title" 	=> (string)$post->title,
										"eventid" 	=> $eventid,
										"txt" 		=> trim(strip_tags((string)$post->description)),
										"url"		=> (string)$post->guid,
										"sourceid"  => $source->id,
										"datum"     => ((string) $post->pubDate!= null && (string) $post->pubDate != '') ? date("Y-m-d H:i:s",strtotime((string) $post->pubDate)) : date("Y-m-d H:i:s",time())
									);
							} else {
								$data = array( 
										"title" 	=> (string)$post->title,
										"eventid" 	=> $eventid,
										"txt" 		=> $description,
										"url"		=> (string)$post->link,
										"sourceid"  => $source->id,
										"datum"     => $date
									);
							}
							if($newsid = _insert($obj, 'newsitem', $data)) {
								//add translated fields to translation table
								if($languages != null) {
									foreach($languages as $language) {
										_addTranslation($obj, 'newsitem', $newsid, 'title', $language->key, (string)$post->title);
										_addTranslation($obj, 'newsitem', $newsid, 'txt', $language->key, (string)$post->description);
									}
								} 
							} else {
								$error = "Oops, something went wrong. Please try again.";
							}
						}
					}
	                //update timestamp
	                $newssourceData = array(
	                    "timestamp" => date("Y-m-d H:i:s",time())
	                );
	                _update($obj, 'newssource', $source->id, $newssourceData);
					_updateTimeStampAPI($obj, $eventid);
	            }
	        }
	    }
	    
	    $res = $obj->db->query("SELECT id, title, txt, image, url, sourceid, datum, eventid FROM newsitem WHERE eventid = $eventid ORDER BY datum DESC, id ASC");
	    if($res->num_rows() == 0) return array();
	    $rows = array();
		foreach($res->result() as $row){
			if($row->image != ''){
				$newspicwidth = _get_meta_data_by_key($obj, 'app', $app->id, 'newspicwidth');
				$newspicheight = _get_meta_data_by_key($obj, 'app', $app->id, 'newspicheight');
				if($newspicheight != null && $newspicwidth != null) {
					$newspicwidth = (int) ($newspicwidth->value) / 2;
					$newspicheight = (int) ($newspicheight->value) / 2;
					$row->image_cropped = base_url() . _api_image_crop($obj, $row->image, $newspicwidth, $newspicheight);
				}
				$row->image = base_url() . $row->image;
			}
	        
	        if($lang != '' && $lang != $app->defaultlanguage) {
	            $transtitle = _getTranslation($obj, 'newsitem',$row->id,'title', $lang);
	            if($transtitle != null) {
	                $row->title = (string)$transtitle->translation;
	            }
	            $transtxt = _getTranslation($obj, 'newsitem',$row->id,'txt', $lang);
	            if($transtxt != null) {
	                $row->txt = (string)$transtxt->translation;
	            }
	        }
	        $rows[] = $row;
		}
		return $rows;
	}
	
	
	function venue($obj, $venueid, $lang = '') {
		$app = _getAppFromVenue($obj, $venueid);
		$languages = _getLanguagesOfApp($obj, $app->id);
		$res = $obj->db->query("SELECT * FROM newssource WHERE venueid = $venueid");
		if($res->num_rows() != 0) {
			$sources = $res->result();
	        foreach($sources as $source) {
	            if($source->refreshrate!= null && $source->refreshrate != 0) {
	                $refreshrate = $source->refreshrate;
	            } else {
	                $refreshrate = 20;
	            }
	            if($source->timestamp < date("Y-m-d H:i:s",time() - $refreshrate * 60)) {
	                //remove translations
	                $news = _getNewsBySourceId($obj,$source->id);
	                foreach($news as $newsitem) {
	                    _removeTranslations($obj,'newsitem', $newsitem->id);
	                }
	                
	                //remove newsitems
	                _removeNewsOfSourceId($obj, $source->id);
	                
	                //insert new news items of source
	                $content = file_get_contents($source->url);  
	                $rss = new SimpleXmlElement($content);
	                
	                foreach($rss->channel->item as $post) {                    
	                    $data = array( 
	                            "title" 	=> (string)$post->title,
	                            "venueid" 	=> $venueid,
	                            "txt" 		=> (string)$post->description,
	                            "url"		=> (string)$post->link,
	                            "sourceid"  => $source->id,
	                            "datum"     => ((string) $post->pubDate!= null && (string) $post->pubDate != '') ? date("Y-m-d H:i:s",strtotime((string) $post->pubDate)) : date("Y-m-d H:i:s",time())
	                        );
	                    if($newsid = _insert($obj, 'newsitem', $data)) {
	                        //add translated fields to translation table
	                        if($languages != null) {
	                            foreach($languages as $language) {
	                                _addTranslation($obj, 'newsitem', $newsid, 'title', $language->key, (string)$post->title);
	                                _addTranslation($obj, 'newsitem', $newsid, 'txt', $language->key, (string)$post->description);
	                            }
	                        } 
	                    } else {
	                        $error = "Oops, something went wrong. Please try again.";
	                    }
	                }
	                
	                //update timestamp
	                $newssourceData = array(
	                    "timestamp" => date("Y-m-d H:i:s",time())
	                );
	                _update($obj, 'newssource', $source->id, $newssourceData);
	            }
	        }
	    }
	    
	    $res = $obj->db->query("SELECT id, venueid, title, txt, image, url, sourceid, datum FROM newsitem WHERE venueid = $venueid ORDER BY datum DESC, id ASC");
	    if($res->num_rows() == 0) return array();
	    $rows = array();
		foreach($res->result() as $row){
			if($row->image != ''){
				$newspicwidth = _get_meta_data_by_key($obj, 'app', $app->id, 'newspicwidth');
				$newspicheight = _get_meta_data_by_key($obj, 'app', $app->id, 'newspicheight');
				if($newspicheight != null && $newspicwidth != null) {
					$newspicwidth = (int) ($newspicwidth->value) / 2;
					$newspicheight = (int) ($newspicheight->value) / 2;
					$row->image_cropped = base_url() . _api_image_crop($obj, $row->image, $newspicwidth, $newspicheight);
				}
				$row->image = base_url() . $row->image;
			}
	        
	        if($lang != '' && $lang != $app->defaultlanguage) {
	            $transtitle = _getTranslation($obj, 'newsitem',$row->id,'title', $lang);
	            if($transtitle != null) {
	                $row->title = (string)$transtitle->translation;
	            }
	            $transtxt = _getTranslation($obj, 'newsitem',$row->id,'txt', $lang);
	            if($transtxt != null) {
	                $row->txt = (string)$transtxt->translation;
	            }
	        }
	        $rows[] = $row;
		}
		return $rows;
	}	
}