<?php 

class deleteFavourite extends MY_Controller {

	//php 5 constructor
	function __construct() {
		parent::MY_Controller();
	}
	
	//php 4 constructor
	function deleteFavourite() {
		parent::MY_Controller();
	}
	
	function index($key = '', $email = '', $eventid = '', $exhibitorid = '', $sessionid = '') {
		// $key, $email, $eventid, $exhibitorid, $sessionid
		
		$key = ($this->input->post('key') ? $this->input->post('key') : '');
		$email = ($this->input->post('email') ? $this->input->post('email') : '');
		$eventid = ($this->input->post('eventid') ? $this->input->post('eventid') : 0);
		$exhibitorid = ($this->input->post('exhibitorid') ? $this->input->post('exhibitorid') : 0);
		$sessionid = ($this->input->post('sessionid') ? $this->input->post('sessionid') : 0);
		
		if($key != "" && $email != '' && $eventid != ""){
			// Secret string opbouwen 
			$secret = md5("tcadm" . $eventid);
			
			// Secret vergelijken met meegestuurde
			if($key == $secret) {
				
				// Insert favourite
				$userdata = array(
						'useremail' 	=> $email,
						'eventid' 		=> $eventid,
						'exhibitorid' 	=> $exhibitorid,
						'sessionid' 	=> $sessionid
					);
				
				if($this->db->delete('favorites', $userdata)){
					echo "OK";
				} else {
					echo "ERROR";
				}
			} else {
				echo "ERROR";
			}
		} else {
			echo "ERROR";
		}
	}
	
}