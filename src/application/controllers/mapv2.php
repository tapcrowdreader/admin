<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Mapv2 extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('map_model');
        $this->load->model('poi_model');
	}
	
	function index() {
		$this->event();
	}
	
	function event($eventid, $type = 'exhibitor', $mapid = 0) {
		if($eventid == FALSE || $eventid == 0) redirect('events');	    
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		if($app->apptypeid == 10 && $type == 'exhibitor') {
			redirect('mapv2/event/'.$eventid.'/session/'.$mapid);
		}
		$maps = $this->map_model->getMapsByEvent($eventid);

		$map = false;
		if($mapid != 0) {
			$map = $this->map_model->getMapsById($mapid);
		} elseif(isset($maps[0])) {
			$map = $maps[0];
		}
		
		$markers = array();
		$zoommaps = array();
		if($map != false && $map != null) {
			$markers = $this->map_model->getMarkersByMapId($map->id, $type, $eventid);
			$zoommaps = $this->map_model->getZoomMaps($map->id);
		}
		

		$this->iframeurl = "mapv2/event/" . $eventid.'/0/'.$map->id;
		
		// Exhibitors meegeven voor koppeling aan locaties op map (indien juiste flavor)
		if ($type == 'session') {
			$sessions = FALSE;
			$this->load->model('sessions_model');
			$sessions = $this->sessions_model->getUnmappedSessionsByEvent($event->id);
			$sessionlocations = $this->sessions_model->getSessionLocationByEventId($event->id);
			$content_data['sessions'] = $sessions;
			$content_data['sessionlocations'] = $sessionlocations;
		} elseif($type == 'poi') {
            // $markers = $this->poi_model->getPoisByMapId($mapid);
        // } elseif($type == 'subfloorplan') {
        // 	$markers = $this->map_model->getSubfloorplans($map->id);
        }
        else {
			$exhibitors = FALSE;
			$this->load->model('exhibitor_model');
			$exhibitors = $this->exhibitor_model->getUnmappedByEvent($event->id, $mapid);
			$content_data['exhibitors'] = $exhibitors;
		}
		
		$content_data['event'] = $event;
		$content_data['app'] = $app;
		$content_data['maps'] = $maps;
		$content_data['map'] = $map;


		$content_data['zoommaps'] = $zoommaps;
		
		$content_data['markers'] = $markers;
		
		$cdata['content'] 		= $this->load->view('c_mapv2', $content_data, TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("event" => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __('Map') => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, __('Map') => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'mapv2');
		$this->load->view('master', $cdata);

	}
	
	function venue($venueid, $type = 'subfloorplan', $mapid = 0) {
		if($venueid == FALSE || $venueid == 0) redirect('venues');
		
		$venue = $this->venue_model->getById($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$markers = array(); 
		
		$maps = $this->map_model->getMapsByVenue($venue->id);
		if($maps != false) {
			if($mapid != 0) {
				$map = $this->map_model->getMapsById($mapid);
			} else {
				$map = $maps[0];
			}
			// if($type == 'subfloorplan' || $type == '') {
			// 	$markers = $this->map_model->getSubfloorplans($map->id);
			if($type == 'poi') {
				$markers = $this->poi_model->getPoisByMapIdForVenue($map->id);
			} elseif($type == 'resto') {
				$markers = $this->map_model->getTablesByMapIdForVenue($map->id);
			}

			$this->iframeurl = "mapv2/venue/" . $venueid.'/0/'.$map->id;
		} else {
			$map = false;
		}		
		
		// Exhibitors meegeven voor koppeling aan locaties op map (indien juiste flavor)
		$exhibitors = FALSE;
		$this->load->model('exhibitor_model');
		$exhibitors = $this->exhibitor_model->getUnmappedByVenue($venue->id);
		
		$cdata['content'] 		= $this->load->view('c_mapv2', array('venue' => $venue, 'maps' => $maps, 'map' => $map, 'exhibitors' => $exhibitors, 'markers' => $markers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("venue" => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __('Map') => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'mapv2');
		$this->load->view('master', $cdata);
	}

	function edit($mapid, $type = 'event') {
		if($type == 'event') {
			$map = $this->map_model->getMapsById($mapid);
		    $this->iframeurl = "mapv2/by-event/" . $map->eventid;
		    
			$event = $this->event_model->getbyid($map->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			redirect('mapv2/event/'.$event->id.'/exhibitor/'.$map->id);
		}
	}
	
	function addMarker() {
		if(stristr($_SERVER['HTTP_REFERER'], base_url()) != FALSE) {
			if($this->input->post('postback') == 'postback'){
				$this->load->model('exhibitor_model');
				$posx = $this->input->post('posx');
				$posy = $this->input->post('posy');
				$mapid = $this->input->post('mapid');
				$markerid = $this->input->post('markerid');
				$sel_koppel = $this->input->post('sel_koppel');
				$exhibitor = $this->exhibitor_model->getById($sel_koppel);
				$event = $this->event_model->getbyid($exhibitor->eventid);
				$app = _actionAllowed($event, 'event', 'returnApp');
				if($this->general_model->update('exhibitor', $sel_koppel, array('mapid' => $mapid, 'x1' => $posx, 'y1' => $posy, 'x2' => 0, 'y2' => 0))) {
					_updateTimeStamp($exhibitor->eventid);
					echo TRUE;
				} else {
					echo FALSE;
				}
			} else {
				echo FALSE;
			}
		} else {
			echo __("False, intruder!");
		}
	}
	
	function addSessionMarker() {
		if(stristr($_SERVER['HTTP_REFERER'], base_url()) != FALSE) {
			if($this->input->post('postback') == 'postback'){
				$posx = $this->input->post('posx');
				$posy = $this->input->post('posy');
				$mapid = $this->input->post('mapid');
				$markerid = $this->input->post('markerid');
				$sel_koppel = $this->input->post('sel_koppel');
				// if($this->general_model->update('session', $sel_koppel, array('xpos' => $posx, 'ypos' => $posy))) {
				// 	echo TRUE;
				// } else {
				// 	echo FALSE;
				// }
				$this->load->model('sessions_model');
				$session = $this->sessions_model->getById($sel_koppel);
				$event = $this->event_model->getbyid($session->eventid);
				$app = _actionAllowed($event, 'event', 'returnApp');
				if($this->sessions_model->mapLocations($posx,$posy,$sel_koppel, $mapid)) {
					_updateTimeStamp($session->eventid);
					echo true;
				} else {
					echo false;
				}
			} else {
				echo FALSE;
			}
		} else {
			echo __("False, intruder!");
		}
	}

	function addSubfloorplan() {
		// if(stristr($_SERVER['HTTP_REFERER'], base_url()) != FALSE) {
		// 	if($this->input->post('postback') == 'postback'){
		// 		$eventid = $this->input->post('eventid');
		// 		$parentid = $this->input->post('parentid');
		// 		if($parentid == false) {
		// 			$maps = $this->map_model->getMapsByEvent($eventid);
		// 			$parentid = $maps[0]->id;
		// 		}
		// 		$posx = $this->input->post('posx');
		// 		$posy = $this->input->post('posy');
		// 		$name = '';
		// 		if($this->input->post('floorplanname')) {
		// 			$name = $this->input->post('floorplanname');
		// 		}
				
		// 		$markerid = $this->input->post('markerid');

		// 		if(!is_dir($this->config->item('imagespath') . 'upload/maps/events/'.$eventid)){
		// 			mkdir($this->config->item('imagespath') . 'upload/maps/events/'.$eventid, 0755, true);
		// 		}
		// 		$configmap['upload_path'] = $this->config->item('imagespath') . 'upload/maps/events/'.$eventid;
		// 		$configmap['allowed_types'] = 'JPG|jpg|jpeg|png';
		// 		$configmap['max_width']  = '3000';
		// 		$configmap['max_height']  = '3000';
		// 		$configmap['max_size']  = '2048';
		// 		$this->load->library('upload', $configmap);
		// 		$this->upload->initialize($configmap);
		// 		if($_FILES['subfloorplan']['name'] == '') {
		// 		    // niets doen, geen upload
		// 		} else {
		// 		    if (!$this->upload->do_upload('subfloorplan')) {
		// 		        // DO nothing.. not succesfull!
		// 		        $internal_error = TRUE;
		// 		        $error = $this->upload->display_errors('','<br />');
		// 				redirect('map/event/'.$eventid.'/?error=image');
		// 		    } else {
		// 		        // successfully uploaded
		// 		        $returndata = $this->upload->data();
		// 		        $image = $returndata['file_name'];
		// 		        $width = $returndata['image_width'];
		// 		        $height = $returndata['image_height'];

		// 		        $splited = split_map('upload/maps/events/'.$eventid.'/'.$image, $eventid, 'event');

		// 		        $this->general_model->insert('map', array(
		// 		        		'parentid' => $parentid,
		// 		        		'eventid' => $eventid,
		// 		        		'imageurl' => 'upload/maps/events/'.$eventid.'/'.$image,
		// 		        		'timestamp' => time(),
		// 		        		'width' => $width,
		// 		        		'height' => $height,
		// 		        		'x' => $posx,
		// 		        		'y' => $posy,
		// 		        		'title' => $name
		// 		        	));

		// 		        _updateTimeStamp($eventid);

		// 		        redirect('map/event/'.$eventid.'/subfloorplan/'.$parentid);
		// 		    }
		// 		}

		// 	} else {
		// 		echo FALSE;
		// 	}
		// } else {
		// 	echo __("False, intruder!");
		// }
	}

	function addSubfloorplanVenue() {
		// if(stristr($_SERVER['HTTP_REFERER'], base_url()) != FALSE) {
		// 	if($this->input->post('postback') == 'postback'){
		// 		$venueid = $this->input->post('venueid');
		// 		$parentid = $this->input->post('parentid');
		// 		if($parentid == false) {
		// 			$maps = $this->map_model->getMapsByVenue($venueid);
		// 			$parentid = $maps[0]->id;
		// 		}
		// 		$posx = $this->input->post('posx');
		// 		$posy = $this->input->post('posy');
		// 		$name = '';
		// 		if($this->input->post('floorplanname')) {
		// 			$name = $this->input->post('floorplanname');
		// 		}
		// 		$markerid = $this->input->post('markerid');

		// 		if(!is_dir($this->config->item('imagespath') . 'upload/maps/venues/'.$venueid)){
		// 			mkdir($this->config->item('imagespath') . 'upload/maps/venues/'.$venueid, 0755, true);
		// 		}
		// 		$configmap['upload_path'] = $this->config->item('imagespath') . 'upload/maps/venues/'.$venueid;
		// 		$configmap['allowed_types'] = 'JPG|jpg|jpeg|png';
		// 		$configmap['max_width']  = '3000';
		// 		$configmap['max_height']  = '3000';
		// 		$configmap['max_size']  = '2048';
		// 		$this->load->library('upload', $configmap);
		// 		$this->upload->initialize($configmap);
		// 		if($_FILES['subfloorplan']['name'] == '') {
		// 		    // niets doen, geen upload
		// 		} else {
		// 		    if (!$this->upload->do_upload('subfloorplan')) {
		// 		        // DO nothing.. not succesfull!
		// 		        $internal_error = TRUE;
		// 		        $error = $this->upload->display_errors('','<br />');
		// 				redirect('map/venue/'.$venueid.'/?error=image');
		// 		    } else {
		// 		        // successfully uploaded
		// 		        $returndata = $this->upload->data();
		// 		        $image = $returndata['file_name'];
		// 		        $width = $returndata['image_width'];
		// 		        $height = $returndata['image_height'];

		// 		        $splited = split_map('upload/maps/venues/'.$venueid.'/'.$image, $venueid, 'venue');

		// 		        $this->general_model->insert('map', array(
		// 		        		'parentid' => $parentid,
		// 		        		'venueid' => $venueid,
		// 		        		'imageurl' => 'upload/maps/venues/'.$venueid.'/'.$image,
		// 		        		'timestamp' => time(),
		// 		        		'width' => $width,
		// 		        		'height' => $height,
		// 		        		'x' => $posx,
		// 		        		'y' => $posy,
		// 		        		'title' => $name
		// 		        	));

		// 		        _updateVenueTimeStamp($venueid);

		// 		        redirect('map/venue/'.$venueid.'/subfloorplan/'.$parentid);
		// 		    }
		// 		}

		// 	} else {
		// 		echo FALSE;
		// 	}
		// } else {
		// 	echo __("False, intruder!");
		// }
	}

	function removefloorplan($eventid, $mapid, $id, $type = 'event') {
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		$this->general_model->remove('map', $id);
		$this->map_model->removeItemsOfMap($id, 'event', $eventid);
		_updateTimeStamp($eventid);
		redirect('mapv2/'.$type.'/'.$eventid.'/subfloorplan/'.$mapid.'/');
	}

	function removefloorplanvenue($venueid, $mapid, $id, $type = 'venue') {
		$venue = $this->venue_model->getById($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->general_model->remove('map', $id);
		$this->map_model->removeItemsOfMap($id, 'venue', $venueid);
		_updateVenueTimeStamp($venueid);
		redirect('mapv2/'.$type.'/'.$venueid.'/subfloorplan/'.$mapid.'/');
	}
	
	function removemarker($eventid, $mapid, $id, $type = 'event') {
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		$this->general_model->update('exhibitor', $id, array('x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 0));
		_updateTimeStamp($eventid);
		redirect('mapv2/'.$type.'/'.$eventid.'/exhibitor/'.$mapid.'/');
	}
	
	function removeSessionmarker($eventid, $mapid, $id, $type = 'event') {
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		$this->load->model('sessions_model');
		$this->sessions_model->mapLocations(0,0,$id, $mapid);
		// $this->general_model->update('session', $id, array('xpos' => 0, 'ypos' => 0));
		_updateTimeStamp($eventid);
		redirect('mapv2/'.$type.'/'.$eventid.'/session/'.$mapid.'/');
	}
	
	function removemarkerVenue($venueid, $mapid, $id, $type = 'venue') {
		$this->general_model->update('exhibitor', $id, array('x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 0));
		redirect('mapv2/'.$type.'/'.$venueid.'/'.$mapid.'/');
	}
    
	function removeTable($venueid, $mapid, $tableid)
	{
		$this->db->query("DELETE FROM tables_in_venue WHERE venueid=$venueid AND mapid=$mapid AND tableid=$tableid");
		redirect('mapv2/venue/'.$venueid.'/resto/'.$mapid);
	}
	
    function add($id, $type='event') {
        if($type == 'event') {
            $this->load->library('form_validation');
            $event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
            if($this->input->post('postback') == "postback") {
            	$this->form_validation->set_rules('floorplanname1', 'name', 'trim');
                $this->form_validation->set_rules('w_map', 'w_map', 'trim');
				$name = '';
				if($this->input->post('floorplanname1')) {
					$name = $this->input->post('floorplanname1');
				}
                if($this->form_validation->run() == FALSE){
                    $error = __("Some fields are missing.");
					redirect('mapv2/event/'.$id.'/?error=noimage');
                } else {
                    //Do after submit of eventdata
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . 'upload/maps/events/'.$id)){
						mkdir($this->config->item('imagespath') . 'upload/maps/events/'.$id, 0755, true);
					}
                    $configmap['upload_path'] = $this->config->item('imagespath') . 'upload/maps/events/'.$id;
                    $configmap['allowed_types'] = 'JPG|jpg|jpeg|png';
                    $configmap['max_size']  = '10240';
                    $this->load->library('upload', $configmap);
                    $this->upload->initialize($configmap);
                    if($_FILES['w_map']['name'] == '') {
                        // niets doen, geen upload
						redirect('mapv2/event/'.$id.'/?error=noimage');
                    } else {
                        if (!$this->upload->do_upload('w_map')) {
                            // DO nothing.. not succesfull!
                            $internal_error = TRUE;
                            $error = $this->upload->display_errors('','<br />');
							redirect('mapv2/event/'.$id.'/?error=image');
                        } else {
                            // successfully uploaded
                            $returndata = $this->upload->data();
                            $image = $returndata['file_name'];
                            $width = $returndata['image_width'];
                            $height = $returndata['image_height'];
                            $this->load->model('map_model');
                            if($this->input->post('replaceId') != null) {
                                $this->map_model->edit($this->input->post('replaceId'), 'upload/maps/events/'.$id.'/'.$image, $width, $height, $name);
                            } else {
                                $this->map_model->add($event->id, 'upload/maps/events/'.$id.'/'.$image, $width, $height, $name);
                            }
                            $splited = split_map('upload/maps/events/'.$id.'/'.$image, $id, 'event');

                            _updateTimeStamp($event->id);
                            if($this->input->post('replaceId') != null) {
                                redirect('mapv2/event/'.$id.'/subfloorplan/'.$this->input->post('replaceId'));
                            } else {
                                redirect('mapv2/event/'.$id);
                            }
                            
                        }
                    }
                }
            }
        } else {
            $this->load->library('form_validation');
            $venue = $this->venue_model->getbyid($id);
            $app = _actionAllowed($venue, 'venue','returnApp');
            if($this->input->post('postback') == "postback") {
                $this->form_validation->set_rules('w_map', 'w_map', 'trim');
                if($this->form_validation->run() == FALSE){
                    $error = __("Some fields are missing.");
					redirect('mapv2/venue/'.$id.'/?error=noimage');
                } else {
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . 'upload/maps/venues/'.$id)){
						mkdir($this->config->item('imagespath') . 'upload/maps/venues/'.$id, 0755, true);
					}
					$configmap['upload_path'] = $this->config->item('imagespath') . 'upload/maps/venues/'.$id;
					$configmap['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configmap['max_size']  = '10240';
					$this->load->library('upload', $configmap);
					$this->upload->initialize($configmap);
                    if($_FILES['w_map']['name'] == '') {
                        // niets doen, geen upload
						redirect('mapv2/venue/'.$id.'/?error=noimage');
                    } else {
						if (!$this->upload->do_upload('w_map')) {
							// DO nothing.. not succesfull!
							redirect('mapv2/venue/'.$id.'/?error=image');
						} else {
							// successfully uploaded
							$returndata = $this->upload->data();
							$image = $returndata['file_name'];
                            $width = $returndata['image_width'];
                            $height = $returndata['image_height'];
							$this->load->model('map_model');
                            if($this->input->post('replaceId') != null) {
                                $this->map_model->editVenueMap($this->input->post('replaceId'), 'upload/maps/venues/'.$id.'/'.$image, $width, $height, $name);
                            } else {
                                $this->map_model->addVenueMap($venue->id, 'upload/maps/venues/'.$id.'/'.$image, $width, $height, $name);
                            }

							_updateVenueTimeStamp($venue->id);
							$splited = split_map('upload/maps/venues/'.$id.'/'.$image, $id, 'venue');
                            if($this->input->post('replaceId') != null) {
                                redirect('mapv2/venue/'.$id.'/subfloorplan/'.$this->input->post('replaceId'));
                            } else {
                                redirect('mapv2/venue/'.$id);
                            }
						}
					}
                }
            }
        }
    }
    
	function removepoi($eventid, $mapid, $id) {
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		$this->general_model->remove('poi', $id);
		redirect('mapv2/event/'.$eventid.'/poi/');
	}
    
	function addPoi() {
		if(stristr($_SERVER['HTTP_REFERER'], base_url()) != FALSE) {
			if($this->input->post('postback') == 'postback'){
				
				$data = array(
					'eventid'	=> $this->input->post('eventid'),
					'venueid'	=> $this->input->post('venueid'),
					'name'		=> $this->input->post('name'),
					'poitypeid' => $this->input->post('sel_koppel'),
					'x1' 		=> $this->input->post('posx'), 
					'y1' 		=> $this->input->post('posy'), 
					'x2' 		=> 0, 
					'y2' 		=> 0
				);
				
				if($this->general_model->insert('poi', $data)) {
					_updateTimeStamp($this->input->post('eventid'));
					echo TRUE;
				} else {
					echo FALSE;
				}
			} else {
				echo FALSE;
			}
		} else {
			echo "False, intruder!";
		}
	}
    
	function addTable() {
		if(stristr($_SERVER['HTTP_REFERER'], base_url()) != FALSE) {
			if($this->input->post('postback') == 'postback'){
				
				$data = array(
					'venueid'	=> $this->input->post('venueid'),
					'mapid'		=> $this->input->post('mapid'),
					'tableid' 	=> $this->input->post('tableid'),
					'xpos' 		=> $this->input->post('posx'), 
					'ypos' 		=> $this->input->post('posy')
				);
				
				$result = $this->map_model->getTableForVenue($data['mapid'], $data['venueid'], $data['tableid']);
				
				if(empty($result))
				{
					if($this->general_model->insert('tables_in_venue', $data)) {
						_updateTimeStamp($this->input->post('venueid'));
						redirect("mapv2/venue/".$data['venueid']."/resto/".$data['mapid']);
					} else {
						echo FALSE;
					}
				}
				else{
					if($this->db->update('tables_in_venue', $data)) {
						_updateTimeStamp($this->input->post('venueid'));
						redirect("mapv2/venue/".$data['venueid']."/resto/".$data['mapid']);
					} else {
						echo FALSE;
					}
				}
			} else {
				echo FALSE;
			}
		} else {
			echo "False, intruder!";
		}
	}
	
	function removepoiVenue($venueid, $mapid, $id) {
		$this->general_model->remove('poi', $id);
		redirect('poi/venue/'.$venueid.'/'.$mapid.'/');
	}

	function addzoommap($typeid, $type) {
		if($type == 'event') {
			$object = $this->event_model->getbyid($typeid);
			$maps = $this->map_model->getMapsByEvent($typeid);
			$map = $maps[0];
		}

		$app = _actionAllowed($object, $type,'returnApp');

		if($this->input->post('zoommap_level') && isset($_FILES['zoommap'])) {
			if(!is_dir($this->config->item('imagespath') . 'upload/maps/'.$type.'s/'.$typeid)){
				mkdir($this->config->item('imagespath') . 'upload/maps/'.$type.'s/'.$typeid, 0775, true);
			}
			$configmap['upload_path'] = $this->config->item('imagespath') . 'upload/maps/'.$type.'s/'.$typeid;
			$configmap['allowed_types'] = 'JPG|jpg|jpeg|png';
			$configmap['max_size']  = '10240';
			$this->load->library('upload', $configmap);
			$this->upload->initialize($configmap);
			if($_FILES['zoommap']['name'] == '') {
				redirect('mapv2/'.$type.'/'.$typeid.'/?error=noimage');
			} else {
			    if (!$this->upload->do_upload('zoommap')) {
			        $internal_error = TRUE;
			        $error = $this->upload->display_errors('','<br />');
					redirect('mapv2/'.$type.'/'.$typeid.'/?error=image');
			    } else {
			        $returndata = $this->upload->data();
			        $image = 'upload/maps/'.$type.'s/'.$typeid.'/'.$returndata['file_name'];

					$this->general_model->insert('map_zoomlevel', array(
						'mapid' => $map->id, 
						'imageurl' => $image, 
						'zoomlevel' => $this->input->post('zoommap_level')
					));
			    }
			}
		}

		redirect('mapv2/'.$type.'/'.$typeid);
	}

	function removezoommap($id) {
		if(is_numeric($id)) {
			$zoommap = $this->general_model->get('map_zoomlevel', 'id', $id);
			$map = $this->map_model->getMapsById($zoommap->mapid);
			if($map->eventid != 0) {
				$object = $this->event_model->getbyid($map->eventid);
				$type = 'event';
			}

			$app = _actionAllowed($object, $type,'returnApp');

			$this->general_model->remove('map_zoomlevel', $id);
		}
		redirect('mapv2/'.$type.'/'.$object->id);
	}

	function addroutingpoint($mapid) {
		if($this->input->post('xpos') && $this->input->post('ypos')) {
			$map = $this->map_model->getMapsById($mapid);
			if($map->eventid != 0) {
				$object = $this->event_model->getbyid($map->eventid);
				$type = 'event';
			}

			$app = _actionAllowed($object, $type,'returnApp');

			$this->general_model->insert('map_routingpoint', array(
					'mapid' => $mapid, 
					'x' => (int)$this->input->post('xpos'),
					'y' => (int)$this->input->post('ypos')
				));
		}

		exit;
	}

	function removeindoorroutingpoint($eventid, $mapid, $pointid) {
		$map = $this->map_model->getMapsById($mapid);
		if($map->eventid == $eventid) {
			$object = $this->event_model->getbyid($map->eventid);
			$type = 'event';
		}

		$app = _actionAllowed($object, $type,'returnApp');

		$point = $this->map_model->getIndoorRoutingPoint($pointid);
		if($point->mapid == $map->id) {
			$this->map_model->removeIndoorRoutingPoint($point->id);
		}

		// redirect('mapv2/event/'.$eventid.'/indoorrouting/'.$mapid);
		exit;
	}

	function removeindoorroutingpointbycoord($eventid, $mapid, $x, $y, $x2, $y2) {
		$map = $this->map_model->getMapsById($mapid);
		if($map->eventid == $eventid) {
			$object = $this->event_model->getbyid($map->eventid);
			$type = 'event';
		}

		$app = _actionAllowed($object, $type,'returnApp');

		$point = $this->map_model->getIndoorRoutingPointByXY($mapid, $x, $y);
		if($point->mapid == $map->id) {
			$this->map_model->removeIndoorRoutingPoint($point->id);
		}

		// redirect('mapv2/event/'.$eventid.'/indoorrouting/'.$mapid);
		exit;
	}

	function removeroutingpath($pathid) {
		$path = $this->map_model->getIndoorRoutingPath($pathid);
		$object = $this->event_model->getbyid($path->eventid);
		$type = 'event';

		$app = _actionAllowed($object, $type,'returnApp');

		$this->map_model->removeIndoorRoutingPath($path->id);
		// redirect('mapv2/event/'.$path->eventid.'/indoorrouting/'.$path->mapid);
		exit;
	}

	function addroutingpath($mapid, $x, $y, $x2, $y2) {
		$x = (int)$x;
		$y = (int)$y;
		$x2 = (int)$x2;
		$y2 = (int)$y2;
		$map = $this->map_model->getMapsById($mapid);
		$object = $this->event_model->getbyid($map->eventid);
		$type = 'event';

		$app = _actionAllowed($object, $type,'returnApp');

		$point1 = $this->map_model->getIndoorRoutingPointByXY($map->id, $x, $y);
		$point2 = $this->map_model->getIndoorRoutingPointByXY($map->id, $x2, $y2);
		if($point1 && $point2) {
			$pathid = $this->general_model->insert('map_routingpath', array(
				'mapid' => $map->id, 
				'map_routingpointid_start' => $point1->id, 
				'map_routingpointid_end' => $point2->id)
			);
		}

		echo $pathid;
		exit;

		// redirect('mapv2/event/'.$map->eventid.'/indoorrouting/'.$map->id);

	}

}