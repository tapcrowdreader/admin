<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Quiz extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('quiz_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Quiz',
			'plural' => 'Quiz',
			'title' => __('Quiz'),
			'parentType' => 'event',
			'browse_url' => 'quiz/--parentType--/--parentId--',
			'add_url' => 'quiz/addquestion/--quizid--',
			'edit_url' => 'quiz/editquestion/--id--',
			'module_url' => 'module/editByController/quiz/--parentType--/--parentId--/0',
			'import_url' => 'import/quiz/--parentType--/--parentId--',
			'headers' => array(
				__('Question') => 'questiontext'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'quiz/editquestion/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'quiz/deletequestion/--id--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_quizquestion_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'checkboxes' => false,
			'deletecheckedurl' => 'quiz/removemany/--parentId--/--parentType--'
		);
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');
		$data = array();

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = '';
		$launcher = $this->module_mdl->getLauncher(69, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$quiz = $this->quiz_model->getByLauncherId($launcher->id);
		$questions = $this->quiz_model->getQuestionsOfQuiz($quiz->id);

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--quizid--'), array($quiz->id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);

		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		$import_url =& $this->_module_settings->import_url;
		$import_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $import_url);

		// $btnCategories = (object)array(
		// 				'title' => __('Edit Categories'),
		// 				'href' => '',
		// 				'icon_class' => 'icon-pencil',
		// 				'btn_class' => 'edit btn',
		// 				);
		// $this->_module_settings->extrabuttons['categories'] = $btnCategories;

		$btnReview = (object)array(
						'title' => __('Review screens'),
						'href' => 'quiz/reviewscreens/'.$quiz->id,
						'icon_class' => 'icon-pencil',
						'btn_class' => 'edit btn',
						);
		$this->_module_settings->extrabuttons['review'] = $btnReview;

		$btnReport = (object)array(
						'title' => __('Report'),
						'href' => 'quiz/report/'.$quiz->id,
						'icon_class' => 'icon-pencil',
						'btn_class' => 'edit btn',
						);
		$this->_module_settings->extrabuttons['report'] = $btnReport;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $questions), true);

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'quiz');
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		$data = array();

		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->iframeurl = '';
		$launcher = $this->module_mdl->getLauncher(69, 'venue', $venue->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$quiz = $this->quiz_model->getByLauncherId($launcher->id);
		$questions = $this->quiz_model->getQuestionsOfQuiz($quiz->id);

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--quizid--'), array($quiz->id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'venue', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'venue', $delete_href);

		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $deletecheckedurl);

		// $btnCategories = (object)array(
		// 				'title' => __('Edit Categories'),
		// 				'href' => '',
		// 				'icon_class' => 'icon-pencil',
		// 				'btn_class' => 'edit btn',
		// 				);
		// $this->_module_settings->extrabuttons['categories'] = $btnCategories;

		$btnReview = (object)array(
						'title' => __('Review screens'),
						'href' => 'quiz/reviewscreens/'.$quiz->id,
						'icon_class' => 'icon-pencil',
						'btn_class' => 'edit btn',
						);
		$this->_module_settings->extrabuttons['review'] = $btnReview;

		$btnReport = (object)array(
						'title' => __('Report'),
						'href' => 'quiz/report/'.$quiz->id,
						'icon_class' => 'icon-pencil',
						'btn_class' => 'edit btn',
						);
		$this->_module_settings->extrabuttons['report'] = $btnReport;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $questions), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']		= checkBreadcrumbsVenue($app, array($venue->name => 'venue/view/'.$id, $this->_module_settings->title => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'quiz');
		$this->load->view('master', $cdata);
	}

	function addquestion($quizid) {
		$error = "";
		$imageerror = '';

		$quiz = $this->quiz_model->getById($quizid);
		$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
		if($launcher->eventid != 0) {
			$type = 'event';
			$typeObject = $this->event_model->getbyid($launcher->eventid);
			$app = _actionAllowed($typeObject, 'event','returnApp');
		} elseif($launcher->venueid != 0) {
			$type = 'venue';
			$typeObject = $this->venue_model->getbyid($launcher->venueid);
			$app = _actionAllowed($typeObject, 'venue','returnApp');
		} elseif($launcher->appid != 0) {
			$type = 'app';
			$app = $this->app_model->getbyid($launcher->appid);
			$typeObject = $app;
			_actionAllowed($app, 'app');
		}

		$questiontypes = $this->quiz_model->getQuestionTypes($quizid);

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('quizquestiontypeid', 'Question Type', 'trim|required');
			$this->form_validation->set_rules('questiontext', 'Question Text', 'trim|required');
			$this->form_validation->set_rules('imageurl', 'imageurl', 'trim');
			$this->form_validation->set_rules('videourl', 'videourl', 'trim');
			$this->form_validation->set_rules('correctanswer', 'Correct answer', 'trim');
			$this->form_validation->set_rules('correctanswer_score', 'Correct answer score', 'trim');
			$this->form_validation->set_rules('explanationtext', 'explanationtext', 'trim');
			$this->form_validation->set_rules('explanationimage', 'explanationimage', 'trim');
			$this->form_validation->set_rules('explanationvideo', 'explanationvideo', 'trim');

			if($this->form_validation->run() == FALSE ){
				$error = validation_errors();
			} else {
				// *** SAVE TAGS *** //
				if($this->input->post('mytagsulselect') == null) {
					$tags = '';
				} else {
					$tags = $this->input->post('mytagsulselect');
					$tags = implode(',', $tags);
				}

				$correctanswer_quizquestionoptionid = 0;
				if($this->input->post('correctanswer_quizquestionoptionid') && $this->input->post('correctanswer_quizquestionoptionid') > 1) {
					$correctanswer_quizquestionoptionid = $this->input->post('correctanswer_quizquestionoptionid');
				}

				$questionid = $this->general_model->insert('quizquestion', array(
						'quizid' => $quizid,
						'quizquestiontypeid' => $this->input->post('quizquestiontypeid'),
						'questiontext' => $this->input->post('questiontext'),
						'videourl' => $this->input->post('videourl'),
						'tags' => $tags,
						'correctanswer' => $this->input->post('correctanswer'),
						'correctanswer_quizquestionoptionid' => $correctanswer_quizquestionoptionid,
						'correctanswer_score' => $this->input->post('correctanswer_score'),
						'explanationtext' => $this->input->post('explanationtext'),
						'explanationvideo' => $this->input->post('explanationvideo'),
					));

				$this->quiz_model->addOptionsToQuestion($questionid, $quiz->id);

				if(!is_dir($this->config->item('imagespath') . "upload/quizimages/question_".$questionid)){
					mkdir($this->config->item('imagespath') . "upload/quizimages/question_".$questionid, 0775,true);
				}

				$configimage['upload_path'] = $this->config->item('imagespath') .'upload/quizimages/question_'.$questionid;
				$configimage['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configimage['max_width']  = '2000';
				$configimage['max_height']  = '2000';
				$this->load->library('upload', $configimage);
				$this->upload->initialize($configimage);
				if(!$this->upload->do_upload('imageurl')) {
					$imageurl = "";
					if($_FILES['imageurl']['name'] != '') {
						$imageerror .= $this->upload->display_errors();
					}
				} else {
					$returndata = $this->upload->data();
					$imageurl = 'upload/quizimages/question_'. $questionid . '/' . $returndata['file_name'];
					$this->general_model->update('quizquestion', $questionid, array('imageurl' => $imageurl));
				}
				if(!$this->upload->do_upload('explanationimage')) {
					$explanationimage = "";
					if($_FILES['explanationimage']['name'] != '') {
						$imageerror .= $this->upload->display_errors();
					}
				} else {
					$returndata = $this->upload->data();
					$explanationimage = 'upload/quizimages/question_'. $questionid . '/' . $returndata['file_name'];
					$this->general_model->update('quizquestion', $questionid, array('explanationimage' => $explanationimage));
				}

				if(!empty($imageerror)) {
					redirect('quiz/editquestion/'.$questionid.'?error=image');
				}

				if($error == '') {
					$this->general_model->update($type, $typeObject->id, array('timestamp' => time()));
					redirect('quiz/'.$type.'/'.$typeObject->id);
				}
			}
		}


		$cdata['content'] 		= $this->load->view('c_quizquestion_add', array('typeObject' => $typeObject, 'type' => $type, 'error' => $error, 'app' => $app, 'quiz' => $quiz, 'questionoptions' => array(), 'questiontypes' => $questiontypes), TRUE);
		if($type == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $typeObject->name => "event/view/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Add question") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($typeObject->name => "event/view/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Add question") => $this->uri->uri_string());
			}
		}

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($type, $typeObject->id, $app, 'quiz');
		$this->load->view('master', $cdata);
	}

	function editquestion($questionid) {
		$error = "";
		$imageerror = '';

		$question = $this->quiz_model->getQuestionById($questionid);
		$quiz = $this->quiz_model->getById($question->quizid);
		$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
		if($launcher->eventid != 0) {
			$type = 'event';
			$typeObject = $this->event_model->getbyid($launcher->eventid);
			$app = _actionAllowed($typeObject, 'event','returnApp');
		} elseif($launcher->venueid != 0) {
			$type = 'venue';
			$typeObject = $this->venue_model->getbyid($launcher->venueid);
			$app = _actionAllowed($typeObject, 'venue','returnApp');
		} elseif($launcher->appid != 0) {
			$type = 'app';
			$app = $this->app_model->getbyid($launcher->appid);
			$typeObject = $app;
			_actionAllowed($app, 'app');
		}

		$questiontypes = $this->quiz_model->getQuestionTypes($quizid);
		$questionoptions = $this->quiz_model->getQuestionOptions($question->id);

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('quizquestiontypeid', 'Question Type', 'trim|required');
			$this->form_validation->set_rules('questiontext', 'Question Text', 'trim|required');
			$this->form_validation->set_rules('imageurl', 'imageurl', 'trim');
			$this->form_validation->set_rules('videourl', 'videourl', 'trim');
			$this->form_validation->set_rules('correctanswer', 'Correct answer', 'trim');
			$this->form_validation->set_rules('correctanswer_score', 'Correct answer score', 'trim');
			$this->form_validation->set_rules('explanationtext', 'explanationtext', 'trim');
			$this->form_validation->set_rules('explanationimage', 'explanationimage', 'trim');
			$this->form_validation->set_rules('explanationvideo', 'explanationvideo', 'trim');

			if($this->form_validation->run() == FALSE ){
				$error = validation_errors();
			} else {
				// *** SAVE TAGS *** //
				if($this->input->post('mytagsulselect') == null) {
					$tags = '';
				} else {
					$tags = $this->input->post('mytagsulselect');
					$tags = implode(',', $tags);
				}

				$correctanswer_quizquestionoptionid = 0;
				if($this->input->post('correctanswer_quizquestionoptionid') && $this->input->post('correctanswer_quizquestionoptionid') > 1) {
					$correctanswer_quizquestionoptionid = $this->input->post('correctanswer_quizquestionoptionid');
				}

				$questionid = $this->general_model->update('quizquestion', $questionid, array(
						'quizid' => $quiz->id,
						'quizquestiontypeid' => $this->input->post('quizquestiontypeid'),
						'questiontext' => $this->input->post('questiontext'),
						'videourl' => $this->input->post('videourl'),
						'tags' => $tags,
						'correctanswer' => $this->input->post('correctanswer'),
						'correctanswer_quizquestionoptionid' => $correctanswer_quizquestionoptionid,
						'correctanswer_score' => $this->input->post('correctanswer_score'),
						'explanationtext' => $this->input->post('explanationtext'),
						'explanationvideo' => $this->input->post('explanationvideo'),
					));

				if(!is_dir($this->config->item('imagespath') . "upload/quizimages/question_".$questionid)){
					mkdir($this->config->item('imagespath') . "upload/quizimages/question_".$questionid, 0775,true);
				}

				$configimage['upload_path'] = $this->config->item('imagespath') .'upload/quizimages/question_'.$questionid;
				$configimage['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configimage['max_width']  = '2000';
				$configimage['max_height']  = '2000';
				$this->load->library('upload', $configimage);
				$this->upload->initialize($configimage);
				if(!$this->upload->do_upload('imageurl')) {
					$imageurl = "";
					if($_FILES['imageurl']['name'] != '') {
						$imageerror .= $this->upload->display_errors();
					}
				} else {
					$returndata = $this->upload->data();
					$imageurl = 'upload/quizimages/question_'. $questionid . '/' . $returndata['file_name'];
					$this->general_model->update('quizquestion', $questionid, array('imageurl' => $imageurl));
				}
				if(!$this->upload->do_upload('explanationimage')) {
					$explanationimage = "";
					if($_FILES['explanationimage']['name'] != '') {
						$imageerror .= $this->upload->display_errors();
					}
				} else {
					$returndata = $this->upload->data();
					$explanationimage = 'upload/quizimages/question_'. $questionid . '/' . $returndata['file_name'];
					$this->general_model->update('quizquestion', $questionid, array('explanationimage' => $explanationimage));
				}

				if(!empty($imageerror)) {
					redirect('quiz/editquestion/'.$questionid.'?error=image');
				}

				if($error == '') {
					$this->general_model->update($type, $typeObject->id, array('timestamp' => time()));
					redirect('quiz/'.$type.'/'.$typeObject->id);
				}
			}
		}


		$cdata['content'] 		= $this->load->view('c_quizquestion_edit', array('typeObject' => $typeObject, 'type' => $type, 'error' => $error, 'app' => $app, 'questiontypes' => $questiontypes, 'question' => $question, 'questionoptions' => $questionoptions), TRUE);
		if($type == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $typeObject->name => "event/view/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Edit question") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($typeObject->name => "event/view/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Edit question") => $this->uri->uri_string());
			}
		}

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($type, $typeObject->id, $app, 'quiz');
		$this->load->view('master', $cdata);
	}

	function deletequestion($id) {
		$question = $this->quiz_model->getQuestionById($id);
		$quiz = $this->quiz_model->getById($question->quizid);
		$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
		if($launcher->eventid != 0) {
			$type = 'event';
			$typeObject = $this->event_model->getbyid($launcher->eventid);
			$app = _actionAllowed($typeObject, 'event','returnApp');
		} elseif($launcher->venueid != 0) {
			$type = 'venue';
			$typeObject = $this->venue_model->getbyid($launcher->venueid);
			$app = _actionAllowed($typeObject, 'venue','returnApp');
		} elseif($launcher->appid != 0) {
			$type = 'app';
			$app = $this->app_model->getbyid($launcher->appid);
			$typeObject = $app;
			_actionAllowed($app, 'app');
		}

		if($this->general_model->remove('quizquestion', $id)){
			$this->session->set_flashdata('event_feedback', __('The question has successfully been deleted'));
			$this->general_model->update($type, $typeObject->id, array('timestamp' => time()));
			redirect('quiz/'.$type.'/'.$typeObject->id);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('quiz/'.$type.'/'.$typeObject->id);
		}
	}

	function addquestionoption($questionid) {
		if(isset($_POST['optiontext'])) {
			$question = $this->quiz_model->getQuestionById($questionid);
			$quiz = $this->quiz_model->getById($question->quizid);
			$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
			if($launcher->eventid != 0) {
				$type = 'event';
				$typeObject = $this->event_model->getbyid($launcher->eventid);
				$app = _actionAllowed($typeObject, 'event','returnApp');
			} elseif($launcher->venueid != 0) {
				$type = 'venue';
				$typeObject = $this->venue_model->getbyid($launcher->venueid);
				$app = _actionAllowed($typeObject, 'venue','returnApp');
			} elseif($launcher->appid != 0) {
				$type = 'app';
				$app = $this->app_model->getbyid($launcher->appid);
				$typeObject = $app;
				_actionAllowed($app, 'app');
			}

			$questionoptionid = $this->general_model->insert('quizquestionoption', array(
					'quizid' => $quiz->id,
					'quizquestionid' => $questionid,
					'optiontext' => $_POST['optiontext']
				));

			echo $questionoptionid;
		}
	}

	function addquestionoptiontoempty($quizid) {
		if(isset($_POST['optiontext'])) {
			$quiz = $this->quiz_model->getById($quizid);
			$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
			if($launcher->eventid != 0) {
				$type = 'event';
				$typeObject = $this->event_model->getbyid($launcher->eventid);
				$app = _actionAllowed($typeObject, 'event','returnApp');
			} elseif($launcher->venueid != 0) {
				$type = 'venue';
				$typeObject = $this->venue_model->getbyid($launcher->venueid);
				$app = _actionAllowed($typeObject, 'venue','returnApp');
			} elseif($launcher->appid != 0) {
				$type = 'app';
				$app = $this->app_model->getbyid($launcher->appid);
				$typeObject = $app;
				_actionAllowed($app, 'app');
			}

			$questionoptionid = $this->general_model->insert('quizquestionoption', array(
					'quizid' => $quiz->id,
					'quizquestionid' => 0,
					'optiontext' => $_POST['optiontext']
				));

			echo $questionoptionid;
		}
	}

	function removequestionoption($optionid) {
		$option = $this->quiz_model->getQuestionOptionById($optionid);
		if($option->quizquestionid == 0) {
			$quiz = $this->quiz_model->getById($option->quizid);
		} else {
			$question = $this->quiz_model->getQuestionById($option->quizquestionid);
			$quiz = $this->quiz_model->getById($question->quizid);
		}
		
		$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
		if($launcher->eventid != 0) {
			$type = 'event';
			$typeObject = $this->event_model->getbyid($launcher->eventid);
			$app = _actionAllowed($typeObject, 'event','returnApp');
		} elseif($launcher->venueid != 0) {
			$type = 'venue';
			$typeObject = $this->venue_model->getbyid($launcher->venueid);
			$app = _actionAllowed($typeObject, 'venue','returnApp');
		} elseif($launcher->appid != 0) {
			$type = 'app';
			$app = $this->app_model->getbyid($launcher->appid);
			$typeObject = $app;
			_actionAllowed($app, 'app');
		}

		if($this->general_model->remove('quizquestionoption', $optionid)) {
			echo $optionid;
		} else {
			echo false;
		}
	}

	function reviewscreens($quizid) {
		$quiz = $this->quiz_model->getById($quizid);
		$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
		if($launcher->eventid != 0) {
			$type = 'event';
			$typeObject = $this->event_model->getbyid($launcher->eventid);
			$app = _actionAllowed($typeObject, 'event','returnApp');
		} elseif($launcher->venueid != 0) {
			$type = 'venue';
			$typeObject = $this->venue_model->getbyid($launcher->venueid);
			$app = _actionAllowed($typeObject, 'venue','returnApp');
		} elseif($launcher->appid != 0) {
			$type = 'app';
			$app = $this->app_model->getbyid($launcher->appid);
			$typeObject = $app;
			_actionAllowed($app, 'app');
		}

		$title = 'Quiz Reviews';
		$this->_module_settings->title = $title;

		$reviews = $this->quiz_model->getReviewsOfQuiz($quiz->id);

		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = site_url('quiz/addreviewscreen/'.$quiz->id);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = site_url('quiz/editreviewscreen/--id--');
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = site_url('quiz/deletereviewscreen/--id--');

		$this->_module_settings->headers = array(
				'Review' => 'title'
			);


		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $reviews), true);

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $typeObject), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $typeObject->name => $type."/view/".$id, "Quiz" => "quiz/event/".$typeObject->id, $title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($typeObject->name => $type."/view/".$id, "Quiz" => "quiz/event/".$typeObject->id, $title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($type, $typeObject->id, $app, 'quiz');
		$this->load->view('master', $cdata);
	}

	function addreviewscreen($quizid) {
		$error = "";
		$imageerror = '';

		$quiz = $this->quiz_model->getById($quizid);
		$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
		if($launcher->eventid != 0) {
			$type = 'event';
			$typeObject = $this->event_model->getbyid($launcher->eventid);
			$app = _actionAllowed($typeObject, 'event','returnApp');
		} elseif($launcher->venueid != 0) {
			$type = 'venue';
			$typeObject = $this->venue_model->getbyid($launcher->venueid);
			$app = _actionAllowed($typeObject, 'venue','returnApp');
		} elseif($launcher->appid != 0) {
			$type = 'app';
			$app = $this->app_model->getbyid($launcher->appid);
			$typeObject = $app;
			_actionAllowed($app, 'app');
		}

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('minimum_score', 'Minimum score', 'trim|required');
			$this->form_validation->set_rules('maximum_score', 'Maximum score', 'trim|required');
			$this->form_validation->set_rules('reviewtext', 'Review text', 'trim');
			$this->form_validation->set_rules('reviewimage', 'Review Image', 'trim');
			$this->form_validation->set_rules('reviewvideo', 'Review video', 'trim');

			if($this->form_validation->run() == FALSE ){
				$error = validation_errors();
			} else {
				$reviewid = $this->general_model->insert('quizreview', array(
						'quizid' => $quizid,
						'minimum_score' => $this->input->post('minimum_score'),
						'maximum_score' => $this->input->post('maximum_score'),
						'reviewtext' => $this->input->post('reviewtext'),
						'reviewvideo' => $this->input->post('reviewvideo')
					));

				if(!is_dir($this->config->item('imagespath') . "upload/quizimages/review_".$reviewid)){
					mkdir($this->config->item('imagespath') . "upload/quizimages/review_".$reviewid, 0775,true);
				}

				$configimage['upload_path'] = $this->config->item('imagespath') .'upload/quizimages/review_'.$reviewid;
				$configimage['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configimage['max_width']  = '2000';
				$configimage['max_height']  = '2000';
				$this->load->library('upload', $configimage);
				$this->upload->initialize($configimage);
				if(!$this->upload->do_upload('reviewimage')) {
					$reviewimage = "";
					if($_FILES['reviewimage']['name'] != '') {
						$imageerror .= $this->upload->display_errors();
					}
				} else {
					$returndata = $this->upload->data();
					$reviewimage = 'upload/quizimages/review_'. $reviewid . '/' . $returndata['file_name'];
					$this->general_model->update('quizreview', $reviewid, array('reviewimage' => $reviewimage));
				}

				if(!empty($imageerror)) {
					redirect('quiz/editreviewscreen/'.$reviewid.'?error=image');
				}

				if($error == '') {
					$this->general_model->update($type, $typeObject->id, array('timestamp' => time()));
					redirect('quiz/reviewscreens/'.$quiz->id);
				}
			}
		}


		$cdata['content'] 		= $this->load->view('c_quizreview_add', array('typeObject' => $typeObject, 'type' => $type, 'error' => $error, 'app' => $app, 'quiz' => $quiz), TRUE);
		if($type == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $typeObject->name => "event/view/".$typeObject->id, "Quiz" => "quiz/event/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Add review") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($typeObject->name => "event/view/".$typeObject->id, "Quiz" => "quiz/event/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Add review") => $this->uri->uri_string());
			}
		}

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($type, $typeObject->id, $app, 'quiz');
		$this->load->view('master', $cdata);
	}

	function editreviewscreen($reviewid) {
		$error = "";
		$imageerror = '';

		$review = $this->quiz_model->getReviewById($reviewid);
		$quiz = $this->quiz_model->getById($review->quizid);
		$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
		if($launcher->eventid != 0) {
			$type = 'event';
			$typeObject = $this->event_model->getbyid($launcher->eventid);
			$app = _actionAllowed($typeObject, 'event','returnApp');
		} elseif($launcher->venueid != 0) {
			$type = 'venue';
			$typeObject = $this->venue_model->getbyid($launcher->venueid);
			$app = _actionAllowed($typeObject, 'venue','returnApp');
		} elseif($launcher->appid != 0) {
			$type = 'app';
			$app = $this->app_model->getbyid($launcher->appid);
			$typeObject = $app;
			_actionAllowed($app, 'app');
		}

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('minimum_score', 'Minimum score', 'trim|required');
			$this->form_validation->set_rules('maximum_score', 'Maximum score', 'trim|required');
			$this->form_validation->set_rules('reviewtext', 'Review text', 'trim');
			$this->form_validation->set_rules('reviewimage', 'Review Image', 'trim');
			$this->form_validation->set_rules('reviewvideo', 'Review video', 'trim');

			if($this->form_validation->run() == FALSE ){
				$error = validation_errors();
			} else {
				$this->general_model->update('quizreview', $review->id, array(
						'minimum_score' => $this->input->post('minimum_score'),
						'maximum_score' => $this->input->post('maximum_score'),
						'reviewtext' => $this->input->post('reviewtext'),
						'reviewvideo' => $this->input->post('reviewvideo')
					));

				if(!is_dir($this->config->item('imagespath') . "upload/quizimages/review_".$reviewid)){
					mkdir($this->config->item('imagespath') . "upload/quizimages/review_".$reviewid, 0775,true);
				}

				$configimage['upload_path'] = $this->config->item('imagespath') .'upload/quizimages/review_'.$reviewid;
				$configimage['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configimage['max_width']  = '2000';
				$configimage['max_height']  = '2000';
				$this->load->library('upload', $configimage);
				$this->upload->initialize($configimage);
				if(!$this->upload->do_upload('reviewimage')) {
					$reviewimage = "";
					if($_FILES['reviewimage']['name'] != '') {
						$imageerror .= $this->upload->display_errors();
					}
				} else {
					$returndata = $this->upload->data();
					$reviewimage = 'upload/quizimages/review_'. $reviewid . '/' . $returndata['file_name'];
					$this->general_model->update('quizreview', $reviewid, array('reviewimage' => $reviewimage));
				}

				if(!empty($imageerror)) {
					redirect('quiz/editreviewscreen/'.$reviewid.'?error=image');
				}

				if($error == '') {
					$this->general_model->update($type, $typeObject->id, array('timestamp' => time()));
					redirect('quiz/reviewscreens/'.$quiz->id);
				}
			}
		}


		$cdata['content'] 		= $this->load->view('c_quizreview_edit', array('typeObject' => $typeObject, 'type' => $type, 'error' => $error, 'app' => $app, 'quiz' => $quiz, 'review' => $review), TRUE);
		if($type == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $typeObject->name => "event/view/".$typeObject->id, "Quiz" => "quiz/event/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Edit review") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($typeObject->name => "event/view/".$typeObject->id, "Quiz" => "quiz/event/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Edit review") => $this->uri->uri_string());
			}
		}

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($type, $typeObject->id, $app, 'quiz');
		$this->load->view('master', $cdata);
	}

	function deletereviewscreen($reviewid) {
		$review = $this->quiz_model->getReviewById($reviewid);
		$quiz = $this->quiz_model->getById($review->quizid);
		$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
		if($launcher->eventid != 0) {
			$type = 'event';
			$typeObject = $this->event_model->getbyid($launcher->eventid);
			$app = _actionAllowed($typeObject, 'event','returnApp');
		} elseif($launcher->venueid != 0) {
			$type = 'venue';
			$typeObject = $this->venue_model->getbyid($launcher->venueid);
			$app = _actionAllowed($typeObject, 'venue','returnApp');
		} elseif($launcher->appid != 0) {
			$type = 'app';
			$app = $this->app_model->getbyid($launcher->appid);
			$typeObject = $app;
			_actionAllowed($app, 'app');
		}

		if($this->general_model->remove('quizreview', $reviewid)){
			$this->session->set_flashdata('event_feedback', __('The review has successfully been deleted'));
			$this->general_model->update($type, $typeObject->id, array('timestamp' => time()));
			redirect('quiz/reviewscreens/'.$review->quizid);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('quiz/reviewscreens/'.$review->quizid);
		}
	}

	function report($quizid) {
		$quiz = $this->quiz_model->getById($quizid);
		$launcher = $this->module_mdl->getLauncherById($quiz->launcherid);
		if($launcher->eventid != 0) {
			$type = 'event';
			$typeObject = $this->event_model->getbyid($launcher->eventid);
			$app = _actionAllowed($typeObject, 'event','returnApp');
		} elseif($launcher->venueid != 0) {
			$type = 'venue';
			$typeObject = $this->venue_model->getbyid($launcher->venueid);
			$app = _actionAllowed($typeObject, 'venue','returnApp');
		} elseif($launcher->appid != 0) {
			$type = 'app';
			$app = $this->app_model->getbyid($launcher->appid);
			$typeObject = $app;
			_actionAllowed($app, 'app');
		}

		$quizsubmissions = $this->quiz_model->getSubmissionsOfQuiz($quiz->id);

		$cdata['content'] 		= $this->load->view('c_quizreport', array('typeObject' => $typeObject, 'type' => $type, 'error' => $error, 'app' => $app, 'submissions' => $quizsubmissions), TRUE);
		if($type == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $typeObject->name => "event/view/".$typeObject->id, "Quiz" => "quiz/event/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Report") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($typeObject->name => "event/view/".$typeObject->id, "Quiz" => "quiz/event/".$typeObject->id, $this->_module_settings->title => "quiz/event/".$typeObject->id, __("Report") => $this->uri->uri_string());
			}
		}

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($type, $typeObject->id, $app, 'quiz');
		$this->load->view('master', $cdata);
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->quiz_model->removeMany($selectedids, 'quizquestion');
    }    

}