<?php

class jWebservice extends CI_Controller {
	//General Section
	//Methods made for the xml webservice
	function jWebservice() {
		parent::__construct();
	}

	function index() {
		header ("content-type: text/xml");
		echo'<?xml version="1.0" encoding="UTF-8"?>';
		echo '<expoplus version="1.0">';
		echo '<error id="404">'.__('404 page not found').'</error>';
		echo '<errortext>'.__('page not found.').'</errortext>';
		echo '</expoplus>';
	}

	function getAppInfo($appid = 0, $token = 0) {
		$url = 'http://api.tapcrowd.com/jwebservice/getAppInfo/'.$appid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function getEventTS($eventid = 0) {
		$url = 'http://api.tapcrowd.com/jwebservice/getEventTS/'.$eventid;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);

	}
	
	function log($params = array()) {
		$myFile = "logjwebservice.txt";
		if(!fopen($myFile, 'a')) {
			fopen($myFile, 'w');
		} 
		$fh = fopen($myFile, 'a');
		$stringData = date('m-d-Y', time());
		foreach($params as $param) {
			$stringData .= ' ' . $param . ' ';
		}
		$stringData .= "\n";
		fwrite($fh, $stringData);

		fclose($fh);
	}

	function getEventDetails($eventid = 0, $token = 0, $lang = '') {
		$url = 'http://api.tapcrowd.com/jwebservice/getEventDetails/'.$eventid.'/'.$token.'/'.$lang;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function getExhibitorDetails($exhiid) {
		if(isset($exhiid) && $exhiid != null && $exhiid != false) {
			$url = 'http://api.tapcrowd.com/jwebservice/getExhibitorDetails/'.$exhiid;
			//open connection
			$ch = curl_init();
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$result = curl_exec($ch);
			
			if($result === false) {
				echo __('Gateway error: ') . curl_error($ch);
			} else {
				// Operation completed without any errors
				header('Content-type: application/json');
				echo $result;
				// echo $result;
			}
			
			curl_close($ch);
		}
	}

	function getEventList($appid = 0, $token = 0) {
		$url = 'http://api.tapcrowd.com/jwebservice/getEventList/'.$appid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}
	
	function getEventListVenue($venueid = 0, $token = 0) {
		$url = 'http://api.tapcrowd.com/jwebservice/getEventListVenue/'.$venueid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function getEventListDev($appid = 0, $token = 0) {
		$url = 'http://api.tapcrowd.com/jwebservice/getEventListDev/'.$appid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function getVenueList($appid = 0, $token = 0) {
		$url = 'http://api.tapcrowd.com/jwebservice/getVenueList/'.$appid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function getVenueDetails($venueid = 0, $token = 0, $catalogselection = '') {
		$url = 'http://api.tapcrowd.com/jwebservice/getVenueDetails/'.$venueid.'/'.$token.'/'.$catalogselection;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function getToken($appid) {
		$url = 'http://api.tapcrowd.com/jwebservice/getToken/'.$appid;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function getTCList($token) {
		$url = 'http://api.tapcrowd.com/jwebservice/getTCList/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function pushFavourite() {
		$this->log(array('pushFavourite'));

		// $key, $email, $eventid, $exhibitorid, $sessionid
		$key = $this->input->post('key');
		$email = $this->input->post('email');
		$eventid = $this->input->post('eventid');
		$exhibitorid = $this->input->post('exhibitorid');
		$sessionid = $this->input->post('sessionid');
		$extra = $this->input->post('extra');

		if($key != "" && $email != '' && $eventid != ""){
			// Secret string opbouwen
			$secret = md5("tcadm" . $eventid);

			// Secret vergelijken met meegestuurde
			if($key == $secret) {

				// Insert favourite
				$userdata = array(
						'useremail' 	=> $email,
						'eventid' 		=> $eventid,
						'exhibitorid' 	=> $exhibitorid,
						'sessionid' 	=> $sessionid, 
						'extra'			=> $extra
					);

				if($this->db->insert('favorites', $userdata)){
					echo __("Accept, data has successfully been saved!");
				} else {
					echo __("Failure, data has not been saved!");
				}
			} else {
				echo __("Invalid token, nothing found!");
			}
		} else {
			echo __("Invalid token, nothing found!");
		}

	}

	function deleteFavourite() {
		$this->log(array('deleteFavourite'));
		// $key, $email, $eventid, $exhibitorid, $sessionid
		$key = mysql_escape_string($this->input->post('key'));
		$email = mysql_escape_string($this->input->post('email'));
		$eventid = mysql_escape_string($this->input->post('eventid'));
		$exhibitorid = mysql_escape_string($this->input->post('exhibitorid'));
		$sessionid = mysql_escape_string($this->input->post('sessionid'));
		$extra = mysql_escape_string($this->input->post('extra'));
		
		if($extra != '' && $email != '' && $eventid !='') {
			// Secret string opbouwen
			$secret = md5("tcadm" . $eventid);

			// Secret vergelijken met meegestuurde
			if($key == $secret) {

				// Insert favourite
				$userdata = array(
						'useremail' 	=> $email,
						'eventid' 		=> $eventid,
						'extra'			=> $extra
					);

				if($this->db->delete('favorites', $userdata)){
					echo __("Accept, data has successfully been deleted!");
				} else {
					echo __("Failure, data has not been deleted!");
				}
			} else {
				echo __("Invalid token, nothing found!");
			}
		} elseif($key != "" && $email != '' && $eventid != ""){
			// Secret string opbouwen
			$secret = md5("tcadm" . $eventid);

			// Secret vergelijken met meegestuurde
			if($key == $secret) {

				// Insert favourite
				$userdata = array(
						'useremail' 	=> $email,
						'eventid' 		=> $eventid,
						'exhibitorid' 	=> $exhibitorid,
						'sessionid' 	=> $sessionid
					);

				if($this->db->delete('favorites', $userdata)){
					echo __("Accept, data has successfully been deleted!");
				} else {
					echo __("Failure, data has not been deleted!");
				}
			} else {
				echo __("Invalid token, nothing found!");
			}
		} else {
			echo __("Invalid token, nothing found!");
		}

	}

	function activatePush() {
		$this->log(array('activatePush'));
		// $key, $phoneid, $appid
		$key = mysql_escape_string($this->input->post('key'));
		$phoneid = mysql_escape_string($this->input->post('phoneid'));
		$appid = mysql_escape_string($this->input->post('appid'));

		if($key != "" && $phoneid != '' && $appid != ""){
			$phoneid = str_replace(" ","",substr(substr($phoneid, 0, -1), 1));

			// Secret string opbouwen
			$secret = md5("tcadm" . $appid);

			// Secret vergelijken met meegestuurde
			if($key == $secret) {

				// Insert favourite
				$userdata = array(
						'appid' 	=> $appid,
						'token' 		=> $phoneid
					);
				if($this->db->get_where('push', array('appid' => $appid, 'token' => $phoneid))->num_rows() == 0){
					if($this->db->insert('push', $userdata)){
						echo "OK";
					} else {
						echo __("Error");
					}
				} else {
					echo "OK";
				}
			} else {
				echo __("Error");
			}
		} else {
			echo __("Error");
		}

	}

	function pushAuthentication() {
		$this->log(array('pushAuthentication'));
		// $key, $phoneid, $appid
		$key = mysql_escape_string($this->input->post('key'));
		$phoneid = mysql_escape_string($this->input->post('phoneid'));
		$appid = mysql_escape_string($this->input->post('appid'));

		if($key != "" && $phoneid != '' && $appid != ""){

			// Secret string opbouwen
			$secret = md5("tcadm" . $appid);

			// Secret vergelijken met meegestuurde
			if($key == $secret) {

				// Insert favourite
				$userdata = array(
						'appid' 	=> $appid,
						'phoneid' 	=> $phoneid
					);
				if($this->db->insert('authstats', $userdata)){
					echo "OK";
				} else {
					echo __("Error");
				}
			} else {
				echo __("Error");
			}
		} else {
			echo __("Error");
		}

	}

	function checkSubdomain($domain = "", $token = "") {
		$url = 'http://api.tapcrowd.com/jwebservice/checkSubdomain/'.$domain.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function checkCName($cname = "", $token = "") {
		$url = 'http://api.tapcrowd.com/jwebservice/checkCName/'.$cname.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

	function checkStores($appid, $token = "") {
		$url = 'http://api.tapcrowd.com/jwebservice/checkStores/'.$appid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);

	}

	function insertScore() {
		$this->log(array('insertScore'));
		// $key, $email, $eventid, $exhibitorid, $sessionid
		$key = mysql_escape_string($this->input->post('key'));
		$facebookid = mysql_escape_string($this->input->post('facebookid'));
		$appid = mysql_escape_string($this->input->post('appid'));
		$score = mysql_escape_string($this->input->post('score'));
		$photos = mysql_escape_string($this->input->post('photos'));
		$comments = mysql_escape_string($this->input->post('comments'));

		if($key != "" && $facebookid != '' && $appid != "" && $score != ''){
			// Secret string opbouwen
			$secret = md5("tcadm" . $facebookid);

			// Secret vergelijken met meegestuurde
			if($key == $secret) {

				$userdata = array(
						'facebookid' 	=> $facebookid,
						'appid' 		=> $appid,
						'score' 	=> $score,
						'photos' 	=> $photos,
						'comments' 	=> $comments
					);

				if($this->db->insert('socialscore', $userdata)){
					echo "OK";
				} else {
					echo __("ERROR");
				}
			} else {
				echo __("ERROR");
			}
		} else {
			echo __("ERROR");
		}

	}

	function updateScore() {
		$this->log(array('updateScore'));
		// $key, $email, $eventid, $exhibitorid, $sessionid
		$key = mysql_escape_string($this->input->post('key'));
		$facebookid = mysql_escape_string($this->input->post('facebookid'));
		$appid = mysql_escape_string($this->input->post('appid'));
		$score = mysql_escape_string($this->input->post('score'));
		$photos = mysql_escape_string($this->input->post('photos'));
		$comments = mysql_escape_string($this->input->post('comments'));

		if($key != "" && $facebookid != '' && $appid != "" && $score != ''){
			// Secret string opbouwen
			$secret = md5("tcadm" . $facebookid);

			// Secret vergelijken met meegestuurde
			if($key == $secret) {

				$userdata = array(
						'score' 	=> $score,
						'photos' 	=> $photos,
						'comments' 	=> $comments
					);

				$this->db->where('facebookid', $facebookid);
				$this->db->where('appid', $appid);
				if($this->db->update('socialscore', $userdata)){
					echo "OK";
				} else {
					echo __("ERROR");
				}
			} else {
				echo __("ERROR");
			}
		} else {
			echo __("ERROR");
		}

	}

	function getSocialRanking() {
		$this->log(array('getSocialRanking'));
		$data['festival_top'] = null;
		$data['user_top'] = null;

		// RANKING BY APPID
		$appid = $this->input->post('appid');
		if ($appid != '') {
			$byapp = $this->db->query("SELECT * FROM socialscore WHERE appid = $appid ORDER BY score DESC LIMIT 10");
			if ($byapp->num_rows() > 0) $data['festival_top'] = $byapp->result();
		}

		// RANKING BY FACEBOOKID
		$byuser = $this->db->query("SELECT facebookid, SUM(score) as total_score, SUM(photos) as total_photos, SUM(comments) as total_comments FROM socialscore GROUP BY facebookid ORDER BY score DESC LIMIT 10");
		if ($byuser->num_rows() > 0) $data['user_top'] = $byuser->result();

		header('Content-type: application/json');
		echo json_encode($data);

	}

	function cleanForShortURL($toClean) {
		$argo = array(
			'?'=>'S', '?'=>'s', '?'=>'Dj','?'=>'Z', '?'=>'z', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬'=>'A', 'ÃƒÆ’Ã†â€™ÃƒÂ¯Ã‚Â¿Ã‚Â½'=>'A', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡'=>'A', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢'=>'A', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾'=>'A',
			'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦'=>'A', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â '=>'A', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¡'=>'C', 'ÃƒÆ’Ã†â€™Ãƒâ€¹Ã¢â‚¬Â '=>'E', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â°'=>'E', 'ÃƒÆ’Ã†â€™Ãƒâ€¦Ã‚Â '=>'E', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹'=>'E', 'ÃƒÆ’Ã†â€™Ãƒâ€¦Ã¢â‚¬â„¢'=>'I', 'ÃƒÆ’Ã†â€™ÃƒÂ¯Ã‚Â¿Ã‚Â½'=>'I', 'ÃƒÆ’Ã†â€™Ãƒâ€¦Ã‚Â½'=>'I',
			'ÃƒÆ’Ã†â€™ÃƒÂ¯Ã‚Â¿Ã‚Â½'=>'I', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‹Å“'=>'N', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢'=>'O', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ'=>'O', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã¯Â¿Â½'=>'O', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¢'=>'O', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã¢â‚¬Å“'=>'O', 'ÃƒÆ’Ã†â€™Ãƒâ€¹Ã…â€œ'=>'O', 'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢'=>'U', 'ÃƒÆ’Ã†â€™Ãƒâ€¦Ã‚Â¡'=>'U',
			'ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Âº'=>'U', 'ÃƒÆ’Ã†â€™Ãƒâ€¦Ã¢â‚¬Å“'=>'U', '?'=>'Y', '?'=>'B', 'ÃƒÆ’Ã†â€™Ãƒâ€¦Ã‚Â¸'=>'Ss','ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â '=>'a', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¡'=>'a', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢'=>'a', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â£'=>'a', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¤'=>'a',
			'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¥'=>'a', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¦'=>'a', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â§'=>'c', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¨'=>'e', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â©'=>'e', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Âª'=>'e', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â«'=>'e', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¬'=>'i', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â­'=>'i', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â®'=>'i',
			'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯'=>'i', '?'=>'o', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±'=>'n', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â²'=>'o', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â³'=>'o', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â´'=>'o', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Âµ'=>'o', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¶'=>'o', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¸'=>'o', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¹'=>'u',
			'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Âº'=>'u', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â»'=>'u', '?'=>'y', '?'=>'y', '?'=>'b', 'ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¿'=>'y', 'ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢'=>'f'
		);
		$toClean     =     str_replace('&', '-and-', $toClean);
		$toClean     =    trim(preg_replace('/[^\w\d_ -]/si', '', $toClean));//remove all illegal chars
		$toClean     =     str_replace(' ', '-', $toClean);
		$toClean     =     str_replace('--', '-', $toClean);

		return strtr($toClean, $argo);
	}

    //get votes of a session (vb. man van de match essevee)
    function updateVotesOfSession() {
		$this->log(array('updateVotesOfSession'));
        $key = mysql_escape_string($this->input->post('key'));
        $sessionid = mysql_escape_string($this->input->post('sessionid'));
        if($key != "" && $sessionid != "") {
			// Secret string opbouwen
			$secret = md5("tcadm" . $sessionid);

			// Secret vergelijken met meegestuurde
			if($key == $secret) {

                $session = $this->db->get_where('session', array('id' => $sessionid))->row();
                $votes = (int)$session->votes + 1;

                $data = array(
                    'votes' => $votes
                );

                $this->db->where('id', $sessionid);
				if($this->db->update('session', $data)){
					echo __("Accept, data has successfully been saved!");
				} else {
					echo __("Failure, data has not been saved!");
				}
            } else {
				echo __("Invalid token, nothing found!");
			}
        } else {
			echo __("Invalid token, nothing found!");
		}
    }

    function getArtistDetails($artistid = 0, $token = 0) {
		$url = 'http://api.tapcrowd.com/jwebservice/getArtistDetails/'.$artistid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
    }

	function getArtistList($appid = 0, $token = 0) {
		$url = 'http://api.tapcrowd.com/jwebservice/getArtistList/'.$appid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

    function getVotesOfSession() {
		$this->log(array('getVotesOfSession'));
        $key = mysql_escape_string($this->input->post('key'));
        $sessionid = mysql_escape_string($this->input->post('sessionid'));
        if($key == '123456' && $sessionid != "") {
            $this->load->model("sessions_model");
            $session = $this->sessions_model->getSessionByID($sessionid);
			header('Content-type: application/json');
			echo json_encode($session->votes);
        } else {
            echo __("ERROR");
        }

    }

    function getTotalVotesOfEvent() {
		$this->log(array('getTotalVotesOfEvent'));
        $key = $this->input->post('key');
        $eventid = $this->input->post('eventid');
        if($key == '123456' && $eventid != "") {
            $this->load->model("sessions_model");
            $votes = $this->sessions_model->getVotesOfEvent($eventid);
			header('Content-type: application/json');
			echo json_encode($votes);
        } else {
            echo __("ERROR");
        }
    }
	
	function isMainSession() {
		$this->log(array('isMainSession'));
        $key = mysql_escape_string($this->input->post('key'));
        $sessionid = mysql_escape_string($this->input->post('sessionid'));
        if($key == '123456' && $sessionid != "") {
			$tags = $this->db->query("SELECT tag FROM tag WHERE tag = $sessionid");
			if($tags->num_rows() == 0) {
				$result = false;
			} else {
				$result = true;
			}
			header('Content-type: application/json');
			echo json_encode($result);
        } else {
            echo __("ERROR");
        }
	}
	
	function getSponsorDetails() {
		$this->log(array('getSponsorDetails'));
		$id = mysql_escape_string($this->input->post('id'));
		$key = mysql_escape_string($this->input->post('key'));
		$this->load->model('sponsor_model');
		if($key == '123456' && $id != "") {
			$sponsor = $this->sponsor_model->getSponsorById($id);
			header('Content-type: application/json');
			echo json_encode($sponsor);
		} else {
            echo __("ERROR");
		}
	}
	
	
	
function getEventDetailsBoekenbeurs($eventid = 0, $token = 0, $lang = '') {
		$url = 'http://api.tapcrowd.com/jwebservice/getEventDetailsBoekenbeurs/'.$eventid.'/'.$token.'/'.$lang;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/json');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */