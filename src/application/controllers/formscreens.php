<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Formscreens extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
			$this->load->model('forms_model');
		$this->load->model('artist_model');
	}
	
	//php 4 constructor
	function Forms() {
		parent::__construct();
		if(_authed()) { }
			$this->load->model('forms_model');
	}
	
	function index() {
		$this->form();
	}
	
	function form($id) {
		if($id == FALSE || $id == 0) redirect('apps');
		$form = $this->forms_model->getFormByID($id);
		$formscreens = $this->forms_model->getFormScreensByFormID($id);
		if($form->singlescreen == 1) {
			redirect('formfields/formscreen/'.$formscreens[0]->id);
		}
		$masterPageTitle = 'Screens in form '.$form->title;
		$launcher = $this->module_mdl->getLauncherById($form->launcherid);
		
		$settingsBtnUrl = 'forms/editlauncher/'.$form->launcherid.'/';
		$reportBtnUrl	= 'forms/export/'.$id;
		$deleteBtnUrl	= 'forms/delete/'.$id.'/';
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$this->iframeurl = 'forms/event/'.$eventid.'/'.$form->launcherid;
			$settingsBtnUrl .= 'event/'.$eventid;
			$deleteBtnUrl .= 'event';
			
		}else if($form->venueid){
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$this->iframeurl = 'forms/venue/'.$venueid.'/'.$form->launcherid;
			$settingsBtnUrl .= 'venue/'.$venueid;
			$deleteBtnUrl .= 'venue';
			
		}else{
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
			$this->iframeurl = 'forms/app/'.$appid.'/'.$form->launcherid;
			$settingsBtnUrl .= 'app';
		}
		
		//--- Setting Form Buttons --- ///
		$formSettingBtns = '<a class="btn" href="'.base_url($settingsBtnUrl).'">
								<img width="16" height="16" alt="TapCrowd App" src="img/Settings.png">
								Form Settings
							</a>
							<a class="btn" href="'.site_url('forms/results/'.$form->id).'" id="viewresults">
								<img width="16" height="16" alt="TapCrowd App" src="img/icons/application_cascade.png">
								View Results
							</a>';
							
		
		if(!empty($formscreens)) {
			$nextformscreen = $this->forms_model->getTopFormScreenByFormID($form->id,0);
			if($nextformscreen) {
				$this->iframeurl = 'formfields/formscreen/'.$nextformscreen->id;
			} else {
				$this->iframeurl = 'formscreens/notfound/'.$form->id;
			}
			
		}
		
		$childcon = "formfields";
		$confunc = "formscreen";
		$headers = array('Title' => 'title', 'Order' => 'order');
		if($launcher->moduletypeid == 62) {
			foreach($formscreens as $s) {
				if($s->visible == 0) {
					$s->startlink = '<a href="'.site_url('formscreens/screenvisible/'.$s->id.'/'.$s->visible).'" style="background-image: url(img/btn-onoff-22.png);background-repeat: no-repeat;height: 28px;background-position: -64px 0;float: left !important;width: 64px;"></a>';
				} else {
					$s->startlink = '<a href="'.site_url('formscreens/screenvisible/'.$s->id.'/'.$s->visible).'" style="background-image: url(img/btn-onoff-22.png);background-repeat: no-repeat;height: 28px;background-position: 0px 0;float: left !important;width: 64px;"></a>';
				}
				$s->resulturl = '<a href="'.site_url('formscreens/result/'.$s->id).'">'.__('View Results').'</a>';
			}
			$headers = array('Title' => 'title', 'Order' => 'order', 'Action' => 'startlink', 'Results' => 'resulturl');
		}
		
		if($type == 'venue'){
			$cdata['content'] 		= $this->load->view('c_listview', array('form' => $form, 'data' => $formscreens, 'headers' => $headers, 'childcon' => $childcon, 'confunc' => $confunc, 'masterPageTitle' => $masterPageTitle, 'formSettingBtns' => $formSettingBtns, 'launcherid' => $form->launcherid, 'venue' => $venue), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $form->title => "formscreens/form/".$form->id,__("Screens") => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
		}else if($type == 'event'){
			$cdata['content'] 		= $this->load->view('c_listview', array('form' => $form, 'data' => $formscreens, 'headers' => $headers, 'childcon' => $childcon, 'confunc' => $confunc, 'masterPageTitle' => $masterPageTitle, 'formSettingBtns' => $formSettingBtns, 'launcherid' => $form->launcherid, 'event' => $event), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id,__("Screens") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id,__("Screens") => $this->uri->uri_string());
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
		}else{
			$cdata['content'] 		= $this->load->view('c_listview', array('form' => $form, 'data' => $formscreens, 'headers' => $headers, 'childcon' => $childcon, 'confunc' => $confunc, 'typeapp' => 'app', 'masterPageTitle' => $masterPageTitle, 'formSettingBtns' => $formSettingBtns, 'launcherid' => $form->launcherid, 'app' => $app), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$cdata['crumb']			= array(__("Apps") => "apps", $app->name => "apps/view/".$app->id, $form->title => "formscreens/form/".$form->id,__("Screens") => $this->uri->uri_string());

		}
		$this->load->view('master', $cdata);
	}
	
	function add($id, $step = 0) {
		if($id == FALSE || $id == 0) redirect('apps');
		$form = $this->forms_model->getFormByID($id);
		$masterPageTitle = 'Screens in form '.$form->title;
		$launcher = $this->module_mdl->getLauncherById($form->launcherid);
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$this->iframeurl = 'forms/event/'.$eventid.'/'.$form->launcherid;
		}else if($form->venueid){
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$this->iframeurl = 'forms/venue/'.$venueid.'/'.$form->launcherid;
		}else{
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
			$this->iframeurl = 'forms/app/'.$appid.'/'.$form->launcherid;
		}
		
		$this->load->library('form_validation');
		$error = "";
		$formscreens = $this->forms_model->getFormScreensByFormID($id);
		if(!empty($formscreens)) {
			$this->iframeurl = 'formfields/formscreen/'.$formscreens[0]->id.'?r=1&width=247';
		}
		
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		//for football event get artists of own team
		if($app->apptypeid == 6) {
			$artists = $this->artist_model->getArtistsByAppID($app->id);
		} else {
			$artists = array();
		}

        // If user don't want multiple screen , Insert one screen automatically and redirect user to adding controls view
        // Start 18-07-12

		if($step == 1)
		{
			$updData = array(
				'singlescreen' => 1,
				);

			$this->db->where('id', $id);
			$this->db->update('form', $updData);

			if($launcher->moduletypeid == 62) {
				$visible = 0;
			} else {
				$visible = 1;
			}

			$data = array(
				"formid" => $id,
				"title" => 'Default Screen',
				"order"	=> 0,
				'visible' => $visible
				);

			if($newid = $this->general_model->insert('formscreen', $data)){
                ////add translated fields to translation table
				foreach($languages as $language) {							
					$this->language_model->addTranslation('formscreen', $newid, 'title', $language->key, 'Default Screen');
				}

				if($type == 'venue'){
					_updateTimeStamp($venue->id);
				}else{
					_updateTimeStamp($event->id);
				}

				redirect('formfields/formscreen/'.$newid);

			}else {
				$error = "Oops, something went wrong. Please try again.";
			}

		}

        // End

		if($this->input->post('postback') == "postback") {
			foreach($languages as $language) {
				$this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
			}				
			$this->form_validation->set_rules('order', 'order', 'trim|numeric');
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {

				if($error == '') {		
					if($launcher->moduletypeid == 62) {
						$visible = 0;
					} else {
						$visible = 1;
					}
					$order = 0;
					if($this->input->post('order') != '') {
						$order = $this->input->post('order');
					}
					$data = array(
						"formid"			=> $id,
						"title"             => set_value('title_'.  $app->defaultlanguage),
						"order"				=> $order,
						'visible'			=> $visible
						);
					
					if($newid = $this->general_model->insert('formscreen', $data)){
						
						//add translated fields to translation table
						foreach($languages as $language) {							
							$this->language_model->addTranslation('formscreen', $newid, 'title', $language->key, $this->input->post('title_'.$language->key));
						}
						
						$this->session->set_flashdata('event_feedback', __('The screen has successfully been add!'));
						
						if($type == 'venue'){
							_updateTimeStamp($venue->id);
						}else{
							_updateTimeStamp($event->id);
						}

						if($error == '') {
							redirect('formscreens/form/'.$form->id);
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}						
				}

			}
		}
		
		if($type == 'venue'){
			$cdata['content'] 		= $this->load->view('c_formscreen_add', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $form->title => "formscreens/form/".$form->id,__("Add Screen") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
		}else if($type == 'event'){
			$cdata['content'] 		= $this->load->view('c_formscreen_add', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id,__("Add Screen") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id,__("Add Screen") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
		}else{
			$cdata['content'] 		= $this->load->view('c_formscreen_add', array('app' => $app, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle' => $masterPageTitle), TRUE);
			$cdata['crumb']			= array(__("Apps") => "apps", $app->name => "apps/view/".$app->id, $form->title => "formscreens/form/".$form->id,__("Add Screen") => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		}
		$this->load->view('master', $cdata);
	}

	function edit($id) {
		if($id == FALSE || $id == 0) redirect('apps');
		$formscreen = $this->forms_model->getFormScreenByID($id);
		$form = $this->forms_model->getFormByID($formscreen->formid);
		$masterPageTitle = 'Screens in form '.$form->title;
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
		}else if($form->venueid){
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			
		}else{
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
		}

		$this->load->library('form_validation');
		$error = "";
		
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		
		$this->iframeurl = 'formfields/formscreen/'.$formscreen->id.'?r=1&width=247';


		if($this->input->post('postback') == "postback") {
			foreach($languages as $language) {
				$this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
			}
			
			$this->form_validation->set_rules('order', 'order', 'trim|numeric');
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();				
			} else {										
				if($error == '') {

					if($this->form_validation->run() == FALSE || $error != ''){
						$error .= validation_errors();
					}else {					

						$data = array(
							"title"     => set_value('title_'.  $app->defaultlanguage),
							"order"		=> set_value('order')
							);

						if($this->forms_model->edit_table($id, $data, 'formscreen')){

						//add translated fields to translation table
							foreach($languages as $language) {

								$title = $this->input->post('title_'.$language->key);
								$nameSuccess = $this->language_model->updateTranslation('formscreen', $id, 'title', $language->key, $title);
								if($nameSuccess == false || $nameSuccess == null) {
									$this->language_model->addTranslation('formscreen', $id, 'title', $language->key, $title);
								}														
							}

							$this->session->set_flashdata('event_feedback', __('The screen has successfully been updated'));
							_updateTimeStamp($form->id);
							redirect('formscreens/form/'.$form->id);
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
				}
			}
		}		
		
		if($type == 'venue'){
			$cdata['content'] 		= $this->load->view('c_formscreen_edit', array('form' => $form, 'formscreen' => $formscreen, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle'=>$masterPageTitle), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $form->title => "formscreens/form/".$form->id,__("Edit: ") . $formscreen->title => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
		}else if($type == 'event'){
			$cdata['content'] 		= $this->load->view('c_formscreen_edit', array('form' => $form, 'formscreen' => $formscreen, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle'=>$masterPageTitle), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id,__("Edit: ") . $formscreen->title => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id,__("Edit: ") . $formscreen->title => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
		}else{
			$cdata['content'] 		= $this->load->view('c_formscreen_edit', array('form' => $form, 'formscreen' => $formscreen, 'error' => $error, 'languages' => $languages, 'app' => $app, 'masterPageTitle'=>$masterPageTitle), TRUE);
			$cdata['crumb']			= array(__("Apps") => "apps", $app->name => "apps/view/".$app->id, $form->title => "formscreens/form/".$form->id,__("Edit: ") . $formscreen->title => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		}
		$this->load->view('master', $cdata);
	}
	
	function delete($id) {		
		
		if($id == FALSE || $id == 0) redirect('apps');
		$formscreen = $this->forms_model->getFormScreenByID($id);
		$form = $this->forms_model->getFormByID($formscreen->formid);
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
		}else if($form->venueid){
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			
		}else{
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
		}
		
		$this->language_model->removeTranslations('formscreen', $id);
		if($this->general_model->remove('formscreen', $id)){
			$this->forms_model->removeFieldsOfScreen($id);
			$this->session->set_flashdata('event_feedback', __('The screen has successfully been deleted'));
			_updateTimeStamp($form->id);
			redirect('formscreens/form/'.$form->id);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('formscreens/form/'.$form->id);
		}
	}

	function sort($type = '') {
		if($type == '' || $type == FALSE) redirect('events');
		
		switch($type){
			case "formscreen":
			$orderdata = $this->input->post('records');
			foreach ($orderdata as $val) {
				$val_split = explode("=", $val);

				$data['order'] = $val_split[1];
				$this->db->where('id', $val_split[0]);
				$this->db->update('formscreen', $data);
				$this->db->last_query();
			}
			break;			
			default:
			break;
		}
	}

	function result($screenid) {
		$formscreen = $this->forms_model->getFormScreenByID($screenid);
		$form = $this->forms_model->getFormByID($formscreen->formid);
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id,$formscreen->title => 'formfields/formscreen/'.$formscreen->id, __('Results') => $this->uri->uri_string());
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
		} elseif($form->venueid) {
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $form->title => "formscreens/form/".$form->id,$formscreen->title => 'formfields/formscreen/'.$formscreen->id, __('Results') => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
		} else {
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
			$cdata['crumb']			= array($app->name => "apps/view/".$app->id, $form->title => "formscreens/form/".$form->id,$formscreen->title => 'formfields/formscreen/'.$formscreen->id, __('Results') => $this->uri->uri_string());
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app, 'forms', $form->launcherid);
		} 

		$formscreenresults = $this->forms_model->getResultsOfScreen($screenid, $form->id);

		$colors = $this->forms_model->getCustomColors($app->id, 'voting');

		$cdata['content'] 		= $this->load->view('c_formscreen_results', array('form' => $form, 'formscreen' => $formscreen, 'results' => $formscreenresults, 'error' => $error, 'languages' => $languages, 'app' => $app, 'colors' => $colors), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		if(isset($_GET['blank'])) {
			$pagelayout = $this->forms_model->getPageLayout($app->id, 'voting');
			$cdata['customhtml'] = $pagelayout;
			$this->load->view('master_blank', $cdata);
		} else {
			$this->load->view('master', $cdata);
		}
		
	}

	function screenvisible($screenid, $visible) {
		$formscreen = $this->forms_model->getFormScreenByID($screenid);
		$form = $this->forms_model->getFormByID($formscreen->formid);
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
		} elseif($form->venueid) {
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
		} else {
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
		} 

		if($visible == 0) {
			$this->general_model->update('formscreen', $screenid, array('visible' => 1));
		} else {
			$this->general_model->update('formscreen', $screenid, array('visible' => 0));
		}

		redirect('formscreens/form/'.$form->id);
	}

	function resultlayout($screenid) {
		$formscreen = $this->forms_model->getFormScreenByID($screenid);
		$form = $this->forms_model->getFormByID($formscreen->formid);
		
		if($form->eventid) {
			$type = "event";
			$eventid = $form->eventid;
			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, $form->title => "formscreens/form/".$form->id,__("Edit: ") . $formscreen->title => $this->uri->uri_string());
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'forms', $form->launcherid);
		} elseif($form->venueid) {
			$type = "venue";
			$venueid = $form->venueid;
			$venue = $this->venue_model->getById($venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $form->title => "formscreens/form/".$form->id,__("Edit: ") . $formscreen->title => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'forms', $form->launcherid);
		} else {
			$type = "app";
			$appid = $form->appid;
			$app = $this->app_model->get($appid);
			$cdata['crumb']			= array($app->name => "apps/view/".$app->id, $form->title => "formscreens/form/".$form->id,__("Edit: ") . $formscreen->title => $this->uri->uri_string());
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app, 'forms', $form->launcherid);
		} 

		$pagelayout = $this->forms_model->getPageLayout($app->id, 'voting');

		$formfields = $this->forms_model->getFormFieldsByFormScreenID($formscreen->id);
		$maxpossiblevalues = 0;
		foreach($formfields as $f) {
			$count = count(explode(',', $f->possiblevalues));
			if($maxpossiblevalues < $count) {
				$maxpossiblevalues = $count;
			}
		}


		$colors = $this->forms_model->getCustomColors($app->id, 'voting');

		if($this->input->post('postback') == "postback") {
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('bodyHtml', 'body html', 'trim');
			$this->form_validation->set_rules('headerHtml', 'header html', 'trim');
			$this->form_validation->set_rules('footerHtml', 'footer html', 'trim');
			$this->form_validation->set_rules('css', 'css', 'trim');

			$i = 0;
			while($i < count($colors)) {
				if(isset($_POST['color'.$i])) {
					$colors[$i] = $_POST['color'.$i];
					if(substr($colors[$i], 0,1) != '#') {
						$colors[$i] = '#'.$colors[$i];
					}
				}

				$i++;
			}

			$data = array(
				'appid' => $app->id,
				'purpose' => 'voting',
				'body' => $this->input->post('bodyHtml'),
				'header' => $this->input->post('headerHtml'),
				'footer' => $this->input->post('footerHtml'),
				'css' => $this->input->post('css'),
				'colors' => implode(',', $colors)
				);
			$id = $this->general_model->insert_unique('customhtml', $data, array('appid', 'purpose'), array($app->id, 'voting'));

			redirect('event/view/'.$event->id);
		}

		$cdata['content'] 		= $this->load->view('c_formscreen_resultlayout', array('form' => $form, 'formscreen' => $formscreen, 'data' => $pagelayout, 'error' => $error, 'app' => $app, 'maxpossiblevalues' => $maxpossiblevalues, 'colors' => $colors), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		if(isset($_GET['blank'])) {
			$this->load->view('master_blank', $cdata);
		} else {
			$this->load->view('master', $cdata);
		}
		
	}
	
}