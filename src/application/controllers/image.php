<?php

	class Image extends CI_Controller
	{
		function render($width, $height,$ext)
		{
			// ====================
			// = RECONSTRUCT PATH =
			// ====================
			$path = "." . $ext;
			for($i = $this->uri->total_segments(); $i > 5; $i--)
			{
				$path = "/" . $this->uri->segment($i) . $path;
			}
			$path = "uploads/" . substr($path,1);
			
			$new_image = BASEPATH . "../cache/" . $width . "_" . $height . "_" . substr($path, strrpos ( $path , "/") + 1);

			$config['image_library'] 	= 'gd2';
			$config['source_image']		= BASEPATH . "../" . $path;
			$config['create_thumb'] 	= FALSE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']	 		= $width;
			$config['height']			= $height;
			$config['dynamic_output'] 	= FALSE;
			$config['new_image'] = $new_image;
			if($width == 0) $config['width'] = 10000;
			if($height == 0) $config['height'] = 10000;
			
			$this->load->library('image_lib', $config); 
			if ( ! $this->image_lib->resize())
			{
			    echo $this->image_lib->display_errors();
			} else {
				$url = base_url() . "cache/" . $width . "_" . $height . "_" . substr($path, strrpos ( $path , "/") + 1);
				//header("Location: " . $url);
				echo file_get_contents($url);
			}
		}
		
		function crop($width, $height,$ext)
		{
			// ====================
			// = RECONSTRUCT PATH =
			// ====================
			$path = "." . $ext;
			for($i = $this->uri->total_segments(); $i > 5; $i--)
			{
				$path = "/" . $this->uri->segment($i) . $path;
			}
			$path = "uploads/" . substr($path,1);
			$path = $this->config->item('imagespath') . $path;
			
			$new_image = BASEPATH . "../cache/cp_" . $width . "_" . $height . "_" . substr($path, strrpos ( $path , "/") + 1);
			
			$sizes = getimagesize($path);
			$old_width = $sizes[0];
			$old_height = $sizes[1];
			$scale = max($width/$old_width, $height/$old_height);

			$config['image_library'] 	= 'gd2';
			$config['source_image']		= $path;
			$config['create_thumb'] 	= FALSE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']	 		= $old_width*$scale;
			$config['height']			= $old_height*$scale;
			$config['dynamic_output'] 	= FALSE;
			$config['new_image'] = $new_image;
			
			$this->load->library('image_lib', $config); 
			if ( ! $this->image_lib->resize())
			{
			    echo $this->image_lib->display_errors();
			} else {
				$this->image_lib->clear();
				
				$config['image_library'] 	= 'gd2';
				$config['source_image']		= $new_image;
				$config['create_thumb'] 	= FALSE;
				$config['maintain_ratio'] 	= FALSE;
				$config['width']	 		= $width;
				$config['height']			= $height;
				$config['dynamic_output'] 	= FALSE;
				$config['source_image']		= $new_image;
				$config["x_axis"] = ($old_width*$scale - $width)/2;
				$config["y_axis"] = ($old_height*$scale - $height)/2;
				$this->image_lib->initialize($config); 
				$this->image_lib->crop();
				
				$url = base_url() . "cache/cp_" . $width . "_" . $height . "_" . substr($path, strrpos ( $path , "/") + 1);
				//header("Location: " . $url);
				echo file_get_contents($url);
			}
		}
	}
	

?>