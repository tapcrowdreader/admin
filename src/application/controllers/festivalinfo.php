<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Festivalinfo extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('metadata_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Info',
			'plural' => 'Info',
			'parentType' => 'event',
			'browse_url' => 'festivalinfo/--parentType--/--parentId--',
			'add_url' => 'festivalinfo/add/--parentId--/--parentType--',
			'edit_url' => 'festivalinfo/edit/--id--',
			'module_url' => 'module/editByController/about/--parentType--/--parentId--/0',
			'headers' => array(
				__('Title') => 'key'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'festivalinfo/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/festivalinfo/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'festivalinfo/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			)
		);
	}
	
	function index() {
		$this->event();
	}
	
	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');
		
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		
		$this->iframeurl = 'event/festivalinfo/'.$id;
		
		$info = $this->metadata_model->get($app->id, 'event', $id);
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $info), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Info") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, __("Info") => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'info');
		$this->load->view('master', $cdata);
	}
	
	function add($id, $type = 'event') {
		$this->load->library('form_validation');
		$error = "";
		$imageError = "";

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$this->iframeurl = 'event/festivalinfo/'.$id;	

		$metadata = $this->metadata_model->get($app->id, 'event', $id);

		if($this->input->post('postback') == "postback") {
		    foreach($languages as $language) {
		        $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
		        $this->form_validation->set_rules('text_'.$language->key, 'text ('.$language->name.')', 'trim|required');
		    }
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {					
				$data = array( 
						"appid" => $app->id,
						"table"	=> "event",
						"identifier" 	=> $event->id,
						"key" 	=> set_value('title_'.$app->defaultlanguage),
						"value" => $this->input->post('text_'.$app->defaultlanguage)
					);
				
				if($metaid = $this->general_model->insert('metadata', $data)){					
		            //add translated fields to translation table
		            foreach($languages as $language) {
		                $this->language_model->addTranslation('metadata', $metaid, 'key', $language->key, $this->input->post('title_'.$language->key));
		                $this->language_model->addTranslation('metadata', $metaid, 'value', $language->key, $this->input->post('text_'.$language->key));
		            }
		            
					$this->session->set_flashdata('event_feedback', __('The item has successfully been added!'));
					_updateTimeStamp($event->id);
					if($error == '') {
						redirect('festivalinfo/event/'.$event->id);
					} 					
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_festivalinfo_add', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Info") => "festivalinfo/event/".$event->id, __("Add") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, __("Info") => "festivalinfo/event/".$event->id, __("Add") => $this->uri->uri_string());
		}
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'info');
		$this->load->view('master', $cdata);
	}

	function image_rules($str){
		$filename = 'image';
		return image_check($filename, $_FILES);
	}  
	
	function edit($id, $type) {  
		$this->load->library('form_validation');
		$error = "";
        $imageError = "";
		// EDIT NEWSITEM
		$metadata = $this->metadata_model->getById($id);
		if($metadata == FALSE) redirect('events');
		
		$event = $this->event_model->getbyid($metadata->identifier);
		$app = _actionAllowed($event, 'event','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$this->iframeurl = 'about/meta/'.$id;

		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                $this->form_validation->set_rules('text_'.$language->key, 'text ('.$language->name.')', 'trim|required');
            }
			
			if($this->form_validation->run() == FALSE || $error != ''){
				$error .= validation_errors();
			} else {					
				$data = array( 
						"key" => set_value('title_'. $app->defaultlanguage),
						"value" => $this->input->post('text_'. $app->defaultlanguage),
					);
				
				if($this->general_model->update('metadata', $id, $data)){
                    //add translated fields to translation table
                    foreach($languages as $language) {
						$nameSuccess = $this->language_model->updateTranslation('metadata', $id, 'key', $language->key, $this->input->post('title_'.$language->key));
						if(!$nameSuccess) {
							$this->language_model->addTranslation('metadata', $id, 'key', $language->key, $this->input->post('title_'.$language->key));
						}
						$descrSuccess = $this->language_model->updateTranslation('metadata', $id, 'value', $language->key, $this->input->post('text_'.$language->key));
						if(!$descrSuccess) {
							$this->language_model->addTranslation('metadata', $id, 'value', $language->key, $this->input->post('text_'.$language->key));
						}
                    }
					$this->session->set_flashdata('event_feedback', __('The item has successfully been updated'));
					_updateTimeStamp($event->id);
					redirect('festivalinfo/event/'.$event->id);
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_festivalinfo_edit', array('event' => $event, 'metadata' => $metadata, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Info") => "festivalinfo/event/".$event->id, __("Edit: ") . $metadata->key => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, __("Info") => "festivalinfo/event/".$event->id, __("Edit: ") . $metadata->key => $this->uri->uri_string());
		}
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'info');
		$this->load->view('master', $cdata);
	}
	
	function delete($id, $type) {
		$metadata = $this->metadata_model->getById($id);
		$event = $this->event_model->getbyid($metadata->identifier);
		$app = _actionAllowed($event, 'event','returnApp');
        $this->language_model->removeTranslations('metadata', $id);
		if($this->general_model->remove('metadata', $id)){
			$this->session->set_flashdata('event_feedback', __('The item has successfully been deleted'));
			_updateTimeStamp($metadata->identifier);
			redirect('festivalinfo/event/'.$metadata->identifier);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('festivalinfo/event/'.$metadata->identifier);
		}
	}
}