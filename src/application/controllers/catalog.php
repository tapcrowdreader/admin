<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Catalog extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('catalog_model');
		$this->load->model('module_mdl');
		$this->load->model('premium_model');
		$this->load->model('basket_model');
		$this->load->model('confbag_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Catalog',
			'plural' => 'Catalogs',
			'title' => 'Catalogs',
			'parentType' => 'event',
			'browse_url' => 'catalog/--parentType--/--parentId--',
			'add_url' => 'catalog/add/--parentId--/--parentType--',
			'edit_url' => 'catalog/edit/--id--',
			'module_url' => 'module/editByController/catalog/--parentType--/--parentId--/0',
			'import_url' => '',
			'headers' => array(
				__('Name') => 'name',
				__('Order') => 'order'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'catalog/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/catalog/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'catalog/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false,
			'deletecheckedurl' => 'catalog/removemany/--parentId--/--parentType--'
		);
	}

	function event($id, $groupid = '') {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		// Get Sessions for event per group
		if($groupid != ''){
			$catalogs = $this->catalog_model->getCatalogByGroups($groupid);
		} else {
			$catalogs = $this->catalog_model->getCatalosgByEventID($id);
		}
		$cataloggroups = $this->catalog_model->getCataloggroupsByEventID($id);
		$headers = array(__('Name') => 'name');

		$cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => $catalogs, 'cataloggroups' => $cataloggroups, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Catalogs") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function venue($id, $groupid = '', $catalogtype = '') {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		if($catalogtype == '') {
			// $this->iframeurl = 'venuecatalog/by-venue/'.$id;
			$this->iframeurl = 'catalogs/venue/'.$id;
		} else {
			// $this->iframeurl = 'venuecatalog/by-venue/'.$id.'/'.$catalogtype;
			$this->iframeurl = $catalogtype.'/venue/'.$id;
		}

		//for new system groups get main categorie group
		$catalogid = $this->catalog_model->getCatalogGroup($venue->id);
		if($catalogid && ($catalogtype == '' || $catalogtype == 'catalogs')) {
			redirect('groups/view/'.$catalogid.'/venue/'.$venue->id.'/catalogs');
		}
		$catalogcategoriesgroupid = $this->catalog_model->getMainCategorieGroup($id);
		$catalogs = $this->catalog_model->getCatalogsByVenueID($id, $catalogtype);
		$cataloggroups = array();

		$moduletypeid = 15;
		if($catalogtype != '') {
			$moduletypeid = $this->module_mdl->getModuletypeByComponent($catalogtype)->id;
		}
		$launcher = $this->module_mdl->getLauncher($moduletypeid, 'venue', $venue->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		if($catalogcategoriesgroupid == false) {
			// Get Catalogs for venue per group
			if($groupid != '' && $groupid != '0'){
				$catalogs = $this->catalog_model->getCatalogsByGroups($groupid, $catalogtype);
			} else {
				$catalogs = $this->catalog_model->getCatalogsByVenueID($id, $catalogtype);
			}
			$cataloggroups = $this->catalog_model->getCataloggroupsByVenueID($id);
		} elseif($catalogtype == '') {
			redirect('groups/view/'.$catalogcategoriesgroupid.'/venue/'.$venue->id.'/catalogs');
		}

		$headers = array(__('Name') => 'name');

		$breadcrumbtype = 'Product catalog';
		$moduletype = false;
		if($catalogtype != '') {
			$breadcrumbtype = ucfirst($catalogtype);
			$moduletype = $this->module_mdl->getModuletypeByComponent($catalogtype);
		}

		if(!empty($catalogtype)) {
			$this->_module_settings->plural = $catalogtype;
			$this->_module_settings->singular = substr($catalogtype, 0, strlen($catalogtype) -1);
			$this->_module_settings->module_url = 'module/edit/'.$moduletype->id.'/--parentType--/--parentId--';
			$this->_module_settings->add_url .= '/'.$catalogtype;
			$this->_module_settings->actions['edit']->href .= '/'.$catalogtype;
			$this->_module_settings->actions['delete']->href .= '/'.$catalogtype;
		}
		$this->_module_settings->parentType = 'venue';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'venue', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'venue', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $deletecheckedurl);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($catalogs)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $this->_module_settings->title => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, $catalogtype);
		$this->load->view('master', $cdata);
	}

	function add($id, $type = 'event', $catalogtype = '', $newgroupid = '', $basegroupid = '') {
		if($catalogtype == 'catalog') {
			$catalogtype = '';
		}
		$basegroup = false;
		if(!empty($basegroupid)) {
			$basegroup = $this->group_model->getById($basegroupid);
		}
		$brands = array();
		$categories = array();
		if($type == 'venue') {
			$this->load->library('form_validation');
			$this->load->library('crocodoc'); // Crocodoc API

			$error = "";
			$venue = $this->venue_model->getbyid($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			$this->load->model('metadata_model');

			// $this->iframeurl = 'venuecatalog/by-venue/'.$id;

			if($catalogtype == '') {
				// $this->iframeurl = 'venuecatalog/by-venue/'.$id;
				$this->iframeurl = 'catalogs/venue/'.$id;
			} else {
				// $this->iframeurl = 'venuecatalog/by-venue/'.$id.'/'.$catalogtype;
				$this->iframeurl = $catalogtype.'/venue/'.$id;
			}

			if(($app->apptypeid == 8 || $app->apptypeid == 7 || $app->apptypeid == 4) && !empty($newgroupid)) {
				$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($newgroupid, $venue->id);
				$this->iframeurl = 'catalogs/resto/'.$venue->id.'/'.$newgroupid;
			}

			$maincat = $this->catalog_model->getMainCategorieGroup($venue->id);
			$catalogid = $this->catalog_model->getCatalogGroup($venue->id);
			if($catalogid && ($catalogtype == '' || $catalogtype == 'catalogs' || $catalogtype == 'catalog')) {
				$maincat = $catalogid;
			}

			$brandshtml = '';
			$catshtml = '';
			$categories = array();

			if($newgroupid != '' || $maincat != false) {
				$catsgroupid = $maincat;
				if($catsgroupid) {
					$this->catalog_model->htmlarray = array();
					$catshtml = $this->catalog_model->display_children($catsgroupid,0, $id);
					$currentCats = $this->catalog_model->groupids;
					$this->catalog_model->groupids = '';
				}
			} else {
				$brands = $this->catalog_model->getBrandsByVenueID($id);
				$categories = $this->catalog_model->getCategoriesByVenueID($id);
			}

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'catalog' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			$moduletypeid = 15;
			if($catalogtype != '') {
				$moduletypeid = $this->module_mdl->getModuletypeByComponent($catalogtype)->id;
			}
			if(!empty($basegroupid) && $basegroupid != 0) {
				$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($basegroupid, $venue->id);
			} else {
				$launcher = $this->module_mdl->getLauncher($moduletypeid, 'venue', $venue->id);
			}

			if(!$launcher) {
				$launcher = $this->module_mdl->getLauncher($moduletypeid, 'venue', $venue->id);
			}
			
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, '', '', $app);

			//price field
			if(!empty($newgroupid)) {
				$group = $this->group_model->getById($newgroupid);
				$parents = $this->group_model->getParents($newgroupid);
				foreach($parents as $p) {
					if(strtolower($p->name) == 'menu' || strtolower($group->name) == 'menu') {
						$catalogmodule = 'menu';
					} elseif(strtolower($p->name) == 'team' || strtolower($group->name) == 'team') {
						$catalogmodule = 'team';
					} elseif(strtolower($p->name) == 'gallery' || strtolower($group->name) == 'gallery') {
						$catalogmodule = 'gallery';
					}
				}
			}

			$confbagactive = $this->confbag_model->checkActive($venue->id, 'venue');

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('selbrands[]', 'selbrands', 'trim');
				$this->form_validation->set_rules('selcategories[]', 'selcategories', 'trim');
				$this->form_validation->set_rules('pdfdoc', 'image', 'trim');
				$this->form_validation->set_rules('imageurl', 'image', 'trim');
				// $this->form_validation->set_rules('email', 'email', 'trim');
				$this->form_validation->set_rules('premium', 'premium', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('externalid', 'external id', 'trim');
				$this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
				$this->form_validation->set_rules('confbagcontent', 'confbagcontent', 'trim');
				if($catalogtype == '' && $maincat == false) {
					$this->form_validation->set_rules('cataloggroup', 'catalog group', 'trim|required');
				} else {
					$this->form_validation->set_rules('cataloggroup', 'cataloggroup', 'trim');
				}
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
                    $this->form_validation->set_rules('urltitle_'.$language->key, 'urltitle_'.$language->key, 'trim');
                    $this->form_validation->set_rules('url_'.$language->key, 'url ('.$language->name.')', 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {	
					$url = $this->input->post('url_'.$app->defaultlanguage);
					if(!stristr($url, 'http') && !empty($url)) {
						$url = 'http://'.$this->input->post('url');
					}				
					if($catalogtype != '') {
						//old group system
						$cataloggroupid = $this->db->query("SELECT id FROM cataloggroup WHERE venueid = $venue->id AND name = '$catalogtype'");
						if($cataloggroupid->num_rows() != 0) {
							$cataloggroupid = $cataloggroupid->row()->id;
						} else {
							$groupdata = array('venueid' => $venue->id ,'name' => $catalogtype);
							$cataloggroupid = $this->general_model->insert('cataloggroup', $groupdata);
						}

						// for V2
						// if($cataloggroupid == '') {
						// 	$cataloggroupid = null;
						// }

						// Save
						$data = array(
								"cataloggroupid"=> $cataloggroupid,
								"venueid"		=> $id,
								"name"			=> set_value('name_'. $app->defaultlanguage),
								"description"	=> set_value('description_'.  $app->defaultlanguage),
								"type"			=> $catalogtype,
								'url' 			=> $url,
								'urltitle'		=> set_value('urltitle_'.$app->defaultlanguage)
							);
					} else {
						$cataloggroupid = $this->input->post('cataloggroup');
						// for V2
						// if($cataloggroupid == '') {
						// 	$cataloggroupid = null;
						// }
						// Save
						$data = array(
								"cataloggroupid"=> $cataloggroupid,
								"venueid"		=> $id,
								"name"			=> set_value('name_'. $app->defaultlanguage),
								"description"	=> set_value('description_'.  $app->defaultlanguage),
								"type"			=> $catalogtype,
								'url' 			=> $url,
								"order"			=> $this->input->post('order'),
								'externalid'	=> $this->input->post('externalid'),
								'urltitle'		=> set_value('urltitle_'. $app->defaultlanguage)
							);
					}

					$res = $this->general_model->insert('catalog', $data);

					//email
					// $email = $this->input->post('email');
					// if(!empty($email)) {
					// 	$this->general_model->insert('metadata', array('appid' => $app->id, '`table`' => 'catalog', 'identifier' => $res, '`key`' => 'email', '`value`' => $this->input->post('email')));
					// }
					
					
					$item = $this->db->query("SELECT * FROM prices WHERE iditem = $res");
					$price = $this->input->post("price");
					
					if(empty($price))
					{
						$price = 0.0;
					}
					
					$arr = array('iditem' => $res,'price' => $price);
					
					if($item->num_rows() > 0)
					{
						$this->db->query("UPDATE prices SET price=$price WHERE iditem=$res");
					}
					else
					{
						$this->db->insert('prices', $arr);
					}

					if($res != FALSE){
						if($app->id != 393) {
							if($this->input->post('mytagsulselect') != null && count($this->input->post('mytagsulselect')) > 0) {
								// *** SAVE TAGS *** //
								// *** SAVE TAGS *** //
								$tags = $this->input->post('mytagsulselect');
								foreach ($tags as $tag) {
									$this->general_model->insert('tc_tag', array(
										'appid' => $app->id,
										'itemtype' => 'catalog',
										'itemid' => $res,
										'tag' => $tag
									));
								}
							}
						} else {
							$tags = array(
									'appid' 	=> $app->id,
									'tag' 		=> $this->input->post('tagdelbar'),
									'catalogitemid' => $res
								);
							$this->db->insert('tag', $tags);
						}
						$uploadError = ''; // used to redirect to catalog-edit if image or pdf file upload has errors
						
						// *** //
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/catalogimages/".$res)){
							mkdir($this->config->item('imagespath') . "upload/catalogimages/".$res, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/catalogimages/'.$res;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('imageurl')) {
							$image = ""; //No image uploaded!
							if($_FILES['imageurl']['name'] != '') {
								$uploadError .= 'image';
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/catalogimages/'. $res . '/' . $returndata['file_name'];
							$this->general_model->update('catalog', $res, array('imageurl' => $image));

							$photo = $this->config->item('publicupload') . $image;
							$size = getimagesize($photo); 

							if($size[0] > 960) {
								//resize
								$newname = 'upload/catalogimages/'.$res.'/catalogimage'.$returndata['raw_name'].'@2x.png';
								$config['image_library'] = 'gd2';
								$config['source_image'] = $this->config->item('imagespath') . $image;
								$config['maintain_ratio'] = TRUE;
								$config['master_dim'] = 'width';
								$config['width'] = 960;
								$config['height'] = 640;
								$config['new_image'] = $this->config->item('imagespath') . $newname;

								$this->image_lib->initialize($config);

								if (!$this->image_lib->resize())
								{
									//echo $this->image_lib->display_errors();
									if($_FILES['imageurl']['name'] != '') {
										// $error .= $this->image_lib->display_errors();
									}
								} else {
									$this->general_model->update('catalog', $res, array('imageurl' => $newname));
								}
							}
						}
						
						// *** //
						//Uploads Pdf						
						if(!is_dir($this->config->item('imagespath') . "upload/catalogpdf/".$res)){
							mkdir($this->config->item('imagespath') . "upload/catalogpdf/".$res, 0775, true);
						}

						foreach($languages as $language) {
							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
									$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'catalog', $res, $app, $language);
										}
									}
								}
							}

							$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/catalogpdf/'.$res;
							$configexlogo['allowed_types'] = 'PDF|pdf';
							$configexlogo['max_size']  = '2048';
							$this->load->library('upload', $configexlogo);
							$this->upload->initialize($configexlogo);
							if (!$this->upload->do_upload('pdfdoc'.$language->key)) {
								if($_FILES['pdfdoc'.$language->key]['name'] != '') {
									$uploadError .= 'pdf';
									$error .= $this->upload->display_errors();
								}
							} else {
								//successfully uploaded
								$returndata = $this->upload->data();
								$response = $this->crocodoc->upload( $this->config->item('publicupload') . 'upload/catalogpdf/'.$res.'/'.$returndata['file_name'] );
								
								if($language->key == $app->defaultlanguage) {
									$this->general_model->update('catalog', $res, array(
										'pdf' => 'upload/catalogpdf/'.$res.'/'.$returndata['file_name'],
										'uuid' => isset( $response['uuid'] ) ? $response['uuid'] : ''
										));
									$pdf = 'upload/catalogpdf/'.$id.'/'.$returndata['file_name'];
									$uuid = isset( $response['uuid'] ) ? $response['uuid'] : '';
								}

								$this->language_model->addTranslation('catalog', $res, 'pdf', $language->key, 'upload/catalogpdf/'.$res.'/'.$returndata['file_name']);
								$this->language_model->addTranslation('catalog', $res, 'uuid', $language->key, isset( $response['uuid'] ) ? $response['uuid'] : '');
							}
						}

						//confbag
						if($confbagactive) {
							if(!is_dir($this->config->item('imagespath') . "upload/confbagfiles/venue/".$venue->id)){
								mkdir($this->config->item('imagespath') . "upload/confbagfiles/venue/".$venue->id, 0775, TRUE);
							}
							$configconfbag['upload_path'] = $this->config->item('imagespath') .'upload/confbagfiles/venue/'.$venue->id;
							$configconfbag['allowed_types'] = 'JPG|jpg|jpeg|png|pdf|xls|xlsx|txt|doc|docx|ppt|pptx';
							$configconfbag['max_size'] = '50000';
							$this->load->library('upload', $configconfbag);

							$this->upload->initialize($configconfbag);
							if ($this->upload->do_upload('confbagcontent')) {
								$returndata = $this->upload->data();
								$confbagcontent = 'upload/confbagfiles/venue/'.$venue->id.'/'.$returndata['file_name'];
								$this->general_model->insert('confbag', array('venueid' => $venue->id, 'itemtable' => 'catalog', 'tableid' => $res, 'documentlink' => $confbagcontent));
							} else {
								if($_FILES['confbagcontent']['name'] != '') {
									$error .= $this->upload->display_errors();
								}
							}
						}
						
						//$this->general_model->update('catalog', $res, array('imageurl' => $image));
						//save premium
						$this->premium_model->save($this->input->post('premium'), 'catalog', $res, $this->input->post('extraline'), false, 'venue', $venue->id, $this->input->post('premiumorder'));

						//new group system
						if($this->input->post('catshidden') != null && $this->input->post('catshidden') != '') {
							$groups = $this->input->post('catshidden');
							$groups = explode('/',$groups);
							foreach($groups as $groupid) {
								if($groupid != '') {
									$group = $this->group_model->getById($groupid);
									$groupitemdata = array(
											'appid'	=> $app->id,
											'venueid'	=> $id,
											'groupid'	=> $group->id,
											'itemtable'	=> 'catalog',
											'itemid'	=> $res,
											'displaytype' => $group->displaytype
										);
									$this->general_model->insert('groupitem', $groupitemdata);
								}
							}
						} else {
							//add to main cat
							if(!empty($newgroupid)) {
								$group = $this->group_model->getById($newgroupid);
							} else {
								$group = $this->group_model->getById($maincat);
							}
							
							$groupitemdata = array(
									'appid'	=> $app->id,
									'venueid'	=> $id,
									'groupid'	=> $group->id,
									'itemtable'	=> 'catalog',
									'itemid'	=> $res,
									'displaytype' => $group->displaytype
								);
							$this->general_model->insert('groupitem', $groupitemdata);
						}

						// Save brands
						if($this->catalog_model->saveBrands($res, $this->input->post('selbrands'))){
							// Save categories
							if($this->catalog_model->saveCategories($res, $this->input->post('selcategories'))){
                                //add translated fields to translation table
                                foreach($languages as $language) {
                                    $this->language_model->addTranslation('catalog', $res, 'name', $language->key, $this->input->post('name_'.$language->key));
                                    $this->language_model->addTranslation('catalog', $res, 'description', $language->key, $this->input->post('description_'.$language->key));
                                    $this->language_model->addTranslation('catalog', $res, 'urltitle', $language->key, $this->input->post('urltitle_'.$language->key));
                                    $this->language_model->addTranslation('catalog', $res, 'url', $language->key, $this->input->post('url_'.$language->key));
                                }
								$this->session->set_flashdata('event_feedback', __('The catalog item has successfully been added'));
								_updateVenueTimeStamp($venue->id);
								if($error == '') {
									if($newgroupid != '') {
										redirect('groups/view/'.$newgroupid.'/venue/'.$venue->id.'/catalog');
									} else {
										redirect('catalog/venue/'.$venue->id.'/0/'.$catalogtype);
									}

								}else {
									//image or pdf error
									redirect('catalog/edit/'.$res.'/venue/catalog/'.$newgroupid.'?error='.$uploadError);
								}

							} else {
								$error = __("Oops, something went wrong. Please try again.");
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

				}
			}
			$item = $this->db->query("SELECT * FROM prices WHERE iditem=$id")->row();

            $groups = $this->catalog_model->getCataloggroupsByVenueID($venue->id);

			$breadcrumbtype = 'Product catalog';
			if($catalogtype != '') {
				$breadcrumbtype = ucfirst($catalogtype);
			}

			$cdata['content'] 		= $this->load->view('c_catalog_add', array('venue' => $venue, 'cataloggroups' => $groups, 'brands' => $brands, 'categories' => $categories, 'error' => $error, 'languages' => $languages, 'app' => $app, 'newgroupid' => $newgroupid, 'catshtml'	=> $catshtml, 'maincat' => $maincat, 'catalogtype' => $catalogtype, 'apptags' => $apptags, 'postedtags' => $postedtags, 'item' => $item, 'basegroup' => $basegroup, 'metadata' => $metadata, 'catalogmodule' => $catalogmodule, 'confbagactive' => $confbagactive), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			if($basegroup) {
				$crumbarray = array($venue->name => 'venue/view/'.$venue->id);

				foreach($parents as $p) {
					if($p->name != 'catalogcategories') {
						if(strtolower($p->name) == 'menu') {
							$launcher = $this->module_mdl->getLauncher(50, 'venue', $venue->id);
							$crumbarray[$launcher->title] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/catalog';
						} elseif(strtolower($p->name) == 'gallery') {
							$launcher = $this->module_mdl->getLauncher(51, 'venue', $venue->id);
							$crumbarray[$launcher->title] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/catalog';
						} elseif(strtolower($p->name) == 'team') {
							$launcher = $this->module_mdl->getLauncher(52, 'venue', $venue->id);
							$crumbarray[$launcher->title] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/catalog';
						} else {
							$crumbarray[$p->name] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/catalog';
						}
					}
				}
				$crumbarray[$group->name] = 'groups/view/'.$group->id.'/venue/'.$venue->id.'/catalog';
				
				$currentCats = explode('/', $currentCats);
				foreach($currentCats as $c) {
					if($c!='' && $c <= $newgroupid) {
						// $group = $this->group_model->getById($c);
						// $crumbarray[$group->name] = 'groups/view/'.$c.'/venue/'.$venue->id.'/catalog';
					}
				}
				$crumbarray[__("Add new ")] = $this->uri->uri_string();
				$cdata['crumb']			= checkBreadcrumbsVenue($app, $crumbarray);
			} else {
				$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "catalog/venue/".$venue->id."/0/".$catalogtype, __("Add new") => $this->uri->uri_string()));
			}
			$menucontroller = $catalogtype;
			if(empty($catalogtype)) {
				$menucontroller = 'catalog';
			}
			if($launcher) {
				$menucontroller = strtolower($launcher->module);
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, $menucontroller);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$brands = $this->catalog_model->getBrandsByEventID($id);
			$categories = $this->catalog_model->getCategoriesByEventID($id);

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('selbrands[]', 'selbrands', '');
				$this->form_validation->set_rules('selcategories[]', 'selcategories', '');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
                }
				$this->form_validation->set_rules('imageurl', 'imageurl', 'trim');
                $this->form_validation->set_rules('cataloggroup', 'cataloggroup', 'trim|required');

				if($this->form_validation->run() == FALSE || ($_POST['cataloggroup'])){
					if(!isset($_GET['cataloggroup'])) {
						$error = __("An item group is required.");
					} else {
						$error = __("Some fields are missing.");
					}
				} else {
					// Save exhibitor
					$data = array(
                            "cataloggroupid"   => set_value('cataloggroup'),
							"eventid"		=> $id,
                            "name"				=> set_value('name_'. $app->defaultlanguage),
                            "description"		=> set_value('description_'.  $app->defaultlanguage),
						);
					$res = $this->general_model->insert('catalog', $data);

					if($res != FALSE){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/catalogimages/".$res)){
							mkdir($this->config->item('imagespath') . "upload/catalogimages/".$res, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/catalogimages/'.$res;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('imageurl')) {
							$image = ""; //No image uploaded!
							$error = $this->upload->display_errors();
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/catalogimages/'. $res . '/' . $returndata['file_name'];
						}

						$this->general_model->update('catalog', $res, array('imageurl' => $image));
						
						// Save brands
						if($this->catalog_model->saveBrands($res, $this->input->post('selbrands'))){
							// Save categories
							if($this->catalog_model->saveCategories($res, $this->input->post('selcategories'))){
                                //add translated fields to translation table
                                foreach($languages as $language) {
                                    //description
                                    $this->language_model->addTranslation('catalog', $res, 'name', $language->key, $this->input->post('name_'.$language->key));
                                    $this->language_model->addTranslation('catalog', $res, 'description', $language->key, $this->input->post('description_'.$language->key));
                                }

								$this->session->set_flashdata('event_feedback', __('The exhibitor has successfully been added'));
								_updateTimeStamp($event->id);
								redirect('catalog/event/'.$event->id);
							} else {
								$error = __("Oops, something went wrong. Please try again.");
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

				}
			}
			
            $groups = $this->catalog_model->getCataloggroupsByEventID($event->id);

			$cdata['content'] 		= $this->load->view('c_catalog_add', array('event' => $event, 'cataloggroups' => $groups, 'brands' => $brands, 'categories' => $categories, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Product Catalog") => "catalog/event/".$event->id, __("Add new") => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function image_rules($str){
		$filename = 'imageurl';
		return image_check($filename, $_FILES);
	}

    function edit($id, $type = 'event', $catalogtype = '', $newgroupid = '', $basegroupid = '') {
		if($catalogtype == 'catalog') {
			$catalogtype = '';
		}
		$basegroup = false;
		if(!empty($basegroupid)) {
			$basegroup = $this->group_model->getById($basegroupid);
		}
		if($type == 'venue') {
			$this->load->library('form_validation');
			$this->load->library('crocodoc'); // Crocodoc API
			$error = "";
			if($catalogtype != '') {
				$this->catalog_model->groupids = '';
			}

			$catalog = $this->catalog_model->getCatalogByID($id);
			if($catalog == FALSE) redirect('venues');
			$venue = $this->venue_model->getbyid($catalog->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			// getting thumbnails of pdf
			foreach($languages as $language){
				$uuid = _getTranslation('catalog', $catalog->id, 'uuid', $language->key);
				if( $uuid ) $catalog->pdfdoc[$language->key] = $this->crocodoc->thumbnail($uuid, 180, 210);
			}
			
			$premium = $this->premium_model->get('catalog', $catalog->id);

			$maincat = $this->catalog_model->getMainCategorieGroup($venue->id);
			if(empty($basegroupid)) {
				$basegroupid = $maincat;
				$basegroup = $this->group_model->getById($basegroupid);
			}
			$brandshtml = '';
			$catshtml = '';
			$brands = array();
			$categories = array();
			$currentCats = '';
			if($newgroupid != '' || $maincat != false) {
				$catsgroupid = $maincat;
				if($catsgroupid) {
					$this->catalog_model->htmlarray = array();
					$catshtml = $this->catalog_model->display_children($basegroupid,0,$catalog->venueid, $catalog->id);
					$currentCats = $this->catalog_model->groupids;
					$this->catalog_model->groupids = '';
				}
			} else {
				$brands = $this->catalog_model->getBrandsByVenueID($catalog->venueid);
				$categories = $this->catalog_model->getCategoriesByVenueID($catalog->venueid);
			}
			
			$catalogid = $this->catalog_model->getCatalogGroup($venue->id);
			if($catalogid && ($catalogtype == '' || $catalogtype == 'catalogs' || $catalogtype == 'catalog')) {
				$maincat = $catalogid;
			}
			// $this->iframeurl = 'venuecatalog/'.$venue->id.'/'.$id;
			if($catalogtype == '') {
				$this->iframeurl = 'catalogs/view/'.$id;
			} else {
				$this->iframeurl = $catalogtype.'/view/'.$id;
			}

			if(($app->apptypeid == 8 || $app->apptypeid == 7 || $app->apptypeid == 4) && !empty($newgroupid)) {
				$launcher = $this->module_mdl->getLauncherOfGroupAndVenue($newgroupid, $venue->id);
				$this->iframeurl = 'catalogs/resto/'.$venue->id.'/'.$newgroupid;
			}

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'catalog' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'catalog'");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag;
				}
				$tags = $tagz;
			}

			$moduletypeid = 15;
			if($catalogtype != '') {
				$moduletypeid = $this->module_mdl->getModuletypeByComponent($catalogtype)->id;
			}
			if(!empty($basegroupid) && $basegroupid != 0) {
				$launcherBase = $this->module_mdl->getLauncherOfGroupAndVenue($basegroupid, $venue->id);
				if($launcherBase) {
					$launcher = $launcherBase;
				} else {
					$launcher = $this->module_mdl->getLauncher($moduletypeid, 'venue', $venue->id);
				}
			} else {
				$launcher = $this->module_mdl->getLauncher($moduletypeid, 'venue', $venue->id);
			}

			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, 'catalog', $id, $app);

			//price field
			if(!empty($newgroupid)) {
				$group = $this->group_model->getById($newgroupid);
				$parents = $this->group_model->getParents($newgroupid);
				foreach($parents as $p) {
					if(strtolower($p->name) == 'menu' || strtolower($group->name) == 'menu') {
						$catalogmodule = 'menu';
					} elseif(strtolower($p->name) == 'team' || strtolower($group->name) == 'team') {
						$catalogmodule = 'team';
					} elseif(strtolower($p->name) == 'gallery' || strtolower($group->name) == 'gallery') {
						$catalogmodule = 'gallery';
					}
				}
			}

			if($launcher && $moduletypeid != 15) {
				$basegroupid = $launcher->groupid;
				$basegroup = $this->group_model->getById($basegroupid);
			}

			if($catalogmodule == 'team' || $catalogmodule == 'menu' || $catalogmodule == 'gallery') {
				if($parents[0]->name == 'catalogcategories') {
					array_shift($parents);
				}
			}
			

			$confbagactive = $this->confbag_model->checkActive($venue->id, 'venue');
			$catalogconfbag = false;
			if($confbagactive) {
				$catalogconfbag = $this->confbag_model->getFromObject('catalog',$catalog->id);
			}

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('selbrands[]', 'selbrands', '');
				$this->form_validation->set_rules('selcategories[]', 'selcategories', '');
				$this->form_validation->set_rules('premium', 'premium', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');
				// $this->form_validation->set_rules('email', 'email', 'trim');
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
				$this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
				$this->form_validation->set_rules('externalid', 'external id', 'trim');
				$this->form_validation->set_rules('confbagcontent', 'confbagcontent', 'trim');
				$this->form_validation->set_rules('groups[]', 'groups', 'trim');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description', 'trim');
                    $this->form_validation->set_rules('urltitle_'.$language->key, 'urltitle_'.$language->key, 'trim');
                    $this->form_validation->set_rules('url_'.$language->key, 'url ('.$language->name.')', 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
				
				if(empty($_POST['groups']) && !empty($newgroupid)) {
					$error .= '<p>'.__("An item group is required.").'</p>';
				}
				if($this->form_validation->run() == FALSE || $error != ''){
					$error .= validation_errors();
				} else {
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/catalogimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/catalogimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/catalogimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('imageurl')) {
						$image = $catalog->imageurl; //No image uploaded!
						if($_FILES['imageurl']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/catalogimages/'. $id . '/' . $returndata['file_name'];

						$photo = $this->config->item('publicupload') . $image;
						$size = getimagesize($photo); 

						if($size[0] > 960) {
							//resize
							$newname = 'upload/catalogimages/'.$id.'/catalogimage'.$returndata['raw_name'].'@2x.png';
							$config['image_library'] = 'gd2';
							$config['source_image'] = $this->config->item('imagespath') . $image;
							$config['maintain_ratio'] = TRUE;
							$config['master_dim'] = 'width';
							$config['width'] = 960;
							$config['height'] = 640;
							$config['new_image'] = $this->config->item('imagespath') . $newname;

							$this->image_lib->initialize($config);

							if (!$this->image_lib->resize())
							{
								//echo $this->image_lib->display_errors();
								if($_FILES['imageurl']['name'] != '') {
									// $error .= $this->image_lib->display_errors();
								}
							} else {
								$this->general_model->update('catalog', $id, array('imageurl' => $newname));
							}
						}
					}
					
					//Uploads Pdf							
					if(!is_dir($this->config->item('imagespath') . "upload/catalogpdf/".$id)){
						mkdir($this->config->item('imagespath') . "upload/catalogpdf/".$id, 0775, true);
					}

					foreach($languages as $language) {
						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								$postfield = _getPostedField($m, $_POST, $_FILES, $language);
								if($postfield !== false) {
									if(_validateInputField($m, $postfield)) {
										_saveInputField($m, $postfield, 'catalog', $id, $app, $language);
									}
								}
							}
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/catalogpdf/'.$id;
						$configexlogo['allowed_types'] = 'PDF|pdf';
						$configexlogo['max_size']  = '2048';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('pdfdoc'.$language->key)) {
							if($_FILES['pdfdoc'.$language->key]['name'] != '') {
								$uploadError .= 'pdf';
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$response = $this->crocodoc->upload( $this->config->item('publicupload') . 'upload/catalogpdf/'.$id.'/'.$returndata['file_name'] );
							
							if($language->key == $app->defaultlanguage) {
								$this->general_model->update('catalog', $id, array(
									'pdf' => 'upload/catalogpdf/'.$id.'/'.$returndata['file_name'],
									'uuid' => isset( $response['uuid'] ) ? $response['uuid'] : ''
									));
								$pdf = 'upload/catalogpdf/'.$id.'/'.$returndata['file_name'];
								$uuid = isset( $response['uuid'] ) ? $response['uuid'] : '';
							}

							$pdf1Success = $this->language_model->updateTranslation('catalog', $id, 'pdf', $language->key, 'upload/catalogpdf/'.$id.'/'.$returndata['file_name']);
							if(!$pdf1Success) {
								$this->language_model->addTranslation('catalog', $id, 'pdf', $language->key, 'upload/catalogpdf/'.$id.'/'.$returndata['file_name']);
							}
							$pdf2Success = $this->language_model->updateTranslation('catalog', $id, 'uuid', $language->key, isset( $response['uuid'] ) ? $response['uuid'] : '');
							if(!$pdf2Success) {
								$this->language_model->addTranslation('catalog', $id, 'uuid', $language->key, isset( $response['uuid'] ) ? $response['uuid'] : '');
							}
						}
					}
					
					$item = $this->db->query("SELECT * FROM prices WHERE iditem = $id");
					$price = $this->input->post("price");
					
					if(empty($price))
					{
						$price = 0.0;
					}
					
					$arr = array('iditem' => $id,'price' => $price);
					
					if($item->num_rows() > 0)
					{
						$this->db->query("UPDATE prices SET price=$price WHERE iditem=$id");
					}
					else
					{
						$this->db->insert('prices', $arr);
					}
					$url = $this->input->post('url_'.$app->defaultlanguage);
					if(!stristr($url, 'http') && !empty($url)) {
						$url = 'http://'.$this->input->post('url');
					}
					$data = array(
							"name"			=> set_value('name_'.  $app->defaultlanguage),
							"description"	=> $this->input->post('description_'. $app->defaultlanguage),
							"imageurl"		=> $image,
							'url' 			=> $url,
							"order"			=> $this->input->post('order'),
							'externalid'	=> $this->input->post('externalid'),
							'urltitle'		=> set_value('urltitle_'. $app->defaultlanguage)
						);
						// no groups for services, careers and projects
					if((isset($groupid) && $groupid != 0) || $maincat != false && $catalogtype == '') {
						$this->catalog_model->deleteGroupItemsFromCatalog($catalog->id);
						//groupitems
						if($this->input->post('groups') != null && $this->input->post('groups') != '') {
							$groups = $this->input->post('groups');
							$this->db->query("DELETE FROM groupitem WHERE itemid = $catalog->id AND itemtable = 'catalog'");
							foreach($groups as $group) {
								if($group != '') {
									$group2 = $this->group_model->getById($group);
									$groupitemdata = array(
											'appid'	=> $app->id,
											'venueid'	=> $catalog->venueid,
											'groupid'	=> $group2->id,
											'itemtable'	=> 'catalog',
											'itemid'	=> $catalog->id	,
											'displaytype' => $group2->displaytype
										);
									$this->general_model->insert('groupitem', $groupitemdata);
								}
							}
						}
					} else {
						// Save brands
						$this->catalog_model->saveBrands($id, $this->input->post('selbrands'));
						// Save categories
						$this->catalog_model->saveCategories($id, $this->input->post('selcategories'));
					}

					if($catalogtype != '') {
						$cataloggroupid = $this->db->query("SELECT id FROM cataloggroup WHERE venueid = $venue->id AND name = '$catalogtype'");
						if($cataloggroupid->num_rows() != 0) {
							$cataloggroupid = $cataloggroupid->row()->id;
						} else {
							$groupdata = array('venueid' => $venue->id ,'name' => $catalogtype);
							$cataloggroupid = $this->general_model->insert('cataloggroup', $groupdata);
						}
						$data['cataloggroupid'] = $cataloggroupid;
					} else {
						$data['cataloggroupid'] = set_value('cataloggroup');
					}

					if($this->general_model->update('catalog', $id, $data)){
						// *** SAVE TAGS *** //
						if(!empty($id)) {
							$this->db->where('catalogitemid', $id);
							$this->db->where('appid', $app->id);
							if($this->db->delete('tag')){
								if ($this->input->post('mytagsulselect')) {
									$this->db->where('itemtype', 'catalog');
									$this->db->where('itemid', $id);
									$this->db->delete('tc_tag');
									$tags = $this->input->post('mytagsulselect');
									foreach ($tags as $tag) {
										$this->general_model->insert('tc_tag', array(
											'appid' => $app->id,
											'itemtype' => 'catalog',
											'itemid' => $id,
											'tag' => $tag
										));
									}
								}
							}
						}

						//confbag
						if($confbagactive) {
							if(!is_dir($this->config->item('imagespath') . "upload/confbagfiles/venue/".$venue->id)){
								mkdir($this->config->item('imagespath') . "upload/confbagfiles/venue/".$venue->id, 0775, TRUE);
							}
							$configconfbag['upload_path'] = $this->config->item('imagespath') .'upload/confbagfiles/venue/'.$venue->id;
							$configconfbag['allowed_types'] = 'JPG|jpg|jpeg|png|pdf|xls|xlsx|txt|doc|docx|ppt|pptx';
							$configconfbag['max_size'] = '50000';
							$this->load->library('upload', $configconfbag);

							$this->upload->initialize($configconfbag);
							if ($this->upload->do_upload('confbagcontent')) {
								$returndata = $this->upload->data();
								$confbagcontent = 'upload/confbagfiles/venue/'.$venue->id.'/'.$returndata['file_name'];
								$this->general_model->insert('confbag', array('venueid' => $venue->id, 'itemtable' => 'catalog', 'tableid' => $id, 'documentlink' => $confbagcontent));
							} else {
								if($_FILES['confbagcontent']['name'] != '') {
									$error .= $this->upload->display_errors();
								}
							}
						}
						
						//save premium
						$this->premium_model->save($this->input->post('premium'), 'catalog', $id, $this->input->post('extraline'), $premium, 'venue', $venue->id, $this->input->post('premiumorder'));
						//email
						// $this->metadata_model->removeKey('email', 'catalog', $id);
						// $email = $this->input->post('email');
						// if(!empty($email)) {
						// 	$this->general_model->insert('metadata', array('appid' => $app->id, '`table`' => 'catalog', 'identifier' => $id, '`key`' => 'email', '`value`' => $this->input->post('email')));
						// }

                        //add translated fields to translation table
                        foreach($languages as $language) {
							$nameSuccess = $this->language_model->updateTranslation('catalog', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
							if(!$nameSuccess) {
								$this->language_model->addTranslation('catalog', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
							}
							$descrSuccess = $this->language_model->updateTranslation('catalog', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							if(!$descrSuccess) {
								$this->language_model->addTranslation('catalog', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							}
							$urltitleSuccess = $this->language_model->updateTranslation('catalog', $id, 'urltitle', $language->key, $this->input->post('urltitle_'.$language->key));
							if(!$urltitleSuccess) {
								$this->language_model->addTranslation('catalog', $id, 'urltitle', $language->key, $this->input->post('urltitle_'.$language->key));
							}
							$urlSuccess = $this->language_model->updateTranslation('catalog', $id, 'url', $language->key, $this->input->post('url_'.$language->key));
							if(!$urlSuccess) {
								$this->language_model->addTranslation('catalog', $id, 'url', $language->key, $this->input->post('url_'.$language->key));
							}
                        }
						$this->session->set_flashdata('event_feedback', __('The catalog has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						if($newgroupid != '') {
							redirect('groups/view/'.$newgroupid.'/venue/'.$venue->id.'/catalog');
						} else {
							redirect('catalog/venue/'.$venue->id.'/0/'.$catalogtype);
						}

					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			
			$item = $this->db->query("SELECT * FROM prices WHERE iditem=$id")->row();
			
            $groups = $this->catalog_model->getCataloggroupsByVenueID($venue->id);

			$breadcrumbtype = 'Product catalog';
			if($catalogtype != '') {
				$breadcrumbtype = ucfirst($catalogtype);
			}

			$cdata['content'] 		= $this->load->view('c_catalog_edit', array('venue' => $venue, 'cataloggroups' => $groups, 'catalog' => $catalog, 'brands' => $brands, 'categories' => $categories, 'error' => $error, 'languages' => $languages, 'app' => $app, 'catalogtype' => $catalogtype, 'tags' => $tags, 'newgroupid' => $newgroupid, 'maincat' => $maincat, 'catshtml' => $catshtml, 'currentCats' => $currentCats, 'apptags' => $apptags, 'premium' => $premium, 'item' => $item, 'metadata' => $metadata, 'basegroup' => $basegroup, 'metadata' => $metadata, 'catalogmodule' => $catalogmodule, 'confbagactive' => $confbagactive, 'catalogconfbag' => $catalogconfbag), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);

			if($basegroup) {
				$crumbarray = array($venue->name => 'venue/view/'.$venue->id);
				$basegroup->name = str_replace('catalogcategories', 'Catalog', $basegroup->name);
				// $crumbarray[$basegroup->name] = 'groups/view/'.$basegroup->id.'/venue/'.$venue->id.'/catalog';
				$currentCats = explode('/', $currentCats);
				foreach($parents as $p) {
					$p->name = str_replace('catalogcategories', 'Catalog', $p->name);
					if(strtolower($p->name) != 'catalogcategories' && strtolower($p->name != 'catalog')) {
						if(strtolower($p->name) == 'menu') {
							$launcher = $this->module_mdl->getLauncher(50, 'venue', $venue->id);
							$crumbarray[$launcher->title] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/catalog';
						} elseif(strtolower($p->name) == 'gallery') {
							$launcher = $this->module_mdl->getLauncher(51, 'venue', $venue->id);
							$crumbarray[$launcher->title] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/catalog';
						} elseif(strtolower($p->name) == 'team') {
							$launcher = $this->module_mdl->getLauncher(52, 'venue', $venue->id);
							$crumbarray[$launcher->title] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/catalog';
						} else {
							$crumbarray[$p->name] = 'groups/view/'.$p->id.'/venue/'.$venue->id.'/catalog';
						}
					}
				}
				$group->name = str_replace('catalogcategories', 'Catalog', $group->name);
				$crumbarray[$group->name] = 'groups/view/'.$group->id.'/venue/'.$venue->id.'/catalog';
				$crumbarray[__("Edit: ") . $catalog->name] = $this->uri->uri_string();
				$cdata['crumb']			= checkBreadcrumbsVenue($app, $crumbarray);
			} else {
				$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "catalog/venue/".$venue->id.'/0/'.$catalogtype, __("Edit: ") . $catalog->name => $this->uri->uri_string()));
			}
			$menucontroller = $catalogtype;
			if(empty($catalogtype)) {
				$menucontroller = 'catalog';
			}
			if($launcher) {
				$menucontroller = strtolower($launcher->module);
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, $menucontroller);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			// EDIT CATALOG
			$catalog = $this->catalog_model->getCatalogByID($id);
			if($catalog == FALSE) redirect('events');

			$brands = $this->catalog_model->getBrandsByEventID($catalog->eventid);
			$categories = $this->catalog_model->getCategoriesByEventID($catalog->eventid);

			$event = $this->event_model->getbyid($catalog->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('selbrands[]', 'selbrands', '');
				$this->form_validation->set_rules('selcategories[]', 'selcategories', '');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
                }
                $this->form_validation->set_rules('cataloggroup', 'cataloggroup', 'trim|required');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/catalogimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/catalogimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/catalogimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = $catalog->image; //No image uploaded!
						$error = $this->upload->display_errors();
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/catalogimages/'. $id . '/' . $returndata['file_name'];
					}

					// Save brands
					if($this->catalog_model->saveBrands($id, $this->input->post('selbrands'))){
						// Save categories
						if($this->catalog_model->saveCategories($id, $this->input->post('selcategories'))){
						// Save exhibitor
							$data = array(
                                    "cataloggroupid"   => set_value('cataloggroup'),
                                    "name"				=> set_value('name_'.  $app->defaultlanguage),
                                    "description"		=> set_value('description_'.  $app->defaultlanguage),
									"imageurl"		=> $image
								);

							if($this->general_model->update('catalog', $id, $data)){
                                //add translated fields to translation table
                                foreach($languages as $language) {
									$nameSuccess = $this->language_model->updateTranslation('catalog', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
									if(!$nameSuccess) {
										$this->language_model->addTranslation('catalog', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
									}
									$descrSuccess = $this->language_model->updateTranslation('catalog', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
									if(!$descrSuccess) {
										$this->language_model->addTranslation('catalog', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
									}
                                }

								$this->session->set_flashdata('event_feedback', __('The catalog has successfully been updated'));
								_updateTimeStamp($event->id);
								redirect('catalog/event/'.$event->id);
							} else {
								$error = __("Oops, something went wrong. Please try again.");
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
            $groups = $this->catalog_model->getCataloggroupsByVenueID($event->id);

			$cdata['content'] 		= $this->load->view('c_catalog_edit', array('event' => $event, 'cataloggroups' => $groups, 'catalog' => $catalog, 'brands' => $brands, 'categories' => $categories, 'error' => $error, 'languages' => $languages, 'app' => $app, 'catalogtype' => $catalogtype, 'item' => $arr), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Catalogs") => "catalog/event/".$event->id, __("Edit: " ). $catalog->name => $this->uri->uri_string());
			$this->load->view('master', $cdata);
		}

	}

	function delete($id, $type = 'event', $catalogtype) {
        $this->language_model->removeTranslations('catalog', $id);
		if($type == 'venue') {
			$catalog = $this->catalog_model->getCatalogByID($id);
			$venue = $this->venue_model->getbyid($catalog->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
            $this->load->model('groupitem_model');
            $this->groupitem_model->removeGroupitemsOfObject('catalog', $id);
			if($this->general_model->remove('catalog', $id)){
				$this->catalog_model->removePremium($id);
				$this->db->delete('catebrand', array('catalogid' => $id));
				$this->db->delete('catecat', array('catalogid' => $id));
				$this->session->set_flashdata('event_feedback', __('The catalog has successfully been deleted.'));
				_updateVenueTimeStamp($catalog->venueid);
				redirect('catalog/venue/'.$catalog->venueid.'/0/'.$catalogtype);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('catalog/venue/'.$catalog->venueid.'/0/'.$catalogtype);
			}
		} else {
			$event = $this->event_model->getbyid($catalog->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$catalog = $this->catalog_model->getCatalogByID($id);
			if($this->general_model->remove('catalog', $id)){
				$this->db->delete('catebrand', array('catalogid' => $id));
				$this->db->delete('catecat', array('catalogid' => $id));
				$this->session->set_flashdata('event_feedback', __('The catalog has successfully been deleted.'));
				_updateTimeStamp($catalog->eventid);
				redirect('catalog/event/'.$catalog->eventid.'/0/'.$catalogtype);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('catalog/event/'.$catalog->eventid.'/0/'.$catalogtype);
			}
		}
	}

    function crop($id, $type = 'event', $selection = '') {
		if($selection == '') {
			$selectionName = 'Product catalog';
		} else {
			$selectionName = $selection;
		}
        $error = '';
        $catalog = $this->catalog_model->getCatalogByID($id);
        if($catalog == FALSE) redirect($type . 's');
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($catalog->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
		} else {
            $event = $this->event_model->getbyid($catalog->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
		}

		$this->iframeurl = 'catalogs/view/'.$id;

        $catalogpicwidth = $this->general_model->get_meta_data_by_key('app', $app->id, 'catalogpicwidth');
        $catalogpicheight = $this->general_model->get_meta_data_by_key('app', $app->id, 'catalogpicheight');
        if(isset($catalogpicwidth) && $catalogpicwidth != false) {
        	$catalogpicwidth = $catalogpicwidth->value;
        } else {
        	$catalogpicwidth = 0;
        }
        if(isset($catalogpicheight) && $catalogpicheight != false) {
        	$catalogpicheight = $catalogpicheight->value;
        } else {
        	$catalogpicheight = 0;
        }

		if($app->apptypeid == 4 || $app->apptypeid == 11) {
			// $catalogpicwidth = 640;
			// $catalogpicheight = 360;
		}

        if($this->input->post('postback') == "postback") {
           $w = $this->input->post('w');
           $h = $this->input->post('h');
           $x = $this->input->post('x');
           $y = $this->input->post('y');

		   $newname = 'upload/catalogimages/'.$catalog->id.'/cropped_'.time().'.jpg';
           $config['image_library']  = 'gd2';
           $config['source_image']  = $this->config->item('imagespath') . $catalog->imageurl;
           $config['create_thumb']  = FALSE;
           $config['maintain_ratio']  = FALSE;
           $config['width']    = $w;
           $config['height']   = $h;
           $config['x_axis']   = $x;
           $config['y_axis']   = $y;
           $config['dynamic_output']  = FALSE;
           $config['new_image'] = $this->config->item('imagespath') . $newname;

           $this->load->library('image_lib', $config);
           $this->image_lib->initialize($config);

           if (!$this->image_lib->crop())
           {
			   $newname = $catalog->imageurl;
			   echo $this->image_lib->display_errors();
           }

			$data = array(
					"imageurl"	=> $newname
				);

			$this->general_model->update('catalog', $catalog->id, $data);
			$catalog->imageurl = $newname;
        }

        if($type == 'venue') {
            $venue = $this->venue_model->getbyid($catalog->venueid);
            $cdata['content'] 		= $this->load->view('c_catalog_crop', array('venue' => $venue, 'catalog' => $catalog, 'error' => $error, 'catalogpicheight' => $catalogpicheight, 'catalogpicwidth' => $catalogpicwidth), TRUE);
            $cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $selectionName => "catalog/venue/".$venue->id."/0/".$selection, __("Edit Picture: ") . $catalog->name => $this->uri->uri_string()));
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
            $this->load->view('master', $cdata);
        }
    }

	function rss($id, $type = 'event', $catalogtype = '') {
        $error = "";
        if($type == 'venue'){
			$venue = $this->venue_model->getbyid($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

            $currentRSSfeeds = $this->catalog_model->getRssOfVenue($id);
			$groups = $this->catalog_model->getCataloggroupsByVenueID($venue->id);

			if($catalogtype == '') {
				$this->iframeurl = 'catalogs/venue/'.$venue->id;
			} else {
				$this->iframeurl = $catalogtype.'/venue/'.$venue->id;
			}

            if($this->input->post('postback') == "postback") {
                $this->load->library('form_validation');
				$this->form_validation->set_rules('url', 'url', 'trim|required');
                $this->form_validation->set_rules('refreshrate', 'refreshrate', 'trim|numeric');
				$this->form_validation->set_rules('cataloggroup', 'cataloggroup', 'trim|required');
				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
                    $url = $this->input->post('url');
                    $content = file_get_contents($url);
                    $rss = new SimpleXmlElement($content);

                    $catalogsourceData = array(
                        "venueid"   => $venue->id,
                        "type"      => "rss",
                        "url"       => $url,
                        "timestamp" => date("Y-m-d H:i:s",time()),
                        "refreshrate" => $this->input->post('refreshrate')
                    );
                    $sourceid = $this->general_model->insert('catalogsource', $catalogsourceData);

                    if($content!= null && $rss != null && !empty($content) && !empty($rss) && $sourceid != null) {
                        foreach($rss->channel->item as $post) {
                            $data = array(
									"cataloggroupid"=> set_value('cataloggroup'),
                                    "name" 	=> (string)$post->title,
                                    "venueid" 	=> $venue->id,
                                    "description" 		=> (string)$post->description,
                                    "url"		=> (string)$post->link,
                                    "sourceid"  => $sourceid,
									"type"		=> $catalogtype
                                );
                            if($id = $this->general_model->insert('catalog', $data)) {
                                //add translated fields to translation table
                                if($languages != null) {
                                    foreach($languages as $language) {
                                        $this->language_model->addTranslation('catalog', $id, 'name', $language->key, (string)$post->title);
                                        $this->language_model->addTranslation('catalog', $id, 'description', $language->key, (string)$post->description);
                                    }
                                }
                            } else {
                                $error = __("Oops, something went wrong. Please try again.");
                            }
                        }
                    } else {
                        $error = __("Oops, something went wrong. Please try again.");
                    }

                    if($error == '') {
                        $this->session->set_flashdata('event_feedback', __('The RSS has successfully been imported'));
                        _updateTimeStamp($venue->id);
                        redirect('catalog/venue/'.$venue->id.'/0/'.$catalogtype);
                    }
                }
            }
            $cdata['content'] 		= $this->load->view('c_catalog_rss', array('venue' => $venue, 'cataloggroups' => $groups, 'error' => $error, 'currentRSSfeeds' => $currentRSSfeeds), TRUE);
            $cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Product catalog") => "catalog/venue/".$venue->id.'/0/'.$catalogtype, __("Import RSS") => $this->uri->uri_string()));
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
            $this->load->view('master', $cdata);
        } else {
            $event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$currentRSSfeeds = $this->catalog_model->getRssOfEvent($id);
			$groups = $this->catalog_model->getCataloggroupsByEventID($event->id);

			$languages = $this->language_model->getLanguagesOfApp($app->id);

            $this->iframeurl = "news/by-event/" . $event->id;

            if($this->input->post('postback') == "postback") {
                $this->load->library('form_validation');
				$this->form_validation->set_rules('url', 'url', 'trim|required');
                $this->form_validation->set_rules('refreshrate', 'refreshrate', 'trim|numeric');
				$this->form_validation->set_rules('cataloggroup', 'cataloggroup', 'trim|required');
				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
                    $url = $this->input->post('url');
                    $content = file_get_contents($url);
                    $rss = new SimpleXmlElement($content);

                    $catalogsourceData = array(
                        "eventid"   => $event->id,
                        "type"      => "rss",
                        "url"       => $url,
                        "timestamp" => date("Y-m-d H:i:s",time()),
                        "refreshrate" => $this->input->post('refreshrate')
                    );
                    $sourceid = $this->general_model->insert('catalogsource', $catalogsourceData);

                    if($content!= null && $rss != null && !empty($content) && !empty($rss) && $sourceid != null) {
                        foreach($rss->channel->item as $post) {
                            $data = array(
									"cataloggroupid"=> set_value('cataloggroup'),
                                    "name" 	=> (string)$post->title,
                                    "eventid" 	=> $event->id,
                                    "description" 	=> (string)$post->description,
                                    "url"		=> (string)$post->link,
                                    "sourceid"  => $sourceid
                                );
                            if($id = $this->general_model->insert('newsitem', $data)) {
                                //add translated fields to translation table
                                if($languages != null) {
                                    foreach($languages as $language) {
                                        $this->language_model->addTranslation('catalog', $id, 'name', $language->key, (string)$post->title);
                                        $this->language_model->addTranslation('catalog', $id, 'description', $language->key, (string)$post->description);
                                    }
                                }
                            } else {
                                $error = __("Oops, something went wrong. Please try again.");
                            }
                        }
                    } else {
                        $error = __("Oops, something went wrong. Please try again.");
                    }

                    if($error == '') {
                        $this->session->set_flashdata('event_feedback', __('The RSS has successfully been imported'));
                        _updateTimeStamp($event->id);
                        redirect('catalog/event/'.$event->id.'/0/'.$catalogtype);
                    }
                }
            }
            $cdata['content'] 		= $this->load->view('c_catalog_rss', array('event' => $event, 'cataloggroups' => $groups, 'error' => $error, 'currentRSSfeeds' => $currentRSSfeeds), TRUE);
            $cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Product Catalog") => "catalog/event/".$event->id, __("Import RSS") => $this->uri->uri_string());
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
            $this->load->view('master', $cdata);
        }
    }

    function deletesource($sourceid, $catalogtype = '') {
        $source = $this->catalog_model->getSourceOfId($sourceid);
        $this->catalog_model->removeCatalogsOfSourceId($sourceid);

        if($this->general_model->remove('catalogsource', $sourceid)){
            if($source->venueid != '' && $source->venueid != '0') {
                $this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
                _updateVenueTimeStamp($source->venueid);
                redirect('catalog/venue/'.$source->venueid . '/0/'.$catalogtype);
            } else {
                $this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
                _updateTimeStamp($source->eventid);
                redirect('catalog/event/'.$source->eventid . '/0/'.$catalogtype);
            }
        } else {
            if($source->venueid != ''  && $source->venueid != '0') {
                $this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
                redirect('catalog/venue/'.$source->venueid . '/0/'.$catalogtype);
            }
            else {
                $this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
                redirect('catalog/event/'.$source->eventid . '/0/'.$catalogtype);
            }
        }
    }

	function removeimage($id, $type = 'event') {
		$catalog = $this->catalog_model->getCatalogByID($id);
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($catalog->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $catalog->imageurl)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $catalog->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('catalog', $id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('catalog/venue/'.$venue->id);
		} else {
			$event = $this->event_model->getbyid($catalog->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $catalog->imageurl)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $catalog->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('catalog', $id, $data);

			_updateTimeStamp($event->id);
			redirect('catalog/event/'.$event->id);
		}
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'catalog');
    }   
	
	function removepdf($id, $langKey, $type = 'event') {
		$catalog = $this->catalog_model->getCatalogByID($id);
		
		$this->load->library('form_validation');
		$this->load->library('crocodoc'); // Crocodoc API
		$transUUID = _getTranslation('catalog', $catalog->id, 'uuid', $langKey);
		$transPdf = _getTranslation('catalog', $catalog->id, 'pdf', $langKey);
		$del = $this->crocodoc->delete( $transUUID );
		
		if( $del ){
			$this->language_model->removeTranslationByFieldName('catalog', $catalog->id, $langKey, 'uuid');
			$this->db->last_query();
			$this->language_model->removeTranslationByFieldName('catalog', $catalog->id, $langKey, 'pdf');
			unlink($this->config->item('imagespath') . $transPdf);
			$this->session->set_flashdata('event_feedback', __('Pdf has successfully been removed.'));
		}
		else
			$this->session->set_flashdata('event_feedback', __('Pdf has not successfully been removed.'));
		
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($catalog->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			
			if( $del ){
				if( $langKey == $app->defaultlanguage ){
					$data = array('uuid' => '', 'pdf' => '');
					$this->general_model->update('catalog', $catalog->id, $data);
				}				
				_updateVenueTimeStamp($venue->id);
			}
			
			redirect('catalog/venue/'.$venue->id);
		} else {
			$event = $this->event_model->getbyid($catalog->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			
			if( $del ){
				if( $langKey == $app->defaultlanguage ){
					$data = array('uuid' => '', 'pdf' => '');
					$this->general_model->update('catalog', $catalog->id, $data);
				}

				_updateTimeStamp($event->id);
			}
			
			redirect('catalog/event/'.$event->id);
		}
	}
}
