<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Formtemplate extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
			$this->load->model('forms_model');
		$this->load->model('module_mdl');
	}
	
	//php 4 constructor
	function Formtemplate() {
		parent::__construct();
		if(_authed()) { }
			$this->load->model('module_mdl');
	}
	
	function index() {
		redirect('events');
	}

	function add($id, $moduletypeid, $email = '', $type = 'event')
	{
		if($email == '--email--') {
			$email = '';
		}
		$this->load->model('formtemplate_model');
		$templateform = $this->formtemplate_model->getFormTemplate($moduletypeid);
		
		$templateformid				= $templateform[0]->id;
		$formtitle 					= $templateform[0]->title;
		$emailsendresult 			= $templateform[0]->emailsendresult;
		$allowmutliplesubmissions   = $templateform[0]->allowmutliplesubmissions;
		$submissionbuttonlabel 		= $templateform[0]->submissionbuttonlabel;
		$submissionconfirmationtext	= $templateform[0]->submissionconfirmationtext;
		$singlescreen 				= $templateform[0]->singlescreen;
		$getgeolocation 			= $templateform[0]->getgeolocation;
		$moduletypeid 				= $templateform[0]->moduletypeid;
		
		$order = 0;

		$launcherorder = 0;
		$defaultlauncher = $this->module_mdl->getDefaultLauncher($moduletypeid);
		if($defaultlauncher) {
			$launcherorder = $defaultlauncher->order;
		}

		$formscreens = $this->formtemplate_model->getFormTemplateScreen($templateformid);

		$formfieldsarray = array();

		$formfields = $this->formtemplate_model->getFormTemplateControls($templateformid);
		foreach($formfields as $field){
			$formfieldsarray[] = array(
				"formscreenid"      => $field->formscreenid,
				"formfieldtypeid"   => $field->formfieldtypeid,
				"label"             => $field->label,
				"possiblevalues"    => $field->possiblevalues,
				"defaultvalue"      => $field->defaultvalue,
				"required"          => $field->required,
				"order"             => $field->order
				);
		}

		if($type == 'event')
		{
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

            //-------------- Addd to Launcher --------------//
			if($app->themeid != 0) {
				//use theme icon
				$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = $moduletypeid LIMIT 1");
				if($icon->num_rows() != 0) {
					$icon = $icon->row();
					$icon = $icon->icon;
					$image = $this->config->item('imagespath') . $icon;
					$newpath = 'upload/appimages/'.$app->id .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
					if(file_exists($image)) {
						if(!is_dir($this->config->item('imagespath') . 'upload/appimages/'.$app->id)){
							mkdir($this->config->item('imagespath') . 'upload/appimages/'.$app->id, 0775,true);
						}

						if(copy($image, $this->config->item('imagespath') . $newpath)) {
							$defaultlauncher->icon = $newpath;
						}
					}
				}
			}

			$data = array(
				'appid'         => 0,
				'eventid'       => $id,
				'title'         => $formtitle,
				'module'        => 'forms',
				'url'           => '',
				'moduletypeid'  => $moduletypeid,
				'order'         => $launcherorder,
				'active'        => 1,
				'templateformid' => $templateformid,
				'icon'			=> $defaultlauncher->icon
				);

			if($launcherid = $this->general_model->insert('launcher', $data)) 
			{

				/*// Count Event Launchers of this ModuleType...
                $eventlaunchers = $this->event_model->countLauncherFromEventModule($event->id, $moduletypeid);
                if($eventlaunchers){
                    $totalmodule = $this->module_mdl->countModuleFromEventModule($event->id, $moduletypeid);
                    if(!$totalmodule){
                        //Add to Module
                        $this->module_mdl->addModuleToEvent($event->id, $moduletypeid);
                    }

                }*/

                
				$this->general_model->insert('module', array(
					'moduletype'	=> $moduletypeid,
					'event'			=> $id,
					'app'			=> 0,
					'venue'			=> 0	
				));
				
                //-------------------------------------------//

				foreach($languages as $language) {
					$this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $formtitle);
				}

                //-------------- Addd to Forms --------------//																	
				$data = array(
					"appid"                         => $app->id,
					"eventid"                       => $id,
					"venueid"                       => 0,
					"launcherid"                    => $launcherid,
					"title"                         => $formtitle,
					"submissionbuttonlabel"         => $submissionbuttonlabel,
					"submissionconfirmationtext"    => $submissionconfirmationtext,
					"email"                         => $email,
					"emailsendresult"               => $emailsendresult,
					"allowmutliplesubmissions"      => $allowmutliplesubmissions,
					"order"                         => $order,
					"getgeolocation"                => $getgeolocation,
					"singlescreen"					=> $singlescreen
					);

				if($newid = $this->general_model->insert('form', $data)){

                    //add translated fields to translation table
					foreach($languages as $language) {
				                        //description

						$this->language_model->addTranslation('form', $newid, 'title', $language->key, $formtitle);
						$this->language_model->addTranslation('form', $newid, 'submissionbuttonlabel', $language->key, $submissionbuttonlabel);
						$this->language_model->addTranslation('form', $newid, 'submissionconfirmationtext', $language->key, $submissionconfirmationtext);
					}						

                    //-------------- Addd to Tags --------------//

					$data = array(
						'appid'	=> $app->id,
						'eventid'	=> $id,
						'formid'	=> $newid,
						'tag'       => $formtitle,
						'active'	=> 1
						);

					$tagid = $this->general_model->insert('tag', $data);
					foreach($languages as $language) {
						$this->language_model->addTranslation('tag', $tagid, 'tag', $language->key, $formtitle);
					}

                    //-------------- --------------- --------------//

                    // Adding a default screen 

					foreach($formscreens as $screen){

						$data = array(
							"formid" => $newid,
							"title" => $screen->title,
							"order"	=> 0,
							"visible" => 1
							);

						if($newscreenid = $this->general_model->insert('formscreen', $data)){
				                            ////add translated fields to translation table
							foreach($languages as $language) {							
								$this->language_model->addTranslation('formscreen', $newscreenid, 'title', $language->key, $screen->title);
							}

				                            // Adding form fields

							foreach($formfieldsarray as $data){
								if($data['formscreenid'] == $screen->id){
									$data['formscreenid'] = $newscreenid;
									if($newfieldid = $this->general_model->insert('formfield', $data)){					
				                                        //add translated fields to translation table
										foreach($languages as $language)
										{
											$this->language_model->addTranslation('formfield', $newfieldid, 'label', $language->key, $data['label']);
											$this->language_model->addTranslation('formfield', $newfieldid, 'possiblevalues', $language->key, $data['possiblevalues']);
											$this->language_model->addTranslation('formfield', $newfieldid, 'defaultvalue', $language->key, $data['defaultvalue']);
										}

										if(!empty($data['possiblevalues'])) {
											//use new form options system
											$options = explode(',', $data['possiblevalues']);
											$isdefault = 1;
											foreach($options as $o) {
												$this->general_model->insert('formfieldoption', array(
														'formfieldid' => $newfieldid,
														'value' => $o,
														'isdefault' => $isdefault,
														'formflow_nextscreenid' => 0
													));
												$isdefault = 0;
											}

											$this->general_model->update('formfield', $newfieldid, array(
													'possiblevalues' => ''
												));
										}

									}
								}
							}

						}
					}
					$this->session->set_flashdata('event_feedback', __('The form has successfully been add!'));
					_updateTimeStamp($event->id);
					redirect('event/view/'.$event->id);
				} 
				else {
					$error = __("Oops, something went wrong. Please try again.");
				}

				}
				}

				elseif ($type == 'app')
				{
					$app = $this->app_model->get($id);
					
					$languages = $this->language_model->getLanguagesOfApp($app->id);

				           //-------------- Addd to Launcher --------------//

					$data = array(
						'appid'         => $app->id,                    
						'title'         => $formtitle,
						'module'        => 'forms',
						'url'           => '',
						'moduletypeid'  => $moduletypeid,
						'order'         => $launcherorder,
						'active'        => 1,
						'templateformid' => $templateformid
						);

					if($launcherid = $this->general_model->insert('launcher', $data)) 
					{
				               /* // Count Event Launchers of this ModuleType...
				                $eventlaunchers = $this->event_model->countLauncherFromEventModule($event->id, $moduletypeid);
				                if($eventlaunchers){
				                    $totalmodule = $this->module_mdl->countModuleFromEventModule($event->id, $moduletypeid);
				                    if(!$totalmodule){
				                        //Add to Module
				                        $this->module_mdl->addModuleToEvent($event->id, $moduletypeid);
				                    }

				                }*/

				                /*
								$this->general_model->insert('module', array(
									'moduletype'	=> $moduletypeid,
									'event'			=> 0,
									'app'			=> $app->id,
									'venue'			=> 0	
								));
								*/
				                //-------------------------------------------//

				foreach($languages as $language) {
					$this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $formtitle);
				}

                //-------------- Addd to Forms --------------//																	
				$data = array(
					"appid"                         => $app->id,
					"eventid"                       => 0,
					"venueid"                       => 0,
					"launcherid"                    => $launcherid,
					"title"                         => $formtitle,
					"submissionbuttonlabel"         => $submissionbuttonlabel,
					"submissionconfirmationtext"    => $submissionconfirmationtext,
					"email"                         => $email,
					"emailsendresult"               => $emailsendresult,
					"allowmutliplesubmissions"      => $allowmutliplesubmissions,
					"order"                         => $order,
					"getgeolocation"                => $getgeolocation,
					"singlescreen"					=> $singlescreen
					);

				if($newid = $this->general_model->insert('form', $data)){

                    //add translated fields to translation table
					foreach($languages as $language) {
				                        //description

						$this->language_model->addTranslation('form', $newid, 'title', $language->key, $formtitle);
						$this->language_model->addTranslation('form', $newid, 'submissionbuttonlabel', $language->key, $submissionbuttonlabel);
						$this->language_model->addTranslation('form', $newid, 'submissionconfirmationtext', $language->key, $submissionconfirmationtext);
					}						

                    //-------------- Addd to Tags --------------//

					$data = array(
						'appid'	=> $app->id,
						'eventid'	=> 0,
						'formid'	=> $newid,
						'tag'       => $formtitle,
						'active'	=> 1
						);

					$tagid = $this->general_model->insert('tag', $data);
					foreach($languages as $language) {
						$this->language_model->addTranslation('tag', $tagid, 'tag', $language->key, $formtitle);
					}

                    //-------------- --------------- --------------//

                    // Adding a default screen 

					foreach($formscreens as $screen){

						$data = array(
							"formid" => $newid,
							"title" => $screen->title,
							"order"	=> 0,
							"visible" => 1
							);

						if($newscreenid = $this->general_model->insert('formscreen', $data)){
				                            ////add translated fields to translation table
							foreach($languages as $language) {							
								$this->language_model->addTranslation('formscreen', $newscreenid, 'title', $language->key, $screen->title);
							}

				                            // Adding form fields

							foreach($formfieldsarray as $data){
								if($data['formscreenid'] == $screen->id){
									$data['formscreenid'] = $newscreenid;
									if($newfieldid = $this->general_model->insert('formfield', $data)){					
				                                        //add translated fields to translation table
										foreach($languages as $language)
										{
											$this->language_model->addTranslation('formfield', $newfieldid, 'label', $language->key, $data['label']);
											$this->language_model->addTranslation('formfield', $newfieldid, 'possiblevalues', $language->key, $data['possiblevalues']);
											$this->language_model->addTranslation('formfield', $newfieldid, 'defaultvalue', $language->key, $data['defaultvalue']);
										}

									}
								}
							}

						}
					}
					$this->session->set_flashdata('event_feedback', __('The form has successfully been add!'));
					_updateTimeStamp($app->id);
					redirect('apps/view/'.$app->id);
				} 
				else {
					$error = __("Oops, something went wrong. Please try again.");
				}

			} 
		}

        else  // type = venue
        {
			// http://admin.shahid.tapcrowd.com/formtemplate/add/12529/55/shahid.hussain@xorlogics.com/venue
        	$venue = $this->venue_model->getById($id);
        	$app = _actionAllowed($venue, 'venue','returnApp');
        	$languages = $this->language_model->getLanguagesOfApp($app->id);

            //-------------- Addd to Launcher --------------//
			if($app->themeid != 0) {
				//use theme icon
				$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = $moduletypeid LIMIT 1");
				if($icon->num_rows() != 0) {
					$icon = $icon->row();
					$icon = $icon->icon;
					$image = $this->config->item('imagespath') . $icon;
					$newpath = 'upload/appimages/'.$app->id .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
					if(file_exists($image)) {
						if(!is_dir($this->config->item('imagespath') . 'upload/appimages/'.$app->id)){
							mkdir($this->config->item('imagespath') . 'upload/appimages/'.$app->id, 0775,true);
						}

						if(copy($image, $this->config->item('imagespath') . $newpath)) {
							$defaultlauncher->icon = $newpath;
						}
					}
				}
			}
        	$data = array(
        		'appid'         => 0,
        		'venueid'       => $id,
        		'title'         => $formtitle,
        		'module'        => 'forms',
        		'url'           => '',
        		'moduletypeid'  => $moduletypeid,
        		'order'         => $launcherorder,
        		'active'        => 1,
        		'templateformid' => $templateformid,
        		'icon' 			=> $defaultlauncher->icon
        		);

        	if($launcherid = $this->general_model->insert('launcher', $data)) 
        	{
                /*// Count Event Launchers of this ModuleType...
                $eventlaunchers = $this->event_model->countLauncherFromEventModule($venue->id, $moduletypeid);
                if($eventlaunchers){
                    $totalmodule = $this->module_mdl->countModuleFromEventModule($venue->id, $moduletypeid);
                    if(!$totalmodule){
                        //Add to Module
                        $this->module_mdl->addModuleToEvent($venue->id, $moduletypeid);
                    }

                }*/
                
				
				$this->general_model->insert('module', array(
					'moduletype'	=> $moduletypeid,
					'event'			=> 0,
					'app'			=> 0,
					'venue'			=> $id	
				));
				
				foreach($languages as $language) {
					$this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $formtitle);
				}

                //-------------- Addd to Forms --------------//																	
				$data = array(
					"appid"                         => $app->id,
					"eventid"                       => 0,
					"venueid"                       => $id,
					"launcherid"                    => $launcherid,
					"title"                         => $formtitle,
					"submissionbuttonlabel"         => $submissionbuttonlabel,
					"submissionconfirmationtext"    => $submissionconfirmationtext,
					"email"                         => $email,
					"emailsendresult"               => $emailsendresult,
					"allowmutliplesubmissions"      => $allowmutliplesubmissions,
					"order"                         => $order,
					"getgeolocation"                => $getgeolocation,
					"singlescreen"					=> $singlescreen
					);

				if($newid = $this->general_model->insert('form', $data)){

                    //add translated fields to translation table
					foreach($languages as $language) {
				                        //description

						$this->language_model->addTranslation('form', $newid, 'title', $language->key, $formtitle);
						$this->language_model->addTranslation('form', $newid, 'submissionbuttonlabel', $language->key, $submissionbuttonlabel);
						$this->language_model->addTranslation('form', $newid, 'submissionconfirmationtext', $language->key, $submissionconfirmationtext);
					}						

                    //-------------- Addd to Tags --------------//

					$data = array(
						'appid'	=> $app->id,
						'venueid'	=> $id,
						'formid'	=> $newid,
						'tag'       => $formtitle,
						'active'	=> 1
						);

					$tagid = $this->general_model->insert('tag', $data);
					foreach($languages as $language) {
						$this->language_model->addTranslation('tag', $tagid, 'tag', $language->key, $formtitle);
					}

                    //-------------- --------------- --------------//

                    // Adding a default screen 

					foreach($formscreens as $screen){

						$data = array(
							"formid" => $newid,
							"title" => $screen->title,
							"order"	=> 0,
							"visible" => 1
							);

						if($newscreenid = $this->general_model->insert('formscreen', $data)){
                            ////add translated fields to translation table
							foreach($languages as $language) {							
								$this->language_model->addTranslation('formscreen', $newscreenid, 'title', $language->key, $screen->title);
							}

                            // Adding form fields

							foreach($formfieldsarray as $data){
								if($data['formscreenid'] == $screen->id){
									$data['formscreenid'] = $newscreenid;
									if($newfieldid = $this->general_model->insert('formfield', $data)){					
				                                        //add translated fields to translation table
										foreach($languages as $language)
										{
											$this->language_model->addTranslation('formfield', $newfieldid, 'label', $language->key, $data['label']);
											$this->language_model->addTranslation('formfield', $newfieldid, 'possiblevalues', $language->key, $data['possiblevalues']);
											$this->language_model->addTranslation('formfield', $newfieldid, 'defaultvalue', $language->key, $data['defaultvalue']);
										}

									}
								}
							}

						}
					}

					$this->session->set_flashdata('event_feedback', __('The form has successfully been add!'));
					_updateTimeStamp($venue->id);
					redirect('venue/view/'.$venue->id);
				} 
				else {
					$error = __("Oops, something went wrong. Please try again.");
				}

			}    
		}
	}
}

?>