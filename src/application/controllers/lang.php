<?php
	
	class lang extends CI_Controller
	{
		function change($langID)
		{
			//$langID = $this->uri->segment(3);
			if($langID == FALSE) redirect();
			$this->session->set_userdata('language',$langID);
			
			if($this->session->userdata('current_page') != FALSE){
				redirect($this->session->userdata('current_page'), 'location');
			} else {
				redirect('', 'location');
			}
		}
	}

?>