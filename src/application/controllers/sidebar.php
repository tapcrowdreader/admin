<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Sidebar extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}
	
	function show() {
		$this->session->set_userdata(array('sidebar' => 'show'));
	}
	
	function hide() {
		$this->session->set_userdata(array('sidebar' => 'hide'));
	}
}