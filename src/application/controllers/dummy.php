<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Dummy extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	function index() {

	}

	function mobilevikingstats() {
		// $appids = $this->db->query("SELECT appid FROM socialscore GROUP BY appid");
		// $appids = $appids->result();
		// $results = array();
		// foreach($appids as $appid) {
		// 	$app = $this->db->query("SELECT * FROM app WHERE id = $appid->appid");
		// 	$app = $app->row();

		// 	$scorePerTypeQuery = $this->db->query("SELECT count(id) as count,action  FROM socialscore WHERE appid = $appid->appid GROUP BY action");
		// 	$scorePerTypeQuery = $scorePerTypeQuery->result();

		// 	foreach($scorePerTypeQuery as $scorePerType) {
		// 		unset($result);
		// 		$type = $this->db->query("SELECT * FROM socialscore_types WHERE id = $scorePerType->action");
		// 		$type = $type->row();
		// 		$result->app = $app->name;
		// 		$result->type = $type->action;
		// 		$result->total = $scorePerType->count;
		// 		$results[] = $result;
		// 	}
		// }
		// echo "<table>";
		// $total = 0;
		// foreach($results as $res) {
		// 	echo "<tr>";
		// 		echo "<td>".$res->app."</td>";
		// 		echo "<td>".$res->type."</td>";
		// 		echo "<td>".$res->total."</td>";
		// 	echo "</tr>";
		// 	$total += $res->total;
		// }
		// echo "</table>";
		// echo '<br/><br/>';


		// $types = array('shareApp', 'postPhoto', 'sharePersProg');
		// $typeidsArray = array();
		// foreach($types as $type) {
		// 	unset($typeidsArray);
		// 	$typeids = $this->db->query("SELECT id FROM socialscore_types WHERE action = '$type'");
		// 	$typeids = $typeids->result();
		// 	foreach($typeids as $typeid) {
		// 		$typeidsArray[] = $typeid->id;
		// 	}

		// 	$idsString = implode(',',$typeidsArray);

		// 	$totalPerType = $this->db->query("SELECT count(id) as count,action FROM socialscore WHERE action IN($idsString)");
		// 	$totalPerType = $totalPerType->result();

		// 	echo "<table>";
		// 	foreach($totalPerType as $t) {
		// 		$action = $this->db->query("SELECT * FROM socialscore_types WHERE id = $t->action");
		// 		$action = $action->row()->action;
		// 			echo "<tr>";
		// 				echo "<td>".$action."</td>";
		// 				echo "<td>".$t->count."</td>";
		// 			echo "</tr>";
		// 	}
		// 	echo "</table>";
		// }

		// echo "<br/><br/>";
		// echo "Total:" . $total;
	}

	function remapxpo() {
		// $exhibitors1 = $this->db->query("SELECT * FROM exhibitor WHERE eventid = 993")->result();

		// $exhibitors2 = $this->db->query("SELECT * FROM exhibitor WHERE eventid = 0 AND username <> ''")->result();

		// $exhibitors3 = array();
		// foreach($exhibitors2 as $row) {
		// 	$exhibitors3[$row->username] = $row;
		// }

		// foreach($exhibitors1 as $exhibitor) {
		// 	$exhibitor->x1 = $exhibitors3[$exhibitor->username]->x1;
		// 	$exhibitor->y1 = $exhibitors3[$exhibitor->username]->y1;

		// 	$data = array(
		// 		'x1' => $exhibitors3[$exhibitor->username]->x1,
		// 		'y1' => $exhibitors3[$exhibitor->username]->y1
		// 		);

		// 	$this->general_model->update('exhibitor', $exhibitor->id, $data);
		// }
		// echo '<pre>';
		// print_r($exhibitors1);
		// echo '</pre>';
	}

	function resizeArtBrussels() {
		// $this->load->library('image_lib');

		// $dir = "/var/www/UPLOAD/Live/upload/artbrusselsnew/";
		// $dir2 = "/var/www/UPLOAD/Live/upload/artbrussels/small/";

  //       $dh = opendir($dir);
  //       $files = array();
  //       while (($file = readdir($dh)) !== false) {
  //           if($file !== '.' --and----and-- $file !== '..' --and----and-- !in_array($file, $array)) {
		// 		$config['image_library'] = 'gd2';
		// 		$config['source_image'] = $dir . $file;
		// 		$config['create_thumb'] = TRUE;
		// 		$config['maintain_ratio'] = TRUE;
		// 		$config['width'] = 100;
		// 		$config['height'] = 100;
		// 		$config['new_image'] = $dir2 . $file;
		// 		$this->image_lib->initialize($config);
		// 		$this->image_lib->resize();
  //           }
  //       }
	}

	function geocode($appid) {
		// define("MAPS_HOST", "maps.google.com");
		// define("KEY", "abcdefg");

		// // Initialize delay in geocode speed
		// $delay = 0;
		// $base_url = "http://" . MAPS_HOST . "/maps/geo?output=xml" . "--and--key=" . KEY;

		// if($appid == 1444) {
		// 	$venues = $this->db->query("SELECT v.* FROM appvenue av, venue v INNER JOIN tag t ON t.venueid = v.id WHERE av.appid = $appid AND t.tag = 'Uwwijkagent' AND v.id = av.venueid AND v.deleted = 0 ORDER BY v.order ASC, v.id DESC")->result();
		// } else {
		// 	$venues = $this->db->query("SELECT v.* FROM appvenue av, venue v WHERE av.appid = $appid AND v.id = av.venueid AND v.deleted = 0 ORDER BY v.order ASC, v.id DESC")->result();
		// }

		// foreach($venues as $venue) {
		// 	if($venue->address != '' --and----and-- trim($venue->lat) == '') {
		// 		$geocode_pending = true;

		// 		while ($geocode_pending) {
		// 		$address = $venue->address;
		// 		$id = $venue->id;
		// 		$request_url = $base_url . "--and--q=" . urlencode($address);
		// 		$xml = simplexml_load_file($request_url) or die("url not loading");

		// 		$status = $xml->Response->Status->code;
		// 		if (strcmp($status, "200") == 0) {
		// 		  // Successful geocode
		// 		  $geocode_pending = false;
		// 		  $coordinates = $xml->Response->Placemark->Point->coordinates;
		// 		  $coordinatesSplit = split(",", $coordinates);
		// 		  // Format: Longitude, Latitude, Altitude
		// 		  $lat = $coordinatesSplit[1];
		// 		  $lng = $coordinatesSplit[0];

		// 		  $data = array(
		// 		  		'lat' => $lat,
		// 		  		'lon' => $lng
		// 		  	);
		// 		  $this->general_model->update('venue', $id, $data);
		// 		} else if (strcmp($status, "620") == 0) {
		// 		  // sent geocodes too fast
		// 		  $delay += 100000;
		// 		} else {
		// 		  // failure to geocode
		// 		  $geocode_pending = false;
		// 		  echo "Address " . $address . " failed to geocoded. ";
		// 		  echo "Received status " . $status . "\n";
		// 		}
		// 		usleep($delay);
		// 		}

		// 		_updateVenueTimeStamp($venue->id);
		// 	}
		// }
	}

	function geocode_name($appid) {
		// define("MAPS_HOST", "maps.google.com");
		// define("KEY", "abcdefg");

		// // Initialize delay in geocode speed
		// $delay = 0;
		// $base_url = "http://" . MAPS_HOST . "/maps/geo?output=xml" . "--and--key=" . KEY;


		// if($appid == 1444) {
		// 	$venues = $this->db->query("SELECT v.* FROM appvenue av, venue v INNER JOIN tag t ON t.venueid = v.id WHERE av.appid = $appid AND t.tag = 'Uwwijkagent' AND v.id = av.venueid AND v.deleted = 0 ORDER BY v.order ASC, v.id DESC")->result();
		// } else {
		// 	$venues = $this->db->query("SELECT v.* FROM appvenue av, venue v WHERE av.appid = $appid AND v.id = av.venueid AND v.deleted = 0 ORDER BY v.order ASC, v.id DESC")->result();
		// }


		// foreach($venues as $venue) {
		// 	if($venue->name != '' --and----and-- $venue->lat == '') {
		// 		$geocode_pending = true;

		// 		while ($geocode_pending) {
		// 		$address = $venue->name;
		// 		$id = $venue->id;
		// 		$request_url = $base_url . "--and--q=" . urlencode($address);
		// 		$xml = simplexml_load_file($request_url) or die("url not loading");

		// 		$status = $xml->Response->Status->code;
		// 		if (strcmp($status, "200") == 0) {
		// 		  // Successful geocode
		// 		  $geocode_pending = false;
		// 		  $coordinates = $xml->Response->Placemark->Point->coordinates;
		// 		  $coordinatesSplit = split(",", $coordinates);
		// 		  // Format: Longitude, Latitude, Altitude
		// 		  $lat = $coordinatesSplit[1];
		// 		  $lng = $coordinatesSplit[0];

		// 		  $data = array(
		// 		  		'lat' => $lat,
		// 		  		'lon' => $lng
		// 		  	);
		// 		  $this->general_model->update('venue', $id, $data);
		// 		} else if (strcmp($status, "620") == 0) {
		// 		  // sent geocodes too fast
		// 		  $delay += 100000;
		// 		} else {
		// 		  // failure to geocode
		// 		  $geocode_pending = false;
		// 		  echo "Address " . $address . " failed to geocoded. ";
		// 		  echo "Received status " . $status . "\n";
		// 		}
		// 		usleep($delay);
		// 		}

		// 		_updateVenueTimeStamp($venue->id);
		// 	}
		// }
	}

	function mail() {
		// $newuser = $this->user_mdl->getUserByLogin('stichoel');
		//Config mail function:
		// $config['mailtype'] 	= 'html';
		// $config['charset']		= 'utf-8';
		// //Set Config!
		// $this->load->library('email');
		// $this->email->initialize($config);
		// //prepare email
		// $to = 'devsquad@tapcrowd.com'; //TO Address
		// $subject = "I'm sexy and I know it"; //Subject of the message
		// $from = 'dwayne@tapcrowd.com';

		// $message = '<img src="http://admin.tapcrowd.com/img/dwayne.png" />';

		// $this->email->from($from, $fromname);
		// $this->email->to($to);
		// $this->email->subject($subject);
		// $this->email->message($message);
		// $this->email->send();
	}

	function horecaexpo() {
		// //excel
		// $headers = true;
		// $catcolumnnumber = 14;
		// $categories = array();
		// $row = 1;
		// $this->load->model('general_model');
		// $eventid = 1431;
		// $prevname = '';
		// $appid = 953;

		// if(file_exists("../horeca.csv")) {
		// 	if (($handle = fopen("../horeca.csv", "r")) !== FALSE) {
		// 		while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
		// 			$num = count($data);
		// 			$row++;
		// 			$newrow = array();
		// 			for ($c=0; $c < $num; $c++) {
		// 				if($headers) {
		// 					if($c < $catcolumnnumber) {
		// 						//get all headings
		// 						$heading[] = $data[$c];
		// 					} else{
		// 						$heading[] = $data[$c];
		// 					}

		// 				} else {
		// 						$newrow[$heading[$c]] = $data[$c];
		// 				}
		// 			}

		// 			if(!$headers --and----and-- $newrow['name'] != '') {
		// 				//insert exhibitor
		// 				if($newrow['Content Language'] != 'fr') {
		// 					$exhibitor = $this->db->query("SELECT id FROM exhibitor WHERE eventid = $eventid AND name = '".mysql_escape_string($newrow['name'])."' LIMIT 1");
		// 					if($exhibitor->num_rows() != 0) {
		// 						$exhibitor = $exhibitor->row();
		// 						$this->general_model->update('exhibitor', $exhibitor->id, array('email' => $newrow['Email2 1']));
		// 					}
		// 				}
		// 			}

		// 			$headers = FALSE;
		// 		}
		// 	} else {
		// 		$error = 'Oops, excel file could not be loaded.';
		// 	}
		// }
		// echo 'succes';
	}

	function removeVenuesWithTag($appid, $tag) {
		$res = $this->db->query("SELECT id, venueid FROM tag WHERE appid = $appid AND tag = '".$tag."'")->result();
		$venueids = array();
		$tagids = array();
		foreach($res as $row) {
			$venueids[] = $row->venueid;
			$tagids[] = $row->id;
		}

		$ids = implode(',', $venueids);

		$tagidsString = implode(',', $tagids);

		$this->db->query("DELETE FROM venue WHERE id IN ($ids)");
		$this->db->query("DELETE FROM tag WHERE id IN ($tagidsString)");
	}

	function resplit() {
		// split_map('upload/eventimages/3832/logo.png', 3832, 'event');
	}

	function infomodules() {
		// var_dump("DELETE FROM module WHERE moduletype = 21");

		// $appids = array();
		// $apps = $this->db->query("SELECT id FROM app WHERE apptypeid = 5 AND id <> 55 AND id <> 612")->result();
		// foreach($apps as $app) {
		// 	$appids[] = $app->id;
		// }

		// echo("INSERT INTO module (moduletype, app) VALUES (21,".implode('),(21,', $appids).")");




		// $venueids = array();
		// $venues = $this->db->query("SELECT id FROM venue")->result();
		// foreach($venues as $v) {
		// 	$venueids[] = $v->id;
		// }

		// echo("INSERT INTO module (moduletype, venue) VALUES (21,".implode('),(21,', $venueids).")");




		// $eventids = array();
		// $events = $this->db->query("SELECT id FROM event")->result();
		// foreach($events as $e) {
		// 	$eventids[] = $e->id;
		// }

		// echo("INSERT INTO module (moduletype, event) VALUES (21,".implode('),(21,', $eventids).")");
	}

	function infopolitie($appid) {
		// $venueids = array();
		// $venues = $this->db->query("SELECT v.* FROM appvenue av INNER JOIN venue v ON v.id = av.venueid WHERE av.appid = $appid")->result();
		// foreach($venues as $v) {
		// 	$venueids[] = $v->id;
		// }
		// $venueidsArray = implode(',', $venueids);
		// echo "DELETE FROM module WHERE venue IN ($venueidsArray) AND moduletype = 21";
		// echo("INSERT INTO module (moduletype, venue) VALUES (21,".implode('),(21,', $venueids).")");
	}

	function resetordervenues($appid) {
		// $venues = $this->db->query("SELECT v.* FROM appvenue av INNER JOIN venue v ON v.id = av.venueid WHERE av.appid = $appid")->result();
		// foreach($venues as $v) {
		// 	$venueids[] = $v->id;
		// }
		// $venueidsArray = implode(',', $venueids);
		// echo "UPDATE venue SET `order` = 0 WHERE id IN ($venueidsArray)";
	}

	function fileupload() {
		// $this->load->library('form_validation');

		// if($this->input->post('postback') == "postback") {
		// 	$image = $_FILES['test'];
		// 	imageupload(array(
		// 				'file' => $image,
		// 				'parenttype' => 'exhibitor',
		// 				'parentid' => 1,
		// 				'imagecheck' => true,
		// 				'filetype' => 'jpg',
		// 				'maxwidth' => 700,
		// 				'maxheight' => 500,
		// 				'thumb' => true,
		// 				'thumbwidth' => 100,
		// 				'thumbheight' => 50)
		// 	);
		// }

		// $this->load->view('dummy', array());
	}

	function attendeestrim($eventid) {
		// $attendees = $this->db->query("SELECT * FROM attendees WHERE eventid = $eventid")->result();
		// foreach($attendees as $a) {
		// 	$name = trim($a->name);
		// 	$this->db->query("UPDATE attendees SET name = '".$name."' WHERE id = $a->id");
		// }
	}

	function copymetadata($appid, $objects = 'events', $tag) {
		// $appid = (int) $appid;
		// $addmetadata = array();
		// if($objects == 'events') {
		// 	$metadata = $this->db->query("SELECT m.*, l.eventid, l.moduletypeid, l.module, l.title, l.icon, l.displaytype, l.order, l.url FROM tc_metadata m INNER JOIN launcher l ON l.id = m.parentId WHERE m.appid = $appid AND m.parentType = 'launcher' AND l.eventid > 0")->result();

		// 	$events = $this->db->query("SELECT e.*, t.tag FROM appevent ae INNER JOIN event e ON e.id = ae.eventid INNER JOIN tag t ON t.eventid = e.id WHERE ae.appid = $appid AND deleted = 0 AND t.tag = '$tag'")->result();

		// 	foreach($metadata as $m) {
		// 		foreach($events as $e) {
		// 			if($e->id != $m->eventid) {
		// 				$newlauncherid = $this->db->query("SELECT id FROM launcher WHERE eventid = $e->id AND moduletypeid = $m->moduletypeid LIMIT 1");
		// 				if($newlauncherid->num_rows == 0) {
		// 					$newlauncherid = $this->general_model->insert('launcher', array(
		// 						'eventid' => $e->id,
		// 						'moduletypeid' => $m->moduletypeid,
		// 						'module' => $m->module,
		// 						'title' => $m->title,
		// 						'icon' => $m->icon,
		// 						'displaytype' => $m->displaytype,
		// 						'order' => $m->order,
		// 						'url' => $m->url,
		// 						'active' => 0
		// 						));
		// 				} else {
		// 					$newlauncherid = $newlauncherid->row()->id;
		// 				}
		// 				$checkExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $newlauncherid AND qname = '$m->qname' LIMIT 1");
		// 				if($checkExists->num_rows() == 0) {
		// 					if(!isset($addmetadata[$newlauncherid.'_'.$m->qname])) {
		// 						$addmetadata[$newlauncherid.'_'.$m->qname] = array(
		// 							'appId' => $appid,
		// 							'parentType' => 'launcher',
		// 							'parentId' => $newlauncherid,
		// 							'sortorder' => $m->sortorder,
		// 							'status' => $m->status,
		// 							'type' => $m->type,
		// 							'opts' => $m->opts,
		// 							'qname' => $m->qname,
		// 							'name' => $m->name,
		// 							'value' => $m->value
		// 							);
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// } elseif($objects == 'venues') {
		// 	$metadata = $this->db->query("SELECT m.*, l.venueid, l.moduletypeid, l.module, l.title, l.icon, l.displaytype, l.order, l.url FROM tc_metadata m INNER JOIN launcher l ON l.id = m.parentId WHERE m.appid = $appid AND m.parentType = 'launcher' AND l.venueid > 0")->result();

		// 	$venues = $this->db->query("SELECT e.*, t.tag FROM appvenue ae INNER JOIN venue e ON e.id = ae.venueid INNER JOIN tag t ON t.venueid = e.id WHERE ae.appid = $appid AND deleted = 0 AND t.tag = '$tag'")->result();

		// 	foreach($metadata as $m) {
		// 		foreach($venues as $e) {
		// 			if($e->id != $m->venueid) {
		// 				$newlauncherid = $this->db->query("SELECT id FROM launcher WHERE venueid = $e->id AND moduletypeid = $m->moduletypeid LIMIT 1");
		// 				if($newlauncherid->num_rows == 0) {
		// 					$newlauncherid = $this->general_model->insert('launcher', array(
		// 						'venueid' => $e->id,
		// 						'moduletypeid' => $m->moduletypeid,
		// 						'module' => $m->module,
		// 						'title' => $m->title,
		// 						'icon' => $m->icon,
		// 						'displaytype' => $m->displaytype,
		// 						'order' => $m->order,
		// 						'url' => $m->url,
		// 						'active' => 0
		// 						));
		// 				} else {
		// 					$newlauncherid = $newlauncherid->row()->id;
		// 				}
		// 				$checkExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $newlauncherid AND qname = '$m->qname' LIMIT 1");
		// 				if($checkExists->num_rows() == 0) {
		// 					if(!isset($addmetadata[$newlauncherid.'_'.$m->qname])) {
		// 						$addmetadata[$newlauncherid.'_'.$m->qname] = array(
		// 							'appId' => $appid,
		// 							'parentType' => 'launcher',
		// 							'parentId' => $newlauncherid,
		// 							'sortorder' => $m->sortorder,
		// 							'status' => $m->status,
		// 							'type' => $m->type,
		// 							'opts' => $m->opts,
		// 							'qname' => $m->qname,
		// 							'name' => $m->name,
		// 							'value' => $m->value
		// 							);
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// }

		// $sql = array();
		// foreach($addmetadata as $am) {
		// 	$sql[] = "('".implode("','", $am)."')";
		// }

		// $sql = implode(',',$sql);

		// $this->db->query("INSERT INTO tc_metadata (appId, parentType, parentId, sortorder, status, type, opts, qname, name, value) VALUES $sql");
		// echo 'success';
	}

	function resetvenueorder($appid) {
		// $venueids = array();
		// $venueidsquery = $this->db->query("SELECT v.id FROM venue v INNER JOIN appvenue av ON av.venueid = v.id WHERE av.appid = $appid")->result();
		// foreach($venueidsquery as $id) {
		// 	$venueids[] = $id->id;
		// }

		// $venueids = implode(',', $venueids);
		// $this->db->query("UPDATE venue SET `order` = 0 WHERE id IN ($venueids)");
	}

	function trouwmariagecopy($eventid = 462, $toeventid = 4436) {
		// $map = $this->db->query("SELECT * FROM map WHERE eventid = $eventid LIMIT 1")->row();
		// $newmapid = $this->general_model->insert('map', array('eventid' => $toeventid, 'imageurl' => $map->imageurl, 'timestamp' => $map->timestamp, 'width' => $map->width, 'height' => $map->height, 'title' => $map->title));
		// $exhibitors = $this->db->query("SELECT * FROM exhibitor WHERE eventid = $eventid")->result();
		// foreach($exhibitors as $ex) {
		// 	$this->general_model->insert('exhibitor', array('external_id' => $ex->external_id, 'eventid' => $toeventid, 'name' => $ex->name, 'shortname' => $ex->shortname, 'booth' => $ex->booth, 'imageurl' => $ex->imageurl, 'mapid' => $newmapid, 'y1' => $ex->y1, 'x1' => $ex->x1, 'description' => $ex->description, 'tel' => $ex->tel, 'address' => $ex->address, 'email' => $ex->email, 'web' => $ex->web, 'order' => $ex->order));
		// }

		// $sessiongroups = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid")->result();
		// foreach($sessiongroups as $sg) {
		// 	$sessions = $this->db->query("SELECT * FROM session WHERE sessiongroupid = $sg->id")->result();
		// 	$sgid = $this->general_model->insert('sessiongroup', array('eventid' => $toeventid, 'name' => $sg->name, 'order' => $sg->order));
		// 	foreach($sessions as $s) {
		// 		$sid = $this->general_model->insert('session', array('external_id' => $s->external_id, 'sessiongroupid' => $sgid, 'eventid' => $toeventid, 'order' => $s->order, 'name' => $s->name, 'description' => $s->description, 'starttime' => $s->starttime, 'endtime' => $s->endtime, 'speaker' => $s->speaker, 'location' => $s->location, 'mapid' => $newmapid, 'xpos' => $s->xpos, 'ypos' => $s->ypos,));
		// 	}
		// }
	}

	function question() {
		// $formid = (int) $_GET['id'];
		// $formsubmissions = array();
		// $con = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin", "tapcrowd");
		// if (!$con)
		//   {
		//   die('Could not connect: ' . mysqli_error());
		//   }
		// mysqli_set_charset($con, "utf8");

		// $formQuery = mysqli_query($con, "SELECT f.id as formid, f.eventid, f.title, fsf.value, ff.label, ff.formfieldtypeid, fs.id as formsubmissionid, ff.order, fs.visible FROM form f
		//     		LEFT JOIN formsubmission fs ON fs.formid = f.id
		//     		LEFT JOIN formsubmissionfield fsf ON fsf.formsubmissionid = fs.id
		//     		LEFT JOIN formfield ff ON ff.id = fsf.formfieldid
		//     		WHERE f.id = $formid
		//     		AND fs.visible = 1
		//     		ORDER BY ff.order ASC");

		// if($formQuery != false) {
		// 	while($form = mysqli_fetch_assoc($formQuery)) {
		// 		$formsubmission = (object) $form;
	 //    		if($formsubmission->formfieldtypeid == 17 --and----and-- !empty($formsubmission->value)) {
	 //    			$speakername =  mysqli_query($con, "SELECT name FROM speaker WHERE id = $formsubmission->value LIMIT 1");
	 //    			$speakername = mysqli_fetch_assoc($speakername);
	 //    			$formsubmission->speakername = $speakername['name'];
	 //    		}
		// 		$formsubmissions[$formsubmission->formsubmissionid][] = $formsubmission;
		// 	}
		// }

		// if(!empty($formsubmissions)) {
		// 	$headers = array();
		// 	if(!empty($formsubmissions)) {
		// 		foreach($formsubmissions as $q) {
		// 			foreach($q as $object) {
		// 				if($object->formfieldtypeid != 10) {
		// 					$headers[$object->label] = $object->label;
		// 				}
		// 			}
		// 			break;
		// 		}
		// 	}

		// 	echo '<table>';
		// 	echo '<tr>';
		// 	foreach($headers as $h) {
		// 		echo '<th>'.$h.'</th>';
		// 	}
		// 	echo '</tr>';
		// 	foreach($formsubmissions as $s) {
		// 		echo '<tr>';
		// 		foreach($s as $f) {
		// 			if($f->formfieldtypeid == 17) {
		// 				echo '<td>'.$f->speakername.'</td>';
		// 			} else {
		// 				echo '<td>' . $f->value . '</td>';
		// 			}
		// 		}
		// 		echo '</tr>';
		// 	}
		// 	echo '</table>';
		// }

		// // echo '<script type="text/javascript">
		// // 	setTimeout(function(){
		// // 	   window.location.reload(1);
		// // 	}, 10000);
		// // 	</script>';

		// mysqli_close($con);
	}

	function boisethabitat($appid = 488, $eventid = 914) {
		// $mainparentid = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'exhibitorcategories' LIMIT 1")->row()->id;
		// $categories = $this->db->query("SELECT * FROM exhibitorcategory WHERE eventid = $eventid")->result();
		// foreach($categories as $c) {
		// 	var_dump($c);
		// 	$c->newid = $this->general_model->insert('group', array(
		// 			'appid' => $appid,
		// 			'eventid' => $eventid,
		// 			'name' => $c->name,
		// 			'parentid' => $mainparentid
		// 		));

		// 	$exhibitorsOfCat = $this->db->query("SELECT * FROM exhicat WHERE exhibitorcategoryid = $c->id")->result();
		// 	foreach($exhibitorsOfCat as $e) {
		// 		$this->general_model->insert('groupitem', array(
		// 				'appid' => $appid,
		// 				'eventid' => $eventid,
		// 				'groupid' => $c->newid,
		// 				'itemtable' => 'exhibitor',
		// 				'itemid' => $e->exhibitorid
		// 			));
		// 	}
		// }

		// echo 'succes';
	}

	function euroclearconfbag($appid, $eventid, $launcherid) {
		// $this->load->model('confbag_model');
		// $event = $this->event_model->getbyid($eventid);
		// $app = $this->app_model->get($appid);

		// $launcher = $this->module_mdl->getLauncher(42, 'event', $event->id);
		// if($launcher->newLauncherItem) {
		// 	$launcher->title = __($launcher->title);
		// }
		// $title = $launcher->title;

		// $ownmail = $_GET['email'];

		// $subject = $event->name . ' - Your personal '. $launcher->title;
		// ini_set('sendmail_from', $ownmail);
		// $headers = "From: ".$ownmail;
		// // $headers .= "\r\nBcc: jens.verstuyft@tapcrowd.com";
		// $headers .=  "\r\n".'Content-type: text/html; charset=utf-8' . "\r\n";

		// $data = $this->confbag_model->getConfbagdata($eventid);
		// $current_email = $data[0]->email;
		// $current_data = array();
		// $startdate = '';

		// $appicon = '';
		// $appiconres = $this->db->query("SELECT * FROM appearance WHERE controlid = 5 AND appid = $app->id LIMIT 1");
		// if($appiconres->num_rows() > 0) {
		// 	$appicon = $appiconres->row()->value;
		// }

		// $iEmails = 0;
		// $data[] = (object) array('email' => 'zzzzzzzzzzzzz@tapcrowd.com');
		// foreach($data as $row) {
		// 	if(trim(strtolower($row->email)) != trim(strtolower($current_email)) || $iEmails == count($data) - 1) {
		// 		$datei = 0;
		// 		$ibg = 0;
		// 		//send mail to current_email with current_data and clear both
		// 		$to = $current_email;
		// 		$message =
		// 		'<table width="690" border="0" cellspacing="0" cellpadding="0">
		// 		    <tr>
		// 		      <td height="40" width="40"></td>
		// 		      <td height="40" width="650"></td>
		// 		      <td height="40" width="40"></td>
		// 		    </tr>
		// 		    <tr>
		// 		      <td height="400" width="40"></td>
		// 		      <td height="400" width="650"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		// 		          <tr>
		// 		            <td width="650" height="151" align="left" valign="top"><img style="display: block; border-top:0px;  border-left:0px;  border-right:0px;  border-bottom:0px;" src="http://clients.tapcrowd.com/euroclear/confbag-email_header.jpg" width="650" height="151" border="0"/></td>
		// 		          </tr>
		// 		          <tr>
		// 		            <td width="650" height="400" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		// 		                <tr>
		// 		                  <td height="40" width="40"></td>
		// 		                  <td height="40" width="570"></td>
		// 		                  <td height="40" width="40"></td>
		// 		                </tr>
		// 		                <tr>
		// 		                  <td width="40"></td>
		// 		                  <td width="570" align="left" valign="top" style="font-weight: normal; line-height: 1.1em; font-family:Verdana, Arial, Geneva, Helvetica, sans-serif; -webkit-font-smoothing: antialiased; font-size: 11px; color: #666666;">
		// 		                  <img src="http://clients.tapcrowd.com/euroclear/confbag-email_title.jpg" style="display: block; border-top:0px;  border-left:0px;  border-right:0px;  border-bottom:0px;" width="277" height="49"  border="0"/> <br /><br />';

		// 		$current_object = '';
		// 		foreach($current_data as $current_row) {
		// 			//group objects
		// 			if($current_object != $current_row->object) {
		// 				if($current_row->object == 'attendee' || $current_row->object == 'session' || $current_row->object == 'exhibitor') {
		// 					$datei = 0;
		// 					$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#CD4933;">'.ucfirst($current_row->object)."s</h2>";
		// 					$message .= '<div style="margin-left:8px;">';
		// 				} else {
		// 					$datei = 0;
		// 					$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#CD4933;">'.ucfirst($current_row->object)."</h2>";
		// 					$message .= '<div style="margin-left:8px;">';
		// 				}

		// 				$current_object = $current_row->object;
		// 			}

		// 			//session times
		// 			if($current_row->starttime != '' --and----and-- $startdate != substr($current_row->starttime,0,10)) {
		// 				$startdate = substr($current_row->starttime,0,10);
		// 				$message .= '<h3 style="margin-bottom:0px;padding-top:10px;color:#CD4933;margin-left:-8px;'.($datei == 0 ? 'margin-top:0px;padding-top:0px;' : 'padding-top:0px;') .'">'.$startdate."</h3>";
		// 			} else {
		// 				$startdate != substr($current_row->starttime,0,10);
		// 			}
		// 			$datei++;
		// 			if($current_row->object == 'attendee') {
		// 				$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. $current_row->firstname . ($current_row->company != '' ? ' (' . $current_row->company . ($current_row->function != '' ? ' - ' . $current_row->function : '') . ')' : '') . '<br/>';
		// 				$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. $current_row->firstname;
		// 				if($current_row->emailatt != '') {
		// 					$message .= '<a href="mailto:'.$current_row->emailatt.'" style="color: #CD4933; text-decoration:underline; font-weight: bold;">'.$current_row->emailatt.'</a>';
		// 				}
		// 				if($current_row->linkedin != '') {
		// 					$message .= ' - <a href="'.$current_row->linkedin.'" style="color: #CD4933; text-decoration:underline; font-weight: bold;">LinkedIn</a>';
		// 				}
		// 			} else {
		// 				$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. $current_row->firstname;
		// 			}

		// 			//document links
		// 			$documentlinks = explode(',', $current_row->documentlink);
		// 			if($current_row->documentlink != '' --and----and-- $current_object == 'attendee') {
		// 				$message .= '<br/>';
		// 			}
		// 			$i = 1;
		// 			foreach($documentlinks as $documentlink) {
		// 				if(substr($documentlink, 0, 7) != 'http://' --and----and-- substr($documentlink, 0, 8) != 'https://' --and----and-- $documentlink != '') {
		// 					$documentlink = utf8_encode('http://upload.tapcrowd.com/'.$documentlink);
		// 				}
		// 				if($documentlink != '') {
		// 					if($current_object == 'attendee') {
		// 						$message .=	'<a href="'.$documentlink.'" style="color: #CD4933; text-decoration:underline; font-weight: bold;">Document'.$i.'</a> ';
		// 					} elseif($current_object == 'other') {
		// 						$message .= '<a href="'.$documentlink.'" style="color: #CD4933; text-decoration:underline; font-weight: bold;">'.$documentlink.'</a><br/>';
		// 					} else {
		// 						$message .= ' - ' . '<a href="'.$documentlink.'" style="color: #CD4933; text-decoration:underline; font-weight: bold;">Document'.$i.'</a> ';
		// 					}
		// 				}
		// 				$i++;
		// 			}

		// 			$message .= '<br clear="both"/>';
		// 			$ibg++;
		// 		}

		// 		$message .= '<br/>'.__("We hope to welcome you again at our next edition.<br/>
		// 							Kind regards,<br/>").
		// 							'<a href="mailto:'.$ownmail.'" style="color:#CD4933">'.$app->name . ' ' . __('Team') . '</a></td>
		// 			                  <td width="40"></td>
		// 			                </tr>
		// 			                <tr>
		// 			                  <td height="40" width="40"></td>
		// 			                  <td height="40" width="570"></td>
		// 			                  <td height="40" width="40"></td>
		// 			                </tr>
		// 			              </table></td>
		// 			          </tr>
		// 			          <tr>
		// 			            <td width="650" height="77" align="left" valign="top"><img style="display: block; border-top:0px;  border-left:0px;  border-right:0px;  border-bottom:0px;" src="http://clients.tapcrowd.com/euroclear/confbag-email_footer.jpg" width="650" height="77" border="0"/></td>
		// 			          </tr>
		// 			        </table></td>
		// 			      <td height="400" width="40"></td>
		// 			    </tr>
		// 			    <tr>
		// 			      <td height="40" width="40"></td>
		// 			      <td height="40" width="650"></td>
		// 			      <td height="40" width="40"></td>
		// 			    </tr>
		// 			  </table>';

		// 		$mail_sent = mail( $to, $subject, $message, $headers );
		// 		$current_data = array();
		// 		$current_email = $row->email;
		// 	}
		// 	$current_data[] = $row;
		// 	$iEmails++;
		// }

		// $this->session->set_flashdata('event_feedback', __('The mails have been sent'));
		// redirect('confbag/event/'.$eventid);
	}


	function metadatatranslate()
	{
// 		$res = $this->db->query("SELECT tc_metadata.id, app.id as appid, app.defaultlanguage FROM app INNER JOIN tc_metadata ON tc_metadata.appid = app.id GROUP BY tc_metadata.id")->result();
// 		foreach($res as $r) {
// 			$this->db->query("UPDATE tc_metavalues SET language = '$r->defaultlanguage' WHERE metaId = $r->id");
// 		}
	}

	function artbrusselstest() {
		// $i = 1;
		// $imagei = 26860;
		// $metaid1 = $this->db->query("SELECT * FROM tc_metadata WHERE id = 27 LIMIT 1")->row();
		// $metaid2 = $this->db->query("SELECT * FROM tc_metadata WHERE id = 28 LIMIT 1")->row();
		// $metaid3 = $this->db->query("SELECT * FROM tc_metadata WHERE id = 29 LIMIT 1")->row();
		// $metaid4 = $this->db->query("SELECT * FROM tc_metadata WHERE id = 30 LIMIT 1")->row();
		// $metaid5 = $this->db->query("SELECT * FROM tc_metadata WHERE id = 31 LIMIT 1")->row();
		// $metaid6 = $this->db->query("SELECT * FROM tc_metadata WHERE id = 32 LIMIT 1")->row();
		// $metaid7 = $this->db->query("SELECT * FROM tc_metadata WHERE id = 33 LIMIT 1")->row();
		// $metaid8 = $this->db->query("SELECT * FROM tc_metadata WHERE id = 34 LIMIT 1")->row();
		// $metaid9 = $this->db->query("SELECT * FROM tc_metadata WHERE id = 35 LIMIT 1")->row();
		// while($i <= 150) {
		// 	$imageiname = $this->db->query("SELECT * FROM exhibitor WHERE id = $imagei LIMIT 1")->row();
		// 	$exid = $this->general_model->insert('exhibitor', array('eventid' => 5538, 'name' => 'exhibitor'.$i));
		// 	$this->general_model->insert('tc_metavalues', array('metaId' => $metaid1->id, 'parentType' => 'exhibitor', 'parentId' => $exid, 'name' => $metaid1->name, 'type' => 'image', 'value' => str_replace('exhibitorimages/', 'artbrussels/exhibitorimages/', $imageiname->image1), 'language' => 'en'));
		// 	$this->general_model->insert('tc_metavalues', array('metaId' => $metaid2->id, 'parentType' => 'exhibitor', 'parentId' => $exid, 'name' => $metaid2->name, 'type' => 'image', 'value' => str_replace('exhibitorimages/', 'artbrussels/exhibitorimages/', $imageiname->image2), 'language' => 'en'));
		// 	$this->general_model->insert('tc_metavalues', array('metaId' => $metaid3->id, 'parentType' => 'exhibitor', 'parentId' => $exid, 'name' => $metaid3->name, 'type' => 'image', 'value' => str_replace('exhibitorimages/', 'artbrussels/exhibitorimages/', $imageiname->image3), 'language' => 'en'));
		// 	$this->general_model->insert('tc_metavalues', array('metaId' => $metaid4->id, 'parentType' => 'exhibitor', 'parentId' => $exid, 'name' => $metaid4->name, 'type' => 'image', 'value' => str_replace('exhibitorimages/', 'artbrussels/exhibitorimages/', $imageiname->image4), 'language' => 'en'));
		// 	$this->general_model->insert('tc_metavalues', array('metaId' => $metaid5->id, 'parentType' => 'exhibitor', 'parentId' => $exid, 'name' => $metaid5->name, 'type' => 'image', 'value' => str_replace('exhibitorimages/', 'artbrussels/exhibitorimages/', $imageiname->image5), 'language' => 'en'));
		// 	$this->general_model->insert('tc_metavalues', array('metaId' => $metaid6->id, 'parentType' => 'exhibitor', 'parentId' => $exid, 'name' => $metaid6->name, 'type' => 'image', 'value' => str_replace('exhibitorimages/', 'artbrussels/exhibitorimages/', $imageiname->image6), 'language' => 'en'));
		// 	$this->general_model->insert('tc_metavalues', array('metaId' => $metaid7->id, 'parentType' => 'exhibitor', 'parentId' => $exid, 'name' => $metaid7->name, 'type' => 'image', 'value' => str_replace('exhibitorimages/', 'artbrussels/exhibitorimages/', $imageiname->image7), 'language' => 'en'));
		// 	$this->general_model->insert('tc_metavalues', array('metaId' => $metaid8->id, 'parentType' => 'exhibitor', 'parentId' => $exid, 'name' => $metaid8->name, 'type' => 'image', 'value' => str_replace('exhibitorimages/', 'artbrussels/exhibitorimages/', $imageiname->image8), 'language' => 'en'));
		// 	$this->general_model->insert('tc_metavalues', array('metaId' => $metaid9->id, 'parentType' => 'exhibitor', 'parentId' => $exid, 'name' => $metaid9->name, 'type' => 'image', 'value' => str_replace('exhibitorimages/', 'artbrussels/exhibitorimages/', $imageiname->image9), 'language' => 'en'));
		// 	$imagei++;
		// 	$i++;
		// }
	}

	function removeformresults($formid) {
		// $formid = (int) $formid;
		
		// $submissionfieldids = $this->db->query("SELECT formsubmissionfield.id FROM formsubmissionfield INNER JOIN formsubmission ON formsubmission.id = formsubmissionfield.formsubmissionid WHERE formsubmission.formid = $formid")->result();
		// $ids = array();
		// foreach($submissionfieldids as $f) {
		// 	$ids[] = $f->id;
		// }

		// if(!empty($ids)) {
		// 	$ids = implode(',', $ids);
		// 	$this->db->query("DELETE FROM formsubmissionfield WHERE id IN ($ids)");
		// }

		// $this->db->query("DELETE FROM formsubmission WHERE formid = $formid");
	}

	function sendData() {

	// $data_string = '';
	 
	// $ch = curl_init('http://api.staging.tapcrowd.com/api/gateway/call/1.4/groezrock');
	// //final server
	// //$ch = curl_init('http://api.tapcrowd.com/api/gateway/call/1.4/groezrock');
	// curl_setopt($ch, CURLOPT_POST, true);                                                                     
	// curl_setopt($ch, CURLOPT_POSTFIELDS, "data=".$data_string);                                                                  
	// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	// // curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	// //     'Content-Type: application/json',                                                                                
	// //     'Content-Length: ' . strlen($data_string))                                                                       
	// // );                                                                                                                   
	 
	// $result = curl_exec($ch);
	// var_dump($result);


	// $ch = curl_init('http://api.staging.tapcrowd.com/api/gateway/call/1.4/groezrock');
	// //final server
	// //$ch = curl_init('http://api.tapcrowd.com/api/gateway/call/1.4/groezrock');
	// // curl_setopt($ch, CURLOPT_POST, true);                                                                  
	// // curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                 
	 
	// // $ch = curl_init('http://api.staging.tapcrowd.com/api/gateway/call/1.4/groezrock');
	// // //final server
	// // //$ch = curl_init('http://api.tapcrowd.com/api/gateway/call/1.4/groezrock');
	// // curl_setopt($ch, CURLOPT_POST, true);                                                                     
	// // curl_setopt($ch, CURLOPT_POSTFIELDS, "data=".$data_string);                                                                  

	// // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	// // curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	// //     'Content-Type: application/json',                                                                                
	// //     'Content-Length: ' . strlen($data_string))                                                                       
	// // );                                                                                                    
	 
 //    curl_setopt($ch, CURLOPT_POST, true);                                                                    
 //    curl_setopt($ch, CURLOPT_POSTFIELDS, "data=".$data_string);                                                                 
 //    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                     
 //    // curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                         
 //    //     'Content-Type: application/json',                                                                               
 //    //     'Content-Length: ' . strlen($data_string))                                                                      
 //    // );                                                                                                                  
     
 //    $result = curl_exec($ch);
	// echo($result);

	}

	function ffi() {
		// $query = $this->db->query("SELECT v.metaId, v.name, n.id as newid FROM tc_metavalues v 
		// 			INNER JOIN tc_metadata m ON m.id = v.metaId
		// 			INNER JOIN launcher l ON l.id = m.parentId
		// 			INNER JOIN (SELECT m.* FROM tc_metadata m
		// 			INNER JOIN launcher l ON l.id = m.parentId
		// 			WHERE m.parentId = 18561 AND m.appid = 2098) n ON v.name = n.name
		// 			WHERE m.parentId <> 18561 AND m.appid = 2098 AND l.venueid <> 0");

		// foreach($query->result() as $row) {
		// 	$this->db->query("UPDATE tc_metavalues SET metaId = $row->newid WHERE metaId = $row->metaId");
		// }

		// $deleteQuery = $this->db->query("SELECT m.id FROM tc_metadata AS m
		// 				INNER JOIN launcher l ON l.id = m.parentId
		// 				WHERE l.venueid <> 0 AND m.parentId <> 18561 AND m.appId = 2098");
		// foreach($deleteQuery->result() as $row) {
		// 	$this->db->query("DELETE FROM tc_metadata WHERE id = $row->id");
		// }

		// $query = $this->db->query("SELECT v.metaId, v.name, n.id as newid FROM tc_metavalues v 
		// 			INNER JOIN tc_metadata m ON m.id = v.metaId
		// 			INNER JOIN launcher l ON l.id = m.parentId
		// 			INNER JOIN (SELECT m.* FROM tc_metadata m
		// 			INNER JOIN launcher l ON l.id = m.parentId
		// 			WHERE m.parentId = 18001 AND m.appid = 2098) n ON v.name = n.name
		// 			WHERE m.parentId <> 18001 AND m.appid = 2098 AND l.eventid <> 0");

		// foreach($query->result() as $row) {
		// 	$this->db->query("UPDATE tc_metavalues SET metaId = $row->newid WHERE metaId = $row->metaId");
		// }

		// $deleteQuery = $this->db->query("SELECT m.id FROM tc_metadata AS m
		// 				INNER JOIN launcher l ON l.id = m.parentId
		// 				WHERE l.eventid <> 0 AND m.parentId <> 18001 AND m.appId = 2098");
		// foreach($deleteQuery->result() as $row) {
		// 	$this->db->query("DELETE FROM tc_metadata WHERE id = $row->id");
		// }
	}

	function rsscron() {
		// $today = date("Y-m-d H:i:s",time());
		// $start = time();
		// $con = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin", "tapcrowd");
		// if (!$con)
		//   {
		//   die('Could not connect: ' . mysqli_error());
		//   }
		// mysqli_set_charset($con, "utf8");

		// $events = array();
		// $eventsQuery = mysqli_query($con, "SELECT e.id FROM event e INNER JOIN newssource n ON n.eventid = e.id WHERE e.deleted = 0");
		// while($event = mysqli_fetch_assoc($eventsQuery)) {
		// 	$events[] = (object) $event;
		// }
		// foreach($events as $event) {
		// 	$sources = array();
		// 	$sourcesQuery =  mysqli_query($con, "SELECT * FROM newssource WHERE eventid = $event->id");
		// 	while($source = mysqli_fetch_assoc($sourcesQuery)) {
		// 		$sources[] = (object) $source;

		// 	}
		// 	foreach($sources as $source) {
		// 		$deletetime = time() - $source->deleteafterdays * 3600 * 24;
		// 		$deletedate = date("Y-m-d H:i:s",$deletetime);
		// 		if($source->timestamp >= $deletedate || $source->deleteafterdays == 0) {
		// 			$insert = array();
		// 			$rss_url = $source->url;
		// 			$rssfeed = $this->rss_to_array($rss_url);
		// 			if($rssfeed) {
		// 				$sourcehash = $this->sourcehash($rssfeed);
		// 				mysqli_query($con, "UPDATE newssource SET hash = '$sourcehash', `timestamp` = '$today' WHERE id = $source->id");
		// 				if($sourcehash != $source->hash) {
		// 					foreach($rssfeed as $item) {
		// 						$hash = $item['hash'];					
									
		// 						$exists = 0;
		// 						$existsQuery = mysqli_query($con, "SELECT id, hash FROM newsitem WHERE hash='$hash' AND eventid = $event->id LIMIT 1");
		// 						$exists = mysqli_num_rows($existsQuery);
		// 						if($exists == 0) {
		// 							$title = $item['title'];
		// 							$txt = $item['description'];
		// 							$url = $item['link'];
		// 							if($item['pubDate']!= null && !empty($item['pubDate'])) {
		// 								$date = date("Y-m-d H:i:s", strtotime($item['pubDate']));
		// 							} else {
		// 								$date = date("Y-m-d H:i:s",time());
		// 							}
		// 							$image = '';
		// 							$imgpos = stripos($txt, '<img');
		// 							if($imgpos !== false) {
		// 								// var_dump($item['description']);
		// 								$imgend = stripos($txt, '</img>', $imgpos) + 6;
		// 								if($imgend === false) {
		// 									$imgend = stripos($txt, '/>', $imgpos) + 3;
		// 								}
		// 								if($imgend !== false && $imgpos !== false) {
		// 									$imglength = $imgend - $imgpos;
		// 									$image = substr($txt, $imgpos, $imglength);
		// 									preg_match('@src="([^"]+)"@' , $image , $imagematch);
		// 									if(isset($imagematch[1])) {
		// 										$image = $imagematch[1];
		// 									} else {
		// 										$image = '';
		// 									}
		// 								}
		// 							}
		// 							if(empty($image)) {
		// 								preg_match('@enclosure url="([^"]+)"@' , $txt , $imagematch);
		// 								if(isset($imagematch[1])) {
		// 									$image = $imagematch[1];
		// 								} else {
		// 									preg_match('@media:content url="([^"]+)"@' , $txt , $imagematch);
		// 									if(isset($imagematch[1])) {
		// 										$image = $imagematch[1];
		// 									} else {
		// 										$image = '';
		// 									}
		// 								}
		// 							}
									
		// 							if($source->deleteafterdays == 0 || strtotime($date) >= $deletetime) {
		// 								$insert[] = "('".$event->id."','".mysqli_real_escape_string($con, $title)."','".mysqli_real_escape_string($con, $txt)."','".$image."','".$url."','".$source->id."','".$date."','".$hash."')";
		// 							}
		// 						}
		// 					}
		// 					if(!empty($insert)) {
		// 						$insert = implode(',',$insert);
		// 						mysqli_query($con, "INSERT INTO newsitem (eventid, title, txt, image, url, sourceid, datum, hash) VALUES $insert");
		// 					}
		// 				}
		// 			}
		// 		}
		// 		if($source->deleteafterdays != 0) {
		// 			mysqli_query($con, "DELETE FROM newsitem WHERE sourceid = $source->id AND datum < '$deletedate'");
		// 		}
		// 	}
		// }







		// $venues = array();
		// $venuesQuery = mysqli_query($con, "SELECT e.id FROM venue e INNER JOIN newssource n ON n.venueid = e.id WHERE e.deleted = 0");
		// while($venue = mysqli_fetch_assoc($venuesQuery)) {
		// 	$venues[] = (object) $venue;
		// }
		// foreach($venues as $venue) {
		// 	$sources = array();
		// 	$sourcesQuery =  mysqli_query($con, "SELECT * FROM newssource WHERE venueid = $venue->id");
		// 	while($source = mysqli_fetch_assoc($sourcesQuery)) {
		// 		$sources[] = (object) $source;

		// 	}
		// 	foreach($sources as $source) {
		// 		$deletetime = time() - $source->deleteafterdays * 3600 * 24;
		// 		$deletedate = date("Y-m-d H:i:s",$deletetime);
		// 		if($source->timestamp >= $deletedate || $source->deleteafterdays == 0) {
		// 			$insert = array();
		// 			$rss_url = $source->url;
		// 			$rssfeed = $this->rss_to_array($rss_url);
		// 			if($rssfeed) {
		// 				$sourcehash = $this->sourcehash($rssfeed);
		// 				mysqli_query($con, "UPDATE newssource SET hash = '$sourcehash', `timestamp` = '$today' WHERE id = $source->id");
		// 				if($sourcehash != $source->hash) {
		// 					foreach($rssfeed as $item) {
		// 						$hash = $item['hash'];					
									
		// 						$exists = 0;
		// 						$existsQuery = mysqli_query($con, "SELECT id, hash FROM newsitem WHERE hash='$hash' AND venueid = $venue->id LIMIT 1");
		// 						$exists = mysqli_num_rows($existsQuery);
		// 						if($exists == 0) {
		// 							$title = $item['title'];
		// 							$txt = $item['description'];
		// 							$url = $item['link'];
		// 							if($item['pubDate']!= null && !empty($item['pubDate'])) {
		// 								$date = date("Y-m-d H:i:s", strtotime($item['pubDate']));
		// 							} else {
		// 								$date = date("Y-m-d H:i:s",time());
		// 							}
		// 							$image = '';
		// 							$imgpos = stripos($txt, '<img');
		// 							if($imgpos !== false) {
		// 								// var_dump($item['description']);
		// 								$imgend = stripos($txt, '</img>', $imgpos) + 6;
		// 								if($imgend === false) {
		// 									$imgend = stripos($txt, '/>', $imgpos) + 3;
		// 								}
		// 								if($imgend !== false && $imgpos !== false) {
		// 									$imglength = $imgend - $imgpos;
		// 									$image = substr($txt, $imgpos, $imglength);
		// 									preg_match('@src="([^"]+)"@' , $image , $imagematch);
		// 									if(isset($imagematch[1])) {
		// 										$image = $imagematch[1];
		// 									} else {
		// 										$image = '';
		// 									}
		// 								}
		// 							}
		// 							if(empty($image)) {
		// 								preg_match('@enclosure url="([^"]+)"@' , $txt , $imagematch);
		// 								if(isset($imagematch[1])) {
		// 									$image = $imagematch[1];
		// 								} else {
		// 									preg_match('@media:content url="([^"]+)"@' , $txt , $imagematch);
		// 									if(isset($imagematch[1])) {
		// 										$image = $imagematch[1];
		// 									} else {
		// 										$image = '';
		// 									}
		// 								}
		// 							}
									
		// 							if($source->deleteafterdays == 0 || strtotime($date) >= $deletetime) {
		// 								$insert[] = "('".$venue->id."','".mysqli_real_escape_string($con, $title)."','".mysqli_real_escape_string($con, $txt)."','".$image."','".$url."','".$source->id."','".$date."','".$hash."')";
		// 							}
		// 						}
		// 					}
		// 					if(!empty($insert)) {
		// 						$insert = implode(',',$insert);
		// 						mysqli_query($con, "INSERT INTO newsitem (venueid, title, txt, image, url, sourceid, datum, hash) VALUES $insert");
		// 					}
		// 				}
		// 			}
		// 		}
		// 		if($source->deleteafterdays != 0) {
		// 			mysqli_query($con, "DELETE FROM newsitem WHERE sourceid = $source->id AND datum < '$deletedate'");
		// 		}
		// 	}
		// }






		// $apps = array();
		// $appsQuery = mysqli_query($con, "SELECT e.id FROM app e INNER JOIN newssource n ON n.appid = e.id");
		// while($app = mysqli_fetch_assoc($appsQuery)) {
		// 	$apps[] = (object) $app;
		// }
		// foreach($apps as $app) {
		// 	$sources = array();
		// 	$sourcesQuery =  mysqli_query($con, "SELECT * FROM newssource WHERE appid = $app->id");
		// 	while($source = mysqli_fetch_assoc($sourcesQuery)) {
		// 		$sources[] = (object) $source;

		// 	}
		// 	foreach($sources as $source) {
		// 		$deletetime = time() - $source->deleteafterdays * 3600 * 24;
		// 		$deletedate = date("Y-m-d H:i:s",$deletetime);
		// 		if($source->timestamp >= $deletedate || $source->deleteafterdays == 0) {
		// 			$insert = array();
		// 			$rss_url = $source->url;
		// 			$rssfeed = $this->rss_to_array($rss_url);
		// 			if($rssfeed) {
		// 				$sourcehash = $this->sourcehash($rssfeed);
		// 				mysqli_query($con, "UPDATE newssource SET hash = '$sourcehash', `timestamp` = '$today' WHERE id = $source->id");
		// 				if($sourcehash != $source->hash) {
		// 					foreach($rssfeed as $item) {
		// 						$hash = $item['hash'];					
									
		// 						$exists = 0;
		// 						$existsQuery = mysqli_query($con, "SELECT id, hash FROM newsitem WHERE hash='$hash' AND appid = $app->id LIMIT 1");
		// 						$exists = mysqli_num_rows($existsQuery);
		// 						if($exists == 0) {
		// 							$title = $item['title'];
		// 							$txt = $item['description'];
		// 							$url = $item['link'];
		// 							if($item['pubDate']!= null && !empty($item['pubDate'])) {
		// 								$date = date("Y-m-d H:i:s", strtotime($item['pubDate']));
		// 							} else {
		// 								$date = date("Y-m-d H:i:s",time());
		// 							}
		// 							$image = '';
		// 							$imgpos = stripos($txt, '<img');
		// 							if($imgpos !== false) {
		// 								// var_dump($item['description']);
		// 								$imgend = stripos($txt, '</img>', $imgpos) + 6;
		// 								if($imgend === false) {
		// 									$imgend = stripos($txt, '/>', $imgpos) + 3;
		// 								}
		// 								if($imgend !== false && $imgpos !== false) {
		// 									$imglength = $imgend - $imgpos;
		// 									$image = substr($txt, $imgpos, $imglength);
		// 									preg_match('@src="([^"]+)"@' , $image , $imagematch);
		// 									if(isset($imagematch[1])) {
		// 										$image = $imagematch[1];
		// 									} else {
		// 										$image = '';
		// 									}
		// 								}
		// 							}
		// 							if(empty($image)) {
		// 								preg_match('@enclosure url="([^"]+)"@' , $txt , $imagematch);
		// 								if(isset($imagematch[1])) {
		// 									$image = $imagematch[1];
		// 								} else {
		// 									preg_match('@media:content url="([^"]+)"@' , $txt , $imagematch);
		// 									if(isset($imagematch[1])) {
		// 										$image = $imagematch[1];
		// 									} else {
		// 										$image = '';
		// 									}
		// 								}
		// 							}
									
		// 							if($source->deleteafterdays == 0 || strtotime($date) >= $deletetime) {
		// 								$insert[] = "('".$app->id."','".mysqli_real_escape_string($con, $title)."','".mysqli_real_escape_string($con, $txt)."','".$image."','".$url."','".$source->id."','".$date."','".$hash."')";
		// 							}
		// 						}
		// 					}
		// 					if(!empty($insert)) {
		// 						$insert = implode(',',$insert);
		// 						mysqli_query($con, "INSERT INTO newsitem (appid, title, txt, image, url, sourceid, datum, hash) VALUES $insert");
		// 					}
		// 				}
		// 			}
		// 		}
		// 		if($source->deleteafterdays != 0) {
		// 			mysqli_query($con, "DELETE FROM newsitem WHERE sourceid = $source->id AND datum < '$deletedate'");
		// 		}

		// 	}
		// }



		// $end = time();

		// echo 'cron finished in ' . ($end-$start);
	}

	function rss_to_array($url) {
		if(!empty($url)) {
			//check lastmodified with curl
			$curl = curl_init($url);
			//don't fetch the actual page, you only want headers
			curl_setopt($curl, CURLOPT_NOBODY, true);
			//stop it from outputting stuff to stdout
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			// attempt to retrieve the modification date
			curl_setopt($curl, CURLOPT_FILETIME, true);
			$result = curl_exec($curl);
			if ($result === false) {
			    return false;
			}
			$timestamp = curl_getinfo($curl, CURLINFO_FILETIME);
			if ($timestamp != -1) { //otherwise unknown
			    if($timestamp < time() - 3600 * 48) {
			    	//last modified older than 2 days ago -> don't import
			    	return false;
			    }
			} 
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$content = curl_exec($curl);
			if ($content === false) {
			    return false;
			}

			try {
				$rss = new SimpleXmlElement($content);
				if(!$rss) {
					return array();
				}
			} catch (Exception $e) {
				$rss = null;
			}

			$rss_array = array();
			$countitems = 0;
			foreach($rss->channel->item as $post) {
					$description = (string)$post->description;
			    	$namespace = $post->children('http://purl.org/rss/1.0/modules/content/');
			    	$content = (string)$namespace->encoded;
			    	if($content != null) {
			    		$description = $content;
			    	}  
					$data = array( 
							"title" 		=> (string)$post->title,
							"description" 	=> $description,
							"link"			=> (string)$post->link,
							"pubDate"		=> ((string) $post->pubDate!= null && (string) $post->pubDate != '') ? date("Y-m-d H:i:s",strtotime((string) $post->pubDate)) : date("Y-m-d H:i:s",time()),
							"hash"			=> md5((string)$post->title.$description.(string)$post->link)
						);

					array_push($rss_array, $data);

					if($countitems >= 20) {
						break;
					}
					$countitems++;
			}

			if(!empty($rss_array)) {
				return $rss_array;
			}
		}

		return false;
	}

	function sourcehash($array) {
		return md5($array[0]['title'].$array[0]['description'].$array[0]['link']);
	}


	function convertteam($appid, $venueid) {
		// $app = $this->app_model->get($appid);
		// _actionAllowed($app, 'app');

		// $groupid = $this->module_mdl->checkTeamCats($venueid, 'venue', $app);

		// $artists = $this->db->query("SELECT * FROM artist WHERE appid = $appid")->result();

		// foreach($artists as $a) {
		// 	$catalog = array(
		// 			'venueid' => $venueid,
		// 			'name' => $a->name,
		// 			'description' => $a->description,
		// 			'imageurl' => $a->imageurl,
		// 			'order' => $a->order
		// 		);

		// 	$catalogid = $this->general_model->insert('catalog', $catalog);

		// 	$groupitem = array(
		// 			'appid' => $appid,
		// 			'venueid' => $venueid,
		// 			'groupid' => $groupid,
		// 			'itemtable' => 'catalog',
		// 			'itemid' => $catalogid
		// 		);
		// 	$this->general_model->insert('groupitem', $groupitem);
		// }
	}

	function generateApiTokens() {
		// $apps = $this->db->query("SELECT * FROM app WHERE token = '123456'")->result();
		// foreach($apps as $app) {
		// 	$token = md5($app->name.$app->subdomain.'tapcrowdapitoken123456');
		// 	$this->db->query("UPDATE app SET token = '$token' WHERE id = $app->id");
		// }
	}

	function updateVenueImages($appid, $tag) {
		// $venueids = array();
		// // $venues = $this->db->query("SELECT id FROM venue INNER JOIN appvenue ON appvenue.venueid = venue.id WHERE appvenue.appid = $appid")->result();
		// $tagsQuery = $this->db->query("SELECT venueid FROM tag WHERE venueid <> 0 AND appid = $appid AND tag = '$tag'")->result();
		// foreach($tagsQuery as $t) {
		// 	$venueids[] = $t->venueid;
		// }

		// if(!empty($venueids)) {
		// 	$venueids = implode($venueids, ',');
		// 	echo "UPDATE venue SET image1 = 'upload/moduleimages/17716/kantoor.png' WHERE id IN ($venueids)";

		// 	$this->db->query("UPDATE venue SET image1 = 'upload/moduleimages/17716/kantoor.png' WHERE id IN ($venueids)");
		// }
	}

	function eshre() {
		// echo '<pre>';
		// $appid = 2579;
		// $eventid = 4956;
		// // $appid = 2353;
		// // $eventid = 4800;


		// $sessionlauncherid = $this->db->query("SELECT id FROM launcher WHERE moduletypeid = 10 AND eventid = $eventid LIMIT 1")->row()->id;

		// $url = 'http://cm.eshre.eu/cmScientificProgramme/export/event_LON2013.xml';

		// $curl = curl_init($url);
		// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		// $content = curl_exec($curl);
		// if ($content === false) {
		//    	echo 'no content';
		// }

		// $rss = $this->xml2array($content);
		// $event = $rss['ns:event'];
		// $persons = $event['ns:persons']['ns:person'];
		// $classifiers = $event['ns:classifiers']['ns:classifier'];
		// $sessiontypes = array();
		// $abstractkeywords = array();
		// $places = $event['ns:floorplans']['ns:floorplan']['ns:places']['ns:place'];
		// $locations = array();
		// foreach($places as $p) {
		// 	$locations[$p['attr']['id']] = $p;
		// }
		// $abstracts1 = $event['ns:abstracts']['ns:abstract'];
		// foreach($classifiers as $c) {
		// 	if(isset($c['ns:type']) && $c['ns:type'] == 'session_type' && isset($c['attr']['id'])) {
		// 		//sessiontypes are linked to session by session['classifications'] (sessiontype_00328)
		// 		$sessiontypes[$c['attr']['id']] = $c;
		// 	} elseif(isset($c['ns:type']) && $c['ns:type'] == 'abstract_keyword' && isset($c['attr']['id'])) {
		// 		$abstractkeywords[$c['attr']['id']] = $c;
		// 	}
		// }

		// $abstracts = array();
		// foreach($abstracts1 as $a) {
		// 	$abstracts[$a['attr']['id']] = $a;
		// }

		// $abstractmetadataname = 'meta_session_abstract';
		// $abstractmetadataSessionExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $sessionlauncherid AND qname = '$abstractmetadataname' LIMIT 1");
		// if($abstractmetadataSessionExists->num_rows() == 0) {
		// 	$abstractmetaId = $this->general_model->insert('tc_metadata', array(
		// 			'appId' => $appid,
		// 			'parentType' => 'launcher',
		// 			'parentId' => $sessionlauncherid,
		// 			'type' => 'text',
		// 			'qname' => $abstractmetadataname,
		// 			'name' => 'Abstract',
		// 			'sortorder' => 8
		// 		));
		// } else {
		// 	$abstractmetaId = $abstractmetadataSessionExists->row()->id;
		// }

		// $abstractRolemetadataname = 'meta_session_abstract_coauthor';
		// $abstractRolemetadataSessionExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $sessionlauncherid AND qname = '$abstractRolemetadataname' LIMIT 1");
		// if($abstractRolemetadataSessionExists->num_rows() == 0) {
		// 	$abstractRolemetaId = $this->general_model->insert('tc_metadata', array(
		// 			'appId' => $appid,
		// 			'parentType' => 'launcher',
		// 			'parentId' => $sessionlauncherid,
		// 			'type' => 'text',
		// 			'qname' => $abstractRolemetadataname,
		// 			'name' => 'Coauthor',
		// 			'sortorder' => 4
		// 		));
		// } else {
		// 	$abstractRolemetaId = $abstractRolemetadataSessionExists->row()->id;
		// }

		// $speakerHeaderName = 'meta_sessions_speaker_header';
		// $speakerHeaderExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $sessionlauncherid AND qname = '$speakerHeaderName' LIMIT 1");
		// if($speakerHeaderExists->num_rows() == 0) {
		// 	$speakerHeaderId = $this->general_model->insert('tc_metadata', array(
		// 			'appId' => $appid,
		// 			'parentType' => 'launcher',
		// 			'parentId' => $sessionlauncherid,
		// 			'type' => 'header',
		// 			'qname' => $speakerHeaderName,
		// 			'name' => 'Speaker header',
		// 			'sortorder' => 7
		// 		));
		// } else {
		// 	$speakerHeaderId = $speakerHeaderExists->row()->id;
		// }

		// $chairmanHeaderName = 'meta_sessions_chairman_header';
		// $chairmanHeaderExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $sessionlauncherid AND qname = '$chairmanHeaderName' LIMIT 1");
		// if($chairmanHeaderExists->num_rows() == 0) {
		// 	$chairmanHeaderId = $this->general_model->insert('tc_metadata', array(
		// 			'appId' => $appid,
		// 			'parentType' => 'launcher',
		// 			'parentId' => $sessionlauncherid,
		// 			'type' => 'header',
		// 			'qname' => $chairmanHeaderName,
		// 			'name' => 'Chairman header',
		// 			'sortorder' => 5
		// 		));
		// } else {
		// 	$chairmanHeaderId = $chairmanHeaderExists->row()->id;
		// }

		// $coauthorHeaderName = 'meta_sessions_coauthor_header';
		// $coauthorHeaderExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $sessionlauncherid AND qname = '$coauthorHeaderName' LIMIT 1");
		// if($coauthorHeaderExists->num_rows() == 0) {
		// 	$coauthorHeaderId = $this->general_model->insert('tc_metadata', array(
		// 			'appId' => $appid,
		// 			'parentType' => 'launcher',
		// 			'parentId' => $sessionlauncherid,
		// 			'type' => 'header',
		// 			'qname' => $coauthorHeaderName,
		// 			'name' => 'Coauthor header',
		// 			'sortorder' => 3
		// 		));
		// } else {
		// 	$coauthorHeaderId = $coauthorHeaderExists->row()->id;
		// }

		// $coauthorGroupId = 0;
		// $coauthorGroupExists = $this->db->query("SELECT id FROM `group` WHERE eventid = $eventid AND name = 'coauthors' LIMIT 1");
		// if($coauthorGroupExists->num_rows() > 0) {
		// 	$coauthorGroupId = $coauthorGroupExists->row()->id;
		// }

		// //sessiongroups
		// foreach($sessiontypes as &$s) {
		// 	$name = $s['ns:name'];
		// 	$exists = $this->db->query("SELECT id FROM sessiongroup WHERE eventid = $eventid AND name = '".mysql_real_escape_string($name)."' LIMIT 1");
		// 	if($exists->num_rows() == 0) {
		// 		$sessiongroupid = $this->general_model->insert('sessiongroup', array(
		// 				'eventid' => $eventid,
		// 				'name' => $s['ns:name'],
		// 			));
		// 	} else {
		// 		$sessiongroupid = $exists->row()->id;
		// 	}

		// 	$s['id'] = $sessiongroupid;
		// }

		// //speakers
		// $speakers = array();
		// foreach($persons as &$p) {
		// 	$name = $p['ns:lastName'];
		// 	$description = '';
		// 	if(isset($p['ns:subInfo']) && !is_array($p['ns:subInfo']) && !empty($p['ns:subInfo'])) {
		// 		$description = $p['ns:subInfo'];
		// 	}

		// 	$exists = $this->db->query("SELECT id FROM speaker WHERE eventid = $eventid AND name = '".mysql_real_escape_string($name)."' LIMIT 1");
		// 	if($exists->num_rows() == 0) {
		// 		$speakerid = $this->general_model->insert('speaker', array(
		// 				'eventid' => $eventid,
		// 				'name' => $p['ns:lastName'],
		// 				'description' => $description,
		// 				// 'company' => $company
		// 			));
		// 	} else {
		// 		$speakerid = $exists->row()->id;
		// 	}

		// 	$p['id'] = $speakerid;
		// 	$speakers[$p['attr']['id']] = $p;
		// }

		// //sessions
		// $timeslots = $event['ns:timeslots'];
		// foreach($timeslots as &$days) {
		// 	//array of days
		// 	foreach($days as &$day) {
		// 		//day
		// 		foreach($day['ns:timeslots'] as &$subdays) {
		// 			// subgroups like morning, afternoon..
		// 			foreach($subdays as &$subday) {
		// 				// subgroup
		// 				if(isset($subdays['ns:timeslots'])) {
		// 					$subday['ns:timeslots'] = $subdays['ns:timeslots'];
		// 				}
		// 				foreach($subday['ns:timeslots'] as &$sessions) {
		// 					//sessions per subgroup
		// 					foreach($sessions as &$session) {
		// 						//session
		// 						if(isset($sessions['ns:name'])) {
		// 							$session = $sessions;
		// 						}
		// 						if(isset($session['ns:name']) && !empty($session['ns:name'])) {
		// 							//session data
		// 							$name = $session['ns:name'];

		// 							$description = '';
		// 							if(isset($session['ns:subInfo']) && !is_array($session['ns:subInfo']) && !empty($session['ns:subInfo'])) {
		// 								$description = $session['ns:subInfo'];
		// 							}
		// 							$sessiongroup['id'] = 0;
		// 							// var_dump($session);
		// 							if(isset($session['ns:classifications']) && isset($session['ns:classifications']['ns:classification'])) {
		// 								$sessiongroupclassification = $session['ns:classifications']['ns:classification'];
		// 								if(is_array($sessiongroupclassification)) {
		// 									if(isset($sessiongroupclassification[0]) && substr($sessiongroupclassification[0], 0, 11) == 'sessiontype') {
		// 										$sessiongroupclassification = $sessiongroupclassification[0];
		// 									} elseif(isset($sessiongroupclassification[1]) && substr($sessiongroupclassification[1], 0, 10) == 'sessiontype') {
		// 										$sessiongroupclassification = $sessiongroupclassification[1];
		// 									}
		// 								}
		// 								if(isset($sessiontypes[$sessiongroupclassification])) {
		// 									$sessiongroup = $sessiontypes[$sessiongroupclassification];
		// 								}
		// 							}
		// 							$location = '';
		// 							if(isset($session['ns:locations']['ns:location']) && isset($locations[$session['ns:locations']['ns:location']])) {
		// 								$location = $locations[$session['ns:locations']['ns:location']]['ns:name'];
		// 							}
		// 							$starttime = '';
		// 							if(isset($session['ns:start'])) {
		// 								$starttime = substr($session['ns:start'], 0, 10) . ' ' . substr($session['ns:start'], 11);
		// 							}
		// 							$endtime = '';
		// 							if(isset($session['ns:start'])) {
		// 								$endtime = substr($session['ns:end'], 0, 10) . ' ' . substr($session['ns:end'], 11);
		// 							}
		// 							$order = 0;
		// 							if(isset($session['attr']['ordering'])) {
		// 								$order = (int)$session['attr']['ordering'];
		// 							}

		// 							$exists = $this->db->query("SELECT id FROM session WHERE eventid = $eventid AND name = '".mysql_real_escape_string($name)."' AND sessiongroupid = '".mysql_real_escape_string($sessiongroup['id'])."' LIMIT 1");
		// 							if($exists->num_rows() == 0) {
		// 								$sessionid = $this->general_model->insert('session', array(
		// 										'eventid' => $eventid,
		// 										'sessiongroupid' => $sessiongroup['id'],
		// 										'name' => $session['ns:name'],
		// 										'description' => $description,
		// 										'location' => $location,
		// 										'starttime' => $starttime,
		// 										'endtime' => $endtime
		// 									));
		// 							} else {
		// 								$sessionid = $exists->row()->id;
		// 								//remove this delete query if other metavalues will be added to sessions
		// 								$this->db->query("DELETE FROM tc_metavalues WHERE parentType = 'session' AND parentId = $sessionid");
		// 							}

		// 							$session['id'] = $sessionid;

		// 							//session speakers
		// 							$this->roles($sessionid, $session, $eventid, $appid, $speakers, $sessionlauncherid, $speakerHeaderId, $chairmanHeaderId);

		// 							if(isset($session['ns:subjects']['ns:subject'])) {
		// 								if(is_array($session['ns:subjects']['ns:subject'])) {
		// 									$abstractloop = $session['ns:subjects']['ns:subject'];
		// 								} else {
		// 									$abstractloop[] = $session['ns:subjects']['ns:subject'];
		// 								}

		// 								foreach($abstractloop as $a) {
		// 									if(isset($abstracts[$a])) {
		// 										if($sessiongroup['id'] == '15630') {
		// 											$abstractsessionexists = $this->db->query("SELECT id FROM session WHERE eventid = $eventid AND parentid = $sessionid AND name = '".mysql_real_escape_string($abstracts[$a]['ns:code'])."' AND sessiongroupid = '".mysql_real_escape_string($sessiongroup['id'])."' LIMIT 1");
		// 											if($abstractsessionexists->num_rows() == 0) {
		// 												$abstractsessionid = $this->general_model->insert('session', array(
		// 														'eventid' => $eventid,
		// 														'sessiongroupid' => $sessiongroup['id'],
		// 														'name' => $abstracts[$a]['ns:code'],
		// 														'parentid' => $sessionid
		// 													));
		// 											} else {
		// 												$abstractsessionid = $abstractsessionexists->row()->id;
		// 											}
		// 										} else {
		// 											$abstractsessionid = $sessionid;
		// 										}

		// 										//abstracts
		// 										$abstractmetavalueExists = $this->db->query("SELECT metaId, value FROM tc_metavalues WHERE metaId = $abstractmetaId AND parentType = 'session' AND parentId = $abstractsessionid LIMIT 1");
		// 										if($abstractmetavalueExists->num_rows() == 0) {
		// 											$this->general_model->insert('tc_metavalues', array(
		// 													'metaId' => $abstractmetaId, 
		// 													'parentType' => 'session', 
		// 													'parentId' => $abstractsessionid,
		// 													'name' => 'Abstract',
		// 													'type' => 'text',
		// 													'value' => $abstracts[$a]['ns:info'],
		// 												));
		// 										} else {
		// 											$metavalueExistsRow = $abstractmetavalueExists->row();
		// 											$this->db->query("UPDATE tc_metavalues SET value = '".mysql_real_escape_string($metavalueExistsRow->value) . ', ' .mysql_real_escape_string($speakers[$sessionspeaker['ns:actor']]['ns:lastName'])."' WHERE metaId = $metavalueExistsRow->metaId AND parentType = 'session' AND parentId = $abstractsessionid");
		// 										}

		// 										//abstract coauthors
		// 										if(isset($abstracts[$a]['ns:role'])) {
		// 											$coauthorHeaderValueExists = $this->db->query("SELECT metaId, value FROM tc_metavalues WHERE metaId = $coauthorHeaderId AND parentType = 'session' AND parentId = $abstractsessionid LIMIT 1");
		// 											if($coauthorHeaderValueExists->num_rows() == 0) {
		// 												$this->general_model->insert('tc_metavalues', array(
		// 														'metaId' => $coauthorHeaderId, 
		// 														'parentType' => 'session', 
		// 														'parentId' => $abstractsessionid,
		// 														'name' => 'Coauthor header',
		// 														'type' => 'header',
		// 														'value' => 'Coauthors'
		// 													));
		// 											}
		// 											foreach($abstracts[$a]['ns:role'] as $coauthor) {
		// 												if(isset($speakers[$coauthor['ns:actor']]['ns:lastName'])) {
		// 													$abstractRolemetavalueExists = $this->db->query("SELECT metaId, value FROM tc_metavalues WHERE metaId = $abstractRolemetaId AND parentType = 'session' AND parentId = $abstractsessionid LIMIT 1");
		// 													if($abstractRolemetavalueExists->num_rows() == 0) {
		// 														$this->general_model->insert('tc_metavalues', array(
		// 																'metaId' => $abstractRolemetaId, 
		// 																'parentType' => 'session', 
		// 																'parentId' => $abstractsessionid,
		// 																'name' => 'Coauthor',
		// 																'type' => 'text',
		// 																'value' => $speakers[$coauthor['ns:actor']]['ns:lastName'],
		// 															));
		// 													} else {
		// 														$abstractRolemetavalueExistsRow = $abstractRolemetavalueExists->row();
		// 														$this->db->query("UPDATE tc_metavalues SET value = '".mysql_real_escape_string($abstractRolemetavalueExistsRow->value) . ', ' .mysql_real_escape_string($speakers[$coauthor['ns:actor']]['ns:lastName'])."' WHERE metaId = $abstractRolemetavalueExistsRow->metaId AND parentType = 'session' AND parentId = $abstractsessionid");
		// 													}

		// 													if($coauthorGroupId != 0) {
		// 														$coauthorid = $speakers[$coauthor['ns:actor']]['id'];
		// 														$authorIsInGroup = $this->db->query("SELECT id FROM groupitem WHERE groupid = $coauthorGroupId AND itemtable = 'speaker' AND itemid = $coauthorid LIMIT 1");
		// 														if($authorIsInGroup->num_rows() == 0) {
		// 															$this->general_model->insert('groupitem', array(
		// 																	'appid' => $appid,
		// 																	'eventid' => $eventid,
		// 																	'groupid' => $coauthorGroupId,
		// 																	'itemtable' => 'speaker', 
		// 																	'itemid' => $coauthorid
		// 																));
		// 														}
		// 													}
		// 												}
		// 											}
		// 										}
		// 									}
		// 								}
		// 							}

		// 							//subsessions
		// 							if(isset($session['ns:timeslots']) && is_array($session['ns:timeslots'])) {
		// 								foreach($session['ns:timeslots'] as $subsession1) {
		// 									foreach($subsession1 as $subsession) {
		// 										if(isset($subsession1['ns:name'])) {
		// 											$subsession = $subsession1;
		// 										}
		// 										if(isset($subsession['ns:name']) && !empty($subsession['ns:name'])) {
		// 											$subsessionname = $subsession['ns:name'];
		// 											$subsessiondescription = '';
		// 											if(isset($subsession['ns:subInfo']) && !is_array($subsession['ns:subInfo']) && !empty($subsession['ns:subInfo'])) {
		// 												$subsessiondescription = $subsession['ns:subInfo'];
		// 											}
		// 											$subsessionlocation = $location;
		// 											if(isset($subsession['ns:locations']['ns:location']) && isset($locations[$subsession['ns:locations']['ns:location']])) {
		// 												$subsessionlocation = $locations[$subsession['ns:locations']['ns:location']]['ns:name'];
		// 											}
		// 											$subsessionstarttime = $starttime;
		// 											if(isset($subsession['ns:start'])) {
		// 												$subsessionstarttime = substr($subsession['ns:start'], 0, 10) . ' ' . substr($subsession['ns:start'], 11);
		// 											}
		// 											$subsessionendtime = $endtime;
		// 											if(isset($subsession['ns:start'])) {
		// 												$subsessionendtime = substr($subsession['ns:end'], 0, 10) . ' ' . substr($subsession['ns:end'], 11);
		// 											}
		// 											$subsessionorder = $order;
		// 											if(isset($subsession['attr']['ordering'])) {
		// 												$subsessionorder = (int)$subsession['attr']['ordering'];
		// 											}

		// 											$subsessiongroup['id'] = 0;
		// 											// var_dump($subsession['ns:classifications']);
		// 											if(isset($subsession['ns:classifications']) && isset($subsession['ns:classifications']['ns:classification'])) {
		// 												$subsessiongroupclassification = $subsession['ns:classifications']['ns:classification'];
		// 												if(isset($sessiontypes[$subsessiongroupclassification])) {
		// 													$subsessiongroup = $sessiontypes[$subsessiongroupclassification];
		// 												}
		// 											}

		// 											$exists = $this->db->query("SELECT id FROM session WHERE eventid = $eventid AND name = '".mysql_real_escape_string($subsessionname)."' AND sessiongroupid = '".mysql_real_escape_string($sessiongroup['id'])."' AND parentid = $sessionid LIMIT 1");
		// 											if($exists->num_rows() == 0) {
		// 												$subsessionid = $this->general_model->insert('session', array(
		// 														'eventid' => $eventid,
		// 														'sessiongroupid' => $sessiongroup['id'],
		// 														'name' => $subsessionname,
		// 														'description' => $subsessiondescription,
		// 														'location' => $subsessionlocation,
		// 														'starttime' => $subsessionstarttime,
		// 														'endtime' => $subsessionendtime,
		// 														'parentid' => $sessionid
		// 													));
		// 											} else {
		// 												$subsessionid = $exists->row()->id;
		// 												//remove this delete query if other metavalues will be added to sessions
		// 												$this->db->query("DELETE FROM tc_metavalues WHERE parentType = 'session' AND parentId = $subsessionid");
		// 											}

		// 											$this->roles($subsessionid, $subsession, $eventid, $appid, $speakers, $sessionlauncherid, $speakerHeaderId, $chairmanHeaderId);

		// 											if(isset($subsession['ns:subjects']['ns:subject'])) {
		// 												if(isset($abstracts[$subsession['ns:subjects']['ns:subject']])) {
		// 													//abstract coauthors
		// 													$abstractcoauthors = '';
		// 													if(isset($abstracts[$subsession['ns:subjects']['ns:subject']]['ns:role'])) {
		// 														$coauthorHeaderValueExists = $this->db->query("SELECT metaId, value FROM tc_metavalues WHERE metaId = $coauthorHeaderId AND parentType = 'session' AND parentId = $subsessionid LIMIT 1");
		// 														if($coauthorHeaderValueExists->num_rows() == 0) {
		// 															$this->general_model->insert('tc_metavalues', array(
		// 																	'metaId' => $coauthorHeaderId, 
		// 																	'parentType' => 'session', 
		// 																	'parentId' => $subsessionid,
		// 																	'name' => 'Coauthor header',
		// 																	'type' => 'header',
		// 																	'value' => 'Coauthors'
		// 																));
		// 														}
		// 														foreach($abstracts[$subsession['ns:subjects']['ns:subject']]['ns:role'] as $coauthor) {
		// 															if(isset($speakers[$coauthor['ns:actor']]['ns:lastName'])) {
		// 																$abstractRolemetavalueExists = $this->db->query("SELECT metaId, value FROM tc_metavalues WHERE metaId = $abstractRolemetaId AND parentType = 'session' AND parentId = $subsessionid LIMIT 1");
		// 																if($abstractRolemetavalueExists->num_rows() == 0) {
		// 																	$this->general_model->insert('tc_metavalues', array(
		// 																			'metaId' => $abstractRolemetaId, 
		// 																			'parentType' => 'session', 
		// 																			'parentId' => $subsessionid,
		// 																			'name' => 'Coauthor',
		// 																			'type' => 'text',
		// 																			'value' => $speakers[$coauthor['ns:actor']]['ns:lastName'],
		// 																		));
		// 																} else {
		// 																	$abstractRolemetavalueExistsRow = $abstractRolemetavalueExists->row();
		// 																	$this->db->query("UPDATE tc_metavalues SET value = '".mysql_real_escape_string($abstractRolemetavalueExistsRow->value) . ', ' .mysql_real_escape_string($speakers[$coauthor['ns:actor']]['ns:lastName'])."' WHERE metaId = $abstractRolemetavalueExistsRow->metaId AND parentType = 'session' AND parentId = $subsessionid");
		// 																}

		// 																if(!empty($abstractcoauthors)) {
		// 																	$abstractcoauthors .= ', <br />';
		// 																}
		// 																$abstractcoauthors .= '* '.$speakers[$coauthor['ns:actor']]['ns:lastName'] . ' ('.$speakers[$coauthor['ns:actor']]['ns:subInfo'].')';

		// 																if($coauthorGroupId != 0) {
		// 																	$coauthorid = $speakers[$coauthor['ns:actor']]['id'];
		// 																	$authorIsInGroup = $this->db->query("SELECT id FROM groupitem WHERE groupid = $coauthorGroupId AND itemtable = 'speaker' AND itemid = $coauthorid LIMIT 1");
		// 																	if($authorIsInGroup->num_rows() == 0) {
		// 																		$this->general_model->insert('groupitem', array(
		// 																				'appid' => $appid,
		// 																				'eventid' => $eventid,
		// 																				'groupid' => $coauthorGroupId,
		// 																				'itemtable' => 'speaker', 
		// 																				'itemid' => $coauthorid
		// 																			));
		// 																	}
		// 																}

		// 																$linkSessionCoauthor = $this->db->query("SELECT id FROM speaker_session WHERE speakerid = $coauthorid AND sessionid = $subsessionid LIMIT 1");
		// 																if($linkSessionCoauthor->num_rows() == 0) {
		// 																	$this->general_model->insert('speaker_session', array(
		// 																			'eventid' => $eventid,
		// 																			'speakerid' => $coauthorid,
		// 																			'sessionid' => $subsessionid
		// 																		));
		// 																}
		// 															}
		// 														}
		// 													}
		// 													//abstracts
		// 													$abstractvalue = $abstracts[$subsession['ns:subjects']['ns:subject']]['ns:info'];
		// 													// if(!empty($abstractcoauthors)) {
		// 													// 	$abstractvalue = '<p>Authors: <br />'.$abstractcoauthors.'</p>'.$abstractvalue;
		// 													// }
															
		// 													$abstractmetavalueExists = $this->db->query("SELECT metaId, value FROM tc_metavalues WHERE metaId = $abstractmetaId AND parentType = 'session' AND parentId = $subsessionid LIMIT 1");
		// 													if($abstractmetavalueExists->num_rows() == 0) {
		// 														$this->general_model->insert('tc_metavalues', array(
		// 																'metaId' => $abstractmetaId, 
		// 																'parentType' => 'session', 
		// 																'parentId' => $subsessionid,
		// 																'name' => 'Abstract',
		// 																'type' => 'text',
		// 																'value' => $abstractvalue,
		// 															));
		// 													} else {
		// 														$abstractmetavalueExistsRow = $abstractmetavalueExists->row();
		// 														$this->db->query("UPDATE tc_metavalues SET value = '".mysql_real_escape_string($abstractvalue)."' WHERE metaId = $abstractmetavalueExistsRow->metaId AND parentType = 'session' AND parentId = $subsessionid");
		// 													}
		// 												}
		// 											}
		// 										}

		// 										if(isset($subsession1['ns:name'])) {
		// 											break;
		// 										}
		// 									}
		// 								}
		// 							}
		// 						}
		// 						if(isset($sessions['ns:name'])) {
		// 							break;
		// 						}
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// }

		// $this->db->query("UPDATE `group` SET name = 'Speaker' WHERE eventid = $eventid AND name = 'speaker'");
		// $this->db->query("UPDATE `group` SET name = 'Chairman' WHERE eventid = $eventid AND name = 'chairman'");
		// $this->db->query("UPDATE `group` SET name = 'Coauthors' WHERE eventid = $eventid AND name = 'coauthors'");
		
		// // print_r($sessions);
		// echo '</pre>';
		// exit;

	}

	function roles($sessionid, $session, $eventid, $appid, $speakers, $sessionlauncherid, $speakerHeaderId, $chairmanHeaderId) {
		// if(isset($session['ns:role'])) {
		// 	$speakerHeaderValueExists = $this->db->query("SELECT metaId, value FROM tc_metavalues WHERE metaId = $speakerHeaderId AND parentType = 'session' AND parentId = $sessionid LIMIT 1");
		// 	if($speakerHeaderValueExists->num_rows() == 0) {
		// 		$this->general_model->insert('tc_metavalues', array(
		// 				'metaId' => $speakerHeaderId, 
		// 				'parentType' => 'session', 
		// 				'parentId' => $sessionid,
		// 				'name' => 'Speaker header',
		// 				'type' => 'header',
		// 				'value' => 'Speakers'
		// 			));
		// 	}

		// 	$chairmanHeaderValueExists = $this->db->query("SELECT metaId, value FROM tc_metavalues WHERE metaId = $chairmanHeaderId AND parentType = 'session' AND parentId = $sessionid LIMIT 1");
		// 	if($chairmanHeaderValueExists->num_rows() == 0) {
		// 		$this->general_model->insert('tc_metavalues', array(
		// 				'metaId' => $chairmanHeaderId, 
		// 				'parentType' => 'session', 
		// 				'parentId' => $sessionid,
		// 				'name' => 'Chairman header',
		// 				'type' => 'header',
		// 				'value' => 'Chairman'
		// 			));
		// 	}
			
		// 	foreach($session['ns:role'] as $sessionspeaker) {
		// 		if(isset($session['ns:role']['ns:actor'])) {
		// 			$sessionspeaker = $session['ns:role'];
		// 		}
		// 		if(isset($speakers[$sessionspeaker['ns:actor']])) {
		// 			$speakerid = $speakers[$sessionspeaker['ns:actor']]['id'];
		// 			$speakersessionrexists = $this->db->query("SELECT id FROM speaker_session WHERE sessionid = $sessionid AND speakerid = $speakerid LIMIT 1");
		// 			if($speakersessionrexists->num_rows() == 0) {
		// 				$this->general_model->insert('speaker_session', array(
		// 					'eventid' => $eventid,
		// 					'speakerid' => $speakerid,
		// 					'sessionid' => $sessionid
		// 				));
		// 			}
		// 			if(isset($sessionspeaker['ns:type'])) {
		// 				//speaker groups
		// 				$role = substr($sessionspeaker['ns:type'], 5);
		// 				$groupexists = $this->db->query("SELECT id FROM `group` WHERE eventid = $eventid AND name = '$role' LIMIT 1");
		// 				if($groupexists->num_rows > 0) {
		// 					$groupid = $groupexists->row()->id;
		// 				} else {
		// 					$parentSpeakers = $this->db->query("SELECT id FROM `group` WHERE eventid = $eventid AND name = 'speakercategories' LIMIT 1");
		// 					if($parentSpeakers->num_rows() == 0) {
		// 						$parentSpeakersId = $this->general_model->insert('group', array(
		// 							'appid' => $appid,
		// 							'eventid' => $eventid,
		// 							'name' => 'speakercategories'
		// 						));
		// 					} else {
		// 						$parentSpeakersId = $parentSpeakers->row()->id;
		// 					}
		// 					$groupid = $this->general_model->insert('group', array(
		// 							'appid' => $appid,
		// 							'eventid' => $eventid,
		// 							'name' => $role,
		// 							'parentid' => $parentSpeakersId
		// 						));
		// 				}

		// 				$roleitemexists = $this->db->query("SELECT id FROM groupitem WHERE groupid = $groupid AND itemtable = 'speaker' AND itemid = $speakerid LIMIT 1");
		// 				if($roleitemexists->num_rows() == 0) {
		// 					$this->general_model->insert('groupitem', array(
		// 							'appid' => $appid,
		// 							'eventid' => $eventid,
		// 							'groupid' => $groupid,
		// 							'itemtable' => 'speaker',
		// 							'itemid' => $speakerid
		// 						));
		// 				}

		// 				//session speaker metadata
		// 				$metadataname = 'meta_session_'.$role;
		// 				$metadataSessionExists = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = $sessionlauncherid AND qname = '$metadataname' LIMIT 1");
		// 				if($metadataSessionExists->num_rows() == 0) {
		// 					$metaId = $this->general_model->insert('tc_metadata', array(
		// 							'appId' => $appid,
		// 							'parentType' => 'launcher',
		// 							'parentId' => $sessionlauncherid,
		// 							'type' => 'text',
		// 							'qname' => 'meta_session_'.$role,
		// 							'name' => $role
		// 						));
		// 				} else {
		// 					$metaId = $metadataSessionExists->row()->id;
		// 				}

		// 				$metavalueExists = $this->db->query("SELECT metaId, value FROM tc_metavalues WHERE metaId = $metaId AND parentType = 'session' AND parentId = $sessionid LIMIT 1");
		// 				if($metavalueExists->num_rows() == 0) {
		// 					$this->general_model->insert('tc_metavalues', array(
		// 							'metaId' => $metaId, 
		// 							'parentType' => 'session', 
		// 							'parentId' => $sessionid,
		// 							'name' => $role,
		// 							'type' => 'text',
		// 							'value' => $speakers[$sessionspeaker['ns:actor']]['ns:lastName']
		// 						));
		// 				} else {
		// 					$metavalueExistsRow = $metavalueExists->row();
		// 					$this->db->query("UPDATE tc_metavalues SET value = '".mysql_real_escape_string($metavalueExistsRow->value) . ', ' .mysql_real_escape_string($speakers[$sessionspeaker['ns:actor']]['ns:lastName'])."' WHERE metaId = $metavalueExistsRow->metaId AND parentType = 'session' AND parentId = $sessionid");
		// 				}
		// 			}
		// 		}

		// 		if(isset($session['ns:role']['ns:actor'])) {
		// 			break;
		// 		}
		// 	}
		// }
	}

	function xml2array($contents, $get_attributes=1, $priority = 'tag') {
		if(!$contents) return array();

		if(!function_exists('xml_parser_create')) {
	        //print "'xml_parser_create()' function not found!";
			return array();
		}

	    //Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
	    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
	    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
	    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
	    xml_parse_into_struct($parser, trim($contents), $xml_values);
	    xml_parser_free($parser);

	    if(!$xml_values) return;//Hmm...

	    //Initializations
	    $xml_array = array();
	    $parents = array();
	    $opened_tags = array();
	    $arr = array();

	    $current = &$xml_array; //Refference

	    //Go through the tags.
	    $repeated_tag_index = array();//Multiple tags with same name will be turned into an array
	    foreach($xml_values as $data) {
	        unset($attributes,$value);//Remove existing values, or there will be trouble

	        //This command will extract these variables into the foreach scope
	        // tag(string), type(string), level(int), attributes(array).
	        extract($data);//We could use the array by itself, but this cooler.

	        $result = array();
	        $attributes_data = array();
	        
	        if(isset($value)) {
	        	if($priority == 'tag') $result = $value;
	            else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
	        }

	        //Set the attributes too.
	        if(isset($attributes) and $get_attributes) {
	        	foreach($attributes as $attr => $val) {
	        		$result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
	            }
	        }

	        //See tag status and do the needed.
	        if($type == "open") {//The starting of the tag '<tag>'
	        $parent[$level-1] = &$current;
	            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
	            	$current[$tag] = $result;
	            	if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
	            	$repeated_tag_index[$tag.'_'.$level] = 1;

	            	$current = &$current[$tag];

	            } else { //There was another element with the same tag name

	                if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
	                	$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
	                	$repeated_tag_index[$tag.'_'.$level]++;
	                } else {//This section will make the value an array if multiple tags with the same name appear together
	                    $current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
	                    $repeated_tag_index[$tag.'_'.$level] = 2;
	                    
	                    if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
	                    	$current[$tag]['0_attr'] = $current[$tag.'_attr'];
	                    	unset($current[$tag.'_attr']);
	                    }

	                }
	                $last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
	                $current = &$current[$tag][$last_item_index];
	            }

	        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
	            //See if the key is already taken.
	            if(!isset($current[$tag])) { //New Key
	            	$current[$tag] = $result;
	            	$repeated_tag_index[$tag.'_'.$level] = 1;
	            	if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

	            } else { //If taken, put all things inside a list(array)
	                if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

	                    // ...push the new element into that array.
	                	$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

	                	if($priority == 'tag' and $get_attributes and $attributes_data) {
	                		$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
	                	}
	                	$repeated_tag_index[$tag.'_'.$level]++;

	                } else { //If it is not an array...
	                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
	                    $repeated_tag_index[$tag.'_'.$level] = 1;
	                    if($priority == 'tag' and $get_attributes) {
	                        if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well

	                        	$current[$tag]['0_attr'] = $current[$tag.'_attr'];
	                        	unset($current[$tag.'_attr']);
	                        }
	                        
	                        if($attributes_data) {
	                        	$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
	                        }
	                    }
	                    $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
	                }
	            }

	        } elseif($type == 'close') { //End of tag '</tag>'
		        $current = &$parent[$level-1];
		    }
		}

		return($xml_array);
	}  


	function sessionsimagesupdate() {
		// $appid = 2849;
		// $eventid = 5161;

		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// $error = '';
		// if(file_exists("/var/www/UPLOAD/Live/upload/sunrise.csv")) {
		// 	if (($handle = fopen("/var/www/UPLOAD/Live/upload/sunrise.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}

		// 		if(!$headers) {
		// 			$name = mysql_real_escape_string($newrow['name']);
		// 			if($name != '') {
		// 				$session = $this->db->query("SELECT id FROM session WHERE eventid = $eventid AND name = '$name' LIMIT 1");
		// 				if($session->num_rows() > 0) {
		// 					$session = $session->row();
		// 					$this->general_model->update('session', $session->id, array(
		// 						'imageurl' => 'http://sunrisefestival.be/content/img/'.$newrow['image location']
		// 					));
		// 				}
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// 	}
		// }
	}

	function removeEmptyMeta() {
		// $appid = 2579;
		// $eventid = 4956;
		// $metaId = 18121;
		// $metaHeaderId = 18116;

		// $metaId2 = 18118;
		// $metaHeaderId2 = 18119;

		// $metaId3 = 18120;
		// $metaHeaderId3 = 18115;

		// $sessions = $this->db->query("SELECT id FROM session WHERE eventid = $eventid")->result();
		// foreach($sessions as $s) {
		// 	$metavalues = $this->db->query("SELECT metaId FROM tc_metavalues WHERE metaId = $metaId AND parentType = 'session' AND parentId = $s->id LIMIT 1");
		// 	if($metavalues->num_rows() == 0) {
		// 		$this->db->query("DELETE FROM tc_metavalues WHERE metaId = $metaHeaderId AND parentType = 'session' AND parentId = $s->id");
		// 	}

		// 	$metavalues2 = $this->db->query("SELECT metaId FROM tc_metavalues WHERE metaId = $metaId2 AND parentType = 'session' AND parentId = $s->id LIMIT 1");
		// 	if($metavalues2->num_rows() == 0) {
		// 		$this->db->query("DELETE FROM tc_metavalues WHERE metaId = $metaHeaderId2 AND parentType = 'session' AND parentId = $s->id");
		// 	}

		// 	$metavalues3 = $this->db->query("SELECT metaId FROM tc_metavalues WHERE metaId = $metaId3 AND parentType = 'session' AND parentId = $s->id LIMIT 1");
		// 	if($metavalues3->num_rows() == 0) {
		// 		$this->db->query("DELETE FROM tc_metavalues WHERE metaId = $metaHeaderId3 AND parentType = 'session' AND parentId = $s->id");
		// 	}
		// }
	}

	function checknewssources() {
		$sources = $this->db->query("SELECT * FROM newssource")->result();

		// foreach($sources as $s) {
		// 	$ch = curl_init($s->url);                                                                
		// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		// 	$result = curl_exec($ch);
		// 	if($result == false || empty($result)) {
		// 		var_dump($s->url);
		// 	}
		// }
	}

    function attendeesSecretary() {
    	// $eventid = 5288;
    	// $appid = 2900;

    	// $attendees = $this->db->query("SELECT * FROM attendees WHERE eventid = $eventid")->result();
    	// foreach($attendees as $a) {
    	// 	$userid = $this->general_model->insert('user', array(
    	// 			'name' => $a->name,
    	// 			'login' => $a->email,
    	// 			'password' => md5('secre?34'.'wvcV23efGead!(va$43'),
    	// 			'email' => $a->email,
    	// 			'attendeeid' => $a->id
    	// 		));
    	// 	$appuserid = $this->general_model->insert('appuser', array(
    	// 			'appid' => $appid,
    	// 			'userid' => $userid,
    	// 			'parentType' => 'app',
    	// 			'parentId' => $appid
    	// 		));
    	// }
    }

    function exhibitorsEshre() {
  //   	$eventid = 4956;
  //   	$appid = 2579;
  //   	$url = 'http://clients.tapcrowd.com/eshre13/exhibitors.xml';

		// $curl = curl_init($url);
		// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		// $content = curl_exec($curl);
		// // var_dump($content);
		// if ($content === false) {
		//    	echo 'no content';
		// }

		// $exhibitors = $this->xml2array($content);
		foreach($exhibitors['exhibitors']['exhibitor'] as $exhibitor) {
			$name = $exhibitor['attr']['name'];
			$existsId = false;
			$exists = $this->db->query("SELECT id FROM exhibitor WHERE name = '$name' AND eventid = 4956 LIMIT 1");
			if($exists->num_rows() > 0) {
				$existsId = $exists->row()->id;
			}
			$description = $exhibitor['attr']['description'];
			$description = preg_replace('#<br\s*/?>#i', "\n", $description);
			$description = html_entity_decode($description);

			$booth = $exhibitor['booths']['booth']['attr']['name'];
			// var_dump($exhibitor);
			if($existsId) {
				$this->general_model->update('exhibitor', $existsId, array(
						'name' => $name,
						'description' => $description,
						'booth' => $booth
					));
			} else {
				$id = $this->general_model->insert('exhibitor', array(
						'eventid' => $eventid,
						'name' => $name,
						'description' => $description,
						'booth' => $booth
					));
			}

		}
		// echo '<pre>'; 
		// print_r($exhibitors);
		// echo '</pre>';
    }

    function copylauncherdata() {
    	// $launcherid = 66444;
    	// $launcher = $this->db->query("SELECT * FROM launcher WHERE id = $launcherid LIMIT 1")->row();

    	// $newappid = 3188;
    	// $newvenueid = 36251;
    	// $newparentgroupid = 43381;

    	// $groups = $this->db->query("SELECT * FROM `group` WHERE parentid = $launcher->groupid")->result();
    	// $groups2 = array();
    	// foreach($groups as $group) {
    	// 	$group->newgroupid = $this->general_model->insert('group', array(
    	// 			'appid' => $newappid,
    	// 			'venueid' => $newvenueid,
    	// 			'name' => $group->name,
    	// 			'parentid' => $newparentgroupid
    	// 		));
    	// 	$groups2[$group->id] = $group;
    	// }
    	// $groupids = array();
    	// foreach($groups as $g) {
    	// 	$groupids[] = $g->id;
    	// }
    	// $groupidsSql = implode(',', $groupids);
    	// $groupitems = $this->db->query("SELECT * FROM groupitem WHERE groupid IN ($groupidsSql)")->result();

    	// $objects = array();
    	// foreach($groupitems as $item) {
    	// 	$object = $this->db->query("SELECT c.*, p.price FROM catalog c INNER JOIN prices p ON p.iditem = c.id WHERE id = $item->itemid LIMIT 1")->row();
    	// 	$object->groupitem = $item;
    	// 	$objects[] = $object;
    	// }

    	// foreach($objects as $object) {
    	// 	$newobjectid = $this->general_model->insert($object->groupitem->itemtable, array(
    	// 			'venueid' => $newvenueid,
    	// 			'name' => $object->name,
    	// 			'description' => $object->description,
    	// 			'imageurl' => $object->imageurl,
    	// 			'order' => $object->order,
    	// 			'url' => $object->url
    	// 		));

    	// 	$this->general_model->insert('prices', array(
    	// 			'iditem' => $newobjectid,
    	// 			'price' => $object->price
    	// 		));

    	// 	$this->general_model->insert('groupitem', array(
    	// 			'appid' => $newappid,
    	// 			'venueid' => $newvenueid,
    	// 			'groupid' => $groups2[$object->groupitem->groupid]->newgroupid,
    	// 			'itemtable' => 'catalog',
    	// 			'itemid' => $newobjectid
    	// 		));
    	// }
    }

    function updateVenueInfo() {
    	$appid = 2066;

    	// $venues = $this->db->query("SELECT venue.id, venue.info FROM venue INNER JOIN appvenue ON appvenue.venueid = venue.id WHERE appvenue.appid = $appid AND address LIKE '%kleine steenweg%'")->result();

    	// $venueids = array();
    	// foreach($venues as $v) {
    	// 	$venueids[] = $v->id;
    	// }

    	// $venueidsSql = implode(',', $venueids);

    	// $this->db->query("UPDATE venue SET fax = '03 338 18 12', telephone = '03 338 18 11', address = 'Uw wijkkantoor: Doornstraat 2, 2610 Wilrijk ' WHERE id IN ($venueidsSql)");

   //  	$venues = $this->db->query("SELECT venue.id, venue.info FROM venue INNER JOIN appvenue ON appvenue.venueid = venue.id WHERE appvenue.appid = $appid AND address LIKE '%Doornstraat 2%'")->result();
   //  	foreach($venues as $v) {
			// // $info = str_replace('Kleine Steenweg 86   2610 Wilrijk  tel: 03 820 92 11', 'Doornstraat 2   2610 Wilrijk  tel: 03 338 18 11' , $v->info);
			// // $this->general_model->update('venue', $v->id, array(
			// // 		'info' => $info
			// // 	));

			// $translation = $this->db->query("SELECT * FROM translation WHERE tableid = $v->id AND `table` = 'venue' AND fieldname = 'info' LIMIT 1")->row();
			// $translationinfo = str_replace('Kleine Steenweg 86   2610 Wilrijk  tel: 03 820 92 11', 'Doornstraat 2   2610 Wilrijk  tel: 03 338 18 11' , $translation->translation);
			// $this->general_model->update('translation', $translation->id, array(
			// 		'translation' => $translationinfo
			// 	));

			// echo $v->id;
   //  	}
    }

    function doublenews() {
    	// $appid = 3065;
    	// $newsitems = $this->db->query("SELECT * FROM newsitem WHERE appid = 3065 AND sourceid <> 0 ORDER BY sourceid ASC, title ASC, id ASC")->result();
    	// $i = 0;
    	// foreach($newsitems as $n) {
    	// 	if($newsitems[$i+1]->title == $n->title) {
    	// 		$this->db->query("DELETE FROM newsitem WHERE id = $n->id");
    	// 		var_dump($n->id);
    	// 	}
    	// 	$i++;
    	// }
    }

    function striptagsExhibitors() {
    	// $exhibitors = $this->db->query("SELECT * FROM exhibitor WHERE eventid = 4956")->result();
    	// foreach($exhibitors as $e) {
    	// 	$description = strip_tags($e->description);
    	// 	$this->general_model->update('exhibitor', $e->id, array(
    	// 			'description' => $description
    	// 		));
    	// }
    }

    function abstractsessionstitle() {
    	// posters
    	// $sessions = $this->db->query("SELECT * FROM session WHERE sessiongroupid = 15630 AND parentid <> 0")->result();

    	// all other sessions
    	// $sessions = $this->db->query("SELECT * FROM session WHERE eventid = 4956")->result();
    	// foreach($sessions as $s) {
    	// 	$abstract = $this->db->query("SELECT * FROM tc_metavalues WHERE parentType = 'session' AND parentId = $s->id AND name = 'Abstract' LIMIT 1")->row();
    	// 	if(!empty($abstract)) {
	    // 		$value = mysql_real_escape_string($s->name . ' <br /> ' . $abstract->value);
	    // 		$this->db->query("UPDATE tc_metavalues SET value = '$value' WHERE metaId = $abstract->metaId AND parentType = 'session' AND parentId = $s->id AND name = 'Abstract'");	
    	// 	}

    	// }
    }

    function knokke() {
    	// $venues = $this->db->query("SELECT venue.* FROM venue INNER JOIN appvenue ON appvenue.venueid = venue.id WHERE appvenue.appid = 2048")->result();

    	// $geschenkkaarten = array();
    	// $visits = array();
    	// foreach($venues as $v) {
    	// 	$geschenkkaart = $this->db->query("SELECT * FROM tc_metavalues WHERE parentType = 'venue' AND parentId = $v->id AND name = 'Geschenkkaart' LIMIT 1");
    	// 	if($geschenkkaart->num_rows() > 0) {
    	// 		$geschenkkaarten[] = 'http://upload.tapcrowd.com/'.$v->image1;
    	// 	}

    	// 	$vist = $this->db->query("SELECT * FROM tc_metavalues WHERE parentType = 'venue' AND parentId = $v->id AND name = 'Visit Knokke-Heist' LIMIT 1");
    	// 	if($vist->num_rows() > 0) {
    	// 		$visits[] = 'http://upload.tapcrowd.com/'.$v->image1;
    	// 	}
    	// }
    	
    	// print_r($geschenkkaarten);
    	// print_r($visits);
    }

	function poll() {
		// // alfazet order states 
		// $stateMessages = array(
		// 		0 => 'Order in polling queue',
		// 		1 => 'Order polled',
		// 		2 => 'Accepted by kassa',
		// 		3 => 'Denied by kassa',
		// 		4 => 'Order on hold in kassa'
		// 	);

		// // database connection
		// $con = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin", "tapcrowd");
		// if (!$con)
		//   {
		//   die('Could not connect: ' . mysqli_error());
		//   }
		// mysqli_set_charset($con, "utf8");

		// // get venues to call
		// $venues = array();
		// $venuesQuery =  mysqli_query($con, 
		// 	"SELECT b.externalvenueid, b.venueid, a.id as appid, a.bundle FROM basket b 
		// 	INNER JOIN appvenue ON appvenue.venueid = b.venueid 
		// 	INNER JOIN app a ON appvenue.appid = a.id 
		// 	WHERE b.send_order_to_api = 'alfazet' AND a.id <> 2");
		// while($venue = mysqli_fetch_assoc($venuesQuery)) {
		// 	if(empty($venue['bundle'])) {
		// 		$venue['bundle'] = 'com.tapcrowd.demoapp';
		// 	}
			
		// 	$venues[$venue['externalvenueid']] = (object) $venue;
		// }
		

		// // fetch orders that are already in our database
		// $alfazet_ordersQuery =  mysqli_query($con, "SELECT * FROM alfazet_push");
		// while($alfazet_order = mysqli_fetch_assoc($alfazet_ordersQuery)) {
		// 	$alfazet_orders[$alfazet_order['orderid']][$alfazet_order['deviceid']][$alfazet_order['status']] = (object) $alfazet_order;
		// }

		// // switch to profiling database
		// mysqli_select_db($con, "profiling");

		// $insert = array();
		// foreach($venues as $externalvenueid => $venue) {
		// 	// fetch orders from alfazet
		// 	$url = 'http://79.174.135.14/tapcrowd-1.0/status/'.$externalvenueid.'/60';
		// 	$curl = curl_init($url);
		// 	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		// 	$content = curl_exec($curl);
		// 	curl_close($curl);

		// 	if ($content !== false) {
		// 		$content = json_decode($content);
		// 		// $content->orders[] = (object) array('orderid' => 42, 'venueid' => 121212, 'state' => 1, 'deviceid' => '1234567dfg8');
		// 		if(isset($content->orders) && !empty($content->orders)) {
		// 			foreach($content->orders as $order) {
		// 				//check if a push notification was already sent for the current status of the current order, if not send push.
		// 				if(!isset($alfazet_orders[$order->orderid][$order->deviceid][$order->state]) && !empty($order->deviceid)) {
		// 					// query profiling db to check device for current bundle, if no found, send using demoapp
		// 					$deviceQuery =  mysqli_query($con, "SELECT * FROM push WHERE bundle ='$venue->bundle' AND deviceid='$order->deviceid'");
		// 					$device_cnt = mysqli_num_rows($deviceQuery);
		// 					if($device_cnt == 0) {
		// 						$venue->bundle = 'com.tapcrowd.demoapp';
		// 					}

		// 					// send push
		// 					$ch = curl_init('http://analytics.tapcrowd.com/0.1/pushservice/sendPushForBundle');
		// 					curl_setopt($ch, CURLOPT_POST, true);                                                                     
		// 					curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$venue->bundle.'&deviceid='.$order->deviceid.'&msg='.urlencode($stateMessages[$order->state]));  
		// 					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// 					$result = curl_exec($ch);
		// 					$result = stristr($result, 'ok');
		// 					if($result) {
		// 						// log push notfication
		// 						$insert[] = "($order->orderid, $order->state, '$order->deviceid')";
		// 					}
		// 					curl_close($ch);
		// 				}
		// 			} 
		// 		}
		// 	}
		// }

		// // switch to tapcrowd database
		// mysqli_select_db($con, "tapcrowd");
		// $insert = implode(',',$insert);
		// mysqli_query($con, "INSERT INTO alfazet_push (orderid, status, deviceid) VALUES $insert");

	}


	function logtest() {
		// $data_string = ' [
		// 	  {
		// 	    "lid" : "e1c682287635724690584662f47e71a4",
		// 	    "path" : "\/App\/3289\/event\/launcher\/",
		// 	    "lon" : "0.000000",
		// 	    "action" : "log",
		// 	    "time" : "1374483465",
		// 	    "lat" : "0.000000",
		// 	    "key" : "5704",
		// 	    "val" : "1",
		// 	    "sessionid" : "e8e6aabf90acbd81bf7b70473cb44123"
		// 	  },
		// 	  {
		// 	    "lid" : "defa0ee7bae8adbecc01b0e69faa0155",
		// 	    "path" : "\/App\/3289\/news",
		// 	    "lon" : "3.727800",
		// 	    "action" : "log",
		// 	    "time" : "1374483467",
		// 	    "lat" : "51.057999",
		// 	    "key" : "",
		// 	    "val" : "1",
		// 	    "sessionid" : "e8e6aabf90acbd81bf7b70473cb44123"
		// 	  },
		// 	  {
		// 	    "lid" : "a5892672337022a3a49a0cfec9a2ed79",
		// 	    "path" : "\/App\/3289\/event\/launcher\/",
		// 	    "lon" : "3.727800",
		// 	    "action" : "log",
		// 	    "time" : "1374483469",
		// 	    "lat" : "51.057999",
		// 	    "key" : "5704",
		// 	    "val" : "1",
		// 	    "sessionid" : "e8e6aabf90acbd81bf7b70473cb44123"
		// 	  },
		// 	  {
		// 	    "lid" : "33e1a50a111770de9db303e8f5a25b6d",
		// 	    "path" : "\/App\/3289\/event\/launcher\/",
		// 	    "lon" : "3.727800",
		// 	    "action" : "log",
		// 	    "time" : "1374483471",
		// 	    "lat" : "51.057999",
		// 	    "key" : "5704",
		// 	    "val" : "1",
		// 	    "sessionid" : "e8e6aabf90acbd81bf7b70473cb44123"
		// 	  },
		// 	  {
		// 	    "lid" : "66ca31585d2a55b684d6e4a077355c89",
		// 	    "path" : "\/App\/3289\/event\/launcher\/",
		// 	    "lon" : "3.727800",
		// 	    "action" : "log",
		// 	    "time" : "1374483473",
		// 	    "lat" : "51.057999",
		// 	    "key" : "5704",
		// 	    "val" : "1",
		// 	    "sessionid" : "e8e6aabf90acbd81bf7b70473cb44123"
		// 	  },
		// 	  {
		// 	    "lid" : "643abd6c1adce94c5ce5a92447ff7065",
		// 	    "path" : "\/App\/3289\/event\/launcher\/",
		// 	    "lon" : "3.727800",
		// 	    "action" : "log",
		// 	    "time" : "1374483474",
		// 	    "lat" : "51.057999",
		// 	    "key" : "5704",
		// 	    "val" : "1",
		// 	    "sessionid" : "e8e6aabf90acbd81bf7b70473cb44123"
		// 	  }
		// 	]';
		// $url = 'http://analytics.tapcrowd.lh/0.2/analyticsservice/bulk';
		// $curl = curl_init($url);
		// curl_setopt($curl, CURLOPT_POST, true);                                                                     
		// curl_setopt($curl, CURLOPT_POSTFIELDS, "json=".$data_string); 
		// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		// $content = curl_exec($curl);
		// curl_close($curl);

		// var_dump($content);
	}


	function removeCatalogsEventoFacil() {
		// $venueid = 22316;
		// $appid = 2878;

		// $metadata = $this->db->query("SELECT id FROM tc_metadata WHERE parentType = 'launcher' AND parentId = 58938")->result();
		// foreach($metadata as $m) {
		// 	$this->db->query("DELETE FROM tc_metavalues WHERE metaId = $m->id");
		// }

		// $this->db->query("DELETE FROM catalog WHERE venueid = $venueid AND `type` <> 'projects'");
	}

	function ftptest() {
	// $mysqli = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin","tapcrowd");
	// if ($mysqli->connect_error) {
	//     die('Connect Error (' . $mysqli->connect_errno . ') '
	//             . $mysqli->connect_error);
	// }

	// $url = "ftp://vps.ijv.be/";
	// $curl = curl_init();
	// curl_setopt($curl, CURLOPT_URL, $url);
	// curl_setopt($curl, CURLOPT_USERPWD, "tapcrowd:eF4SdRd2");
	// curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1) ;
	// curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'NLST');

	// $ftp=curl_exec($curl);
	// $directory=split("[\n|\r]",$ftp);

	// foreach($directory as $key=>$value) {
	//     if($value=='' || !stripos($value, 'csv')) {
	//     	unset($directory[$key]);
	//     } 
	// }

	// foreach($directory as $k => $v) {
	// 	// $path = '/Users/JensLovesMobileJuice/Downloads/'.$v;
	// 	$ch = curl_init($url.$v);
	// 	// curl_setopt($ch, CURLOPT_FILE, $fp);
	// 	curl_setopt($ch, CURLOPT_USERPWD, "tapcrowd:eF4SdRd2");
	// 	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1) ;
	// 	$filedata = curl_exec($ch);
	// 	// file_put_contents($path, $data, FILE_APPEND);

	// 	$externalids = array();
		
	// 	$appid = 0;
	// 	$eventid = 0;

	// 	if(stristr($v, 'estetika')) {
	// 		$eventid = 5488;
	// 		$appid = 3093;
	// 	} elseif(stristr($v, 'jaarbeurs')) {
	// 		$eventid = 5339;
	// 		$appid = 2945;
	// 	}

	// 	if(!empty($appid) && !empty($eventid)) {
	// 		$mainparentid = $mysqli->query("SELECT id FROM `group` WHERE eventid = $eventid AND name = 'exhibitorcategories' LIMIT 1");
	// 		$mainparentid = $mainparentid->fetch_assoc();
	// 		$mainparentid = $mainparentid['id'];
	// 		$headers = true;
	// 		$heading = array();
	// 		$categories = array();

	// 		if(($data2 = str_getcsv($filedata, "\n")) !== false) {
	// 			foreach($data2 as $data) {
	// 				$data = explode(';', $data);
	// 				$num = count($data);
	// 				$newrow = array();
	// 				for ($c=0; $c < $num; $c++) {
	// 					if($headers) {
	// 						if(!empty($data[$c]) && stripos($data[$c], 'category') !== false) {
	// 							$categories[$data[$c]] = $data[$c];
	// 							$heading[] = $data[$c];
	// 						} elseif(!empty($data[$c])) {
	// 							$heading[] = $data[$c];
	// 						}
	// 					} elseif(isset($data[$c]) && !empty($data[$c]) && isset($heading[$c])) {
	// 						$newrow[$heading[$c]] = $mysqli->real_escape_string($data[$c]);
	// 					}
	// 				}

	// 				if(!$headers && $newrow['name'] != '') {
	// 					$externalids[$newrow['external_id']] = $newrow['external_id'];
	// 					//insert exhibitor
	// 					foreach($heading as $k) {
	// 						if(!isset($newrow[$k])) {
	// 							$newrow[$k] = '';
	// 						}
	// 					}

	// 					//insert exhibitor
	// 					$exhibitorExists = $mysqli->query("SELECT id FROM exhibitor WHERE eventid = $eventid AND external_id = '".$newrow['external_id']."' LIMIT 1");
	// 					if($exhibitorExists->num_rows == 0) {
	// 						$stmt = $mysqli->prepare("INSERT INTO exhibitor (external_id, eventid, name, booth, description, address, tel, email, web, imageurl) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	// 						$stmt->bind_param("sissssssss", $newrow['external_id'], $eventid, $newrow['name'], $newrow['booth'], $newrow['description'], $newrow['address'], $newrow['tel'], $newrow['email'], $newrow['web'], $newrow['image1']);
	// 						$stmt->execute();
	// 						$exhibitorid = $stmt->insert_id;
	// 						$stmt->close();
	// 					} else {
	// 						$exhibitorid = $exhibitorExists->fetch_assoc();
	// 						$exhibitorid = $exhibitorid['id'];
	// 						$stmt = $mysqli->prepare("UPDATE exhibitor SET external_id = ?, eventid = ?, name = ?, booth = ?, description = ?, address = ?, tel = ?, email = ?, web = ?, imageurl = ? WHERE id = ?");
	// 						$stmt->bind_param("sissssssssi",$newrow['external_id'], $eventid, $newrow['name'], $newrow['booth'], $newrow['description'], $newrow['address'], $newrow['tel'], $newrow['email'], $newrow['web'], $newrow['image1'], $exhibitorid);
	// 						$stmt->execute();
	// 						$stmt->close();
	// 					}

	// 					if(!empty($exhibitorid)) {
	// 						$stmt = $mysqli->prepare("DELETE FROM groupitem WHERE itemid = ? AND itemtable = 'exhibitor' AND eventid = ?");
	// 						$stmt->bind_param("ii",$exhibitorid, $eventid);
	// 						$stmt->execute();
	// 						$stmt->close();

	// 						foreach($categories as $catname) {
	// 							if(isset($newrow[$catname]) && !empty($newrow[$catname])) {
	// 								// if(!stripos($catname, '_fr')) {
	// 									$catExists = $mysqli->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = '".$mysqli->real_escape_string($newrow[$catname])."' LIMIT 1");
	// 									if($catExists->num_rows == 0) {
	// 										$stmt = $mysqli->prepare("INSERT INTO `group` (appid, eventid, name, parentid) VALUES (?, ?, ?, ?)");
	// 										$stmt->bind_param("iisi",$appid, $eventid, $newrow[$catname], $mainparentid);
	// 										$stmt->execute();
	// 										$catid = $stmt->insert_id;
	// 										$stmt->close();
	// 									} else {							
	// 										$catid = $catExists->fetch_assoc();
	// 										$catid = $catid['id'];
	// 									}

	// 									$stmt = $mysqli->prepare("INSERT INTO groupitem (itemid, itemtable, appid, eventid, groupid) VALUES (?, 'exhibitor', ?, ?, ?)");
	// 									$stmt->bind_param("iiii",$exhibitorid, $appid, $eventid, $catid);
	// 									$stmt->execute();
	// 									$stmt->close();
	// 								// } else {
	// 								// 	$dutchname = $newrow[str_replace('_fr', '_nl', $catname)];
	// 								// 	if(!empty($dutchname)) {
	// 								// 		$catExists = $mysqli->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = '".mysql_escape_string($dutchname)."' LIMIT 1");
	// 								// 		$catExists = $catExists->fetch_assoc();
	// 								// 		$catExists = $catExists['id'];
	// 								// 		$translationExists = $mysqli->query("SELECT id FROM translation WHERE `table` = 'group' AND 'tableid' = $catExists AND `language` = 'fr' LIMIT 1");
	// 								// 		if($translationExists->num_rows == 0) {
	// 								// 			$stmt = $mysqli->prepare("INSERT INTO `translation` (`table`, tableid, language, translation) VALUES ('group', ?, ?, ?)");
	// 								// 			$stmt->bind_param("siss", $catExists, 'fr', $newrow[$catname]);
	// 								// 			$stmt->execute();
	// 								// 			$stmt->close();
	// 								// 		}
	// 								// 	}
	// 								// }
	// 							}
	// 						}
	// 					}
	// 				}

	// 				$headers = FALSE;
	// 			}
	// 		}

	// 		if(!empty($externalids)) {
	// 			$externalidsSql = implode(',', $externalids);
	// 			$$externalidsSql = $mysqli->real_escape_string(utf8_encode($externalidsSql));
	// 			$stmt = $mysqli->prepare("DELETE FROM exhibitor WHERE eventid = ? AND external_id NOT IN (".$externalidsSql.") AND external_id <> ''");
	// 			$stmt->bind_param("i",$eventid);
	// 			$stmt->execute();
	// 			$stmt->close();
	// 		}
	// 	}

	// 	curl_close($ch);
	// }

	// echo "imported\n\n";
	// // echo("<pre>".print_r($directory,true)."</pre>");
	// curl_close ($curl);
	// $mysqli->close();
	}

	function synch() {
		// $salt = 'wvcV23efGead!(va$43';
		// $appid = 3342;
		// $eventid = 5772;
		// $mysqli = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin","tapcrowd");
		// if ($mysqli->connect_error) {
		//     die('Connect Error (' . $mysqli->connect_errno . ') '
		//             . $mysqli->connect_error);
		// }

		// $url = 'http://api.eventdrive.be/AttendeeContactInfos/?&%24inlinecount=allpages&$format=json&Authorization=9529ddfc-761b-4f99-a6ac-2fe5202eca90&%24top=15';
		// $curl = curl_init($url);
		// curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		// 	'Accept: application/json'
	 //    ));
		// $data=curl_exec($curl);
		// curl_close($curl);

		// if($data) {
		// 	$data = $this->jsonRemoveUnicodeSequences($data);
		// 	$data = json_decode($data);
		// }

		// $attendeelauncheridExists = $mysqli->query("SELECT id FROM launcher WHERE eventid = $eventid AND moduletypeid = 14 LIMIT 1");
		// if($attendeelauncheridExists->num_rows > 0) {
		// 	$attendeelauncherid = $attendeelauncheridExists->fetch_assoc();
		// 	$attendeelauncherid = $attendeelauncherid['id'];
		// } else {
		// 	$stmt = $mysqli->prepare("INSERT INTO launcher (eventid, moduletypeid, module, title, icon, active) VALUES (?, ?, ?, ?, ?, ?)");
		// 	$stmt->bind_param("iisssi", $eventid, 14, 'Attendee list', 'Attendee list', 'l_attendees', 1);
		// 	$stmt->execute();
		// 	$attendeelauncherid = $stmt->insert_id;
		// 	$stmt->close();
		// }
		
		// if($data && isset($data->d->results)) {
		// 	foreach($data->d->results as $attendee) {
		// 		$newattendee->external_id = ($attendee->cust_id != null) ? $attendee->cust_id : '';
		// 		$newattendee->firstname = ($attendee->cust_fname != null) ? $attendee->cust_fname : '';
		// 		$newattendee->name = ($attendee->cust_lname != null) ? $attendee->cust_lname : '';
		// 		$newattendee->email = ($attendee->cust_email != null) ? $attendee->cust_email : '';
		// 		$newattendee->country = ($attendee->cust_country != null) ? $attendee->cust_country : '';
		// 		$newattendee->company = ($attendee->cust_company != null) ? $attendee->cust_company : '';
		// 		$newattendee->function = ($attendee->cust_functie != null) ? $attendee->cust_functie : '';
		// 		$newattendee->phonenr = ($attendee->cust_phone != null) ? $attendee->cust_phone : '';

		// 		$attendeeExists = $mysqli->query("SELECT id FROM attendees WHERE eventid = $eventid AND external_id = '".$newattendee->external_id."' LIMIT 1");
		// 		if($attendeeExists->num_rows > 0) {
		// 			$newattendee->id = $attendeeExists->fetch_assoc();
		// 			$newattendee->id = $newattendee->id['id'];

		// 			$stmt = $mysqli->prepare("UPDATE attendees SET external_id = ?, eventid = ?, firstname = ?, name = ?, email = ?, country = ?, company = ?, function = ?, phonenr = ? WHERE id = ?");
		// 			$stmt->bind_param("sisssssssi", $newattendee->external_id, $eventid, $newattendee->firstname, $newattendee->name, $newattendee->email, $newattendee->country, $newattendee->company, $newattendee->function, $newattendee->phonenr, $newattendee->id);
		// 			$stmt->execute();
		// 			$stmt->close();
		// 		} else {
		// 			$stmt = $mysqli->prepare("INSERT INTO attendees (external_id, eventid, firstname, name, email, country, company, function, phonenr) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		// 			$stmt->bind_param("sisssssss", $newattendee->external_id, $eventid, $newattendee->firstname, $newattendee->name, $newattendee->email, $newattendee->country, $newattendee->company, $newattendee->function, $newattendee->phonenr);
		// 			$stmt->execute();
		// 			$newattendee->id = $stmt->insert_id;
		// 			$stmt->close();
		// 		}

		// 		$user->external_id = $newattendee->external_id;
		// 		$user->login = $newattendee->email;
		// 		$user->email = $newattendee->email;
		// 		$user->name = $newattendee->firstname . ' ' . $newattendee->name;
		// 		$user->password = md5($attendee->cust_code.$salt);
		// 		$user->attendeeid = $newattendee->id;

		// 		$userExists = $mysqli->query("SELECT u.id FROM user u INNER JOIN appuser au ON au.userid = u.id WHERE au.appid = $appid AND u.external_id = '".$user->external_id."' LIMIT 1");
		// 		if($userExists->num_rows > 0) {
		// 			$user->id = $userExists->fetch_assoc();
		// 			$user->id = $user->id['id'];

		// 			$stmt = $mysqli->prepare("UPDATE user SET external_id = ?, login = ?, email = ?, name = ?, password = ?, attendeeid = ? WHERE id = ?");
		// 			$stmt->bind_param("sssssii", $user->external_id, $user->login, $user->email, $user->name, $user->password, $user->attendeeid, $user->id);
		// 			$stmt->execute();
		// 			$stmt->close();
		// 		} else {
		// 			$stmt = $mysqli->prepare("INSERT INTO user (external_id, login, email, name, password, attendeeid) VALUES (?, ?, ?, ?, ?, ?)");
		// 			$stmt->bind_param("sssssi", $user->external_id, $user->login, $user->email, $user->name, $user->password, $user->attendeeid);
		// 			$stmt->execute();
		// 			$user->id = $stmt->insert_id;
		// 			$stmt->close();
		// 		}

		// 		$appuserExists = $mysqli->query("SELECT au.id FROM appuser au WHERE au.appid = $appid AND au.userid = '$u->id' AND au.parentType = 'launcher' AND au.parentId = $attendeelauncherid LIMIT 1");
		// 		if($appuserExists->num_rows == 0) {
		// 			$parentType = 'launcher';
		// 			$stmt = $mysqli->prepare("INSERT INTO appuser (appid, userid, parentType, parentId) VALUES (?, ?, ?, ?)");
		// 			$stmt->bind_param("iisi", $appid, $user->id, $parentType, $attendeelauncherid);
		// 			$stmt->execute();
		// 			$appuseruser->id = $stmt->insert_id;
		// 			$stmt->close();
		// 		}
		// 	}
		// }

		// $mysqli->close();
		// echo "imported\n\n";
	}

	function jsonRemoveUnicodeSequences($struct) {
	   return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $struct);
	}

	function notimmo() {
		// $appid = 1994;
		// $mysqli = mysqli_connect("localhost","tcAdminDB","2012?tapcrowdadmin","notimmo");
		// if ($mysqli->connect_error) {
		//     die('Connect Error (' . $mysqli->connect_errno . ') '
		//             . $mysqli->connect_error);
		// }
		// // $mysqli->set_charset("utf8");

		// $currentids = array();
		// $externalids = array();
		// $lats = array();
		// $lons = array();
		// $addresses = array();
		// $venues = array();
		// $venuesQuery = $mysqli->query("SELECT id, externalid, latitude, longitude, address FROM venues_b2c");
		// if($venuesQuery->num_rows > 0) {
		// 	while ($data = $venuesQuery->fetch_assoc())
		// 	{
		// 	   $venues[] = $data;
		// 	}
		// 	foreach($venues as $v) {
		// 		if(isset($v['externalid'])) {
		// 			$externalids[$v['externalid']] = $v['id'];
		// 			$lats[$v['externalid']] = $v['latitude'];
		// 			$lons[$v['externalid']] = $v['longitude'];
		// 			$addresses[$v['externalid']] = $v['address'];
		// 		}
		// 	}
		// }


		// $url = "ftp://FTPCLASS:iC1AssIM!@ftpservices.corelio.be/EXPORT/CLASSIIMMO/IpadfeedImmoNot.xml";
		// $curl = curl_init($url);
		// curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		// $data=curl_exec($curl);

		// // $filename = "../IpadfeedImmoNot.xml";
		// // if (($handle = fopen($filename, "r")) !== FALSE) {
		// // 	$data = fread($handle, filesize($filename));
		// // }

		// $categories = array('74988' => '01', '74990' => '06', '74991' => '06', '74992' => '05', '74993' => '03', '74995' => '03', '74997' => '07');


		// $rss = new SimpleXmlElement($data);
		// foreach($rss->Zook as $item) {
		// 	$attributes = $item->attributes();
		// 	$venue->externalid = (string) $attributes['AdvertentieID'];
		// 	$currentids[$venue->externalid] = $venue->externalid;
		// 	$venue->name = utf8_decode((string)$item->Data->Title);
		// 	$venue->description = utf8_decode((string)$item->Data->ContentOnline);
		// 	$venue->address = '';
		// 	$venue->street = '';
		// 	$venue->number = '';
		// 	$venue->zipcode = '';
		// 	$venue->city = '';
		// 	if(isset($item->Data->ObjectLocationStreet)) {
		// 		$venue->street = utf8_decode((string)$item->Data->ObjectLocationStreet);
		// 		$venue->address .= $venue->street . ' ';
		// 	} 
		// 	if(isset($item->Data->ObjectLocationNumber)) {
		// 		$venue->number .= utf8_decode((string)$item->Data->ObjectLocationNumber);
		// 		$venue->address .= $venue->number . ', ';
		// 	}
		// 	if(isset($item->Data->ObjectLocationZipCode)) {
		// 		$venue->zipcode .= utf8_decode((string)$item->Data->ObjectLocationZipCode);
		// 		$venue->address .= $venue->zipcode . ' ';
		// 	}
		// 	if(isset($item->Data->ObjectLocationCity)) {
		// 		$venue->city .= utf8_decode((string)$item->Data->ObjectLocationCity);
		// 		$venue->address .= $venue->city;
		// 	}

		// 	$venue->country = utf8_decode((string)$item->Data->ObjectLocationCountry);

		// 	// $iImages = 0;
		// 	// while($iImages < 5) {
		// 	// 	if(isset($item->Images->Image[$iImages]) && !empty($item->Images->Image[$iImages])) {
		// 	// 		$venue->{'image'.($iImages+1)} = utf8_decode((string)$item->Images->Image[$iImages]);
		// 	// 	}
		// 	// 	$iImages++;
		// 	// }
		// 	$venue->urltophoto = utf8_decode((string)$item->Image);
		// 	$venue->price = (int)utf8_decode((string)$item->Data->DemandPrice);
		// 	$venue->urltopage = utf8_decode((string)$item->Data->ObjectContentExternURL);
		// 	$venue->categoryalias_original = (string) $attributes['category'];
		// 	$venue->categoryalias = '';
		// 	if(isset($categories[$venue->categoryalias_original])) {
		// 		$venue->categoryalias = (string)$categories[$venue->categoryalias_original];
		// 	}
		// 	$venue->dateofselling = '2013-01-01';
			

		// 	// $venue->active = 1;
		// 	// $venue->timestamp = time();

		// 	if(isset($externalids[$venue->externalid])) {
		// 		if(isset($lats[$venue->externalid])) {
		// 			$venue->lat = $lats[$venue->externalid];
		// 		}
		// 		if(isset($lons[$venue->externalid])) {
		// 			$venue->lon = $lons[$venue->externalid];
		// 		}
		// 		if(!(isset($addresses[$venue->externalid]) && $addresses[$venue->externalid] == $venue->address) && !empty($venue->address)) {
		// 			$string = str_replace (" ", "+", utf8_encode($venue->address));
		// 			$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";

		// 			$ch = curl_init();
		// 			curl_setopt($ch, CURLOPT_URL, $details_url);
		// 			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// 			$response = json_decode(curl_exec($ch), true);
		// 			if ($response['status'] == 'OK') {
		// 				$geometry = $response['results'][0]['geometry'];

		// 			    $venue->lat = $geometry['location']['lat'];
		// 			    $venue->lon = $geometry['location']['lng'];
		// 			}

		// 			curl_close($ch);
		// 			sleep(2);
		// 		}
		// 		$venue->id = $externalids[$venue->externalid];
		// 		$stmt = $mysqli->prepare("UPDATE venues_b2c SET externalid = ?, name = ?, latitude = ?, longitude = ?, country = ?, city = ?, street = ?, `number` = ?, zipcode = ?, price = ?, description = ?, categoryalias = ?, categoryalias_original = ?, urltopage = ?, urltophoto = ?, address = ?, dateofselling = ? WHERE id = ?");
		// 		$stmt->bind_param("ssddsssssdsssssssi",$venue->externalid, $venue->name, $venue->lat, $venue->lon, $venue->country, $venue->city, $venue->street, $venue->number, $venue->zipcode, $venue->price, $venue->description, $venue->categoryalias, $venue->categoryalias_original, $venue->urltopage, $venue->urltophoto, $venue->address, $venue->dateofselling, $externalids[$venue->externalid]);
		// 		$stmt->execute();
		// 		$stmt->close();
		// 	} else {
		// 		$venue->lat = 0;
		// 		$venue->lon = 0;
		// 		if(!empty($venue->address)) {
		// 			$string = str_replace (" ", "+", utf8_encode($venue->address));
		// 			$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";

		// 			$ch = curl_init();
		// 			curl_setopt($ch, CURLOPT_URL, $details_url);
		// 			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// 			$response = json_decode(curl_exec($ch), true);
		// 			if ($response['status'] == 'OK') {
		// 				$geometry = $response['results'][0]['geometry'];

		// 			    $venue->lat = $geometry['location']['lat'];
		// 			    $venue->lon = $geometry['location']['lng'];
		// 			}

		// 			curl_close($ch);
		// 			sleep(2);
		// 		}

		// 		$stmt = $mysqli->prepare("INSERT INTO venues_b2c (externalid, name, latitude, longitude, country, city, street, `number`, zipcode, price, description, categoryalias, categoryalias_original, urltopage, urltophoto, address, dateofselling) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		// 		$stmt->bind_param("ssddsssssdsssssss", $venue->externalid, $venue->name, $venue->lat, $venue->lon, $venue->country, $venue->city, $venue->street, $venue->number, $venue->zipcode, $venue->price, $venue->description, $venue->categoryalias, $venue->categoryalias_original, $venue->urltopage, $venue->urltophoto, $venue->address, $venue->dateofselling);
		// 		$stmt->execute();
		// 		$venue->id = $stmt->insert_id;
		// 		$stmt->close();
		// 		$externalids[$v['externalid']] = $venue->id;
		// 	}

		// 	// translations
		// 	$stmt = $mysqli->prepare("DELETE FROM b2c_translations WHERE tableid = $venue->id");
		// 	$stmt->execute();
		// 	$stmt->close();

		// 	if(isset($item->Data->TitleFR)) {
		// 		$stmt = $mysqli->prepare("INSERT INTO b2c_translations (tableid, fieldname, language, translation) VALUES (?, 'name', 'fr', ?)");
		// 		$stmt->bind_param("ss", $venue->id, utf8_decode((string)$item->Data->TitleFR));
		// 		$stmt->execute();
		// 		$stmt->close();
		// 	}
		// 	if(isset($item->Data->TitleNL)) {
		// 		$stmt = $mysqli->prepare("INSERT INTO b2c_translations (tableid, fieldname, language, translation) VALUES (?, 'name', 'nl', ?)");
		// 		$stmt->bind_param("ss", $venue->id, utf8_decode((string)$item->Data->TitleNL));
		// 		$stmt->execute();
		// 		$stmt->close();
		// 	}
		// 	if(isset($item->Data->ContentOnlineFR)) {
		// 		$stmt = $mysqli->prepare("INSERT INTO b2c_translations (tableid, fieldname, language, translation) VALUES (?, 'description', 'fr', ?)");
		// 		$stmt->bind_param("ss", $venue->id, utf8_decode((string)$item->Data->ContentOnlineFR));
		// 		$stmt->execute();
		// 		$stmt->close();
		// 	}
		// 	if(isset($item->Data->ContentOnlineNL)) {
		// 		$stmt = $mysqli->prepare("INSERT INTO b2c_translations (tableid, fieldname, language, translation) VALUES (?, 'description', 'nl', ?)");
		// 		$stmt->bind_param("ss", $venue->id, utf8_decode((string)$item->Data->ContentOnlineNL));
		// 		$stmt->execute();
		// 		$stmt->close();
		// 	}
		// }

		// foreach($externalids as $k => $v) {
		// 	if(!isset($currentids[$k])) {
		// 		$stmt = $mysqli->prepare("DELETE FROM venues_b2c WHERE id = ?");
		// 		$stmt->bind_param("i",$v);
		// 		$stmt->execute();
		// 		$stmt->close();
		// 	}
		// }

		// echo "imported\n\n";
		// curl_close ($curl);
		// $mysqli->close();
	}

	function bundles() {
		// $some_special_chars = array("'", '"', '&', '%', '$', '.', ',', '(', ')', '!', ' ',';', ':');
		// $replacement_chars  = array("", "", '', '', '', '', '', '', '', '', '', '', '');

		// $apps = $this->db->query("SELECT * FROM app WHERE bundle = ''")->result();
		// $bundles = $this->app_model->getBundles();
		// foreach($apps as $app) {
		// 	$bundle = $app->name.$app->id;

		// 	$bundle =  preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $bundle);
		// 	$bundle    = 'com.tapcrowd.'.str_replace($some_special_chars, $replacement_chars, $bundle);
		// 	if(isset($bundles[$bundle])) {
		// 		$bundle = $bundle.substr(md5($bundle.time()), 0, 5);
		// 	}
		// 	$this->general_model->update('app', $app->id, array('bundle' => $bundle));
		// }
	}

	function certificatesinpushtable() {
		// $apps = $this->db->query("SELECT * FROM app WHERE certificate != ''")->result();
		// foreach($apps as $app) {
		// 	if(!empty($app->certificate) && substr($app->certificate, strlen($app->certificate) - 4) != '.pem') {
		// 		$app->certificate .= '.pem';
		// 	}
		// 	$this->db->query("UPDATE push SET certificate = '$app->certificate' WHERE appid = $app->id");
		// }

		// $this->db->query("UPDATE push SET certificate = 'demoapp-prod.pem' WHERE certificate = ''");
	}

	function pushcertificates() {
		// $apps = $this->db->query("SELECT * FROM app WHERE certificate != ''")->result();
		// foreach($apps as $app) {
		// 	if(!empty($app->certificate) && substr($app->certificate, strlen($app->certificate) - 4) != '.pem') {
		// 		$app->certificate .= '.pem';
		// 	}
			
		// 	if(file_exists('/var/www/CLIENTS/push/ios/'.$app->certificate)) {
		// 		if(($f = fopen('/var/www/CLIENTS/push/ios/'.$app->certificate, "r")) !== FALSE) {
		// 			$contents = fread($f, filesize('/var/www/CLIENTS/push/ios/'.$app->certificate));
		// 			fclose($f); 

		// 			$exists = $this->db->query("SELECT * FROM push_certificates WHERE appid = $app->id AND certificate_name = '$app->certificate'");
		// 			if($exists->num_rows() == 0) {
		// 				$this->db->query("INSERT INTO push_certificates (appid, type, certificate_name, content) VALUES ($app->id, 'ios', '$app->certificate', '$contents')");
		// 			}
		// 		}
		// 	}
		// }
	}

	function registerapp() {
		// $service = 'http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment';
  //       $bundle = 'com.tapcrowd.pushtest43617';
  //       $demo_certificate_filename = '/var/www/CLIENTS/push/ios/demoapp-prod.pem';
  //       if(version_compare(PHP_VERSION, '5.5') >= 0) {
  //           $cfile = curl_file_create($demo_certificate_filename);
  //       } else {
  //           $cfile = "@$demo_certificate_filename";
  //       }

  //       $ch = curl_init();
  //       curl_setopt($ch, CURLOPT_URL, $service);
  //       curl_setopt($ch, CURLOPT_POST, true);
  //       curl_setopt($ch, CURLOPT_POSTFIELDS, $post = array(
  //           'bundle' => $bundle,
  //           'environmentName' => 'tapcrowd_demo',
  //           'platform' => 'ios',
  //           'push_certificate'=> $cfile,
  //       ));

  //       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //       $result = curl_exec($ch);
  //       var_dump($result);
  //       curl_close($ch);
	}

	function appenvironments() {
		// $apps = $this->db->query("SELECT * FROM app WHERE isdeleted = 0")->result();

		// foreach($apps as $app) {
		// 	$this->general_model->insert('appenvironment', array(
		// 			'appid' => $app->id,
		// 			'environmentname' => 'tapcrowd_demo',
		// 			'push_certificateid' => 1
		// 		));

		// 	$certificateid = 0;
		// 	$certificatequery = $this->db->query("SELECT id FROM push_certificates WHERE appid = $app->id LIMIT 1");
		// 	if($certificatequery->num_rows() > 0) {
		// 		$certificateid = $certificatequery->row()->id;
		// 	}
		// 	$this->general_model->insert('appenvironment', array(
		// 			'appid' => $app->id,
		// 			'environmentname' => 'live',
		// 			'push_certificateid' => $certificateid
		// 		));
		// }
	}

	function phone() {
		// $eventid = 5114;
		// $attendees = $this->db->query("SELECT * FROM attendees WHERE eventid = $eventid")->result();
		// foreach($attendees as $att) {
		// 	$phonenr = '';
		// 	if(substr($att->phonenr, 0, 1) !== '0') {
		// 		$phonenr = '+'.trim($att->phonenr);
		// 		$this->db->query("UPDATE attendees SET phonenr = '$phonenr' WHERE id = $att->id");
		// 	}
		// }
	}

	function autosalon() {
		$appid = 3240;
		$eventid = 5640;
		$headers = true;
		$row = 0;
		$mapids = array(
				'1' => 1687,
				'2' => 1688,
				'3' => 1696,
				'5' => 1697,
				'6' => 1686,
				'7' => 1685,
				'8' => 1684,
				'9' => 1683,
				'10' => 1682,
				// '11' => 1699,
				// '12' => 1699,
				'13' => 1689,
				// '14' => 1698,
				'15' => 1695,
				'18' => 1698,
				'19' => 1699
			);
		if(file_exists($this->config->item('imagespath') . "upload/import/autosalon/autosalon.csv")) {
			if (($handle = fopen($this->config->item('imagespath') . "upload/import/autosalon/autosalon.csv", "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
					$num = count($data);
					$row++;
					$newrow = array();
					for ($c=0; $c < 14; $c++) {
						if($headers) {
							$heading[] = $data[$c];
						} else {
							$newrow[$heading[$c]] = $data[$c];
						}
					}

					if(!$headers) {
						$external_id = $newrow['external_id'];
						$mapid = 0;
						if(isset($mapids[$newrow['map_id']])) {
							$mapid = $mapids[$newrow['map_id']];
						}
						
						$x = $newrow['x'];
						$y = $newrow['y'];

						if(!empty($external_id)) {
							$this->db->query("UPDATE exhibitor SET mapid = ?, x1 = ?, y1 = ? WHERE eventid = ? AND external_id = ?", array($mapid, $x, $y, $eventid, $external_id));
						}
					}

					$headers = FALSE;
				}
			} else {
				$error = 'Oops, excel file could not be loaded.';
			}
		}
	}

	
}
