<?php
if (! defined('BASEPATH')) exit(__('No direct script access'));

require_once dirname(__DIR__) . '/models/session_model.php';
require_once dirname(__DIR__) . '/models/account_model.php';

/**
 * Aps Controller
 */
class Aps extends CI_Controller
{
	private $_sess_model;
	private $_module_settings;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		# Setup controller module
		$this->_module_settings = (object)array();

		# Get session model
		$this->_sess_model = \Tapcrowd\Model\Session::getInstance();
		$this->_account_model = Tapcrowd\Model\Account::getInstance();
	}

	public function index()
	{
		# Setup curl callback
		$domain = 'http://'.$_SERVER['HTTP_HOST'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$_curl_callback = function($params, $method) use ($ch, $domain)
		{
			# Create query
			$query = array();
			while(list($k,$v) = each($params)) $query[] = "$k=".urlencode($v);
			$query = implode('&',$query);

			# Create hash
			$hash = sha1( $query . 'athingofbeautyisajoyforever' );

			# Create url
			$url = "/aps/$method?$query&hash=$hash";

			# Output
			curl_setopt($ch, CURLOPT_URL, $domain . $url);
			$output = curl_exec($ch);
			echo "<strong>APS::$method() Example : <small>$url</small></strong>";
			echo '<pre>'.$output.'</pre>';
		};

		# SSO Test
		$params = array(
			'userid' => 123,
			'name' => 'White Test mouse',
			'email' => 'email@opencl.it',
			'credits_basicapp' => 22,
			'credits_plusapp' => 33,
			'credits_proapp' => 44,
			'channelname' => 'apstest',
		);
		$_curl_callback($params, 'sso');

		# Test getBilling
		$params = array(
			'userid' => 123,
			'channelname' => 'apstest',
		);
		$_curl_callback($params, 'getbilling');

		# Test cancelApp
		$params = array(
			'userid' => 123,
			'appid' => 456,
			'channelname' => 'apstest',
		);
		$_curl_callback($params, 'cancelapp');

		curl_close($ch);
	}

	/**
	 * Private Method to output http response
	 *
	 * @param int $http_code The http response code
	 * @param array $message Optional; The message
	 * @param array $headers Optional; Additional headers
	 * @return void
	 */
	private function _sendResponse( $http_code = 200, $message = '', $headers = null )
	{
		# Status header
		switch($http_code)
		{
			case 200: header('HTTP/1.1 200 OK'); break;
			case 302: header('HTTP/1.1 302 Found'); break;
			case 303: header('HTTP/1.1 303 See Other'); break;
			case 400: header('HTTP/1.1 400 Bad Request'); break;
			case 401: header('HTTP/1.1 401 Unauthorized'); break;
			case 405: header('HTTP/1.1 405 Method Not Allowed'); break;
			case 500: header('HTTP/1.1 500 Internal Server Error'); break;
		}

		# Headers
		if(is_array($headers)) foreach($headers as $header) header($header);

		# Message
		echo (empty($message) && $http_code == 200)? 'OK' : $message;
		exit;
	}

	/**
	 * Fake method to return the channel
	 *
	 * @param array $params Input parameters
	 * @return Channel object
	 */
	private function _getChannel( Array &$params )
	{
		#
		# TODO : Implement REAL channels
		#

		if($params['channelname'] != 'apstest') return null;
		else return (object)array(
			'title' => 'APStest',
			'qname' => 'apstest',
			'salt' => 'athingofbeautyisajoyforever',
		);
	}

	/**
	 * Fake method to return the channel
	 *
	 * @param array $params Input parameters
	 * @return Channel object
	 */
	private function _getAccount( Array &$params )
	{
		$channel = $this->_getChannel($params);

		$account = new \Tapcrowd\Account();
		$account->login = $channel->qname . '_' . $params['userid'];
		$account->accountId = $this->_sess_model->getAccountForLogin( $account->login );
		return $account;
	}

	/**
	 * Validate the request and returns the sanitized input params
	 * Responds with HTTP 4** response on failure
	 *
	 * @param array $keys The required keys (input value names)
	 * @return array params
	 */
	private function _verifyRequest( Array $keys )
	{
		# Verify HTTP Method
		if($_SERVER['REQUEST_METHOD'] != 'GET') {
			$this->_sendResponse(405, $_SERVER['REQUEST_METHOD']);
		}

		# Verify input
		$filter = array_fill_keys($keys, FILTER_SANITIZE_STRING);
		$params = filter_input_array(INPUT_GET, $filter);
		if($params === null || in_array(null, $params, true)) {
			$this->_sendResponse(400,'INVALID_PARAMS');
		}

		# Get channel
		$channel = $this->_getChannel($params);
		if($channel === null) {
			$this->_sendResponse(400,'INVALID_CHANNEL');
		}

		# Verify hash
		$query = array();
		$hash = filter_input(INPUT_GET, 'hash', FILTER_SANITIZE_STRING);
		while(list($k,$v) = each($params)) $query[] = "$k=".urlencode($v);
		if(( $hash !==  sha1( implode('&',$query) . $channel->salt ) )) {
			$this->_sendResponse(400,'INVALID_HASH');
		}

		return $params;
	}

	/**
	 * Single Sign on Method
	 *
	 * POST/GET parameters:
	 * 	userid
	 * 	name
	 * 	email
	 * 	credits_basicapp
	 * 	credits_plusapp
	 * 	credits_proapp
	 * 	channelname
	 * 	hash
	 */
	public function sso()
	{
		# Verify request
		$keys = array('userid','name','email','credits_basicapp','credits_plusapp','credits_proapp','channelname');
		$params = $this->_verifyRequest($keys);
		$channel = $this->_getChannel($params);

		# Get account
		$account = $this->_getAccount($params);
		$pass = hash_hmac('whirlpool', $account->login, $channel->salt);
		if($account->accountId === null) {
			try {
				$this->_sess_model->saveLogin( $account->login, $pass, $params['name'], $params['email']);
			} catch(\Exception $e) {
				$this->_sendResponse(500,'COULD_NOT_REGISTER '.$e->getMessage());
			}
		}

		# Authenticate account
		# -> This creates a new authenticated session linked using a token
		try {
			$token_query = $this->_sess_model->sso($account->login, $pass);
		} catch(\Exception $e) {
			$this->_sendResponse(500,'COULD_NOT_AUTHENTICATE '.$e->getMessage());
		}
		unset($account->login);

		# Update Account credits
		try {
			$balance = new \Tapcrowd\Balance();
			$balance->account = $account;
			$balance->credits->basic = (int)$params['credits_basicapp'];
			$balance->credits->plus = (int)$params['credits_plusapp'];
			$balance->credits->pro = (int)$params['credits_proapp'];

			$this->_account_model->setAccountBalance( $balance );
		} catch(\Exception $e) {
			$this->_sendResponse(500,'COULD_NOT_SET_BALANCE');
		}

		# Send response (token)
		$url = 'http://'.$_SERVER['HTTP_HOST'].'/?'.$token_query;
		$this->_sendResponse(303, "<a href=\"$url\">$token_query</a>", array("Location: $url"));
	}

	/**
	 * Returns the recurrent billing information
	 *
	 * @return int
	 */
	public function getBilling()
	{
		# Verify request
		$keys = array('userid','channelname');
		$params = $this->_verifyRequest($keys);
		$channel = $this->_getChannel();

		# Verify Account
		$account = $this->_getAccount($params);
		if($account->accountId === null) {
			$this->_sendResponse(400,'INVALID_USER');
		}
		unset($account->login);

		# Send Account balance
		$balance = $this->_account_model->getAccountBalance($account);
		$this->_sendResponse(200, json_encode($balance));
	}

	/**
	 * Cancels the application
	 *
	 * @return int
	 */
	public function cancelApp()
	{
		# Verify request
		$keys = array('userid','appid','channelname');
		$params = $this->_verifyRequest($keys);
		$channel = $this->_getChannel();

		# Verify account
		$account = $this->_getAccount($params);
		if($account->accountId === null) {
			$this->_sendResponse(400,'INVALID_USER');
		}

		try {
			#
			# TODO : Cancel the application (or at least payment)
			#
		} catch(\Exception $e) {
			$this->_sendResponse(500,'COULD_NOT_CANCEL_APP');
		}

		$this->_sendResponse(200, 'OK');
	}
}