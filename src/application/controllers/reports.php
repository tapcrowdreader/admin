<?php if(!defined('BASEPATH')) exit(__('No direct script access'));
class Reports extends CI_Controller {
	
	private $report = array(); #This variable will contain information to create the reports
	
	//php 5 constructor
	function __construct()
	{
		parent::__construct();
		$this->load->model('reports_model');
	}
	
	/**
	 * @param $function function to call next.
	 * @param $type the type, event or venue.
	 * @param $csv if 1 will produce a csv file with the information in $this->report
	 */
	function view($function, $type, $csv = 0)
	{
		switch($function)
		{
			case "personal": $this->personal($type, $csv); break;
			case "basket": $this->basket($type,$csv); break;
		}
	}
	
	/**
	 * @param $type the type, event or venue.
	 * @param $csv if 1 will produce a csv file with the information in $this->report
	 */
	function personal($type, $csv, $eventid = '')
	{
		if($type == 'venue') {
			#Local variable initialization
			if($eventid != '') {
				$venue = $this->venue_model->getById($eventid); //$event contains the current event object
			} else {
				$venue = _currentVenue(); //$event contains the current event object
			}
			
			$users = $this->reports_model->users($venue->id, 'venue'); //$users contain a list of the users that are using a conference bag
			$reporttype = "confbag"; //$reporttype is the type of report to be produced
			
			#Fills $this->report with all the existent bags information
			foreach($users->result() as $user){
				
				//$results will contain all the $items in the $user's conference bag.
				$results = $this->reports_model->getVenueInfo("personal", "email", $user->email, $venue->id);
				
				//array that will contain the items that the user added to it's bag
				$items = array();
				
				foreach($results->result() as $result)
				{
					if($result->table == "catalog")
					{
						//gets the attendee information
						$catalog = $this->reports_model->getVenueInfo("catalog","id",$result->tableid, $venue->id)->row();
						
						//generates a full name for the attendee
						$name = $catalog->name;
						
						//adds the attendee to the $items array
						$items[] = array('type' => "Catalog", 'element' => $name, 'categories' => array());
					}
					else
						//adds other elements to the $items array
						$items[] = array('type' => "Other", 'element' => $result->extra, 'categories' => array());
				}
				//adds the user and it's items to the report
				$this->report[] = array('user' => $user->email, 'items' => $items);
			}
			
			//Produces a report in html or csv
			if($csv == 1)
			{
				//calls the csv file creation function
				$this->getReport($reporttype);
			} elseif($csv == 2) {
				//return data
				return json_encode($this->report);
			}
			else
			{
				$launcher = $this->module_mdl->getLauncher(42, 'venue', $venue->id);
				if($launcher->newLauncherItem) {
					$launcher->title = __($launcher->title);
				}
				$title = $launcher->title;
				$cdata['crumb']	= array($venue->name => "$type/view/$venue->id",
								$title => "confbag/$type/$venue->id",
								"Reports" => "reports/personal/$type/0/".$venue->id);
				$cdata['content'] = $this->load->view('c_report_bag', array('report' => $this->report, 'type' => $type, 'typeid' => $venue->id), TRUE);
				$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'confbag');
				
				$this->load->view('master', $cdata);
			}
		} else {
			#Local variable initialization
			if($eventid != '') {
				$event = $this->event_model->getbyid($eventid); //$event contains the current event object
			} else {
				$event = _currentEvent(); //$event contains the current event object
			}
			
			$users = $this->reports_model->users($event->id); //$users contain a list of the users that are using a conference bag
			$reporttype = "confbag"; //$reporttype is the type of report to be produced
			
			#Fills $this->report with all the existent bags information
			foreach($users->result() as $user){
				
				//$results will contain all the $items in the $user's conference bag.
				$results = $this->reports_model->getEventInfo("personal", "email", $user->email, $event->id, $reporttype);
				
				//array that will contain the items that the user added to it's bag
				$items = array();
				
				foreach($results->result() as $result)
				{
					if($result->table == "exhibitors")
					{
						$categories = array();
						
						$res = $this->reports_model->getEventInfo("groupitem","itemid",$result->tableid,$event->id, $reporttype);
						
						//saves the categories this exhibitor has into the $categories array
						if($res->num_rows() != 0)
						{
							foreach($res->result() as $row)
							{
								$cat = $this->reports_model->getEventInfo("group","id",$row->groupid,$event->id, $reporttype)->row();
								$categories[] = $cat->name;
							}
						}
						
						//gets the exhibitor information
						$exhibitor = $this->reports_model->getEventInfo("exhibitor","id",$result->tableid,$event->id, $reporttype)->row();
						
						//adds the exhibitor to the $items array
						$items[] = array('type' => "Exhibitor", 'element' => $exhibitor->name, 'categories' => $categories);
						
					}
					elseif($result->table == "sessions" || $result->table == 'session')
					{
						//gets the session information
						$session = $this->reports_model->getEventInfo("session","id",$result->tableid, $event->id, $reporttype)->row();
						
						//adds the session to the $items array
						$items[] = array('type' => "Session", 'element' => $session->name, 'categories' => array());
					}
					elseif($result->table == "attendees")
					{
						//gets the attendee information
						$attendee = $this->reports_model->getEventInfo("attendees","id",$result->tableid, $event->id, $reporttype)->row();
						
						//generates a full name for the attendee
						$name = $attendee->firstname." ".$attendee->name;
						
						//adds the attendee to the $items array
						$items[] = array('type' => "Attendee", 'element' => $name, 'categories' => array());
					}
					else {
						//adds other elements to the $items array
						$items[] = array('type' => "Other", 'element' => $result->extra, 'categories' => array());
					}
				}
				//adds the user and it's items to the report
				$this->report[] = array('user' => $user->email, 'items' => $items);
			}
			
			//Produces a report in html or csv
			if($csv == 1)
			{
				//calls the csv file creation function
				$this->getReport($reporttype);
			} elseif($csv == 2) {
				//return data
				return json_encode($this->report);
			}
			else
			{
				$launcher = $this->module_mdl->getLauncher(42, 'event', $event->id);
				if($launcher->newLauncherItem) {
					$launcher->title = __($launcher->title);
				}
				$title = $launcher->title;
				$cdata['crumb']	= array($event->name => "$type/view/$event->id",
								$title => "confbag/$type/$event->id",
								"Reports" => "reports/view/personal/$type");
				$cdata['content'] = $this->load->view('c_report_bag', array('report' => $this->report, 'type' => $type, 'typeid' => $event->id), TRUE);
				$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'confbag');
				
				$this->load->view('master', $cdata);
			}
		}
	}
	
	/**
	 * @param $type the type, event or venue.
	 * @param $csv if 1 will produce a csv file with the information in $this->report
	 */
	function basket($type, $csv)
	{
		#Local variable initialization
		
		$venue = _currentVenue(); //contains the current venue object
		$orders = $this->reports_model->orders($venue->id); //contains a list of the orders that were made to this venue
		$reporttype = "basket"; //the type of report to be produced
		
		#Fills $this->report with all the existent bags information
		foreach($orders->result() as $order)
		{
			//$results will contain all the items in a specific $order
			$results = $this->reports_model->getVenueInfo("orders", "orderid", $order->orderid, $venue->id);
			//$items will store all the items for that $order
			$items = array();
			
			foreach($results->result() as $result)
			{
				//$item will store a specific item information
				$item = $this->reports_model->getVenueInfo("catalog","id",$result->catalogid,$venue->id)->row();
				//$group the group information for the $item
				$group = $this->reports_model->getVenueInfo("group","id",$result->groupid,$venue->id)->row();
				//$quantity is the quantity of $item bought
				$quantity = $result->quantity;
				
				//adds the item to the $items array
				$items[] = array('element' => $item->name, 'category' => $group->name, 'quantity' => $quantity);
			}
			
			//adds the order to the report
			$this->report[] = array('order' => $order->orderid, 'items' => $items);
		}
		
		//Produces a report in html or csv
		if($csv == 1)
		{
			//calls the csv file creation function
			$this->getReport($reporttype);
		}
		else
		{
			$cdata['crumb']	= array($venue->name => "venue/view/$venue->id",
							'Basket' => "basket/venue/$venue->id",
							"Reports" => "reports/view/basket/venue/$venue->id");
			$cdata['content'] = $this->load->view('c_report_basket', array('report' => $this->report), TRUE);
			
			$this->load->view('master', $cdata);
		}
	}
	
	/**
	 * @param $type is the type of report
	 */
	function getReport($type){
		
		#Local variables initialization
		$date = date("dmY"); //$date will contain today's date
		$filename = $type.$date."report.csv"; //$filename will be composed of the type of report+date and report.csv
		
		/*
		 * headers so that the browser recognizes he has to produce a downloadable file
		 * out of content that will be 'echo'ed next
		 */
		header('Content-Encoding: UTF-8');
        header('Content-type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment;filename='.$filename);
		
		//echo "\xEF\xBB\xBF";
		
		//open the stream
        $fp = fopen('php://output','w');
		
		//calls the function that will create the csv structure
		$this->array_to_csv($type,$this->report);
		
		//closes the stream
		fclose($fp);
	}
	
	/**
	 * @param $type the type of report
	 * @param $array containing the report information
	 */
	function array_to_csv($type, $array){
		switch($type)
		{
			case "confbag":
				echo 'User;Type;Name;Categories'."\n";
				$this->exhibitor_array($array);
				break;
	  		case "basket":
				echo 'Order;Category;Item;Quantity'."\n";
				$this->basket_array($array);
				break;
			case "bisbeurs":
				echo 'User;Name;Firstname;Tel;Type;Title;Categories;'."\n";
				$this->bisbeurs_array($array);
				break;
		}
	}
	
	/**
	 * @param $array containing the report information
	 */
	function exhibitor_array($array)
	{
		foreach($array as $user)
		{
			//$items is the $items array for the $user
			$items = $user['items'];
			foreach($items as $item)
			{
				//send out the user
				$str = str_replace(";"," ",$user['user']);
				echo $str.";";
				
				//In case it's an exhibitor checks if he has categories
				if($item['type'] == "Exhibitor")
				{
					//send out the item type
					$str = str_replace(";"," ",$item['type']);
					echo $str.";";
					
					//$categories contains the categories the item belongs to
					$categories = $item['categories'];
					
					//If the category is empty send only the exhibitor out,
					//Else sends the exhibitor and it's categories
					if(empty($categories))
					{
						$str = str_replace(";"," ",$item['element']);
						echo $str;
						echo "\n";
					}
					else
					{
						$str = str_replace(";"," ",$item['element']);
						echo $str.";";
						foreach($categories as $category)
						{
							$str = str_replace(";"," ",$category);
							echo "(".$str.")";
						}
						echo "\n";
					}
				}
				else
				{
					//Sends out the type of item and the item itself
					$str = str_replace(";"," ",$item['type']);
					echo $str.";";
					$str = str_replace(";"," ",$item['element']);
					echo $str.";";
					echo "\n";
				}
			}
		}
	}

	/**
	 * @param $array containing the report information
	 */
	function bisbeurs_array($array)
	{
		foreach($array as $user)
		{
			//$items is the $items array for the $user
			$items = $user['items'];
			foreach($items as $item)
			{		

				//send out the user
				$str = str_replace(";"," ",$user['user']);
				echo $str.";";
				//send out the user info
				$str = str_replace(";"," ",$user['familyname']);
				echo $str.";";
				$str = str_replace(";"," ",$user['firstname']);
				echo $str.";";
				$str = str_replace(";"," ",$user['tel']);
				echo $str.";";

				//In case it's an exhibitor checks if he has categories
				if($item['type'] == "Exhibitor")
				{
					//send out the item type
					$str = str_replace(";"," ",$item['type']);
					echo $str.";";
					
					//$categories contains the categories the item belongs to
					$categories = $item['categories'];
					
					//If the category is empty send only the exhibitor out,
					//Else sends the exhibitor and it's categories
					if(empty($categories))
					{
						$str = str_replace(";"," ",$item['element']);
						echo $str;
						echo "\n";
					}
					else
					{
						$str = str_replace(";"," ",$item['element']);
						echo $str.";";
						foreach($categories as $category)
						{
							$str = str_replace(";"," ",$category);
							echo "(".$str.")";
						}
						echo "\n";
					}
				}
				else
				{
					//Sends out the type of item and the item itself
					$str = str_replace(";"," ",$item['type']);
					echo $str.";";
					$str = str_replace(";"," ",$item['element']);
					echo $str.";";
					echo "\n";
				}
			}
		}
	}
	
	/**
	 * @param $array containing the report information
	 */
	function basket_array($array)
	{
		foreach($array as $order)
		{
			$items = $order['items'];
			
			foreach($items as $item)
			{
				//sending out all the information about an item on a specific order
				$str = str_replace(";"," ",$order['order']);
				echo $str.";";
				$str = str_replace(";"," ",$item['category']);
				echo $str.";";
				$str = str_replace(";"," ",$item['element']);
				echo $str.";";
				$str = str_replace(";"," ",$item['quantity']);
				echo $str;
				echo "\n";
			}
		}
	}
	
	
	function bisbeursconfbag() {
		$formid = 131;
		$formeventid = 3786;
		$eventid = 2441;
		$reporttype = 'bisbeurs';

		$res = json_decode($this->personal('event', 2, $eventid));

		$formsubmissions = $this->db->query("SELECT * FROM formsubmission WHERE formid = $formid")->result();
		$submissionids = array();
		foreach($formsubmissions as $fs) {
			$submissionids[] = $fs->id;
		}
		$submissionids = implode(',', $submissionids);
		$query = $this->db->query("SELECT id, formsubmissionid, GROUP_CONCAT(concat(label, '=', value) SEPARATOR '----') as value FROM (
									SELECT fs.id, fs.formsubmissionid, fd.label, fs.value 
									FROM formsubmissionfield fs INNER JOIN formfield fd ON fd.id = fs.formfieldid 
									WHERE formsubmissionid IN ($submissionids)) as tmp GROUP BY tmp.formsubmissionid
									")->result();

		// $results = $this->db->query("SELECT id, GROUP_CONCAT(`value` SEPARATOR '----') as value FROM formsubmissionfield WHERE formsubmissionid IN ($submissionids) GROUP BY formsubmissionid ORDER BY formsubmissionid")->result();
		foreach($query as $q) {
			$values = explode('----', $q->value);
			foreach($values as $v) {
				$values2 = explode('=', $v);
				if($values2[0] == 'E-Mail') {
					$email = $values2[1];
				} elseif($values2[0] == 'Naam') {
					$familyname = $values2[1];
				} elseif($values2[0] == 'Voornaam') {
					$firstname = $values2[1];
				} elseif($values2[0] == 'Telefoonnummer') {
					$tel = $values2[1];
				}
				foreach($res as &$r) {
					if($r->user == $email) {
						$r->email = $email;
						$r->familyname = $familyname;
						$r->firstname = $firstname;
						$r->tel = $tel;
					}
				}
			}
		}
		foreach($res as &$r) {
			foreach($r->items as &$items) {
				$items = (array) $items;
			}
			if($r instanceof stdClass) {
				$r = (array) $r;
			}
		}
		$this->report = $res;
		$this->getReport($reporttype);
	}
}