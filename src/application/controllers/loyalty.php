<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Loyalty extends CI_Controller {
	
	//php 5 constructor
	function __construct()
	{
		parent::__construct();
	}
	
	function index(){
		$this->venue();
	}
	
	/**
	 * @param $id, The venue ID
	 * @return Creates the views for edit or add a loyalty module to a venue
	 */
	function venue($id) {
		$this->load->library('form_validation');

		$error = "";

		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->iframeurl = 'loyalty/venue/'.$id;

		$languages = $this->language_model->getLanguagesOfApp($app->id);
		
		$result = $this->db->query("SELECT * FROM loyalty WHERE venueid = $venue->id");
		
		$row = $result->row();
		
		$loyaltyid = 0;
		
		if($this->input->post('postback') == "postback") {
			$data = array(
					'venueid'	=> $venue->id,
					'title'		=> $this->input->post('title_'.$app->defaultlanguage),
					'text'		=> $this->input->post('text_'.$app->defaultlanguage),
					'code'		=> $this->input->post('code')
				);
				

			if($result->num_rows() == 0)
				$this->general_model->insert('loyalty', $data);
			else{
				$title = $data['title'];
				$text = $data['text'];
				$code = $data['code'];
				
				$this->db->query("UPDATE loyalty SET title='$title', text='$text', code='$code' WHERE venueid=$venue->id");
			}
			
			$lid = $this->db->query("SELECT loyaltyid FROM loyalty WHERE venueid=$venue->id");
			
			$loyaltyid = $lid->row()->loyaltyid;
						
			if(intval($loyaltyid) != 0){
				
                foreach($languages as $language) {
                	$trans = $this->language_model->getTranslation('loyalty', $loyaltyid, 'title', $language->key, $this->input->post('title_'.$language->key));
										
					if(empty($trans))
					{	
						//add translated fields to translation table
                		$this->language_model->addTranslation('loyalty', $loyaltyid, 'title', $language->key, $this->input->post('title_'.$language->key));
                    	$this->language_model->addTranslation('loyalty', $loyaltyid, 'text', $language->key, $this->input->post('text_'.$language->key));
					}
					else {
						//update translated fields to translation table
	                    foreach($languages as $language) {
	                    	$this->language_model->updateTranslation('loyalty', $loyaltyid, 'title', $language->key, $this->input->post('title_'.$language->key));
	                        $this->language_model->updateTranslation('loyalty', $loyaltyid, 'text', $language->key, $this->input->post('text_'.$language->key));
						}
					}
				}
				
				$this->session->set_flashdata('venue_feedback', __('Loyalty module changed.'));
				_updateTimeStamp($event->id);
				if($error == '') {
					redirect('venue/view/'.$venue->id);
				} else {
					//image error
					redirect('loyalty/venue/'.$id.'?error=image');
				}
			} else {
				$error = __("Oops, something went wrong. Please try again.");
			}
		}
		
		if($result->num_rows() == 0){
			$cdata['content'] 	= $this->load->view('c_loyalty', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
		} else {
			$cdata['content'] 	= $this->load->view('c_loyalty_edit', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'loyalty'=>$row), TRUE);
		}
		$cdata['crumb']			= array($venue->name => "venue/view/".$venue->id, __("Loyalty") => 'loyalty/venue/'.$id);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('loyalty' => $loyalty), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'loyalty');
		$this->load->view('master', $cdata);
	}

	/**
	 * @param $id, the venue id
	 * @param $code, the Code for adding new points that will be translated into a qr code
	 */
	function printcode($id, $code) {
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->iframeurl = 'loyalty/venue/'.$id;
		$domain = 'http://'.$app->subdomain.'.'.$this->config->item('mobilesite');
		
		$mycode = $domain.'/loyalty/venue/'.$venue->id.'/'.$code;
		
		$this->load->view('c_print_qrcode', array('app' => $app, 'venueid' => $id, 'code' => $mycode));
	}
}

	