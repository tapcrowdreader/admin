<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Apps extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('app_model');
		$this->load->model('appearance_model');
		$this->load->model('citycontent_model');
	}

	function dashboard() {
		$this->session->set_userdata('appid', '');
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');

		#
		# Fetch Applications for current account
		#
		$account = \Tapcrowd\API::getCurrentUser();
		$apps = \Tapcrowd\API::listApplications($account);
		// $apps = $this->app_model->appsByOrganizer(_currentUser()->organizerId);

		$latestApp = false;
		$downloadinfo = false;
		$totaldownloads = 0;
		$androiddownloads = 0;
		$iosdownloads = 0;
		$webdownloads = 0;
		$totalactions = 0;
		if($apps != false && !empty($apps)) {
			if(count($apps) > 5) {
				for($i = count($apps) - 1; $i >= count($apps) - 5; $i--) {
					$latestApps[] = $apps[$i];
				}
			} else {
				$latestApps = $apps;
			}

			// $ch = curl_init('http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']).'/0.2/analyticsservice/uniquev2?bundle='.$latestApp->bundle);
			// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			// $downloadinfo = curl_exec($ch);
			// curl_close($ch);

			// $downloadinfo = json_decode($downloadinfo);
			// foreach($downloadinfo as $d) {
			// 	if($d->OS == 'android') {
			// 		$androiddownloads += $d->downloads;
			// 	} elseif($d->OS == 'iOS') {
			// 		$iosdownloads += $d->downloads;
			// 	} elseif($d->OS == 'web') {
			// 		$webdownloads += $d->downloads;
			// 	}
			// }

			// $totaldownloads = $androiddownloads + $iosdownloads + $webdownloads;
			// $androidpercent = round($androiddownloads * 100 / $totaldownloads, 0);
			// $iospercent = round($iosdownloads * 100 / $totaldownloads, 0);
			// $webpercent= round($webdownloads * 100 / $totaldownloads, 0);

			// $ch = curl_init('http://'.str_replace('admin', 'taptarget', $_SERVER['SERVER_NAME']).'/0.2/analyticsservice/totalActions?bundle='.$latestApp->bundle);
			// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			// $totalactions = curl_exec($ch);
			// curl_close($ch);

			// $totalactions = json_decode($totalactions)->total;
		} else {
			$latestApps = array();
		}

		$this->load->view('c_dashboard', array('latestApps' => $latestApps, 'account' => $account));

	}

	function index() {
		redirect('apps/dashboard');
		$this->session->set_userdata('appid', '');
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');

		#
		# Fetch Applications for current account
		#
		$account = \Tapcrowd\API::getCurrentUser();
		$apps = \Tapcrowd\API::listApplications($account);
		// $apps = $this->app_model->appsByOrganizer(_currentUser()->organizerId);

		if($apps == false || empty($apps)) {

			# Validate account permissions
			if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.create', $account) === false) {
				\Notifications::getInstance()->alert('No Permission to Create Applications', 'notice');
			} else {
				redirect('apps/add');
			}
		}

		// if($apps != false && !_isAdmin() && count($apps) == 1) {
		// 	redirect('apps/view/'.$apps[0]->id);
		// }

        $right = __('<h3>New app</h3><p>Click "New Mobile app" to create an app</p>
                  <h3>Build your app</h3><p>Click "Manage" to build your mobile app and to add information. </p>
                  <h3>Submit your app</h3><p>When your app is ready, click "Submit" to make your app available to your audience.</p>');

		$cdata['content'] 		= $this->load->view('c_apps', array('apps' => $apps, 'right' => $right), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array();
		$this->load->view('master_wizard', $cdata);
	}

	function myapps() {
		$this->session->set_userdata('appid', '');
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');

		#
		# Fetch Applications for current account
		#
		$account = \Tapcrowd\API::getCurrentUser();
		$apps = \Tapcrowd\API::listApplications($account);
		// $apps = $this->app_model->appsByOrganizer(_currentUser()->organizerId);

		if($apps == false || empty($apps)) {

			# Validate account permissions
			if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.create', $account) === false) {
				\Notifications::getInstance()->alert('No Permission to Create Applications', 'notice');
			} else {
				redirect('apps/add');
			}
		}

		// if($apps != false && !_isAdmin() && count($apps) == 1) {
		// 	redirect('apps/view/'.$apps[0]->id);
		// }

        $right = __('<h3>New app</h3><p>Click "New Mobile app" to create an app</p>
                  <h3>Build your app</h3><p>Click "Manage" to build your mobile app and to add information. </p>
                  <h3>Submit your app</h3><p>When your app is ready, click "Submit" to make your app available to your audience.</p>');

		$cdata['content'] 		= $this->load->view('c_apps', array('apps' => $apps, 'right' => $right), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array();
		$this->load->view('master_wizard', $cdata);
	}

	function manage($id) {
		$app = $this->app_model->get($id);
		if($app == FALSE) {
			$this->session->set_flashdata('tc_error', __('App not found!'));
			redirect('apps');
		}
		_actionAllowed($app, 'app');
			$this->session->set_flashdata('tc_error', __('We would recommend to manage your own apps!'));
			redirect('apps');

		$this->session->set_userdata('appid', $id);
		if($app->familyid == 3) redirect('apps/view/'.$app->id);
		if($app->familyid == 2) redirect('venues');
		redirect('events');
	}

	function add() {
		$this->load->model('module_mdl');
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');

		$error = '';

		$this->load->library('form_validation');

        $languages = $this->language_model->getAllLanguages();

        $right = __('<h3>Select a flavor</h3>
                    <p>Select a flavor for your app. The flavor defines what type of application you will build. e.g. an app about a restaurant, a festival, ...</p>');
		$apps = $this->app_model->appsByOrganizer(_currentUser()->organizerId);
		// if(!_isAdmin() && $apps) {
		// 	//Bartel Van Herreweghe has access
		// 	if(_currentUser()->organizerId != 74) {
		// 		redirect('apps/view/'.$apps[0]->id);
		// 	}
		// }

		$flavors = $this->app_model->getAppTypes();
		if(!$flavors) $flavors = array();

		// APPTYPE CHOSEN
		if($this->input->post('postback') == 'postback' || isset($_GET['apptypeid'])) {
			// lastlogin aanpassen van de user voor volgende login
			if(isset($_GET['apptypeid'])) {
				$flavorid = $_GET['apptypeid'];
			} else {
				$flavorid = $this->input->post('flavorid');
			}
			

			$this->load->model('module_mdl');
			$subflavors = $this->module_mdl->getSubflavorsOfFlavor($flavorid);
			$flavor = $this->app_model->getFlavorOfId($flavorid);

			$cdata['content'] = $this->load->view('c_app_add', array('step' => 2, 'error' => $error, 'flavorid' => $flavorid, 'flavors' => $flavors, 'languages' => $languages, 'right' => $right, 'subflavors' => $subflavors, 'flavor' => $flavor), TRUE);

			if($flavorid == 12) {
				$cdata['content'] = $this->load->view('c_app_blank', array('error' => $error, 'flavorid' => $flavorid), TRUE);
			}
		} else {
			$cdata['content'] = $this->load->view('c_app_add', array('step' => 1, 'error' => $error, 'languages' => $languages, 'flavors' => $flavors, 'right' => $right), TRUE);
		}

		// ADD APP TO DATABASE
		if($this->input->post('postback2') == 'postback2') {

			# Validate account permissions
			$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
			if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.create', $account) === false) {
				\Notifications::getInstance()->alert('No Permission to Create Applications', 'error');
				redirect('apps/add');
			}

            $flavors = $this->app_model->getAppTypes();
			//$this->form_validation->set_rules('flavor', 'flavor', 'trim|required');
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('urlname', 'urlname', 'trim|required');
			// $this->form_validation->set_rules('cname', 'cname', 'trim');
			$this->form_validation->set_rules('cname', 'Website', 'trim|prep_url|valid_url|xss_clean');
            $this->form_validation->set_rules('defaultlang', 'defaultlang', 'trim|required');
			$this->form_validation->set_rules('subflavor', 'subflavor', 'trim');
			$this->form_validation->set_rules('flavor', 'flavor', 'trim');

			if($this->form_validation->run() == FALSE){
				$error = __("<p>Some fields are missing.</p>");
			} else {

				# Validate subdomain
				try {
					$subdomain = $this->input->post('urlname');
					$this->app_model->validateSubdomain($subdomain);
				} catch(\InvalidArgumentException $e) {
					$error .= '<p>' . $e->getMessage() . '</p>';
				}

				if($error == '') {
					$responsive = 1;
					if($this->input->post('flavorid') == 9) {
						$responsive = 0;
					}
					$appdata = array(
							'apptypeid'			=> $this->input->post('flavorid'),
							'organizerid'		=> _currentUser()->organizerId,
							'accountId'			=> _currentUser()->accountId,
							'name'				=> set_value('name'),
							'token'				=> md5($this->input->post('name').$this->input->post('urlname').'tapcrowdapitoken123456'),
							'certificate'		=> '',
							'subdomain' 		=> set_value('urlname'),
							'cname' 			=> set_value('cname'),
							'advancedFilter'	=> 0,
	                        'defaultlanguage'   => $this->input->post('defaultlang'),
	                        'responsive'		=> $responsive,
	                        'bundle'			=> 'com.tapcrowd.'.rand(0,50000)
						);


					$res = $this->general_model->insert('app', $appdata);

					$bundles = $this->app_model->getBundles();

					$bundle = $this->input->post('name').$res;

					$some_special_chars = array("'", '"', '&', '%', '$', '.', ',', '(', ')', '!', ' ',';', ':');
					$replacement_chars  = array("", "", '', '', '', '', '', '', '', '', '', '', '');

					$bundle =  preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $bundle);
					$bundle    = 'com.tapcrowd.'.str_replace($some_special_chars, $replacement_chars, $bundle);
					if(isset($bundles[$bundle])) {
						$bundle = $bundle.substr(md5($bundle.time()), 0, 5);
					}
					$this->general_model->update('app', $res, array('bundle' => $bundle));

					//insert info launcher
					$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE id = 3")->row();

					$launcherdata = array(
						'moduletypeid'	=> 21,
						'appid'			=> $res,
						'module'		=> $defaultlauncher->module,
						'title'			=> $defaultlauncher->title,
						'icon'			=> $defaultlauncher->icon,
						'order'			=> $defaultlauncher->order,
						'active'		=> 1
						);

					$this->general_model->insert('launcher',$launcherdata);

					//for cities insert news module on app level + other default modules like Wine & Dine, ...
					if($this->input->post('flavorid') == 5) {
						$this->module_mdl->createCityLaunchers($res, 3);
					}

					//for content apps insert home module
					if($this->input->post('flavorid') == 9) {
						$this->module_mdl->createContentLaunchers($res, 9);
					}

	                if($this->input->post('language') != "") {
	                    $sellanguages = $this->input->post('language');
	                    foreach($sellanguages as $lang) {
	                        $langdata = array(
	                            'appid'     => $res,
	                            'language'  => $lang
	                        );
	                        $this->general_model->insert('applanguage', $langdata);
	                    }
	                }


					if($res != FALSE){
						if(set_value('subflavor') != null && set_value('subflavor') != '') {
							$subflavordata = array(
								'appid'		=> $res,
								'subflavorid'	=> set_value('subflavor')
							);
							$this->general_model->insert('appsubflavor', $subflavordata);
						} else {
							$subflavordata = array(
								'appid'		=> $res,
								'subflavorid'	=> 3
							);
							$this->general_model->insert('appsubflavor', $subflavordata);
						}

						//Make app in demo state
						$this->general_model->insert('appprice', array(
								'appid' => $res,
								'priceid' => 0,
								'appstatusid' => 1
							));

						// update lastlogin
						$this->load->model('user_mdl');
						$this->user_mdl->edit_user(_currentUser()->organizerId, array('lastlogin' => date('Y-m-d H:i:s', time())));
						// $this->session->set_flashdata('event_feedback', 'The app has successfully been created!');
						$this->session->set_userdata('appid', $res);

						// register app in taptarget
						$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerApplication');
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.strtolower($bundle).'&title='.urlencode($appdata['name']));
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);

						$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/setApplicationRole');
						curl_setopt($ch, CURLOPT_POST, true);
						$role = 'user';
						if(_isAdmin()) {
							$role = 'admin';
						}
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&accountId='._currentUser()->accountId.'&role='.$role);  
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);

						$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
						$democertificate = '/var/www/CLIENTS/push/ios/demoapp-prod.pem';
						$post = array('bundle' => $bundle, 'environmentName' => 'tapcrowd_demo', 'platform' => 'ios', 'push_certificate'=>'@'.$democertificate);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $post);  
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);

						$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=tapcrowd_demo&platform=android');  
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);

						$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=tapcrowd_demo&platform=web');  
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);

						$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=live&platform=android');  
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);

						$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=live&platform=ios');  
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);

						$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
						curl_setopt($ch, CURLOPT_POST, true);                                                                     
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=live&platform=web');  
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);

						$this->general_model->insert('appenvironment', array(
								'appid' => $res,
								'environmentname' => 'tapcrowd_demo',
								'push_certificateid' => 1
							));

						$this->general_model->insert('appenvironment', array(
								'appid' => $res,
								'environmentname' => 'live'
							));

						if($this->input->post('flavorid') == 3 || $this->input->post('flavorid') == 10) {
							redirect("event/add?newapp");
						} elseif($this->input->post('flavorid') == 1 || $this->input->post('flavorid') == 8 || $this->input->post('flavorid') == 7 || $this->input->post('flavorid') == 4 || $this->input->post('flavorid') == 11) {
							redirect("venue/add?newapp");
						} else {
							redirect("apps/theme/$res?newapp");
						}

					} else {
						$error = "Error, something went wrong, please try again.";
					}
				}
				$cdata['content'] = $this->load->view('c_app_add', array('step' => 2, 'error' => $error, 'flavorid' => $this->input->post('flavorid'), 'flavors' => $flavors, 'right' => $right, 'sellanguages' => $this->input->post('language'), 'seldefaultlang' => $this->input->post('defaultlang'), 'languagesarray' => $this->language_model->getLanguagesAsArray()), TRUE);
			}
        $cdata['content'] = $this->load->view('c_app_add', array('step' => 2, 'error' => $error, 'flavorid' => $this->input->post('flavorid'), 'flavors' => $flavors, 'right' => $right, 'sellanguages' => $this->input->post('language'), 'seldefaultlang' => $this->input->post('defaultlang'), 'languagesarray' => $this->language_model->getLanguagesAsArray()), TRUE);
		}

		// $cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array("My mobile apps" => "apps");
		$this->load->view('master_wizard', $cdata);
	}

	function edit($id) {
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');

		$this->load->library('form_validation');
		$error = "";

        $languages = $this->language_model->getAllLanguages();

		$flavors = $this->app_model->getAppTypes();
		if(!$flavors) $flavors = array();

		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		if($app->apptypeid == 12) {
			redirect('apps/blank/'.$app->id);
		}

		$applanguages = $this->language_model->getLanguagesOfApp($id);
		$applangkeys = array();
		foreach($applanguages as $applang) {
			$applangkeys[] = $applang->key;
		}

		$scriptphp = $this->app_model->getRedirectScript('php', $app);
		$scriptjs = $this->app_model->getRedirectScript('js', $app);
		$popupscript = $this->app_model->getPopupScript($app);

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('info', 'info', 'trim');
			$this->form_validation->set_rules('externalids', 'externalids', 'trim');
			$this->form_validation->set_rules('urlname', 'urlname', 'trim|required');
			// $this->form_validation->set_rules('cname', 'cname', 'trim');
			$this->form_validation->set_rules('cname', 'Website', 'trim|prep_url|valid_url|xss_clean');
			$this->form_validation->set_rules('launcherview', 'launcherview', 'trim');
			$this->form_validation->set_rules('certificate', 'certificate', 'trim');
			$this->form_validation->set_rules('environment', 'certificate', 'trim');

			if($this->form_validation->run() == FALSE){
				$error = "<p>Some fields are missing.</p>";
			} else {

				# Validate subdomain
				try {
					$subdomain = $this->input->post('urlname');
					$this->app_model->validateSubdomain($subdomain, $app->id);
				} catch(\InvalidArgumentException $e) {
					$error .= '<p>' . $e->getMessage() . '</p>';
				}

				if($error == '') {
					$externalids = 0;
					if($this->input->post('externalids')) {
						$externalids = 1;
					}
					$certificate = '';
					if($_FILES['certificate']['size'] > 0) {
						if(file_exists($_FILES['certificate']['tmp_name'])) {
							if(($f = fopen($_FILES['certificate']['tmp_name'], "r")) !== FALSE) {
								$certificate = fread($f, filesize($_FILES['certificate']['tmp_name']));
								fclose($f); 

								$this->db->query("DELETE FROM push_certificates WHERE appid = $app->id AND type = 'ios'");
								$certificateid = $this->general_model->insert('push_certificates', array('appid' => $app->id, 'type' => 'ios', 'content' => $certificate));
								$environmentexists = $this->db->query("SELECT id FROM appenvironment WHERE appid = ? AND environmentname = ? LIMIT 1", array($app->id, $this->input->post('environment')));
								if($environmentexists->num_rows() == 0) {
									$this->general_model->insert('appenvironment', array('appid' => $app->id, 'environmentname' => $this->input->post('environment'), 'push_certificateid' => $certificateid));
								} else {
									$this->general_model->update('appenvironment',  $environmentexists->row()->id, array('push_certificateid' => $certificateid));
								}

								$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
								$democertificate = $_FILES['certificate']['tmp_name'];
								$post = array('bundle' => $bundle, 'environmentName' => $this->input->post('environment'), 'platform' => 'ios', 'push_certificate'=>'@'.$democertificate);
								curl_setopt($ch, CURLOPT_POST, true);                                                                     
								curl_setopt($ch, CURLOPT_POSTFIELDS, $post);  
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$result = curl_exec($ch);
								curl_close($ch);
							}
						}						
					}
					$data = array(
							"name" 		=> set_value('name'),
							"info"		=> set_value('info'),
							"subdomain" => set_value('urlname'),
							'cname' 	=> set_value('cname'),
							'show_external_ids_in_cms'	=> $externalids,
							'launcherview' => $this->input->post('launcherview')
						);
	                //remove all languages of app
	                foreach($languages as $lang) {
	                    $this->language_model->removeLanguageFromApp( $app->id, $lang->key);
	                }
	                //add selected languages to app
	                $sellanguages = $this->input->post('language');
	                if(!in_array($app->defaultlanguage, $sellanguages)) {
	                	$sellanguages[] = $app->defaultlanguage;
	                }

	                foreach($sellanguages as $lang) {
	                    $langdata = array(
	                        'appid'     =>  $app->id,
	                        'language'  => $lang
	                    );
	                    $this->general_model->insert('applanguage', $langdata);
	                }
					if($this->general_model->update('app', $app->id, $data)){
						$this->session->set_flashdata('event_feedback', 'The application has successfully been updated');
						redirect('apps/view/'.$app->id);
					} else {
						$error = "Oops, something went wrong. Please try again.";
					}
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_app_edit', array('flavors' => $flavors, 'error' => $error, 'languages' => $languages, 'app' => $app, 'applanguages' => $applangkeys, 'scriptphp' => $scriptphp, 'scriptjs' => $scriptjs, 'popupscript' => $popupscript), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array("Edit application" => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function delete($id) {
		/*if($this->general_model->remove('app', $id)){
			$this->session->set_flashdata('event_feedback', 'The app has successfully been deleted');
			redirect('apps/');
		} else {
			$this->session->set_flashdata('event_error', 'Oops, something went wrong. Please try again.');
			redirect('apps/');
		}*/
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.manage', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to delete Applications', 'error');
			redirect('apps/');
		}

		if($this->general_model->update('app', $id, array('isdeleted' => 1))){
			$this->session->set_flashdata('event_feedback', 'The app has successfully been deleted');
			redirect('apps/');
		} else {
			$this->session->set_flashdata('event_error', 'Oops, something went wrong. Please try again.');
			redirect('apps/');
		}

	}

	/**
	 * Display App's modules
	 *
	 * @param int $id Application identifier
	 */
	public function view($id)
	{
		$this->load->model('module_mdl');
		$this->session->set_userdata('appid', $id);
		$app = $this->app_model->get($id);
		if($app == FALSE) {
			$this->session->set_flashdata('tc_error', 'App not found!');
			redirect('apps');
		}

		_actionAllowed($app, 'app');

		if($app->familyid == 1) {
			redirect('events');
		} elseif($app->familyid == 2 && $app->apptypeid != 1 && $app->apptypeid != 2) {
			redirect('venues');
		}

		//content app
		if($app->apptypeid == 9) {
			$this->load->model('contentmodule_model');
			$flavor = $this->app_model->getFlavorOfId($app->apptypeid);
			$contentmodules = $this->contentmodule_model->getOfApp($app->id);
			$this->iframeurl = 'contentmodule/app/'.$id;
			$cdata['content'] 		= $this->load->view('c_app_view_content', array('app' => $app, 'flavor' => $flavor, 'contentmodules' => $contentmodules), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['crumb']			= array();
			$this->load->view('master', $cdata);
		} else {
			if($app->subdomain != null && $app->subdomain != "") {
				if($app->familyid == 1) {
					if($this->app_model->getEventsByApp($app->id)) {
						$this->iframeurl = 'events/app/'.$id;
					}
				} elseif($app->familyid == 2) {
					if($this->app_model->getVenuesByApp($app->id)) {
						$this->iframeurl = 'venues/app/'.$id;
					}
				} elseif($app->familyid == 3) {
					$this->iframeurl = 'city/index/'.$app->id;
				}
			}

	        $error = "";

	        $flavor = $this->app_model->getFlavorOfId($app->apptypeid);

	        $languages = $this->language_model->getAllLanguages();


			#
			# Load Tags
			#
			$this->load->model('tag_model');


			$apptags = $this->db->query("SELECT * FROM tag WHERE appid = $id AND (venueid <> 0 OR eventid <> 0 OR citycontentid <> 0) GROUP BY tag")->result();
			if($apptags == false) {
				$apptags = array();
			}

			$apptagsEvents = $this->db->query("SELECT * FROM tag WHERE appid = $id AND (venueid = 0 AND eventid <> 0) GROUP BY tag")->result();
			if($apptagsEvents == false) {
				$apptagsEvents = array();
			}

			$apptagsVenues = $this->db->query("SELECT * FROM tag WHERE appid = $id AND (venueid <> 0 AND eventid = 0) GROUP BY tag")->result();
			if($apptagsVenues == false) {
				$apptagsVenues = array();
			}

			$apptagsCitycontent = $this->db->query("SELECT * FROM tag WHERE appid = $id AND (citycontentid <> 0 AND venueid = 0 AND eventid = 0) GROUP BY tag")->result();
			if($apptagsCitycontent == false) {
				$apptagsCitycontent = array();
			}

			$venues = $this->venue_model->getAppVenue($id);
			$events = $this->event_model->allFromApp($id);

			if($app->apptypeid == 1 && $venues != false) {
				$this->iframeurl = 'app/expo/'.$app->id.'/venues';
			}



			if($venues!=false && $venues != null) {
				$this->tag_model->appendTags($venues, 'venue', $id);
			}

			if($events!=false && $events != null) {
				$this->tag_model->appendTags($events, 'event', $id);
			}

			$citycontent = $this->citycontent_model->allFromApp($id);
			if($citycontent!=false && $citycontent != null) {
				$this->tag_model->appendTags($citycontent, 'citycontent', $id);
			}

			$launchers = $this->module_mdl->getAllLaunchersOfApp($id);

			foreach($launchers as $l) {
				if($l->moduletypeid == 1 || $l->moduletypeid == 21 || $l->moduletypeid == 30) {
					$l->package = 'Basic';
				} elseif($l->moduletypeid == 31) {
					$l->package = 'Plus';
				} else {
					$l->package = 'Pro';
				}
			}

			$artistsActive = $this->module_mdl->artistsActive($id);
			$socialActive = $this->module_mdl->socialActive($id);

			$subflavors = $this->module_mdl->getSubflavorsOfFlavor($app->apptypeid);

			$this->load->model('formtemplate_model');
			$templateforms = $this->formtemplate_model->getFormTemplateIdByAppId($app->apptypeid, $app->id, 0, 'city');

			//$apptagz = array_unique($apptags);
			if($id == 55) {
				#
				# App exception : VISIT LIER
				#
				$cdata['content'] 		= $this->load->view('lier/c_app_lier', array('flavor' => $flavor, 'error' => $error, 'languages' => $languages, 'app' => $app, 'venues' => $venues, 'events' => $events, 'citycontent' => $citycontent, 'apptags' => $apptags, 'apptagsEvents' => $apptagsEvents, 'apptagsVenues' => $apptagsVenues, 'apptagsCitycontent' => $apptagsCitycontent, 'launchers' => $launchers, 'artistsActive' => $artistsActive, 'socialActive' => $socialActive), TRUE);
				$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
				$cdata['crumb']			= array();
				$this->load->view('master', $cdata);
			} else {
				$cdata['content'] 		= $this->load->view('c_app', array('flavor' => $flavor, 'error' => $error, 'languages' => $languages, 'app' => $app, 'venues' => $venues, 'events' => $events, 'citycontent' => $citycontent, 'apptags' => $apptags, 'apptagsEvents' => $apptagsEvents, 'apptagsVenues' => $apptagsVenues, 'apptagsCitycontent' => $apptagsCitycontent, 'launchers' => $launchers, 'artistsActive' => $artistsActive, 'socialActive' => $socialActive, 'subflavors' => $subflavors, 'templateform' =>$templateforms), TRUE);
				$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
				$cdata['crumb']			= array();
				$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app);

				$html = $this->load->view('master', $cdata, true);
				echo $html;
			}
		}
	}

	function module($trigger= '', $appid, $moduleid, $type = '', $typeid = '') {
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');


		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.manage', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to manage Applications', 'error');
			redirect('apps/view/'.$appid);
		}

		$this->load->model('module_mdl');
		if($moduleid == 49) {
			$this->module_mdl->checkUserModuleSettings($app->id);
		}
		switch($trigger){
			case 'activate':
				// Add module to event
				$this->module_mdl->addModuleToApp($appid, $moduleid);
				break;
			case 'deactivate':
				// TODO Remove module from event
				$this->module_mdl->removeModuleFromApp($appid, $moduleid);
				break;
			default:
				break;
		}
		if($type != '' && $typeid != '') {
			redirect($type.'/view/'.$typeid);
		} else {
			redirect('apps/view/'.$appid);
		}
	}

	function appearance($id) {
		$error = '';
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.manage', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to manage Applications', 'error');
			redirect('apps/view/'.$app->id);
		}

		$languages = $this->language_model->getAllLanguages();
		$this->load->library('form_validation');
		$contentflavor = false;
		if($app->apptypeid == 9 && isset($_GET['appearance'])) {
			$contentflavor = true;
		}
		$controls = $this->appearance_model->getControlsForApp($app, $contentflavor);
		$config = array();
		$this->load->library('image_lib', $config);

		if($app->apptypeid == 9) {
			if($css = file_get_contents($this->config->item('imagespath') . "upload/contentcss/".$app->id.'/css'.$app->id.'.css')) {

			} elseif($css = file_get_contents('/var/www/CLIENTS/defaultcss/contentflavor.css')) {

			} else {
				$css = '';
			}

			if($tabletcss = file_get_contents($this->config->item('imagespath') . "upload/contentcss/".$app->id.'/tabletcss'.$app->id.'.css')) {

			} elseif($tabletcss = file_get_contents('/var/www/CLIENTS/defaultcss/contentflavortablet.css')) {

			} else {
				$tabletcss = '';
			}
			// $css = file_get_contents('http://clients.tapcrowd.com/defaultcss/contentflavor.css');
		}

		if($this->input->post('postback') == "postback") {
			//remove old data
			$this->appearance_model->deleteAppearanceFromAppId($id);

			if(!is_dir($this->config->item('imagespath') . "upload/appimages/".$id)){
				mkdir($this->config->item('imagespath') . "upload/appimages/".$id, 0775, true);
			}

			foreach($controls as $control) {
				//colors
				if($control->typeid == 3) {
					$data = array(
						'appid'		=> $id,
						'controlid'	=> $control->controlid,
						'value'		=> $this->input->post($control->controlid),
					);

					//insert new data
					$this->general_model->insert('appearance', $data);
				}

				//images
				if($control->typeid == 2) {
					$image = $control->value;
					$this->form_validation->set_rules($control->controlid.'_image', $control->controlid.'_image', 'trim');
					//if($this->input->post($control->controlid.'_image') != '') {
						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/appimages/'.$id;
						if($control->controlid == 4 || $control->controlid == 5 || $control->controlid == 55) {
							$configexlogo['allowed_types'] = 'png';
						} else {
							$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						}

						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$configexlogo['raw_name']  = $control->image_name . '@2x';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload($control->controlid.'_image')) {
							$error = $this->upload->display_errors();
							if($image != null) {
								$data = array(
									"appid"		=> $id,
									"controlid"	=> $control->controlid,
									"value"		=> $image
								);

								$this->general_model->insert('appearance', $data);
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/appimages/'. $id . '/' . $returndata['file_name'];

							if($image != null && $control->controlid != 5) {
								$data = array(
									"appid"		=> $id,
									"controlid"	=> $control->controlid,
									"value"		=> $image
								);

								$this->general_model->insert('appearance', $data);
							}

							//resize and rename
							if($image!= null && $image != '') {
								if($control->controlid != 5 && $control->controlid != 9) {
									$newname = 'upload/appimages/'.$id.'/'. $control->image_name.'@2x.png';
									$config['image_library'] = 'gd2';
									$config['source_image'] = $this->config->item('imagespath') . $image;
									$config['width'] = $control->image_width;
									$config['height'] = $control->image_height;
									$config['maintain_ratio'] = false;
									$config['new_image'] = $this->config->item('imagespath') . $newname;

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										echo $this->image_lib->display_errors();
									}

									$newname = 'upload/appimages/'.$id.'/'. $control->image_name.'.png';
									$config['image_library'] = 'gd2';
									$config['source_image'] = $this->config->item('imagespath') . $image;
									$config['width'] = $control->image_width / 2;
									$config['height'] = $control->image_height / 2;
									$config['maintain_ratio'] = false;
									$config['new_image'] = $this->config->item('imagespath') . $newname;

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										echo $this->image_lib->display_errors();
									}
								} elseif($control->controlid == 5 ) {
									//icon specific
									$newname = 'upload/appimages/'.$id.'/'. $control->image_name.'1024.png';
									$config['image_library'] = 'gd2';
									$config['source_image'] = $this->config->item('imagespath') . $image;
									$config['width'] = $control->image_width;
									$config['height'] = $control->image_height;
									$config['maintain_ratio'] = false;
									$config['new_image'] = $this->config->item('imagespath') . $newname;
									if(file_exists($this->config->item('imagespath') . $newname)) {
										unlink($this->config->item('imagespath') . $newname);
									}

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										echo $this->image_lib->display_errors();
									}
									$appicon = $newname;
									$data = array(
										"appid"		=> $id,
										"controlid"	=> $control->controlid,
										"value"		=> $appicon
									);

									$this->general_model->insert('appearance', $data);
									$newname = 'upload/appimages/'.$id.'/'. $control->image_name.'512.png';
									$config['image_library'] = 'gd2';
									$config['source_image'] = $this->config->item('imagespath') . $image;
									$config['width'] = 512;
									$config['height'] = 512;
									$config['maintain_ratio'] = false;
									$config['new_image'] = $this->config->item('imagespath') . $newname;

									if(file_exists($this->config->item('imagespath') . $newname)) {
										unlink($this->config->item('imagespath') . $newname);
									}

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										echo $this->image_lib->display_errors();
									}
									$newname = 'upload/appimages/'.$id.'/'. $control->image_name.'@2x.png';
									$config['image_library'] = 'gd2';
									$config['source_image'] = $this->config->item('imagespath') . $image;
									$config['width'] = 114;
									$config['height'] = 114;
									$config['maintain_ratio'] = false;
									$config['new_image'] = $this->config->item('imagespath') . $newname;

									if(file_exists($this->config->item('imagespath') . $newname)) {
										unlink($this->config->item('imagespath') . $newname);
									}

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										echo $this->image_lib->display_errors();
									}
									$newname = 'upload/appimages/'.$id.'/'. $control->image_name.'.png';
									$config['image_library'] = 'gd2';
									$config['source_image'] = $this->config->item('imagespath') . $image;
									$config['width'] = 57;
									$config['height'] = 57;
									$config['maintain_ratio'] = false;
									$config['new_image'] = $this->config->item('imagespath') . $newname;

									if(file_exists($this->config->item('imagespath') . $newname)) {
										unlink($this->config->item('imagespath') . $newname);
									}

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										echo $this->image_lib->display_errors();
									}
								}
							}
						}
					//}
				}
			}

			if(isset($appicon) && $appicon != false) {
				$this->general_model->update('app', $id, array('app_icon' => $appicon));
			}

			// make sure app rebuilds
			$this->load->model('buildqueue_model');
			$this->buildqueue_model->rebuild($id, 'android');
			$this->buildqueue_model->rebuild($id, 'ios');

			$this->general_model->update('app', $id, array('timestamp' => time()));
			$this->session->set_flashdata('event_feedback', 'successfully changed your appearance settings.');
			redirect('apps/appearance/'.$id);
		}

        $headerphone  = $app->header_html_phones;
		$headertablet  = $app->header_html_tablets;
        $footerphone  = $app->footer_html_phones;
		$footertablet  = $app->footer_html_tablets;

        $cdata['content'] 		= $this->load->view('c_app_appearance', array('error' => $error, 'languages' => $languages, 'app' => $app,'controls' => $controls, 'css' => $css, 'tabletcss' => $tabletcss, 'headerphone'=>$headerphone, 'headertablet'=>$headertablet, 'footerphone'=>$footerphone, 'footertablet'=>$footertablet), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array($app->name => 'apps/view/'.$app->id, "Appearance" => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function theme($id) {
		$error = '';
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.manage', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to manage Applications', 'error');
			redirect('apps/view/'.$app->id);
		}

		if($app->familyid == 4) {
			redirect('apps/appearance/'.$app->id);
		}

		$this->load->model('theme_model');
		$themes = $this->theme_model->getAllLive();

		if(isset($_GET['newapp'])) {
			$cdata['content'] 		= $this->load->view('c_app_theme', array('error' => $error, 'app' => $app, 'themes' => $themes), TRUE);
			$this->load->view('master_wizard', $cdata);
		} else {
			$cdata['content'] 		= $this->load->view('c_app_theme', array('error' => $error, 'app' => $app, 'themes' => $themes), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['crumb']			= array($app->name => 'apps/view/'.$app->id,"Theme" => $this->uri->uri_string());
			$this->load->view('master', $cdata);
		}
	}

	function applytheme($themeid, $appid) {
		$error = '';
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');

		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.manage', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to delete Applications', 'error');
			redirect('apps/view/'.$appid);
		}

		$this->load->model('theme_model');
		$themes = $this->theme_model->apply($themeid, $appid);

		redirect('apps/view/'.$appid);
	}

	function removeimage($appid, $appearanceid) {
		$appearance = $this->appearance_model->getAppearanceById($appearanceid);
		if($appid == $appearance->appid) {
			$app = $this->app_model->get($appearance->appid);
			_actionAllowed($app, 'app');
			if(file_exists($this->config->item('imagespath') . $appearance->value)) {
				unlink($this->config->item('imagespath') . $appearance->value);
				if(file_exists($this->config->item('imagespath') . 'cache/'. $appearance->value)) {
					unlink($this->config->item('imagespath') . 'cache/'. $appearance->value);
				}
			}
			$this->general_model->remove('appearance', $appearanceid);
		}

		redirect('apps/appearance/'.$appid);
	}

	function info($appid) {
		$error = '';
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');
		$languages = $this->language_model->getAllLanguages();
		$this->load->library('form_validation');
		$applanguages = $this->language_model->getLanguagesOfApp($app->id);

		if($app->familyid == 3) {
			$this->iframeurl = "app/info/".$app->id;
		}

		$launcher = $this->module_mdl->getLauncher(21, 'app', $app->id);
		$metadata = $this->metadata_model->getMetadata($launcher, 'app', $app->id, $app);

		if($this->input->post('postback') == "postback") {
            foreach($applanguages as $language) {
                $this->form_validation->set_rules('info_'.$language->key, 'info_'.$language->key, 'trim');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}
            }
			$this->form_validation->set_rules('address', 'address', 'trim');
			$this->form_validation->set_rules('lat', 'lat', 'trim');
			$this->form_validation->set_rules('lon', 'lon', 'trim');
			$this->form_validation->set_rules('tel', 'tel', 'trim');
			$this->form_validation->set_rules('email', 'email', 'trim');
			$this->form_validation->set_rules('website', 'website', 'trim');

			$website = checkhttp($this->input->post('website'));

			if($this->form_validation->run() != FALSE){
				$data = array(
					'info'			=> set_value('info_'.  $app->defaultlanguage),
					'address'		=> set_value('address'),
					'lat'			=> set_value('lat'),
					'lon'			=> set_value('lon'),
					'telephone'		=> set_value('tel'),
					'email'			=> set_value('email'),
					'website'		=> $website
				);
				if($this->general_model->update('app', $appid, $data)) {
                    foreach($applanguages as $language) {
						$infoSucces = $this->language_model->updateTranslation('app', $appid, 'info', $language->key, $this->input->post('info_'.$language->key));
						if(!$infoSucces) {
							$this->language_model->addTranslation('app', $appid, 'info', $language->key, $this->input->post('info_'.$language->key));
						}

						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								$postfield = _getPostedField($m, $_POST, $_FILES, $language);
								if($postfield !== false) {
									if(_validateInputField($m, $postfield)) {
										_saveInputField($m, $postfield, 'app', $app->id, $app, $language);
									}
								}
							}
						}
                    }

					redirect('apps/view/'.$appid);
				}
			}

		}

		$cdata['content'] 		= $this->load->view('c_app_info', array('error' => $error, 'languages' => $languages, 'app' => $app, 'applanguages'=>$applanguages, 'metadata' => $metadata), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array("Info" => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function wizard() {
		$this->load->model('creator_model');
		$flavors = $this->creator_model->listFlavors(1);
		$languages = $this->language_model->getAllLanguages();

		if(isset($_POST['creatorform'])) {
			switch($_POST['selectedFlavorid']) {
				case '3':
					$appname = $_POST['selectedAppName'];
					$eventname = $_POST['selectedEventName'];
					if(trim($appname) == '') {
						$appname = $eventname;
					}
					$startdate = substr($_POST['selectedStartdate'], 6,4).'-'.substr($_POST['selectedStartdate'], 3,2).'-'.substr($_POST['selectedStartdate'], 0,2);
					$enddate = substr($_POST['selectedEnddate'], 6,4).'-'.substr($_POST['selectedEnddate'], 3,2).'-'.substr($_POST['selectedEnddate'], 0,2);

					$appdata = array(
							'apptypeid'			=> $_POST['selectedFlavorid'],
							'organizerid'		=> _currentUser()->organizerId,
							'name'				=> $appname,
							'token'				=> '123456',
							'certificate'		=> '',
							'subdomain' 		=> $_POST['selectedMobileurl'],
							'advancedFilter'	=> 0,
		                    'defaultlanguage'   => $_POST['selectedDefaultLanguage']
						);

					$appid = $this->general_model->insert('app', $appdata);
					$latlon = (object) array();
					$latlon->lat = $_POST['selectedLat'];
					$latlon->lng = $_POST['selectedLon'];
					$venueid = $this->venue_model->add('', $_POST['selectedAddress'], $latlon);
					$eventid = $this->event_model->add($eventname, _currentUser()->organizerId, $venueid, 3, $startdate, $enddate, 1, '', '', '', '', '', '',0);
					$this->app_model->addEventToApp($appid, $eventid);
					$this->app_model->addEventToApp(2, $eventid);
					break;
				case '10':
					$appname = $_POST['selectedAppName'];
					$eventname = $_POST['selectedEventName'];
					if($appname == '') {
						$appname = $eventname;
					}
					$startdate = substr($_POST['selectedStartdate'], 6,4).'-'.substr($_POST['selectedStartdate'], 3,2).'-'.substr($_POST['selectedStartdate'], 0,2);
					$enddate = substr($_POST['selectedEnddate'], 6,4).'-'.substr($_POST['selectedEnddate'], 3,2).'-'.substr($_POST['selectedEnddate'], 0,2);
					$appdata = array(
							'apptypeid'			=> $_POST['selectedFlavorid'],
							'organizerid'		=> _currentUser()->organizerId,
							'name'				=> $appname,
							'token'				=> '123456',
							'certificate'		=> '',
							'subdomain' 		=> $_POST['selectedMobileurl'],
							'advancedFilter'	=> 0,
		                    'defaultlanguage'   => $_POST['selectedDefaultLanguage']
						);

					$appid = $this->general_model->insert('app', $appdata);
					$latlon = (object) array();
					$latlon->lat = $_POST['selectedLat'];
					$latlon->lng = $_POST['selectedLon'];
					$venueid = $this->venue_model->add('', $_POST['selectedAddress'], $latlon);
					$eventid = $this->event_model->add($eventname, _currentUser()->organizerId, $venueid, 3, $startdate, $enddate, 1, '', '', '', '', '', '',0);
					$this->app_model->addEventToApp($appid, $eventid);
					$this->app_model->addEventToApp(2, $eventid);
					break;
				case '7':
					$appname = $_POST['selectedAppName'];
					$venuename = $_POST['selectedVenueName'];
					if($appname == '') {
						$appname = $venuename;
					}
					$appdata = array(
							'apptypeid'			=> $_POST['selectedFlavorid'],
							'organizerid'		=> _currentUser()->organizerId,
							'name'				=> $appname,
							'token'				=> '123456',
							'certificate'		=> '',
							'subdomain' 		=> $_POST['selectedMobileurl'],
							'advancedFilter'	=> 0,
		                    'defaultlanguage'   => $_POST['selectedDefaultLanguage']
						);

					$appid = $this->general_model->insert('app', $appdata);
					$venuedata = array(
							'name'			=> $venuename,
							'address'		=> $_POST['selectedAddress'],
							'lat'			=> $_POST['selectedLat'],
							'lon'			=> $_POST['selectedLon'],
							'timestamp'		=> time()
						);

					$venueid = $this->general_model->insert('venue', $venuedata);
					$this->general_model->insert('appvenue', array('venueid' => $venueid, "appid" => $appid));
					$this->general_model->insert('appvenue', array('venueid' => $venueid, "appid" => 2));
					//modules for shop app
					if($app->apptypeid == 7) {
						$this->general_model->insert("module", array("moduletype" => '15', "venue" => $newid));
						$this->general_model->insert("module", array("moduletype" => '41', "venue" => $newid));
						$this->venue_model->insertShopLaunchers($app->id, $newid);
					}
					break;
				case '8':
					$appname = $_POST['selectedAppName'];
					$venuename = $_POST['selectedVenueName'];
					if($appname == '') {
						$appname = $venuename;
					}
					$appdata = array(
							'apptypeid'			=> $_POST['selectedFlavorid'],
							'organizerid'		=> _currentUser()->organizerId,
							'name'				=> $appname,
							'token'				=> '123456',
							'certificate'		=> '',
							'subdomain' 		=> $_POST['selectedMobileurl'],
							'advancedFilter'	=> 0,
		                    'defaultlanguage'   => $_POST['selectedDefaultLanguage']
						);

					$appid = $this->general_model->insert('app', $appdata);
					$venuedata = array(
							'name'			=> $venuename,
							'address'		=> $_POST['selectedAddress'],
							'lat'			=> $_POST['selectedLat'],
							'lon'			=> $_POST['selectedLon'],
							'timestamp'		=> time()
						);

					$venueid = $this->general_model->insert('venue', $venuedata);
					$this->general_model->insert('appvenue', array('venueid' => $venueid, "appid" => $appid));
					$this->general_model->insert('appvenue', array('venueid' => $venueid, "appid" => 2));
					//modules for resto app
					if($app->apptypeid == 8) {
						$this->general_model->insert("module", array("moduletype" => '15', "venue" => $newid));
						$this->general_model->insert("module", array("moduletype" => '41', "venue" => $newid));
						$this->venue_model->insertRestoLaunchers($app->id, $newid);
					}
					break;
			}

			if($_POST['selectedLanguages']!= "") {
			    $sellanguages = explode(',', $_POST['selectedLanguages']);
			    foreach($sellanguages as $lang) {
			        $langdata = array(
			            'appid'     => $appid,
			            'language'  => $lang
			        );
			        $this->general_model->insert('applanguage', $langdata);
			    }
			}
			$subflavordata = array(
				'appid'		=> $appid,
				'subflavorid'	=> 3
			);
			$this->general_model->insert('appsubflavor', $subflavordata);
			echo $appid;
			exit;
		}

		$this->load->view('c_wizard', array('flavors' => $flavors, 'languages' => $languages));
	}

	function blank($id = 0) {
		$this->load->library('form_validation');
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');
		$error = "";

		if($id == 0) {
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				// $this->form_validation->set_rules('info', 'info', 'trim');
				$this->form_validation->set_rules('blankurl', 'Url', 'trim|prep_url|valid_url|xss_clean');

				if($this->form_validation->run() == FALSE){
					$error .= validation_errors();
				} else {
					if($error == '') {
						$data = array(
							'apptypeid'			=> 12,
							'organizerid'		=> _currentUser()->organizerId,
							'accountId'			=> _currentUser()->accountId,
							'name'				=> set_value('name'),
							// 'info'				=> set_value('info'),
							'token'				=> '123456',
							'certificate'		=> '',
							'advancedFilter'	=> 0,
	                        'defaultlanguage'   => 'en',
	                        'blankurl'			=> set_value('blankurl'),
	                        'bundle'			=> 'com.tapcrowd.'.rand(0,50000)
						);

						if($id = $this->general_model->insert('app', $data)){
							$bundles = $this->app_model->getBundles();

							$bundle = $this->input->post('name').$id;

							$some_special_chars = array("'", '"', '&', '%', '$', '.', ',', '(', ')', '!', ' ',';', ':');
							$replacement_chars  = array("", "", '', '', '', '', '', '', '', '', '', '', '');

							$bundle =  preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $bundle);
							$bundle    = 'com.tapcrowd.'.str_replace($some_special_chars, $replacement_chars, $bundle);
							if(isset($bundles[$bundle])) {
								$bundle = $bundle.substr(md5($bundle.time()), 0, 5);
							}
							$this->general_model->update('app', $id, array('bundle' => $bundle));

							// register app in taptarget
							$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerApplication');
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.strtolower($bundle).'&title='.urlencode($appdata['name']));
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							curl_close($ch);

							$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/setApplicationRole');
							curl_setopt($ch, CURLOPT_POST, true);
							$role = 'user';
							if(_isAdmin()) {
								$role = 'admin';
							}
							curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&accountId='._currentUser()->accountId.'&role='.$role);  
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							curl_close($ch);

							$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
							$democertificate = '/var/www/CLIENTS/push/ios/demoapp-prod.pem';
							$post = array('bundle' => $bundle, 'environmentName' => 'tapcrowd_demo', 'platform' => 'ios', 'push_certificate'=>'@'.$democertificate);
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $post);  
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							curl_close($ch);

							$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=tapcrowd_demo&platform=android');  
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							curl_close($ch);

							$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=tapcrowd_demo&platform=web');  
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							curl_close($ch);

							$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=live&platform=android');  
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							curl_close($ch);

							$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=live&platform=ios');  
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							curl_close($ch);

							$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
							curl_setopt($ch, CURLOPT_POST, true);                                                                     
							curl_setopt($ch, CURLOPT_POSTFIELDS, 'bundle='.$bundle.'&environmentName=live&platform=web');  
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							curl_close($ch);

							$this->general_model->insert('appenvironment', array(
									'appid' => $res,
									'environmentname' => 'tapcrowd_demo',
									'push_certificateid' => 1
								));

							$this->general_model->insert('appenvironment', array(
									'appid' => $res,
									'environmentname' => 'live'
								));
						
							$this->session->set_flashdata('event_feedback', 'The application has successfully been added');
							redirect('apps/blank/'.$id);
						} else {
							$error = "Oops, something went wrong. Please try again.";
						}
					}
				}
			}
		} else {
			if($error == '') {
				$app = $this->app_model->get($id);
				_actionAllowed($app, 'app');

				$this->session->set_userdata('appid', $id);

				if($this->input->post('postback') == "postback") {
					$this->form_validation->set_rules('name', 'name', 'trim|required');
					// $this->form_validation->set_rules('info', 'info', 'trim');
					$this->form_validation->set_rules('blankurl', 'Url', 'trim|prep_url|valid_url|xss_clean');
					$this->form_validation->set_rules('certificate', 'certificate', 'trim');
					$this->form_validation->set_rules('environment', 'environment', 'trim');

					if($this->form_validation->run() == FALSE){
						$error .= validation_errors();
					} else {
						if($error == '') {
							$data = array(
									"name" 		=> set_value('name'),
									// "info"		=> set_value('info'),
									'blankurl'	=> set_value('blankurl')
								);

							$certificate = '';
							if($_FILES['certificate']['size'] > 0) {
								if(file_exists($_FILES['certificate']['tmp_name'])) {
									if(($f = fopen($_FILES['certificate']['tmp_name'], "r")) !== FALSE) {
										$certificate = fread($f, filesize($_FILES['certificate']['tmp_name']));
										fclose($f); 

										$this->db->query("DELETE FROM push_certificates WHERE appid = $app->id AND type = 'ios'");
										$certificateid = $this->general_model->insert('push_certificates', array('appid' => $app->id, 'type' => 'ios', 'content' => $certificate));
										$environmentexists = $this->db->query("SELECT id FROM appenvironment WHERE appid = ? AND environmentname = ? LIMIT 1", array($app->id, $this->input->post('environment')));
										if($environmentexists->num_rows() == 0) {
											$this->general_model->insert('appenvironment', array('appid' => $app->id, 'environmentname' => $this->input->post('environment'), 'push_certificateid' => $certificateid));
										} else {
											$this->general_model->update('appenvironment',  $environmentexists->row()->id, array('push_certificateid' => $certificateid));
										}

										$ch = curl_init('http://taptarget.tapcrowd.com/0.2/analyticsservice/registerAppEnvironment');
										$democertificate = $_FILES['certificate']['tmp_name'];
										$post = array('bundle' => $bundle, 'environmentName' => $this->input->post('environment'), 'platform' => 'ios', 'push_certificate'=>'@'.$democertificate);
										curl_setopt($ch, CURLOPT_POST, true);                                                                     
										curl_setopt($ch, CURLOPT_POSTFIELDS, $post);  
										curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
										$result = curl_exec($ch);
										curl_close($ch);
									}
								}						
							}
							if($this->general_model->update('app', $app->id, $data)){
								$this->session->set_flashdata('event_feedback', 'The application has successfully been updated');
								redirect('apps/blank/'.$app->id);
							} else {
								$error = "Oops, something went wrong. Please try again.";
							}
						}
					}
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_app_blank', array('error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array("Edit application" => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function appstoresettings($id) {		
		$this->load->library('form_validation');
		
		if($id == FALSE || $id == 0) {
			$this->session->set_flashdata('tc_error', __('Invalid ApplicationID!'));
			redirect('apps');
		}
		
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$error = "";
		$postedtags = explode(";", $app->searchterms);
		
		if($this->input->post('postback') == 'postback') {
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('remarks', 'remarks', 'trim');
			$this->form_validation->set_rules('category[]', 'category', 'trim');
			
			$this->form_validation->set_rules('appicon', 'appicon', 'trim');
			$this->form_validation->set_rules('homescreen', 'homescreen', 'trim');
			
			// $this->form_validation->set_rules('chk_appstore', 'chk_appstore', 'trim');
			// $this->form_validation->set_rules('chk_androidm', 'chk_androidm', 'trim');
			
			// $this->form_validation->set_rules('chk_worldwide', 'chk_worldwide', 'trim');
			// $this->form_validation->set_rules('chk_europe', 'chk_europe', 'trim');
			// $this->form_validation->set_rules('chk_us', 'chk_us', 'trim');
			
			if($this->form_validation->run() == FALSE){
				$error = "Some fields are missing.";
			} else {
				$appicon = $app->app_icon;
				if($_FILES['appicon']['size'] > 0) {
					$configicon['upload_path'] = $this->config->item('imagespath') .'upload/appicons/';
					$configicon['allowed_types'] = 'png';
					$configicon['max_width']  = '1024';
					$configicon['max_height']  = '1024';
					$this->load->library('upload', $configicon);
					$this->upload->initialize($configicon);
					if ($this->upload->do_upload('appicon')) {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/appicons/' . $returndata['file_name'];

						$this->db->query("DELETE FROM appearance WHERE appid = $app->id AND controlid = 5");
						$this->general_model->insert('appearance', array('appid' => $app->id, 'controlid' => 5, 'value' => $image));

						$appicon = $image;
					} 
				}
				
				$homescreen = '';
				if($_FILES['homescreen']['size'] > 0) {
					$configicon['upload_path'] = $this->config->item('imagespath') .'upload/homescreens/';
					$configicon['allowed_types'] = 'png';
					$configicon['max_width']  = '640';
					$configicon['max_height']  = '960';
					$this->load->library('upload', $configicon);
					$this->upload->initialize($configicon);
					if ($this->upload->do_upload('homescreen')) {
						$returndata = $this->upload->data();
						$homescreen = 'upload/homescreens/' . $returndata['file_name'];

						$this->db->query("DELETE FROM appearance WHERE appid = $app->id AND controlid = 4");
						$this->general_model->insert('appearance', array('appid' => $app->id, 'controlid' => 4, 'value' => $homescreen));
					}
				}

				$splash5 = '';
				if($_FILES['splash5']['size'] > 0) {
					$configicon['upload_path'] = $this->config->item('imagespath') .'upload/homescreens/';
					$configicon['allowed_types'] = 'png';
					$this->load->library('upload', $configicon);
					$this->upload->initialize($configicon);
					if ($this->upload->do_upload('homescreen')) {
						$returndata = $this->upload->data();
						$splash5 = 'upload/homescreens/' . $returndata['file_name'];

						$this->db->query("DELETE FROM appearance WHERE appid = $app->id AND controlid = 55");
						$this->general_model->insert('appearance', array('appid' => $app->id, 'controlid' => 55, 'value' => $splash5));
					}
				}

				$categories = $this->input->post('category');
				$categories = implode(';', $categories);

				$postedtags = $this->input->post('mytagsulselect');
				if($this->input->post('mytagsulselect') != null) {
					$searchterms = implode(';',$this->input->post('mytagsulselect'));
				} else {
					$searchterms = '';
				}

				if(!is_dir($this->config->item('imagespath') . "upload/appscreens/".$app->id)){
					mkdir($this->config->item('imagespath') . "upload/appscreens/".$app->id, 0775,true);
				}
				$configscreenshots['upload_path'] = $this->config->item('imagespath') .'upload/appscreens/'.$app->id;
				$configscreenshots['allowed_types'] = 'JPG|jpg|jpeg|png';
				$this->load->library('upload', $configscreenshots);
				$this->upload->initialize($configscreenshots);
				$i = 1;
				while($i <= 5) {
					if($_FILES['iphone_'.$i]['size'] > 0) {
						if($this->upload->do_upload('iphone_'.$i)) {
							$returndata = $this->upload->data();
							$image = 'upload/appscreens/'.$app->id .'/' . $returndata['file_name'];
							$this->general_model->insert('appscreenshots', array(
									'appid' => $app->id,
									'type' => 'iphone',
									'value' => $image
								)); 
						} else {
							$error .= $this->upload->display_errors();
						}
					}
					if($_FILES['iphone5_'.$i]['size'] > 0) {
						if($this->upload->do_upload('iphone5_'.$i)) {
							$returndata = $this->upload->data();
							$image = 'upload/appscreens/'.$app->id .'/' . $returndata['file_name'];
							$this->general_model->insert('appscreenshots', array(
									'appid' => $app->id,
									'type' => 'iphone5',
									'value' => $image
								)); 
						}
					}
					if($_FILES['android_'.$i]['size'] > 0) {
						if($this->upload->do_upload('android_'.$i)) {
							$returndata = $this->upload->data();
							$image = 'upload/appscreens/'.$app->id .'/' . $returndata['file_name'];
							$this->general_model->insert('appscreenshots', array(
									'appid' => $app->id,
									'type' => 'android',
									'value' => $image
								)); 	
						}
					}

					$i++;
				}

				$this->db->where('id', $id);
				$this->db->update('app', array(
						'submit_description' 	=> set_value('remarks'),
						'category' 				=> $categories,
						'app_icon' 				=> $appicon,
						'homescreen' 			=> $homescreen,
						'searchterms'			=> $searchterms
					));
				$app = $this->app_model->get($id);

				// make sure app rebuilds
				$this->load->model('buildqueue_model');
				$this->buildqueue_model->rebuild($app->id, 'android');
				$this->buildqueue_model->rebuild($app->id, 'ios');
			}
		}

		$appscreens = $this->appearance_model->getScreens($app->id);
		foreach($appscreens as $screen) {
			if($screen->type == 'iphone') {
				$iphonescreenshots[] = $screen->value;
			} elseif($screen->type == 'iphone5') {
				$iphone5screenshots[] = $screen->value;
			} elseif($screen->type == 'android') {
				$androidscreenshots[] = $screen->value;
			}
		}
		$appearance = $this->appearance_model->getControlsForApp($app);
		
		$cdata['content'] 		= $this->load->view('c_app_appstoresettings', array('app' => $app, 'error' => $error, 'appearance' => $appearance, 'appscreens' => $appscreens, 'postedtags' => $postedtags, 'iphonescreenshots' => $iphonescreenshots, 'iphone5screenshots' => $iphone5screenshots, 'androidscreenshots' => $androidscreenshots), TRUE);
		$cdata['crumb']			= array($app->name => "apps/view/".$app->id);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}

	function done($id) {
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$cdata['content'] 		= $this->load->view('c_done', array('app' => $app), TRUE);
		$this->load->view('master_wizard', $cdata);
	}
}
