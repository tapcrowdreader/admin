<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Catalogbrands extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('catalogbrands_model');
	}
	
	//php 4 constructor
	function Catalogbrands() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('catalogbrands_model');
	}
	
	function index() {
		$this->event();
	}
	
	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');
		
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		
		$brands = $this->catalogbrands_model->getCatalogbrandsByEventID($id);
		$headers = array(__('Name') => 'name');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => $brands, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Catalog Brands") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}
	
	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		
		$brands = $this->catalogbrands_model->getCatalogbrandsByVenueID($id);
		$headers = array(__('Name') => 'name');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'data' => $brands, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Catalog Brands") => $this->uri->uri_string()));
		$this->load->view('master', $cdata);
	}
	
	function add($id, $type = "event") {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
			
			$venue = $this->venue_model->getbyid($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					// Save exhibitor
					$data = array( 
							"venueid"		=> $id,
							"name" 			=> set_value('name')
						);
					$res = $this->general_model->insert('catalogbrand', $data);
					
					if($res != FALSE){
						$this->session->set_flashdata('event_feedback', __('The brand has successfully been added'));
						redirect('catalogbrands/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
					
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_catalogbrand_add', array('venue' => $venue, 'error' => $error), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Catalog Brands") => "catalogbrands/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					// Save exhibitor
					$data = array( 
							"eventid"		=> $id,
							"name" 			=> set_value('name')
						);
					$res = $this->general_model->insert('catalogbrand', $data);

					if($res != FALSE){
						$this->session->set_flashdata('event_feedback', __('The brand has successfully been added'));
						_updateTimeStamp($event->id);
						redirect('catalogbrands/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

				}
			}

			$cdata['content'] 		= $this->load->view('c_catalogbrand_add', array('event' => $event, 'error' => $error), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Catalog Brands") => "catalogbrands/event/".$event->id, __("Add new") => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}
	
	function edit($id, $type = "event") {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
			
			// EDIT EXHIBITOR
			$brand = $this->catalogbrands_model->getCatalogbrandByID($id);
			if($brand == FALSE) redirect('venues');
			
			$venue = $this->venue_model->getbyid($brand->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array( 
							"venueid"		=> $venue->id,
							"name" 			=> set_value('name')
						);
					
					if($this->general_model->update('catalogbrand', $id, $data)){
						$this->session->set_flashdata('event_feedback', __('The brand has successfully been updated'));
						redirect('catalogbrands/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_catalogbrand_edit', array('venue' => $venue, 'brand' => $brand, 'error' => $error), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Brand") => "catalogbrands/venue/".$venue->id, __("Edit: ") . $brand->name => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			// EDIT EXHIBITOR
			$brand = $this->catalogbrands_model->getCatalogbrandByID($id);
			if($brand == FALSE) redirect('events');

			$event = $this->event_model->getbyid($brand->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array( 
							"eventid"		=> $event->id,
							"name" 			=> set_value('name')
						);

					if($this->general_model->update('catalogbrand', $id, $data)){
						$this->session->set_flashdata('event_feedback', __('The brand has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('catalogbrands/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_catalogbrand_edit', array('event' => $event, 'brand' => $brand, 'error' => $error), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Brand") => "catalogbrands/event/".$event->id, __("Edit: ") . $brand->name => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}
	
	function delete($id, $type = "event") {
		if($type == 'venue') {
			$brand = $this->catalogbrands_model->getCatalogBrandByID($id);
			$venue = $this->venue_model->getbyid($brand->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($this->general_model->remove('catalogbrand', $id)){
				$this->db->delete('catebrand', array('catalogbrandid' => $id));
				$this->session->set_flashdata('event_feedback', __('The brand has successfully been deleted'));
				redirect('catalogbrands/venue/'.$brand->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('catalogbrands/venue/'.$brand->venueid);
			}
		} else {
			$brand = $this->catalogbrands_model->getCatalogBrandByID($id);
			$event = $this->event_model->getbyid($brand->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			if($this->general_model->remove('catalogbrand', $id)){
				$this->db->delete('catebrand', array('catalogbrandid' => $id));
				$this->session->set_flashdata('event_feedback', __('The brand has successfully been deleted'));
				_updateTimeStamp($brand->eventid);
				redirect('catalogbrands/event/'.$brand->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('catalogbrands/event/'.$brand->eventid);
			}
		}
	}

}