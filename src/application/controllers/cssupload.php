<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Cssupload extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	function app($id) {
		$app = $this->app_model->get($id);
		$app = _actionAllowed($app, 'app','returnApp');
		$this->load->library('form_validation');

		if($this->input->post('postback') == "postback") {
			if(!is_dir($this->config->item('imagespath') . "upload/contentcss/".$app->id)){
				mkdir($this->config->item('imagespath') . "upload/contentcss/".$app->id, 0777, TRUE);
			}
			$css = strip_tags($this->input->post('txt_css'));
			file_put_contents($this->config->item('imagespath') . "upload/contentcss/".$app->id.'/css'.$app->id.'.css', $css);
			$cssfile = 'upload/contentcss/'. $app->id . '/' . 'css'.$app->id.'.css';

			$this->general_model->update('app', $app->id, array('contentmodule_css_phone' => $cssfile));	
		}
		redirect('apps/view/'.$id);
	}

	function tablet($id) {
		$app = $this->app_model->get($id);
		$app = _actionAllowed($app, 'app','returnApp');
		$this->load->library('form_validation');

		if($this->input->post('postback') == "postback") {
			if(!is_dir($this->config->item('imagespath') . "upload/contentcss/".$app->id)){
				mkdir($this->config->item('imagespath') . "upload/contentcss/".$app->id, 0777, TRUE);
			}
			$css = strip_tags($this->input->post('txt_tabletcss'));
			file_put_contents($this->config->item('imagespath') . "upload/contentcss/".$app->id.'/tabletcss'.$app->id.'.css', $css);
			$cssfile = 'upload/contentcss/'. $app->id . '/' . 'tabletcss'.$app->id.'.css';

			$this->general_model->update('app', $app->id, array('contentmodule_css_tablet' => $cssfile));	
		}
		redirect('apps/view/'.$id);
	}
        
	function phoneheader($id) {
		$app = $this->app_model->get($id);
		$app = _actionAllowed($app, 'app','returnApp');
		$this->load->library('form_validation');
                $txtValue = $this->input->post('header_html_phones');
                $this->general_model->update('app', $app->id, array('header_html_phones' => $txtValue));
		if($this->input->post('postback') == "postback") {
			$this->general_model->update('app', $app->id, array('header_html_phones' => $txtValue));	
		}
 		redirect('apps/view/'.$id);
	}
        
	function tabletheader($id) {
		$app = $this->app_model->get($id);
		$app = _actionAllowed($app, 'app','returnApp');
		$this->load->library('form_validation');
                $txtValue = $this->input->post('header_html_tablets');
                
		if($this->input->post('postback') == "postback") {
			$this->general_model->update('app', $app->id, array('header_html_tablets' => $txtValue));	
		}
		redirect('apps/view/'.$id);
	}
	function phonefooter($id) {
		$app = $this->app_model->get($id);
		$app = _actionAllowed($app, 'app','returnApp');
		$this->load->library('form_validation');
                $txtValue = $this->input->post('footer_html_phones');
                
		if($this->input->post('postback') == "postback") {
			$this->general_model->update('app', $app->id, array('footer_html_phones' => $txtValue));	
		}
		redirect('apps/view/'.$id);
	}
        
	function tabletfooter($id) {
		$app = $this->app_model->get($id);
		$app = _actionAllowed($app, 'app','returnApp');
		$this->load->library('form_validation');
                $txtValue = $this->input->post('footer_html_tablets');
                
		if($this->input->post('postback') == "postback") {
			$this->general_model->update('app', $app->id, array('footer_html_tablets' => $txtValue));	
		}
		redirect('apps/view/'.$id);
	}
        
        function updateapperance($id){
		$app = $this->app_model->get($id);
		$app = _actionAllowed($app, 'app','returnApp');
		$this->load->library('form_validation');
                
		if($this->input->post('postback') == "postback") {
			if(!is_dir($this->config->item('imagespath') . "upload/contentcss/".$app->id)){
				mkdir($this->config->item('imagespath') . "upload/contentcss/".$app->id, 0777, TRUE);
			}
                        
			$phonecssfile = strip_tags($this->input->post('txt_css'));
			file_put_contents($this->config->item('imagespath') . "upload/contentcss/".$app->id.'/css'.$app->id.'.css', $phonecssfile);
			$phonecssfile = 'upload/contentcss/'. $app->id . '/' . 'css'.$app->id.'.css';
                        
			$tabletcssfile = strip_tags($this->input->post('txt_tabletcss'));
			file_put_contents($this->config->item('imagespath') . "upload/contentcss/".$app->id.'/tabletcss'.$app->id.'.css', $tabletcssfile);
			$tabletcssfile = 'upload/contentcss/'. $app->id . '/' . 'tabletcss'.$app->id.'.css';

                        $header_html_phones = $this->input->post('header_html_phones');                
                        $header_html_tablets = $this->input->post('header_html_tablets');                
                        $footer_html_phones = $this->input->post('footer_html_phones');                
                        $footer_html_tablets = $this->input->post('footer_html_tablets');

			$this->general_model->update('app', $app->id, array('contentmodule_css_phone' => $phonecssfile,'contentmodule_css_tablet' => $tabletcssfile,'header_html_phones' => $header_html_phones,'header_html_tablets' => $header_html_tablets,'footer_html_phones' => $footer_html_phones,'footer_html_tablets' => $footer_html_tablets));	
		}
		redirect('apps/view/'.$id);
            
        }        
}
