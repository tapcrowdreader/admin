<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Cityinfo extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}
	
	//php 4 constructor
	function Cityinfo() {
		parent::__construct();
		if(_authed()) { }
	}
	
	function index() {
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');
		
		$error = '';
		
		$this->load->library('form_validation');
		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('txt_cityinfo', 'txt_cityinfo', 'trim');
			
			if($this->form_validation->run() == FALSE){
				$error = __("Some fields are missing.");
			} else {
				// Save exhibitor
				$res = $this->general_model->update('app', _currentApp()->id, array("info" => set_value('txt_cityinfo')));
				
				if($res != FALSE){
					$this->session->set_flashdata('event_feedback', __('The City Info has successfully been set!'));
					redirect('cityinfo');
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
				
			}
		}
		
		$cdata['content'] 		= $this->load->view('c_cityinfo', array('error' => $error), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__("City Info") => "cityinfo");
		$this->load->view('master', $cdata);
	}

}