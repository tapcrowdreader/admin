<?php
if (!defined('BASEPATH'))
    exit('No direct script access');

class Appclone extends CI_Controller {
	private $clonnedEvents, $clonnedVenues;
	// clonnedEvents / clonnedVenues holds the previous and new event ids / venue ids 

    //php 5 constructor
    function __construct() {
        parent::__construct();
        if (_authed()) {
        }
		
		$this->clonnedEvents = array();
		$this->clonnedVenues = array();
		
        $this->load->model('app_model');
        $this->load->model('appearance_model');
        $this->load->model('citycontent_model');
        $this->load->model('event_model');
        $this->load->model('sessions_model');
        $this->load->model('appclone_model');
        $this->load->model('language_model');
        $this->load->model('general_model');
        $this->load->model('module_mdl');
    }

    //php 4 constructor
    function Apps() {
        parent::__construct();
        if (_authed()) {
        }
		
		$this->clonnedEvents = array();
		$this->clonnedVenues = array();
		
        $this->load->model('app_model');
        $this->load->model('appearance_model');
        $this->load->model('citycontent_model');
        $this->load->model('event_model');
        $this->load->model('sessions_model');
        $this->load->model('appclone_model');
        $this->load->model('language_model');
        $this->load->model('general_model');
        $this->load->model('module_mdl');
    }

    function index() {
		$this->config->item('imagespath') .'upload/pathtoimage';
        $this->session->set_userdata('appid', '');
        $this->session->set_userdata('eventid', '');
        $this->session->set_userdata('venueid', '');
        $apps = $this->app_model->appsByOrganizer(_currentUser()->organizerId);
        $dropdown = $this->appclone_model->getAppNames();
        $organizers = $this->appclone_model->getOrganizers();
        $cdata['content'] = $this->load->view('appclone', array('dropdown' => $dropdown,'organizer'=>$organizers), TRUE);
        $cdata['dropdown'] = $this->appclone_model->getAppNames();
        $this->load->view('master', $cdata);
    }

    function cloneApp() {
		$appId = $this->__getAppId();
        if (empty($appId)) {
            redirect('appclone/');
        }

        $this->__insertNewApp();
        $this->__insertAppEvents($appId);
        //$this->__insertAppStores($appId); // Don't insert data into appstore table.
        $this->__insertAppApperance($appId);
        $this->__insertAppVenues();
        $this->__insertAppSubFlavors($appId);
        $this->__insertEventModules();
        $this->__insertVenueModules();
		$this->__insertEventExhibitor();
        $this->__insertEventMap();
        $this->__insertVenueMap();        
        //$this->__insertVenueExhibitor();
        //$this->__insertEventSession();
        $this->__insertEventNews();
        $this->__insertVenueNews();
        //$this->__insertEventPoi();
        $this->__insertEventFacebook();
        $this->__insertEventFavorites();
        $this->__insertEventAds();
        $this->__insertEventLauncher();
        $this->__insertAppLauncher();
        //$this->__insertAppGroups();
        $this->__insertAppSocials();
        $this->__insertVenueSocials();
        $this->__insertEventSocials();
        $this->__insertAppArtists();
        $this->__insertAppAttendees();
        $this->__insertAppPushNotifications($appId);
        $this->__insertAppSchedule();
        $this->__insertVenueSchedule();
        $this->__insertAppSessionGroups();
        $this->__insertAppLanguages($appId);
        $this->__insertAppNews();
        $this->__insertVenueLauncher();
        $this->__insertEventCatalog();
        $this->__insertVenueCatalog();
        $this->__insertVenueArtists();
        $this->__insertContentModule();
        // $this->__insertEventForm();
		$this->session->set_flashdata('successmessage', '1');
        redirect('appclone/');
        $organizer_id = $this->appclone_model->getOrganizerId($appId);
        $events = $this->appclone_model->getEvents($appId);
        $cdata['content'] = $this->load->view('appclone', array('events' => $events), TRUE);
        $this->load->view('master', $cdata);
    }

    protected function __getNewAppName() {
        $newAppName = $this->input->post('newappname');

        if (empty($newAppName)) {
            $this->session->set_flashdata('nameerror', '1');
            redirect('appclone/');
        } else {
            return $newAppName;
        }
    }

    protected function __getAppId() {
        $appId = $this->input->post('appid');
        return $appId;
    }

    protected function __getOrganizerId() {
		//return _currentUser()->organizerId; // User can clone self apps, so new organizer id should pe the session organizer id
        $organizeridId = $this->input->post('organizerid');
        return $organizeridId;
    }

    protected function __getLastAppId() {
        $appId = $this->appclone_model->getLastAppId();
        return $appId;
    }

    protected function __getLastSessionGroupId() {
        $appId = $this->appclone_model->getLastSessiongroupId();
        return $appId;
    }
    protected function __getAppTypeId() {
        $appId = $this->appclone_model->getAppTypeId($appId);
        return $appId;
    }
    protected function __getLastEventId() {
        $eventId = $this->appclone_model->getLastEventId();
        return $eventId;
    }

    protected function __getLastVenuetId() {
        $venueId = $this->appclone_model->getLastVenueId();
        return $venueId;
    }
	
	protected function __getAccountId($organizerId){
		return $this->appclone_model->getAccountId($organizerId);
	}
	
    function copyImage($currentId, $lastId, $image){
        $dest="";
        $dbfilepath = explode('/',$image);
        $filepath = str_replace($dbfilepath[count($dbfilepath)-1],'',$image);
        $filepath = str_replace('/'.$currentId,'',$filepath);
		
        if(!empty($image)){
            $dir = $this->config->item('imagespath');
            $src = $image;
            $dest = str_replace($currentId,$lastId, $src);
            mkdir($dir.$filepath.$lastId,0777,TRUE);
                if(!is_dir($dir.$filepath.$lastId)){
                    $this->session->set_flashdata('direrror', "1");
                    $this->session->set_flashdata('direrrormsg', __('Unable to create directory: %s', $dir.$filepath.$lastId));
                }
            //copy($dir.$src, $dir.$dest) or die("Unable to copy $dir$src to $dir$dest");
            if (!copy($dir.$src, $dir.$dest)) {
                $this->session->set_flashdata('imageerror', "1");
                $this->session->set_flashdata('imageerrormsg', __('Unable to copy %s to %s', $dir.$src, $dir.$dest));
            }
        }
        return $dest;
    }

    function copydir($source,$destination)
    {
        if(!is_dir($destination)){
            $oldumask = umask(0);
            mkdir($destination, 0777); // so you get the sticky bit set
            umask($oldumask);
        }
        $dir_handle = @opendir($source) or die(__("Unable to open"));
        while ($file = readdir($dir_handle))
        {
            if($file!="." && $file!=".." && !is_dir("$source/$file")) //if it is file
            copy("$source/$file","$destination/$file");
            if($file!="." && $file!=".." && is_dir("$source/$file")) //if it is folder
            $this->copydir("$source/$file","$destination/$file");
        }
        closedir($dir_handle);
    }
    protected function __insertNewApp() {
        $appId = $this->__getAppId();
        $data = $this->appclone_model->getAppData($appId);
        $app_icon = $data->app_icon;
        $subdomain = $data->subdomain;
        if (empty($data->telephone)) {
            $telephone = 00000;
        } else {
            $telephone = $data->telephone;
        }

        $appname = $this->__getNewAppName();
        if (empty($appname)) {
            $appname = $data->name;
        } else {
            $appname = $this->__getNewAppName();
        }

        $organizerid = $this->__getOrganizerId();
        if(_isAdmin()== FALSE){
            //$organizerid = $data->organizerid;
            $organizerid = _currentUser()->organizerId;
        }else{
            $organizerid = $this->__getOrganizerId();
        }
		
		$accountId = $this->__getAccountId($organizerid);
		if( ! $accountId )
			$accountId = $organizerid;
		
        $appdata = array(
            'apptypeid' => $data->apptypeid,
            'organizerid' => $organizerid,
			'accountId' => $accountId,
            'name' => $appname,
            'info' => $data->info,
            'submit_description' => $data->submit_description,
            'category' => $data->category,
            'app_icon' => $data->app_icon,
            'homescreen' => $data->app_icon,
            'token' => $data->token,
            'certificate' => $data->certificate,
            'subdomain' => $data->subdomain,
            'cname' => $data->cname,
            'advancedFilter' => $data->advancedFilter,
            'status' => $data->status,
            'isdeleted' => $data->isdeleted,
            'creation' => date('Y-m-d H:i:m:s'),
            'defaultlanguage' => $data->defaultlanguage,
            'visibleintapcrowd' => $data->visibleintapcrowd,
            'applestoreid' => $data->applestoreid,
            'address' => $data->address,
            'lat' => $data->lat,
            'lon' => $data->lon,
            'telephone' => $telephone,
            'email' => $data->email,
            'website' => $data->website,
            'contentmodule_css_phone' => $data->contentmodule_css_phone,
            'contentmodule_css_tablet' => $data->contentmodule_css_tablet,
            'timestamp' => time()
        );

        $this->db->insert('app', $appdata);
        $lastAppId = $this->__getLastAppId();
        $newSubDomain = $subdomain.$lastAppId;

        $dest = $this->copyImage($appId, $lastAppId, $app_icon);

        $newSubDomain = $subdomain.$lastAppId;
        $data = array(
                    'subdomain' => $newSubDomain,
                    'app_icon'=>$dest,
                    'homescreen'=>$dest
                    );

        $this->db->where('id', $lastAppId);
        $this->db->update('app', $data);

        $path = '/var/www/MOBILESITE/src/public_html/themes/';

        $source = $path.$subdomain;
        $destination = $path.$newSubDomain;

        if(is_dir($source)){
            shell_exec( " mkdir $destination;  cp -r -a dir_source/* $source 2>&1 " );
            //$this->copydir($source,$destination);
        }

        $table='app';
        $translation = $this->appclone_model->getTranslation($table,$appId);
        if(!empty($translation)){
            foreach($translation as $tKey => $tValue){

                    $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                        $translationData = array(
                            'table' => $tValue->table,
                            'tableid' => $lastAppId,
                            'fieldname' => $tValue->fieldname,
                            'language' => $tValue->language,
                            'translation' => $tValue->translation
                        );
                        $this->db->insert('translation', $translationData);
                }
        }
    }

    protected function __insertAppEvents() {
        $appId = $this->input->post('appid');
        $events = $this->appclone_model->getEvents($appId);
        $lastAppId = $this->__getLastAppId();
        $organizerid = $this->__getOrganizerId();

        if(_isAdmin()== FALSE){
            $organizerid = _currentUser()->organizerId;
        }else{
            $organizerid = $this->__getOrganizerId();
        }

        foreach ($events as $key => $value) {
            $venueId = $event->venueid;
            $event = $this->appclone_model->getEventDetail($value);

            $oldEventId = $event->id;

            $app_icon = $event->eventlogo;

            $eventData=array(
                        'organizerid'=>$organizerid,
                        'venueid'=>$event->venueid,
                        'eventtypeid'=>$event->eventtypeid,
                        'name'=>$event->name,
                        'datefrom'=>$event->datefrom,
                        'dateto'=>$event->dateto,
                        'active'=>$event->active,
                        'eventlogo'=>$event->eventlogo,
                        'description'=>$event->description,
                        'phonenr'=>$event->phonenr,
                        'website'=>$event->website,
                        'ticketlink'=>$event->ticketlink,
                        'qrshare'=>$event->qrshare,
                        'timestamp'=>time(),
                        'deleted'=>$event->deleted,
                        'newsrss'=>$event->newsrss,
                        'order'=>$event->order,
                        'email'=>$event->email,
                        'parentid'=>$event->parentid,
            );

            if(count($event)>0){
                $this->db->insert('event', $eventData);
            }

            $lastEventId = $this->__getLastEventId();
			
			// Keeping old and new event ids
			$this->clonnedEvents[$oldEventId] = $lastEventId;

            //======= Cloning Form Module

            $this->__insertForms($appId, $oldEventId, $lastAppId, $lastEventId, $type = 'event');

            //======= End Cloning Form Module

/////////////////////////////////////////////////////////////////////////////////
                    $sponsorgroup = $this->appclone_model->getEventSponsorGroup($value);
                    if(count($sponsorgroup)>0){
                        foreach($sponsorgroup as $gkey=>$gvalue){
                                    $sponsorGroupData = array(
                                        'eventid' => $lastEventId,
                                        'venueid' => 0,
                                        'name' => $gvalue->name,
                                        'order' => $gvalue->order
                                    );
                            $this->db->insert('sponsorgroup', $sponsorGroupData);
                            $sponsorGroupLastId = $this->appclone_model->getLastSponsorGroupId();

                            $sponsor = $this->appclone_model->getEventSponsorDetail($value, $gvalue->id);

                            foreach($sponsor as $key2=>$value2){
                                        $sponsorData=array(
                                                'eventid' => $lastEventId,
                                                'venueid' => 0,
                                                'sponsorgroupid' => $sponsorGroupLastId,
                                                'name' => $value2->name,
                                                'description' => $value2->description,
                                                'image' => $value2->image,
                                                'website' => $value2->website,
                                                'order' => $value2->order,
                                                'x' => $value2->x,
                                                'y' => $value2->y
                                        );
                                        $this->db->insert('sponsor', $sponsorData);
                                        $lastSponsorId = $this->appclone_model->getLastSponsorId();
                            }
                        }
                    }
/////////////////////////////////////////////////////////////////////////////////
                    $this->__insertEventBrands($value,$lastEventId,$lastAppId);
                    $this->__insertEventCategories($value,$lastEventId,$lastAppId);
/////////////////////////////////////////////////////////////////////////////////
            $tags = $this->appclone_model->getEventTags($event->id);
            if(!empty($tags)){
                foreach($tags as $tagKey => $tagValue){
                    if($tagValue->formid > 0)
                        continue;
                    $tag = $this->appclone_model->getTagDetail($tagValue->id);
                    $tagData = array(
                        'tag' => $tag->tag,
                        'appid' =>$lastAppId,
                        'eventid'=> $lastEventId
                    );
                    if($tag->eventid!=0){
                        $this->db->insert('tag', $tagData);
                    }
                }
            }

            $dest = $this->copyImage($value, $lastEventId, $app_icon);

            $table='event';
            $translation = $this->appclone_model->getTranslation($table,$event->id);
            if(!empty($translation)){
                foreach($translation as $tKey => $tValue){

                        $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                            $translationData = array(
                                'table' => $tValue->table,
                                'tableid' => $lastEventId,
                                'fieldname' => $tValue->fieldname,
                                'language' => $tValue->language,
                                'translation' => $tValue->translation
                            );
                            $this->db->insert('translation', $translationData);
                    }
            }

            $sessions = $this->appclone_model->getSessions($value);

            foreach($sessions as $sessionKey => $sessionValue){

                //$session = $this->appclone_model->getSessionDetail($sessionValue);
                $sessionData = array(
                                'eventid' => $lastEventId,
                                'sessiongroupid' => $sessionValue->sessiongroupid,
                                'order' => $sessionValue->order,
                                'name' => $sessionValue->name,
                                'description' => $sessionValue->description,
                                'starttime' => $sessionValue->starttime,
                                'endtime' => $sessionValue->endtime,
                                'speaker' => $sessionValue->speaker,
                                'location' => $sessionValue->location,
                                'mapid' => $sessionValue->mapid,
                                'xpos' => $sessionValue->xpos,
                                'ypos' => $sessionValue->ypos,
                                'maptype' => $sessionValue->maptype,
                                'imageurl' => $sessionValue->imageurl,
                                'presentation' => $sessionValue->presentation,
                                'organizer' => $sessionValue->organizer,
                                'twitter' => $sessionValue->twitter,
                                'allowAddToFavorites' => $sessionValue->allowAddToFavorites,
                                'allowAddToAgenda' => $sessionValue->allowAddToAgenda,
                                'votes' => $sessionValue->votes,
                                'url' => $sessionValue->url
                );

                $this->db->insert('session', $sessionData);
                $lastSessionId = $this->appclone_model->getLastSessionId();

                //speakers
                $speakers = $this->appclone_model->getSpeakersOfSession($sessionValue->id);
                foreach($speakers as $speaker) {
                    $this->appclone_model->insertSpeaker($speaker, $lastSessionId, $lastEventId);
                }
                
                


                $tags = $this->appclone_model->getSessionTags($sessionValue->id);
                if(!empty($tags)){
                    foreach($tags as $tagKey => $tagValue){
                        if($tagValue->formid > 0)
                            continue;
                        $tag = $this->appclone_model->getTagDetail($tagValue->id);
                        $tagData = array(
                            'tag' => $tag->tag,
                            'appid' =>$lastAppId,
                            'sessionid'=> $lastSessionId
                        );
                        if($tag->sessionid!=0){
                            $this->db->insert('tag', $tagData);
                        }
                    }
                }

                $table='session';
                $translation = $this->appclone_model->getTranslation($table,$sessionValue->id);
                if(!empty($translation)){
                    foreach($translation as $tKey => $tValue){

                            $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                $translationData = array(
                                    'table' => $tValue->table,
                                    'tableid' => $lastSessionId,
                                    'fieldname' => $tValue->fieldname,
                                    'language' => $tValue->language,
                                    'translation' => $tValue->translation
                                );
                                $this->db->insert('translation', $translationData);
                        }
                }

           }
            $venue = $this->appclone_model->getVenueDetail($event->venueid);

            $venueData = array(
                        'name'=>$venue->name,
                        'address'=>$venue->address,
                        'lat'=>$venue->lat,
                        'lon'=>$venue->lon,
                        'telephone'=>$venue->telephone,
                        'fax'=>$venue->fax,
                        'email'=>$venue->email,
                        'openinghours'=>$venue->openinghours,
                        'info'=>$venue->info,
                        'travelinfo'=>$venue->travelinfo,
                        'image1'=>$venue->image1,
                        'image2'=>$venue->image2,
                        'image3'=>$venue->image3,
                        'image4'=>$venue->image4,
                        'image5'=>$venue->image5,
                        'active'=>$venue->active,
                        'timestamp'=>time(),
                        'deleted'=>$venue->deleted,
                        'website'=>$venue->website,
                        'facebookurl'=>$venue->facebookurl,
                        'twitterurl'=>$venue->twitterurl,
                        'toururl'=>$venue->toururl,
                        'vimeourl'=>$venue->vimeourl,
                        'order'=>$venue->order,
                        'eventid'=>$lastEventId
                );


            $this->db->insert('appevent', array('appid' => $lastAppId, 'eventid' => $lastEventId));
            $this->db->insert('venue', $venueData);

            $lastVenueId=$this->__getLastVenuetId();

            $tags = $this->appclone_model->getVenueTags($venue->id);
            if(!empty($tags)){
                foreach($tags as $tagKey => $tagValue){
                    if($tagValue->formid > 0)
                        continue;
                    $tag = $this->appclone_model->getTagDetail($tagValue->id);
                    $tagData = array(
                        'tag' => $tag->tag,
                        'appid' =>$lastAppId,
                        'venueid'=> $lastVenueId
                    );

                    if($tag->venueid!=0){
                        $this->db->insert('tag', $tagData);
                    }
                }
            }

            $updatedata = array(
               'eventlogo' => $dest,
                'venueid' => $lastVenueId
            );

            $this->db->where('id', $lastEventId);
            $this->db->update('event', $updatedata);
        }
    }

    protected function __insertAppVenues() {
        $appId = $this->input->post('appid');
        $venues = $this->appclone_model->getAppVenues($appId);
        $organizerid = $this->__getOrganizerId();
        if(_isAdmin()== FALSE){
            $organizerid = _currentUser()->organizerId;
        }else{
            $organizerid = $this->__getOrganizerId();
        }

        $lastAppId = $this->__getLastAppId();
        foreach ($venues as $key => $value) {
            $venue = $this->appclone_model->getVenueDetail($value);

            $oldVenueId = $venue->id;

            $venueData = array(
                        'name'=>$venue->name,
                        'address'=>$venue->address,
                        'lat'=>$venue->lat,
                        'lon'=>$venue->lon,
                        'telephone'=>$venue->telephone,
                        'fax'=>$venue->fax,
                        'email'=>$venue->email,
                        'openinghours'=>$venue->openinghours,
                        'info'=>$venue->info,
                        'travelinfo'=>$venue->travelinfo,
                        'image1'=>$venue->image1,
                        'image2'=>$venue->image2,
                        'image3'=>$venue->image3,
                        'image4'=>$venue->image4,
                        'image5'=>$venue->image5,
                        'active'=>$venue->active,
                        'timestamp'=>time(),
                        'deleted'=>$venue->deleted,
                        'website'=>$venue->website,
                        'facebookurl'=>$venue->facebookurl,
                        'twitterurl'=>$venue->twitterurl,
                        'toururl'=>$venue->toururl,
                        'vimeourl'=>$venue->vimeourl,
                        'order'=>$venue->order,
                        'eventid'=>0
                );

            if(count($venue)>0){
                $this->db->insert('venue',$venueData);
            }

            $lastVenueId=$this->__getLastVenuetId();
			
			// Keeping old and new event ids
			$this->clonnedVenues[$oldVenueId] = $lastVenueId;
			
            //== Cloning Form Module
            $this->__insertForms($appId, $oldVenueId, $lastAppId, $lastVenueId, $type = 'venue');
            //== End Cloning Form Module
			
			//== Cloning Coupons Module
            $this->__insertCoupons($oldVenueId, $lastVenueId);
            //== End Cloning Coupons Module
			
			//== Cloning Loyalty Module
            $this->__insertLoyalty($oldVenueId, $lastVenueId);
            //== End Cloning Loyalty Module
			
			/////////////////////////////////////////////////////////////////////////////////
                    $sponsorgroup = $this->appclone_model->getVenueSponsorGroup($value);
                    foreach($sponsorgroup as $gkey=>$gvalue){
                                $sponsorGroupData = array(
                                    'eventid' => 0,
                                    'venueid' => $lastVenueId,
                                    'name' => $gvalue->name,
                                    'order' => $gvalue->order
                                );

                        $this->db->insert('sponsorgroup', $sponsorGroupData);
                        $sponsorGroupLastId = $this->appclone_model->getLastSponsorGroupId();
                        $sponsors = $this->appclone_model->getVenueSponsorDetail($value, $gvalue->id);

                        foreach($sponsors as $key2=>$value2){
                                    $sponsorData=array(
                                            'eventid' => 0,
                                            'venueid' => $lastVenueId,
                                            'sponsorgroupid' => $sponsorGroupLastId,
                                            'name' => $value2->name,
                                            'description' => $value2->description,
                                            'image' => $value2->image,
                                            'website' => $value2->website,
                                            'order' => $value2->order,
                                            'x' => $value2->x,
                                            'y' => $value2->y
                                    );

                                    $this->db->insert('sponsor', $sponsorData);
                                    $lastSponsorId = $this->appclone_model->getLastSponsorId();
                        }
                    }
                  /////////////////////////////////////////////////////////////////////////////////
                    $this->__insertCatalogVenueCategories($value,$lastVenueId,$lastAppId);
                  /////////////////////////////////////////////////////////////////////////////////

            $tags = $this->appclone_model->getVenueTags($venue->id);
            if(!empty($tags)){
                foreach($tags as $tagKey => $tagValue){
                    if($tagValue->formid > 0)
                        continue;

                    $tag = $this->appclone_model->getTagDetail($tagValue->id);
                    $tagData = array(
                        'tag' => $tag->tag,
                        'appid' =>$lastAppId,
                        'venueid'=> $lastVenueId
                    );

                    if($tag->venueid!=0){
                        $this->db->insert('tag', $tagData);
                    }
                }
            }

            $this->db->insert('appvenue', array('appid' => $lastAppId, 'venueid' => $lastVenueId));

            $dest1 = $this->copyImage($value, $lastVenueId, $venue->image1);
            $dest2 = $this->copyImage($value, $lastVenueId, $venue->image2);
            $dest3 = $this->copyImage($value, $lastVenueId, $venue->image3);
            $dest4 = $this->copyImage($value, $lastVenueId, $venue->image4);
            $dest5 = $this->copyImage($value, $lastVenueId, $venue->image5);

            $updatedata = array(
               'image1' => $dest1,
               'image2' => $dest2,
               'image3' => $dest3,
               'image4' => $dest4,
               'image5' => $dest5
            );

            $this->db->where('id', $lastVenueId);
            $this->db->update('venue', $updatedata);

            $table='venue';
            $translation = $this->appclone_model->getTranslation($table,$venue->id);
            if(!empty($translation)){
                foreach($translation as $tKey => $tValue){

                        $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                            $translationData = array(
                                'table' => $tValue->table,
                                'tableid' => $lastVenueId,
                                'fieldname' => $tValue->fieldname,
                                'language' => $tValue->language,
                                'translation' => $tValue->translation
                            );
                            $this->db->insert('translation', $translationData);
                    }
            }

        }
    }

    protected function __insertAppSubFlavors($appId) {
        $appId = $this->input->post('appid');
        $events = $this->appclone_model->getSubFlavors($appId);

        $lastAppId = $this->__getLastAppId();

        foreach ($events as $key => $value) {
            $this->db->insert('appsubflavor', array('appid' => $lastAppId, 'subflavorid' => $value));
        }
    }

    protected function __insertAppStores($appId) {
        $appId = $this->__getAppId();
        $stores = $this->appclone_model->getAppStores($appId);
        $lastAppId = $this->__getLastAppId();

        count($stores);
        $data = array(
            'appid' => $lastAppId,
            'name' => $stores['name'],
            'appleappstore' => $stores['appleappstore'],
            'androidmarket' => $stores['androidmarket'],
            'blackberry' => $stores['blackberry'],
            'mobwebsite' => $stores['mobwebsite']
        );
        if (count($stores) > 0) {
            $this->db->insert('appstores', $data);
        }
    }

    protected function __insertAppApperance($appId) {
        $apperance = $this->appclone_model->getAppApperance($appId);
        $lastAppId = $this->__getLastAppId();

        foreach ($apperance as $value) {
            $data = array();
            $data['appid'] = $lastAppId;
            $data['controlid'] = $value->controlid;
            $data['value'] = $value->value;

            $this->db->insert('appearance', $data);
        }
    }

    protected  function __insertEventModules(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }
            $i=0;

            foreach ($events as $key => $value){

                    $modules = $this->appclone_model->getModuleDetail($value);
                    foreach($modules as $key => $value){
                        $moduleData=array(
                                        'moduletype' => $value->moduletype,
                                        'event' => $newEvents[$i]
                        );
                        $this->db->insert('module', $moduleData);
                    }
                    $i++;
            }
    }
    protected  function __insertVenueModules(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getAppVenues($appId);
            $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);
            $thisAppVenue = array();
            foreach($thisAppVenues as $key=>$value){
                $thisAppVenue[] = $value;
            }
            $i=0;

            foreach ($events as $key => $value){

                    $modules = $this->appclone_model->getVenueModuleDetail($value);
                    foreach($modules as $key => $value){
                        $moduleData=array(
                                        'moduletype' => $value->moduletype,
                                        'venue' => $thisAppVenue[$i]
                        );
                        $this->db->insert('module', $moduleData);
                    }
                    $i++;
            }
    }
	
	protected function __cloneEventMaps($oldEventId, $newEventId, $parentId = 0, $newParentId = 0){
		$maps = array();
		$this->db->select('*');
		$this->db->where('eventid', $oldEventId);
		$this->db->where('parentId', $parentId);
		$res = $this->db->get('map');
		if( $res->num_rows() > 0 ) $maps = $res->result();
		
		foreach($maps as $map){
			$data = array(
				'eventid' => $newEventId,
				'venueid' => $map->venueid,
				'imageurl' => $map->imageurl,
				'showNamesOnMap' => $map->showNamesOnMap,
				'parentId' => $newParentId,			
				'timestamp' => time(),
				'width' => $map->width,
				'height' => $map->height,
				'x' => $map->x,
				'y' => $map->y,
				'title' => $map->title
			);
			
			if( $map->imageurl ){
            	$this->db->insert('map', $data);
				$lastMapId= $this->db->insert_id(); //$this->appclone_model->getLastMapId();
				
				$dest = $this->copyImage($oldEventId, $newEventId, $map->imageurl);
				$updatedata = array(
					'imageurl' => $dest,
				);
				
				$this->db->where('id', $lastMapId);
                $this->db->update('map', $updatedata);
				
				$updatedata = array(
					'mapid' => $lastMapId,
				);
				
				$table = 'session';
				$this->db->query( 'UPDATE ' . $table . ' SET mapid = ' . $lastMapId . ' WHERE mapid = ' . $map->id . ' AND eventid = ' . $newEventId );
				
				$table = 'exhibitor';
				$this->db->query( 'UPDATE ' . $table . ' SET mapid = ' . $lastMapId . ' WHERE mapid = ' . $map->id . ' AND eventid = ' . $newEventId );
				
				$this->__cloneEventMaps($oldEventId, $newEventId, $map->id, $lastMapId);
			}
		}
		
	}
	
	protected function __cloneVenueMaps($oldVenueId, $newVenueId, $parentId = 0, $newParentId = 0){
		$maps = array();
		$this->db->select('*');
		$this->db->where('venueid', $oldVenueId);
		$this->db->where('parentId', $parentId);
		$res = $this->db->get('map');
		if( $res->num_rows() > 0 ) $maps = $res->result();
		
		foreach($maps as $map){
			$data = array(
				'eventid' => $map->eventid,
				'venueid' => $newVenueId,
				'imageurl' => $map->imageurl,
				'showNamesOnMap' => $map->showNamesOnMap,
				'parentId' => $newParentId,			
				'timestamp' => time(),
				'width' => $map->width,
				'height' => $map->height,
				'x' => $map->x,
				'y' => $map->y,
				'title' => $map->title
			);
			
			if( $map->imageurl ){
            	$this->db->insert('map', $data);
				$lastMapId= $this->db->insert_id(); //$this->appclone_model->getLastMapId();
				
				$dest = $this->copyImage($oldVenueId, $newVenueId, $map->imageurl);
				$updatedata = array(
					'imageurl' => $dest,
				);
				
				$this->db->where('id', $lastMapId);
                $this->db->update('map', $updatedata);
				
				$updatedata = array(
					'mapid' => $lastMapId,
				);
				
				$this->__cloneVenueMaps($oldVenueId, $newVenueId, $map->id, $lastMapId);
			}
		}
		
	}
    
	protected  function __insertEventMap(){
		foreach( $this->clonnedEvents as $oldEvent => $newEvent ){
			$this->__cloneEventMaps($oldEvent, $newEvent);
		}
		return TRUE;
		
		/*
		$appId = $this->input->post('appid');
		$lastAppId = $this->__getLastAppId();
		$events = $this->appclone_model->getEvents($appId);
		$thisAppEvents = $this->appclone_model->getEvents($lastAppId);

		$newEvents=array();

		foreach($thisAppEvents as $key => $value){
			$newEvents[] = $value;
		}

		$i=0;

		foreach ($events as $key => $value){

				$map = $this->appclone_model->getMapDetail($value);
				$mapData=array(
							'eventid' => $newEvents[$i],
							'venueid' => $map->venueid,
							'imageurl' => $map->imageurl,
							'showNamesOnMap' => $map->showNamesOnMap,
							'timestamp' => time()
				);

				if(!empty($map->imageurl)){
					$this->db->insert('map', $mapData);
					$lastMapId= $this->appclone_model->getLastMapId();

					$dest = $this->copyImage($value, $newEvents[$i], $map->imageurl);
						$updatedata = array(
							'imageurl' => $dest,
						);

						$this->db->where('id', $lastMapId);
						$this->db->update('map', $updatedata);
				}
				$i++;
		}
			*/
    }

    protected  function __insertVenueMap(){
		foreach( $this->clonnedVenues as $oldVenue => $newVenue ){
			$this->__cloneVenueMaps($oldVenue, $newVenue);
		}
		return TRUE;
		/*
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $venues = $this->appclone_model->getAppVenues($appId);
            $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);

            $newVenues=array();

            foreach($thisAppVenues as $key => $value){
                $newVenues[] = $value;
            }

            $i=0;

            foreach ($venues as $key => $value){

                    $map = $this->appclone_model->getVenueMapDetail($value);
                    $mapData=array(
                                'eventid' => 0,
                                'venueid' => $newVenues[$i],
                                'imageurl' => $map->imageurl,
                                'showNamesOnMap' => $map->showNamesOnMap,
                                'timestamp' => time()
                    );

                    if(!empty($map->imageurl)){
                        $this->db->insert('map', $mapData);
                        $lastMapId= $this->appclone_model->getLastMapId();

                        $dest = $this->copyImage($value, $newVenues[$i], $map->imageurl);

                            $updatedata = array(
                                'imageurl' => $dest,
                            );

                            $this->db->where('id', $lastMapId);
                            $this->db->update('map', $updatedata);
                    }
                    $i++;
            }
			*/
    }

     protected  function __insertEventExhibitor(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){

                    $exhibitor = $this->appclone_model->getExhibitorDetail($value);
                    foreach($exhibitor as $key2 => $value2){
                        $exhibitorData=array(
                                'venueid' => 0,
                                'eventid' => $newEvents[$i],
                                'exhibitorcategoryid' => 0,
                                'name' => $value2->name,
                                'shortname' => $value2->shortname,
                                'booth' => $value2->booth,
                                'imageurl' => $value2->imageurl,
                                'mapid' => $value2->mapid,
                                'y1' => $value2->y1,
                                'y2' => $value2->y2,
                                'x1' => $value2->x1,
                                'x2' => $value2->x2,
                                'description' => $value2->description,
                                'tel' => $value2->tel,
                                'address' => $value2->address,
                                'web' => $value2->web,
                                'code' => $value2->code,
//                                'username' => $value2->username,
//                                'password' => $value2->password,
                                'image1' => $value2->image1,
                                'imagedescription1' => $value2->imagedescription1,
                                'image2' => $value2->image2,
                                'imagedescription2' => $value2->imagedescription2,
                                'image3' => $value2->image3,
                                'imagedescription3' => $value2->imagedescription3,
                                'image4' => $value2->image4,
                                'imagedescription4' => $value2->imagedescription4,
                                'image5' => $value2->image5,
                                'imagedescription5' => $value2->imagedescription5,
                                'image6' => $value2->image6,
                                'imagedescription6' => $value2->imagedescription6,
                                'image7' => $value2->image7,
                                'imagedescription7' => $value2->imagedescription7,
                                'image8' => $value2->image8,
                                'imagedescription8' => $value2->imagedescription8,
                                'image9' => $value2->image9,
                                'imagedescription9' => $value2->imagedescription9,
                                'image10' => $value2->image10,
                                'imagedescription10' => $value2->imagedescription10,
                                'image11' => $value2->image11,
                                'imagedescription11' => $value2->imagedescription11,
                                'image12' => $value2->image12,
                                'imagedescription12' => $value2->imagedescription12,
                                'image13' => $value2->image13,
                                'imagedescription13' => $value2->imagedescription13,
                                'image14' => $value2->image14,
                                'imagedescription14' => $value2->imagedescription14,
                                'image15' => $value2->image15,
                                'imagedescription15' => $value2->imagedescription15,
                                'image16' => $value2->image16,
                                'imagedescription16' => $value2->imagedescription16,
                                'image17' => $value2->image17,
                                'imagedescription17' => $value2->imagedescription17,
                                'image18' => $value2->image18,
                                'imagedescription18' => $value2->imagedescription18,
                                'image19' => $value2->image19,
                                'imagedescription19' => $value2->imagedescription19,
                                'image20' => $value2->image20,
                                'imagedescription20' => $value2->imagedescription20
                        );

                        $this->db->insert('exhibitor', $exhibitorData);
                        $lastExhibitorId = $this->appclone_model->getLastExhibitorId();

                        $groupItem = $this->appclone_model->getGroupItem($value2->id);
                        foreach($groupItem as $grkey=>$grvalue){
                            $groupname = $this->appclone_model->getGroupName($grvalue->groupid);
                            $getLastGroupId = $this->appclone_model->getLastExhibitorGroupId($groupname);
                            $getLastGroupId;
                            $groupItemData = array (
                                        'appid' => $lastAppId,
                                        'eventid' => $newEvents[$i],
                                        'venueid' => 0,
                                        'groupid' => $getLastGroupId,
                                        'itemtable' => 'exhibitor',
                                        'itemid' => $lastExhibitorId,
                                        'displaytype' => 0
                            );
                            if(!empty($getLastGroupId )){
                                $this->db->insert('groupitem', $groupItemData);
                            }
                        }

                        ////////////////Exhibitor Tags////////////////
                        $tags = $this->appclone_model->getExhibitorTags($value2->id);
                        if(!empty($tags)){
                            foreach($tags as $tagKey => $tagValue){
                                if($tagValue->formid > 0)
                                    continue;

                                $tag = $this->appclone_model->getTagDetail($tagValue->id);
                                $tagData = array(
                                    'tag' => $tag->tag,
                                    'appid' =>$lastAppId,
                                    'exhibitorid'=> $lastExhibitorId
                                );

                                if($tag->exhibitorid!=0){
                                    $this->db->insert('tag', $tagData);
                                }
                            }
                        }
                        ////////////////Exhibitor Translation////////////////
                        $table='exhibitor';
                        $translation = $this->appclone_model->getTranslation($table,$value2->id);
                        if(!empty($translation)){
                            foreach($translation as $tKey => $tValue){

                                    $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                        $translationData = array(
                                            'table' => $tValue->table,
                                            'tableid' => $lastExhibitorId,
                                            'fieldname' => $tValue->fieldname,
                                            'language' => $tValue->language,
                                            'translation' => $tValue->translation
                                        );
                                        $this->db->insert('translation', $translationData);
                                }
                        }
                   }$i++;
            }
    }

    protected  function __insertEventSession(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){
                    $session = $this->appclone_model->getSessionDetail($value);
                    $sessionData=array(
                                'eventid' => $newEvents[$i],
                                'sessiongroupid' => $session->sessiongroupid,
                                'order' => $session->order,
                                'name' => $session->name,
                                'description' => $session->description,
                                'starttime' => $session->starttime,
                                'endtime' => $session->endtime,
                                'speaker' => $session->speaker,
                                'location' => $session->location,
                                'mapid' => $session->mapid,
                                'xpos' => $session->xpos,
                                'ypos' => $session->ypos,
                                'maptype' => $session->maptype,
                                'imageurl' => $session->imageurl,
                                'presentation' => $session->presentation,
                                'organizer' => $session->organizer,
                                'twitter' => $session->twitter,
                                'allowAddToFavorites' => $session->allowAddToFavorites,
                                'allowAddToAgenda' => $session->allowAddToAgenda,
                                'votes' => $session->votes,
                                'url' => $session->url
                    );

                    if(!empty($session->eventid)){
                        $this->db->insert('session', $sessionData);
                    }
                    $i++;
            }
    }

    protected  function __insertEventNews(){
		foreach( $this->clonnedEvents as $oldEventId => $newEventId){
			$newsItems = array();
			$newsItemSourceIds = array();
			
			$this->db->select('*');
			$this->db->where('eventid', $oldEventId);
			$this->db->order_by('sourceid', 'ASC');
			$res = $this->db->get('newsitem');
			if( $res->num_rows() > 0 ) $newsItems = $res->result();
			$prvSourceId = 0;
			$newSourceId = 0;
			
			foreach( $newsItems as $newsItem ){				
				if($newsItem->sourceid && $prvSourceId != $newsItem->sourceid){
					$prvSourceId = $newsItem->sourceid;
					
					$nsQry = 'SELECT * FROM newssource WHERE id = ' . $newsItem->sourceid;
					$nsRes = $this->db->query($nsQry);
					if( $nsRes->num_rows() > 0 ){
						$qry = 'INSERT INTO  newssource  ( `id`, `eventid`, `venueid`, `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` )  
						SELECT \'\', \'' . $newEventId . '\', `venueid`, `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` FROM newssource 
						WHERE `id` = ' . $newsItem->sourceid;			
						$this->db->query($qry);
						$newSourceId = $this->db->insert_id();
					}
				}
				
				$nData = array(
					'eventid' => $newEventId,
					'venueid' => $newsItem->venueid,
					'appid' => $newsItem->appid,
					'title' => $newsItem->title,
					'txt' => $newsItem->txt,
					'image' => $newsItem->image,
					'url' => $newsItem->url,
					'videourl' => $newsItem->videourl,
					'sourceid' => ( $newSourceId > 0 ) ? $newSourceId : $newsItem->sourceid,
					'datum' => $newsItem->datum,
					'order' => $newsItem->order
				);
				$this->db->insert('newsitem', $nData);
				$newId = $this->db->insert_id();
				$oldId = $newsItem->id;
				
				$newsItemSourceIds[] = $newsItem->sourceid;
				$qry = 'INSERT INTO  translation  ( `id`, `table`, `tableid`, `fieldname`, `language`, `translation` )  
				SELECT \'\', `table`, \'' . $newId . '\', `fieldname`, `language`, `translation` from translation 
				WHERE `tableid` = ' . $oldId . ' AND `table` = \'newsitem\'';
				$this->db->query($qry);
				
				if( $newsItem->image ){
					$dest = $this->copyImage($oldId, $newId, $newsItem->image);
					$updatedata = array(
                    	'image' => $dest,
					);

					$this->db->where('id', $newId);
					$this->db->update('newsitem', $updatedata);	
				}
				
			} // foreach( $newsItems as $newsItem )
			
			if( count( $newsItemSourceIds ) > 0 ){
				$inClz = implode(',', $newsItemSourceIds);
				if( $inClz ){
					$qry = 'INSERT INTO  newssource  ( `id`, `eventid`, `venueid`, `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` )  
					SELECT \'\', \'' . $newEventId . '\', `venueid`, `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` FROM newssource 
					WHERE `eventid` = ' . $oldEventId . ' AND `id` NOT IN (' . $inClz . ')';
					$this->db->query($qry);
				}
			} // if( count( $newsItemSourceIds ) > 0 )
			
		} // foreach( $this->clonnedEvents as $oldEventId => $newEventId)
		
		return TRUE;
		/*
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $apptypeid = $this->__getAppTypeId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){
                    $thisEventId = $value;

                    $news = $this->appclone_model->getNewsDetail($value);

                    foreach($news as $nkey => $nvalue){
                        $newsData=array(
                                    'eventid' => $newEvents[$i],
                                    'venueid' => 0,
                                    'appid' => 0,
                                    'title' => $nvalue->title,
                                    'txt' => $nvalue->txt,
                                    'image' => $nvalue->image,
                                    'url' => $nvalue->url,
                                    'sourceid' => $nvalue->sourceid,
                                    'datum' => $nvalue->datum,
                                    'order' => $nvalue->order
                        );
                            $this->db->insert('newsitem', $newsData);
                            $lastNewsId= $this->db->insert_id(); //$this->appclone_model->getLastNewsId();

                            $tags = $this->appclone_model->getNewsTags($value->id);
                            if(!empty($tags)){
                                foreach($tags as $tagKey => $tagValue){
                                    if($tagValue->formid > 0)
                                        continue;

                                    $tag = $this->appclone_model->getTagDetail($tagValue->id);
                                    $tagData = array(
                                        'tag' => $tag->tag,
                                        'appid' =>$lastAppId,
                                        'newsitemid'=> $lastNewsId
                                    );

                                    if($tag->newsitemid!=0){
                                        $this->db->insert('tag', $tagData);
                                    }
                                }
                            }

                            $dest = $this->copyImage($value->id, $lastNewsId, $value->image);

                                $updatedata = array(
                                    'image' => $dest,
                                );

                                $this->db->where('id', $lastNewsId);
                                $this->db->update('newsitem', $updatedata);

                                    $table='newsitem';
                                    $translation = $this->appclone_model->getTranslation($table,$value->id);
                                    if(!empty($translation)){
                                        foreach($translation as $tKey => $tValue){

                                                $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                                    $translationData = array(
                                                        'table' => $tValue->table,
                                                        'tableid' => $lastNewsId,
                                                        'fieldname' => $tValue->fieldname,
                                                        'language' => $tValue->language,
                                                        'translation' => $tValue->translation
                                                    );
                                                    $this->db->insert('translation', $translationData);
                                            }
                                    }
                                    $rss = $this->appclone_model->getRssData($thisEventId);
                                    if(count($rss)>0){
                                        foreach($rss as $rsskey=>$rssvalue){
                                                $rssData=array(
                                                            'eventid' => $newEvents[$i],
                                                            'venueid' => 0,
                                                            'appid' => 0,
                                                            'type' => $rssvalue->type,
                                                            'url' => $rssvalue->url,
                                                            'timestamp' => date('Y-m-d H:i:s'),
                                                            'refreshrate' => $rssvalue->refreshrate,
                                                            'hash' => $rssvalue->hash
                                                );

                                                $this->db->insert('newssource', $rssData);

                                                $lastRssId= $this->db->insert_id(); //$this->appclone_model->getLastRssId();
                                        }
                                    }
                                    $updatedata = array(
                                        'sourceid' => $lastRssId,
                                    );

                                    $this->db->where('eventid', $newEvents[$i]);

                                    $this->db->update('newsitem', $updatedata);
                    }
                    $i++;

            }
			*/
    }

    protected  function __insertVenueNews(){
		foreach( $this->clonnedVenues as $oldVenueId => $newVenueId){
			$newsItems = array();
			$newsItemSourceIds = array();
			
			$this->db->select('*');
			$this->db->where('venueid', $oldVenueId);
			$this->db->order_by('sourceid', 'ASC');
			$res = $this->db->get('newsitem');
			if( $res->num_rows() > 0 ) $newsItems = $res->result();
			$prvSourceId = 0;
			$newSourceId = 0;
			
			foreach( $newsItems as $newsItem ){				
				if($newsItem->sourceid && $prvSourceId != $newsItem->sourceid){
					$prvSourceId = $newsItem->sourceid;
					
					$nsQry = 'SELECT * FROM newssource WHERE id = ' . $newsItem->sourceid;
					$nsRes = $this->db->query($nsQry);
					if( $nsRes->num_rows() > 0 ){
						$qry = 'INSERT INTO  newssource  ( `id`, `eventid`, `venueid`, `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` )  
						SELECT \'\', `eventid`, \'' . $newVenueId . '\', `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` FROM newssource 
						WHERE `id` = ' . $newsItem->sourceid;
						$this->db->query($qry);
						$newSourceId = $this->db->insert_id();
					}
				}
				
				$nData = array(
					'eventid' => $newsItem->eventid,
					'venueid' => $newVenueId,
					'appid' => $newsItem->appid,
					'title' => $newsItem->title,
					'txt' => $newsItem->txt,
					'image' => $newsItem->image,
					'url' => $newsItem->url,
					'videourl' => $newsItem->videourl,
					'sourceid' => ( $newSourceId > 0 ) ? $newSourceId : $newsItem->sourceid,
					'datum' => $newsItem->datum,
					'order' => $newsItem->order
				);
				$this->db->insert('newsitem', $nData);
				$newId = $this->db->insert_id();
				$oldId = $newsItem->id;
				
				$newsItemSourceIds[] = $newsItem->sourceid;
				$qry = 'INSERT INTO  translation  ( `id`, `table`, `tableid`, `fieldname`, `language`, `translation` )  
				SELECT \'\', `table`, \'' . $newId . '\', `fieldname`, `language`, `translation` from translation 
				WHERE `tableid` = ' . $oldId . ' AND `table` = \'newsitem\'';
				$this->db->query($qry);
				
				if( $newsItem->image ){
					$dest = $this->copyImage($oldId, $newId, $newsItem->image);
					$updatedata = array(
                    	'image' => $dest,
					);

					$this->db->where('id', $newId);
					$this->db->update('newsitem', $updatedata);	
				}
				
			} // foreach( $newsItems as $newsItem )
			
			if( count( $newsItemSourceIds ) > 0 ){
				$inClz = implode(',', $newsItemSourceIds);
				if( $inClz ){
					$qry = 'INSERT INTO  newssource  ( `id`, `eventid`, `venueid`, `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` )  
					SELECT \'\', `eventid`, \'' . $newVenueId . '\', `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` FROM newssource 
					WHERE `venueid` = ' . $oldVenueId . ' AND `id` NOT IN (' . $inClz . ')';
					$this->db->query($qry);
				}
			} // if( count( $newsItemSourceIds ) > 0 )
			
		} // foreach( $this->clonnedVenues as $oldVenueId => $newVenueId)
		
		return TRUE;
		/*
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $venues = $this->appclone_model->getAppVenues($appId);

            $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);

            $newVenues = array();

            foreach($thisAppVenues as $keym => $valuem){
                $newVenues[] = $valuem;
            }
            $i=0;
            foreach ($venues as $key => $value){
                    $thisVenueId = $value;
                    $news = $this->appclone_model->getVenueNewsDetail($value);
                    foreach($news as $key => $value){
                        $newsData=array(
                                    'eventid' => 0,
                                    'venueid' => $newVenues[$i],
                                    'appid' => 0,
                                    'title' => $value->title,
                                    'txt' => $value->txt,
                                    'image' => $value->image,
                                    'url' => $value->url,
                                    'sourceid' => $value->sourceid,
                                    'datum' => $value->datum,
                                    'order' => $value->order
                        );


                            $this->db->insert('newsitem', $newsData);
                            $lastNewsId= $this->db->insert_id(); //$this->appclone_model->getLastNewsId();

                            $tags = $this->appclone_model->getNewsTags($value->id);
                            if(!empty($tags)){
                                foreach($tags as $tagKey => $tagValue){
                                    if($tagValue->formid > 0)
                                        continue;
                                    $tag = $this->appclone_model->getTagDetail($tagValue->id);
                                    $tagData = array(
                                        'tag' => $tag->tag,
                                        'appid' =>$lastAppId,
                                        'newsitemid'=> $lastNewsId
                                    );

                                    if($tag->newsitemid!=0){

                                        $this->db->insert('tag', $tagData);
                                    }
                                }
                            }

                            $dest = $this->copyImage($value->id, $lastNewsId, $value->image);

                                $updatedata = array(
                                    'image' => $dest,
                                );

                                $this->db->where('id', $lastNewsId);
                                $this->db->update('newsitem', $updatedata);


                                $table='newsitem';
                                $translation = $this->appclone_model->getTranslation($table,$value->id);
                                if(!empty($translation)){
                                    foreach($translation as $tKey => $tValue){

                                            $translation = $this->appclone_model->getTranslationDetail($tValue->id);

                                                $translationData = array(
                                                    'table' => $tValue->table,
                                                    'tableid' => $lastNewsId,
                                                    'fieldname' => $tValue->fieldname,
                                                    'language' => $tValue->language,
                                                    'translation' => $tValue->translation
                                                );

                                                $this->db->insert('translation', $translationData);
                                        }
                                }


                                $rss = $this->appclone_model->getVenueRssData($thisVenueId);

                                if(count($rss)>0){
                                    foreach($rss as $rsskey=>$rssvalue){
                                            $rssData=array(
                                                        'eventid' => 0,
                                                        'venueid' => $newVenues[$i],
                                                        'appid' => 0,
                                                        'type' => $rssvalue->type,
                                                        'url' => $rssvalue->url,
                                                        'timestamp' => date('Y-m-d H:i:s'),
                                                        'refreshrate' => $rssvalue->refreshrate,
                                                        'hash' => $rssvalue->hash
                                            );

                                            $this->db->insert('newssource', $rssData);

                                            $lastRssId = $this->db->insert_id(); //$this->appclone_model->getLastRssId();
                                    }
                                }
                                $updatedata = array(
                                    'sourceid' => $lastRssId,
                                );

                                $this->db->where('venueid', $newVenues[$i]);

                                $this->db->update('newsitem', $updatedata);
                   }

                   $i++;
            }
			*/
    }

    protected  function __insertAppNews(){
		$appId = $this->input->post('appid');
        $lastAppId = $this->__getLastAppId();
		
		$newsItems = array();
		$newsItemSourceIds = array();
			
		$this->db->select('*');
		$this->db->where('appid', $appId);
		$this->db->order_by('sourceid', 'ASC');
		$res = $this->db->get('newsitem');
		if( $res->num_rows() > 0 ) $newsItems = $res->result();
		$prvSourceId = 0;
		$newSourceId = 0;
			
		foreach( $newsItems as $newsItem ){
			if($newsItem->sourceid && $prvSourceId != $newsItem->sourceid){
				$prvSourceId = $newsItem->sourceid;
					
				$nsQry = 'SELECT * FROM newssource WHERE id = ' . $newsItem->sourceid;
				$nsRes = $this->db->query($nsQry);
				if( $nsRes->num_rows() > 0 ){
					$qry = 'INSERT INTO  newssource  ( `id`, `eventid`, `venueid`, `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` )  
					SELECT \'\', `eventid`, `venueid`, \'' . $lastAppId . '\', `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` FROM newssource 
					WHERE `id` = ' . $newsItem->sourceid;
					$this->db->query($qry);
					$newSourceId = $this->db->insert_id();
				}
			}
				
			$nData = array(
				'eventid' => $newsItem->eventid,
				'venueid' => $newsItem->venueid,
				'appid' => $lastAppId,
				'title' => $newsItem->title,
				'txt' => $newsItem->txt,
				'image' => $newsItem->image,
				'url' => $newsItem->url,
				'videourl' => $newsItem->videourl,
				'sourceid' => ( $newSourceId > 0 ) ? $newSourceId : $newsItem->sourceid,
				'datum' => $newsItem->datum,
				'order' => $newsItem->order
			);
			$this->db->insert('newsitem', $nData);
			$newId = $this->db->insert_id();
			$oldId = $newsItem->id;
				
			$newsItemSourceIds[] = $newsItem->sourceid;
			$qry = 'INSERT INTO  translation  ( `id`, `table`, `tableid`, `fieldname`, `language`, `translation` )  
			SELECT \'\', `table`, \'' . $newId . '\', `fieldname`, `language`, `translation` from translation 
			WHERE `tableid` = ' . $oldId . ' AND `table` = \'newsitem\'';
			$this->db->query($qry);
				
			if( $newsItem->image ){
				$dest = $this->copyImage($oldId, $newId, $newsItem->image);
				$updatedata = array(
                   	'image' => $dest,
				);

				$this->db->where('id', $newId);
				$this->db->update('newsitem', $updatedata);	
			}
				
		} // foreach( $newsItems as $newsItem )
			
		if( count( $newsItemSourceIds ) > 0 ){
			$inClz = implode(',', $newsItemSourceIds);
			if( $inClz ){
				$qry = 'INSERT INTO  newssource  ( `id`, `eventid`, `venueid`, `appid`, `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` )  
				SELECT \'\', `eventid`, `venueid`, \'' . $lastAppId . '\', `type`, `url`, `timestamp`, `refreshrate`, `hash`, `tags` FROM newssource 
				WHERE `appid` = ' . $appId . ' AND `id` NOT IN (' . $inClz . ')';
				$this->db->query($qry);
			}
		} // if( count( $newsItemSourceIds ) > 0 )
			
		return TRUE;
		
		/*
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $appnews = $this->appclone_model->getAppNews($appId);

            foreach ($appnews as $key => $value){


                    $news = $this->appclone_model->getAppNewsDetail($value);
                    foreach($news as $key => $value){
                        $newsData=array(
                                    'eventid' => 0,
                                    'venueid' => 0,
                                    'appid' => $lastAppId,
                                    'title' => $value->title,
                                    'txt' => $value->txt,
                                    'image' => $value->image,
                                    'url' => $value->url,
                                    'sourceid' => $value->sourceid,
                                    'datum' => $value->datum,
                                    'order' => $value->order
                        );

                            $this->db->insert('newsitem', $newsData);
                            $lastNewsId= $this->db->insert_id(); //$this->appclone_model->getLastNewsId();

                            $tags = $this->appclone_model->getNewsTags($value->id);
                            if(!empty($tags)){

                                foreach($tags as $tagKey => $tagValue){
                                    if($tagValue->formid > 0)
                                        continue;
                                    $tag = $this->appclone_model->getTagDetail($tagValue->id);

                                    $tagData = array(
                                        'tag' => $tag->tag,
                                        'appid' =>$lastAppId,
                                        'newsitemid'=> $lastNewsId
                                    );

                                    if($tag->newsitemid!=0){

                                        $this->db->insert('tag', $tagData);
                                    }
                                }
                            }

                            $table='newsitem';
                            $translation = $this->appclone_model->getTranslation($table,$value->id);
                            if(!empty($translation)){
                                foreach($translation as $tKey => $tValue){

                                        $translation = $this->appclone_model->getTranslationDetail($tValue->id);

                                            $translationData = array(
                                                'table' => $tValue->table,
                                                'tableid' => $lastNewsId,
                                                'fieldname' => $tValue->fieldname,
                                                'language' => $tValue->language,
                                                'translation' => $tValue->translation
                                            );

                                            $this->db->insert('translation', $translationData);
                                    }
                            }

                    }

                    $rss = $this->appclone_model->getAppRssData($appId);

                    if(count($rss)>0){
                        foreach($rss as $rsskey=>$rssvalue){
                                $rssData=array(
                                            'eventid' => 0,
                                            'venueid' => 0,
                                            'appid' => $lastAppId,
                                            'type' => $rssvalue->type,
                                            'url' => $rssvalue->url,
                                            'timestamp' => date('Y-m-d H:i:s'),
                                            'refreshrate' => $rssvalue->refreshrate,
                                            'hash' => $rssvalue->hash
                                );

                                $this->db->insert('newssource', $rssData);

                                $lastRssId= $this->db->insert_id(); //$this->appclone_model->getLastRssId();
                        }
                    }
                    $updatedata = array(
                        'sourceid' => $lastRssId,
                    );

                    $this->db->where('appid', $lastAppId);

                    $this->db->update('newsitem', $updatedata);
            }
			*/
    }

    protected  function __insertEventCatalog(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $apptypeid = $this->__getAppTypeId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){


                    $cataloggroup = $this->appclone_model->getEventCatalogGroup($value);
                    if(count($cataloggroup)>0){
                    foreach($cataloggroup as $gkey=>$gvalue){
                         $catalogGroupData = array(
                            'eventid' => $newEvents[$i],
                            'venueid' => 0,
                            'name' => $gvalue->name,
                            'order' => $gvalue->order
                         );

                         $this->db->insert('cataloggroup', $catalogGroupData);

                         }
                    }
                    $catalogGroupLastId = $this->appclone_model->getLastCatalogGroupId();
                    $catalog = $this->appclone_model->getEventCatalogDetail($value);

                    foreach($catalog as $key => $value){
                        $catalogData=array(
                                    'eventid' => $newEvents[$i],
                                    'venueid' => 0,
                                    'cataloggroupid' => $catalogGroupLastId,
                                    'name' => $value->name,
                                    'description' => $value->description,
                                    'imageurl' => $value->imageurl,
                                    'order' => $value->order,
                                    'type' => $value->type,
                                    'sourceid' => $value->sourceid,
                                    'url' => $value->url,
                                    'date' => date('Y-m-d')
                            );
                            $this->db->insert('catalog', $catalogData);
                            $lastCatalogId= $this->appclone_model->getLastCatalogId();

                            $tags = $this->appclone_model->getCatalogTags($value->id);
                            if(!empty($tags)){
                                foreach($tags as $tagKey => $tagValue){
                                    if($tagValue->formid > 0)
                                        continue;
                                    $tag = $this->appclone_model->getTagDetail($tagValue->id);
                                    $tagData = array(
                                        'tag' => $tag->tag,
                                        'appid' =>$lastAppId,
                                        'catalogitemid'=> $lastCatalogId
                                    );

                                    if($tag->catalogitemid!=0){
                                        $this->db->insert('tag', $tagData);
                                    }
                                }
                            }

                            $dest = $this->copyImage($value->id, $lastCatalogId, $value->imageurl);

                                $updatedata = array(
                                    'imageurl' => $dest,
                                );

                                $this->db->where('id', $lastCatalogId);
                                $this->db->update('catalog', $updatedata);

                                    $table='catalog';
                                    $translation = $this->appclone_model->getTranslation($table,$value->id);
                                    if(!empty($translation)){
                                        foreach($translation as $tKey => $tValue){

                                                $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                                    $translationData = array(
                                                        'table' => $tValue->table,
                                                        'tableid' => $lastCatalogId,
                                                        'fieldname' => $tValue->fieldname,
                                                        'language' => $tValue->language,
                                                        'translation' => $tValue->translation
                                                    );
                                                    $this->db->insert('translation', $translationData);
                                            }
                                    }
                    }
                    $i++;
            }
    }

    protected  function __insertVenueCatalog(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $venues = $this->appclone_model->getAppVenues($appId);

            $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);

            $newVenues = array();

            foreach($thisAppVenues as $keym => $valuem){
                $newVenues[] = $valuem;
            }
            $i=0;
            foreach ($venues as $key => $value){

                    $cataloggroup = $this->appclone_model->getVenueCatalogGroup($value);
                    if(count($cataloggroup)>0){
                    foreach($cataloggroup as $gkey=>$gvalue){
                         $catalogGroupData = array(
                            'eventid' => 0,
                            'venueid' => $newVenues[$i],
                            'name' => $gvalue->name,
                            'order' => $gvalue->order
                         );
                         $this->db->insert('cataloggroup', $catalogGroupData);
                         }
                    }
                    $catalogGroupLastId = $this->appclone_model->getLastCatalogGroupId();

                    $catalog = $this->appclone_model->getVenueCatalogDetail($value);
                    foreach($catalog as $key => $value){
                        $catalogData=array(
                                    'eventid' => 0,
                                    'venueid' => $newVenues[$i],
                                    'cataloggroupid' => $catalogGroupLastId,
                                    'name' => $value->name,
                                    'description' => $value->description,
                                    'imageurl' => $value->imageurl,
                                    'order' => $value->order,
                                    'type' => $value->type,
                                    'sourceid' => $value->sourceid,
                                    'url' => $value->url,
                                    'date' => date('Y-m-d')
                            );

                            $this->db->insert('catalog', $catalogData);
                            $lastCatalogId= $this->appclone_model->getLastCatalogId();

                            $dest = $this->copyImage($value->id, $lastCatalogId, $value->imageurl);

                                $updatedata = array(
                                    'imageurl' => $dest,
                                );

                            $this->db->where('id', $lastCatalogId);
                            $this->db->update('catalog', $updatedata);

                            ////////////////////////////////////////////////////////////
                            $groupItem = $this->appclone_model->getGroupItem($value->id);
                            foreach($groupItem as $grkey=>$grvalue){
                                $groupname = $this->appclone_model->getGroupName($grvalue->groupid);
                                $getLastGroupId = $this->appclone_model->getLastExhibitorGroupId($groupname);
                                $groupItemData = array (
                                            'appid' => $lastAppId,
                                            'eventid' => 0,
                                            'venueid' => $newVenues[$i],
                                            'groupid' => $getLastGroupId,
                                            'itemtable' => 'catalog',
                                            'itemid' => $lastCatalogId,
                                            'displaytype' => 0
                                );
                                $this->db->insert('groupitem', $groupItemData);
                            }

                            //////////////////////////////////////////////////////////
                            $tags = $this->appclone_model->getCatalogTags($value->id);
                            if(!empty($tags)){
                                foreach($tags as $tagKey => $tagValue){
                                    if($tagValue->formid > 0)
                                        continue;
                                    $tag = $this->appclone_model->getTagDetail($tagValue->id);
                                    $tagData = array(
                                        'tag' => $tag->tag,
                                        'appid' =>$lastAppId,
                                        'catalogitemid'=> $lastCatalogId
                                    );

                                    if($tag->catalogitemid!=0){
                                        $this->db->insert('tag', $tagData);
                                    }
                                }
                            }

                            $dest = $this->copyImage($value->id, $lastCatalogId, $value->image);

                                $updatedata = array(
                                    'imageurl' => $dest,
                                );

                                $this->db->where('id', $lastCatalogId);
                                $this->db->update('catalog', $updatedata);

                                $table='catalog';
                                $translation = $this->appclone_model->getTranslation($table,$value->id);
                                if(!empty($translation)){
                                    foreach($translation as $tKey => $tValue){

                                            $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                                $translationData = array(
                                                    'table' => $tValue->table,
                                                    'tableid' => $lastCatalogId,
                                                    'fieldname' => $tValue->fieldname,
                                                    'language' => $tValue->language,
                                                    'translation' => $tValue->translation
                                                );
                                                $this->db->insert('translation', $translationData);
                                        }
                                }

                   }$i++;
            }
    }

   /* protected  function __insertEventPoi(){
        $appId = $this->input->post('appid');
        $lastAppId = $this->__getLastAppId();
        $events = $this->appclone_model->getEvents($appId);
        $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

        $newEvents=array();

        foreach($thisAppEvents as $key => $value){
            $newEvents[] = $value;
        }

        $i=0;

        foreach ($events as $key => $value){
                $poi = $this->appclone_model->getPoiDetail($value);
                foreach($poi as $key2=>$value2){
                    $poiData=array(
                                'eventid' => $newEvents[$i],
                                'venueid' => $value2->venueid,
                                'appid' => $lastAppId,
                                'title' => $value2->title,
                                'txt' => $value2->txt,
                                'image' => $value2->image,
                                'url' => $value2->url,
                                'sourceid' => $value2->sourceid,
                                'datum' => $value2->datum,
                                'order' => $value2->order
                    );

                        $this->db->insert('poi', $poiData);
                }
                $i++;
        }
    }*/

    protected  function __insertAppLauncher(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $launchers = $this->appclone_model->getAppLaunchers($appId);
            foreach ($launchers as $key => $value){
                    $launcherid = $value->id;
                    $launcher = $this->appclone_model->getAppLauncherDetail($launcherid);
                    foreach($launcher as $key2 => $value2){
                        if($value2->controller == 'forms')
                            continue;
                        $launcherData=array(
                                    'eventid' => $value2->eventid,
                                    'venueid' => $value2->venueid,
                                    'appid' => $lastAppId,
                                    'moduletypeid' => $value2->moduletypeid,
                                    'module' => $value2->module,
                                    'title' => $value2->title,
                                    'icon' => $value2->icon,
                                    'displaytype' => $value2->displaytype,
                                    'order' => $value2->order,
                                    'url' => $value2->url,
                                    'tag' => $value2->tag,
                                    'active' => $value2->active,
                                    'groupid' => $value2->groupid
                        );

                            $this->db->insert('launcher', $launcherData);
                            $lastLauncherId = $this->appclone_model->getLastLauncherId();
                            $table='launcher';
                            $translation = $this->appclone_model->getTranslation($table,$value2->id);
                            if(!empty($translation)){
                                foreach($translation as $tKey => $tValue){

                                        $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                            $translationData = array(
                                                'table' => $tValue->table,
                                                'tableid' => $lastLauncherId,
                                                'fieldname' => $tValue->fieldname,
                                                'language' => $tValue->language,
                                                'translation' => $tValue->translation
                                            );
                                            $this->db->insert('translation', $translationData);
                                    }
                            }

                    }
                    $i++;
            }
    }

    protected  function __insertVenueLauncher(){

            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $vlaunchers = $this->appclone_model->getAppVenues($appId);

           $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);
            $thisAppVenue = array();

            foreach($thisAppVenues as $key=>$value){
                $thisAppVenue[] = $value;
            }
            $i=0;

            foreach ($vlaunchers as $key => $value){

                    $vlauncherid = $value;
                    $vlauncher = $this->appclone_model->getVenueLaunchers($vlauncherid);

                    foreach($vlauncher as $key2 => $value2){
                        if($value2->controller == 'forms')
                            continue;
                        $vlauncherData=array(
                                    'eventid' => 0,
                                    'venueid' => $thisAppVenues[$i],
                                    'appid' => 0,
                                    'moduletypeid' => $value2->moduletypeid,
                                    'module' => $value2->module,
                                    'title' => $value2->title,
                                    'icon' => $value2->icon,
                                    'displaytype' => $value2->displaytype,
                                    'order' => $value2->order,
                                    'url' => $value2->url,
                                    'tag' => $value2->tag,
                                    'active' => $value2->active,
                                    'groupid' => $value2->groupid
                        );

                            $this->db->insert('launcher', $vlauncherData);
                            $lastLauncherId = $this->appclone_model->getLastLauncherId();
                            $table='launcher';
                            $translation = $this->appclone_model->getTranslation($table,$value2->id);
                            if(!empty($translation)){
                                foreach($translation as $tKey => $tValue){
                                        $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                            $translationData = array(
                                                'table' => $tValue->table,
                                                'tableid' => $lastLauncherId,
                                                'fieldname' => $tValue->fieldname,
                                                'language' => $tValue->language,
                                                'translation' => $tValue->translation
                                            );
                                            $this->db->insert('translation', $translationData);
                                    }
                            }
                    }
                    $i++;
            }
    }

    protected  function __insertEventLauncher(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $elaunchers = $this->appclone_model->getEvents($appId);

            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);
            $thisAppEvent = array();

            foreach($thisAppEvents as $key => $value){
                $thisAppEvent[] = $value;
            }

            $i=0;

            foreach ($elaunchers as $key => $value){
                    $elauncher = $this->appclone_model->getLauncherDetail($value);

                    foreach($elauncher as $key2 => $value2){
                        if($value2->controller == 'forms')
                            continue;

                        $elauncherData=array(
                                    'eventid' => $thisAppEvent[$i],
                                    'venueid' =>0,
                                    'appid' => 0,
                                    'moduletypeid' => $value2->moduletypeid,
                                    'module' => $value2->module,
                                    'title' => $value2->title,
                                    'icon' => $value2->icon,
                                    'displaytype' => $value2->displaytype,
                                    'order' => $value2->order,
                                    'url' => $value2->url,
                                    'tag' => $value2->tag,
                                    'active' => $value2->active,
                                    'groupid' => $value2->groupid
                        );

                            $this->db->insert('launcher', $elauncherData);
                            $lastLauncherId = $this->appclone_model->getLastLauncherId();


                            $table='launcher';
                            $translation = $this->appclone_model->getTranslation($table,$value2->id);
                            if(!empty($translation)){
                                foreach($translation as $tKey => $tValue){
                                        //
                                        $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                            $translationData = array(
                                                'table' => $tValue->table,
                                                'tableid' => $lastLauncherId,
                                                'fieldname' => $tValue->fieldname,
                                                'language' => $tValue->language,
                                                'translation' => $tValue->translation
                                            );
                                            $this->db->insert('translation', $translationData);
                                    }
                            }

                    }
                    $i++;
            }
    }


    protected  function __insertEventFacebook(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){

                    $facebook = $this->appclone_model->getFacebookDetail($value);
                    foreach($facebook as $key2=>$value2){
                        $facebookData=array(
                                    'eventid' => $newEvents[$i],
                                    'venueid' => $value2->venueid,
                                    'json' => $value2->json,
                                    'timestamp' => time()
                        );

                            $this->db->insert('facebook', $facebookData);
                    }
                    $i++;
            }

    }

    protected  function __insertEventFavorites(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){

                    $favorites = $this->appclone_model->getFavoritesDetail($value);
                    foreach($favorites as $key2 => $value2){
                        $favoritesData=array(
                                    'useremail' => $value2->useremail,
                                    'eventid' => $newEvents[$i],
                                    'exhibitorid' => $value2->exhibitorid,
                                    'sessionid' => $value2->sessionid,
                                    'added' => $value2->added,
                                    'notified' => $value2->notified,
                                    'extra' => $value2->extra
                        );

                            $this->db->insert('favorites', $favoritesData);
                    }
                    $i++;
            }
    }

    protected  function __insertEventAds(){
            $appId = $this->input->post('appid');

            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){

                    $ad = $this->appclone_model->getAdDetail($value);
                    foreach($ad as $key2 => $value2){
                        $favoritesData=array(
                                    'eventid' => $newEvents[$i],
                                   'venueid' => $value2->venueid,
                                    'name' => $value2->name,
                                    'image' => $value2->image,
                                    'order' => $value2->order,
                                    'time' => $value2->time,
                                    'website' => $value2->website,
                                    'appid' => $lastAppId
                        );

                            $this->db->insert('ad', $favoritesData);
                            $lastAdId = $this->appclone_model->getLastAdId();

                            if(!empty($value2->image)){
                                $dest = $this->copyImage($value2->id, $lastAdId, $value2->image);
                                    $updatedata = array(
                                        'image' => $dest,
                                    );
                                $this->db->where('id', $lastAdId);
                                $this->db->update('ad', $updatedata);

                            }

                    }
                    $i++;
            }
    }

    protected  function __insertAppGroups(){
            $appId = $this->input->post('appid');

            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){

                    $group = $this->appclone_model->getGroupDetail($value);
                    foreach($group as $key2 => $value2){
                        $favoritesData=array(
                                    'appid' => $lastAppId,
                                    'eventid' => $newEvents[$i],
                                   'venueid' => $value2->venueid,
                                    'name' => $value2->name,
                                    'parentid' => $value2->parentid,
                                    'imageurl' => $value2->imageurl,
                                    'displaytype' => $value2->displaytype,
                                    'tree' => $value2->tree
                        );

                            $this->db->insert('group', $favoritesData);
                            $lastGroupId = $this->appclone_model->getLastGroupId();

                            if(!empty($value2->imageurl)){
                                $dest = $this->copyImage($value2->id, $lastGroupId, $value2->imageurl);
                                    $updatedata = array(
                                        'imageurl' => $dest,
                                    );
                                $this->db->where('id', $lastGroupId);
                                $this->db->update('group', $updatedata);

                            }

                            $table='group';
                            $translation = $this->appclone_model->getTranslation($table,$value2->id);
                            if(!empty($translation)){
                                foreach($translation as $tKey => $tValue){

                                        $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                           $translationData = array(
                                                'table' => $tValue->table,
                                                'tableid' => $lastGroupId,
                                                'fieldname' => $tValue->fieldname,
                                                'language' => $tValue->language,
                                                'translation' => $tValue->translation
                                            );

                                            $this->db->insert('translation', $translationData);
                                    }
                            }

                    }
                    $i++;
            }
    }

    protected  function __insertVenueSocials(){
            $appId = $this->input->post('appid');

            $lastAppId = $this->__getLastAppId();
            $venues = $this->appclone_model->getAppVenues($appId);
            $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);

            $newVenues=array();

            foreach($thisAppVenues as $key => $value){
                $newVenues[] = $value;
            }

            $i=0;

            foreach ($venues as $key => $value){

                    $social = $this->appclone_model->getVenueSocialDetail($value);
                    foreach($social as $key2 => $value2){
                        $socialData=array(
                                    'eventid' => 0,
                                   'venueid' => $newVenues[$i],
                                    'appid' => 0,
                                    'twitter' => $value2->twitter,
                                    'twithash' => $value2->twithash,
                                    'facebookid' => $value2->facebookid,
                                    'RSS' => $value2->RSS,
                                    'facebookappid' => $value2->facebookid,
                                    'postorwall' => $value2->postorwall,
                                    'photostreamurl' => $value2->photostreamurl
                        );

                            $this->db->insert('socialmedia', $socialData);
                    }
                    $i++;
            }
    }

        protected  function __insertEventSocials(){
            $appId = $this->input->post('appid');

            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){

                    $social = $this->appclone_model->getEventSocialDetail($value);


                    foreach($social as $key2 => $value2){
                        $socialData=array(
                                    'eventid' => $newEvents[$i],

                                    'venueid' => 0,
                                    'appid' => 0,
                                    'twitter' => $value2->twitter,
                                    'twithash' => $value2->twithash,
                                    'facebookid' => $value2->facebookid,
                                    'RSS' => $value2->RSS,
                                    'facebookappid' => $value2->facebookid,
                                    'postorwall' => $value2->postorwall,
                                    'photostreamurl' => $value2->photostreamurl
                        );

                            $this->db->insert('socialmedia', $socialData);
                    }
                    $i++;
            }
    }

    protected  function __insertAppSocials(){
            $appId = $this->input->post('appid');

            $lastAppId = $this->__getLastAppId();

            $social = $this->appclone_model->getAppSocialDetail($appId);
            foreach ($social as $key2=>$value2){
                $socialData=array(
                            'eventid' => 0,
                            'venueid' => 0,
                            'appid' => $lastAppId,
                            'twitter' => $value2->twitter,
                            'twithash' => $value2->twithash,
                            'facebookid' => $value2->facebookid,
                            'RSS' => $value2->RSS,
                            'facebookappid' => $value2->facebookid,
                            'postorwall' => $value2->postorwall,
                            'photostreamurl' => $value2->photostreamurl
                );

               $this->db->insert('socialmedia', $socialData);
         }
    }

    protected  function __insertAppAttendees(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){

                    $attendee = $this->appclone_model->getAttendeeDetail($value);

                    foreach($attendee as $key2 => $value2){
                        $attendeeData=array(
                                    'eventid' => $newEvents[$i],

                                    'venueid' => $value2->venueid,
                                    'businessid' => $value2->businessid,
                                    'name' => $value2->name,
                                    'firstname' => $value2->firstname,
                                    'company' => $value2->company,
                                    'function' => $value2->function,
                                    'email' => $value2->email,
                                    'linkedin' => $value2->linkedin,
                                    'phonenr' => $value2->phonenr,
                                    'description' => $value2->description,

                                    'imageurl' => $value2->imageurl
                        );
                        $this->db->insert('attendees', $attendeeData);
                        $lastAttendeeId = $this->appclone_model->getLastAttendeeId();

                        if(!empty($value2->imageurl)){
                            $dest = $this->copyImage($value2->id, $lastAttendeeId, $value2->imageurl);
                                $updatedata = array(
                                    'imageurl' => $dest,
                                );
                            $this->db->where('id', $lastAttendeeId);
                            $this->db->update('attendees', $updatedata);
                        }

                        $table='attendees';
                        $translation = $this->appclone_model->getTranslation($table,$value2->id);
                        if(!empty($translation)){
                            foreach($translation as $tKey => $tValue){

                                    $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                      $translationData = array(
                                            'table' => $tValue->table,
                                            'tableid' => $lastAttendeeId,
                                            'fieldname' => $tValue->fieldname,
                                            'language' => $tValue->language,
                                            'translation' => $tValue->translation
                                        );

                                        $this->db->insert('translation', $translationData);
                                }
                        }


                    }
                    $i++;
            }
    }

    protected  function __insertAppArtists(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $artists = $this->appclone_model->getArtists($appId);

            foreach ($artists as $key => $value){
                    $artist = $this->appclone_model->getArtistDetail($value->id);
                    foreach($artist as $key2 => $value2){
                        $artistData=array(
                                    'appid' => $lastAppId,
                                    'eventid' => 0,
                                   'venueid' => 0,
                                    'name' => $value2->name,
                                    'description' => $value2->description,
                                    'imageurl' => $value2->imageurl,
                                    'order' => $value2->order,

                                    'number' => $value2->number
                        );

                        $this->db->insert('artist', $artistData);
                        $lastArtistId = $this->appclone_model->getLastArtistId();

                        $tags = $this->appclone_model->getArtistTags($value2->id);
                        if(!empty($tags)){
                            foreach($tags as $tagKey => $tagValue){
                                if($tagValue->formid > 0)
                                        continue;
                                $tag = $this->appclone_model->getTagDetail($tagValue->id);
                               $tagData = array(
                                    'tag' => $tag->tag,
                                    'appid' =>$lastAppId,
                                    'artistid'=> $lastArtistId
                                );

                                if($tag->artistid!=0){

                                    $this->db->insert('tag', $tagData);
                                }
                            }
                        }


                        if(!empty($value2->imageurl)){
                            $dest = $this->copyImage($value2->id, $lastArtistId, $value2->imageurl);
                                $updatedata = array(
                                    'imageurl' => $dest,
                                );
                            $this->db->where('id', $lastArtistId);
                            $this->db->update('artist', $updatedata);

                        }

                        $table='artist';
                        $translation = $this->appclone_model->getTranslation($table,$value2->id);
                        if(!empty($translation)){
                            foreach($translation as $tKey => $tValue){

                                    $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                        $translationData = array(
                                            'table' => $tValue->table,
                                            'tableid' => $lastArtistId,
                                            'fieldname' => $tValue->fieldname,
                                            'language' => $tValue->language,
                                            'translation' => $tValue->translation
                                        );

                                        $this->db->insert('translation', $translationData);
                                }
                        }

                    }

            }
    }

    protected  function __insertVenueArtists(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $venues = $this->appclone_model->getAppVenues($appId);
            $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);

            $newVenues=array();

            foreach($thisAppVenues as $key => $value){
                $newVenues[] = $value;
            }

            $i=0;

            foreach ($venues as $key => $value){

                    $artist = $this->appclone_model->getVenueArtistDetail($value);

                    foreach($artist as $key2 => $value2){
                        $artistData=array(
                                    'appid' => 0,
                                    'eventid' => 0,

                                    'venueid' => $newVenues[$i],
                                    'name' => $value2->name,
                                    'description' => $value2->description,
                                    'imageurl' => $value2->imageurl,
                                    'order' => $value2->order,

                                    'number' => $value2->number
                        );
                        $this->db->insert('artist', $artistData);
                        $lastArtistId = $this->appclone_model->getLastArtistId();

                        $tags = $this->appclone_model->getArtistTags($value2->id);
                        if(!empty($tags)){
                            foreach($tags as $tagKey => $tagValue){
                                if($tagValue->formid > 0)
                                        continue;
                                $tag = $this->appclone_model->getTagDetail($tagValue->id);
                                $tagData = array(
                                    'tag' => $tag->tag,
                                    'appid' =>$lastAppId,
                                    'artistid'=> $lastArtistId
                                );

                                if($tag->artistid!=0){

                                    $this->db->insert('tag', $tagData);
                                }
                            }
                        }


                        if(!empty($value2->imageurl)){
                            $dest = $this->copyImage($value2->id, $lastArtistId, $value2->imageurl);
                                $updatedata = array(
                                    'imageurl' => $dest,
                                );
                            $this->db->where('id', $lastArtistId);
                            $this->db->update('artist', $updatedata);

                        }

                        $table='artist';
                        $translation = $this->appclone_model->getTranslation($table,$value2->id);
                        if(!empty($translation)){
                            foreach($translation as $tKey => $tValue){

                                    $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                        $translationData = array(
                                            'table' => $tValue->table,
                                            'tableid' => $lastArtistId,
                                            'fieldname' => $tValue->fieldname,
                                            'language' => $tValue->language,
                                            'translation' => $tValue->translation
                                        );

                                        $this->db->insert('translation', $translationData);
                                }
                        }

                    }
                    $i++;
            }
    }

    protected  function __insertAppSchedule(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){

                    $schedule = $this->appclone_model->getScheduleDetail($value);

                    foreach($schedule as $key2 => $value2){
                        $scheduleData=array(
                                    'eventid' => $newEvents[$i],
                                   'venueid' => $value2->venueid,
                                    'date' => $value2->date,
                                    'key' => $value2->key,
                                    'caption' => $value2->caption
                        );
                        $this->db->insert('schedule', $scheduleData);
                    }
                    $i++;
            }

    }

     protected  function __insertVenueSchedule(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $venues = $this->appclone_model->getAppVenues($appId);

            foreach ($venues as $key => $value){

                    $vSchedule = $this->appclone_model->getVenueScheduleDetail($value);
                    $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);

                    $newVenue=array();
                    foreach($thisAppVenues as $vkey=>$vvalue){
                        $newVenue[]=$vvalue;
                    }
                    $i=0;
                    foreach($vSchedule as $key2 => $value2){
                        $scheduleData=array(

                                    'eventid' => 0,
                                   'venueid' => $newVenue[$i],
                                    'date' => date('Y-m-d'),
                                    'key' => $value2->key,
                                    'caption' => $value2->caption
                        );
                        $this->db->insert('schedule', $scheduleData);
                    }
                    $i++;
            }
    }

     protected  function __insertEventExhibitorBrand(){
            $appId = $this->input->post('appid');

            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);

            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvent= array();
            foreach($thisAppEvents as $ekey=>$evalue){
                $newEvent[] = $evalue;
            }


            foreach ($events as $key => $value){
                    $vexhibitor = $this->appclone_model->getVenueExhibitorBrands($value);
                    $i=0;
                    foreach($vexhibitor as $key2 => $value2){
                        $exhibitorBrandData=array(
                                    'eventid' => $newEvent[$i],
                                   'venueid' => 0,
                                    'name' => $value2->name
                        );
                        if(count($vexhibitor)>0){
                        $this->db->insert('exhibitorbrand', $exhibitorBrandData);
                        }
                        $i++;
                    }
            }
    }

     protected  function __insertEventExhibitorCategory(){
            $appId = $this->input->post('appid');

            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);

            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);
            $newEvent= array();

            foreach($thisAppEvents as $ekey=>$evalue){
                $newEvent[] = $evalue;
            }

            foreach ($events as $key => $value){

                    $vexhibitor = $this->appclone_model->getVenueExhibitorCategory($value);

                    $i=0;
                    foreach($vexhibitor as $key2 => $value2){
                        $exhibitorCategoryData=array(
                                    'eventid' => $newEvent[$i],
                                   'venueid' => 0,
                                    'name' => $value2->name
                        );
                        if(count($vexhibitor)>0){
                        $this->db->insert('exhibitorcategory', $exhibitorCategoryData);
                        }
                        $i++;
                    }
            }
    }

     protected  function __insertVenueExhibitorBrand(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $vlaunchers = $this->appclone_model->getAppVenues($appId);

            foreach ($vlaunchers as $key => $value){

                    $vexhibitor = $this->appclone_model->getVenueExhibitorBrands($value);

                    $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);

                    $newVenue= array();
                    foreach($thisAppVenues as $vkey=>$vvalue){
                        $newVenue[] = $vvalue;
                    }

                    $i=0;
                    foreach($vexhibitor as $key2 => $value2){
                        $exhibitorBrandData=array(
                                    'eventid' => 0,
                                    'venueid' => $newVenue[$i],
                                    'name' => $value2->name
                        );

                        if(count($vexhibitor)>0){
                        $this->db->insert('exhibitorbrand', $exhibitorBrandData);
                        }
                        $i++;
                    }
            }
    }

     protected  function __insertVenueExhibitorCategory(){
            $appId = $this->input->post('appid');

            $lastAppId = $this->__getLastAppId();
            $vlaunchers = $this->appclone_model->getAppVenues($appId);

            foreach ($vlaunchers as $key => $value){

                    $vexhibitor = $this->appclone_model->getVenueExhibitorCategory($value);

                    $thisAppVenues = $this->appclone_model->getAppVenues($lastAppId);


                    $newVenue= array();
                    foreach($thisAppVenues as $vkey=>$vvalue){
                        $newVenue[] = $vvalue;
                    }


                    $i=0;
                    foreach($vexhibitor as $key2 => $value2){
                        $exhibitorCategoryData=array(
                                    'eventid' => 0,
                                   'venueid' => $newVenue[$i],
                                    'name' => $value2->name
                        );
                        if(count($vexhibitor)>0){
                        $this->db->insert('exhibitorcategory', $exhibitorCategoryData);
                        }
                        $i++;
                    }
            }
    }


    protected  function __insertAppSessionGroups(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){

                    $sessionGroup = $this->appclone_model->getSessionGroupDetail($value);

                    foreach($sessionGroup as $key2 => $value2){
                        $sessionGroupData=array(
                                    'eventid' => $newEvents[$i],
                                   'venueid' => $value2->venueid,
                                    'name' => $value2->name,
                                    'order' => $value2->order
                        );
                        $this->db->insert('sessiongroup', $sessionGroupData);
                        $lastSessionGroupId = $this->__getLastSessionGroupId();


                        $tags = $this->appclone_model->getSessionGroupTags($value2->id);
                        if(!empty($tags)){
                            foreach($tags as $tagKey => $tagValue){
                                if($tagValue->formid > 0)
                                        continue;
                                $tag = $this->appclone_model->getTagDetail($tagValue->id);

                                $tagData = array(
                                    'tag' => $tag->tag,
                                    'appid' =>$lastAppId,
                                    'sessiongroupid'=> $lastArtistId
                                );

                                if($tag->sessiongroupid!=0){

                                    $this->db->insert('tag', $tagData);
                                }
                            }
                        }


                        $updatedata = array(
                            'sessiongroupid' => $lastSessionGroupId,
                        );

                        $this->db->where('sessiongroupid', $value2->id);
                        $this->db->where('eventid', $newEvents[$i]);
                        $this->db->update('session', $updatedata);

                        $table='sessiongroup';
                        $translation = $this->appclone_model->getTranslation($table,$value2->id);
                        if(!empty($translation)){
                            foreach($translation as $tKey => $tValue){

                                    $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                      $translationData = array(
                                            'table' => $tValue->table,
                                            'tableid' => $lastSessionGroupId,
                                            'fieldname' => $tValue->fieldname,
                                            'language' => $tValue->language,
                                            'translation' => $tValue->translation
                                        );

                                        $this->db->insert('translation', $translationData);
                                }
                        }
                    }
                    $i++;
            }
    }

   protected function __insertAppPushNotifications($appId) {
        $appId = $this->__getAppId();
        $stores = $this->appclone_model->getAppPushNotifications($appId);
        $lastAppId = $this->__getLastAppId();

        count($stores);
        foreach($stores as $key => $value){
            $data = array(
                'appid' => $lastAppId,
                'token' => $value->token
            );
            if (count($stores) > 0) {
                $this->db->insert('push', $data);
            }

        }
    }

    protected function __insertAppLanguages($appId) {
        $appId = $this->__getAppId();
        $languages = $this->appclone_model->getAppLanguages($appId);
        $lastAppId = $this->__getLastAppId();

        foreach($languages as $key => $value){
            $data = array(
                'appid' => $lastAppId,
                'language' => $value->language
            );
            if (count($languages) > 0) {
                $this->db->insert('applanguage', $data);

            }

        }
    }

    protected function __insertEventBrands($value,$lastEventId,$lastAppId){

                    $groupBrand = $this->appclone_model->getBrandGroupDetail($value);
                    $groupBrandChildren1 = $this->appclone_model->get_list($groupBrand['id']);
                    $groupData=array(
                                'appid' => $lastAppId,
                                'eventid' => $lastEventId,
                                'venueid' => 0,
                                'name' => $groupBrand['name'],
                                'parentid' => 0,
                                'imageurl' => $groupBrand['imageurl'],
                                'displaytype' => $groupBrand['displaytype'],
                                'tree' => $groupBrand['tree']
                        );
                    if(!empty($groupBrand['id'])){
                        $this->db->insert('group', $groupData);
                    }
                    $lastGroupId1 = $this->appclone_model->getLastGroupId();
                        if(count($groupBrandChildren1) > 0 && !empty($groupBrand['id'])){
                            foreach($groupBrandChildren1 as $key1 => $value1){
                                $groupData1=array(
                                        'appid' => $lastAppId,
                                        'eventid' => $lastEventId,
                                        'venueid' => 0,
                                        'name' => $value1->name,
                                        'parentid' => $lastGroupId1,
                                        'imageurl' => $value1->imageurl,
                                        'displaytype' => $value1->displaytype,
                                        'tree' => $value1->tree
                                );
                                $this->db->insert('group', $groupData1);
                            $lastGroupId2 = $this->appclone_model->getLastGroupId();
                                $groupBrandChildren2 = $this->appclone_model->get_list($value1->id);

                                if(count($groupBrandChildren2) > 0){
                                    foreach($groupBrandChildren2 as $key2 => $value2){
                                        $groupData2=array(
                                                'appid' => $lastAppId,
                                                'eventid' => $lastEventId,
                                                'venueid' => 0,
                                                'name' => $value2->name,
                                                'parentid' => $lastGroupId2,
                                                'imageurl' => $value2->imageurl,
                                                'displaytype' => $value2->displaytype,
                                                'tree' => $value2->tree
                                        );
                                        $this->db->insert('group', $groupData2);
                                        $lastGroupId3 = $this->appclone_model->getLastGroupId();
                                        $groupBrandChildren3 = $this->appclone_model->get_list($value2->id);

                                        if(count($groupBrandChildren3) > 0){
                                            foreach($groupBrandChildren3 as $key3 => $value3){
                                                $groupData3=array(
                                                        'appid' => $lastAppId,
                                                      'eventid' => $lastEventId,
                                                       'venueid' => 0,
                                                        'name' => $value3->name,
                                                        'parentid' => $lastGroupId3,
                                                        'imageurl' => $value3->imageurl,
                                                        'displaytype' => $value3->displaytype,
                                                        'tree' => $value3->tree
                                                );
                                                $this->db->insert('group', $groupData3);
                                                $lastGroupId4 = $this->appclone_model->getLastGroupId();
                                                $groupBrandChildren4 = $this->appclone_model->get_list($value3->id);

                                                if(count($groupBrandChildren4) > 0){
                                                    foreach($groupBrandChildren4 as $key4 => $value4){
                                                        $groupData4=array(
                                                                'appid' => $lastAppId,
                                                                'eventid' => $lastEventId,
                                                                'venueid' => 0,
                                                                'name' => $value4->name,
                                                                'parentid' => $lastGroupId4,
                                                                'imageurl' => $value4->imageurl,
                                                                'displaytype' => $value4->displaytype,
                                                                'tree' => $value4->tree
                                                        );
                                                        $this->db->insert('group', $groupData4);
                                                        $lastGroupId5 = $this->appclone_model->getLastGroupId();
                                                        $groupBrandChildren5 = $this->appclone_model->get_list($value4->id);

                                                        if(count($groupBrandChildren5) > 0){
                                                            foreach($groupBrandChildren5 as $key5 => $value5){
                                                                $groupData5=array(
                                                                        'appid' => $lastAppId,
                                                                        'eventid' => $lastEventId,
                                                                        'venueid' => 0,
                                                                        'name' => $value5->name,
                                                                        'parentid' => $lastGroupId5,
                                                                        'imageurl' => $value5->imageurl,
                                                                        'displaytype' => $value5->displaytype,
                                                                        'tree' => $value5->tree
                                                                );
                                                                $this->db->insert('group', $groupData5);
                                                                $lastGroupId6 = $this->appclone_model->getLastGroupId();
                                                                $groupBrandChildren6 = $this->appclone_model->get_list($value5->id);

                                                                if(count($groupBrandChildren6) > 0){
                                                                    foreach($groupBrandChildren6 as $key6 => $value6){
                                                                        $groupData6=array(
                                                                                'appid' => $lastAppId,
                                                                                'eventid' => $lastEventId,
                                                                                'venueid' => 0,
                                                                                'name' => $value6->name,
                                                                                'parentid' => $lastGroupId6,
                                                                                'imageurl' => $value6->imageurl,
                                                                                'displaytype' => $value6->displaytype,
                                                                                'tree' => $value6->tree
                                                                        );
                                                                        $this->db->insert('group', $groupData6);
                                                                        $lastGroupId7 = $this->appclone_model->getLastGroupId();
                                                                        $groupBrandChildren7 = $this->appclone_model->get_list($value6->id);

                                                                        if(count($groupBrandChildren7) > 0){
                                                                            foreach($groupBrandChildren7 as $key7 => $value7){
                                                                                $groupData7=array(
                                                                                        'appid' => $lastAppId,
                                                                                        'eventid' => $lastEventId,
                                                                                        'venueid' => 0,
                                                                                        'name' => $value7->name,
                                                                                        'parentid' => $lastGroupId7,
                                                                                        'imageurl' => $value7->imageurl,
                                                                                        'displaytype' => $value7->displaytype,
                                                                                        'tree' => $value7->tree
                                                                                );
                                                                                $this->db->insert('group', $groupData7);
                                                                                $lastGroupId8 = $this->appclone_model->getLastGroupId();
                                                                                $groupBrandChildren8 = $this->appclone_model->get_list($value7->id);

                                                                                if(count($groupBrandChildren8) > 0){
                                                                                    foreach($groupBrandChildren8 as $key8 => $value8){
                                                                                        $groupData8=array(
                                                                                                'appid' => $lastAppId,

                                                                                                'eventid' => $lastEventId,

                                                                                                'venueid' => 0,
                                                                                                'name' => $value8->name,
                                                                                                'parentid' => $lastGroupId8,
                                                                                                'imageurl' => $value8->imageurl,
                                                                                                'displaytype' => $value8->displaytype,
                                                                                                'tree' => $value8->tree
                                                                                        );
                                                                                        $this->db->insert('group', $groupData8);
                                                                                        $lastGroupId9 = $this->appclone_model->getLastGroupId();
                                                                                        $groupBrandChildren9 = $this->appclone_model->get_list($value8->id);

                                                                                        if(count($groupBrandChildren9) > 0){
                                                                                            foreach($groupBrandChildren9 as $key9 => $value9){
                                                                                                $groupData9=array(
                                                                                                        'appid' => $lastAppId,

                                                                                                        'eventid' => $lastEventId,

                                                                                                        'venueid' => 0,
                                                                                                        'name' => $value9->name,
                                                                                                        'parentid' => $lastGroupId9,
                                                                                                        'imageurl' => $value9->imageurl,
                                                                                                        'displaytype' => $value9->displaytype,
                                                                                                        'tree' => $value9->tree
                                                                                                );
                                                                                                $this->db->insert('group', $groupData9);
                                                                                                $lastGroupId10 = $this->appclone_model->getLastGroupId();
                                                                                                $groupBrandChildren10 = $this->appclone_model->get_list($value9->id);

                                                                                                if(count($groupBrandChildren10) > 0){
                                                                                                    foreach($groupBrandChildren10 as $key10 => $value10){
                                                                                                        $groupData10=array(
                                                                                                                'appid' => $lastAppId,

                                                                                                                'eventid' => $lastEventId,
                                                                                                              'venueid' => 0,
                                                                                                                'name' => $value10->name,
                                                                                                                'parentid' => $lastGroupId10,
                                                                                                                'imageurl' => $value10->imageurl,
                                                                                                                'displaytype' => $value10->displaytype,
                                                                                                                'tree' => $value10->tree
                                                                                                        );
                                                                                                        $this->db->insert('group', $groupData10);
                                                                                                        $lastGroupId11 = $this->appclone_model->getLastGroupId();
                                                                                                        $groupBrandChildren11 = $this->appclone_model->get_list($value10->id);


                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                    }
                                                                                }

                                                                            }
                                                                        }

                                                                    }
                                                                }

                                                            }
                                                        }

                                                    }
                                                }

                                            }
                                        }

                                    }
                                }


                            }
                        }

    }

    protected function __insertEventCategories($value,$lastEventId,$lastAppId){
                    $groupCategory = $this->appclone_model->getCategoryGroupDetail($value);

                    $groupCategoryChildren1 = $this->appclone_model->get_list($groupCategory['id']);

                    $groupCatData=array(
                                'appid' => $lastAppId,

                                'eventid' => $lastEventId,
                                'venueid' => 0,
                                'name' => $groupCategory['name'],
                                'parentid' => 0,
                                'imageurl' => $groupCategory['imageurl'],
                                'displaytype' => $groupCategory['displaytype'],
                                'tree' => $groupCategory['tree']
                        );
                    if(!empty($groupCategory['id'])){
                        $this->db->insert('group', $groupCatData);
                    }
                    $lastCatGroupId1 = $this->appclone_model->getLastGroupId();

                        if(count($groupCategoryChildren1) > 0 && !empty($groupCategory['id'])){
                            foreach($groupCategoryChildren1 as $key1 => $value1){
                                $groupCatData1=array(
                                        'appid' => $lastAppId,
                                        'eventid' => $lastEventId,
                                        'venueid' => 0,
                                        'name' => $value1->name,
                                        'parentid' => $lastCatGroupId1,
                                        'imageurl' => $value1->imageurl,
                                        'displaytype' => $value1->displaytype,
                                        'tree' => $value1->tree
                                );
                                $this->db->insert('group', $groupCatData1);

                                $lastCatGroupId2 = $this->appclone_model->getLastGroupId();
                                $groupCategoryChildren2 = $this->appclone_model->get_list($value1->id);

                                if(count($groupCategoryChildren2) > 0){
                                    foreach($groupCategoryChildren2 as $key2 => $value2){
                                        $groupCatData2=array(
                                                'appid' => $lastAppId,
                                                'eventid' => $lastEventId,
                                                'venueid' => 0,
                                                'name' => $value2->name,
                                                'parentid' => $lastCatGroupId2,
                                                'imageurl' => $value2->imageurl,
                                                'displaytype' => $value2->displaytype,
                                                'tree' => $value2->tree
                                        );
                                        $this->db->insert('group', $groupCatData2);
                                        $lastCatGroupId3 = $this->appclone_model->getLastGroupId();
                                        $groupCategoryChildren3 = $this->appclone_model->get_list($value2->id);

                                        if(count($groupCategoryChildren3) > 0){
                                            foreach($groupCategoryChildren3 as $key3 => $value3){
                                                $groupCatData3=array(
                                                        'appid' => $lastAppId,

                                                        'eventid' => $lastEventId,

                                                        'venueid' => 0,
                                                        'name' => $value3->name,
                                                        'parentid' => $lastCatGroupId3,
                                                        'imageurl' => $value3->imageurl,
                                                        'displaytype' => $value3->displaytype,
                                                        'tree' => $value3->tree
                                                );
                                                $this->db->insert('group', $groupCatData3);
                                                $lastCatGroupId4 = $this->appclone_model->getLastGroupId();
                                                $groupCategoryChildren4 = $this->appclone_model->get_list($value3->id);

                                                if(count($groupCategoryChildren4) > 0){
                                                    foreach($groupCategoryChildren4 as $key4 => $value4){
                                                        $groupCatData4=array(
                                                                'appid' => $lastAppId,

                                                                'eventid' => $lastEventId,

                                                                'venueid' => 0,
                                                                'name' => $value4->name,
                                                                'parentid' => $lastCatGroupId4,
                                                                'imageurl' => $value4->imageurl,
                                                                'displaytype' => $value4->displaytype,
                                                                'tree' => $value4->tree
                                                        );
                                                        $this->db->insert('group', $groupCatData4);
                                                        $lastCatGroupId5 = $this->appclone_model->getLastGroupId();
                                                        $groupCategoryChildren5 = $this->appclone_model->get_list($value4->id);

                                                        if(count($groupCategoryChildren5) > 0){
                                                            foreach($groupCategoryChildren5 as $key5 => $value5){
                                                                $groupCatData5=array(
                                                                        'appid' => $lastAppId,
                                                                        'eventid' => $lastEventId,
                                                                        'venueid' => 0,
                                                                        'name' => $value5->name,
                                                                        'parentid' => $lastCatGroupId5,
                                                                        'imageurl' => $value5->imageurl,
                                                                        'displaytype' => $value5->displaytype,
                                                                        'tree' => $value5->tree
                                                                );
                                                                $this->db->insert('group', $groupCatData5);
                                                                $lastCatGroupId6 = $this->appclone_model->getLastGroupId();
                                                                $groupCategoryChildren6 = $this->appclone_model->get_list($value5->id);

                                                                if(count($groupCategoryChildren6) > 0){
                                                                    foreach($groupCategoryChildren6 as $key6 => $value6){
                                                                        $groupCatData6=array(
                                                                                'appid' => $lastAppId,

                                                                                'eventid' => $lastEventId,

                                                                                'venueid' => 0,
                                                                                'name' => $value6->name,
                                                                                'parentid' => $lastCatGroupId6,
                                                                                'imageurl' => $value6->imageurl,
                                                                                'displaytype' => $value6->displaytype,
                                                                                'tree' => $value6->tree
                                                                        );
                                                                        $this->db->insert('group', $groupCatData6);
                                                                        $lastCatGroupId7 = $this->appclone_model->getLastGroupId();
                                                                        $groupCategoryChildren7 = $this->appclone_model->get_list($value6->id);

                                                                        if(count($groupCategoryChildren7) > 0){
                                                                            foreach($groupCategoryChildren7 as $key7 => $value7){
                                                                                $groupCatData7=array(
                                                                                        'appid' => $lastAppId,

                                                                                        'eventid' => $lastEventId,

                                                                                        'venueid' => 0,
                                                                                        'name' => $value7->name,
                                                                                        'parentid' => $lastCatGroupId7,
                                                                                        'imageurl' => $value7->imageurl,
                                                                                        'displaytype' => $value7->displaytype,
                                                                                        'tree' => $value7->tree
                                                                                );
                                                                                $this->db->insert('group', $groupCatData7);
                                                                                $lastCatGroupId8 = $this->appclone_model->getLastGroupId();
                                                                                $groupCategoryChildren8 = $this->appclone_model->get_list($value7->id);

                                                                                if(count($groupCategoryChildren8) > 0){
                                                                                    foreach($groupCategoryChildren8 as $key8 => $value8){
                                                                                        $groupCatData8=array(
                                                                                                'appid' => $lastAppId,

                                                                                                'eventid' => $lastEventId,

                                                                                                'venueid' => 0,
                                                                                                'name' => $value8->name,
                                                                                                'parentid' => $lastCatGroupId8,
                                                                                                'imageurl' => $value8->imageurl,
                                                                                                'displaytype' => $value8->displaytype,
                                                                                                'tree' => $value8->tree
                                                                                        );
                                                                                        $this->db->insert('group', $groupCatData8);
                                                                                        $lastCatGroupId9 = $this->appclone_model->getLastGroupId();
                                                                                        $groupCategoryChildren9 = $this->appclone_model->get_list($value8->id);

                                                                                        if(count($groupCategoryChildren9) > 0){
                                                                                            foreach($groupCategoryChildren9 as $key9 => $value9){
                                                                                                $groupCatData9=array(
                                                                                                        'appid' => $lastAppId,

                                                                                                        'eventid' => $lastEventId,

                                                                                                        'venueid' => 0,
                                                                                                        'name' => $value9->name,
                                                                                                        'parentid' => $lastCatGroupId9,
                                                                                                        'imageurl' => $value9->imageurl,
                                                                                                        'displaytype' => $value9->displaytype,
                                                                                                        'tree' => $value9->tree
                                                                                                );
                                                                                                $this->db->insert('group', $groupCatData9);
                                                                                                $lastCatGroupId10 = $this->appclone_model->getLastGroupId();
                                                                                                $groupCategoryChildren10 = $this->appclone_model->get_list($value9->id);

                                                                                                if(count($groupCategoryChildren10) > 0){
                                                                                                    foreach($groupCategoryChildren10 as $key10 => $value10){
                                                                                                        $groupCatData10=array(
                                                                                                                'appid' => $lastAppId,

                                                                                                                'eventid' => $lastEventId,

                                                                                                                'venueid' => 0,
                                                                                                                'name' => $value10->name,
                                                                                                                'parentid' => $lastCatGroupId10,
                                                                                                                'imageurl' => $value10->imageurl,
                                                                                                                'displaytype' => $value10->displaytype,
                                                                                                                'tree' => $value10->tree
                                                                                                        );
                                                                                                        $this->db->insert('group', $groupCatData10);
                                                                                                        $lastCatGroupId11 = $this->appclone_model->getLastGroupId();
                                                                                                        $groupCategoryChildren11 = $this->appclone_model->get_list($value10->id);


                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                    }
                                                                                }

                                                                            }
                                                                        }

                                                                    }
                                                                }

                                                            }
                                                        }

                                                    }
                                                }

                                            }
                                        }

                                    }
                                }


                            }
                        }

    }

    protected function __insertCatalogVenueCategories($value,$lastVenueId,$lastAppId){
		            $groupCategory = $this->appclone_model->getVenueCategoryGroupDetail($value);
                    $groupCategoryChildren1 = $this->appclone_model->get_list($groupCategory['id']);

                    $groupCatData=array(
                                'appid' => $lastAppId,

                                'eventid' => 0,

                                'venueid' => $lastVenueId,
                                'name' => $groupCategory['name'],
                                'parentid' => 0,
                                'imageurl' => $groupCategory['imageurl'],
                                'displaytype' => $groupCategory['displaytype'],
                                'tree' => $groupCategory['tree']
                        );
                    if(!empty($groupCategory['id'])){
                        $this->db->insert('group', $groupCatData);
                    }
                    $lastCatGroupId1 = $this->appclone_model->getLastGroupId();

                        if(count($groupCategoryChildren1) > 0 && !empty($groupCategory['id'])){
                            foreach($groupCategoryChildren1 as $key1 => $value1){
                                $groupCatData1=array(
                                        'appid' => $lastAppId,

                                        'eventid' => 0,

                                        'venueid' => $lastVenueId,
                                        'name' => $value1->name,
                                        'parentid' => $lastCatGroupId1,
                                        'imageurl' => $value1->imageurl,
                                        'displaytype' => $value1->displaytype,
                                        'tree' => $value1->tree
                                );
                                $this->db->insert('group', $groupCatData1);

                                $lastCatGroupId2 = $this->appclone_model->getLastGroupId();
                                $groupCategoryChildren2 = $this->appclone_model->get_list($value1->id);

                                if(count($groupCategoryChildren2) > 0){
                                    foreach($groupCategoryChildren2 as $key2 => $value2){
                                        $groupCatData2=array(
                                                'appid' => $lastAppId,

                                                'eventid' => 0,
                                                'venueid' => $lastVenueId,
                                                'name' => $value2->name,
                                                'parentid' => $lastCatGroupId2,
                                                'imageurl' => $value2->imageurl,
                                                'displaytype' => $value2->displaytype,
                                                'tree' => $value2->tree
                                        );
                                        $this->db->insert('group', $groupCatData2);
                                        $lastCatGroupId3 = $this->appclone_model->getLastGroupId();
                                        $groupCategoryChildren3 = $this->appclone_model->get_list($value2->id);

                                        if(count($groupCategoryChildren3) > 0){
                                            foreach($groupCategoryChildren3 as $key3 => $value3){
                                                $groupCatData3=array(
                                                        'appid' => $lastAppId,

                                                        'eventid' => 0,
                                                        'venueid' => $lastVenueId,
                                                        'name' => $value3->name,
                                                        'parentid' => $lastCatGroupId3,
                                                        'imageurl' => $value3->imageurl,
                                                        'displaytype' => $value3->displaytype,
                                                        'tree' => $value3->tree
                                                );
                                                $this->db->insert('group', $groupCatData3);
                                                $lastCatGroupId4 = $this->appclone_model->getLastGroupId();
                                                $groupCategoryChildren4 = $this->appclone_model->get_list($value3->id);

                                                if(count($groupCategoryChildren4) > 0){
                                                    foreach($groupCategoryChildren4 as $key4 => $value4){
                                                        $groupCatData4=array(
                                                                'appid' => $lastAppId,

                                                                'eventid' => 0,
                                                                'venueid' => $lastVenueId,
                                                                'name' => $value4->name,
                                                                'parentid' => $lastCatGroupId4,
                                                                'imageurl' => $value4->imageurl,
                                                                'displaytype' => $value4->displaytype,
                                                                'tree' => $value4->tree
                                                        );
                                                        $this->db->insert('group', $groupCatData4);
                                                        $lastCatGroupId5 = $this->appclone_model->getLastGroupId();
                                                        $groupCategoryChildren5 = $this->appclone_model->get_list($value4->id);

                                                        if(count($groupCategoryChildren5) > 0){
                                                            foreach($groupCategoryChildren5 as $key5 => $value5){
                                                                $groupCatData5=array(
                                                                        'appid' => $lastAppId,

                                                                        'eventid' => 0,
                                                                        'venueid' => $lastVenueId,
                                                                        'name' => $value5->name,
                                                                        'parentid' => $lastCatGroupId5,
                                                                        'imageurl' => $value5->imageurl,
                                                                        'displaytype' => $value5->displaytype,
                                                                        'tree' => $value5->tree
                                                                );
                                                                $this->db->insert('group', $groupCatData5);
                                                                $lastCatGroupId6 = $this->appclone_model->getLastGroupId();
                                                                $groupCategoryChildren6 = $this->appclone_model->get_list($value5->id);

                                                                if(count($groupCategoryChildren6) > 0){
                                                                    foreach($groupCategoryChildren6 as $key6 => $value6){
                                                                        $groupCatData6=array(
                                                                                'appid' => $lastAppId,

                                                                                'eventid' => 0,
                                                                                'venueid' => $lastVenueId,
                                                                                'name' => $value6->name,
                                                                                'parentid' => $lastCatGroupId6,
                                                                                'imageurl' => $value6->imageurl,
                                                                                'displaytype' => $value6->displaytype,
                                                                                'tree' => $value6->tree
                                                                        );
                                                                        $this->db->insert('group', $groupCatData6);
                                                                        $lastCatGroupId7 = $this->appclone_model->getLastGroupId();
                                                                        $groupCategoryChildren7 = $this->appclone_model->get_list($value6->id);

                                                                        if(count($groupCategoryChildren7) > 0){
                                                                            foreach($groupCategoryChildren7 as $key7 => $value7){
                                                                                $groupCatData7=array(
                                                                                        'appid' => $lastAppId,
                                                                                        'eventid' => 0,
                                                                                        'venueid' => $lastVenueId,
                                                                                        'name' => $value7->name,
                                                                                        'parentid' => $lastCatGroupId7,
                                                                                        'imageurl' => $value7->imageurl,
                                                                                        'displaytype' => $value7->displaytype,
                                                                                        'tree' => $value7->tree
                                                                                );
                                                                                $this->db->insert('group', $groupCatData7);
                                                                                $lastCatGroupId8 = $this->appclone_model->getLastGroupId();
                                                                                $groupCategoryChildren8 = $this->appclone_model->get_list($value7->id);

                                                                                if(count($groupCategoryChildren8) > 0){
                                                                                    foreach($groupCategoryChildren8 as $key8 => $value8){
                                                                                        $groupCatData8=array(
                                                                                                'appid' => $lastAppId,
                                                                                                'eventid' => 0,
                                                                                                'venueid' => $lastVenueId,
                                                                                                'name' => $value8->name,
                                                                                                'parentid' => $lastCatGroupId8,
                                                                                                'imageurl' => $value8->imageurl,
                                                                                                'displaytype' => $value8->displaytype,
                                                                                                'tree' => $value8->tree
                                                                                        );
                                                                                        $this->db->insert('group', $groupCatData8);
                                                                                        $lastCatGroupId9 = $this->appclone_model->getLastGroupId();
                                                                                        $groupCategoryChildren9 = $this->appclone_model->get_list($value8->id);

                                                                                        if(count($groupCategoryChildren9) > 0){
                                                                                            foreach($groupCategoryChildren9 as $key9 => $value9){
                                                                                                $groupCatData9=array(
                                                                                                        'appid' => $lastAppId,
                                                                                                        'eventid' => 0,
                                                                                                        'venueid' => $lastVenueId,
                                                                                                        'name' => $value9->name,
                                                                                                        'parentid' => $lastCatGroupId9,
                                                                                                        'imageurl' => $value9->imageurl,
                                                                                                        'displaytype' => $value9->displaytype,
                                                                                                        'tree' => $value9->tree
                                                                                                );
                                                                                                $this->db->insert('group', $groupCatData9);
                                                                                                $lastCatGroupId10 = $this->appclone_model->getLastGroupId();
                                                                                                $groupCategoryChildren10 = $this->appclone_model->get_list($value9->id);

                                                                                                if(count($groupCategoryChildren10) > 0){
                                                                                                    foreach($groupCategoryChildren10 as $key10 => $value10){
                                                                                                        $groupCatData10=array(
                                                                                                                'appid' => $lastAppId,
                                                                                                                'eventid' => 0,
                                                                                                                'venueid' => $lastVenueId,
                                                                                                                'name' => $value10->name,
                                                                                                                'parentid' => $lastCatGroupId10,
                                                                                                                'imageurl' => $value10->imageurl,
                                                                                                                'displaytype' => $value10->displaytype,
                                                                                                                'tree' => $value10->tree
                                                                                                        );
                                                                                                        $this->db->insert('group', $groupCatData10);
                                                                                                        $lastCatGroupId11 = $this->appclone_model->getLastGroupId();
                                                                                                        $groupCategoryChildren11 = $this->appclone_model->get_list($value10->id);

                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                    }
                                                                                }

                                                                            }
                                                                        }

                                                                    }
                                                                }

                                                            }
                                                        }

                                                    }
                                                }

                                            }
                                        }

                                    }
                                }


                            }
                        }

    }

    function __insertContentModule(){
        $appId = $this->__getAppId();
        $modules = $this->appclone_model->getContentModule($appId);
        $lastAppId = $this->__getLastAppId();
        foreach($modules as $modkey=>$modvalue){
            $moduleData =array (
                'appid'=>$lastAppId,
                'title'=>$modvalue->title,
                'icon'=>$modvalue->icon,
                'homemodule'=>$modvalue->homemodule,
                'hideinmenu'=>$modvalue->hideinmenu,
                'order'=>$modvalue->order
            );
            $this->db->insert('contentmodule',$moduleData);
            $lastContentModuleId=$this->appclone_model->getLastContentModuleId();

            if(!empty($modvalue->icon)){
                $dest = $this->copyImage($modvalue->id, $lastContentModuleId, $modvalue->icon);

                    $updatedata = array(
                        'icon' => $dest,
                    );

                    $this->db->where('id', $lastContentModuleId);

                    $this->db->update('contentmodule', $updatedata);
            }
            ////////////////Content Tags////////////////
            $tags = $this->appclone_model->getContentModuleTags($modvalue->id);
            if(!empty($tags)){
                foreach($tags as $tagKey => $tagValue){
                    if($tagValue->formid > 0)
                        continue;

                    $tag = $this->appclone_model->getTagDetail($tagValue->id);
                    $tagData = array(
                        'tag' => $tag->tag,
                        'appid' =>$lastAppId,
                        'contentmoduleid'=> $lastContentModuleId
                    );

                    if($tag->contentmoduleid!=0){
                        $this->db->insert('tag', $tagData);
                    }
                }

            }


            $moduleSections = $this->appclone_model->getModuleSections($modvalue->id);
            foreach($moduleSections as $sectkey=>$sectvalue){
                $moduleSectionData= array(
                        'contentmoduleid' => $lastContentModuleId,
                        'sectiontypeid' => $sectvalue->sectiontypeid,
                        'order' => $sectvalue->order,
                        'maxitems' => $sectvalue->maxitems,
                        'columns' => $sectvalue->columns,
                        'title' => $sectvalue->title,
                        'showlabels' => $sectvalue->showlabels
                );
                $this->db->insert('section',$moduleSectionData);
                $lastSectionId = $this->appclone_model->getLastSectionId();

                ////////////////Section Tags////////////////
                $secttags = $this->appclone_model->getSectionTags($sectvalue->id);
                if(!empty($secttags)){

                    foreach($secttags as $tagKey => $tagValue){
                       $tag = $this->appclone_model->getTagDetail($tagValue->id);
                        $tagData = array(
                            'tag' => $tag->tag,
                            'appid' =>$lastAppId,

                            'sectionid'=> $lastSectionId
                       );

                        if($tag->sectionid!=0){
                            $this->db->insert('tag', $tagData);
                        }
                    }

                }


                $sectionContent = $this->appclone_model->getSectionContent($modvalue->id);
                foreach($sectionContent as $contkey=>$contvalue){
                    $sectionContentData= array(
                            'sectionid' => $lastSectionId,
                            'icon' => $contvalue->icon,
                            'title' => $contvalue->title,
                            'order' => $contvalue->order,
                            'contentmoduleid' => $lastContentModuleId
                    );
                    $this->db->insert('sectioncontent',$sectionContentData);
                    $lastSectionContentId = $this->appclone_model->getLastSectionContentId();

                    if(!empty($contvalue->icon)){
                        $dest = $this->copyImage($contvalue->id, $lastSectionContentId, $contvalue->icon);

                            $updatedata = array(
                                'icon' => $dest,
                            );

                            $this->db->where('id', $getLastContentModuleId);

                            $this->db->update('sectioncontent', $updatedata);
                    }

                    ////////////////Section Content Tags////////////////
                    $conttags = $this->appclone_model->getSectionContentTags($contvalue->id);
                    if(!empty($conttags)){
                        foreach($conttags as $tagKey => $tagValue){

                            $tag = $this->appclone_model->getTagDetail($tagValue->id);
                            $tagData = array(
                                'tag' => $tag->tag,
                                'appid' =>$lastAppId,

                                'sectioncontentid'=> $lastSectionId

                            );

                            if($tag->sectioncontentid!=0){
                                $this->db->insert('tag', $tagData);
                            }
                        }

                    }

                }
            }
        }
    }

    protected  function __insertEventForm(){
            $appId = $this->input->post('appid');
            $lastAppId = $this->__getLastAppId();
            $events = $this->appclone_model->getEvents($appId);
            $thisAppEvents = $this->appclone_model->getEvents($lastAppId);

            $newEvents=array();

            foreach($thisAppEvents as $key => $value){
                $newEvents[] = $value;
            }

            $i=0;

            foreach ($events as $key => $value){
                    $form = $this->appclone_model->getFormDetail($value);
                    foreach($form as $key2 => $value2){
                        $oldlauncher = $this->module_mdl->getLauncherById($value2->launcherid);
                        $newlauncher = $this->module_mdl->getLauncherByTitle('event', $newEvents[$i], $oldlauncher->title);
                        $formData=array(
                                'venueid' => 0,
                                'eventid' => $newEvents[$i],
                                'exhibitorcategoryid' => 0,
                                'name' => $value2->name,
                                'shortname' => $value2->shortname,
                                'booth' => $value2->booth,
                                'imageurl' => $value2->imageurl,
                                'mapid' => $value2->mapid,
                                'y1' => $value2->y1,
                                'y2' => $value2->y2,
                                'x1' => $value2->x1,
                                'x2' => $value2->x2,
                                'description' => $value2->description,
                                'tel' => $value2->tel,
                                'address' => $value2->address,
                                'web' => $value2->web,
                                'code' => $value2->code
                        );

                        $this->db->insert('exhibitor', $exhibitorData);
                        $lastExhibitorId = $this->appclone_model->getLastExhibitorId();

                        $groupItem = $this->appclone_model->getGroupItem($value2->id);


                        foreach($groupItem as $grkey=>$grvalue){
                            $groupname = $this->appclone_model->getGroupName($grvalue->groupid);
                            $getLastGroupId = $this->appclone_model->getLastExhibitorGroupId($groupname);
                            $getLastGroupId;
                            $groupItemData = array (
                                        'appid' => $lastAppId,
                                        'eventid' => $newEvents[$i],
                                        'venueid' => 0,
                                        'groupid' => $getLastGroupId,
                                        'itemtable' => 'exhibitor',
                                        'itemid' => $lastExhibitorId,
                                        'displaytype' => 0
                            );
                            if(!empty($getLastGroupId )){
                                $this->db->insert('groupitem', $groupItemData);
                            }
                        }

                        ////////////////Exhibitor Tags////////////////
                        $tags = $this->appclone_model->getExhibitorTags($value2->id);
                        if(!empty($tags)){
                            foreach($tags as $tagKey => $tagValue){
                                if($tagValue->formid > 0)
                                    continue;
                                $tag = $this->appclone_model->getTagDetail($tagValue->id);
                                $tagData = array(
                                    'tag' => $tag->tag,
                                    'appid' =>$lastAppId,
                                    'exhibitorid'=> $lastExhibitorId
                                );

                                if($tag->exhibitorid!=0){
                                    $this->db->insert('tag', $tagData);
                                }
                            }
                        }
                        ////////////////Exhibitor Translation////////////////
                        $table='exhibitor';
                        $translation = $this->appclone_model->getTranslation($table,$value2->id);
                        if(!empty($translation)){
                            foreach($translation as $tKey => $tValue){

                                    $translation = $this->appclone_model->getTranslationDetail($tValue->id);
                                        $translationData = array(
                                            'table' => $tValue->table,
                                            'tableid' => $lastExhibitorId,
                                            'fieldname' => $tValue->fieldname,
                                            'language' => $tValue->language,
                                            'translation' => $tValue->translation
                                        );
                                        $this->db->insert('translation', $translationData);
                                }
                        }
                   }$i++;
            }
    }

    protected function __insertForms($oldAppId, $oldTypeId, $appId, $typeId, $type = 'event'){

        $launcherData   = array();
        $formData       = array();
        $screenData     = array();
        $fieldData      = array();
        $laucherRes     = FALSE;

        $moduletypeid   = 44;
        $typeArr        = array();

        $languages = $this->language_model->getLanguagesOfApp($oldAppId);

        if($type == 'event'): // if($type == 'event')

            $this->db->select('*');
            $this->db->where('eventid', $oldTypeId);
            $this->db->where('moduletypeid', '44');
            $this->db->where('module', 'forms');
            $laucherRes = $this->db->get('launcher');
            if($laucherRes->num_rows() == 0) $laucherRes = FALSE;

            $typeArr[] = 'eventid';
            $typeArr[] = $typeId;

        else: // if($type == 'venue')

            $this->db->select('*');
            $this->db->where('venueid', $oldTypeId);
            $this->db->where('moduletypeid', '44');
            $this->db->where('module', 'forms');
            $laucherRes = $this->db->get('launcher');
            if($laucherRes->num_rows() == 0) $laucherRes = FALSE;

            $typeArr[] = 'venueid';
            $typeArr[] = $typeId;

        endif;

        if($laucherRes && $appId):
            // Adding Launchers
            foreach($laucherRes->result() as $row):
                $oldLaucherId = $row->id;
                $launcherData           = $row;
                $launcherData->id       = '';
                $launcherData->appid    = $appId;
                $launcherData->$typeArr[0] = $typeArr[1];

                $this->db->insert('launcher', $launcherData);
                $launcherid = $this->db->insert_id();

                if($launcherid){
                    // Count Event Launchers of this ModuleType...
                    $eventlaunchers = $this->event_model->countLauncherFromEventModule($event->id, $moduletypeid);
                    if($eventlaunchers){
                        $totalmodule = $this->module_mdl->countModuleFromEventModule($event->id, $moduletypeid);
                        if(!$totalmodule){
                            //Add to Module
                            $this->module_mdl->addModuleToEvent($event->id, $moduletypeid);
                        }

                    }
                    //-------------------------------------------//

                    foreach($languages as $language) {
                        $translation = $this->language_model->getTranslation('launcher', $oldLaucherId, 'title', $language->key)->translation;
                        if(!$translation)
                            $translation = $launcherData->title;

                        $this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $translation);
                    }

                    //-------------- Addd to Forms --------------//

                    $this->db->select('*');
                    $this->db->where('appid', $oldAppId);
                    $this->db->where('launcherid', $oldLaucherId);

                    $formRes = $this->db->get('form');
                    if($formRes->num_rows() == 0) $formRes = FALSE;

                    foreach($formRes->result() as $frmRow):
                        $oldFormId = $frmRow->id;

                        $formData               = $frmRow;
                        $formData->id           = '';
                        $formData->appid        = $appId;
                        $formData->launcherid   = $launcherid;
                        $formData->$typeArr[0]  = $typeArr[1];
                        $this->db->insert('form', $formData);
                        $formid = $this->db->insert_id();

                        if($formid){

                            //-------------- Addd to Tags --------------//
                            $tagData = (object) array(
                                'appid'     => $formData->appid,
                                'formid'    => $formid,
                                'tag'       => $formData->title,
                                'active'    => 1
                            );
                            $tagData->$typeArr[0]  = $typeArr[1];
                            $tagid = $this->general_model->insert('tag', $tagData);

                            foreach($languages as $language) {

                                $transTitle     = $this->language_model->getTranslation('form', $oldFormId, 'title', $language->key)->translation;
                                $transSubBtn    = $this->language_model->getTranslation('form', $oldFormId, 'submissionbuttonlabel', $language->key)->translation;
                                $transSubTxt    = $this->language_model->getTranslation('form', $oldFormId, 'submissionconfirmationtext', $language->key)->translation;

                                if(!$transTitle)
                                    $transTitle = $formData->title;
                                if(!$transSubBtn)
                                    $transSubBtn = $formData->submissionbuttonlabel;
                                if(!$transSubTxt)
                                    $transSubTxt = $formData->submissionconfirmationtext;

                                $this->language_model->addTranslation('form', $formid, 'title', $language->key, $transTitle);
                                $this->language_model->addTranslation('form', $formid, 'submissionbuttonlabel', $language->key, $transSubBtn);
                                $this->language_model->addTranslation('form', $formid, 'submissionconfirmationtext', $language->key, $transSubTxt);

                                // Add tag translation
                                $this->language_model->addTranslation('tag', $tagid, 'tag', $language->key, $transTitle);
                            }


                            //-------------- Addd to Screens --------------//

                            $this->db->select('*');
                            $this->db->where('formid', $oldFormId);
                            $screenRes = $this->db->get('formscreen');
                            if($screenRes->num_rows() == 0) $screenRes = FALSE;

                            foreach($screenRes->result() as $scrRow):
                                $oldScreenId = $scrRow->id;

                                $screenData         = $scrRow;
                                $screenData->id     = '';
                                $screenData->formid = $formid;

                                $this->db->insert('formscreen', $screenData);
                                $screenid = $this->db->insert_id();

                                if($screenid){

                                    foreach($languages as $language) {
                                        $transTitle     = $this->language_model->getTranslation('formscreen', $oldScreenId, 'title', $language->key)->translation;
                                        if(!$transTitle)
                                            $transTitle = $screenData->title;

                                        $this->language_model->addTranslation('formscreen', $screenid, 'title', $language->key, $transTitle);
                                    }

                                    //-------------- Addd to Fields --------------//

                                    $this->db->select('*');
                                    $this->db->where('formscreenid', $oldScreenId);
                                    $fieldRes = $this->db->get('formfield');
                                    if($fieldRes->num_rows() == 0) $fieldRes = FALSE;

                                    foreach($fieldRes->result() as $fldRow):
                                        $oldFieldId = $fldRow->id;

                                        $fieldData               = $fldRow;
                                        $fieldData->id           = '';
                                        $fieldData->formscreenid = $screenid;

                                        $this->db->insert('formfield', $fieldData);
                                        $newfieldid = $this->db->insert_id();

                                        if($newfieldid){
                                                //add translated fields to translation table
                                                foreach($languages as $language)
                                                {
                                                    $transTitle     = $this->language_model->getTranslation('formfield', $oldFieldId, 'label', $language->key)->translation;
                                                    $transPosVal    = $this->language_model->getTranslation('formfield', $oldFieldId, 'possiblevalues', $language->key)->translation;
                                                    $transDfltVal   = $this->language_model->getTranslation('formfield', $oldFieldId, 'defaultvalue', $language->key)->translation;

                                                    if(!$transTitle)
                                                        $transTitle = $fieldData->label;
                                                    if(!$transPosVal)
                                                        $transPosVal = $fieldData->possiblevalues;
                                                    if(!$transDfltVal)
                                                        $transDfltVal = $fieldData->defaultvalue;

                                                    $this->language_model->addTranslation('formfield', $newfieldid, 'label', $language->key, $transTitle);
                                                    $this->language_model->addTranslation('formfield', $newfieldid, 'possiblevalues', $language->key, $transPosVal);
                                                    $this->language_model->addTranslation('formfield', $newfieldid, 'defaultvalue', $language->key, $transDfltVal);
                                                }

                                        }
                                    endforeach;
                                } // if($screenid)
                            endforeach;
                        } // if($formid)
                    endforeach;
                } // if($launcherid)
            endforeach; // foreach($laucherRes->result() as $row)
        endif;

    } // protected function __insertForms(...)
	
	protected function __insertCoupons($oldVenueId, $lastVenueId){
		
		if( ! ( $oldVenueId && $lastVenueId ) ){
			return FALSE;
		}
		
		$qry = 'INSERT INTO coupons  ( `id`, `venueid`, `title`, `content`, `uses`, `image` )  
		SELECT \'\', \'' . $lastVenueId . '\', `title`, `content`, `uses`, `image` from coupons 
		WHERE `venueid`= ' . $oldVenueId;
		
		if( $this->db->query( $qry ) ){
			$rows = $this->appclone_model->getCouponsByVenueId($lastVenueId);
			foreach( $rows as $row ){
				
				$tmp = explode('/', $row->image);
				$oldId = $tmp[ count( $tmp ) - 2 ] ;
				if( $oldId ){
					
					$newImage = $this->copyImage($oldId, $row->id, $row->image);	
				
					$this->db->where('id', $row->id);
					$this->db->update('coupons', array('image' => $newImage));
				
					$qry = 'INSERT INTO  translation  ( `id`, `table`, `tableid`, `fieldname`, `language`, `translation` )  
					SELECT \'\', `table`, \'' . $row->id . '\', `fieldname`, `language`, `translation` from translation 
					WHERE `tableid`= ' . $oldId . ' AND `table` = \'coupons\'';
					
					$this->db->query($qry);
				}
				
			}// foreach( $rows as $row )
		
		} // if( $this->db->query( $qry ) )
		
	}
	
	protected function __insertLoyalty($oldVenueId, $lastVenueId){
		
		if( ! ( $oldVenueId && $lastVenueId ) ){
			return FALSE;
		}
		
		$rows = $this->appclone_model->getLoyaltyByVenueId($oldVenueId);
		foreach( $rows as $row ){
			$data = array(
				'venueid'	=> $lastVenueId,
				'title'		=> $row->title,
				'text'		=> $row->text,
				'code'		=> $row->code,
				'order'		=> $row->order
			);
			
			$this->db->insert('loyalty', $data);
			$newId = $this->db->insert_id();
			
			$qry = 'INSERT INTO  translation  ( `id`, `table`, `tableid`, `fieldname`, `language`, `translation` )  
			SELECT \'\', `table`, \'' . $newId . '\', `fieldname`, `language`, `translation` from translation 
			WHERE `tableid` = ' . $row->loyaltyid . ' AND `table` = \'loyalty\'';
			
			$this->db->query($qry);
				
		}// foreach( $rows as $row )
		
	}
}

