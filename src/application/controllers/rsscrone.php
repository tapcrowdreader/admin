<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Rsscrone extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('news_model');
	}

	//php 4 constructor
	function News() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('news_model');
	}

	function index() {
		$this->getContentRssFeeds();
	}

	function getContentRssFeeds(){
		$newsSources = $this->news_model->getContentRssFeeds();
		//echo '<pre>';print_r($newsSources);echo '</pre>';
		foreach($newsSources as $source){
			$this->insertRssNewsItems($source->url, $source->newsid, $source->appid);
		}
	}

	function insertRssNewsItems($url,$sourceid,$appid){
        $content = file_get_contents($url);
        try {
            $rss = new SimpleXmlElement($content);
        } catch (Exception $e) {
            $rss = null;
        }
	    foreach ($rss->channel->item as $post) {
	        $description = (string) $post->description;
	        $namespace = $post->children('http://purl.org/rss/1.0/modules/content/');
	        $content = (string) $namespace->encoded;
	        if ($content != null) {
	            $description = $content;
	        }

			// get host name from URL
			preg_match('@^(?:http://)?([^/]+)@i',
			    $post->link, $matches);
			$host = $matches[1];

			// get last two segments of host name
			preg_match('/[^.]+\.[^.]+$/', $host, $matches);
			$hostUrl = $matches[0];

			// Check if its a youtube URL for a video
			if($hostUrl=="youtube.com"){
				$field = "videourl";
			}else{
				$field = "url";
			}

	        $data = array(
	            "title" => (string) $post->title,
	            "appid" => $appid,
	            "txt" => $description,
	            "".$field."" => (string) $post->link,
	            "sourceid" => $sourceid,
	            "datum" => ((string) $post->pubDate != null && (string) $post->pubDate != '') ? date("Y-m-d H:i:s", strtotime((string) $post->pubDate)) : date("Y-m-d H:i:s", time())
	        );

			$dom = new domDocument;
			$dom->loadHTML($data['txt']);
			$dom->preserveWhiteSpace = false;
			$images = $dom->getElementsByTagName('img');
			
			foreach ($images as $image) {
			  $desc_image = $image->getAttribute('src');
			}

	        $newsItemExists = $this->news_model->newsItemExists($sourceid, $post->link);
	        //echo $post->link.' : '.$newsItemExists.'<br>';
	        if($newsItemExists <= 0){
	            if ($newsid = $this->general_model->insert('newsitem', $data)) {

				  if (!is_dir($this->config->item('imagespath').'upload/newsimages/'.$newsid)){
					    mkdir($this->config->item('imagespath').'upload/newsimages/'.$newsid);
	                                                        if(isset($post->image)){
										                        $file_name = $this->ShowFileName($post->image);
										                        $file_ext = $this->ShowFileExtension($post->image);                                                                        	
	                                                            copy($post->image, $this->config->item('imagespath').'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext);
	                                                            $data = array("image" => 'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext);
	                                                            $this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext));
	                                                        }
	                                                        
	                                                        if(isset($post->enclosure->attributes()->url)){
										                        $file_name = $this->ShowFileName($post->enclosure->attributes()->url);
										                        $file_ext = $this->ShowFileExtension($post->enclosure->attributes()->url);                                                                        	
	                                                            copy($post->enclosure->attributes()->url, $this->config->item('imagespath').'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext);
	                                                            $data = array("image" => 'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext);
	                                                            $this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext));
	                                                                                                                              
	                                                        }                                                                    
	                                                        
	                                                        if(!empty($desc_image)){
										                        $file_name = $this->ShowFileName($desc_image);
										                        $ext = $this->ShowFileExtension($desc_image);													                        
	                                                        	if($ext=="jpg" || $ext=="gif" || $ext=="png"){
	                                                            	copy($desc_image, $this->config->item('imagespath').'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$ext);
	                                                            	$this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$ext));
	                                                        	}else{
	                                                        		$this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/no-image-placeholder.png'));
	                                                        	}
	                                                        }

	                                                        if(!isset($post->image) && !isset($desc_image) && !isset($post->enclosure->attributes()->url)){
	                                                      		$this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/no-image-placeholder.png'));
	                                                        }
	               }

	                //add translated fields to translation table
	                if ($languages != null) {
	                    foreach ($languages as $language) {
	                        $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, (string) $post->title);
	                        $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $description);
	                    }
	                }

	                /*$mystring = 'tag1,tag2,tag3';

					$pos = strrpos($mystring, ",");
					if (strlen($pos) > -1) { // note: three equal signs
					    echo $pos;
					}*/
					
	                $tagsList = array();
	                if(isset($post->category)){
						$categories = strpos($post->category, ',');

						if(strlen($categories) > -1) {
							$categories = explode(",",$categories);

	                        foreach($categories as $category){
	                        	$tagsList[] = $category;

	                        }

						}else{
	                	  $tagsList[] = $post->category;
	                	}
	                }
	                foreach($tagsList as $tag){
	                    $tagData = array(
	                        "newsitemid" => (string) $newsid,
	                        "tag" => (string) $tag                                
	                    );
	                    echo '<pre>';print_r($post->category);echo '</pre>';
	                    //$this->general_model->insert('tag', $tagData);
	                }
	            } else {
	                $error = __("<p>Oops, something went wrong. Please try again.</p>");
	            }
	        }   
	    }		

	}

    public function ShowFileExtension($filepath)
    {
        preg_match('/[^?]*/', $filepath, $matches);
        $string = $matches[0];
     
        $pattern = preg_split('/\./', $string, -1, PREG_SPLIT_OFFSET_CAPTURE);

        # check if there is any extension
        if(count($pattern) == 1)
        {
            echo 'No File Extension Present';
            exit;
        }
       
        if(count($pattern) > 1)
        {
            $filenamepart = $pattern[count($pattern)-1][0];
            preg_match('/[^?]*/', $filenamepart, $matches);
            return $matches[0];
        }
    }
   
    public function ShowFileName($filepath)
    {
        preg_match('/[^?]*/', $filepath, $matches);
        $string = $matches[0];
        #split the string by the literal dot in the filename
        $pattern = preg_split('/\./', $string, -1, PREG_SPLIT_OFFSET_CAPTURE);
        #get the last dot position
        $lastdot = $pattern[count($pattern)-1][1];
        #now extract the filename using the basename function
        $filename = basename(substr($string, 0, $lastdot-1));
        #return the filename part
        return $filename;
    }	
}