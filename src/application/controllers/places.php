<?php
// namespace Tapcrowd;

if(!defined('BASEPATH')) exit(__('No direct script access'));

require_once dirname(__DIR__) . '/models/bread_model.php';
session_start();

use \Tapcrowd\Models\Map as MapModel;


/**
 * Place Struct
 */
class Place extends \Tapcrowd\Struct
{
	public $lat = 0;
	public $lng = 0;
	public $addr;
	public $title;
	public $info;
	public $imageurl;
}

/**
 * Places controller
 * @author Tom Van de Putte
 */
class Places extends CI_Controller
{
	private $_place_model;
	private $_resource_model;
	private $_meta_model;

	private $_module_settings;

	private $_pdo;

	protected $app;

	private $moduletypeid = 54;

	function __construct()
	{
		parent::__construct();

		$this->load->model('metadata_model');

		# Create PDO Connection
		$dsn = 'mysql:dbname='.$this->db->database.';host='.$this->db->hostname;
		$driver_opts = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"');
		$this->_pdo = new \PDO($dsn, $this->db->username, $this->db->password, $driver_opts);

// 		$pdo = new \PDO('mysql:dbname='.$this->db->database.';host='.$this->db->hostname, $this->db->username, $this->db->password);
		if(!$this->_pdo instanceOf \PDO) {
			throw new \Exception('Unable to create PDO object for ' . __CLASS__);
		}
// 		$this->_pdo = $pdo;

		# Load models
		$this->_place_model = new \Tapcrowd\Bread_model('tc_places', $this->_pdo, 'Place');
// 		$this->_resource_model = new \Tapcrowd\Bread_model('tc_resources', $pdo, '\Tapcrowd\Resource');
// 		$this->_meta_model = new \Tapcrowd\Bread_model('tc_metadata', $pdo, '\Tapcrowd\Meta');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Place',
			'plural' => 'Places',
			'title' => __('Places'),
			'browse_url' => 'places/--parentType--/--parentId--',
			'add_url' => 'places/add/--parentType--/--parentId--',
			'edit_url' => 'places/edit/--id--',
			'module_url' => 'module/editByController/places/--parentType--/--parentId--/0',
			'headers' => array(
				__('Title') => 'title',
				__('Addr') => 'addr',
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'places/read/--id--?view=edit',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'places/delete/--id--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
				'add' => 'tc_place_edit'
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			// 'metadata' => array(
			// 	'place_phone' => (object)array(
			// 		'id' => 1,
			// 		'type' => 'phone',
			// 		'qname' => 'place_phone',
			// 		'name' => 'Phone',
			// 		'value' => '',
			// 	),
			// 	'place_fax' => (object)array(
			// 		'id' => 4,
			// 		'type' => 'fax',
			// 		'qname' => 'place_fax',
			// 		'name' => 'Fax',
			// 		'value' => '',
			// 	),
			// 	'place_email' => (object)array(
			// 		'id' => 2,
			// 		'type' => 'email',
			// 		'qname' => 'place_email',
			// 		'name' => 'Email',
			// 		'value' => '',
			// 	),
			// 	'place_image' => (object)array(
			// 		'id' => 3,
			// 		'type' => 'image',
			// 		'qname' => 'place_image',
			// 		'name' => 'Image',
			// 		'value' => '',
			// 	),
			// ),
			'metadata' => array(),
			'launcher' => null,
			'checkboxes' => false,
			'deletecheckedurl' => 'places/removemany/--parentId--/--parentType--',
			'enable_maps' => false,
		);

		# Set launcher
		try {
			$this->_module_settings->launcher = $this->_getLauncher($this->_pdo);
		} catch(\Exception $e) {
			$this->_module_settings->launcher = null;
		}

		# Get metadata
		try {
			$metadata = $this->_getMetadata($this->_pdo);
			foreach($metadata as $m) {
				$this->_module_settings->metadata[$m->qname] = (object)array(
						'id' => $m->id,
						'type' => $m->type,
						'q_name' => $m->qname,
						'name' => $m->name,
						'value' => ''
					);
			}
		} catch(\Exception $e) {
			$this->_module_settings->metadata = null;
		}
	}

	protected function _getMetadata( &$pdo, $language = '' )
	{
		$launcher = $this->_module_settings->launcher;
		if($launcher == null) {
			return array();
		}

		# Get normal metadata
		$sql = "select * from tc_metadata where parentType='launcher' AND parentId=$launcher->id";
		$stat = $pdo->query($sql);
		$data = $stat->fetchAll(\PDO::FETCH_OBJ);

// 		# Check for MarkerMap; Add Marker Icon metadata
// 		$mapmodel = MapModel::getInstance();
// 		$maps = $mapmodel->getMaps('launcher', $launcher->id);
// 		if(!empty($maps)) {
// 			$data[] = (object)array(
// 				'sortorder' => 0,
// 				'type' => 'image',
// 				'qname' => 'markerIcon',
// 				'name' => 'Marker Icon',
// 			);
// 		}

		if(!empty($language) && !empty($data) && !empty($data[0])) {
			$transname = _getTranslation('tc_metadata', $data[0]->id, 'name', $language);
			if($transname) {
				$data[0]->name = $transname;
			}
		}

		if(empty($data) || empty($data[0])) {
			return array();
		}
		else {
			return $data;
		}
	}

	/**
	 * Try to get the current launcher/module
	 *
	 * @param \PDO $pdo PDO Connection
	 * @return stdclass Launcher object
	 */
	protected function _getLauncher( &$pdo, $launcherid = '', $parentType = '', $parentId = '', $moduletypeid = '' )
	{
		if(!empty($launcherid)) {
			$sql = "select * from launcher where id=$launcherid";
		} elseif(!empty($parentType) && !empty($parentId) && !empty($moduletypeid)) {
			$column = $parentType.'id';
			$sql = "SELECT * FROM launcher WHERE moduletypeid = $moduletypeid AND {$column} = $parentId LIMIT 1";
		} else {
			# Parse URI
			$uri = explode('/',$_SERVER['PATH_INFO']);
			if(empty($uri) || count($uri) < 2) {
				throw new \RuntimeException(__('Unsupported request (%s)', $_SERVER['PATH_INFO']));
			}

			# Get parent data
			if(count($uri) == 4) {
				array_shift($uri);
				$controller = array_shift($uri);
				$method = array_shift($uri);
				$parentId = (int)array_shift($uri);
			} elseif(count($uri) == 5) {
				array_shift($uri);
				$controller = array_shift($uri);
				$function = array_shift($uri);
				$method = array_shift($uri);
				$parentId = (int)array_shift($uri);
			}

			# Build sql
			if($method == 'venue' || $method == 'event') {
				$sql = "select * from launcher where {$method}id=$parentId and module='Places'";
			} else {
				throw new \RuntimeException(__('Unsupported parent type (%s) in %s', $method, $controller));
			}
		}

		# Fetch launcher
		$stat = $pdo->query($sql);
		$data = $stat->fetchAll(\PDO::FETCH_OBJ);
		if(empty($data) || empty($data[0])) {
			throw new \RuntimeException(__('No launcher for parent (%s #%d)', $parentType, $parentId));
		}
		else {
			return $data[0];
		}
	}

	/**
	 * Validate and return parent object
	 *
	 * @param string $parentType The parent type
	 * @param string $parentId The parent identifier
	 * @return mixed the parent object
	 */
	protected function _getParentObject( $parentType, $parentId )
	{
		$parent_model = $parentType.'_model';
		if(!isset($this->$parent_model) || method_exists('getbyid', $this->$parent_model)) {
			throw new RuntimeException(__('Unknown parenttype %s', $parentType));
		}
		$parent = $this->$parent_model->getbyid($parentId);
		$this->app = _actionAllowed($parent,$parentType, 'returnApp');
		return $parent;
	}

	/**
	 * Return a new instance of the given struct with given parent
	 *
	 * @param \Tapcrowd\Struct $parent The parent struct
	 * @param \Tapcrowd\Struct $child The child struct
	 */
	protected function _makeChildStruct( \Tapcrowd\Struct &$parent, \Tapcrowd\Struct &$child )
	{
		$child->parentType = get_class($parent);
		$child->parentId = (int)$parent->id;
	}

	/**
	 * Validate and return current Place
	 *
	 * @param int $placeId The place identifier
	 * @return \Tapcrowd\Place
	 */
	protected function _getPlaceStruct( $placeId )
	{
		$place = new Place( (int)$placeId );
		if($this->_place_model->read($place) === false) {
			$_SESSION['error'] = __('Place %d not found', $placeId);
			return false;
		}
		return $place;
	}

	/**
	 * Shortcut for venue apps (calls browse method)
	 * @param int $venueId The venue identifier
	 */
	public function venue( $venueId )
	{
		if(!is_numeric($venueId)) {
			exit;
		}
		$this->browse( $this->appid, 'venue', $venueId );
	}

	/**
	 * Shortcut for event apps (calls browse method)
	 * @param int $eventId The event identifier
	 */
	public function event( $eventId )
	{
		if(!is_numeric($eventId)) {
			exit;
		}
		$this->browse( $this->appid, 'event', $eventId );
	}

	/**
	 * Lists structures from database
	 *
	 * @param string $parentType The parentType
	 * @param int $parentId The parent identifier
	 * @param int $groupId Optional group identifier
	 * @param string $tags Optional comma separated lists of tags
	 * @param string $query Optional search query
	 * @param int $offset Optional offset in the resultset, defaults to 0 (zero)
	 * @param int $limit Optional max. number of results, defaults to 500
	 */
	public function browse( $appId, $parentType, $parentId, $groupId = null, $tags = null, $query = null, $offset = 0, $limit = 500 )
	{
		$parent = $this->_getParentObject( $parentType, $parentId );

		# Set mobilesite url
		$this->iframeurl = 'places/'.$parentType.'/'.$parentId;
		$listview = $this->_module_settings->views['list'];
		$this->_module_settings->add_url = 'places/add/'.$parentType.'/'.$parentId;
		$this->_module_settings->import_url = 'import/places/'.$parentType.'/'.$parentId;

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array($parentType, $parentId), $module_url);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array($parentType, $parentId), $deletecheckedurl);

		# categories
		$placescategoriesgroupid = $this->group_model->getMainCategorieGroup($parentId, $parentType, 'places');
		if($placescategoriesgroupid) {
			$btnCategories = (object)array(
							'title' => __('Edit Categories'),
							'href' => 'groups/view/'.$placescategoriesgroupid.'/'.$parentType.'/'.$parentId.'/places',
							'icon_class' => 'icon-pencil',
							'btn_class' => 'edit btn',
							);
			$this->_module_settings->extrabuttons['categories'] = $btnCategories;
		}

		$launcher = $this->module_mdl->getLauncher(54, $parentType, $parentId);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		# Get places
		$query = new \Tapcrowd\Query($appId, $parentType, $parentId, $groupId, $tags, $query, $offset, $limit);
		$places = $this->_place_model->browse($query);

		#
		# BUG Fix for map creation: this is a common entry point for all places related calls
		# so, we will test for map existance here.
		#
		$res = $this->_pdo->query('SELECT COUNT(*) FROM map WHERE launcherid=' . (int)$launcher->id)->fetchAll(\PDO::FETCH_NUM);
		if($res[0][0] == 0) {
			$url = 'https://maps.google.com/?z=8&t=t&ll=50.753887,4.269936';
			$sql = 'INSERT INTO map (appid, imageurl, launcherid, mapType) VALUES(%d,"%s",%d,"geographic")';
			$this->_pdo->exec(sprintf($sql, $appId, $url, (int)$launcher->id));
		}

		# Get and set output
		$cdata = array(
			'content' => $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $places), true),
			'sidebar' => $this->load->view('c_sidebar', array($parentType => $parent), true),
			'crumb' => array($parent->name => "$parentType/view/".$parentId, $this->_module_settings->title => 'places/'.$parentType.'/'.$parentId),
			'modules' => $this->module_mdl->getModulesForMenu($parentType, $parentId, $this->app, 'places'),
		);
		$this->load->view('master', $cdata);
	}

	/**
	 * Display structure in display or read mode
	 */
	public function read( $placeId, $groupid = '', $basegroupid = '' )
	{
		$this->_module_settings->languages = $this->language_model->getLanguagesOfApp((int)$this->appid);
		$place = $this->_getPlaceStruct( $placeId );
		if($place === false) {
			$content = __('Place %d not found', $placeId);
		} else {
			# Validate parent
			$parent = $this->_getParentObject( $place->parentType, $place->parentId );

			# metadata
			$launcher = $this->_getLauncher($this->_pdo, '', $place->parentType, $place->parentId, $this->moduletypeid);
			$metadata = $this->metadata_model->getMetadata($launcher, 'place', $place->id, $this->app);

			$this->iframeurl = 'places/view/'.$placeId;

			# Get view
			$view = filter_input(INPUT_GET, 'view');
			if(!isset($this->_module_settings->views[$view])) $view = 'read';
			$view = $this->_module_settings->views[$view];

			$parent = $this->_getParentObject( $place->parentType, $place->parentId );

			$maincat = $this->group_model->getMainCategorieGroup($place->parentId, $place->parentType, 'places');
			if(empty($basegroupid) && $maincat) {
				$basegroupid = $maincat;
			}
			$catshtml = '';
			$categories = array();
			$currentCats = '';
			if($maincat != false) {
				if($maincat) {
					$this->group_model->htmlarray = array();
					$catshtml = $this->group_model->display_children($basegroupid,0,$place->parentId, $place->id, $place->parentType, 'tc_places');
					$currentCats = $this->group_model->groupids;
					$this->group_model->groupids = '';
				}
			}

			if(!empty($groupid)) {
				$this->_module_settings->edit_url = 'places/edit/--id--/'.$place->parentType.'/places/'.$groupid;
			}

			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $this->appid AND itemtype = 'place' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $place->id AND itemtype = 'place'");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag;
				}
				$tags = $tagz;
			}

			$data = array(
				'settings' => $this->_module_settings,
				'app' => $this->app,
				'metadata' => $metadata,
				'languages' => $this->_module_settings->languages,
				'data' => $place,
				'catshtml' => $catshtml,
				'currentCats' => $currentCats,
				'apptags' => $apptags,
				'tags' => $tags
			);
			$content = $this->load->view($view, $data, true);

			$launcher = $this->module_mdl->getLauncher(54, $place->parentType, $place->parentId);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
		}

		$cdata = array(
			'content' => $content,
			'sidebar' => $this->load->view('c_sidebar', array($parentType => $parent), true),
			'crumb' => array($parent->name => "{$place->parentType}/view/".$place->parentId, $this->_module_settings->title => 'places/'.$place->parentType.'/'.$place->parentId, $place->title => uri_string_params()),
			'modules' => $this->module_mdl->getModulesForMenu($place->parentType, $place->parentId, $this->app, 'places'),
		);
		$this->load->view('master', $cdata);
	}

	/**
	 * Edit page
	 */
	public function edit( $placeId = null, $parentType = '', $object = 'places', $groupid = 0)
	{
		$this->_module_settings->languages = $this->language_model->getLanguagesOfApp((int)$this->appid);

		# Get place and parent object
		if($placeId != null) {
			$place = $this->_getPlaceStruct( $placeId );
			if($place === false) { redirect('/places'); }
		} else {
			$place = new Place;
		}

		if(isset($_POST['parentType']) && isset($_POST['parentId'])) {
			$place->parentType = filter_input(INPUT_POST, 'parentType');
			$place->parentId = filter_input(INPUT_POST, 'parentId');
		}

		$parent = $this->_getParentObject( $place->parentType, $place->parentId );
		$place->appId = $this->app->id;

		# Parse input
		$prefix = strtolower(get_class($place)) . '_';
		$params = array_diff_key(get_object_vars($place), get_class_vars('\Tapcrowd\Struct'));
		foreach($params as $param => $value) {
			if($param == 'title' || $param == 'info') {
				$input = filter_input(INPUT_POST, $prefix.$param.'_'.$this->app->defaultlanguage);
			} else {
				$input = filter_input(INPUT_POST, $prefix.$param);
			}

			if($param != 'imageurl') {
				if($input !== null) $place->$param = $input;
			}
		}

		# Validate/Set status
		$status = filter_input(INPUT_POST, $prefix.'status');
		if(isset($status)) {
			if(!in_array($status, $this->_module_settings->statuses)) {
				$error = __('Invalid status code "%s"', $status);
			} else {
				$place->status = $status;
			}
		}

		# Validate latitude and longitude
		$lat = ($place->lat >= -180 && $place->lat <= 180);
		$lng = ($place->lng >= -180 && $place->lng <= 180);
		if($lat === false || $lng === false) {
			$error = __('Invalid longitude/latitude : (%f | %f)', $place->lng, $place->lat);
		}


		# Resolve
		if(isset($error)) {
			$_SESSION['error'] = $error;
		} else {
			try {
				if($placeId == null) {
					$this->_place_model->add($place);
				} else {
					$this->_place_model->edit($place);
				}

				$_SESSION['message'] = __('Update successful');
			} catch(\Exception $e) {
				$_SESSION['error'] = $e->getMessage();
			}
		}


		foreach($params as $param => $value) {
			if($param == 'title' || $param == 'info') {
				foreach($this->_module_settings->languages as $language) {
					$input = filter_input(INPUT_POST, $prefix.$param.'_'.$language->key);
					$transSuccess = $this->language_model->updateTranslation('place', $place->id, $param, $language->key, $input);
					if(!$transSuccess) {
						$this->language_model->addTranslation('place', $place->id, $param, $language->key, $input);
					}
				}
			}
		}

		//Uploads Images
		if($_FILES['imageurl']['name'] != '') {
			if(!is_dir($this->config->item('imagespath') . "upload/placeimages/".$place->id)){
				mkdir($this->config->item('imagespath') . "upload/placeimages/".$place->id, 0775, true);
			}

			$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/placeimages/'.$place->id;
			$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
			$configexlogo['max_width']  = '2000';
			$configexlogo['max_height']  = '2000';
			$this->load->library('upload', $configexlogo);
			$this->upload->initialize($configexlogo);
			if (!$this->upload->do_upload('imageurl')) {
				$image = ""; //No image uploaded!
				$error .= $this->upload->display_errors();
			} else {
				//successfully uploaded
				$returndata = $this->upload->data();
				$image = 'upload/placeimages/'. $place->id . '/' . $returndata['file_name'];
			}

			$place->imageurl = $image;
			$this->general_model->update('tc_places', $place->id, array('imageurl' => $image));
		}

		$languages =& $this->_module_settings->languages;
		$launcher = $this->_getLauncher($this->_pdo, '', $place->parentType, $place->parentId, $this->moduletypeid);
		$metadata = $this->metadata_model->getMetadata($launcher, 'place', $place->id, $this->app);

		foreach($languages as $language) {
			foreach($metadata as $m) {
				if(_checkMultilang($m, $language->key, $this->app)) {
					$postfield = _getPostedField($m, $_POST, $_FILES, $language);
					if($postfield !== false) {
						if(_validateInputField($m, $postfield)) {
							_saveInputField($m, $postfield, 'place', $place->id, $this->app, $language);
						}
					}
				}
			}
		}

		if(!empty($_POST['groupids'])) {
			$place->groupids = explode(',', filter_input(INPUT_POST, 'groupids'));
			if(!empty($place->groupids)) {
				foreach($place->groupids as $groupid) {
					$res = $this->db->query("SELECT id FROM groupitem WHERE groupid = $groupid AND itemtable = 'tc_places' AND itemid = $place->id LIMIT 1");
					if($res->num_rows() == 0) {
						$this->general_model->insert('groupitem', array(
							'appid' => (int)$this->appid,
							$place->parentType.'id' => $place->parentId,
							'groupid' => $groupid,
							'itemtable' => 'tc_places',
							'itemid' => $place->id
							));
					}
				}
			}
		}

		if($this->input->post('groups')) {
			$this->group_model->deleteGroupItemsFromObject($place->id, 'tc_places');
			//groupitems
			if($this->input->post('groups') != null && $this->input->post('groups') != '') {
				$groups = $this->input->post('groups');
				$this->db->query("DELETE FROM groupitem WHERE itemid = $place->id AND itemtable = 'tc_places'");
				foreach($groups as $group) {
					if($group != '') {
						$group2 = $this->group_model->getById($group);
						$groupitemdata = array(
								'appid'	=> (int)$this->appid,
								$place->parentType.'id'	=> $place->parentId,
								'groupid'	=> $group2->id,
								'itemtable'	=> 'tc_places',
								'itemid'	=> $place->id	,
								'displaytype' => $group2->displaytype
							);
						$this->general_model->insert('groupitem', $groupitemdata);
					}
				}
			}
		}

		$this->db->where('itemtype', 'place');
		$this->db->where('itemid', $place->id);
		$this->db->delete('tc_tag');
		$tags = $this->input->post('mytagsulselect');
		foreach ($tags as $tag) {
			$this->general_model->insert('tc_tag', array(
				'appid' => $this->appid,
				'itemtype' => 'place',
				'itemid' => $place->id,
				'tag' => $tag
			));
		}

		# Update timestamps
		if($place->parentType == 'event') {
			_updateTimeStamp($place->parentId);
		} else {
			_updateVenueTimeStamp($place->parentId);
		}
		$this->general_model->update('app', $this->app->id, array('timestamp' => time()));

		# Redirect
		if($groupid != 0) {
			redirect('groups/view/'.$groupid.'/'.$place->parentType.'/'.$place->parentId.'/places');
		}
		redirect('places/'.$place->parentType.'/'.$place->parentId);
	}

	/**
	 * Add page, redirects to edit page on success
	 *
	 * @param string $parentType The parent type
	 * @param int $parentId The parent identifier
	 */
	public function add( $parentType, $parentId, $type='places', $groupid = 0 )
	{
		//nasty bugfix for places in groups. URL of places module is the other way around,
		//first parent type then id, would require much code rewriting to files of groups which are also used by other controllers (By Jens)
		if(is_numeric($parentType) && !is_numeric($parentId)) {
			$parentType2 = $parentType;
			$parentType = $parentId;
			$parentId = $parentType2;
		}

		$parent = $this->_getParentObject( $parentType, $parentId );
		$this->iframeurl = 'places/'.$parentType.'/'.$parentId;
		# Create default object
		$place = new Place;
		$place->appId = (int)$this->appid;
		$place->parentType = $parentType;
		$place->parentId = (int)$parentId;
		$place->status = 'created';
		$place->groupids = $groupid;

		$launcher = $this->module_mdl->getLauncher(54, $place->parentType, $place->parentId);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;
		$this->_module_settings->launcher = $launcher;

		$this->_module_settings->languages = $this->language_model->getLanguagesOfApp((int)$this->appid);

		# metadata
		$metadata = $this->metadata_model->getMetadata($launcher, '', '', $this->app);

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $this->appid AND itemtype = 'place' GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}
		//validation
		if($this->input->post('mytagsulselect') != null) {
			$postedtags = $this->input->post('mytagsulselect');
		} else {
			$postedtags = array();
		}

		# Get view
		$view = $this->_module_settings->views['add'];

		$data = array(
				'settings' => $this->_module_settings,
				'app' => $this->app,
				'metadata' => $metadata,
				'languages' => $this->_module_settings->languages,
				'data' => $place,
				'apptags' => $apptags,
				'postedtags' => $postedtags
			);
		$this->_module_settings->edit_url = 'places/edit';
		$content = $this->load->view($view, $data, true);

		// $href = $this->_module_settings->actions['edit']->href;
		// redirect(str_replace('--id--', $place->id, $href));
		$cdata = array(
			'content' => $content,
			'sidebar' => $this->load->view('c_sidebar', array($parentType => $parent), true),
			'crumb' => array($parent->name => "{$place->parentType}/view/".$place->parentId, $this->_module_settings->title => 'places/'.$place->parentType.'/'.$place->parentId),
			'modules' => $this->module_mdl->getModulesForMenu($place->parentType, $place->parentId, $this->app, 'places'),
		);
		$this->load->view('master', $cdata);
	}

	/**
	 * Delete request (redirects to browse)
	 */
	public function delete( $placeId )
	{
		$place = $this->_getPlaceStruct( $placeId );
		if($place === false) { redirect('/places'); }
		$parent = $this->_getParentObject( $place->parentType, $place->parentId );

		$singular =& $this->_module_settings->singular;
		if(!$this->_place_model->delete($place)) {
			$_SESSION['error'] = __('Could not delete %s #%d', $singular, $place->id);
		} else {
			$this->metadata_model->removeFieldsFromObject('place', $placeId);
			$_SESSION['message'] = __('%s #%d successfully deleted', $singular, $place->id);
		}
		redirect('places/'.$place->parentType.'/'.$place->parentId);
	}

    public function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'tc_places');
		$this->metadata_model->removeFieldsFromManyObjects('place', $selectedids);
    }

   	function removeimage($placeId) {
		$place = $this->_getPlaceStruct( $placeId );
		if($place === false) { redirect('/places'); }
		$parent = $this->_getParentObject( $place->parentType, $place->parentId );

		if(!file_exists($this->config->item('imagespath') . $place->imageurl)) redirect('places/'.$place->parentType.'/'.$place->parentId);

		// Delete image + generated thumbs
		unlink($this->config->item('imagespath') . $place->imageurl);
		$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

		$data = array('imageurl' => '');
		$this->general_model->update('tc_places', $place->id, $data);

		redirect('places/'.$place->parentType.'/'.$place->parentId);
	}
}
