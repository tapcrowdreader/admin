<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Translate extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	function setLang($lang) {
		setcookie('language', $lang,time()+3600*24*365, "/", 'admin.tapcrowd'.substr($_SERVER['SERVER_NAME'], strrpos($_SERVER['SERVER_NAME'], '.')));
		setcookie('language', $lang,time()+3600*24*365, "/", 'tapcrowd'.substr($_SERVER['SERVER_NAME'], strrpos($_SERVER['SERVER_NAME'], '.')));
	}
}
