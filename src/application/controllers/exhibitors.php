<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Exhibitors extends CI_Controller {

	private $_module_settings;
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('exhibitor_model');
		$this->load->model('group_model');
		$this->load->model('confbag_model');
		$this->load->model('premium_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Exhibitor',
			'plural' => 'Exhibitors',
			'title' => __('Exhibitors'),
			'parentType' => 'event',
			'parentId' => null,
			'browse_url' => 'exhibitors/--parentType--/--parentId--',
			'add_url' => 'exhibitors/add/--parentId--/--parentType--',
			'edit_url' => 'exhibitors/edit/--id--',
			'module_url' => 'module/editByController/exhibitors/--parentType--/--parentId--/0',
			'import_url' => 'import/exhibitors/--parentId--/--parentType--',
			'headers' => array(
				__('Name') => 'name',
				__('Order') => 'order'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'exhibitors/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'exhibitors/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false,
			'deletecheckedurl' => 'exhibitors/removemany/--parentId--/--parentType--',
			'premium' => 'exhibitor'
		);
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = 'exhibitors/event/'.$id;

		//for new system groups get main categorie and brand group
		$exhibitorcategoriesgroupid = $this->exhibitor_model->getMainCategorieGroup($id);
		// $exhibitorbrandsgroupid = $this->exhibitor_model->getMainBrandGroup($id);

		$exhibitors = $this->exhibitor_model->getExhibitorsByEventID($id);
		// $headers = array(__('Name') => 'name');

		// $cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => $exhibitors, 'headers' => $headers, 'exhibitorcategoriesgroupid' => $exhibitorcategoriesgroupid, 'exhibitorbrandsgroupid' => $exhibitorbrandsgroupid), TRUE);

		# Module edit url
		$this->_module_settings->parentId = $id;
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# Module import url
		$import_url =& $this->_module_settings->import_url;
		$import_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $import_url);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		// $btnBrands = (object)array(
		// 				'title' => __('Edit Brands'),
		// 				'href' => 'groups/view/'.$exhibitorbrandsgroupid.'/event/'.$id.'/exhibitors',
		// 				'icon_class' => 'icon-pencil',
		// 				'btn_class' => 'edit btn',
		// 				);
		// $this->_module_settings->extrabuttons['brands'] = $btnBrands;
		$btnCategories = (object)array(
						'title' => __('Edit Categories'),
						'href' => 'groups/view/'.$exhibitorcategoriesgroupid.'/event/'.$id.'/exhibitors',
						'icon_class' => 'icon-pencil',
						'btn_class' => 'edit btn',
						);
		$this->_module_settings->extrabuttons['categories'] = $btnCategories;

		$launcher = $this->module_mdl->getLauncher(2, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;


		#
		# Check for API call
		#
		$this->_handleAPIRequest($exhibitors);


		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($exhibitors)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'exhibitors');
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$exhibitors = $this->exhibitor_model->getExhibitorsByVenueID($id);


		#
		# Check for API call
		#
		$this->_handleAPIRequest($exhibitors);


		$headers = array(__('Name') => 'name');
		$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'data' => $exhibitors, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Catalog") => $this->uri->uri_string()));
		$this->load->view('master', $cdata);
	}

	function add($id, $type = 'event', $groupid = '') {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			$brands = $this->exhibitor_model->getBrandsByVenueID($id);
			$categories = $this->exhibitor_model->getCategoriesByVenueID($id);

			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			$launcher = $this->module_mdl->getLauncher(2, 'venue', $venue->id);
			$metadata = $this->metadata_model->getMetadata($launcher, 'venue', $venue->id, $app);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('selbrands[]', 'brands', '');
				$this->form_validation->set_rules('selcategories[]', 'categories', '');
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
                $this->form_validation->set_rules('address', 'address', 'trim');
                $this->form_validation->set_rules('email', 'email', 'trim');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description', 'trim');
                }

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {
					// Save exhibitor
					$data = array(
							"venueid"		=> $id,
							"name" 			=> set_value('name_'.  $app->defaultlanguage),
							"description" 	=> set_value('description_'.  $app->defaultlanguage),
                            "address"       => set_value('address'),
                            "email"         => set_value('email')
						);
					$res = $this->general_model->insert('exhibitor', $data);
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/exhibitorimages/".$res)){
						mkdir($this->config->item('imagespath') . "upload/exhibitorimages/".$res, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/exhibitorimages/'.$res;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('imageurl')) {
						$image = ""; //No image uploaded!
							if($_FILES['imageurl']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/exhibitorimages/'. $res . '/' . $returndata['file_name'];
					}

					$this->general_model->update('exhibitor', $res, array('imageurl' => $image));

                    //add translated fields to translation table
                    foreach($languages as $language) {
                        //description
                        $this->language_model->addTranslation('exhibitor', $res, 'name', $language->key, $this->input->post('name_'.$language->key));
                        $this->language_model->addTranslation('exhibitor', $res, 'description', $language->key, $this->input->post('description_'.$language->key));
                    }

					if($res != FALSE){
						// Save brands
						if($this->exhibitor_model->saveBrands($res, $this->input->post('selbrands'))){
							// Save categories
							if($this->exhibitor_model->saveCategories($res, $this->input->post('selcategories'))){
								$this->session->set_flashdata('event_feedback', __('The exhibitor has successfully been added'));
								_updateVenueTimeStamp($venue->id);
								if($error == '') {
									redirect('exhibitors/venue/'.$venue->id);
								} else {
									//image error
									redirect('exhibitors/edit/'.$res.'/venue?error=image');
								}
							} else {
								$error = __("Oops, something went wrong. Please try again.");
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

				}
			}



			#
			# Check for API call
			#
			$this->_handleAPIRequest(null, $error);


			$cdata['content'] 		= $this->load->view('c_exhibitor_add', array('app' => $app, 'venue' => $venue, 'brands' => $brands, 'categories' => $categories, 'error' => $error, 'languages' => $languages), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Exhibitors") => "exhibitors/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'exhibitors/event/'.$id;

			$maincat = $this->exhibitor_model->getMainCatGroup($event->id);

			$brandshtml = '';
			$catshtml = '';
			$brands = array();
			$categories = array();
			if($groupid != '' || $maincat != false) {
				$brandgroupid = $this->exhibitor_model->getMainBrandGroup($id);
				if($brandgroupid) {
					$brandshtml = $this->exhibitor_model->display_children($brandgroupid,0, $id);
				}

				$catsgroupid = $this->exhibitor_model->getMainCatGroup($id);
				if($catsgroupid) {
					$this->exhibitor_model->htmlarray = array();
					$catshtml = $this->exhibitor_model->display_children($catsgroupid,0, $id);
				}
			} else {
				$brands = $this->exhibitor_model->getBrandsByEventID($id);
				$categories = $this->exhibitor_model->getCategoriesByEventID($id);
			}

			$launcher = $this->module_mdl->getLauncher(2, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, '', '', $app);

			$premiumtitle = $this->premium_model->getTitle('exhibitor', 'event', $event->id);

			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'exhibitor' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'exhibitor'");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag;
				}
				$tags = $tagz;
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('selbrands[]', 'brands', '');
				$this->form_validation->set_rules('selcategories[]', 'categories', '');
				$this->form_validation->set_rules('booth', 'booth', 'trim');
				$this->form_validation->set_rules('tel', 'telephone', 'trim');
				$this->form_validation->set_rules('web', 'Website', 'trim|prep_url|valid_url|xss_clean');
            	$this->form_validation->set_rules('imageurl1', 'image', 'trim|callback_image_rules');
                $this->form_validation->set_rules('address', 'address', 'trim');
                $this->form_validation->set_rules('email', 'email', 'trim');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description', 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
                $this->form_validation->set_rules('confbagcontent', 'confbagcontent', 'trim');
                $this->form_validation->set_rules('premium', 'premium', 'trim');
                $this->form_validation->set_rules('order', 'order', 'trim');
                $this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
                $this->form_validation->set_rules('premiumtitle', 'premiumtitle', 'trim');


				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {
					// Save exhibitor
					$data = array(
							"eventid"		=> $id,
							"name" 			=> set_value('name_'.  $app->defaultlanguage),
							"booth" 		=> set_value('booth'),
							"description" 	=> set_value('description_'.  $app->defaultlanguage),
							"tel" 			=> set_value('tel'),
							"web"		 	=> checkhttp($this->input->post('web')),
                            "address"       => set_value('address'),
                            "email"         => set_value('email'),
                            "order"			=> $this->input->post("order")
						);
					$res = $this->general_model->insert('exhibitor', $data);

					//groupitems
					if($this->input->post('brandshidden') != null && $this->input->post('brandshidden') != '') {
						$groups = $this->input->post('brandshidden');
						$groups = explode('/',$groups);
						foreach($groups as $group) {
							if($group != '') {
								$groupitemdata = array(
										'appid'	=> $app->id,
										'eventid'	=> $id,
										'groupid'	=> $group,
										'itemtable'	=> 'exhibitor',
										'itemid'	=> $res
									);
								$this->general_model->insert('groupitem', $groupitemdata);
							}
						}
					}
					if($this->input->post('catshidden') != null && $this->input->post('catshidden') != '') {
						$groups = $this->input->post('catshidden');
						$groups = explode('/',$groups);
						foreach($groups as $group) {
							if($group != '') {
								$groupitemdata = array(
										'appid'	=> $app->id,
										'eventid'	=> $id,
										'groupid'	=> $group,
										'itemtable'	=> 'exhibitor',
										'itemid'	=> $res
									);
								$this->general_model->insert('groupitem', $groupitemdata);
							}
						}
					}

					if($res != FALSE){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/exhibitorimages/".$res)){
							mkdir($this->config->item('imagespath') . "upload/exhibitorimages/".$res, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/exhibitorimages/'.$res;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);

						$this->upload->initialize($configexlogo);
						if ($this->upload->do_upload('imageurl1')) {
							$returndata = $this->upload->data();
							$image1 = 'upload/exhibitorimages/'.$res.'/'.$returndata['file_name'];
							$this->general_model->update('exhibitor',$res,array('imageurl' => $image1));
						} else {
							if($_FILES['imageurl1']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						}

						if(!is_dir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id)){
							mkdir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id, 0755, TRUE);
						}
						$configconfbag['upload_path'] = $this->config->item('imagespath') .'upload/confbagfiles/event/'.$event->id;
						$configconfbag['allowed_types'] = 'JPG|jpg|jpeg|png|pdf|xls|xlsx|txt|doc|docx|ppt|pptx';
						$configconfbag['max_size'] = '50000';
						$this->load->library('upload', $configconfbag);

						$this->upload->initialize($configconfbag);
						if ($this->upload->do_upload('confbagcontent')) {
							$returndata = $this->upload->data();
							$confbagcontent = 'upload/confbagfiles/event/'.$event->id.'/'.$returndata['file_name'];
							$this->general_model->insert('confbag', array('eventid' => $event->id, 'itemtable' => 'exhibitor', 'tableid' => $res, 'documentlink' => $confbagcontent));
						} else {
							if($_FILES['confbagcontent']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						}

						//save premium
						$this->premium_model->save($this->input->post('premium'), 'exhibitor', $res, $this->input->post('extraline'), false, 'event', $event->id, $this->input->post('premiumorder'), $this->input->post('premiumtitle'));

						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$this->general_model->insert('tc_tag', array(
								'appid' => $app->id,
								'itemtype' => 'exhibitor',
								'itemid' => $res,
								'tag' => $tag
							));
						}

						// Save brands
						if($this->exhibitor_model->saveBrands($res, $this->input->post('selbrands'))){
							// Save categories
							if($this->exhibitor_model->saveCategories($res, $this->input->post('selcategories'))){

								//Uploads Images
								if(!is_dir($this->config->item('imagespath') ."upload/exhibitorimages/".$res)){
									mkdir($this->config->item('imagespath') ."upload/exhibitorimages/".$res, 0755, true);
								}

								$configpics['upload_path'] = $this->config->item('imagespath') .'upload/exhibitorimages/'.$res;
								$configpics['allowed_types'] = 'JPG|jpg|jpeg|png';
								$configpics['max_width']  = '2000';
								$configpics['max_height']  = '2000';
								$configpics['max_size']  = '1024';
								$this->load->library('upload', $configpics);

								$images = array();
								for($i = 1; $i <= 20; $i++){
									$this->upload->initialize($configpics);
									if ($this->upload->do_upload('imageurl'.$i)) {
										// successfully uploaded
										$returndata = $this->upload->data();
										$images['image'.$i] = 'upload/exhibitorimages/'.$res.'/'.$returndata['file_name'];
									}
								}
								//update item with images
								if(count($images) > 0){
									$this->general_model->update('exhibitor', $res, $images);
								}
                                //add translated fields to translation table
                                foreach($languages as $language) {
                                    //description
                                    $this->language_model->addTranslation('exhibitor', $res, 'name', $language->key, $this->input->post('name_'.$language->key));
                                    $this->language_model->addTranslation('exhibitor', $res, 'description', $language->key, $this->input->post('description_'.$language->key));
		  							foreach($metadata as $m) {
		  								if(_checkMultilang($m, $language->key, $app)) {
											$postfield = _getPostedField($m, $_POST, $_FILES, $language);
											if($postfield !== false) {
												if(_validateInputField($m, $postfield)) {
													_saveInputField($m, $postfield, 'exhibitor', $res, $app, $language);
												}
											}
										}
									}
                                }

								$this->session->set_flashdata('event_feedback', __('The exhibitor has successfully been added'));
								_updateTimeStamp($event->id);


								#
								# Check for API call
								#
								$this->_handleAPIRequest($res, $error);


								if($error == '') {
									redirect('exhibitors/event/'.$event->id);
								} else {
									//image error
									redirect('exhibitors/edit/'.$res.'/event?error=image');
								}
							} else {
								$error = __("Oops, something went wrong. Please try again.");
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

				}
			}

			//confbag
			$confbagactive = $this->confbag_model->checkActive($event->id);

			$cdata['content'] 		= $this->load->view('c_exhibitor_add', array('event' => $event, 'brands' => $brands, 'categories' => $categories, 'error' => $error, 'languages' => $languages, 'app' => $app, 'groupid' => $groupid, 'brandshtml' => $brandshtml, 'catshtml'	=> $catshtml, 'maincat' => $maincat, 'confbagactive' => $confbagactive, 'metadata' => $metadata, 'premiumtitle' => $premiumtitle, 'tags' => $tags, 'postedtags' => $postedtags), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "exhibitors/event/".$event->id, __("Add new") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "exhibitors/event/".$event->id, __("Add new") => $this->uri->uri_string());
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'exhibitors');
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function edit($id, $type = 'event', $groupid = '') {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			// EDIT EXHIBITOR
			$exhibitor = $this->exhibitor_model->getExhibitorByID($id);
			if($exhibitor == FALSE) redirect('venues');

			$brands = $this->exhibitor_model->getBrandsByVenueID($exhibitor->venueid);
			$categories = $this->exhibitor_model->getCategoriesByVenueID($exhibitor->venueid);

			$venue = $this->venue_model->getById($exhibitor->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('selbrands[]', 'selbrands', '');
				$this->form_validation->set_rules('selcategories[]', 'selcategories', '');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
                }
                $this->form_validation->set_rules('email', 'email', 'trim');
                $this->form_validation->set_rules('address', 'address', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/exhibitorimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/exhibitorimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/exhibitorimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('imageurl')) {
						$image = $ad->image; //No image uploaded!
						$error = $this->upload->display_errors();
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/exhibitorimages/'. $id . '/' . $returndata['file_name'];
					}

					// Save brands
					if($this->exhibitor_model->saveBrands($id, $this->input->post('selbrands'))){
						// Save categories

						if($this->exhibitor_model->saveCategories($id, $this->input->post('selcategories'))){
						// Save exhibitor
							$data = array(
									"name" 			=> set_value('name_'.  $app->defaultlanguage),
									"description" 	=> set_value('description_'.  $app->defaultlanguage),
									"imageurl"		=> $image,
                                    "address"       => set_value('address'),
                                    "email"         => set_value('email')
								);

							if($this->general_model->update('exhibitor', $id, $data)){
                                //add translated fields to translation table
                                foreach($languages as $language) {
									$nameSuccess = $this->language_model->updateTranslation('exhibitor', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
									if(!$nameSuccess) {
										$this->language_model->addTranslation('exhibitor', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
									}
									$descrSuccess = $this->language_model->updateTranslation('exhibitor', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
									if(!$descrSuccess) {
										$this->language_model->addTranslation('exhibitor', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
									}
                                }
								$this->session->set_flashdata('event_feedback', __('The exhibitor has successfully been updated'));
								_updateVenueTimeStamp($venue->id);
								redirect('exhibitors/venue/'.$venue->id);
							} else {
								$error = __("Oops, something went wrong. Please try again.");
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}


			#
			# Check for API call
			#
			$this->_handleAPIRequest($exhibitor, $error);


			$cdata['content'] 		= $this->load->view('c_exhibitor_edit', array('venue' => $venue, 'exhibitor' => $exhibitor, 'brands' => $brands, 'categories' => $categories, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Catalog") => "exhibitors/venue/".$venue->id, __("Edit: ") . $exhibitor->name => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			// EDIT EXHIBITOR
			$exhibitor = $this->exhibitor_model->getExhibitorByID($id);
			if($exhibitor == FALSE) redirect('events');
			
			$brands = $this->exhibitor_model->getBrandsByEventID($exhibitor->eventid);
			$categories = $this->exhibitor_model->getCategoriesByEventID($exhibitor->eventid);

			$event = $this->event_model->getbyid($exhibitor->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'exhibitors/view/'.$id;

			$premium = $this->premium_model->get('exhibitor', $exhibitor->id);
			$premiumtitle = $this->premium_model->getTitle('exhibitor', 'event', $event->id);

			$maincat = $this->exhibitor_model->getMainCatGroup($event->id);
			$brandshtml = '';
			$catshtml = '';
			$brands = array();
			$currentBrands = '';
			$currentCats = '';
			$categories = array();
			if($groupid != '' || $maincat != false) {
				$brandgroupid = $this->exhibitor_model->getMainBrandGroup($exhibitor->eventid);
				if($brandgroupid) {
					$brandshtml = $this->exhibitor_model->display_children($brandgroupid,0,$exhibitor->eventid, $id);
					$currentBrands = $this->exhibitor_model->groupids;
					$this->exhibitor_model->groupids = '';
				}

				$catsgroupid = $this->exhibitor_model->getMainCatGroup($exhibitor->eventid);
				if($catsgroupid) {
					$this->exhibitor_model->htmlarray = array();
					$catshtml = $this->exhibitor_model->display_children($catsgroupid,0,$exhibitor->eventid, $id);
					$currentCats = $this->exhibitor_model->groupids;
					$this->exhibitor_model->groupids = '';
				}
			} else {
				$brands = $this->exhibitor_model->getBrandsByEventID($exhibitor->eventid);
				$categories = $this->exhibitor_model->getCategoriesByEventID($exhibitor->eventid);
			}

			$launcher = $this->module_mdl->getLauncher(2, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, 'exhibitor', $exhibitor->id, $app);

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'exhibitor' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'exhibitor'");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag;
				}
				$tags = $tagz;
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('selbrands[]', 'brands', '');
				$this->form_validation->set_rules('selcategories[]', 'categories', '');
				$this->form_validation->set_rules('booth', 'booth', 'trim');

                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description', 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
                $this->form_validation->set_rules('address', 'address', 'trim');
				$this->form_validation->set_rules('tel', 'telephone', 'trim');
				$this->form_validation->set_rules('web', 'Website', 'trim|prep_url|valid_url|xss_clean');
                $this->form_validation->set_rules('email', 'email', 'trim');
                $this->form_validation->set_rules('confbagcontent', 'confbagcontent', 'trim');
                $this->form_validation->set_rules('premium', 'premium', 'trim');
                for($i = 1;$i <= 20; $i++) {
                	$this->form_validation->set_rules('imageurl'.$i, 'image', 'trim|callback_image_rules');
                }
                $this->form_validation->set_rules('order', 'order', 'trim');
                $this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
                $this->form_validation->set_rules('premiumtitle', 'premiumtitle', 'trim');

				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/exhibitorimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/exhibitorimages/".$id, 0755, true);
				}

				$configpics['upload_path'] = $this->config->item('imagespath') .'upload/exhibitorimages/'.$id;
				$configpics['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configpics['max_width']  = '2000';
				$configpics['max_height']  = '2000';
				$configpics['max_size']  = '1024';
				$this->load->library('upload', $configpics);

				$this->upload->initialize($configpics);
				if ($this->upload->do_upload('imageurl')) {
					$returndata = $this->upload->data();
					$image1 = 'upload/exhibitorimages/'.$id.'/'.$returndata['file_name'];
					$this->general_model->update('exhibitor',$id,array('imageurl' => $image1));
				} else {
					if($_FILES['imageurl']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				}

				if(!is_dir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id)){
					mkdir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id, 0755, TRUE);
				}
				$configconfbag['upload_path'] = $this->config->item('imagespath') .'upload/confbagfiles/event/'.$event->id;
				$configconfbag['allowed_types'] = 'JPG|jpg|jpeg|png|pdf|xls|xlsx|txt|doc|docx|ppt|pptx';
				$configconfbag['max_size'] = '50000';
				$this->load->library('upload', $configconfbag);

				$this->upload->initialize($configconfbag);
				if ($this->upload->do_upload('confbagcontent')) {
					$returndata = $this->upload->data();
					$confbagcontent = 'upload/confbagfiles/event/'.$event->id.'/'.$returndata['file_name'];
					$this->general_model->insert('confbag', array('eventid' => $event->id, 'itemtable' => 'exhibitor', 'tableid' => $id, 'documentlink' => $confbagcontent));
				} else {
					if($_FILES['confbagcontent']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				}

				if($this->form_validation->run() == FALSE || $error != ''){
					$error .= validation_errors();
				}  else {
					if($groupid != 0 || $maincat != false) {
						$this->exhibitor_model->deleteGroupItemsFromExhibitor($exhibitor->id);
						//groupitems
						if($this->input->post('brandshidden') != null && $this->input->post('brandshidden') != '') {
							$groups = $this->input->post('brandshidden');
							$groups = explode('/',$groups);
							foreach($groups as $group) {
								if($group != '') {
									$groupitemdata = array(
											'appid'	=> $app->id,
											'eventid'	=> $exhibitor->eventid,
											'groupid'	=> $group,
											'itemtable'	=> 'exhibitor',
											'itemid'	=> $exhibitor->id
										);
									$this->general_model->insert('groupitem', $groupitemdata);
								}
							}
						}
						if($this->input->post('catshidden') != null && $this->input->post('catshidden') != '') {
							$groups = $this->input->post('catshidden');
							$groups = explode('/',$groups);
							foreach($groups as $group) {
								if($group != '') {
									$groupitemdata = array(
											'appid'	=> $app->id,
											'eventid'	=> $exhibitor->eventid,
											'groupid'	=> $group,
											'itemtable'	=> 'exhibitor',
											'itemid'	=> $exhibitor->id
										);
									$this->general_model->insert('groupitem', $groupitemdata);
								}
							}
						}
					} else {
						// Save brands
						if($this->exhibitor_model->saveBrands($id, $this->input->post('selbrands'))){
							// Save categories
							if($this->exhibitor_model->saveCategories($id, $this->input->post('selcategories'))){

							} else {
								$error = __("Oops, something went wrong. Please try again.");
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
					// Save exhibitor
					$data = array(
							"name" 			=> set_value('name_'.  $app->defaultlanguage),
							"booth" 		=> set_value('booth'),
							"description" 	=> set_value('description_'.  $app->defaultlanguage),
							"tel" 			=> set_value('tel'),
							"web"		 	=> checkhttp($this->input->post('web')),
                            "address"       => set_value('address'),
                            "email"         => set_value('email'),
                            "order"			=> $this->input->post("order")
						);

					if($this->general_model->update('exhibitor', $id, $data)){
						//save premium
						$this->premium_model->save($this->input->post('premium'), 'exhibitor', $exhibitor->id, $this->input->post('extraline'), $premium, 'event', $event->id, $this->input->post('premiumorder'), $this->input->post('premiumtitle'));

						$this->db->where('itemtype', 'exhibitor');
						$this->db->where('itemid', $id);
						$this->db->delete('tc_tag');
						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$this->general_model->insert('tc_tag', array(
								'appid' => $app->id,
								'itemtype' => 'exhibitor',
								'itemid' => $id,
								'tag' => $tag
							));
						}

						//update item with images
						if(isset($images) && count($images) > 0){
							$this->general_model->update('exhibitor', $id, $images);
						}

                        //add translated fields to translation table
                        foreach($languages as $language) {
							$nameSuccess = $this->language_model->updateTranslation('exhibitor', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
							if(!$nameSuccess) {
								$this->language_model->addTranslation('exhibitor', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
							}
							$descrSuccess = $this->language_model->updateTranslation('exhibitor', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							if(!$descrSuccess) {
								$this->language_model->addTranslation('exhibitor', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							}

							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
									$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'exhibitor', $id, $app, $language);
										}
									}
								}
							}
                        }

						$this->session->set_flashdata('event_feedback', __('The exhibitor has successfully been updated'));
						_updateTimeStamp($event->id);
						#
						# Check for API call
						#
						$this->_handleAPIRequest($exhibitor->id, $error);
						redirect('exhibitors/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			#
			# Check for API call
			#
			$this->_handleAPIRequest($exhibitor->id, $error);
			//confbag
			$confbagactive = $this->confbag_model->checkActive($event->id);
			$exhibitorconfbag = false;
			if($confbagactive) {
				$exhibitorconfbag = $this->confbag_model->getFromObject('exhibitor',$exhibitor->id);
			}

			$cdata['content'] 		= $this->load->view('c_exhibitor_edit', array('event' => $event, 'exhibitor' => $exhibitor, 'brands' => $brands, 'categories' => $categories, 'error' => $error, 'languages' => $languages, 'app' => $app, 'groupid' => $groupid, 'brandshtml' => $brandshtml, 'catshtml'	=> $catshtml, 'maincat' => $maincat, 'currentBrands' => $currentBrands, 'currentCats' => $currentCats, 'confbagactive' => $confbagactive, 'exhibitorconfbag' => $exhibitorconfbag, 'premium' => $premium, 'metadata' => $metadata, 'premiumtitle' => $premiumtitle, 'tags' => $tags, 'postedtags' => $postedtags), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "exhibitors/event/".$event->id, __("Edit: ") . $exhibitor->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "exhibitors/event/".$event->id, __("Edit: ") . $exhibitor->name => $this->uri->uri_string());
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'exhibitors');
			$this->load->view('master', $cdata);
		}

	}

	function delete($id, $type = 'event') {
		if($type == 'venue') {
			$exhibitor = $this->exhibitor_model->getExhibitorByID($id);
			$venue = $this->venue_model->getbyid($exhibitor->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
            $this->language_model->removeTranslations('exhibitor', $id);
			if($this->general_model->remove('exhibitor', $id)){
				$this->db->delete('exhibrand', array('exhibitorid' => $id));
				$this->db->delete('exhicat', array('exhibitorid' => $id));
				$this->session->set_flashdata('event_feedback', __('The exhibitor has successfully been deleted'));
				_updateVenueTimeStamp($exhibitor->venueid);
				redirect('exhibitors/venue/'.$exhibitor->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('exhibitors/venue/'.$exhibitor->venueid);
			}
		} else {
			$exhibitor = $this->exhibitor_model->getExhibitorByID($id);
			$event = $this->event_model->getbyid($exhibitor->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
            $this->language_model->removeTranslations('exhibitor', $id);
            //remove groupitems of exhibitor if they exist
            $this->load->model('groupitem_model');
            $this->groupitem_model->removeGroupitemsOfObject('exhibitor', $id);
			if($this->general_model->remove('exhibitor', $id)){
				$this->exhibitor_model->removePremium($id);
				$this->db->delete('exhibrand', array('exhibitorid' => $id));
				$this->db->delete('exhicat', array('exhibitorid' => $id));
				$this->session->set_flashdata('event_feedback', __('The exhibitor has successfully been deleted'));
				_updateTimeStamp($exhibitor->eventid);
				redirect('exhibitors/event/'.$exhibitor->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('exhibitors/event/'.$exhibitor->eventid);
			}
		}
	}

	function removeimage($id, $type = 'event') {
		$item = $this->exhibitor_model->getExhibitorByID($id);
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($item->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->imageurl)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('exhibitor', $id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('exhibitors/venue/'.$venue->id);
		} else {
			$event = $this->event_model->getbyid($item->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->imageurl)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('exhibitor', $id, $data);

			_updateTimeStamp($event->id);
			redirect('exhibitors/event/'.$event->id);
		}
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'exhibitor');
    }


	/**
	 * Handles API requests
	 *
	 * @param array $exhibitors
	 */
	protected function _handleAPIRequest( $data, $errors = null )
	{
		# Verify Request Accept header
		if($_SERVER['HTTP_ACCEPT'] != 'application/json') return;

		# Parse errors
		if(!empty($errors)) {
			echo json_encode(array('error' => strip_tags($errors)));
			exit;
		}

		# Cleanup the $exhibitors data
		$trash = array('username','password','mapid','y1','x1','x2','y2');

		# Verify empty image params && Trash bloated params
		$cleanup = function( &$exh ) use($trash)
		{
			$i = 20; do {
				if(($k = "imagedescription$i") && empty($exh->$k)) $trash[] = $k;
				if(($k = "image$i") && empty($exh->$k)) $trash[] = $k;
			} while($i--);
			foreach($trash as $k) unset($exh->$k);
		};
		if(is_array($data)) array_walk($data, $cleanup); else $cleanup($data);

		# Output json encoded data
		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($data);
		exit;
	}

}