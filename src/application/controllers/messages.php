<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Messages extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }

		$this->load->model('users_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Message',
			'plural' => 'Messages',
			'title' => __('Messages'),
			'parentType' => 'event',
			'parentId' => null,
			'module_url' => 'module/editByController/messages/--parentType--/--parentId--/0',
			'headers' => array(
			),
			'actions' => array(
			),
			'views' => array(
				'list' => 'c_messages',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false,
		);
	}
	
	function event($id) {
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->load->library('form_validation');

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);

		$users = $this->users_model->getUserByAppId($app->id);

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('sender', 'sender', 'trim|required');
			$this->form_validation->set_rules('message', 'message', 'trim|required');
			$this->form_validation->set_rules('title', 'title', 'trim|required');

			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {
				require_once(APPPATH.'libraries/ApnsPHP/Autoload.php');	

			    $androidapiKey = "AIzaSyBCeqS1Ln_lwZA8ZavvIi5qNQPTNjwcmt8";
			    $androidurl = 'https://android.googleapis.com/gcm/send';
				foreach($users as $user) {
					$message = array(
							'appid' => $app->id,
							'senderuserid' => $this->input->post('sender'),
							'recipientuserid' => $user->id,
							'title' => $this->input->post('title'),
							'description' => $this->input->post('message'),
						);
					$messageid = $this->general_model->insert('message', $message);

					$recipient = $user;
					$sender = $this->users_model->getUserById($this->input->post('sender'));
					$title = $this->input->post('title');
					$description = $this->input->post('message');

					$pemcontentArray = array();
					$deviceids = $this->db->query("SELECT udid FROM user_udid WHERE user_id = $recipient->id");
					if($deviceids->num_rows() > 0) {
						$deviceids = $deviceids->result();
						foreach($deviceids as $deviceid) {
							// try ios
							$certificateQuery = $this->db->query("SELECT pc.id, pc.content, push.token FROM push 
								INNER JOIN appenvironment ae ON ae.appid = push.appid AND ae.environmentname = push.environment
								INNER JOIN push_certificates pc ON pc.id = ae.push_certificateid 
								WHERE push.deviceid = ? AND push.appid = ? AND push.environment = 'live' LIMIT 1", array($deviceid->udid, $app->id));
							if($certificateQuery->num_rows() == 0) {
								$certificateQuery = $this->db->query("SELECT pc.id, pc.content, push.token FROM push 
									INNER JOIN appenvironment ae ON ae.appid = push.appid AND ae.environmentname = push.environment
									INNER JOIN push_certificates pc ON pc.id = ae.push_certificateid 
									WHERE push.deviceid = ? AND push.appid = ? AND push.environment = 'tapcrowd_demo' LIMIT 1", array($deviceid->udid, $app->id));
							}

							if($certificateQuery->num_rows() > 0) {
								$certificateResult = $certificateQuery->row();
								$handle = fopen("certificate.pem", "w");
								fwrite($handle, $certificateResult->content);
								fclose($handle);

								$push = new ApnsPHP_Push(
					            ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
					            "certificate.pem"
					            );

					            $push->connect();
				                $pushmessage = new ApnsPHP_Message($certificateResult->token);
			                    $pushmessage->setText($sender->name . ': '. $title.' - '.$description);
			                    $pushmessage->setCustomProperty('payload', json_decode(json_encode(array('messageid' => $messageid))));
			                    $pushmessage->setSound();
				                $pushmessage->setCustomIdentifier("dev-".$certificateResult->token);
				                $pushmessage->setExpiry(30);
				                $push->add($pushmessage);

					            $aErrorQueue = $push->getErrors();
					            if (!empty($aErrorQueue)) {
					                mail('jens@tapcrowd.com', 'Push Cue Error', 'id: ' . $row['id'] . '<br/>'. $aErrorQueue, $headers);
					            }

					            $push->send();
					            $push->disconnect();
					            unlink("certificate.pem");
								$this->db->query("UPDATE message SET timestampsent = now() WHERE id = $messageid");
							} else {
								// try android
								$certificateQuery = $this->db->query("SELECT pushandroid.token FROM pushandroid 
									WHERE pushandroid.deviceid = ? AND pushandroid.appid = ? AND pushandroid.environment = 'live' LIMIT 1", array($deviceid->udid, $app->id));
								if($certificateQuery->num_rows() == 0) {
									$certificateQuery = $this->db->query("SELECT pushandroid.token FROM pushandroid 
										WHERE pushandroid.deviceid = ? AND pushandroid.appid = ? AND pushandroid.environment = 'tapcrowd_demo' LIMIT 1", array($deviceid->udid, $app->id));
								}

								if($certificateQuery->num_rows() > 0) {
									$fields = array(
										'registration_ids'  => array($certificateQuery->row()->token),
										'data'              => array( "message" => utf8_encode($sender->name . ': '. $title.' - '.$description) ),
									);

									$headers = array( 
										'Authorization: key=' . $androidapiKey,
										'Content-Type: application/json'
									);

									$ch = curl_init();
									curl_setopt( $ch, CURLOPT_URL, $androidurl );
									curl_setopt( $ch, CURLOPT_POST, true );
									curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
									curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
									curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
									$curlres = curl_exec($ch);
									curl_close($ch);
								}
							}
						}
					}
				}
			}
		}

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array(), 'users' => $users), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'messages');
		$this->load->view('master', $cdata);
	}	
}
