<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Flashlight extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = "event/index/" . $id;

		$headers = array(__('Name') => 'name');

		$cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => array(), 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Flashlight") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, __("Flashlight") => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'flashlight');
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->iframeurl = "venue/index/" . $id;

		$headers = array(__('Name') => 'name');

		$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'data' => array(), 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']		= checkBreadcrumbsVenue($app, array($venue->name => 'venue/view/'.$id, __('Flashlight') => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'flashlight');
		$this->load->view('master', $cdata);
	}
}
