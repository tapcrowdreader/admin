<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Euroclear extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function result($id) {
		$headers = array();
		$data = array();
		$res = $this->db->query("SELECT  fsf.formsubmissionid, fsf.formfieldid, fsf.value, ff.formscreenid,ff.label FROM formsubmissionfield fsf INNER JOIN formfield ff ON ff.id = fsf.formfieldid WHERE ff.formscreenid = $id")->result();
		foreach($res as $r) {
			if(!isset($headers[$r->formfieldid])) {
				$headers[$r->formfieldid] = $r->label;
			}

			$data[$r->formsubmissionid][$r->formfieldid] = $r->value;
		}

		$filename = 'Form-' . $id . '.xls';
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Form');
		$rowCounter = 1;
		$fieldletter = 'A';
		foreach($headers as $key => $value) {
			$this->excel->getActiveSheet()->setCellValue($fieldletter . $rowCounter, $value);
			$fieldletter++;	

		}
		$rowCounter++;
		foreach($data as $row) {
			$fieldletter = 'A'; // Excel header counter
			foreach( $row as $key=>$value ){
				$this->excel->getActiveSheet()->setCellValue($fieldletter . $rowCounter, $value);
				$fieldletter++;		
			}
			$rowCounter++;
		}

		header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
	}
}