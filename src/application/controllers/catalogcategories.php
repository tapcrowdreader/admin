<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Catalogcategories extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('catalogcategory_model');
	}
	
	//php 4 constructor
	function Catalogcategories() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('catalogcategory_model');
	}
	
	function index() {
		$this->event();
	}
	
	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');
		
		$event = $this->event_model->getbyid($id);
		if($event == FALSE) redirect('events');
		$app = _actionAllowed($event, 'event','returnApp');
		
		$categories = $this->catalogcategory_model->getCatalogCategoriesByEventID($id);
		$headers = array(__('Name') => 'name');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => $categories, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Catalog Categories") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}
	
	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		
		$categories = $this->catalogcategory_model->getCatalogcategoriesByVenueID($id);
		$headers = array(__('Name') => 'name');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'data' => $categories, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Catalog Categories") => $this->uri->uri_string()));
		$this->load->view('master', $cdata);
	}
	
	function add($id, $type = 'event') {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
			
			$venue = $this->venue_model->getbyid($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					// Save exhibitor
					$data = array( 
							"venueid"		=> $id,
							"name" 			=> set_value('name')
						);
					$res = $this->general_model->insert('catalogcategory', $data);
					
					if($res != FALSE){
						$this->session->set_flashdata('event_feedback', __('The category has successfully been added'));
						redirect('catalogcategories/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
					
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_catalogcategory_add', array('venue' => $venue, 'error' => $error), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Categories") => "catalogcategories/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					// Save exhibitor
					$data = array( 
							"eventid"		=> $id,
							"name" 			=> set_value('name')
						);
					$res = $this->general_model->insert('catalogcategory', $data);

					if($res != FALSE){
						$this->session->set_flashdata('event_feedback', __('The category has successfully been added'));
						_updateTimeStamp($event->id);
						redirect('catalogcategories/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

				}
			}

			$cdata['content'] 		= $this->load->view('c_catalogcategory_add', array('event' => $event, 'error' => $error), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Categories") => "catalogcategories/event/".$event->id, __("Add new") => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}
	
	function edit($id, $type = 'event') {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
			
			// EDIT EXHIBITOR
			$category = $this->catalogcategory_model->getCatalogCategoryByID($id);
			if($category == FALSE) redirect('venues');
			
			$venue = $this->venue_model->getbyid($category->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array( 
							"venueid"		=> $venue->id,
							"name" 			=> set_value('name')
						);
					
					if($this->general_model->update('catalogcategory', $id, $data)){
						$this->session->set_flashdata('event_feedback', __('The category has successfully been updated'));
						redirect('catalogcategories/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_catalogcategory_edit', array('venue' => $venue, 'category' => $category, 'error' => $error), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Categories") => "catalogcategories/venue/".$venue->id, __("Edit: ") . $category->name => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			// EDIT EXHIBITOR
			$category = $this->catalogcategory_model->getCatalogCategoryByID($id);
			if($category == FALSE) redirect('events');

			$event = $this->event_model->getbyid($category->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array( 
							"eventid"		=> $event->id,
							"name" 			=> set_value('name')
						);

					if($this->general_model->update('catalogcategory', $id, $data)){
						$this->session->set_flashdata('event_feedback', __('The category has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('catalogcategories/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_catalogcategory_edit', array('event' => $event, 'category' => $category, 'error' => $error), TRUE);
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Categories") => "catalogcategories/event/".$event->id, __("Edit: ") . $category->name => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}
	
	function delete($id, $type = 'event') {
		if($type == 'venue') {
			$category = $this->catalogcategory_model->getCatalogCategoryByID($id);
			$venue = $this->venue_model->getbyid($category->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($this->general_model->remove('catalogcategory', $id)){
				$this->db->delete('catecat', array('catalogcategoryid' => $id));
				$this->session->set_flashdata('event_feedback', __('The category has successfully been deleted'));
				redirect('catalogcategories/venue/'.$category->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('catalogcategories/venue/'.$category->venueid);
			}
		} else {
			$category = $this->catalogcategory_model->getCatalogCategoryByID($id);
			$event = $this->event_model->getbyid($category->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			if($this->general_model->remove('catalogcategory', $id)){
				$this->db->delete('catecat', array('catalogcategoryid' => $id));
				$this->session->set_flashdata('event_feedback', __('The category has successfully been deleted'));
				_updateTimeStamp($category->eventid);
				redirect('catalogcategories/event/'.$category->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('catalogcategories/event/'.$category->eventid);
			}
		}
	}

}