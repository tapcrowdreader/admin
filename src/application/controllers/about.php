<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class About extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}
	
	function edit($id, $type='event', $action) {
		if($type == 'venue') {
			$venue = $this->venue_model->getById($id); 
			$app = _actionAllowed($venue, 'venue','returnApp');
		} elseif($type == 'event') {
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
		}
        $languages = $this->language_model->getLanguagesOfApp($app->id);
		$this->load->library('form_validation');
		$error = "";
		
		if($action == 'info') {
			$this->iframeurl = 'venue/info/' . $id;
		} else {
			$this->iframeurl = 'venue/'.$action . '/' . $id;
		}

		if($action == 'info') {
			if($type == 'venue') {
				$launcher = $this->module_mdl->getLauncher(21, 'venue', $venue->id);
				$metadata = $this->metadata_model->getMetadata($launcher, 'venue', $venue->id, $app);
				if($this->input->post('postback') == "postback") {
					$this->_editInfoVenue($languages, $app, $id, $metadata);
				}
				$cdata['content'] 		= $this->load->view('c_venue_info_edit', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'metadata' => $metadata), TRUE);
				$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Info") => "about/edit/".$venue->id.'/venue/info'));
				$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
				$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'about');
				$this->load->view('master', $cdata);
			}
		} elseif($action == 'location') {
			if($type == 'venue') {	
				$launcher = $this->module_mdl->getLauncher(22, 'venue', $venue->id);
				$metadata = $this->metadata_model->getMetadata($launcher, 'venue', $venue->id, $app);		
				if($this->input->post('postback') == "postback") {
					$this->_editLocationVenue($languages, $app, $id, $metadata);
				}
				$cdata['content'] 		= $this->load->view('c_venue_location_edit', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'metadata' => $metadata), TRUE);
				$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Location") => "about/edit/".$venue->id.'/venue/location'));
				$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
				$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'location');
				$this->load->view('master', $cdata);
			}
		} elseif($action == 'contact') {
			if($type == 'venue') {		
				$launcher = $this->module_mdl->getLauncher(23, 'venue', $venue->id);
				$metadata = $this->metadata_model->getMetadata($launcher, 'venue', $venue->id, $app);		
				if($this->input->post('postback') == "postback") {
					$this->_editContactVenue($languages, $app, $id, $metadata);
				}
				$cdata['content'] 		= $this->load->view('c_venue_contact_edit', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'metadata' => $metadata), TRUE);
				$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Contact") => "about/edit/".$venue->id.'/venue/contact'));
				$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
				$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'contact');
				$this->load->view('master', $cdata);
			}
		}
	}
	
	function _editInfoVenue($languages, $app, $venueid, $metadata) {
		$this->load->library('form_validation');
		foreach($languages as $language) {
			$this->form_validation->set_rules('w_name_'.$language->key, 'w_name_'.$language->key, 'trim');
			$this->form_validation->set_rules('w_info_'.$language->key, 'w_info_'.$language->key, 'trim');
			foreach($metadata as $m) {
				if(_checkMultilang($m, $language->key, $app)) {
					foreach(_setRules($m, $language) as $rule) {
						$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
					}
				}
			}
		}
		$this->form_validation->set_rules('imageurl1', 'imageurl1', 'trim');


		$venue = $this->venue_model->getbyid($venueid);
		$image = $venue->image1;

		if($this->form_validation->run() == FALSE){
			$error = __("Some fields are missing.");
		} else {
			//Uploads Images
			if(!is_dir("upload/venueimages/".$venueid)){
				mkdir("upload/venueimages/".$venueid, 0755);
			}

			$configpics['upload_path'] = $this->config->item('imagespath') . 'upload/venueimages/'.$venueid;
			$configpics['allowed_types'] = 'JPG|jpg|jpeg|png';
			$configpics['max_width']  = '2000';
			$configpics['max_height']  = '2000';
			$this->load->library('upload', $configpics);
			$this->upload->initialize($configpics);
			if ($this->upload->do_upload('imageurl1')) {
				// successfully uploaded
				$returndata = $this->upload->data();
				$image = 'upload/venueimages/'.$venueid.'/'.$returndata['file_name'];
			}
			if($this->input->post('w_name_'. $app->defaultlanguage) == null || $this->input->post('w_name_'. $app->defaultlanguage) == '') {
				$name = $venue->name;
			} else {
				$name = $this->input->post('w_name_'. $app->defaultlanguage);
			}
			$data = array(
					'name'			=> $name,
					'info'			=> $this->input->post('w_info_'. $app->defaultlanguage),
					'image1'		=> $image
				);

			if($this->general_model->update('venue', $venueid, $data)){
				//add translated fields to translation table
				foreach($languages as $language) {
					if($this->input->post('w_name_'. $app->defaultlanguage) != null && $this->input->post('w_name_'. $app->defaultlanguage) != '') {
						$nameSuccess = $this->language_model->updateTranslation('venue', $venueid, 'name', $language->key, $this->input->post('w_name_'.$language->key));
						if(!$nameSuccess) {
							$this->language_model->addTranslation('venue', $venueid, 'name', $language->key, $this->input->post('w_name_'.$language->key));
						}
					}
					$descrSuccess = $this->language_model->updateTranslation('venue', $venueid, 'info', $language->key, $this->input->post('w_info_'.$language->key));
					if(!$descrSuccess) {
						$this->language_model->addTranslation('venue', $venueid, 'info', $language->key, $this->input->post('w_info_'.$language->key));
					}

					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							$postfield = _getPostedField($m, $_POST, $_FILES, $language);
							if($postfield !== false) {
								if(_validateInputField($m, $postfield)) {
									_saveInputField($m, $postfield, 'venue', $venueid, $app, $language);
								}
							}
						}
					}
				}

				$this->session->set_flashdata('event_feedback', __('Your changes have successfully been saved.'));
				_updateVenueTimeStamp($venueid);
				redirect('venue/view/'.$venueid);
			} else {
				$error = __("Oops, something went wrong, please try again.");
			}
		}
	}
	
	function _editLocationVenue($languages, $app, $venueid, $metadata) {
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$this->form_validation->set_rules('w_address', 'w_address', 'trim');
		$this->form_validation->set_rules('w_lat', 'w_lat', 'trim');
		$this->form_validation->set_rules('w_lon', 'w_lon', 'trim');
		foreach($languages as $language) {
			$this->form_validation->set_rules('txt_travelinfo_'.$language->key, 'txt_travelinfo_'.$language->key, 'trim');
			foreach($metadata as $m) {
				if(_checkMultilang($m, $language->key, $app)) {
					foreach(_setRules($m, $language) as $rule) {
						$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
					}
				}
			}
		}

		if($this->form_validation->run() == FALSE){
			$error = "Some fields are missing.";
		} else {
			$data = array(
						'address'		=> $this->input->post('w_address'),
						'lat'			=> $this->input->post('w_lat'),
						'lon'			=> $this->input->post('w_lon'),
						'travelinfo' 	=> $this->input->post('txt_travelinfo_'.$app->defaultlanguage)
				);
			if($this->general_model->update('venue', $venueid, $data)){
				foreach($languages as $language) {
					$travelSuccess = $this->language_model->updateTranslation('venue', $venueid, 'travelinfo', $language->key, $this->input->post('txt_travelinfo_'.$language->key));
					if(!$travelSuccess) {
						$this->language_model->addTranslation('venue', $venueid, 'travelinfo', $language->key, $this->input->post('txt_travelinfo_'.$language->key));
					}
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							$postfield = _getPostedField($m, $_POST, $_FILES, $language);
							if($postfield !== false) {
								if(_validateInputField($m, $postfield)) {
									_saveInputField($m, $postfield, 'venue', $venueid, $app, $language);
								}
							}
						}
					}
				}

				$this->session->set_flashdata('event_feedback', __('Your changes have successfully been saved.'));
				_updateVenueTimeStamp($venueid);
				redirect('venue/view/'.$venueid);
			} else {
				$error = __("Oops, something went wrong, please try again.");
			}
		}
	}
	
	function _editContactVenue($languages, $app, $venueid, $metadata) {
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$this->form_validation->set_rules('w_address', 'w_address', 'trim');
		$this->form_validation->set_rules('w_lat', 'w_lat', 'trim');
		$this->form_validation->set_rules('w_lon', 'w_lon', 'trim');
		$this->form_validation->set_rules('w_tel', 'w_tel', 'trim');
		$this->form_validation->set_rules('w_email', 'email', 'trim|valid_email');
		$this->form_validation->set_rules('website', 'website', 'trim|prep_url|valid_url|xss_clean');
		foreach($languages as $language) {
			foreach($metadata as $m) {
				if(_checkMultilang($m, $language->key, $app)) {
					foreach(_setRules($m, $language) as $rule) {
						$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
					}
				}
			}
		}

		if($this->form_validation->run() == FALSE){
			$error = "Some fields are missing.";
		} else {
			$data = array(
					'address'		=> $this->input->post('w_address'),
					'lat'			=> $this->input->post('w_lat'),
					'lon'			=> $this->input->post('w_lon'),
					'telephone'		=> $this->input->post('w_tel'),
					'email'			=> $this->input->post('w_email'),
					'website'		=> $this->input->post('website')
				);
			if($this->general_model->update('venue', $venueid, $data)){
				foreach($languages as $language) {
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							$postfield = _getPostedField($m, $_POST, $_FILES, $language);
							if($postfield !== false) {
								if(_validateInputField($m, $postfield)) {
									_saveInputField($m, $postfield, 'venue', $venueid, $app, $language);
								}
							}
						}
					}
				}
				$this->session->set_flashdata('event_feedback', __('Your changes have successfully been saved.'));
				_updateVenueTimeStamp($venueid);
				redirect('venue/view/'.$venueid);
			} else {
				$error = __("Oops, something went wrong, please try again.");
			}
		}
	}
	
	function venue($id) {
		redirect('venue/edit/'.$id);
	}

	function event($id) {
		redirect('event/edit/'.$id);
	}

	function app($id) {
		redirect('apps/info/'.$id);
	}
}