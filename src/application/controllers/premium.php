<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Premium extends CI_Controller {

	private $_module_settings;
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('exhibitor_model');
		$this->load->model('premium_model');
		$this->load->model('sessions_model');
		$this->load->model('catalog_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Premium item',
			'plural' => 'Premium items',
			'title' => __('Premium items'),
			'parentType' => 'event',
			'browse_url' => 'premium/--parentType--/--parentId--/--type--',
			'add_url' => 'premium/add/--parentId--/--parentType--/--type--',
			'edit_url' => 'premium/edit/--id--/--type--',
			'headers' => array(
				__('Name') => 'name'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'premium/edit/--id--/--type--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'premium/delete/--id--/--type--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false,
			'deletecheckedurl' => 'premium/removemany/--parentId--/--parentType--/--type--'
		);
	}

	function index() {
		$this->event();
	}

	function event($id, $type) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		if($type == 'exhibitor') {
			$objects = $this->exhibitor_model->getPremiumExhibitorsByEventID($id);
			$launcher = $this->module_mdl->getLauncher(2, 'event', $event->id);
		} elseif($type == 'session') {
			$objects = $this->sessions_model->getPremiumSessionsByEventID($id);
			$launcher = $this->module_mdl->getLauncher(10, 'event', $event->id);
		}

		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--', '--type--'), array('event', $id, $type), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--type--', $type, $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--type--', $type, $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--','--type--'), array('event', $id, $type), $deletecheckedurl);

		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = 'Premium: ' . $launcher->title;

		$this->iframeurl = $launcher->controller.'/event/'.$id;
		
		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($objects)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $launcher->title => $launcher->module.'/event/'.$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $launcher->title => $launcher->controller.'/event/'.$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, $launcher->controller);
		$this->load->view('master', $cdata);
	}

	function venue($id, $type) {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		if($type == 'catalog') {
			$objects = $this->catalog_model->getPremiumCatalogsByVenueID($id);
			$launcher = $this->module_mdl->getLauncher(15, 'venue', $venue->id);
		}

		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--', '--type--'), array('venue', $id, $type), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--type--', $type, $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--type--', $type, $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--','--type--'), array('venue', $id, $type), $deletecheckedurl);

		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = 'Premium: ' . $launcher->title;

		$this->iframeurl = $launcher->controller.'/venue/'.$id;
		
		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($objects)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $launcher->title => $launcher->controller.'/venue/'.$id, $this->_module_settings->title => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, $launcher->controller);
		$this->load->view('master', $cdata);
	}

	function add($parentId, $parentType, $type) {
		if($parentType == 'event') {
			$event = $this->event_model->getbyid($parentId);
			$app = _actionAllowed($event, 'event','returnApp');
		} elseif($parentType == 'venue') {
			$venue = $this->venue_model->getbyid($parentId);
			$app = _actionAllowed($venue, 'venue','returnApp');
		}

		$parentObject = false;

		if($type == 'exhibitor') {
			$objects = $this->exhibitor_model->getExhibitorsByEventID($event->id);
			$premiumobjects = $this->exhibitor_model->getPremiumExhibitorsByEventID($event->id);
			$launcher = $this->module_mdl->getLauncher(2, 'event', $event->id);
			$parentObject = $event;
		} elseif($type == 'session') {
			$objects = $this->sessions_model->getSessionByEventID($event->id);
			$premiumobjects = $this->sessions_model->getPremiumSessionsByEventID($event->id);
			$launcher = $this->module_mdl->getLauncher(10, 'event', $event->id);
			$parentObject = $event;
		} elseif($type == 'catalog') {
			$objects = $this->catalog_model->getCatalogsByVenueID($venue->id);
			$premiumobjects = $this->catalog_model->getPremiumCatalogsByVenueID($venue->id);
			$launcher = $this->module_mdl->getLauncher(15, 'venue', $venue->id);
			$parentObject = $venue;
		}
		$premiumtitle = $this->premium_model->getTitle($type, $parentType, $parentObject->id);

		//remove premium objects from available list
		foreach($premiumobjects as $p) {
			$premiumobjects2[$p->id] = $p;
		}
		foreach($objects as $o) {
			$objects2[$o->id] = $o;
		}
		foreach($objects as $o) {
			if(isset($premiumobjects2[$o->id])) {
				unset($objects2[$o->id]);
			}
		}
		$objects = $objects2;

		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = 'Premium: ' . $launcher->title;
		$this->iframeurl = $launcher->controller.'/'.$parentType.'/'.$parentId;

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('extraline', 'extraline', 'trim');
			$this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
			$this->form_validation->set_rules('premiumtitle', 'premiumtitle', 'trim');
			if($this->form_validation->run() == FALSE || $error != ''){
				$error .= validation_errors();
			} else {
				if($parentType == 'event') {
					$this->premium_model->save(true, $type, $this->input->post('objects'), $this->input->post('extraline'), $premium, 'event', $event->id, $this->input->post('premiumorder'), $this->input->post('premiumtitle'));
					redirect('premium/event/'.$event->id.'/'.$type);
				} elseif($parentType == 'venue') {
					$this->premium_model->save(true, $type, $this->input->post('objects'), $this->input->post('extraline'), $premium, 'venue', $venue->id, $this->input->post('premiumorder'), $this->input->post('premiumtitle'));
					redirect('premium/venue/'.$venue->id.'/'.$type);
				}				
			}
		}

		$cdata['content'] 		= $this->load->view('c_premium_add', array('settings' => $this->_module_settings, 'data' => $objects, 'type' => $type, 'premiumtitle' => $premiumtitle), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		if($parentType == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $launcher->title => $launcher->module.'/event/'.$id, $this->_module_settings->title => 'premium/'.$parentType.'/'.$parentId.'/'.$type, 'Add' => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$id, $launcher->title => $launcher->controller.'/event/'.$id, $this->_module_settings->title => 'premium/'.$parentType.'/'.$parentId.'/'.$type, 'Add' => $this->uri->uri_string());
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, $launcher->controller);
		} elseif($parentType == 'venue') {
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $launcher->title => $launcher->controller.'/venue/'.$id, $this->_module_settings->title => 'premium/'.$parentType.'/'.$parentId.'/'.$type, 'Add' => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, $launcher->controller);
		}
		$this->load->view('master', $cdata);
	}

	function edit($id, $type) {
		$parentObject = false;
		$app = false;
		$error = '';
		$parentType = 'event';
		if($type == 'exhibitor') {
			$object = $this->exhibitor_model->getExhibitorByID($id);
			if($object->eventid > 0) {
				$parentObject = $this->event_model->getbyid($object->eventid);
				$app = _actionAllowed($parentObject, $parentType,'returnApp');
			}
			$launcher = $this->module_mdl->getLauncher(2, $parentType, $parentObject->id);
		} elseif($type =='session') {
			$object = $this->sessions_model->getById($id);
			if($object->eventid > 0) {
				$parentObject = $this->event_model->getbyid($object->eventid);
				$app = _actionAllowed($parentObject, $parentType,'returnApp');
			}
			$launcher = $this->module_mdl->getLauncher(10, $parentType, $parentObject->id);
		} elseif($type == 'catalog') {
			$object = $this->catalog_model->getCatalogByID($id);
			if($object->venueid > 0) {
				$parentType = 'venue';
				$parentObject = $this->venue_model->getbyid($object->venueid);
				$app = _actionAllowed($parentObject, $parentType,'returnApp');
			}
			$launcher = $this->module_mdl->getLauncher(15, $parentType, $parentObject->id);
		}

		$premium = $this->premium_model->get($type, $object->id);
		$premiumtitle = $this->premium_model->getTitle($type, $parentType, $parentObject->id);
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$this->iframeurl = $launcher->controller.'/view/'.$id;

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('extraline', 'extraline', 'trim');
			$this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
			$this->form_validation->set_rules('premiumtitle', 'premiumtitle', 'trim');
			if($this->form_validation->run() == FALSE || $error != ''){
				$error .= validation_errors();
			} else {
				$this->premium_model->save(true, $type, $object->id, $this->input->post('extraline'), $premium, $parentType, $parentObject->id, $this->input->post('premiumorder'), $this->input->post('premiumtitle'));

				redirect('premium/'.$parentType.'/'.$parentObject->id.'/'.$type);
			}
		}

		$cdata['content'] 		= $this->load->view('c_premium_edit', array($parentType => $parentObject, 'object' => $object, 'error' => $error, 'languages' => $languages, 'app' => $app, 'premium' => $premium, 'premiumtitle' => $premiumtitle), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		if($parentType == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $parentObject->name => $parentType."/view/".$parentObject->id, $launcher->title => $launcher->controller.'/'.$parentType.'/'.$id, $this->_module_settings->title => $launcher->controller."/".$parentType."/".$parentObject->id, __("Edit: ") . $object->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($parentObject->name => "event/view/".$parentObject->id, $launcher->title => $launcher->controller.'/'.$parentType.'/'.$id, $this->_module_settings->title => "premium/".$parentType."/".$parentObject->id.'/'.$type, __("Edit: ") . $object->name => $this->uri->uri_string());
			}
		} elseif($parentType == 'venue') {
			$cdata['crumb']	= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $launcher->title => $launcher->controller.'/venue/'.$id, $this->_module_settings->title => 'premium/'.$parentType.'/'.$parentId.'/'.$type, __("Edit: ") . $object->name => $this->uri->uri_string()));
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($parentType, $parentObject->id, $app, $launcher->controller);
		$this->load->view('master', $cdata);
	}

	function delete($id, $type) {
		$parentType = 'event';
		$parentObject = false;
		$app = false;
		$error = '';
		if($type == 'exhibitor') {
			$object = $this->exhibitor_model->getExhibitorByID($id);
			if($object->eventid > 0) {
				$parentObject = $this->event_model->getbyid($object->eventid);
				$app = _actionAllowed($parentObject, $parentType,'returnApp');
			}
		} elseif($type =='session') {
			$object = $this->sessions_model->getById($id);
			if($object->eventid > 0) {
				$parentObject = $this->event_model->getbyid($object->eventid);
				$app = _actionAllowed($parentObject, $parentType,'returnApp');
			}
		} elseif($type == 'catalog') {
			$object = $this->catalog_model->getCatalogByID($id);
			if($object->venueid > 0) {
				$parentType = 'venue';
				$parentObject = $this->venue_model->getbyid($object->venueid);
				$app = _actionAllowed($parentObject, $parentType,'returnApp');
			}
		}
		$premium = $this->premium_model->get($type, $id);
		$this->general_model->remove('premium', $premium->id);

		redirect('premium/'.$parentType.'/'.$parentObject->id.'/'.$type);
	}

    function removemany($parentId, $parentType, $type) {
    	if($parentType == 'venue') {
			$venue = $this->venue_model->getbyid($parentId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($parentType == 'event') {
			$event = $this->event_model->getbyid($parentId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($parentType == 'app') {
			$app = $this->app_model->get($parentId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->premium_model->removeMany($selectedids, $type);
    }

}