<?php
if (! defined('BASEPATH')) exit(__('No direct script access'));

require_once dirname(__DIR__) . '/models/session_model.php';

/**
 * Authentication Controller
 */
class Auth extends CI_Controller
{
	private $_model;
	private $_module_settings;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		# Try to create session
		$this->_model = \Tapcrowd\Model\Session::getInstance();
		$this->_model->startSession();

		# Setup controller module
		$this->_module_settings = (object)array();
	}

	/**
	 * Private Method to output html
	 *
	 * @param string $view The html view
	 * @param array $params Optional; The parameters to pass through
	 * @param array $title Optional; The page title
	 */
	private function _outputHTML( $view, $params = null, $title = null )
	{
		# Add settings to args
		$params['settings'] = $this->_module_settings;
		$params['channel'] = \Tapcrowd\API::getCurrentChannel();

		$this->load->view('c_authmaster', array(
			'content' => $this->load->view($view, $params, true),
			'title' => $title,
		));
	}

	/**
	 * Private Method to send email
	 *
	 * @param string $view The email view
	 * @param array $params Optional; The parameters to pass through
	 * @param string $email The email address (receiver)
	 */
	private function _sendMail( $view, $params, $email )
	{
		# Load recovery email
		$msg = $this->load->view($view, $params, true);

		# Parse title
		$r = preg_match('/<title>(.*)<\/title>/', $msg, $m);
		$title = (isset($m[1]))? $m[1] : 'No Title';

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
		$headers .= 'From: TapCrowd Registration <accounts@tapcrowd.com>' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";

		try {
			mail( $email, $title, $msg, $headers );
			$msg = 'pleasecheckemail';
		} catch(\Exception $e) {
			$msg = 'couldnotmail';
		}
	}

	/**
	 * Default method => redirects to login()
	 */
	public function index()
	{
		$this->login();
	}

	/**
	 * Login page
	 */
	public function login($error = 0)
	{
		$this->_outputHTML('c_login', null, 'Sign in');
	}

	/**
	 * Authenticate user (Login POST endpoint)
	 */
	public function authenticate()
	{
		$login = filter_input(INPUT_POST, 'auth_login', FILTER_SANITIZE_STRING);
		$pass = filter_input(INPUT_POST, 'auth_pass', FILTER_SANITIZE_STRING);

		try {
			$authed = $this->_model->authenticate( $login, $pass );
			if(!$authed) {
				redirect('/auth/login?errormsg=invalidcredentials');
			}

			# Set CI Session
			#
			# TODO This '$CI->session->set_userdata' dependency should be removed !!!
			#
			$CI =& get_instance();
			$account = $this->_model->getCurrentAccount();
			$CI->session->set_userdata(array(
				'logged_in' => true,
				'name' => $account->fullname,
			));

			redirect('/');

		} catch(\Exception $e) {
			redirect('/auth/login?errormsg=invalidcredentials');
		}
	}

	/**
	 * Create/Update account
	 */
	public function save()
	{
		$msg = null;

		# Login
		$login = filter_input(INPUT_POST, 'auth_login');
		if(empty($login)) $msg = 'invalidlogin';

		# Password
		$pass = filter_input(INPUT_POST, 'auth_pass');
		$pass2 = filter_input(INPUT_POST, 'auth_pass2');
		if($pass !== $pass2) $msg = 'passmismatch';

		# Get token
// 		$flags = array('flags' => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
// 		$token = filter_input(INPUT_POST, 'auth_token', FILTER_UNSAFE_RAW, $args);
// 		if(empty($token)) $msg = 'invalidtoken';
// 		else {
// 			try {
// 				$this->_model->verifyToken($token);
// 			} catch(\Exception $e) {
// 				$msg = 'invalidtoken';
// 			}
// 		}

		# In case this is is a password reset
		$reset = filter_input(INPUT_POST, 'auth_reset');
		if($reset !== null) {

			# Respond to errors
			if($msg !== null) {
				redirect('/auth/recovery?errormsg='.$msg);
			}

			try {
				$this->_model->saveLogin( $login, $pass);
				$this->_model->authenticate( $login, $pass );
				redirect('/');
			} catch(\Exception $e) {
				redirect('/auth/recovery?errormsg=couldnotlogin&msg=' . $e->getMessage() );
			}
		}

		# Fullname
		$fullname = filter_input(INPUT_POST, 'auth_fn', FILTER_SANITIZE_STRING);
		$email = filter_input(INPUT_POST, 'auth_email', FILTER_VALIDATE_EMAIL);
		if(!isValidEmail($email)) $msg = 'invalidemail';

		# Check if login already exists
		$login_exists = $this->_model->getAccountForLogin( $login );
		if(!empty($login_exists)) $msg = 'loginexists';

		# Respond to errors
		if($msg !== null) {
			redirect('/auth/register?errormsg='.$msg);
		}

		# Create account
		try {
			$this->_model->saveLogin( $login, $pass, $fullname, $email );
		} catch(\Exception $e) {
			alert($e->getMessage(), 'error');
			redirect('/auth/register');
		}

		# Send registration mail or activation email (depending on email status)
		$args = array(
			'fullname' => $fullname,
			'email' => $email,
			'login' => $login,
		);
		if($this->_model->emailStatus($email) == 'active') {
			$template = 'mails/e_registration';
		} else {
			$template = 'mails/e_activation';
			$token = $this->_model->generateToken( 86400, $email);
			$args['url'] = site_url('/auth/activate?auth_token=' . $token);
		}
		$this->_sendMail($template, $args, $email);

		# Authenticate acount ( === automatic login after register)
		try {
			$this->_model->authenticate( $login, $pass );
		} catch(\Exception $e) {
			redirect('/auth/login?errormsg=couldnotlogin');
		}

		redirect('/');
	}

	/**
	 * Logout and redirect to login
	 */
	public function logout()
	{
		$CI =& get_instance();
		$CI->session->set_userdata(array(
			'logged_in_as_admin' => false,
			'logged_in' => false,
			'name' => 'Anonymous',
		));
		$CI->session->sess_destroy();
		$this->_model->closeSession();

		redirect('/auth/login?errormsg=loggedout');
	}

	/**
	 * Register page
	 */
	public function register( $mode = 'normal' )
	{
		$token = $this->_model->generateToken(600);
		$args = array('auth_token' => $token, 'cancel_url' => '/auth/login');

		if($mode == 'compact') {
			$this->load->view('c_register_compact', $args);
		} else {
			$this->_outputHTML('c_register', $args, 'Register');
		}
	}

	/**
	 * Recovery page
	 */
	public function recovery()
	{
		$args = array();
		$msg = null;

		# Check for email (fase 2)
		$email = filter_input(INPUT_POST, 'auth_email', FILTER_VALIDATE_EMAIL);
		if(!empty($email)) {

			#
			# TODO : AUTOMATED ATTEMPTS: If more then ex. 3 tokens are created for this email
			# in the last 10 minutes, skip this call and notify client
			#

			$token = $this->_model->generateToken( 86400, $email);

			# Send recovery email
			$this->_sendMail('mails/e_recovery', array(
				'url' => site_url('/auth/recovery?auth_token=' . $token)
			), $email);

			try {
				$r = mail( $email, $title, $msg, $headers);
				$msg = 'pleasecheckemail';
			} catch(\Exception $e) {
				$msg = 'couldnotmail';
			}
		} elseif($email === false) {
			$msg = 'invalidemail';
		}

		# Check for token (fase 3)
		$flags = array('flags' => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		$token = filter_input(INPUT_GET, 'auth_token', FILTER_UNSAFE_RAW, $args);
		if(!empty($token)) {

			try {
				$valid = $this->_model->verifyToken($token);
			} catch(\Exception $e) {
				$valid = false;
			}

			if($valid === false) {
				$msg = 'invalidtoken';
			} else {
				$logins = $this->_model->getLoginsForToken($token);
				$args = array('auth_token' => $token, 'logins' => $logins);
			}
		} elseif($token === false) {
			$msg = 'invalidtoken';
		}

		# Resolve
		if(!empty($msg)) {
			redirect('/auth/recovery?errormsg='.$msg);
		} else {
			$this->_outputHTML('c_recovery', $args, 'Recover');
		}
	}

	/**
	 * Email / Account Activation method
	 */
	public function activate()
	{
		$flags = array('flags' => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		$token = filter_input(INPUT_GET, 'auth_token', FILTER_UNSAFE_RAW, $args);
		if(!empty($token)) {
			try {
				$valid = $this->_model->verifyToken($token);
			} catch(\Exception $e) {
				$valid = false;
			}

			# Get email
			try {
				$email = $this->_model->getEmailForToken($token);
			} catch(\Exception $e) {
				$email = false;
			}

			if($valid === false || $email === false) {
				$msg = 'invalidtoken';
			} else {
				$r = $this->_model->emailStatus( $email, 'active');
			}
		} elseif($token === false) {
			$msg = 'invalidtoken';
		}

		# Resolve
		if(!empty($msg)) {
			redirect('/auth/login?errormsg='.$msg);
		} else {
			redirect('/');
		}
	}


	/**
	 * Single Sign on Method
	 */
	public function sso( /*$email, $external_id, $fullname, $timestamp, $hash*/ )
	{
		$channel_model = \Tapcrowd\Model\Channel::getInstance();

		# Sanitize / Validate
		$errors = array();
		$params = array('email','external_id','fullname','timestamp');
		foreach($params as $p) {
			$$p = filter_var($_GET[$p], FILTER_SANITIZE_STRING);
			if(empty($$p)) $errors[] = "$p can not be empty";
		}

		# Timestamp
// 		if(!is_numeric($timestamp) || ((time() - $timestamp) > 3600)) {
// 			$errors[] = "Invalid timestamp";
// 		}

		# Hash
		$hash = filter_var($_GET['hash'], FILTER_SANITIZE_STRING);
		$string = http_build_query( compact($params) );
		$real_hash = $channel_model->getHash($string, 'sha1');
		if($hash !== $real_hash) $errors[] = "Invalid Hash";

		# Respond
		$this->_handle_sso_request( $external_id, $fullname, $email, $errors );
	}

	/**
	 * OneLogin SSO integration
	 *
	 * NOTE: OneLogin is a INSECURE SSO implementation; Do not use this unless neccesery
	 *
	 * The OneLogin Server will call this endpoint with GET parameters:
	 * ?firstname={firstname}&lastname={lastname}&email={email}&timestamp={timestamp}&signature={signature}
	 *
	 * @see https://onelogin.zendesk.com/entries/271913-Federated-Authentication-API
	 */
	public function onelogin()
	{
		$channel_model = \Tapcrowd\Model\Channel::getInstance();

		# Initialize
		$onelogin_params = array('firstname','lastname','email','timestamp','signature');
		$token = $channel_model->getHash("one_login_sso", 'sha1');
		$errors = array();

		# Sanitize input
		foreach($onelogin_params as $param) {
			$$param = filter_input(INPUT_GET, $param, FILTER_SANITIZE_STRING);
			if(empty($$param)) {
				$errors[] = "Invalid $param; Should not be empty";
			}
		}

		# Validate time
		if(!is_numeric($timestamp) || ((time() - $timestamp) > 300)) {
			$errors[] = "Invalid timestamp : $timestamp";
		}

		# Validate hash
		$hash = sha1(implode('', compact(array('firstname','lastname','email','timestamp','token'))));
		if($hash !== $signature) $errors[] = "Invalid Hash: $hash !== $signature";

		# Respond
		$external_id = $channel_model->getHash("$firstname$lastname$email", 'sha1');
		$this->_handle_sso_request( $external_id, "$firstname $lastname", $email, $errors );
	}

	/**
	 * Internal method to respond to SSO requests
	 *
	 * @param int $external_id
	 * @param string $fullname
	 * @param string $email
	 * @param array|null $errors Defaults to null
	 * @return void
	 */
	private function _handle_sso_request( $external_id, $fullname, $email, $errors = null )
	{
		$log = function($msg)
		{
			$msg = sprintf("\n[%s]\t%s", date(DATE_RFC822), $msg);
			file_put_contents('/tmp/tapcrowd-sso.log', $msg, FILE_APPEND);
		};

		# Error handling
		if(!empty($errors)) {
			$log('400 Bad Request: '.implode(',', $errors) .' '. $string);
			$accept = filter_var($_SERVER['HTTP_ACCEPT'], FILTER_SANITIZE_STRING);
			switch($accept) {
				case '*/*':
				case 'text/html':
					$response = var_export((object)array('errors' => $errors));
					break;
				case 'application/json':
					$response = json_encode((object)array('errors' => $errors));
					break;
				case 'application/xml':
					$response = new \SimpleXMLElement('<response />');
					$child = $response->addChild('errors');
					array_walk($errors, function($msg) use ($child)
					{
						$child->addChild('error', $msg);
					});
					$response = $response->asXML();
					break;
				default:
					header('HTTP/1.1 406 Not Acceptable');
					echo $accept;
					exit;
			}

			header('HTTP/1.1 400 Bad Request');
			echo $response;
			exit;
		}

		try {
			$session_model = \Tapcrowd\Model\Session::getInstance();
			$channel_model = \Tapcrowd\Model\Channel::getInstance();

			#
			# Create login/pass and authenticate
			# TODO This logic should be moved to the session controller
			#
			$sso_salt = 'sso_secret';
			$login = $channel_model->getHash($sso_salt.$external_id, 'md5');
			$password = $channel_model->getHash($sso_salt.$login, 'md5');

			$session_model->saveLogin( $login, $password, $fullname, $email);
			$session_model->authenticate( $login, $password);

		} catch(\Exception $e) {
			$log('500 Internal Server Error: ' . $e->getMessage() );
			header('HTTP/1.1 500 Internal Server Error');
			exit;
		}

		# Redirect to home page
		header('HTTP/1.1 303 See Other');
		header('Location: /');
	}



	// SSO WITH THIRD PARTY WEBSITES
// 	function sso($external_id = '', $key = '', $username = '', $email = '', $login = '', $password = '') {
// 		$internal_error = TRUE;
// 		$secret = md5('tcadm'.$external_id);
// 		$channelid = $this->channel->id;
// 		if($secret == $key) {
// 			// CHECK IF USER EXISTS
// 			$users = $this->db->query("SELECT * FROM organizer WHERE external_id = '$external_id' AND channelid = $channelid");
// 			if($users->num_rows() > 0){
// 				// USER EXISTS => START SESSION
// 				$internal_error = FALSE;
// 			} else {
// 				// NO USER FOUND => CREATE USER
// 				if ($username != '') {
// 					if ($email != '') {
// 						$email = urldecode($email);
// 						if ($this->db->insert('organizer', array(
// 							'external_id'	=> $external_id,
// 							'name'			=> $username,
// 							'login'			=> $login,
// 							'password'		=> $password,
// 							'email'			=> $email,
// 							'credits'		=> 0,
// 							'channelid' 	=> $channelid,
// 							'activation'	=> 'active',
// 							'lastlogin'		=> date('Y-m-d H:i:s', time())
// 						))) {
// 							$internal_error = FALSE;
// 						}
// 					} else {
// 						echo "EMPTY EMAIL";
// 						exit();
// 					}
// 				} else {
// 					echo "EMPTY USERNAME";
// 					exit();
// 				}
// 			}
//
// 			if (!$internal_error) {
// 				$this->session->sess_destroy();
// 				$this->db->where('external_id', $external_id);
// 				$query = $this->db->get_where('organizer');
// 				$user_data = $query->row_array();
// 				$user_data['logged_in'] = true;
// 				$this->session->set_userdata($user_data);
// 				echo redirect('apps');
// 			} else {
// 				echo "ERROR";
// 				exit();
// 			}
// 		} else {
// 			echo __("INVALID AUTHENTICATION");
// 			exit();
// 		}
// 	}

	function mailtest() {
		$i = 1;
		while($i <= 50) {
			// $newuser = $this->user_mdl->getUserByLogin('stichoel');
			//Config mail function:
			$config['mailtype'] 	= 'html';
			$config['charset']		= 'utf-8';
			//Set Config!
			$this->load->library('email');
			$this->email->initialize($config);
			//prepare email
			$to = 'tom@tapcrowd.com'; //TO Address
			$toname = 'Tom'; //TO Address
			$subject = 'testsendmail'; //Subject of the message
			$from = 'info@tapcrowd.com';
			$fromname = 'TapCrowd';
			$newuser->id = 1;
			//HTML Message:
			// $mailcontent = $this->load->view('mails/e_activation', array('title' => $subject, 'newuser' => $newuser, 'name' => 'ddsf'), TRUE);
			// $message = $this->load->view('mails/e_master', array('title' => $subject, 'content' => $mailcontent), TRUE);
			$message = 'sdfhiudsfhdsf dsgf dsgfsd fydsgf sdg';

			if($from && $fromname && $message) {
				$this->email->from($from, $fromname);
				$this->email->to($to);
				//$this->email->bcc('log@floatleft.be');
				$this->email->subject($subject);
				$this->email->message($message);
				$this->email->send();
				$this->email->print_debugger();
				// redirect('auth');
			}

			$i++;
		}
	}

// 	function activate($id) {
// 		$this->simpleloginsecure->logout();
//
// 		if($id == FALSE || $id == '') $error = __('Userdata not found, please check the given link, or contact us.');
//
// 		if($this->user_mdl->activateUser($id)){
// 			$this->session->set_flashdata('form_feedback', __('Activation complete, you can now log on!'));
// 			redirect("/auth/login/");
// 		}
//
// 		$error = __('Account activation incomplete, please contact support');
// 		$cdata['content'] 		= $this->load->view('auth/register', array('error' => $error), TRUE);
// 		$this->load->view('auth/master', $cdata);
// 	}

	function sendactivation($id) {
		// get User
		$user = $this->db->query("SELECT * FROM organizer WHERE MD5(id) = '$id' LIMIT 1");
		if($user->num_rows() == 0) redirect('auth/login');
		$newuser = $user->row();

		//Config mail function:
		$config['mailtype'] = 'html';
		//Set Config!
		$this->load->library('email');
		$this->email->initialize($config);
		//prepare email
		$to = $newuser->email; //TO Address
		$toname = $newuser->name; //TO Address
		$subject = __('Registration'); //Subject of the message
		$from = 'info@mobilejuice.be';
		$fromname = 'TapCrowd';
		//HTML Message:
		$mailcontent = $this->load->view('mails/e_activation', array('title' => $subject, 'newuser' => $newuser, 'name' => $newuser->name), TRUE);
		$message = $this->load->view('mails/e_master', array('title' => $subject, 'content' => $mailcontent), TRUE);

		if($from && $fromname && $message) {
			$this->load->library('email');
			$this->email->from($from, $fromname);
			$this->email->to($to);
			$this->email->bcc('log@floatleft.be');
			$this->email->subject($subject);
			$this->email->message($message);
			$this->email->send();
		}

		$this->session->set_flashdata('form_feedback', __('New activation mail has been sent, check your mailbox!'));
		redirect("/auth/login/");
	}

	function _genRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$string = '';
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters)-1)];
		}
		return $string;
	}

	function from_ticketing() {
		parse_str($_SERVER['QUERY_STRING'], $_GET);

		$organizerid = $_GET['organizerId'];
		$eventid = $_GET['eventId'];

		// Get user with organizerID from querystring
		$res = $this->db->query("SELECT * FROM organizer WHERE SHA1(id) = '$organizerid'");
		$user_data = FALSE;
		if($res->num_rows() >= 1) {
			$user_data = $res->row_array();
		}

		// Get event with eventID from querystring
		$eventres = $this->db->query("SELECT * FROM event WHERE SHA1(id) = '$eventid'");
		$event = FALSE;
		if($eventres->num_rows() >= 1) {
			$event = $eventres->row_array();
		}

		// Destroy Session
		$this->session->sess_destroy();
		//Create a fresh, brand new session
		$this->session->sess_create();
		//Set session data
		$user_data['logged_in'] = true;
		$user_data['mijnevent'] = true;
		$this->session->set_userdata($user_data);
		redirect('event/view/'.$event['id']);

	}


	/**
	 * Create/Update account
	 */
	public function registercompact()
	{
		$msg = null;

		# Login
		$login = filter_input(INPUT_POST, 'inputEmail');
		if(empty($login)) $msg = 'invalidlogin';

		# Password
		$pass = filter_input(INPUT_POST, 'inputPassword');

		# Fullname
		$fullname = filter_input(INPUT_POST, 'inputFullName', FILTER_SANITIZE_STRING);
		$email = filter_input(INPUT_POST, 'inputEmail', FILTER_VALIDATE_EMAIL);
		if(!isValidEmail($email)) $msg = 'invalidemail';

		$company = filter_input(INPUT_POST, 'inputCompany', FILTER_SANITIZE_STRING);

		# Check if login already exists
		$login_exists = $this->_model->getAccountForLogin( $login );
		if(!empty($login_exists)) $msg = 'loginexists';

		# Respond to errors
		if($msg !== null) {
			redirect('/auth/register?errormsg='.$msg);
		}

		# Create account
		try {
			$this->_model->saveLogin( $login, $pass, $fullname, $email, 0, $company );
		} catch(\Exception $e) {
			alert($e->getMessage(), 'error');
			redirect('/auth/register');
		}

		# Send registration mail or activation email (depending on email status)
		$args = array(
			'fullname' => $fullname,
			'email' => $email,
			'login' => $login,
		);
		if($this->_model->emailStatus($email) == 'active') {
			$template = 'mails/e_registration';
		} else {
			$template = 'mails/e_activation';
			$token = $this->_model->generateToken( 86400, $email);
			$args['url'] = site_url('/auth/activate?auth_token=' . $token);
		}
		$this->_sendMail($template, $args, $email);

		# Authenticate acount ( === automatic login after register)
		try {
			$this->_model->authenticate( $login, $pass );
		} catch(\Exception $e) {
			echo 'ERROR: couldnotlogin';
			exit;
		}

		// redirect('/');
		redirect('apps/dashboard?welcome=true');
	}

}
