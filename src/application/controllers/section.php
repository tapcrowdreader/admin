<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Section extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('contentmodule_model');
	}
	
	function add($contentmoduleid) {
		$contentmodule = $this->contentmodule_model->getById($contentmoduleid);
		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$error = '';
		$this->load->library('form_validation');
		$sectiontypes = $this->contentmodule_model->getSectiontypes();
		$this->iframeurl = 'contentmodule/view/'.$contentmoduleid;

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND sectionid <> 0 GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}	

		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
            }
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('maxitems', 'maxitems', 'trim');
			
			$this->form_validation->set_rules('sectiontype', 'sectiontype', 'trim|required');
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {	
				$showlabels = 0;
				if($this->input->post('showlabels')) {
					$showlabels = 1;
				}
				$data = array(
						'contentmoduleid' => $contentmoduleid,
						'title'		=> $this->input->post('title_'.$app->defaultlanguage),
						'maxitems' => set_value('maxitems'),
						'order'		=> $this->input->post('order'),
						'sectiontypeid' => $this->input->post('sectiontype'),
						'showlabels'	=> $showlabels
					);
				$id = $this->general_model->insert('section', $data);

				// *** SAVE TAGS *** //
				$tags = $this->input->post('mytagsulselect');
				foreach ($tags as $tag) {
					$tags = array(
							'appid' 	=> $app->id,
							'tag' 		=> htmlspecialchars_decode(html_entity_decode($tag)),
							'sectionid' => $id
						);
					$this->db->insert('tag', $tags);
				}
				
				$this->general_model->update('app', $app->id, array('timestamp' => time()));
				if($error == '') {
					redirect('contentmodule/view/'.$contentmoduleid);
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_section_add', array('app' => $app, 'error' => $error, 'languages' => $languages, 'sectiontypes' => $sectiontypes, 'apptags' => $apptags), TRUE);
		$cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id ,__("Add Section") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}

	function edit($sectionid) {
		$section = $this->contentmodule_model->getSectionById($sectionid);
		$contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$error = '';
		$this->load->library('form_validation');
		$sectiontypes = $this->contentmodule_model->getSectiontypes();
		$this->iframeurl = 'contentmodule/view/'.$contentmodule->id;

		// TAGS
		$tags = $this->db->query("SELECT * FROM tag WHERE sectionid = $sectionid");
		if($tags->num_rows() == 0) {
			$tags = array();
		} else {
			$tagz = array();
			foreach ($tags->result() as $tag) {
				$tagz[] = $tag;
			}
			$tags = $tagz;
		}
		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND sectionid <> 0 GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}	

		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
            }
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('maxitems', 'maxitems', 'trim');
			
			$this->form_validation->set_rules('sectiontype', 'sectiontype', 'trim|required');
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {	
				$showlabels = 0;
				if($this->input->post('showlabels')) {
					$showlabels = 1;
				}
				$data = array(
						'title'		=> $this->input->post('title_'.$app->defaultlanguage),
						'maxitems' => set_value('maxitems'),
						'order'		=> $this->input->post('order'),
						'sectiontypeid' => $this->input->post('sectiontype'),
						'showlabels'	=> $showlabels
					);
				$id = $this->general_model->update('section', $sectionid, $data);

				// *** SAVE TAGS *** //
				if(!empty($sectionid)) {
					$this->db->where('sectionid', $sectionid);
					$this->db->where('appid', $app->id);
	                $this->db->delete('tag');
					$tags = $this->input->post('mytagsulselect');
					foreach ($tags as $tag) {
						$tags = array(
								'appid' 	=> $app->id,
								'tag' 		=> htmlspecialchars_decode(html_entity_decode($tag)),
								'sectionid' => $sectionid
							);
						$this->db->insert('tag', $tags);
					}
				}
				
				$this->general_model->update('app', $app->id, array('timestamp' => time()));
				if($error == '') {
					redirect('contentmodule/view/'.$section->contentmoduleid);
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_section_edit', array('app' => $app, 'error' => $error, 'languages' => $languages, 'sectiontypes' => $sectiontypes, 'section' => $section, 'tags' => $tags, 'apptags' => $apptags), TRUE);
		$cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id ,__("Edit Section") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}

	public function view($id) {
		$section = $this->contentmodule_model->getSectionById($id);

		$contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
		$this->iframeurl = 'contentmodule/view/'.$contentmodule->id;

		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');

		$sectioncontents = $this->contentmodule_model->getSectioncontentBySectionId($id);

		$cdata['content'] 		= $this->load->view('c_section_view', array('app' => $app, 'section' => $section, 'sectioncontents' => $sectioncontents), TRUE);
		$cdata['crumb']			= array($section->tilte => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}

	public function delete($id) {
		$section = $this->contentmodule_model->getSectionById($id);
		$contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);

		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');

		$this->general_model->remove('section',$id);
		redirect('contentmodule/view/'.$contentmodule->id);
	}

	function news($id) {
		$this->load->model('news_model');
		$this->load->model('contentmodule_model');
		if($id == FALSE || $id == 0) redirect('apps');
		
		$section = $this->contentmodule_model->getSectionById($id);
		$contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
		$app = $this->app_model->get($contentmodule->appid);

		_actionAllowed($app, 'app');
		
		$this->iframeurl = 'contentmodule/app/'.$app->id;

		$tags = $this->contentmodule_model->getSectionTags($id);
		$news = array();
		foreach($tags as $tag) {
			$news2 = $this->news_model->getNewsByTagId($tag->id);
			foreach($news2 as $n) {
				if(!empty($n->hash)) {
					$news[$n->hash] = $n;
				} else {
					$news[] = $n;
				}
				
			}

			$sourcenews = $this->news_model->getNewsBySourceTag($tag->tag, $app->id);
			foreach($sourcenews as $n) {
				if(!isset($news[$n->hash])) {
					$news[] = $n;
				}
			}
		}



		$headers = array(__('Title') => 'title', __('Date') => 'datum');
		
		$cdata['content'] 		= $this->load->view('c_listview', array('app' => $app, 'data' => $news, 'headers' => $headers, 'section' => $section), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id, $section->title => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function addnews($id) {
		$this->load->model('contentmodule_model');
		$this->load->library('form_validation');
		$error = "";
	    $imageError = "";
		$section = $this->contentmodule_model->getSectionById($id);
		$contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app');
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND newsitemid <> 0 GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}	
		$startingtags = $this->db->query("SELECT tag FROM tag WHERE sectionid = $id")->result();

		$this->iframeurl = 'contentmodule/app/'.$app->id;
		
		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
            if($languages != null) {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                $this->form_validation->set_rules('txt_'.$language->key, 'text ('.$language->name.')', 'trim|required');
            }
            }
            $this->form_validation->set_rules('order', 'order', 'trim');
            $this->form_validation->set_rules('tags', 'tags', 'trim');
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {
				$url = $this->input->post('url');
				if(!strstr($url,'http://') && trim($url) != '') {
					$url = 'http://'.$url;
				}
				$data = array( 
						"title" 	=> $this->input->post('title_'.$app->defaultlanguage),
						"appid" 	=> $app->id,
						"txt" 		=> $this->input->post('txt_'.$app->defaultlanguage),
                        "datum"     => date("Y-m-d H:i:s",time()),
                        "order"		=> set_value('order'),
                        'videourl'		=> $url
					);
				
				if(($newsid = $this->general_model->insert('newsitem', $data)) && $imageError == ''){
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$newsid)){
						mkdir($this->config->item('imagespath') . "upload/newsimages/".$newsid, 0755);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$newsid;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = ""; //No image uploaded!
						if($_FILES['image']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/newsimages/'. $newsid . '/' . $returndata['file_name'];
					}
					
					$this->general_model->update('newsitem', $newsid, array('image' => $image));
					
                    //add translated fields to translation table
                    if($languages != null) {
                    foreach($languages as $language) {
                        $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, $this->input->post('title_'.$language->key));
                        $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $this->input->post('txt_'.$language->key));
                    }
                    }

					// *** SAVE TAGS *** //
					$tags = $this->input->post('mytagsulselect');
					foreach ($tags as $tag) {
						$tags = array(
								'appid' 	=> $app->id,
								'tag' 		=> $tag,
								'newsitemid' => $newsid
							);
						$this->general_model->insert('tag', $tags);
					}
					if($this->input->post('tagfield') != null && trim($this->input->post('tagfield')) != '') {
						$tags = array(
								'appid' 	=> $app->id,
								'tag' 		=> $this->input->post('tagfield'),
								'newsitemid' => $newsid
							);
						$this->general_model->insert('tag', $tags);
					}
                    
					$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been added!'));
					$this->general_model->update('app', $app->id, array('timestamp' => time()));
					if($error == '') {
						redirect('section/news/'.$section->id);
					}else {
						//image error
						redirect('news/edit/'.$newsid.'/app?error=image');
					}
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
                
                if($imageError != '') {
                    $error .= '<br />' .$imageError;
                }
			}
		}
		
		$cdata['content'] 		= $this->load->view('c_news_add_content', array('app' => $app, 'error' => $error, 'languages' => $languages, 'apptags' => $apptags, 'startingtags' => $startingtags), TRUE);
		$cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id, $section->title => 'section/news/'.$section->id, __("Add new") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$this->load->view('master', $cdata);
	}

	function editnews($id, $sectionid) {
		$this->load->model('contentmodule_model');
		$this->load->library('form_validation');
		$error = "";
	    $imageError = "";
		$section = $this->contentmodule_model->getSectionById($sectionid);
		$contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app');
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$this->load->model('news_model');
		$newsitem = $this->news_model->getNewsById($id);

		// TAGS
		$tags = $this->db->query("SELECT * FROM tag WHERE newsitemid = $id");
		if($tags->num_rows() == 0) {
			$tags = array();
		} else {
			$tagz = array();
			foreach ($tags->result() as $tag) {
				$tagz[] = $tag;
			}
			$tags = $tagz;
		}
		$this->iframeurl = 'news/view/'.$id;
		
		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                $this->form_validation->set_rules('txt_'.$language->key, 'text ('.$language->name.')', 'trim|required');
            }
			$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
            $this->form_validation->set_rules('order', 'order', 'trim');
            $this->form_validation->set_rules('tags', 'tags', 'trim');
            
			//Uploads Images
			if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$id)){
				mkdir($this->config->item('imagespath') . "upload/newsimages/".$id, 0755);
			}

			$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$id;
			$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
			$configexlogo['max_width']  = '2000';
			$configexlogo['max_height']  = '2000';
			$this->load->library('upload', $configexlogo);
			$this->upload->initialize($configexlogo);
			if (!$this->upload->do_upload('image')) {
				$image = $newsitem->image; //No image uploaded!
				if($_FILES['image']['name'] != '') {
					$error .= $this->upload->display_errors();
				}
			} else {
				//successfully uploaded
				$returndata = $this->upload->data();
				$image = 'upload/newsimages/'. $id . '/' . $returndata['file_name'];
			}
			
			if($this->form_validation->run() == FALSE || $error != ''){
				$error .= validation_errors();
			} else {
// 				$url = $this->input->post('url');
// 				if(!strstr($url,'http://')) {
// 					$url = 'http://'.$url;
// 				}

				# Do not append 'http://'
				$url = filter_input(INPUT_POST, 'url', FILTER_SANITIZE_URL);
				if(empty($url)) $url = '';

				$data = array( 
						"title" => $this->input->post('title_'. $app->defaultlanguage),
						"txt" 	=> $this->input->post('txt_'. $app->defaultlanguage),
						"image"	=> $image,
                        "datum" => date("Y-m-d H:i:s",time()),
                        "order" => set_value('order'),
                        'videourl'	=> $url
					);
				
				if($this->general_model->update('newsitem', $id, $data) && $imageError == ''){
                    //add translated fields to translation table
                    foreach($languages as $language) {
						$nameSuccess = $this->language_model->updateTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
						if(!$nameSuccess) {
							$this->language_model->addTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
						}
						$descrSuccess = $this->language_model->updateTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('txt_'.$language->key));
						if(!$descrSuccess) {
							$this->language_model->addTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('txt_'.$language->key));
						}
                    }
					// *** SAVE TAGS *** //
					if(!empty($id)) {
						$this->db->where('newsitemid', $id);
						$this->db->where('appid', $app->id);
		                $this->db->delete('tag');
						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$tags = array(
									'appid' 	=> $app->id,
									'tag' 		=> $tag,
									'newsitemid' => $id
								);
							$this->general_model->insert('tag', $tags);
						}
						if($this->input->post('tagfield') != null && trim($this->input->post('tagfield')) != '') {
							$tags = array(
									'appid' 	=> $app->id,
									'tag' 		=> $this->input->post('tagfield'),
									'newsitemid' => $id
								);
							$this->general_model->insert('tag', $tags);
						}
					}

					$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been updated'));
					$this->general_model->update('app', $app->id, array('timestamp' => time()));
					redirect('section/news/'.$section->id);
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
                
                if($imageError != '') {
                    $error .= '<br />' .$imageError;
                }
			}
		}
		
		$cdata['content'] 		= $this->load->view('c_news_edit_content', array('app' => $app, 'newsitem' => $newsitem, 'error' => $error, 'languages' => $languages, 'tags' => $tags, 'apptags' => $apptags, 'tags' => $tags), TRUE);
		$cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id, $section->title => 'section/news/'.$section->idd, __("Edit: ") . $newsitem->title => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$this->load->view('master', $cdata);
	}

	function deletenews($id, $sectionid) {
		$this->load->model('news_model');
		$newsitem = $this->news_model->getNewsById($id);
		$appid = $newsitem->appid;
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');
        //delete translations
        $this->language_model->removeTranslations('newsitem', $id);
		if($this->general_model->remove('newsitem', $id)){
			$this->load->model('tag_model');
			if($id != 0) {
				$this->tag_model->removeTagsOfObject('newsitemid', $id);
			}
			
			$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
			$this->general_model->update('app',$appid,array('timestamp' => time()));
			redirect('section/news/'.$sectionid);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('section/news/'.$sectionid);
		}
	}

    public function importrss($id) {
    	$this->load->model('news_model');
    	$this->load->model('tag_model');
        $section = $this->contentmodule_model->getSectionById($id);
        $contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
        $app = $this->app_model->getbyid($contentmodule->appid);
        _actionAllowed($app, 'app');
        if ($this->input->post('postback') == "postback") {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('url', 'url', 'trim|required');
            $this->form_validation->set_rules('tag', 'tag', 'trim|required');            
            if ($this->form_validation->run() == FALSE) {
                $error = __("Some fields are missing.");
            } else {
                $tag = $this->input->post('tag');
                $url = $this->input->post('url');
                $urlExists = $this->contentmodule_model->urlExists($app->id,$url);
                
                if($urlExists > 0){
                    $error = __("<p>RSS Feed URL already exists.</p>");
                    $this->session->set_flashdata('error', __("<p>RSS Feed URL already exists.</p>"));                        
                    redirect('section/importrss/' . $section->id);
                }

                $newssourceData = array(
                    "appid"   => $app->id,
                    "type"      => "rss",
                    "url"       => $url,
                    "timestamp" => date("Y-m-d H:i:s",time()),
                    "refreshrate" => abs($this->input->post('refreshrate')),
                    'tag' => $tag
                );
                if($sourceid = $this->general_model->insert('newssource', $newssourceData)){
                	$tagExists = $this->tag_model->checkTag('section', $section->id, $tag);
                	if($tagExists == false) {
                        $tagData = array(
                            "sectionid" => $section->id,
                            "tag" => (string) $tag                                
                        );                            
                        $this->general_model->insert('tag', $tagData);
                    }                       
                }

				$source = (object) $newssourceData;
				$source->id = $sourceid;

				$insert = array();
				$rss_url = $url;
				$rssfeed = $this->news_model->rss_to_array($rss_url);
				foreach($rssfeed as $item) {
					$hash = $item['hash'];
						
					$exists = $this->db->query("SELECT id, hash FROM newsitem WHERE hash='$hash' AND appid = $app->id LIMIT 1");
					if($exists->num_rows() == 0) {
						$insert = '';
						$title = $item['title'];
						$txt = $item['description'];
						$url = $item['link'];
						$videourl = $item['videourl'];
						if($item['pubDate']!= null && !empty($item['pubDate'])) {
							$date = date("Y-m-d H:i:s", strtotime($item['pubDate']));
						} else {
							$date = date("Y-m-d H:i:s",time());
						}
						$image = '';
						$imgpos = stripos($txt, '<img');
						if($imgpos !== false) {
							// var_dump($item['description']);
							$imgend = stripos($txt, '</img>', $imgpos) + 6;
							if($imgend === false || $imgend == 6) {
								$imgend = stripos($txt, '/>', $imgpos) + 3;
							}
							if($imgend !== false && $imgpos !== false) {
								$imglength = $imgend - $imgpos;
								$image = substr($txt, $imgpos, $imglength);
								preg_match('@src="([^"]+)"@' , $image , $imagematch);
								if(isset($imagematch[1])) {
									$image = $imagematch[1];
								} else {
									$image = '';
								}
								// $txt = substr($txt, 0, $imgpos) . substr($txt, $imgend - 1);
							}
						}
						
						if(empty($image)) {
							preg_match('@enclosure url="([^"]+)"@' , $txt , $imagematch);
							if(isset($imagematch[1])) {
								$image = $imagematch[1];
							} else {
								preg_match('@media:content url="([^"]+)"@' , $txt , $imagematch);
								if(isset($imagematch[1])) {
									$image = $imagematch[1];
								} else {
									$image = '';
								}
							}
						}

						if(isset($item['image'])) {
							$image = $item['image'];
						}

						$deletetime = time() - $source->deleteafterdays * 3600 * 24;
						if($source->deleteafterdays == 0 || strtotime($date) >= $deletetime) {
							$insert = "('".$app->id."','".mysql_real_escape_string($title)."','".mysql_real_escape_string($txt)."','".$image."','".$url."','".$source->id."','".$date."','".$hash."', '".$videourl."')";
						}

					
						if(!empty($insert)) {
							// $insert = implode(',',$insert);
							$this->db->query("INSERT INTO newsitem (appid, title, txt, image, url, sourceid, datum, hash, videourl) VALUES $insert");
							$newsid = $this->db->insert_id();
						}


						//add translated fields to translation table
						if($newsid) {
							if($languages != null) {
							    foreach ($languages as $language) {
							        $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, (string) $title);
							        $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $txt);
							    }
							}
							$tagData = array(
								"appid" => $app->id,
							    "newsitemid" => (string) $newsid,
							    "tag" => (string) $tag                                
							);
							$this->general_model->insert('tag', $tagData);
						}
					}
				}

                if ($error == '') {
                    $this->session->set_flashdata('event_feedback', __('The rss has successfully been imported'));
                    $this->general_model->update('app', $app->id, array('timestamp' => time()));
                    redirect('section/news/' . $section->id);
                }
            }
        }

        $cdata['content'] = $this->load->view('c_importrss_view', array('error' => $error, 'app' => $app, 'section' => $section, 'contentmodule' => $contentmodule), TRUE);
        $cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id, $section->title => 'section/news/'.$section->id, 'Import RSS' => $this->uri->uri_string());
        $this->load->view('master', $cdata);
    }

    function rsslist($sectionid) {
    	$this->load->model('tag_model');
    	$this->load->model('news_model');
        $error = "";
        $section = $this->contentmodule_model->getSectionById($sectionid);
        $contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
		$app = $this->app_model->getbyid($contentmodule->appid);
		_actionAllowed($app, 'app');

		$tags = $this->tag_model->getTagsOfObject('section', $section->id);
		$currentRSSfeeds = array();
		foreach($tags as $t) {
			$feeds = $this->news_model->getRssSourceByTag($app->id, $t->tag);
			if($feeds) {
				foreach($feeds as $feed) {
					$currentRSSfeeds[] = $feed;
				}
			}
		}
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$cdata['content'] 		= $this->load->view('c_news_rsslist', array('app' => $app, 'error' => $error, 'currentRSSfeeds' => $currentRSSfeeds, 'section' => $section), TRUE);
		$cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id, $section->title => 'section/news/'.$section->id, 'RSS' => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$this->load->view('master', $cdata);
        
    }

    function refreshsource($sourceid, $sectionid) {
  //   	$this->load->model('news_model');
  //       $source = $this->news_model->getSourceOfId($sourceid);

		// $app = $this->app_model->get($source->appid);
		// _actionAllowed($app, 'app');
		// $this->news_model->refreshrss($source, $app, 'app');
	 //    $this->general_model->update('app', $app->id, array('timestamp' => time()));
	 //    redirect('section/news/'.$sectionid);
    }

	function deletesource($sourceid, $sectionid) {
		$this->load->model('news_model');
		$source = $this->news_model->getSourceOfId($sourceid);
		$app = $this->app_model->get($source->appid);
		_actionAllowed($app, 'app');
		$this->news_model->removeNewsOfSourceId($sourceid);
		$this->general_model->remove('newssource', $sourceid);
		$this->general_model->update('app', $app->id, array('timestamp' => time()));
		redirect('section/rsslist/'.$sectionid);
	}
}
