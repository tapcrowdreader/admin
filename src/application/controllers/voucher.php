<?php if(!defined('BASEPATH')) exit('No direct script access');

class Voucher extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Voucher() {
		parent::__construct();
	}
	
	function index() {
		$this->activate();
	}
	
	function activate($phoneid = '', $eventid = '') {
		
		$this->load->library('form_validation');
		if($phoneid != '' && $eventid != '') {
			// INIT CONFIG
			$error = '';
			$completion_code = '1234';
			
			/*
			$cafes = array(
				'SN1001' => 'Six Nations',
				'BB1001' => 'Biberium',
				'BL1001' => 'Brasserie Du Lombard',
				'MZ1001' => 'Mezzo',
				'MK1001' => 'Monk',
				'MC1001' => 'Michael Collins'
			);
			*/
			
			$cafes = array( 'SN1001', 'BB1001', 'BL1001', 'MZ1001', 'MK1001', 'MC1001' );
			
			$granted = FALSE;
			
			$event = $this->db->query("SELECT * FROM event WHERE id = '$eventid' LIMIT 1");
			if ($event->num_rows() == 0) {
				$error = "Unknown event!";
			} else {
				$objsess = $event->row();
				if ($this->input->post('postback') == 'postback') {
					$this->form_validation->set_rules('code', 'code', 'trim|required');
					
					if($this->form_validation->run() == FALSE) {
						$error = "Code must be filled in.";
					} else {
						if (in_array(strtoupper(set_value('code')), $cafes)) {
							$vouchers = $this->db->query("SELECT * FROM voucher WHERE phoneid = '$phoneid' AND eventid = '$eventid' AND code = '" . set_value('code') . "' LIMIT 1");
							if ($vouchers->num_rows() == 0) {
								$voucherdata = array(
									"phoneid"	=> $phoneid,
									"eventid" 	=> $eventid,
									"code" 		=> set_value('code')
								);
								
								if($this->db->insert('voucher', $voucherdata)){
									$this->session->set_flashdata('feedback_voting','SantÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â©, geniet van uw frisse Palm!');
									redirect("voucher/activate/".$phoneid."/".$eventid."/");
								} else {
									$error = "Validation failed, please try again?";
								}
							} else {
								$granted = TRUE;
							}
						} else {
							$error = "Invalid code!";
						}
					}
				}
			}
			
			$this->load->view('voucher/master', array('granted' => $granted, 'error' => $error));
			
		} else {
			die('INVALID AUTHENTICATION');
		}
		
	}
	
	function redbull($phoneid = '', $eventid = '') {
		
		$this->load->library('form_validation');
		if($phoneid != '' && $eventid != '') {
			// INIT CONFIG
			$error = '';
			$completion_code = '1234';
			
			$granted = FALSE;
			
			$event = $this->db->query("SELECT * FROM event WHERE id = '$eventid' LIMIT 1");
			if ($event->num_rows() == 0) {
				$error = "Unknown event!";
			} else {
				$objsess = $event->row();
				if ($this->input->post('postback') == 'postback') {
					$this->form_validation->set_rules('code', 'code', 'trim|required');
					
					if($this->form_validation->run() == FALSE) {
						$error = "Code must be filled in.";
					} else {
						$vouchers = $this->db->query("SELECT * FROM voucher WHERE phoneid = '$phoneid' AND eventid = '$eventid' LIMIT 1");
						if ($vouchers->num_rows() == 0) {
							$voucherdata = array(
								"phoneid"	=> $phoneid,
								"eventid" 	=> $eventid,
								"datetime" 	=> date("Y-m-d H:i:s", time())
							);
							
							if($this->db->insert('voucher', $voucherdata)){
								$this->session->set_flashdata('feedback_voting','Cheers, enjoy your free RedBull!');
								redirect("voucher/redbull/".$phoneid."/".$eventid."/");
							} else {
								$error = "Validation failed, please try again?";
							}
						} else {
							$granted = TRUE;
						}
					}
				}
			}
			
			$this->load->view('voucher/redbull', array('granted' => $granted, 'error' => $error));
			
		} else {
			die('INVALID AUTHENTICATION');
		}
		
	}
	
}