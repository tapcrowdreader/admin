<?php 
class Uploadify extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function confbagexhibitor() {
		/*
		Uploadify
		Copyright (c) 2012 Reactive Apps, Ronnie Garcia
		Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
		*/

		// Define a destination
		$targetFolder = '/var/www/UPLOAD/Live/upload/confbagfiles/'; // Relative to the root

		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $targetFolder;
			$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];

			if (file_exists($targetFile)) {
				$targetFile = rtrim($targetPath,'/') . '/' . substr($_FILES['Filedata']['name'], 0, strpos($_FILES['Filedata']['name'], '.')).'_'.time().substr($_FILES['Filedata']['name'], strpos($_FILES['Filedata']['name'], '.'), strlen($_FILES['Filedata']['name']));
			}
			
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif','png', 'xsl', 'pdf', 'xlsx', 'xls', 'csv', 'doc', 'docx', 'ppt', 'pptx', 'txt'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			
			if (in_array($fileParts['extension'],$fileTypes)) {
				move_uploaded_file($tempFile,$targetFile);
				echo '1';
			} else {
				echo 'Invalid file type.';
			}
		}
	}

}