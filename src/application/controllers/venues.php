<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Venues extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}
	
	//php 4 constructor
	function Venues() {
		parent::__construct();
		if(_authed()) { }
	}
	
	function index($tag = '') {
		$this->session->set_userdata('venueid', '');
		$this->session->set_userdata('eventid', '');
		$this->myVenues($tag);
	}

	function launcher($launcherid) {
		$app = _currentApp();
		$launcher = $this->db->query("SELECT * FROM launcher WHERE id = ?", $launcherid);
		if($launcher->num_rows() != 0) {
			$launcher = $launcher->row();
			$tag = $launcher->tag;
			$this->myVenues($tag, $launcherid);
		}
	}

	function chooselauncher($appid) {
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');
		$launchers = $this->module_mdl->getVenuesCityLaunchers($app);
		if(count($launchers) == 1) {
			redirect('venues/launcher/'.$launchers[0]->id);
		}
		$cdata['content'] 		= $this->load->view('c_events_chooselauncher', array('launchers' => $launchers, 'app' => $app), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array();
		if($app->familyid == 2) {
			$crumbapp = array($app->name => 'apps/view/'.$app->id);
			$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app);
		$this->load->view('master', $cdata);
	}
	
	function myVenues($tagParam = '', $launcherid = '') {
		$app = _currentApp();
        if($app != false && isset($app->subdomain) && $app->subdomain != "") {
            $this->iframeurl = "home/index";
        }
		if($tagParam == '') {
			$venues = $this->venue_model->getAppVenue($app->id);
		} else {
			$venues = $this->venue_model->getVenuesByTag($app->id, $tagParam);
		}
		
		$venuesTags = array();
		if(empty($venues) || $venues == false && $app->familyid == 2) {
			redirect('venue/add');
		}
		
		if(count($venues) == 1 && $app->familyid == 2) {
			redirect('venue/view/'.$venues[0]->id);
		}

		if($app->apptypeid == 5 && $tagparam == '' && $launcherid == '') {
			redirect('venues/chooselauncher/'.$app->id);
		}


		if(isset($venues) && $venues != false) {
			if($app->familyid == 3) {
				if($tagParam != '') {
					$this->iframeurl = "venues/tag/".$app->id."/".$tagParam;
				} else {
					$this->iframeurl = "venues/app/".$app->id;
				}
			} else {
				$this->iframeurl = "venues/app/".$app->id;
			}
		}

		if($launcherid != '') {
			$this->iframeurl = 'venues/launcher/'.$app->id.'/'.$launcherid;
		}

		if($app->apptypeid == 1 && $venues != false) {
			$this->iframeurl = 'app/expo/'.$app->id.'/venues';
		}

		if($venues!=false && $venues != null) {
			foreach($venues as $venue) {
				// TAGS
				$tagNames = explode(',', $venue->tags);
				$venue->tags = $tagNames;

				//custom lier
				if($app->id == 55 && $tagParam == 'ToDrink') {
					$this->iframeurl = 'venues/tag/'.$app->id.'/ToDrink__tag__ToEat';
					if(!in_array(strtolower($tagParam),$tagNames) && !in_array(strtolower('ToEat'),$tagNames)) {
						unset($venue);
					}
				} 
			}
		}



		//Metadata curstomfields
		$showButtonMetadata = false;
		// if(!empty($launcherid)) {
		// 	$venueslaunchers = $this->module_mdl->getLauncheridsOfObjects('venue', $venues);
		// 	$metadata = $this->metadata_model->getMetadataNamesOfLaunchers($venueslaunchers);
		// 	if(!empty($metadata)) {
		// 		$showButtonMetadata = true;
		// 	}
		// 	$count = 0;
		// 	$prev = '';
		// 	foreach($metadata as $m) {
		// 		$countCurrent = count($m);
		// 		if($countCurrent != $count && $count != 0 && $countCurrent != 0) {
		// 			$showButtonMetadata = false;
		// 			break;
		// 		}

		// 		$prev = $m;

		// 		$count = $countCurrent;
		// 	}

		// 	if($showButtonMetadata) {
		// 		$namesPrev = array();
		// 		$first = array_shift($metadata);
		// 		foreach($first as $f) {
		// 			$namesPrev[$f->name] = $f->name;
		// 		}
		// 		foreach($metadata as $m) {
		// 			foreach($m as $m2) {
		// 				if(!isset($namesPrev[$m2->name])) {
		// 					$showButtonMetadata = false;
		// 					break;
		// 				}
		// 			}
		// 		}
		// 	}
		// }

		
		$cdata['content'] 		= $this->load->view('c_venues', array('venues' => $venues, 'app' => $app, 'tagParam' => $tagParam, 'launcherid' => $launcherid, 'showButtonMetadata' => $showButtonMetadata), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		if($launcherid == '') {
			$cdata['crumb']			= array(($app->familyid == 3) ? __("POI's") : __("Venues") => "venues");
		} else {
			$this->load->model('module_mdl');
			$launcher = $this->module_mdl->getLauncherById($launcherid);
			$cdata['crumb']			= array($launcher->title => "venues/launcher/".$launcherid);
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app);
		$this->load->view('master', $cdata);
	}
	
	function sort() {
		$ids = $this->input->post('ids');
		$i = 10;
		if(isset($ids) && $ids != null && $ids != false) {
			foreach($ids as $id) {
				$data = array('order' => $i);
				$this->general_model->update('venue', $id, $data);
				$i++;
				_updateVenueTimeStamp($id);
			}
		}
	}

	function removelauncher($launcherid) {
		$launcher = $this->module_mdl->getLauncherById($launcherid);
		$app = $this->app_model->get($launcher->appid);
		_actionAllowed($app, 'app');

		$this->module_mdl->removeLauncher($launcher);

		redirect('apps/view/'.$launcher->appid);
	}
}