<?php

/**
 * InAppPurchase controller
 *
 * Objects hold [appid,productid,productvalue,type]
 */
class Inapppurchases extends CI_Controller
{
	/**
	 * Module settings
	 * @var array
	 */
	protected $_module_settings = array(
		'plural' => 'InAppPurchases',
		'singular' => 'InAppPurchase',
		'add_url' => 'inapppurchases/add',
		'edit_url' => 'inapppurchases/save/--id--',
	);

	/**
	 * Redirect to current app
	 *
	 * @return void
	 */
	public function index()
	{
		redirect('inapppurchases/app/'._currentApp()->id);
	}

	/**
	 * Display Inapppurchases for given appId
	 *
	 * @param int $appId
	 * @return void
	 */
	public function app( $appId )
	{
		if(!is_numeric($appId)) redirect(404);

		$this->_module_settings['headers'] = array(
			__('Product Id') => 'productid',
			__('Product Value') => 'productvalue',
		);

		$this->_module_settings['actions'] = array(
			'edit' => (object)array(
				'title' => __('Edit'),
				'href' => 'inapppurchases/edit/--id--',
				'icon_class' => 'icon-pencil',
				'btn_class' => 'btn-warning',
			),
			'delete' => (object)array(
				'title' => __('Delete'),
				'href' => 'inapppurchases/remove/--id--',
				'icon_class' => 'icon-remove',
				'btn_class' => 'btn-danger',
			),
		);

		try {
			$query = array('appid' => $appId);
			$inapppurchases = \Tapcrowd\Model\Inapppurchase::getInstance()->select($query);
		} catch(\Exception $e) {
			Notifications::getInstance()->alert($e->getMessage(), 'error');
			redirect('inapppurchases');
		}

		$params = array('data' => array_reverse($inapppurchases));
		$this->_loadView('tc_listview', $params);
	}

	/**
	 * Display Inapppurchases add screen
	 *
	 * @param int $inapppurchaseId
	 * @return void
	 */
	public function add()
	{
		$inapppurchase = \Tapcrowd\Model\Inapppurchase::getInstance()->getEmpty();
		$this->_loadView('tc_editview', array('data' => $inapppurchase));
	}

	/**
	 * Display Inapppurchases edit screen
	 *
	 * @param int $inapppurchaseId
	 * @return void
	 */
	public function edit( $inapppurchaseId )
	{
		if(!is_numeric($inapppurchaseId)) redirect(404);

		try {
			$query = array('id' => $inapppurchaseId);
			$inapppurchase = \Tapcrowd\Model\Inapppurchase::getInstance()->selectFirst($query);
		} catch(\Exception $e) {
			Notifications::getInstance()->alert($e->getMessage(), 'error');
			redirect('inapppurchases');
		}

		$this->_loadView('tc_editview', array('data' => $inapppurchase));
	}

	/**
	 * Store Inapppurchase, redirects
	 *
	 * @param int $appId
	 * @return void
	 */
	public function save()
	{
		# Validate
		$input = filter_input_array(INPUT_POST, array(
			'id' => FILTER_SANITIZE_NUMBER_INT,
			'productid' => FILTER_SANITIZE_STRING,
			'productvalue' => FILTER_SANITIZE_STRING,
		));

		$app = _currentApp();
		$input['appid'] = $app->id;
		$input['type'] = 'non-consumable';

		try {
			$inapppurchase = \Tapcrowd\Model\Inapppurchase::getInstance()->save($input);
		} catch(\Exception $e) {
			Notifications::getInstance()->alert($e->getMessage(), 'error');
			$page = 'inapppurchases/' . (empty($input['id'])? 'add/' : 'edit/');
			redirect($page.$app->id);
		}

		redirect('inapppurchases/app/'.$app->id);
	}

	/**
	 * Remove Inapppurchase. redirects
	 *
	 * @param int $inapppurchaseId
	 * @return void
	 */
	public function remove( $inapppurchaseId )
	{
		if(!is_numeric($inapppurchaseId)) redirect(404);

		try {
			$app = _currentApp();
			$query = array('id' => $inapppurchaseId);
			$inapppurchase = \Tapcrowd\Model\Inapppurchase::getInstance()->delete($query);
		} catch(\Exception $e) {
			Notifications::getInstance()->alert($e->getMessage(), 'error');
			redirect('inapppurchases');
		}

		redirect('inapppurchases/app/'.$app->id);
	}

	/**
	 * Internal method to load the given view
	 *
	 * @param string $view
	 * @param array $params
	 * @return void
	 */
	protected function _loadView( $view, $params )
	{
		$app = _currentApp();
		$data = array('app' => $app, 'settings' => (object)$this->_module_settings);
		$cdata['content'] = $this->load->view($view, array_merge($data, $params), true);
		$cdata['crumb'] = array(__('InAppPurchases') => 'inapppurchases/app/'.$app->id);
		$cdata['sidebar'] = $this->load->view('c_sidebar', array(), true);
		$this->load->view('master', $cdata);
	}
}
