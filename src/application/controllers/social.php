<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Social extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('social_model');
	}
	
	//php 4 constructor
	function Social() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('social_model');
	}
	
	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = "social/eventtwitter/" . $id;

		$social = $this->social_model->getByEventId($id);
		if($social != false && $social != null) {
			redirect('social/edit/'.$social->id.'/event');
		} else {
			redirect('social/add/'.$event->id.'/event');
		}
	}
	
	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->iframeurl = "social/venuetwitter/" . $id;
		
		$social = $this->social_model->getByVenueId($id);

		if($social != false && $social != null) {
			redirect('social/edit/'.$social->id.'/venue');
		} else {
			redirect('social/add/'.$venue->id.'/venue');
		}
	}
	
	function app($id) {
		if($id == FALSE || $id == 0) redirect('apps');
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');
		$this->iframeurl = "social/apptwitter/" . $id;
		
		$social = $this->social_model->getByAppId($id);

		if($social != false && $social != null) {
			redirect('social/edit/'.$social->id.'/app');
		} else {
			redirect('social/add/'.$id.'/app');
		}
	}
	
	function add($id, $type = 'event') {
		if($type == 'venue'){
			$this->load->library('form_validation');
			$error = "";
			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = "news/venue/" . $id;
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('twitter', 'twitter', 'trim');
				$this->form_validation->set_rules('twithash', 'twithash', 'trim');
				$this->form_validation->set_rules('facebookid', 'facebookid', 'trim');
				$this->form_validation->set_rules('facebookappid', 'facebookid', 'trim');
				$this->form_validation->set_rules('rss', 'rss', 'trim');
				$this->form_validation->set_rules('postorwall', 'postorwall', 'trim');
				$this->form_validation->set_rules('youtube', 'youtube', 'trim');
				$this->form_validation->set_rules('mentions', 'mentions', 'trim');

				$mentions = 0;
				if($this->input->post('mentions')) {
					$mentions = 1;
				}
				
				if($this->form_validation->run() == FALSE && $imageError != ''){
					$error = __("Some fields are missing.");
				} else {
					$data = array( 
							'venueid'		=> $venue->id,
							'twitter'		=> set_value('twitter'),
							'twithash'		=> set_value('twithash'),
							'facebookid'	=> set_value('facebookid'),
							// 'rss'			=> set_value('rss'),
							'postorwall'	=> set_value('postorwall'),
							// 'facebookappid' => set_value('facebookappid'),
							// 'youtube'		=> set_value('youtube'),
							'mentions'		=> $mentions
						);
					
					if(($newid = $this->general_model->insert('socialmedia', $data))){                        
						$this->session->set_flashdata('event_feedback', __('The social media has successfully been added!'));
						_updateVenueTimeStamp($venue->id);
						redirect('social/edit/'.$newid.'/venue');
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_social_add', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Social media") => "social/venue/".$venue->id, __("Add") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'social');
			$this->load->view('master', $cdata);
		} elseif($type == 'app') {
			$this->load->library('form_validation');
			$error = "";
			
			$app = $this->app_model->get($id);
			_actionAllowed($app, 'app');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			$this->iframeurl = "news/event/" . $id;
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('twitter', 'twitter', 'trim');
				$this->form_validation->set_rules('twithash', 'twithash', 'trim');
				$this->form_validation->set_rules('facebookid', 'facebookid', 'trim');
				$this->form_validation->set_rules('rss', 'rss', 'trim');
				$this->form_validation->set_rules('postorwall', 'postorwall', 'trim');
				$this->form_validation->set_rules('facebookappid', 'facebookappid', 'trim');
				$this->form_validation->set_rules('youtube', 'youtube', 'trim');
				$this->form_validation->set_rules('mentions', 'mentions', 'trim');

				$mentions = 0;
				if($this->input->post('mentions')) {
					$mentions = 1;
				}
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {					
					$data = array( 
							'appid'			=> $app->id,
							'twitter'		=> set_value('twitter'),
							'twithash'		=> set_value('twithash'),
							'facebookid'	=> set_value('facebookid'),
							// 'rss'			=> set_value('rss'),
							'postorwall'	=> set_value('postorwall'),
							// 'facebookappid' => set_value('facebookappid'),
							// 'youtube'		=> set_value('youtube')
							'mentions'		=> $mentions
						);
					
					if(($newid = $this->general_model->insert('socialmedia', $data))){                        
						$this->session->set_flashdata('event_feedback', __('The social media has successfully been added!'));
						$this->general_model->update('app', $id, array('timestamp' => time()));
						redirect('social/edit/'.$newid.'/app');
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_social_add', array('app' => $app, 'error' => $error, 'languages' => $languages), TRUE);
			$cdata['crumb']			= array(__("Apps") => "apps", $app->name => "apps/view/".$app->id, __("Social media") => "social/app/".$app->id, __("Add") => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";
			
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = "news/app/" . $id;
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('twitter', 'twitter', 'trim');
				$this->form_validation->set_rules('twithash', 'twithash', 'trim');
				$this->form_validation->set_rules('facebookid', 'facebookid', 'trim');
				$this->form_validation->set_rules('rss', 'rss', 'trim');
				$this->form_validation->set_rules('postorwall', 'postorwall', 'trim');
				$this->form_validation->set_rules('facebookappid', 'facebookappid', 'trim');
				$this->form_validation->set_rules('youtube', 'youtube', 'trim');
				$this->form_validation->set_rules('mentions', 'mentions', 'trim');

				$mentions = 0;
				if($this->input->post('mentions')) {
					$mentions = 1;
				}
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {					
					$data = array( 
							'eventid'		=> $event->id,
							'twitter'		=> set_value('twitter'),
							'twithash'		=> set_value('twithash'),
							'facebookid'	=> set_value('facebookid'),
							// 'rss'			=> set_value('rss'),
							'postorwall'	=> set_value('postorwall'),
							// 'facebookappid' => set_value('facebookappid'),
							// 'youtube'		=> set_value('youtube')
							'mentions'		=> $mentions
						);
					
					if(($newid = $this->general_model->insert('socialmedia', $data))){                        
						$this->session->set_flashdata('event_feedback', __('The social media has successfully been added!'));
						_updateTimeStamp($event->id);
						redirect('social/edit/'.$newid.'/event');
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_social_add', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Social media") => "social/event/".$event->id, __("Add") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, __("Social media") => "social/event/".$event->id, __("Add") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'social');
			$this->load->view('master', $cdata);
		}
	}
	
	function edit($id, $type) {
		if($type == 'venue'){
			$this->load->library('form_validation');
			$error = "";

			$social = $this->social_model->getById($id);
			if($social == FALSE) redirect('venues');
			$venue = $this->venue_model->getById($social->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = $this->social_model->socialiframe($social);
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('twitter', 'twitter', 'trim');
				$this->form_validation->set_rules('twithash', 'twithash', 'trim');
				$this->form_validation->set_rules('facebookid', 'facebookid', 'trim');
				$this->form_validation->set_rules('rss', 'rss', 'trim');
				$this->form_validation->set_rules('postorwall', 'postorwall', 'trim');
				$this->form_validation->set_rules('facebookappid', 'facebookappid', 'trim');
				$this->form_validation->set_rules('youtube', 'youtube', 'trim');
				$this->form_validation->set_rules('mentions', 'mentions', 'trim');

				$mentions = 0;
				if($this->input->post('mentions')) {
					$mentions = 1;
				}
				
				if($this->form_validation->run() == FALSE && $imageError != ''){
					$error = "Some fields are missing.";
				} else {
					$data = array( 
							'venueid'		=> $venue->id,
							'twitter'		=> set_value('twitter'),
							'twithash'		=> set_value('twithash'),
							'facebookid'	=> set_value('facebookid'),
							// 'rss'			=> set_value('rss'),
							'postorwall'	=> set_value('postorwall'),
							// 'facebookappid' => set_value('facebookappid'),
							// 'youtube'		=> set_value('youtube')
							'mentions'		=> $mentions
						);
					
					if($this->general_model->update('socialmedia', $id, $data)){

						$this->db->where('venueid', $venue->id);
						$this->db->delete('facebook'); 

						$this->session->set_flashdata('event_feedback', __('The social media has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						redirect('social/edit/'.$id.'/venue');
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			
			
			$cdata['content'] 		= $this->load->view('c_social_edit', array('venue' => $venue, 'social' => $social, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Social media") => "social/venue/".$venue->id, __("Edit") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'social');
			$this->load->view('master', $cdata);
		} elseif($type=='app') {
			$this->load->library('form_validation');
			$error = "";

			$social = $this->social_model->getById($id);
			$app = $this->app_model->get($social->appid);
			_actionAllowed($app, 'app');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			$this->iframeurl = $this->social_model->socialiframe($social);
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('twitter', 'twitter', 'trim');
				$this->form_validation->set_rules('twithash', 'twithash', 'trim');
				$this->form_validation->set_rules('facebookid', 'facebookid', 'trim');
				$this->form_validation->set_rules('rss', 'rss', 'trim');
				$this->form_validation->set_rules('postorwall', 'postorwall', 'trim');
				$this->form_validation->set_rules('facebookappid', 'facebookappid', 'trim');
				$this->form_validation->set_rules('youtube', 'youtube', 'trim');
				$this->form_validation->set_rules('mentions', 'mentions', 'trim');

				$mentions = 0;
				if($this->input->post('mentions')) {
					$mentions = 1;
				}
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {					
					$data = array( 
							'appid'			=> $app->id,
							'twitter'		=> set_value('twitter'),
							'twithash'		=> set_value('twithash'),
							'facebookid'	=> set_value('facebookid'),
							// 'rss'			=> set_value('rss'),
							'postorwall'	=> set_value('postorwall'),
							// 'facebookappid' => set_value('facebookappid'),
							// 'youtube'		=> set_value('youtube')
							'mentions'		=> $mentions
						);
					
					if($this->general_model->update('socialmedia', $id, $data)){
						$this->db->where('appid', $app->id);
						$this->db->delete('facebook'); 

						$this->session->set_flashdata('event_feedback', __('The social media has successfully been updated'));
						$this->general_model->update('app', $app->id, array('timestamp' => time()));
						redirect('social/edit/'.$id.'/app');
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_social_edit', array('social' => $social, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
			$cdata['crumb']			= array(__("Social media") => "social/app/".$app->id, __("Edit") => $this->uri->uri_string());
			$cdata['modules'] 		= $this->module_mdl->getModulesForMenu('app', $app->id, $app, 'social');
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$social = $this->social_model->getById($id);
			if($social == FALSE) redirect('events');
			$event = $this->event_model->getbyid($social->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = $this->social_model->socialiframe($social);
			
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('twitter', 'twitter', 'trim');
				$this->form_validation->set_rules('twithash', 'twithash', 'trim');
				$this->form_validation->set_rules('facebookid', 'facebookid', 'trim');
				$this->form_validation->set_rules('rss', 'rss', 'trim');
				$this->form_validation->set_rules('postorwall', 'postorwall', 'trim');
				$this->form_validation->set_rules('facebookappid', 'facebookappid', 'trim');
				$this->form_validation->set_rules('youtube', 'youtube', 'trim');
				$this->form_validation->set_rules('mentions', 'mentions', 'trim');

				$mentions = 0;
				if($this->input->post('mentions')) {
					$mentions = 1;
				}
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {					
					$data = array( 
							'eventid'		=> $event->id,
							'twitter'		=> set_value('twitter'),
							'twithash'		=> set_value('twithash'),
							'facebookid'	=> set_value('facebookid'),
							// 'rss'			=> set_value('rss'),
							'postorwall'	=> set_value('postorwall'),
							// 'facebookappid' => set_value('facebookappid'),
							// 'youtube'		=> set_value('youtube')
							'mentions'		=> $mentions
						);
					
					if($this->general_model->update('socialmedia', $id, $data)){
						$this->db->where('eventid', $event->id);
						$this->db->delete('facebook'); 
						$this->session->set_flashdata('event_feedback', __('The social media has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('social/edit/'.$id.'/event');
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_social_edit', array('event' => $event, 'social' => $social, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Social media") => "social/event/".$event->id, __("Edit") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, __("Social media") => "social/event/".$event->id, __("Edit") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'social');
			$this->load->view('master', $cdata);
		}
	}
}