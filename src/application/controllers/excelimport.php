<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Excelimport extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	//php 4 constructor
	function Excelimport() {
		parent::__construct();
		if(_authed()) { }
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		if($event == FALSE) redirect('events');
		if($event->organizerid != _currentUser()->organizerId) redirect('events');

		$cdata['content'] 		= $this->load->view('c_excelimport', array('event' => $event), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Import") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, __("Import") => $this->uri->uri_string());
		}
		$this->load->view('master', $cdata);
	}

	function upload($id, $type = '') {
		if($id == FALSE || $id == 0 || $type == '') redirect('events');

		$error = "";
		$event = $this->event_model->getbyid($id);
		if($event == FALSE) redirect('events');
		if($event->organizerid != _currentUser()->organizerId) redirect('events');

		$this->load->library("form_validation");
		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('userfile', 'userfile', 'trim');

			if($this->form_validation->run() == FALSE){
				$error = __("Some fields are missing.");
			} else {
				// CHECK FOLDERSTRUCTURE
				if(is_dir("upload/import/$type/")) {
					if(!is_dir("upload/import/$type/$id")){
						mkdir("upload/import/$type/$id", 0777);
					}

					$config['upload_path'] 		= "upload/import/$type/$id/";
					$config['allowed_types'] 	= 'csv';
					$config['file_name'] 		= date("Ymd_His", time());
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					if($_FILES['userfile']['size'] === 0 || empty($_FILES['userfile']['tmp_name'])) {
						$error = __("You really need to give me a file ready for import! (csv only)");
					} else {
						if (!$this->upload->do_upload('userfile')) {
							$error = __("Oops, uploading failed?! Please try again.");
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = "upload/import/$type/$id/" . $returndata['file_name'];

							$this->session->set_flashdata('import_feedback', __('File-upload succeeded!'));
							redirect("excelimport/upload/$id/$type");
						}
					}
				}
			}
		}

		$cdata['content'] 		= $this->load->view('import/c_upload', array('event' => $event, "error" => $error, 'type' => $type), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Import") => "excelimport/event/".$id, ucfirst($type) => "excelimport/event/$id/");
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Import") => "excelimport/event/".$id, ucfirst($type) => "excelimport/event/$id/");
		}
		$this->load->view('master', $cdata);
	}

	function process($id, $type = '', $file = '') {
		if($id == FALSE || $id == 0 || $type == '' || $file == '') redirect('excelimport/event/'.$id);

		$event = $this->event_model->getbyid($id);
		if($event == FALSE) redirect('events');

		$final = FALSE;
		if($this->input->post('final') == "final") $final = TRUE;

		$log = '';
		$errorz = 0;
		$aantal = 0;
		switch ($type) {
			case 'session':
			case 'sessions':
				$session_table = 'session';
				$sessiongroup_table = 'sessiongroup';

				if(!$final) {
					$session_table = "session_".date('YmdHis', time());
					$this->db->query("create table $session_table like session");
					$sessiongroup_table = "sessiongroup_".date('YmdHis', time());
					$this->db->query("create table $sessiongroup_table like sessiongroup");
				}

				$headers = TRUE;
				$heading = array();
				$dataarray = array();
				$row = 1;
				if (($handle = fopen("upload/import/$type/$id/$file", "r")) !== FALSE) {
					$sessiongroup = "";
					$sessiongroupid = "";
					while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
						$num = count($data);
						$row++;
						$newrow = array();
						for ($c=0; $c < $num; $c++) {
							if($headers) {
								$heading[] = $data[$c];
							} else {
								$newrow[$heading[$c]] = $data[$c];
							}
						}

						if(!$headers) {
							if ($newrow['groepsnaam'] != '' && $newrow['groepsnaam'] != $sessiongroup) {
								$sessiongroup = $newrow['groepsnaam'];
								if($this->db->insert($sessiongroup_table, array(
									"eventid" 	=> $id,
									"name" 		=> $sessiongroup
								))) {
									$sessiongroupid = $this->db->insert_id();
									$log .= __("<br />Create group '" . $sessiongroup . "' and return ID<br />");
								} else {
									$sessiongroupid = "0";
									$log .= __("<br />(!) Create group '" . $sessiongroup . "' and return ID<br />");
									$errorz++;
								}
							}

							if($newrow['artistnaam'] != '') {
								if($this->db->insert($session_table, array(
									'sessiongroupid' 	=> $sessiongroupid,
									'name' 				=> $newrow['artistnaam'],
									'description' 		=> $newrow['artistomschrijving'],
									'starttime' 		=> $newrow['starttijd'],
									'endtime' 			=> $newrow['eindtijd'],
									'location' 			=> $newrow['locatie']
								))) {
									$log .= __("&nbsp;&nbsp; + Added session " . $newrow['artistnaam'] . "<br />");
								} else {
									$log .= __("&nbsp;&nbsp; ! Failed to ad session to group " . $sessiongroup . "<br />");
									$errorz++;
								}
								// data in $dataarray toevoegen;
								$dataarray[] = $newrow;
							}
						}
						$headers = FALSE;
					}
					fclose($handle);

					if(!$final) {
						$this->db->query("drop table if exists $session_table");
						$this->db->query("drop table if exists $sessiongroup_table");
					} else {
						$this->session->set_flashdata('import_feedback', __('Import succeeded!'));
						redirect("excelimport/event/$id/");
					}
				}

				$aantal = count($dataarray);

				break;
		}

		$cdata['content'] 		= $this->load->view('import/c_process', array('event' => $event, 'log' => $log, 'errors' => $errorz, 'aantal' => $aantal), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Import") => "excelimport/event/$id/");
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Import") => "excelimport/event/$id/");
		}
		$this->load->view('master', $cdata);
	}

	function delete($id, $type = '', $userfile = '') {
		if($id == FALSE || $id == 0 || $type == '' || $userfile == '') redirect('excelimport/event/'.$id);
		$fileurl = "upload/import/".$type."/".$id."/".$userfile;
		if(!file_exists($fileurl)) redirect('excelimport/event/'.$id);
		// Delete image + generated thumbs
		unlink($fileurl);

		$this->session->set_flashdata('import_feedback', __('File is deleted!'));
		redirect("excelimport/upload/$id/$type");
	}


	function sessions($eventid) {
		$this->load->model('general_model');

		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		if($this->input->post('postback') == 'postback') {
			//Uploads excel
			if(!is_dir($this->config->item('imagespath') . "upload/excelfiles/".$eventid)){
				mkdir($this->config->item('imagespath') . "upload/excelfiles/".$eventid, 0755,true);
			}
			$config['upload_path'] = $this->config->item('imagespath') .'upload/excelfiles/'.$eventid;
			$config['allowed_types'] = 'text/csv|text|csv';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('excelfile')) {
				$image = ""; //No image uploaded!
				if($_FILES['excelfile']['name'] != '') {
					$error .= $this->upload->display_errors();
				}
			} else {
				//successfully uploaded
				$returndata = $this->upload->data();
				$headers = TRUE;
				$heading = array();

				$row = 1;
				$error = '';
				if(file_exists($this->config->item('imagespath') . "upload/excelfiles/".$eventid ."/sessions.csv")) {
					if (($handle = fopen($this->config->item('imagespath') . "upload/excelfiles/".$eventid ."/sessions.csv", "r")) !== FALSE) {
					while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
						$num = count($data);
						$row++;
						$newrow = array();
						for ($c=0; $c < $num; $c++) {
							if($headers) {
								$heading[] = $data[$c];
							} else {
								$newrow[$heading[$c]] = $data[$c];
							}
						}
						if(!$headers) {
							$name = str_replace("'", "\'", $newrow['Name']);
							if($name != '') {
								$sessiongroup = $newrow['Date'];
								$sessiongroupExists = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid AND name = ? LIMIT 1", $sessiongroup);
								if($sessiongroupExists->num_rows() != 0) {
									$sessiongroupid = $sessiongroupExists->row()->id;
								} else {
									$sessiongroupid = $this->general_model->insert('sessiongroup', array('eventid' => $eventid, 'name' => $sessiongroup));
								}
								$date = '';
								if(stristr($newrow['Date'],'-')) {
									//date with -
									$datearray = explode('-',$newrow['Date']);
									if(strlen($datearray[0]) == 4) {
										$date = $datearray[0] . '-' . $datearray[1] . '-' . $datearray[2];
									} elseif(strlen($datearray[2]) == 4) {
										$date = $datearray[2] . '-' . $datearray[1] . '-' . $datearray[0];
									}
								} elseif(stristr($newrow['Date'],'/')) {
									//date with /
									$datearray = explode('/',$newrow['Date']);
									if(strlen($datearray[0]) == 4) {
										$date = $datearray[0] . '-' . $datearray[1] . '-' . $datearray[2];
									} elseif(strlen($datearray[2]) == 4) {
										$date = $datearray[2] . '-' . $datearray[1] . '-' . $datearray[0];
									}
								}  elseif(stristr($newrow['Date'],' ')) {
									//date with spaces
									$newrow['Date'] = strtolower($newrow['Date']);
									if($timestamp = strtotime($newrow['Date'])) {
										$date = date('Y-m-d',$timestamp);
									}
								} else {
									$error .= 'wrongdate';
								}

								$starttime = $date . ' ' . $newrow['Starttime'];
								$endtime = $date . ' ' . $newrow['Endtime'];

								if($error == '') {
									$sessionid = $this->general_model->insert('session', array(
										'sessiongroupid' 	=> $sessiongroupid,
										'eventid'			=> $eventid,
										'name' 				=> $name,
										'description' 		=> $newrow['Description'],
										'starttime' 		=> $starttime,
										'endtime' 			=> $endtime,
										'location' 			=> $newrow['Location'],
										'twitter' 			=> $newrow['Twitter'],
										'url' 				=> $newrow['Url']
									));
								}
							}
						}
						$headers = FALSE;
					}
					fclose($handle);
				}
				} else {
					echo "no file";
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_excelimport2', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Sessions") => "sessions/event/".$event->id, __("Excel Import") => "excelimport/sessions/".$event->id);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$this->load->view('master', $cdata);
	}

//	function importExhibitorsVOV() {
//		if (($handle = fopen("upload/import/importVOVtags.csv", "r")) !== FALSE) {
//			while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
//				$exid = $data[0];
//				$i = 0;
//				foreach($data as $data) {
//					if($i != 0) {
//						$tag = $data;
//
//						if(trim($tag) != '') {
//							$tagdata = array(
//								'appid'			=> 228,
//								'tag'			=> $tag,
//								'exhibitorid'	=> $exid
//							);
//							$this->general_model->insert('tag', $tagdata);
//						}
//					}
//					$i++;
//				}
//			}
//		}
//	}

	function importSessionsVOV() {
//		$this->load->model('general_model');
//		$headers = TRUE;
//		$heading = array();
//		$dataarray = array();
//		$row = 1;
//		if (($handle = fopen($this->config->item('imagespath') . "upload/import/sessions/sessionsVOV3.csv", "r")) !== FALSE) {
//			while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
//
//				$num = count($data);
//				$row++;
//				$newrow = array();
//				for ($c=0; $c < $num; $c++) {
//					if($headers) {
//						$heading[] = $data[$c];
//					} else {
//						$newrow[$heading[$c]] = $data[$c];
//					}
//				}
//
//				if(!$headers) {
////					$name = $newrow['Session name'];
////					$date = $newrow['Date'];
//
//					//$existingRow = $this->db->query("SELECT * FROM session WHERE name = '".str_replace("'","\'", $name)."' AND sessiongroupid = '$groupid'");
//					//$existingRow = $existingRow->row();
////					//var_dump($newrow);
////
//					if($sessionid = $this->general_model->insert('session', array(
//						'sessiongroupid' 	=> $newrow['sessiongroup'],
//						'eventid'			=> '517',
//						'name' 				=> $newrow['Session name'],
//						'description' 		=> $newrow['Description'],
//						'starttime' 		=> $newrow['Starttime'],
//						'endtime' 			=> $newrow['endtime'],
//						'location' 			=> $newrow['Zaal'],
//						'speaker' 			=> $newrow['Spreker']
//					))) {
//						if($newrow['Bedrijven'] != '') {
//						$metadata = $this->db->insert('metadata', array(
//							'table'			=> 'session',
//							'identifier'	=> $sessionid,
//							'key'			=> 'bedrijf',
//							'value'			=> $newrow['Bedrijven']
//						));
//						$log .= "&nbsp;&nbsp; + Added session " . $newrow['Session name'] . "<br />";
//						}
//					} else {
//						$log .= "&nbsp;&nbsp; ! Failed to ad session to group " . $newrow['sessiongroup'] . "<br />";
//						$errorz++;
//					}
//					// data in $dataarray toevoegen;
//					$dataarray[] = $newrow;
//				}
//				$headers = FALSE;
//			}
//			fclose($handle);
//		}
	}

	function boekenbeurs() {
//		$this->load->model('general_model');
//		$headers = TRUE;
//		$heading = array();
//		$dataarray = array();
//		$row = 1;
//		if (($handle = fopen($this->config->item('imagespath') . "upload/import/sessions/sessionsBoekenbeurs3.csv", "r")) !== FALSE) {
//			while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
//
//				$num = count($data);
//				$row++;
//				$newrow = array();
//				for ($c=0; $c < $num; $c++) {
//					if($headers) {
//						$heading[] = $data[$c];
//					} else {
//						$newrow[$heading[$c]] = $data[$c];
//					}
//				}
//
//				if(!$headers) {
//					//groups
//					$groupName = $newrow['Date'];
//					$existingGroup = $this->db->query("SELECT * FROM sessiongroup WHERE name = '$groupName' AND eventid = '589'");
//					if($existingGroup->num_rows() == 0) {
//						$groupid = $this->general_model->insert('sessiongroup', array("eventid" => "589", "name" => $groupName));
//					} else {
//						$groupid = $existingGroup->row()->id;
//					}
//
//					//session
//					$name = $newrow['Session Name'];
//					$existingRow = $this->db->query("SELECT * FROM session WHERE name = '".str_replace("'","\'", $name)."' AND sessiongroupid = '$groupid'");
//
//					$data = array(
//						"name"			=> $name,
//						"sessiongroupid"=> $groupid,
//						"eventid"		=> '589',
//						"description"	=> $newrow['Description'],
//						"starttime"		=> $newrow['Starttime'],
//						"endtime"		=> $newrow['Endtime'],
//						"location"		=> $newrow['Location']
//					);
//
//					if($existingRow->num_rows() == 0) {
//						$sessionid = $this->general_model->insert('session', $data);
//					} else {
//						$sessionid = $existingRow->row()->id;
//						$this->general_model->update('session', $sessionid, $data);
//					}
//
//					//tag
//					$tagName = $newrow['Tag'];
//					$tagData = array(
//							"tag" => $newrow['Tag'],
//							"appid" => '283',
//							"sessionid" => $sessionid
//						);
//					$existingTag = $this->db->query("SELECT * FROM tag WHERE tag = '$tagName' AND sessionid = $sessionid");
//					if($existingTag->num_rows() == 0) {
//						$tagid = $this->general_model->insert('tag', $tagData);
//					} else {
//						$tagid = $this->general_model->update('tag', $existingTag->row()->id, $tagData);
//					}
//
//
//				}
//				$headers = FALSE;
//			}
//			fclose($handle);
//		}
	}

	function vovconf() {
//		//excel
//		$headers = true;
//		//voor gesprekshoeken en presentaties
//		if(file_exists($this->config->item('imagespath') . "upload/import/favorites/vovPresentaties.csv")) {
//			if (($handle = fopen($this->config->item('imagespath') . "upload/import/favorites/vovPresentaties.csv", "r")) !== FALSE) {
//				while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
//
//					$num = count($data);
//					$row++;
//					$newrow = array();
//					for ($c=0; $c < $num; $c++) {
//						if($headers) {
//							$heading[] = $data[$c];
//						} else {
//							$newrow[$heading[$c]] = $data[$c];
//						}
//					}
//
//					if(!$headers) {
//						$title = $newrow['title'];
//						$link = $newrow['vov_link'];
//
//						$title = str_replace("'","\'", $title);
//
//						//update sessions SET presentation = '$link' where name LIKE %'$title'%
//						$this->db->query("UPDATE session SET presentation = '$link' WHERE eventid = 517 AND name LIKE '%$title%'");
//					}
//
//					$headers = FALSE;
//				}
//			} else {
//				$error = 'Oops, excel file could not be loaded.';
//			}
//		}
	}

	function jumpingmechelen() {
//		//excel
//		$headers = true;
//		$prevname = '';
//		$prevdescription = '';
//		$id = 0;
//		//voor gesprekshoeken en presentaties
//		if(file_exists($this->config->item('imagespath') . "upload/import/sessions/jumpingmechelen.csv")) {
//			if (($handle = fopen($this->config->item('imagespath') . "upload/import/sessions/jumpingmechelen.csv", "r")) !== FALSE) {
//				while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
//
//					$num = count($data);
//					$row++;
//					$newrow = array();
//					for ($c=0; $c < $num; $c++) {
//						if($headers) {
//							$heading[] = $data[$c];
//						} else {
//							$newrow[$heading[$c]] = $data[$c];
//						}
//					}
//
//					if(!$headers) {
//						$name = $newrow['name'];
//						$description = $newrow['Description'];
//						$starttime = $newrow['Starttime'];
//						$endtime = $newrow['endtime'];
//						$location = $newrow['Location'];
//						$twitter = $newrow['link to startlist'];
//						$url = $newrow['link to result page'];
//
//						$name = str_replace("'","\'", $name);
//						$description = str_replace("'","\'", $description);
//
//						if($name == $prevname && trim($endtime) == '') {
//							//no new session, just add description
//							$prevdescription = $prevdescription . '\n'.$description;
//
//							//update record
//							$this->general_model->update('session', $id, array('description' => $prevdescription));
//							echo "UPDATE: description = " . $prevdescription . '\n';
//						} else {
//							//insert new record
//							if(strlen($endtime) == 5) {
//								$endtime = '20'.substr($starttime,6,2).'-'.substr($starttime,0,2).'-'.substr($starttime,3,2).' '.$endtime;
//							} elseif(strlen($endtime) == 4) {
//								$endtime = '20'.substr($starttime,6,2).'-'.substr($starttime,0,2).'-'.substr($starttime,3,2).' 0'.$endtime;
//							}
//
//							if(strlen($starttime) == 14) {
//								$starttime = '20'.substr($starttime,6,2).'-'.substr($starttime,0,2).'-'.substr($starttime,3,2).substr($starttime, 8, 6);
//							} elseif(strlen($starttime) == 13) {
//								$starttime = '20'.substr($starttime,6,2).'-'.substr($starttime,0,2).'-'.substr($starttime,3,2).' 0'.substr($starttime, 9,4);
//							}
//
//							$prevdescription = $description;
//							$prevname = $name;
//
//							$sessiongroupid = '';
//							if($newrow['Date'] == '26/12/2011') {
//								$sessiongroupid = 913;
//							} elseif($newrow['Date'] == '27/12/2011') {
//								$sessiongroupid = 914;
//							} elseif($newrow['Date'] == '28/12/2011') {
//								$sessiongroupid = 915;
//							} elseif($newrow['Date'] == '29/12/2011') {
//								$sessiongroupid = 917;
//							} elseif($newrow['Date'] == '30/12/2011') {
//								$sessiongroupid = 918;
//							}
//
//							$data = array(
//								'eventid'		=> 596,
//								'sessiongroupid'=> $sessiongroupid,
//								'name'			=> $name,
//								'description'	=> $description,
//								'starttime'		=> $starttime,
//								'endtime'		=> $endtime,
//								'location'		=> $location,
//								'twitter'		=> $twitter,
//								'url'			=> $url
//							);
//							$id = $this->general_model->insert('session', $data);
//							echo "INSERT name =" . $name . 'description = ' . $description . ' starttime: ' . $starttime . 'endtime: ' . $endtime . '<br/>';
//						}
//					}
//
//					$headers = FALSE;
//				}
//			} else {
//				$error = 'Oops, excel file could not be loaded.';
//			}
//		}
	}

	function importExhibitors() {
		// //excel
		// $headers = true;
		// $catcolumnnumber = 15;
		// $categories = array();
		// $row = 1;
		// $this->load->model('general_model');
		// $eventid = 914;
		// $prevname = '';
		// $appid = 488;
		// if(file_exists("../boisexhibitors.csv")) {
		// 	if (($handle = fopen("../boisexhibitors.csv", "r")) !== FALSE) {
		// 		while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
		// 			$num = count($data);
		// 			$row++;
		// 			$newrow = array();
		// 			for ($c=0; $c < $num; $c++) {
		// 				if($headers) {
		// 					if($c < $catcolumnnumber) {
		// 						//get all headings
		// 						$heading[] = $data[$c];
		// 					} else{
		// 						if($catcolumnnumber != $c) {
		// 							//insert category if category doesn't exist
		// 							$catExists = $this->db->query("SELECT * FROM exhibitorcategory WHERE eventid = $eventid AND name = '".mysql_escape_string($data[$c])."'");
		// 							if($catExists->num_rows() == 0) {
		// 								$categorie = array(
		// 										'eventid'=>$eventid,
		// 										'name'=>$data[$c]
		// 									);
		// 								$categorieid = $this->general_model->insert('exhibitorcategory', $categorie);
		// 							} else {
		// 								$categorieid = $catExists->row()->id;
		// 							}

		// 							$categories[$data[$c]] = $categorieid;
		// 						}
		// 						$heading[] = $data[$c];
		// 					}

		// 				} else {
		// 						$newrow[$heading[$c]] = $data[$c];
		// 				}
		// 			}

		// 			if(!$headers) {
		// 				//check if new exhibitor or translation of categories
		// 				if($newrow['Company Name'] == $prevname) {
		// 					//update category translation
		// 					foreach($categories as $catname => $catid) {
		// 						if(isset($newrow[$catname]) && $newrow[$catname] != '') {
		// 							//add exhibitor to category
		// 							// $lang = $newrow['Content Language'];
		// 							// $translationExists = $this->db->query("SELECT id FROM translation WHERE `table` = 'exhibitorcategory' AND 'tableid' = $catid AND `language` = '$lang'");
		// 							// if($translationExists->num_rows() == 0) {
		// 							// 	$translationdata = array(
		// 							// 			'`table`'=>'exhibitorcategory',
		// 							// 			'tableid'=>$catid,
		// 							// 			'language'=>$lang,
		// 							// 			'translation'=>$newrow[$catname]
		// 							// 		);
		// 							// 	$this->general_model->insert('translation', $translationdata);
		// 							// }
		// 						}
		// 					}
		// 				} else {
		// 					//insert exhibitor
		// 					//exhibitordata
		// 					// $bus = (isset($newrow['Bus 1']) && $newrow['Bus 1'] != '' ) ? ' B' . $newrow['Bus 1'] : '';
		// 					$address = $newrow['Street 1'] . ' ' . $newrow['Number 1'] . $bus . ' - ' . $newrow['Zip Code 1'] . ' ' . $newrow['City 1'] . ' ' . $newrow['Country 1'];
		// 					$tel = str_replace(' (0)','',$newrow['Phone 1']);
		// 					$data = array(
		// 						'eventid' => $eventid,
		// 						'name'=>$newrow['Company Name'],
		// 						'booth'=>$newrow['Stand Number'],
		// 						'address'=>$address,
		// 						// 'username'=>$newrow['Username'],
		// 						// 'password'=>$newrow['Password'],
		// 						'tel'=>$tel,
		// 						'email'=> $newrow['Email1 1'],
		// 						'web'=>$newrow['Website 1']
		// 						);

		// 					//insert exhibitor
		// 					$exhibitorid = $this->general_model->insert('exhibitor', $data);

		// 					//categorien koppelen
		// 					foreach($categories as $catname => $catid) {
		// 						if(isset($newrow[$catname]) && $newrow[$catname] != '') {
		// 							//add exhibitor to category
		// 							$exhicatdata = array(
		// 									'exhibitorid'=>$exhibitorid,
		// 									'exhibitorcategoryid'=>$catid
		// 								);
		// 							$this->general_model->insert('exhicat', $exhicatdata);
		// 						}
		// 					}

		// 					//metadata
		// 					$metadata = array(
		// 							// 'contentlanguage' => $newrow['Content Language'],
		// 							'exposantlanguage' => $newrow['Exposant Language'],
		// 							'fax' => $newrow['Fax 1'],
		// 							// 'cellphone' => $newrow['Cell Phone 1'],
		// 							// 'email' => $newrow['Email2 1'],
		// 							// 'contactperson' => $newrow['Contact Person 1'],
		// 							// 'openinghours' => $newrow['Opening Hours '],
		// 							// 'showlogo' => $newrow['Show logo'],
		// 							// 'clickableurl' => $newrow['Clickable url']
		// 						);

		// 					foreach($metadata as $key => $value) {
		// 						if($value != null) {
		// 						$data = array(
		// 							'`table`'=> 'exhibitor',
		// 							'`identifier`'=>$exhibitorid,
		// 							'key'=>$key,
		// 							'value'=>$value,
		// 							'appid'=>$appid
		// 							);
		// 						$this->general_model->insert('metadata',$data);
		// 						}
		// 					}
		// 				}

		// 				$prevname = $newrow['Company Name'];
		// 			}

		// 			$headers = FALSE;
		// 		}
		// 	} else {
		// 		$error = 'Oops, excel file could not be loaded.';
		// 	}
		// }
		// echo 'succes';
	}
	function importExhibitorsWithGroupsbois() {
		// //excel
		$headers = true;
		$catcolumnnumber = 12;
		$categories = array();
		$row = 1;
		$this->load->model('general_model');
		$eventid = 3747;
		$prevname = '';
		$appid = 1467;
		$maincategorie = 0;
		$mainparent = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'exhibitorcategories' LIMIT 1");
		if($mainparent->num_rows() != 0) {
			$mainparentid = $mainparent->row()->id;
		} else {
			$mainparentid = 0;
		}
		$mainparentbrand = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'exhibitorbrands' LIMIT 1");
		if($mainparentbrand->num_rows() != 0) {
			$mainparentbrandid = $mainparentbrand->row()->id;
		} else {
			$mainparentbrandid = 0;
		}
		if(file_exists("../horeca_energie.csv")) {
			if (($handle = fopen("../horeca_energie.csv", "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
					$num = count($data);
					$row++;
					$newrow = array();
					for ($c=0; $c < $num; $c++) {
						if($headers) {
							if($c < $catcolumnnumber) {
								//get all headings
								$heading[] = $data[$c];
							} else{
								if($catcolumnnumber != $c && $data[$c] != '') {
									//insert category if category doesn't exist
									$catExists = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = '".mysql_escape_string($data[$c])."' LIMIT 1");
									if($catExists->num_rows() == 0) {
										if($c > 42) {
											$categorie = array(
													'appid' => $appid,
													'eventid'=>$eventid,
													'name'=>$data[$c],
													'parentid'=>$mainparentbrandid
												);
											$categorieid = $this->general_model->insert('group', $categorie);
										} else {
											$categorie = array(
													'appid' => $appid,
													'eventid'=>$eventid,
													'name'=>$data[$c],
													'parentid'=>$mainparentid
												);
											$categorieid = $this->general_model->insert('group', $categorie);
										}
									} else {
										$categorieid = $catExists->row()->id;
									}

									$categories[$data[$c]] = $categorieid;
								}
								$heading[] = $data[$c];
							}

						} else {
								$newrow[$heading[$c]] = $data[$c];
						}
					}

					if(!$headers && $newrow['Company Name'] != '') {
						if($newrow['Company Name'] == $prevname) {
							//update category translation
							// foreach($categories as $catname => $catid) {
							// 	if(isset($newrow[$catname]) && $newrow[$catname] != '') {
							// 		//add exhibitor to category
							// 		$lang = $newrow['Content Language'];
							// 		$translationExists = $this->db->query("SELECT id FROM translation WHERE `table` = 'group' AND 'tableid' = $catid AND `language` = '$lang' LIMIT 1");
							// 		if($translationExists->num_rows() == 0) {
							// 			$translationdata = array(
							// 					'`table`'=>'group',
							// 					'tableid'=>$catid,
							// 					'language'=>$lang,
							// 					'translation'=>$newrow[$catname]
							// 				);
							// 			$this->general_model->insert('translation', $translationdata);
							// 		}
							// 	}
							// }

							//add description translation
							// $lang = $newrow['Content Language'];
							// $descriptionExists = $this->db->query("SELECT id FROM translation WHERE `table` = 'exhibitor' AND 'tableid' = $exhibitorid AND `language` = '$lang' LIMIT 1");
							// if($descriptionExists->num_rows() == 0) {
							// 	$translationdata = array(
							// 			'`table`'=>'exhibitor',
							// 			'tableid'=>$exhibitorid,
							// 			'fieldname' => 'description',
							// 			'language'=>$lang,
							// 			'translation'=>$newrow['description']
							// 		);
							// 	$this->general_model->insert('translation', $translationdata);
							// }
						} else {
							//insert exhibitor
							$address = $newrow['Street 1'] . ' - ' . $newrow['Zip Code 1'] . ' ' . $newrow['City 1'] . ' ' . $newrow['Country 1'];
							$tel = str_replace(' (0)','',$newrow['Phone 1']);
							$data = array(
								'eventid' => $eventid,
								'name'=>$newrow['Company Name'],
								'booth'=>$newrow['Stand Number'],
								'address'=>$address,
								'tel'=>$tel,
								'email'=> $newrow['Email1 1'],
								'web'=>$newrow['Website 1']
								);

							//insert exhibitor
							$exhibitorid = $this->general_model->insert('exhibitor', $data);
							// $exhibitordata = array(
							// 		'`table`'=>'exhibitor',
							// 		'tableid'=>$exhibitorid,
							// 		'fieldname' => 'description',
							// 		'language'=>$newrow['Content Language'],
							// 		'translation'=>$newrow['description']
							// 	);
							// $this->general_model->insert('translation', $exhibitordata);

							//categorien koppelen
							foreach($categories as $catname => $catid) {
								if(isset($newrow[$catname]) && $newrow[$catname] != '') {
									//add exhibitor to category
									$exhicatdata = array(
											'itemid'=>$exhibitorid,
											'itemtable'=>'exhibitor',
											'appid'=>$appid,
											'eventid'=>$eventid,
											'groupid'=>$catid
										);
									$this->general_model->insert('groupitem', $exhicatdata);
								}
							}

						}
						$prevname = $newrow['Company Name'];
					}

					$headers = FALSE;
				}
			} else {
				$error = 'Oops, excel file could not be loaded.';
			}
		}
		echo 'succes';
	}

	function importExhibitorsWithGroups() {
		//excel
		// $headers = true;
		// $categories = array();
		// $row = 1;
		// $this->load->model('general_model');
		// $eventid = 1220;
		// $prevname = '';
		// $appid = 683;
		// $maincategorie = 0;
		// $mainparent = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'exhibitorcategories' LIMIT 1");
		// if($mainparent->num_rows() != 0) {
		// 	$mainparentid = $mainparent->row()->id;
		// } else {
		// 	$mainparentid = 0;
		// }
		// if(file_exists("../exhibitorsCavalor.csv")) {
		// 	if (($handle = fopen("../exhibitorsCavalor.csv", "r")) !== FALSE) {
		// 		while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
		// 			$num = count($data);
		// 			$row++;
		// 			$newrow = array();
		// 			for ($c=0; $c < $num; $c++) {
		// 				if($headers) {
		// 						//get all headings
		// 						$heading[] = $data[$c];
		// 				} else {
		// 						$newrow[$heading[$c]] = $data[$c];
		// 				}
		// 			}

		// 			if(!$headers && $newrow['Name'] != '') {
		// 				//insert exhibitor
		// 				//exhibitordata
		// 				$bus = '';
		// 				// $address = $newrow['Corresp Addr. 1'] . ' - ' . $newrow['Zip'] . ' ' . $newrow['City'] . ' ' . $newrow['Country'];
		// 				// $tel = str_replace(' (0)','',$newrow['Gen Tel']);
		// 				$data = array(
		// 					'eventid' => $eventid,
		// 					'name'=>$newrow['Name'],
		// 					'description' => $newrow['Description']
		// 					// 'booth'=>$newrow['Stand'],
		// 					// 'address'=>$address,
		// 					// 'tel'=>$tel,
		// 					// 'email'=> $newrow['Gen E-mail'],
		// 					// 'web'=>$newrow['Website'],
		// 					);

		// 				//insert exhibitor
		// 				$exhibitorid = $this->general_model->insert('exhibitor', $data);

		// 				//categorien koppelen
		// 				//insert category if category doesn't exist
		// 				$parentid = $mainparentid;
		// 				$parentquery = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = '".mysql_escape_string($newrow['Category'])."' LIMIT 1");
		// 				if($parentquery->num_rows() != 0) {
		// 					$parentid = $parentquery->row()->id;
		// 				}
		// 				$catExists = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = '".mysql_escape_string($newrow['sub-category'])."' LIMIT 1");
		// 				if($catExists->num_rows() == 0) {
		// 					$categorie = array(
		// 							'appid' => $appid,
		// 							'eventid'=>$eventid,
		// 							'name'=>$newrow['sub-category'],
		// 							'parentid'=>$parentid
		// 						);
		// 					$categorieid = $this->general_model->insert('group', $categorie);
		// 				} else {
		// 					$categorieid = $catExists->row()->id;
		// 				}

		// 				if(isset($newrow['sub-category']) && $newrow['sub-category'] != '') {
		// 					//add exhibitor to category
		// 					$exhicatdata = array(
		// 							'itemid'=>$exhibitorid,
		// 							'itemtable'=>'exhibitor',
		// 							'appid'=>$appid,
		// 							'eventid'=>$eventid,
		// 							'groupid'=>$categorieid
		// 						);
		// 					$this->general_model->insert('groupitem', $exhicatdata);
		// 				}

		// 				//metadata
		// 				// $metadata = array(
		// 				// 		'fax' => $newrow['Gen Fax'],
		// 				// 		'contactperson' => $newrow['CP1 First name'] . ' ' . $newrow['CP1 Last name'],
		// 				// 	);

		// 				// foreach($metadata as $key => $value) {
		// 				// 	if($value != null) {
		// 				// 	$data = array(
		// 				// 		'`table`'=> 'exhibitor',
		// 				// 		'`identifier`'=>$exhibitorid,
		// 				// 		'key'=>$key,
		// 				// 		'value'=>$value,
		// 				// 		'appid'=>$appid
		// 				// 		);
		// 				// 	$this->general_model->insert('metadata',$data);
		// 				// 	}
		// 				// }
		// 			}

		// 			$headers = FALSE;
		// 		}
		// 	} else {
		// 		$error = 'Oops, excel file could not be loaded.';
		// 	}
		// }
		// echo 'succes';
	}

	function importVenues($appid) {
		// $this->load->model('general_model');
		// //excel
		// $headers = true;

		// if(file_exists("../venuez.csv")) {
		// 	if (($handle = fopen("../venuez.csv", "r")) !== FALSE) {
		// 		while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {

		// 			$num = count($data);
		// 			$row++;
		// 			$newrow = array();
		// 			for ($c=0; $c < $num; $c++) {
		// 				if($headers) {
		// 					$heading[] = $data[$c];
		// 				} else {
		// 					$newrow[$heading[$c]] = $data[$c];
		// 				}
		// 			}

		// 			if(!$headers) {
		// 				$name = $newrow['Name'];
		// 				if($name != '') {
		// 					$description = $newrow['Description'];
		// 					$tel = $newrow['Tel'];
		// 					$address = $newrow['Address'];
		// 					$websiteurl = $newrow['websiteurl'];
		// 					$email = $newrow['e-mail'];

		// 					$data = array(
		// 						'name' => $name,
		// 						'info' => $description,
		// 						'address'	=> $address,
		// 						'telephone' => $tel,
		// 						'email'		=> $email,
		// 						'website'	=> $websiteurl
		// 						);

		// 					$venueid = $this->general_model->insert('venue', $data);

		// 					$appvenuedata = array(
		// 							'appid' => $appid,
		// 							'venueid'	=> $venueid
		// 						);

		// 					$this->general_model->insert('appvenue', $appvenuedata);

		// 					$tags = $newrow['tags'];
		// 					$tagsarray = explode(',', $tags);
		// 					foreach($tagsarray as $tag) {
		// 						$tagdata = array(
		// 								'appid'	=> $appid,
		// 								'tag'	=> trim($tag),
		// 								'venueid'	=> $venueid
		// 							);

		// 						$this->general_model->insert('tag',$tagdata);
		// 					}
		// 				}
		// 			}

		// 			$headers = FALSE;
		// 		}
		// 	} else {
		// 		$error = 'Oops, excel file could not be loaded.';
		// 	}
		// }
	}

	function importVenuesCavalor($appid) {
		$this->load->model('general_model');
		//excel
		$headers = true;

		if(file_exists("../venuesCavalor.csv")) {
			if (($handle = fopen("../venuesCavalor.csv", "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
					$num = count($data);
					$row++;
					$newrow = array();
					for ($c=0; $c < $num; $c++) {
						if($headers) {
							$heading[] = $data[$c];
						} else {
							$newrow[$heading[$c]] = $data[$c];
						}
					}

					if(!$headers) {
						$name = $newrow['NAAM'];
						if($name != '') {
							$tel = $newrow['TELEFOON'];
							$address = $newrow['STRAAT'] . ' - ' . $newrow['POSTCODE'] . ' ' . $newrow['PLAATS'] . ' ' . $newrow['LAND'];
							$websiteurl = $newrow['WEBSITE'];
							$email = $newrow['EMAIL'];
							$fax = $newrow['FAX'];
							$description = '';

							$data = array(
								'name' => $name,
								'info' => $description,
								'address'	=> $address,
								'telephone' => $tel,
								'email'		=> $email,
								'website'	=> $websiteurl,
								'fax'		=> $fax
								);

							$venueid = $this->general_model->insert('venue', $data);

							$this->general_model->insert('module', array('moduletype' => 21, 'venue' => $venueid));
							$this->general_model->insert('launcher', array('venueid' => $venueid, 'moduletypeid' => 21, 'module' => 'info', 'title' => 'Info', 'active' => 1));

							$appvenuedata = array(
									'appid' => $appid,
									'venueid'	=> $venueid
								);

							$this->general_model->insert('appvenue', $appvenuedata);

							if(trim($newrow['feeds']) != '' && trim($newrow['supplements']) != "'") {
								$tagdata = array(
										'appid'	=> $appid,
										'tag'	=> 'feeds',
										'venueid'	=> $venueid
									);

								$this->general_model->insert('tag',$tagdata);
							}
							if(trim($newrow['supplements']) != '' && trim($newrow['supplements']) != "'") {
								$tagdata = array(
										'appid'	=> $appid,
										'tag'	=> 'supplements',
										'venueid'	=> $venueid
									);

								$this->general_model->insert('tag',$tagdata);
							}
							if(trim($newrow['both']) != '' && trim($newrow['supplements']) != "'") {
								$tagdata = array(
										'appid'	=> $appid,
										'tag'	=> 'both',
										'venueid'	=> $venueid
									);

								$this->general_model->insert('tag',$tagdata);
							}
							$tagdata = array(
									'appid'	=> $appid,
									'tag'	=> $newrow['LAND'],
									'venueid'	=> $venueid
								);

							$this->general_model->insert('tag',$tagdata);
						}
					}

					$headers = FALSE;
				}
			} else {
				$error = 'Oops, excel file could not be loaded.';
			}
		}
	}

	function importEvents($appid) {
		// $this->load->model('general_model');
		// //excel
		// $headers = true;
		// $organizerid = 594;
		// //voor gesprekshoeken en presentaties
		// if(file_exists("../Socializing2.csv")) {
		// 	if (($handle = fopen("../Socializing2.csv", "r")) !== FALSE) {
		// 		while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {

		// 			$num = count($data);
		// 			$row++;
		// 			$newrow = array();
		// 			for ($c=0; $c < $num; $c++) {
		// 				if($headers) {
		// 					$heading[] = $data[$c];
		// 				} else {
		// 					$newrow[$heading[$c]] = $data[$c];
		// 				}
		// 			}

		// 			if(!$headers) {
		// 				$name = $newrow['NAME'];
		// 				if($name != '') {
		// 					$description = $newrow['DESCRIPTION'];
		// 					$when = $newrow['WHEN'];
		// 					$where = $newrow['WHERE'];
		// 					$website = $newrow['WEBSITE'];
		// 					$email = $newrow['E-MAIL'];
		// 					$datefrom = '1970-01-01';
		// 					$dateto = '1970-01-01';

		// 					//IMPORTANT: organizerid && eventtype
		// 					$venuedata = array(
		// 							'name'	=> '',
		// 							'address'	=> $where
		// 						);
		// 					$venueid = $this->general_model->insert('venue', $venuedata);

		// 					$data = array(
		// 						'name' => $name,
		// 						'organizerid' => $organizerid,
		// 						'description' => $description,
		// 						'email'		=> $email,
		// 						'website'	=> $website,
		// 						'venueid'	=> $venueid,
		// 						'datefrom'	=> $datefrom,
		// 						'dateto'	=> $dateto
		// 						);

		// 					$eventid = $this->general_model->insert('event', $data);

		// 					$appeventdata = array(
		// 							'appid' => $appid,
		// 							'eventid'	=> $eventid
		// 						);

		// 					$this->general_model->insert('appevent', $appeventdata);

		// 					$tagdata = array(
		// 							'appid'	=> $appid,
		// 							'tag'	=> 'Socialising',
		// 							'eventid'	=> $eventid
		// 						);

		// 					$this->general_model->insert('tag',$tagdata);
		// 				}
		// 			}

		// 			$headers = FALSE;
		// 		}
		// 	} else {
		// 		$error = 'Oops, excel file could not be loaded.';
		// 	}
		// }
	}

	function importExhibitorsArtBrussels() {
		// $appid = 606;
		// $eventid = 1087;
		// $this->load->model('general_model');
		// //excel
		// $headers = true;
		// //voor gesprekshoeken en presentaties
		// if(file_exists("../exhibitorsGalleryNight.csv")) {
		// 	if (($handle = fopen("../exhibitorsGalleryNight.csv", "r")) !== FALSE) {
		// 		while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {

		// 			$num = count($data);
		// 			$row++;
		// 			$newrow = array();
		// 			for ($c=0; $c < $num; $c++) {
		// 				if($headers) {
		// 					$heading[] = $data[$c];
		// 				} else {
		// 					$newrow[$heading[$c]] = $data[$c];
		// 				}
		// 			}

		// 			if(!$headers) {
		// 				$name = $newrow['Artist'];
		// 				if($name != '') {
		// 					$booth = $newrow['Gallery'];
		// 					$address = $newrow['Street'] . ' ' . $newrow['N°'] .' - ' . $newrow['Postal code'] . ' ' . $newrow['City'];
		// 					$web = $newrow['website'];
		// 					$description = $newrow['Downtown/Uptown'];
		// 					$name = str_replace("'","\'", $name);

		// 					$data = array(
		// 						'name' => $name,
		// 						'booth' => $booth,
		// 						'address' => $address,
		// 						'web' => $web,
		// 						'description' => $description,
		// 						'eventid' => $eventid
		// 						);

		// 					$id = $this->general_model->insert('exhibitor', $data);
		// 				}
		// 			}

		// 			$headers = FALSE;
		// 		}
		// 	} else {
		// 		$error = 'Oops, excel file could not be loaded.';
		// 	}
		// }
	}

	function importVenuesArtBrussels() {
		// $appid = 606;
		// $eventid = 991;
		// $this->load->model('general_model');
		// //excel
		// $headers = true;
		// //voor gesprekshoeken en presentaties
		// if(file_exists("../artbrusselsvenuesmap.csv")) {
		// 	if (($handle = fopen("../artbrusselsvenuesmap.csv", "r")) !== FALSE) {
		// 		while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {

		// 			$num = count($data);
		// 			$row++;
		// 			$newrow = array();
		// 			for ($c=0; $c < $num; $c++) {
		// 				if($headers) {
		// 					$heading[] = $data[$c];
		// 				} else {
		// 					$newrow[$heading[$c]] = $data[$c];
		// 				}
		// 			}

		// 			if(!$headers) {
		// 				$name = $newrow['Name'];
		// 				if($name != '') {
		// 					$address = $newrow['Street'] . ' ' . $newrow['N°'] .' - ' . $newrow['Postal code'] . ' ' . $newrow['City'];
		// 					$web = $newrow['Website'];
		// 					$description = $newrow['Description'];
		// 					$name = str_replace("'","\'", $name);
		// 					$description = str_replace("'","\'", $description);
		// 					$telephone = $newrow['Phone'];

		// 					$rates = $newrow['Rates'];
		// 					$tag = $newrow['Category'];

		// 					if($rates != '') {
		// 						$description .= "\n Rates: " . $rates;
		// 					}

		// 					if($location != '') {
		// 						$description .= "\n Location: " . $location;
		// 					}

		// 					$data = array(
		// 						'name' => $name,
		// 						'address' => $address,
		// 						'website' => $web,
		// 						'info' => $description,
		// 						'eventid' => $eventid
		// 						);

		// 					$venueid = $this->general_model->insert('venue', $data);

		// 					$appvenuedata = array(
		// 						'appid' => $appid,
		// 						'venueid' => $venueid
		// 						);
		// 					$this->general_model->insert('appvenue', $appvenuedata);

		// 					if($tag != '') {
		// 					$tagdata = array(
		// 						'appid' => $appid,
		// 						'tag'	=> $tag,
		// 						'venueid' => $venueid,
		// 						'active'	=> 1
		// 						);

		// 					$this->general_model->insert('tag', $tagdata);
		// 					}
		// 				}
		// 			}

		// 			$headers = FALSE;
		// 		}
		// 	} else {
		// 		$error = 'Oops, excel file could not be loaded.';
		// 	}
		// }
	}

	function sessionImport() {
		// $appid = 657;
		// $eventid = 1118;
		// $this->load->model('general_model');
		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// if(file_exists("../groezrock5.csv")) {
		// 	if (($handle = fopen("../groezrock5.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}

		// 		if(!$headers) {
		// 			$name = str_replace("'", "\'", $newrow['Artist name']);
		// 			if($name != '') {
		// 				$sessiongroup = $newrow['Date'];
		// 				$sessiongroupExists = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid AND name = ? LIMIT 1", $sessiongroup);
		// 				if($sessiongroupExists->num_rows() != 0) {
		// 					$sessiongroupid = $sessiongroupExists->row()->id;
		// 				} else {
		// 					$sessiongroupid = $this->general_model->insert('sessiongroup', array('eventid' => $eventid, 'name' => $sessiongroup));
		// 				}

		// 				$starttime = $newrow['Date'] . ' ' . $newrow['Starttime'];
		// 				$endtime = $newrow['Date'] . ' ' . $newrow['endtime'];
		// 				if(!isset($newrow['Location'])) {
		// 					$newrow['Location'] = '';
		// 				}

		// 				if($sessionid = $this->general_model->insert('session', array(
		// 					'sessiongroupid' 	=> $sessiongroupid,
		// 					'eventid'			=> $eventid,
		// 					'name' 				=> $name,
		// 					'description' 		=> str_replace("'", "\'", $newrow['Description (UK)']),
		// 					'starttime' 		=> $starttime,
		// 					'endtime' 			=> $endtime,
		// 					'location' 			=> $newrow['Location']
		// 				))) {
		// 					if($newrow['Facebookurl'] != '') {
		// 					$metadata = $this->db->insert('metadata', array(
		// 						'table'			=> 'session',
		// 						'appid'			=> $appid,
		// 						'identifier'	=> $sessionid,
		// 						'key'			=> 'facebookurl',
		// 						'value'			=> $newrow['Facebookurl']
		// 					));
		// 					}

		// 					if($newrow['Twitter hashtag (artist)'] != '') {
		// 					$metadata = $this->db->insert('metadata', array(
		// 						'table'			=> 'session',
		// 						'appid'			=> $appid,
		// 						'identifier'	=> $sessionid,
		// 						'key'			=> 'twitter',
		// 						'value'			=> $newrow['Twitter hashtag (artist)']
		// 					));
		// 					}

		// 					if($newrow['Description (NL)'] != '') {
		// 					$translation = $this->db->insert('translation', array(
		// 						'table'		=> 'session',
		// 						'tableid'	=> $sessionid,
		// 						'fieldname'	=> 'description',
		// 						'language'	=> 'nl',
		// 						'translation' => str_replace("'", "\'", $newrow['Description (NL)'])
		// 					));
		// 					}
		// 				}
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// }
		// } else {
		// 	echo "no file";
		// }
	}


	function artbrusselsgallery() {
		//change this
		$appid = 2360;
		$eventid = 4806;

		$headers = TRUE;
		$heading = array();

		$app = $this->db->query("SELECT * FROM app WHERE id = $appid LIMIT 1")->row();

		$existingExhibitors = array();
		$existingExhibitors1 = $this->db->query("SELECT * FROM exhibitor WHERE eventid = $eventid")->result();
		foreach($existingExhibitors1 as $ex) {
			$existingExhibitors[$ex->external_id] = $ex;
		}

		$metadataExhibitingArtists = $this->db->query("SELECT * FROM tc_metadata WHERE appId = $appid AND name = 'ExhibitingArtistsValue' LIMIT 1");
		if($metadataExhibitingArtists->num_rows() == 0) {
			$metadataExhibitingArtists = false;
		} else {
			$metadataExhibitingArtists = $metadataExhibitingArtists->row();
			$this->db->query("DELETE FROM tc_metavalues WHERE metaId = $metadataExhibitingArtists->id");
		}
		

		$metadataGalleryArtists = $this->db->query("SELECT * FROM tc_metadata WHERE appId = $appid AND name = 'GalleryArtistsValue' LIMIT 1");
		if($metadataGalleryArtists->num_rows() == 0) {
			$metadataGalleryArtists = false;
		} else {
			$metadataGalleryArtists = $metadataGalleryArtists->row();
			$this->db->query("DELETE FROM tc_metavalues WHERE metaId = $metadataGalleryArtists->id");
		}

		$metadataGalleryArtistsHeader = $this->db->query("SELECT * FROM tc_metadata WHERE appId = $appid AND name = 'Gallery Artists' LIMIT 1");
		if($metadataGalleryArtistsHeader->num_rows() == 0) {
			$metadataGalleryArtistsHeader = false;
		} else {
			$metadataGalleryArtistsHeader = $metadataGalleryArtistsHeader->row();
			$this->db->query("DELETE FROM tc_metavalues WHERE metaId = $metadataGalleryArtistsHeader->id");
		}

		$metadataExhibitingArtistsHeader = $this->db->query("SELECT * FROM tc_metadata WHERE appId = $appid AND name = 'Exhibiting Artists' LIMIT 1");
		if($metadataExhibitingArtistsHeader->num_rows() == 0) {
			$metadataExhibitingArtistsHeader = false;
		} else {
			$metadataExhibitingArtistsHeader = $metadataExhibitingArtistsHeader->row();
			$this->db->query("DELETE FROM tc_metavalues WHERE metaId = $metadataExhibitingArtistsHeader->id");
		}
		

		$imagesi = 1;
		while($imagesi <= 20) {
			${'metadataImage'.$imagesi} = $this->db->query("SELECT * FROM tc_metadata WHERE appId = $appid AND name = 'image".$imagesi."' LIMIT 1");
			if(${'metadataImage'.$imagesi}->num_rows() == 0) {
				${'metadataImage'.$imagesi} = false;
			} else {
				${'metadataImage'.$imagesi} = ${'metadataImage'.$imagesi}->row();
				$this->db->query("DELETE FROM tc_metavalues WHERE metaId = ".${'metadataImage'.$imagesi}->id);
			}
			

			${'metadataImageDescription'.$imagesi} = $this->db->query("SELECT * FROM tc_metadata WHERE appId = $appid AND name = 'imagedescription".$imagesi."' LIMIT 1");
			if(${'metadataImageDescription'.$imagesi}->num_rows() == 0) {
				${'metadataImageDescription'.$imagesi} = false;
			} else {
				${'metadataImageDescription'.$imagesi} = ${'metadataImageDescription'.$imagesi}->row();
				$this->db->query("DELETE FROM tc_metavalues WHERE metaId = ".${'metadataImageDescription'.$imagesi}->id);
			}
			

			$imagesi++;
		}

		// $row = 1;
		if(file_exists("/var/www/UPLOAD/Live/upload/artbrussels/DownloadCSV122.csv")) {
			if (($handle = fopen("/var/www/UPLOAD/Live/upload/artbrussels/DownloadCSV122.csv", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
				$num = count($data);
				$row++;
				$newrow = array();
				for ($c=0; $c < $num; $c++) {
					if($headers) {
						$heading[] = $data[$c];
					} else {
						$newrow[$heading[$c]] = $data[$c];
					}
				}

				if(!$headers) {
					//insert gallery
					$galleryexternalid = $newrow['id'];
					$galleryname = str_replace("'", "\'", $newrow['GalleryName']);
					$gallerybooth = $newrow['Booth'];
					$galleryaddress = $newrow['Street1'] . ', ' . $newrow['Postalcode1'] . ' ' . $newrow['City1'] . ' - ' .$newrow['Country1'];
					$galleryemail = $newrow['Email1'];
					$galleryweb = $newrow['Website1'];
					if(!empty($newrow['Image'])) {
						$galleryimageurl = 'upload/artbrussels/artbrussels2013/' . $newrow['Image'];
					} else {
						$galleryimageurl = '';
					}
					
					$gallerytel = $newrow['Tel1'];
					$galleryfax = $newrow['Fax1'];
					$sector = $newrow['Sector'];
					if(!empty($sector)) {
						$gallerybooth .= ' ('.$sector.')';
					}

					$description = '';
					$soloshow = $newrow['SoloArtist'];
					if(!empty($soloshow)) {
						$description .= 'Solo show: ' . $soloshow;
					}

					$gallerydata = array(
							'external_id' => $galleryexternalid,
							'eventid'	=> $eventid,
							'name'	=> $galleryname,
							'description' => $description,
							'booth'	=> $gallerybooth,
							'address'	=> $galleryaddress,
							'email'	=> $galleryemail,
							'web'	=> $galleryweb,
							'tel'	=> $gallerytel,
							'imageurl'	=> $galleryimageurl
						);

					if(isset($existingExhibitors[$galleryexternalid])) {
						$this->general_model->update('exhibitor', $existingExhibitors[$galleryexternalid]->id, $gallerydata);
						$galleryid = $existingExhibitors[$galleryexternalid]->id;
					} else {
						$galleryid = $this->general_model->insert('exhibitor', $gallerydata);
					}

					$exhibitingArtists = $newrow['ExhibitingArtists'];
					$exhibitingArtists = str_replace('$hardReturn$', ',', $exhibitingArtists);
					if($metadataExhibitingArtists) {
						$this->general_model->insert('tc_metavalues', array(
								'metaId' => $metadataExhibitingArtists->id,
								'parentType' => 'exhibitor',
								'parentId' => $galleryid,
								'name' => $metadataExhibitingArtists->name,
								'type' => $metadataExhibitingArtists->type,
								'value' => $exhibitingArtists,
								'language' => $app->defaultlanguage
							));
					}	

					$galleryArtists = $newrow['GalleryArtists'];
					$galleryArtists = str_replace('$hardReturn$', ',', $galleryArtists);
					if($metadataGalleryArtists) {
						$this->general_model->insert('tc_metavalues', array(
								'metaId' => $metadataGalleryArtists->id,
								'parentType' => 'exhibitor',
								'parentId' => $galleryid,
								'name' => $metadataGalleryArtists->name,
								'type' => $metadataGalleryArtists->type,
								'value' => $galleryArtists,
								'language' => $app->defaultlanguage
							));
					}	

					if($metadataGalleryArtistsHeader) {
						$this->general_model->insert('tc_metavalues', array(
								'metaId' => $metadataGalleryArtistsHeader->id,
								'parentType' => 'exhibitor',
								'parentId' => $galleryid,
								'name' => $metadataGalleryArtistsHeader->name,
								'type' => $metadataGalleryArtistsHeader->type,
								'value' => 'Gallery Artists',
								'language' => $app->defaultlanguage
							));
					}	
					if($metadataExhibitingArtistsHeader) {
						$this->general_model->insert('tc_metavalues', array(
								'metaId' => $metadataExhibitingArtistsHeader->id,
								'parentType' => 'exhibitor',
								'parentId' => $galleryid,
								'name' => $metadataExhibitingArtistsHeader->name,
								'type' => $metadataExhibitingArtistsHeader->type,
								'value' => 'Exhibiting Artists',
								'language' => $app->defaultlanguage
							));
					}					

					//images
					for($i = 1; $i <= 20; $i++) {
						if(${'metadataImageDescription'.$i} && ${'metadataImage'.$i}) {
							$image = $newrow['ImagePress'.$i];
							if($image != '') {
								$imagedescription = $newrow['PressArtist'.$i] . "\n" . $newrow['PressTitle'.$i] . "\n" . $newrow['PressTechnic'.$i] . "\n" . $newrow['PressYear'.$i] . "\n" . $newrow['PressMeasures'.$i] . "\n" . $newrow['PressCopyright'.$i];

								$this->general_model->insert('tc_metavalues', array(
										'metaId' => ${'metadataImage'.$i}->id,
										'parentType' => 'exhibitor',
										'parentId' => $galleryid,
										'name' => ${'metadataImage'.$i}->name,
										'type' => ${'metadataImage'.$i}->type,
										'value' => 'upload/artbrussels/artbrussels2013/'.$image,
										'language' => $app->defaultlanguage
									));
								$this->general_model->insert('tc_metavalues', array(
										'metaId' => ${'metadataImageDescription'.$i}->id,
										'parentType' => 'exhibitor',
										'parentId' => $galleryid,
										'name' => ${'metadataImageDescription'.$i}->name,
										'type' => ${'metadataImageDescription'.$i}->type,
										'value' => $imagedescription,
										'language' => $app->defaultlanguage
									));
							}
						}
					}
				}
				$headers = FALSE;
			}
			fclose($handle);
			}
		} else {
			echo "no file";
		}
	}

	function artbrusselsremove() {
		$res = $this->db->query("SELECT v.metaId FROM tc_metavalues v INNER JOIN tc_metadata m ON m.id = v.metaId WHERE m.appId = 2360 AND m.name <> 'Kids Corner title' AND m.name <> 'Kids Corner' AND m.name <> 'Marker'"); 

		$ids = array();
		foreach($res->result() as $r) {
			$ids[] = $r->metaId;
		}

		$ids = array_unique($ids);
		$ids = implode(',', $ids);
		$this->db->query("DELETE FROM tc_metavalues WHERE metaId IN ($ids)");
	}

	function exhibitorsImec() {
		// $eventid = 1250;
		// $this->load->model('general_model');

		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// $error = '';
		// if(file_exists("../exhibitorsimec.csv")) {
		// 	if (($handle = fopen("../exhibitorsimec.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}
		// 		if(!$headers) {
		// 			$name = $newrow['Speaker Name'];
		// 			$description = $newrow['Function'] . "\n" . str_replace("'", "\'", $newrow['Bio']);
		// 			if($name != '') {
		// 					$exid = $this->general_model->insert('exhibitor', array(
		// 						'eventid'			=> $eventid,
		// 						'name' 				=> trim($name),
		// 						'description' 		=> $description
		// 					));
		// 					var_dump($exid);
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// 	}
		// }
	}

	function sessionsImec() {
		// $eventid = 1250;
		// $this->load->model('general_model');

		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// $error = '';
		// $currorder = 10;
		// if(file_exists("../sessionsimec.csv")) {
		// 	if (($handle = fopen("../sessionsimec.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}
		// 		if(!$headers) {
		// 			$name = $newrow['name'];
		// 			$description = $newrow['description'];
		// 			if($name != '') {
		// 				$sessiongroup = $newrow['date'];
		// 				if($sessiongroup != '') {
		// 					$sessiongroupExists = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid AND name = ? LIMIT 1", $sessiongroup);
		// 					if($sessiongroupExists->num_rows() != 0) {
		// 						$sessiongroupid = $sessiongroupExists->row()->id;
		// 					} else {
		// 						$sessiongroupid = $this->general_model->insert('sessiongroup', array('eventid' => $eventid, 'name' => $sessiongroup));
		// 					}
		// 					$date = $newrow['date'];

		// 					$starttime = $date . ' ' . $newrow['starttime'];
		// 					$endtime = $date . ' ' . $newrow['endtime'];

		// 					$speakername = $newrow['Speaker'];
		// 					$speakerquery = $this->db->query("SELECT * FROM exhibitor WHERE eventid = $eventid AND name = '$speakername' LIMIT 1");
		// 					if($speakerquery->num_rows() != 0) {
		// 						$speakerid = $speakerquery->row()->id;
		// 					} else {
		// 						$speakerid = '';
		// 					}

		// 					$sessionid = $this->general_model->insert('session', array(
		// 						'sessiongroupid' 	=> $sessiongroupid,
		// 						'eventid'			=> $eventid,
		// 						'name' 				=> $name,
		// 						'description' 		=> $description,
		// 						'starttime' 		=> $starttime,
		// 						'endtime' 			=> $endtime,
		// 						'location' 			=> $newrow['location'],
		// 						'speaker'			=> $speakerid,
		// 						'order'				=> $currorder
		// 					));
		// 					$currorder = $currorder + 10;
		// 				}
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// 	}
		// }
	}

	function attendeesIMDC() {
		// $eventid = 1140;
		// $this->load->model('general_model');

		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// $error = '';
		// if(file_exists("../attendeesimdc2.csv")) {
		// 	if (($handle = fopen("../attendeesimdc2.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}
		// 		if(!$headers) {
		// 			$name = $newrow['Name'];
		// 			if($name != '') {
		// 					$attid = $this->general_model->insert('attendees', array(
		// 						'eventid'			=> $eventid,
		// 						'name' 				=> $name,
		// 						'firstname'			=> $newrow['Firstname'],
		// 						'company'			=> $newrow['Company'],
		// 						'function'			=> $newrow['Function'],
		// 						'email'				=> $newrow['E-mail'],
		// 						'linkedin'			=> $newrow['Public LinkedIn'],
		// 						'phonenr'			=> $newrow['Phone']
		// 					));
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// 	}
		// }
	}
	function sessionImportWithSpeakers() {
		// $appid = 115;
		// $eventid = 379;
		// $this->load->model('general_model');
		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// if(file_exists("../sessionsETT.csv")) {
		// 	if (($handle = fopen("../sessionsETT.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}

		// 		if(!$headers) {
		// 			$name = $newrow['name'];
		// 			if($name != '') {
		// 				$sessiongroup = $newrow['date'];
		// 				$sessiongroupExists = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid AND name = ? LIMIT 1", $sessiongroup);
		// 				if($sessiongroupExists->num_rows() != 0) {
		// 					$sessiongroupid = $sessiongroupExists->row()->id;
		// 				} else {
		// 					$sessiongroupid = $this->general_model->insert('sessiongroup', array('eventid' => $eventid, 'name' => $sessiongroup));
		// 				}

		// 				$starttime = $newrow['date'] . ' ' . $newrow['starttime'];
		// 				$endtime = $newrow['date'] . ' ' . $newrow['endtime'];
		// 				if(!isset($newrow['location'])) {
		// 					$newrow['location'] = '';
		// 				}

		// 				if($sessionid = $this->general_model->insert('session', array(
		// 					'sessiongroupid' 	=> $sessiongroupid,
		// 					'eventid'			=> $eventid,
		// 					'name' 				=> $name,
		// 					'description' 		=> $newrow['description'],
		// 					'starttime' 		=> $starttime,
		// 					'endtime' 			=> $endtime,
		// 					'location' 			=> $newrow['location']
		// 				))) {
		// 					//speakers
		// 					if(trim($newrow['Speaker']) != 'TBC' && $newrow['Speaker'] != '') {
		// 						$speakersarray = explode(',', $newrow['Speaker']);
		// 						foreach($speakersarray as $speaker) {
		// 							$this->general_model->insert('speaker', array(
		// 									'eventid' => $eventid,
		// 									'sessionid'	=> $sessionid,
		// 									'name'		=> trim($speaker)
		// 								));
		// 						}
		// 					}
		// 				}
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// }
		// } else {
		// 	echo "no file";
		// }
	}

	function sponsors() {
	// 	$eventid = 1318;
	// 	$appid = 850;

	// 	$this->load->model('general_model');

	// 	$headers = TRUE;
	// 	$heading = array();

	// 	$row = 1;
	// 	$error = '';
	// 	if(file_exists("../sponsorsIB.csv")) {
	// 		if (($handle = fopen("../sponsorsIB.csv", "r")) !== FALSE) {
	// 		while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
	// 			$num = count($data);
	// 			$row++;
	// 			$newrow = array();
	// 			for ($c=0; $c < $num; $c++) {
	// 				if($headers) {
	// 					$heading[] = $data[$c];
	// 				} else {
	// 					$newrow[$heading[$c]] = $data[$c];
	// 				}
	// 			}
	// 			if(!$headers) {
	// 				$sponsorgroup = $newrow['type'];
	// 				$sponsorgroupExists = $this->db->query("SELECT * FROM sponsorgroup WHERE eventid = $eventid AND name = ? LIMIT 1", $sponsorgroup);
	// 				if($sponsorgroupExists->num_rows() != 0) {
	// 					$sponsorgroupid = $sponsorgroupExists->row()->id;
	// 				} else {
	// 					$sponsorgroupid = $this->general_model->insert('sponsorgroup', array('eventid' => $eventid, 'name' => $sponsorgroup));
	// 				}

	// 				$name = $newrow['name'];
	// 				if($name != '') {
	// 						$id = $this->general_model->insert('sponsor', array(
	// 							'eventid'			=> $eventid,
	// 							'sponsorgroupid'	=> $sponsorgroupid,
	// 							'name' 				=> $name,
	// 							'description'		=> $newrow['Description'],
	// 							'website'			=> $newrow['Website'],
	// 						));
	// 				}
	// 			}
	// 			$headers = FALSE;
	// 		}
	// 		fclose($handle);
	// 		}
	// 	}
	}

	function sessionWithSpeakersNew() {
		// $appid = 850;
		// $eventid = 1318;
		// $this->load->model('general_model');
		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// $sessionid = 0;
		// if(file_exists("../sessionsIB.csv")) {
		// 	if (($handle = fopen("../sessionsIB.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}

		// 		if(!$headers) {
		// 			$name = $newrow['name'];
		// 			if($name != '') {
		// 				$sessiongroup = $newrow['date'];
		// 				$sessiongroupExists = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid AND name = ? LIMIT 1", $sessiongroup);
		// 				if($sessiongroupExists->num_rows() != 0) {
		// 					$sessiongroupid = $sessiongroupExists->row()->id;
		// 				} else {
		// 					$sessiongroupid = $this->general_model->insert('sessiongroup', array('eventid' => $eventid, 'name' => $sessiongroup));
		// 				}

		// 				$starttime = $newrow['date'] . ' ' . str_replace('h',':',$newrow['starttime']);
		// 				$endtime = $newrow['date'] . ' ' . str_replace('h',':',$newrow['endtime']);
		// 				if(!isset($newrow['location'])) {
		// 					$newrow['location'] = '';
		// 				}

		// 				if($sessionid = $this->general_model->insert('session', array(
		// 					'sessiongroupid' 	=> $sessiongroupid,
		// 					'eventid'			=> $eventid,
		// 					'name' 				=> $name,
		// 					'description' 		=> $newrow['description'],
		// 					'starttime' 		=> $starttime,
		// 					'endtime' 			=> $endtime,
		// 					'location' 			=> $newrow['location']
		// 				))) {
		// 					//speakers
		// 					if(trim($newrow['Speaker']) != 'TBC' && $newrow['Speaker'] != '') {
		// 						$description = "";
		// 						if(isset($newrow['biography (Speaker)'])) {
		// 							$description = $newrow['biography (Speaker)'];
		// 						}
		// 						$speaker = $newrow['Speaker'];
		// 						$speakerid = $this->general_model->insert('speaker', array(
		// 								'eventid' => $eventid,
		// 								'sessionid'	=> $sessionid,
		// 								'name'		=> trim($speaker),
		// 								'description'	=> $description
		// 							));

		// 						$this->general_model->insert('speaker_session', array(
		// 								'eventid'	=> $eventid,
		// 								'speakerid'	=> $speakerid,
		// 								'sessionid' => $sessionid
		// 							));
		// 					}
		// 				}
		// 			} else {
		// 				//speakers
		// 				if(trim($newrow['Speaker']) != 'TBC' && $newrow['Speaker'] != '') {
		// 					$description = "";
		// 					if(isset($newrow['biography (Speaker)'])) {
		// 						$description = $newrow['biography (Speaker)'];
		// 					}
		// 					$speaker = $newrow['Speaker'];
		// 					$speakerid = $this->general_model->insert('speaker', array(
		// 							'eventid' => $eventid,
		// 							'sessionid'	=> $sessionid,
		// 							'name'		=> trim($speaker),
		// 							'description'	=> $description
		// 						));

		// 					$this->general_model->insert('speaker_session', array(
		// 							'eventid'	=> $eventid,
		// 							'speakerid'	=> $speakerid,
		// 							'sessionid' => $sessionid
		// 						));
		// 				}
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// }
		// } else {
		// 	echo "no file";
		// }
	}

	function importVenuesExpats($appid) {
		// $this->load->model('general_model');
		// //excel
		// $headers = true;

		// if(file_exists("../SOS.csv")) {
		// 	if (($handle = fopen("../SOS.csv", "r")) !== FALSE) {
		// 		while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {

		// 			$num = count($data);
		// 			$row++;
		// 			$newrow = array();
		// 			for ($c=0; $c < $num; $c++) {
		// 				if($headers) {
		// 					$heading[] = $data[$c];
		// 				} else {
		// 					$newrow[$heading[$c]] = $data[$c];
		// 				}
		// 			}

		// 			if(!$headers) {
		// 				$name = $newrow['Name'];
		// 				if($name != '') {
		// 					$description = $newrow['Description'];
		// 					$tel = $newrow['Tel'];
		// 					$address = $newrow['Address'];
		// 					$websiteurl = $newrow['websiteurl'];
		// 					$email = $newrow['e-mail'];

		// 					$data = array(
		// 						'name' => $name,
		// 						'info' => $description,
		// 						'address'	=> $address,
		// 						'telephone' => $tel,
		// 						'email'		=> $email,
		// 						'website'	=> $websiteurl
		// 						);

		// 					$venueid = $this->general_model->insert('venue', $data);

		// 					$appvenuedata = array(
		// 							'appid' => $appid,
		// 							'venueid'	=> $venueid
		// 						);

		// 					$this->general_model->insert('appvenue', $appvenuedata);

		// 					$tagdata = array(
		// 							'appid'	=> $appid,
		// 							'tag'	=> 'SOS',
		// 							'venueid'	=> $venueid
		// 						);

		// 					$this->general_model->insert('tag',$tagdata);
		// 				}
		// 			}

		// 			$headers = FALSE;
		// 		}
		// 	} else {
		// 		$error = 'Oops, excel file could not be loaded.';
		// 	}
		// }
	}

	function attendeesIMEC() {
		// $eventid = 1250;
		// $this->load->model('general_model');

		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// $error = '';
		// if(file_exists("../attendeesimec.csv")) {
		// 	if (($handle = fopen("../attendeesimec.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}
		// 		if(!$headers) {
		// 			$name = $newrow['Lastname'];
		// 			if($name != '') {
		// 					$attid = $this->general_model->insert('attendees', array(
		// 						'eventid'			=> $eventid,
		// 						'name' 				=> $name,
		// 						'firstname'			=> $newrow['Firstname'],
		// 						'company'			=> $newrow['Company']
		// 					));
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// 	}
		// }
	}

	function attendeesIB() {
		// $eventid = 1318;
		// $this->load->model('general_model');

		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// $error = '';
		// if(file_exists("../attendeesib.csv")) {
		// 	if (($handle = fopen("../attendeesib.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}
		// 		if(!$headers) {
		// 			$name = $newrow['Name'];
		// 			if($name != '') {
		// 					$attid = $this->general_model->insert('attendees', array(
		// 						'eventid'			=> $eventid,
		// 						'name' 				=> $name,
		// 						'firstname'			=> $newrow['Firstname'],
		// 						'function'			=> $newrow['function'],
		// 						'email'				=> $newrow['email'],
		// 						'linkedin'			=> $newrow['Public linkedInUrl'],
		// 						'company'			=> $newrow['Company'],
		// 						'country'			=> $newrow['Country']
		// 					));
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// 	}
		// }
	}

	function linkfbtosession($eventid) {
		// $this->load->model('general_model');
		// $app = $this->app_model->getRealAppOfEvent($eventid);

		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// $error = '';
		// if(file_exists("../sessionsfb.csv")) {
		// 	if (($handle = fopen("../sessionsfb.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}
		// 		if(!$headers) {
		// 			$name = $newrow['Artist name'];
		// 			if($name != '') {
		// 				$sessionid = $this->db->query("SELECT id FROM session WHERE name = '".$name."' AND eventid = $eventid LIMIT 1")->row()->id;
		// 				$fburl = $newrow['Facebookurl'];
		// 				if($fburl != '' && $sessionid != null) {
		// 					$this->general_model->update('session', $sessionid, array(
		// 							'url'		=> $fburl
		// 						));
		// 				}
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// 	}
		// }
	}

	function houza() {
		// $appid = 1355;
		// $eventid = 3424;
		// $this->load->model('general_model');

		// $headers = TRUE;
		// $heading = array();

		// $row = 1;
		// $error = '';
		// $currorder = 10;
		// if(file_exists("../houza.csv")) {
		// 	if (($handle = fopen("../houza.csv", "r")) !== FALSE) {
		// 	while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
		// 		$num = count($data);
		// 		$row++;
		// 		$newrow = array();
		// 		for ($c=0; $c < $num; $c++) {
		// 			if($headers) {
		// 				$heading[] = $data[$c];
		// 			} else {
		// 				$newrow[$heading[$c]] = $data[$c];
		// 			}
		// 		}
		// 		if(!$headers) {
		// 			$name = $newrow['session name '];
		// 			$description = $newrow['description '];

		// 			if($name != '') {
		// 				$sessiongroup = $newrow['sessiongroup name'];
		// 				if($sessiongroup != '') {
		// 					$sessiongroupExists = $this->db->query("SELECT * FROM sessiongroup WHERE eventid = $eventid AND name = ? LIMIT 1", $sessiongroup);
		// 					if($sessiongroupExists->num_rows() != 0) {
		// 						$sessiongroupid = $sessiongroupExists->row()->id;
		// 					} else {
		// 						$sessiongroupid = $this->general_model->insert('sessiongroup', array('eventid' => $eventid, 'name' => $sessiongroup));
		// 					}
		// 					$date = $newrow['sessiongroup name'];

		// 					$starttime = $sessiongroup . ' ' . substr($newrow['starttime '], strlen($newrow['starttime']) - 5, 5) . ':00';
		// 					$endtime = $sessiongroup . ' ' . substr($newrow['endtime '], strlen($newrow['endtime']) - 5, 5) . ':00';
		// 					echo $starttime;
		// 					$sessionid = $this->general_model->insert('session', array(
		// 						'sessiongroupid' 	=> $sessiongroupid,
		// 						'eventid'			=> $eventid,
		// 						'name' 				=> $name,
		// 						'description' 		=> $description,
		// 						'starttime' 		=> $starttime,
		// 						'endtime' 			=> $endtime,
		// 						'location' 			=> $newrow['location '],
		// 						'imageurl'			=> 'upload/houzapalooza/'.$newrow['imageurl'],
		// 						'order'				=> $currorder
		// 					));
		// 					$currorder = $currorder + 10;

		// 					//metadata
		// 					$metadata = array(
		// 							'facebook' => $newrow['facebook'],
		// 							'twitter' => $newrow['twitter'],
		// 							'youtube' => $newrow['youtube']
		// 						);

		// 					foreach($metadata as $key => $value) {
		// 						if($value != null) {
		// 						$data = array(
		// 							'`table`'=> 'session',
		// 							'`identifier`'=>$sessionid,
		// 							'key'=>$key,
		// 							'value'=>$value,
		// 							'appid'=>$appid
		// 							);
		// 						$this->general_model->insert('metadata',$data);
		// 						}
		// 					}
		// 					echo 'added';
		// 				}
		// 			}
		// 		}
		// 		$headers = FALSE;
		// 	}
		// 	fclose($handle);
		// 	}
		// 	echo 'succes';
		// }
	}


	function importExhibitorsWithGroupsbbs() {
		// //excel
		// $headers = true;
		// $catcolumnnumber = 9;
		// $categories = array();
		// $row = 1;
		// $this->load->model('general_model');
		// $eventid = 817;
		// $prevname = '';
		// $appid = 405;
		// $maincategorie = 0;
		// $mainparent = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = 'exhibitorcategories' LIMIT 1");
		// if($mainparent->num_rows() != 0) {
		// 	$mainparentid = $mainparent->row()->id;
		// } else {
		// 	$mainparentid = 0;
		// }
		// if(file_exists("../bbs.csv")) {
		// 	if (($handle = fopen("../bbs.csv", "r")) !== FALSE) {
		// 		while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
		// 			$num = count($data);
		// 			$row++;
		// 			$newrow = array();
		// 			for ($c=0; $c < $num; $c++) {
		// 				if($headers) {
		// 					if($c < $catcolumnnumber) {
		// 						//get all headings
		// 						$heading[] = $data[$c];
		// 					} else{
		// 						if($catcolumnnumber != $c && $data[$c] != '') {
		// 							//insert category if category doesn't exist
		// 							$catExists = $this->db->query("SELECT * FROM `group` WHERE eventid = $eventid AND name = '".mysql_escape_string($data[$c])."' LIMIT 1");
		// 							if($catExists->num_rows() == 0) {
		// 								$categorie = array(
		// 										'appid' => $appid,
		// 										'eventid'=>$eventid,
		// 										'name'=>$data[$c],
		// 										'parentid'=>$mainparentid
		// 									);
		// 								$categorieid = $this->general_model->insert('group', $categorie);
		// 							} else {
		// 								$categorieid = $catExists->row()->id;
		// 							}

		// 							$categories[$data[$c]] = $categorieid;
		// 						}
		// 						$heading[] = $data[$c];
		// 					}

		// 				} else {
		// 						$newrow[$heading[$c]] = $data[$c];
		// 				}
		// 			}

		// 			if(!$headers && $newrow['name_nl'] != '') {
		// 				if($newrow['name_nl'] == $prevname) {

		// 				} else {
		// 					//insert exhibitor
		// 					$address = $newrow['address'];
		// 					$data = array(
		// 						'eventid' => $eventid,
		// 						'name'=>$newrow['name_nl'],
		// 						'booth'=>$newrow['booth'],
		// 						'address'=>$address,
		// 						'email'=> $newrow['email'],
		// 						'web'=>$newrow['web'],
		// 						'tel'=>$newrow['tel']
		// 						);

		// 					//insert exhibitor
		// 					$exhibitorid = $this->general_model->insert('exhibitor', $data);
		// 					$exhibitordata = array(
		// 							'`table`'=>'exhibitor',
		// 							'tableid'=>$exhibitorid,
		// 							'fieldname' => 'name',
		// 							'language'=>'fr',
		// 							'translation'=>$newrow['name_fr']
		// 						);
		// 					$this->general_model->insert('translation', $exhibitordata);

		// 					//categorien koppelen
		// 					foreach($categories as $catname => $catid) {
		// 						if(isset($newrow[$catname]) && $newrow[$catname] != '') {
		// 							//add exhibitor to category
		// 							$exhicatdata = array(
		// 									'itemid'=>$exhibitorid,
		// 									'itemtable'=>'exhibitor',
		// 									'appid'=>$appid,
		// 									'eventid'=>$eventid,
		// 									'groupid'=>$catid
		// 								);
		// 							$this->general_model->insert('groupitem', $exhicatdata);
		// 						}
		// 					}

		// 				}
		// 				$prevname = $newrow['name_nl'];
		// 			}

		// 			$headers = FALSE;
		// 		}
		// 	} else {
		// 		$error = 'Oops, excel file could not be loaded.';
		// 	}
		// }
		// echo 'succes';
	}

	function places() {
		$eventid = 5824;
		$appid = 3421;
		$app = $this->db->query("SELECT * FROM app WHERE id = $appid LIMIT 1")->row();

		$headers = TRUE;
		$heading = array();

		$row = 1;
		$error = '';
		if(file_exists("../MustEat.csv")) {
			if (($handle = fopen("../MustEat.csv", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
				$num = count($data);
				$row++;
				$newrow = array();
				for ($c=0; $c < $num; $c++) {
					if($headers) {
						$heading[] = $data[$c];
					} else {
						$newrow[$heading[$c]] = $data[$c];
					}
				}
				if(!$headers) {
					$id = false;
					$title = $newrow['title'];
					
					if($title != '') {
						if(!empty($newrow['external_id']) || $newrow['external_id'] == 0) {
							$itemExists = $this->db->query("SELECT id FROM tc_places WHERE parentType = 'event' AND parentId = $eventid AND external_id = ? LIMIT 1", $newrow['external_id']);
							if($itemExists->num_rows() > 0) {
								$id = $itemExists->row()->id;
							}
						}

						if($id !== false) {
							$id = $this->general_model->update('tc_places', $id, array(
								'appid'	=> $appid,
								'parentType' => 'event',
								'parentId' => $eventid,
								'title' 	=> $title,
								'info'		=> $newrow['info'],
								'addr'	=> $newrow['address'],
								'external_id' => $newrow['external_id'],
								'lat' => $newrow['latitude(optional)'],
								'lng' => $newrow['longitude(optional)'],
								// 'imageurl' => 'http://clients.tapcrowd.com/musteat/'.$newrow['imageurl1'].'.jpg'
							));
						} else {
							$id = $this->general_model->insert('tc_places', array(
								'appid'	=> $appid,
								'parentType' => 'event',
								'parentId' => $eventid,
								'title' 	=> $title,
								'info'		=> $newrow['info'],
								'addr'	=> $newrow['address'],
								'external_id' => $newrow['external_id'],
								'lat' => $newrow['latitude(optional)'],
								'lng' => $newrow['longitude(optional)'],
								// 'imageurl' => 'http://clients.tapcrowd.com/musteat/'.$newrow['imageurl1'].'.jpg'
							));
						}
					}

					if($id) {
						$igroup = 1;
						while($igroup <= 20) {
							$group = $newrow['category'.$igroup];
							if(!empty($group)) {
								$groupExists = $this->db->query("SELECT id FROM `group` WHERE eventid = $eventid AND name = ? LIMIT 1", $group);
								if($groupExists->num_rows() != 0) {
									$groupid = $groupExists->row()->id;
								} else {
									$groupid = $this->general_model->insert('group', array('eventid' => $eventid, 'name' => $group));
								}

								if($groupid) {
									$groupitemExists = $this->db->query("SELECT id FROM groupitem WHERE groupid = $groupid AND itemtable = 'tc_places' AND itemid = ? LIMIT 1", $id);
									if($groupitemExists->num_rows() != 0) {
										$groupitemid = $groupitemExists->row()->id;
									} else {
										$groupitemid = $this->general_model->insert('groupitem', array('groupid' => $groupid, 'itemtable' => 'tc_places', 'itemid' => $id, 'appid' => $appid, 'eventid' => $eventid));
									}
								}
							}
							$igroup++;
						}

						// musteat
						if(!empty($newrow['Tel'])) {
							$metaExists = $this->db->query("SELECT id, type FROM tc_metadata WHERE appid = $appid AND name = 'Tel' LIMIT 1");
							if($metaExists->num_rows() > 0) {
								$metaId = $metaExists->row()->id;
								$metaType = $metaExists->row()->type;

								$metavalueExists = $this->db->query("SELECT metaId FROM tc_metavalues WHERE metaId = $metaId AND parentType = 'place' AND parentId = $id LIMIT 1");
								if($metavalueExists->num_rows() == 0) {
									$this->general_model->insert('tc_metavalues', array('metaId' => $metaId, 'parentType' => 'place', 'parentId' => $id, 'name' => 'Tel', 'type' => $metaType, 'value' => $newrow['Tel'], 'language' => $app->defaultlanguage));
								}
							}
						}

						if(!empty($newrow['Url'])) {
							$metaExists = $this->db->query("SELECT id, type FROM tc_metadata WHERE appid = $appid AND name = 'Url' LIMIT 1");
							if($metaExists->num_rows() > 0) {
								$metaId = $metaExists->row()->id;
								$metaType = $metaExists->row()->type;

								$metavalueExists = $this->db->query("SELECT metaId FROM tc_metavalues WHERE metaId = $metaId AND parentType = 'place' AND parentId = $id LIMIT 1");
								if($metavalueExists->num_rows() == 0) {
									$this->general_model->insert('tc_metavalues', array('metaId' => $metaId, 'parentType' => 'place', 'parentId' => $id, 'name' => 'Url', 'type' => $metaType, 'value' => $newrow['Url'], 'language' => $app->defaultlanguage));
								}
							}
						}

						if(!empty($newrow['imageurl1'])) {
							$metaExists = $this->db->query("SELECT id, type FROM tc_metadata WHERE appid = $appid AND name = 'image1' LIMIT 1");
							if($metaExists->num_rows() > 0) {
								$metaId = $metaExists->row()->id;
								$metaType = $metaExists->row()->type;

								$metavalueExists = $this->db->query("SELECT metaId FROM tc_metavalues WHERE metaId = $metaId AND parentType = 'place' AND parentId = $id LIMIT 1");
								if($metavalueExists->num_rows() == 0) {
									$this->general_model->insert('tc_metavalues', array('metaId' => $metaId, 'parentType' => 'place', 'parentId' => $id, 'name' => 'imageurl1', 'type' => $metaType, 'value' => 'http://clients.tapcrowd.com/musteat/'.$newrow['imageurl1'].'.jpg', 'language' => $app->defaultlanguage));
								}
							}
						}

						if(!empty($newrow['imageurl2'])) {
							$metaExists = $this->db->query("SELECT id, type FROM tc_metadata WHERE appid = $appid AND name = 'image2' LIMIT 1");
							if($metaExists->num_rows() > 0) {
								$metaId = $metaExists->row()->id;
								$metaType = $metaExists->row()->type;

								$metavalueExists = $this->db->query("SELECT metaId FROM tc_metavalues WHERE metaId = $metaId AND parentType = 'place' AND parentId = $id LIMIT 1");
								if($metavalueExists->num_rows() == 0) {
									$this->general_model->insert('tc_metavalues', array('metaId' => $metaId, 'parentType' => 'place', 'parentId' => $id, 'name' => 'imageurl2', 'type' => $metaType, 'value' => 'http://clients.tapcrowd.com/musteat/'.$newrow['imageurl2'].'.jpg', 'language' => $app->defaultlanguage));
								}
							}
						}

						if(!empty($newrow['imageurl3'])) {
							$metaExists = $this->db->query("SELECT id, type FROM tc_metadata WHERE appid = $appid AND name = 'image3' LIMIT 1");
							if($metaExists->num_rows() > 0) {
								$metaId = $metaExists->row()->id;
								$metaType = $metaExists->row()->type;

								$metavalueExists = $this->db->query("SELECT metaId FROM tc_metavalues WHERE metaId = $metaId AND parentType = 'place' AND parentId = $id LIMIT 1");
								if($metavalueExists->num_rows() == 0) {
									$this->general_model->insert('tc_metavalues', array('metaId' => $metaId, 'parentType' => 'place', 'parentId' => $id, 'name' => 'imageurl3', 'type' => $metaType, 'value' => 'http://clients.tapcrowd.com/musteat/'.$newrow['imageurl3'].'.jpg', 'language' => $app->defaultlanguage));
								}
							}
						}
					}
				}
				$headers = FALSE;
			}
			fclose($handle);
			}
		}
	}
}