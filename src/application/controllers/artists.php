<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Artists extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('artist_model');
		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Team member',
			'plural' => 'Team members',
			'title' => 'Team members',
			'parentType' => 'event',
			'browse_url' => 'artists/--parentType--/--parentId--',
			'add_url' => 'artists/add/--parentId--/--parentType--',
			'edit_url' => 'artists/edit/--id--',
			'module_url' => 'module/editByController/artists/--parentType--/--parentId--/0',
			'import_url' => '',
			'headers' => array(
				__('Name') => 'name',
				__('Order') => 'order'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'artists/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/artists/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'artists/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false,
			'deletecheckedurl' => 'artists/removemany/--parentId--/--parentType--'
		);
	}

	function index() {
		$this->app(_currentApp()->id);
	}

	function app($id) {
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');
        $this->session->set_userdata('artistid', '');
		if($id == FALSE || $id == 0) redirect('apps');

		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$this->iframeurl = "artists/index/" . $id;

        $artists = $this->artist_model->getArtistsByAppID($id);

        $this->_module_settings->parentType = 'app';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('app', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('app', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'app', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'app', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('app', $id), $deletecheckedurl);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($artists)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$cdata['crumb']			= array(__("Artists") => "artists");
		$this->load->view('master', $cdata);
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		// $this->iframeurl = "artists/event/" . $id;
		$this->iframeurl = "artists/index/" . $app->id;

        $artists = $this->artist_model->getArtistsByEventId($id);

        $this->_module_settings->parentType = 'event';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($artists)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Artists") => "artists/event/".$id);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'artists');
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		// $this->iframeurl = "artists/venue/" . $id;
		$this->iframeurl = "artists/index/" . $app->id;

        $artists = $this->artist_model->getArtistsByVenueId($id);

        $this->_module_settings->parentType = 'venue';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'venue', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'venue', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $deletecheckedurl);

		$launcher = $this->module_mdl->getLauncher(18, 'venue', $venue->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($artists)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "artists/venue/".$id));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'artists');
		$this->load->view('master', $cdata);
	}

	function add($typeid = '', $type= '') {
		if($type == '') {
			//app
			$this->load->library('form_validation');
	        $error = "";

	        $app = $this->app_model->get($typeid);

			_actionAllowed($app, 'app');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$defaultMeta = $this->general_model->get_default_meta_data('artist', $app->apptypeid);
			$metadatatemp = $this->general_model->get_meta_data_temp('artist', $app->id);
			$usedMetadata = $this->general_model->getUsedMetadata($typeid,'artist');

			$this->iframeurl = "artists/index/" . $typeid;

			$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND artistid <> 0 GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}

	        if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				foreach($languages as $language) {
                    $this->form_validation->set_rules('description_'.$language->key, 'description ('.$language->name.')', 'trim|required');
				}
	            $this->form_validation->set_rules('order', 'order', 'trim');
	            $this->form_validation->set_rules('number', 'number', 'trim');
	            $this->form_validation->set_rules('imageurl', 'imageurl', 'trim');

				foreach($usedMetadata as $used) {
					$this->form_validation->set_rules('usedmeta_'.$used->key, $used->key, 'trim');
				}


	            if($this->form_validation->run() == FALSE){
	                $error = __("Some fields are missing.");
	            } else {
	                $data = array(
	                        "appid"             => $app->id,
							"name"				=> set_value('name'),
							"description"		=> set_value('description_'.  $app->defaultlanguage),
	                        "order"				=> set_value('order'),
	                        "number"			=> set_value('number')
	                    );

	                if($newid = $this->general_model->insert('artist', $data)){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/artistimages/".$newid)){
							mkdir($this->config->item('imagespath') . "upload/artistimages/".$newid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/artistimages/'.$newid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('imageurl')) {
							$image = ""; //No image uploaded!
							$error = $this->upload->display_errors();
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/artistimages/'. $newid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('artist', $newid, array('imageurl' => $image));

						// *** SAVE TAGS *** //
						if($typeid != 393) {
							if($this->input->post('mytagsulselect') != null && count($this->input->post('mytagsulselect')) > 0) {
								foreach ($this->input->post('mytagsulselect') as $tag) {
									$tags = array(
											'appid' 	=> $app->id,
											'tag' 		=> $tag,
											'artistid' => $newid
										);
									$this->db->insert('tag', $tags);
								}
							}
						} else {
							$tagdata = array(
									'appid' 	=> $app->id,
									'tag' 		=> str_replace('&','&amp;',$this->input->post('tagdelbar')),
									'artistid' => $newid
								);
							$this->db->insert('tag',$tagdata);
						}
						// *** //

					   //add translated fields to translation table
						foreach($languages as $language) {
							//description
							$this->language_model->addTranslation('artist', $newid, 'description', $language->key, $this->input->post('description_'.$language->key));
						}

						foreach($usedMetadata as $used) {
							if($this->input->post('usedmeta_'.$used->key) != null && $this->input->post('usedmeta_').$used->key != '') {
								$useddata = array(
										'appid'	=> $typeid,
										'`table`'	=> 'artist',
										'identifier'	=> $newid,
										'`key`'	=> $used->key,
										'value'	=> $this->input->post('usedmeta_'.$used->key)
									);
								$this->general_model->insert('metadata', $useddata);
							}
						}

						// foreach($defaultMeta as $meta) {
						// 	$metadata = array(
						// 			"appid"			=> $typeid,
						// 			"table"			=> 'artist',
						// 			"identifier"	=> $newid,
						// 			"key"           => $meta->key,
						// 			"value"			=> $this->input->post($meta->key)
						// 		);

						// 	$this->general_model->insert('metadata', $metadata);
						// }

						foreach($metadatatemp as $meta) {
							$metadata = array(
									"appid"			=> $typeid,
									"table"			=> 'artist',
									"identifier"	=> $newid,
									"key"           => $meta->key,
									"value"			=> $meta->value
								);

							$this->general_model->insert('metadata', $metadata);
							$this->general_model->remove('metadatatemp', $meta->id);
						}

						setcookie('forminfoname', '',time()-60);
						setcookie('forminfodescription', '',time()-60);
						setcookie('forminfoorder', '',time()-60);
						setcookie('forminfonumber', '',time()-60);


	                    $this->session->set_flashdata('artist_feedback', __('The artist has successfully been add!'));
	                    $this->general_model->update('app', $app->id, array('timestamp' => time()));

	                    //quickfix delbar
	                    if($app->id == 393) {
	                    	_updateVenueTimeStamp(1042);
	                    }

	                    redirect('artists/app/'.$app->id);
	                } else {
	                    $error = __("Oops, something went wrong. Please try again.");
	                }
	            }
	        }

	        $cdata['content'] 		= $this->load->view('c_artist_add', array('error' => $error, 'languages' => $languages, 'app' => $app, 'defaultMeta' => $defaultMeta, 'metadatatemp' => $metadatatemp, 'usedMetadata' => $usedMetadata, 'apptags' => $apptags), TRUE);
	        $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
	        $cdata['crumb']			= array(__("Artists") => "artists", __("New") => "artist/add/");
	        $this->load->view('master', $cdata);
		} elseif($type == 'event') {
			$this->load->library('form_validation');
	        $error = "";
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			$appid = $app->id;

			$this->iframeurl = "artists/event/" . $typeid;


			$defaultMeta = $this->general_model->get_default_meta_data('artist', $app->apptypeid);
			$metadatatemp = $this->general_model->get_meta_data_temp('artist', $app->id);
			$usedMetadata = $this->general_model->getUsedMetadata($appid,'artist');

			$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND artistid <> 0 GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}

	        if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				foreach($languages as $language) {
                    $this->form_validation->set_rules('description_'.$language->key, 'description ('.$language->name.')', 'trim|required');
				}
	            $this->form_validation->set_rules('order', 'order', 'trim');
	            $this->form_validation->set_rules('number', 'number', 'trim');
	            $this->form_validation->set_rules('imageurl', 'imageurl', 'trim');

				foreach($usedMetadata as $used) {
					$this->form_validation->set_rules('usedmeta_'.$used->key, $used->key, 'trim');
				}

	            if($this->form_validation->run() == FALSE){
	                $error = "Some fields are missing.";
	            } else {
	                $data = array(
	                        "eventid"           => $typeid,
							"name"				=> set_value('name'),
							"description"		=> set_value('description_'.  $app->defaultlanguage),
	                        "order"				=> set_value('order'),
	                        "number"			=> set_value('number')
	                    );

	                if($newid = $this->general_model->insert('artist', $data)){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/artistimages/".$newid)){
							mkdir($this->config->item('imagespath') . "upload/artistimages/".$newid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/artistimages/'.$newid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('imageurl')) {
							$image = ""; //No image uploaded!
							$error = $this->upload->display_errors();
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/artistimages/'. $newid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('artist', $newid, array('imageurl' => $image));

						// *** SAVE TAGS *** //
						if($appid != 393) {
							if($this->input->post('mytagsulselect') != null && count($this->input->post('mytagsulselect')) > 0) {
								foreach ($this->input->post('mytagsulselect') as $tag) {
									$tags = array(
											'appid' 	=> $app->id,
											'tag' 		=> $tag,
											'artistid' => $newid
										);
									$this->db->insert('tag', $tags);
								}
							}
						} else {
							$tagdata = array(
									'appid' 	=> $app->id,
									'tag' 		=> str_replace('&','&amp;',$this->input->post('tagdelbar')),
									'artistid' => $newid
								);
							$this->db->insert('tag',$tagdata);
						}
						// *** //

					   //add translated fields to translation table
						foreach($languages as $language) {
							//description
							$this->language_model->addTranslation('artist', $newid, 'description', $language->key, $this->input->post('description_'.$language->key));
						}

						foreach($usedMetadata as $used) {
							if($this->input->post('usedmeta_'.$used->key) != null && $this->input->post('usedmeta_').$used->key != '') {
								$useddata = array(
										'appid'	=> $appid,
										'`table`'	=> 'artist',
										'identifier'	=> $newid,
										'`key`'	=> $used->key,
										'value'	=> $this->input->post('usedmeta_'.$used->key)
									);
								$this->general_model->insert('metadata', $useddata);
							}
						}

						// foreach($defaultMeta as $meta) {
						// 	$metadata = array(
						// 			"appid"			=> $appid,
						// 			"table"			=> 'artist',
						// 			"identifier"	=> $newid,
						// 			"key"           => $meta->key,
						// 			"value"			=> $this->input->post($meta->key)
						// 		);

						// 	$this->general_model->insert('metadata', $metadata);
						// }

						foreach($metadatatemp as $meta) {
							$metadata = array(
									"appid"			=> $appid,
									"table"			=> 'artist',
									"identifier"	=> $newid,
									"key"           => $meta->key,
									"value"			=> $meta->value
								);

							$this->general_model->insert('metadata', $metadata);
							$this->general_model->remove('metadatatemp', $meta->id);
						}

						setcookie('forminfoname', '',time()-60);
						setcookie('forminfodescription', '',time()-60);
						setcookie('forminfoorder', '',time()-60);
						setcookie('forminfonumber', '',time()-60);


	                    $this->session->set_flashdata('artist_feedback', __('The artist has successfully been add!'));
	                    $this->general_model->update('app', $app->id, array('timestamp' => time()));

	                    _updateTimeStamp($typeid);

	                    redirect('artists/event/'.$typeid);
	                } else {
	                    $error = __("Oops, something went wrong. Please try again.");
	                }
	            }
	        }

	        $cdata['content'] 		= $this->load->view('c_artist_add', array('error' => $error, 'languages' => $languages, 'app' => $app, 'event' => $event, 'defaultMeta' => $defaultMeta, 'metadatatemp' => $metadatatemp, 'usedMetadata' => $usedMetadata, 'apptags' => $apptags), TRUE);
	        $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
	        $cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Artists") => "artists/event/".$event->id, __("New") => "artist/add/".$appid.'/event/'.$typeid);
	        $cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'artists');
	        $this->load->view('master', $cdata);
		}elseif($type == 'venue') {
			$this->load->library('form_validation');
	        $error = "";
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = "artists/venue/" . $typeid;

			// $defaultMeta = $this->general_model->get_default_meta_data('artist', $app->apptypeid);
			// $metadatatemp = $this->general_model->get_meta_data_temp('artist', $app->id);
			// $usedMetadata = $this->general_model->getUsedMetadata($appid,'artist');

			$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND artistid <> 0 GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}

			$launcher = $this->module_mdl->getLauncher(18, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, '', '');

	        if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				foreach($languages as $language) {
                    $this->form_validation->set_rules('description_'.$language->key, 'description ('.$language->name.')', 'trim|required');
				}
	            $this->form_validation->set_rules('order', 'order', 'trim');
	            $this->form_validation->set_rules('number', 'number', 'trim');
	            $this->form_validation->set_rules('imageurl', 'imageurl', 'trim');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}

				foreach($usedMetadata as $used) {
					$this->form_validation->set_rules('usedmeta_'.$used->key, $used->key, 'trim');
				}

	            if($this->form_validation->run() == FALSE){
	                $error = "Some fields are missing.";
	            } else {
	                $data = array(
	                		'appid'				=> $app->id,
	                        "venueid"           => $typeid,
							"name"				=> set_value('name'),
							"description"		=> set_value('description_'.  $app->defaultlanguage),
	                        "order"				=> set_value('order'),
	                        "number"			=> set_value('number')
	                    );

	                if($newid = $this->general_model->insert('artist', $data)){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/artistimages/".$newid)){
							mkdir($this->config->item('imagespath') . "upload/artistimages/".$newid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/artistimages/'.$newid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('imageurl')) {
							$image = ""; //No image uploaded!
							$error = $this->upload->display_errors();
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/artistimages/'. $newid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('artist', $newid, array('imageurl' => $image));

						// *** SAVE TAGS *** //
						if($appid != 393) {
							if($this->input->post('mytagsulselect') != null && count($this->input->post('mytagsulselect')) > 0) {
								foreach ($this->input->post('mytagsulselect') as $tag) {
									$tags = array(
											'appid' 	=> $app->id,
											'tag' 		=> $tag,
											'artistid' => $newid
										);
									$this->db->insert('tag', $tags);
								}
							}
						} else {
							$tagdata = array(
									'appid' 	=> $app->id,
									'tag' 		=> str_replace('&','&amp;',$this->input->post('tagdelbar')),
									'artistid' => $newid
								);
							$this->db->insert('tag',$tagdata);
						}
						// *** //

					   //add translated fields to translation table
						foreach($languages as $language) {
							//description
							$this->language_model->addTranslation('artist', $newid, 'description', $language->key, $this->input->post('description_'.$language->key));
						}

						foreach($metadata as $m) {
							$postfield = _getPostedField($m, $_POST, $_FILES);
							if($postfield !== false) {
								if(_validateInputField($m, $postfield)) {
									_saveInputField($m, $postfield, 'artist', $newid, $app->id);
								}
							}
						}

						// foreach($usedMetadata as $used) {
						// 	if($this->input->post('usedmeta_'.$used->key) != null && $this->input->post('usedmeta_').$used->key != '') {
						// 		$useddata = array(
						// 				'appid'	=> $appid,
						// 				'`table`'	=> 'artist',
						// 				'identifier'	=> $newid,
						// 				'`key`'	=> $used->key,
						// 				'value'	=> $this->input->post('usedmeta_'.$used->key)
						// 			);
						// 		$this->general_model->insert('metadata', $useddata);
						// 	}
						// }

						// foreach($metadatatemp as $meta) {
						// 	$metadata = array(
						// 			"appid"			=> $appid,
						// 			"table"			=> 'artist',
						// 			"identifier"	=> $newid,
						// 			"key"           => $meta->key,
						// 			"value"			=> $meta->value
						// 		);

						// 	$this->general_model->insert('metadata', $metadata);
						// 	$this->general_model->remove('metadatatemp', $meta->id);
						// }

						// setcookie('forminfoname', '',time()-60);
						// setcookie('forminfodescription', '',time()-60);
						// setcookie('forminfoorder', '',time()-60);
						// setcookie('forminfonumber', '',time()-60);


	                    $this->session->set_flashdata('artist_feedback', __('The artist has successfully been add!'));
	                    $this->general_model->update('app', $app->id, array('timestamp' => time()));

	                    _updateVenueTimeStamp($typeid);

	                    redirect('artists/venue/'.$typeid);
	                } else {
	                    $error = __("Oops, something went wrong. Please try again.");
	                }
	            }
	        }

	        $cdata['content'] 		= $this->load->view('c_artist_add', array('error' => $error, 'languages' => $languages, 'app' => $app, 'venue' => $venue, 'defaultMeta' => $defaultMeta, 'metadatatemp' => $metadatatemp, 'usedMetadata' => $usedMetadata, 'apptags' => $apptags, 'metadata' => $metadata), TRUE);
	        $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
	        $cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "artists/venue/".$venue->id, __("New") => "artist/add/".$appid.'/venue/'.$typeid));
	        $cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'artists');
	        $this->load->view('master', $cdata);
	    }

	}

	function edit($id, $type = "event", $typeid = '') {
		if($type == 'app' || $type == '') {
			if($typeid == '') {
				$appid = _currentApp()->id;
			} else {
				$appid = $typeid;
			}
			$app = $this->app_model->get($appid);
			$languages = $this->language_model->getLanguagesOfApp($app->id);
	        $this->load->library('form_validation');
	        $error = "";

			$this->iframeurl = "artists/view/" . $id;

	        // EDIT SESSION
	        $artist = $this->artist_model->getArtistByID($id);
	        if($artist == FALSE) redirect('artists');
			if($artist->appid != $app->id) redirect('apps');

	        $metadata = $this->general_model->get_meta_data('artist', $id);
	        $usedMetadata = $this->general_model->getUsedMetadata($appid,'artist');

			// TAGS
			$tags = $this->db->query("SELECT tag FROM tag WHERE artistid = $id");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag->tag;
				}
				$tags = $tagz;
			}

			$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND artistid <> 0 GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}

	        if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				foreach($languages as $language) {
					$this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
				}
	            $this->form_validation->set_rules('order', 'order', 'trim');
	            $this->form_validation->set_rules('number', 'number', 'trim');


				foreach($usedMetadata as $used) {
					$this->form_validation->set_rules('usedmeta_'.$used->key, $used->key, 'trim');
				}

	            if($this->form_validation->run() == FALSE){
	                $error = "Some fields are missing.";
	            } else {
					if($this->form_validation->run() == FALSE){
						$error = "Some fields are missing.";
					} else {
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/artistimages/".$id)){
							mkdir($this->config->item('imagespath') . "upload/artistimages/".$id, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/artistimages/'.$id;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('imageurl')) {
							$image = $artist->imageurl; //No image uploaded!
							$error = $this->upload->display_errors();
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/artistimages/'. $id . '/' . $returndata['file_name'];
						}

						$data = array(
								"appid"             => $app->id,
								"name"				=> set_value('name'),
								"description"		=> set_value('description_'. $app->defaultlanguage),
								"imageurl"			=> $image,
								"order"				=> set_value('order'),
								"number"			=> set_value('number')
							);

						if($this->artist_model->edit($id, $data)){
							// *** SAVE TAGS *** //
							$this->db->where('artistid', $id);
							// if($this->db->delete('tag')){
							// 	if($app->id != 393) {
							// 			if ($this->input->post('mytagsulselect') != '' && count($this->input->post('mytagsulselect')) > 0) {
							// 				foreach ($this->input->post('mytagsulselect') as $tag) {
							// 					$tags = array(
							// 							'appid' 	=> $app->id,
							// 							'tag' 		=> $tag,
							// 							'artistid' => $id
							// 						);
							// 					$this->db->insert('tag', $tags);
							// 				}
							// 			}

							// 	} else {
							// 		$tagdata = array(
							// 				'appid' 	=> $app->id,
							// 				'tag' 		=> str_replace('&','&amp;',$this->input->post('tagdelbar')),
							// 				'artistid' => $id
							// 			);
							// 		$this->db->insert('tag',$tagdata);
							// 	}
							// }
							// *** //

							foreach($usedMetadata as $used) {
								if($this->input->post('usedmeta_'.$used->key) != null && $this->input->post('usedmeta_').$used->key != '') {
									$useddata = array(
											'appid'	=> $appid,
											'`table`'	=> 'artist',
											'identifier'	=> $id,
											'`key`'	=> $used->key,
											'value'	=> $this->input->post('usedmeta_'.$used->key)
										);
									$this->general_model->insert_or_update('metadata', $useddata, 'value');
								}
							}

							//add translated fields to translation table
							foreach($languages as $language) {
								$descrSuccess = $this->language_model->updateTranslation('artist', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
								if(!$descrSuccess) {
									$this->language_model->addTranslation('artist', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
								}
							}

							$this->session->set_flashdata('artist_feedback', __('The artist has successfully been updated'));
							$this->general_model->update('app', $app->id, array('timestamp' => time()));

							//quickfix delbar
		                    if($app->id == 393) {
		                    	_updateVenueTimeStamp(1042);
		                    }

							redirect('artists/app/'.$app->id);
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
	            }
	        }

	        $cdata['content'] 		= $this->load->view('c_artist_edit', array('artist' => $artist,'error' => $error, 'metadata' => $metadata, 'languages' => $languages, 'app' => $app, 'tags' => $tags, 'usedMetadata' => $usedMetadata, 'apptags' => $apptags), TRUE);
	        $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('artist' => $artist), TRUE);
	        $cdata['crumb']			= array(__("Artists") => "artists", __("Edit: ") . $artist->name => "artists/edit/".$artist->id);
	        $this->load->view('master', $cdata);
		} elseif($type == 'event') {
	        $artist = $this->artist_model->getArtistByID($id);
	        if($artist == FALSE) redirect('events');
			$event = $this->event_model->getbyid($artist->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$appid = $app->id;
			$languages = $this->language_model->getLanguagesOfApp($app->id);
	        $this->load->library('form_validation');
	        $error = "";

	        $this->iframeurl = "artists/view/" . $id;

	        $metadata = $this->general_model->get_meta_data('artist', $id);
	        $usedMetadata = $this->general_model->getUsedMetadata($appid,'artist');

			// TAGS
			$tags = $this->db->query("SELECT tag FROM tag WHERE artistid = $id");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag->tag;
				}
				$tags = $tagz;
			}

			$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND artistid <> 0 GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}


	        if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				foreach($languages as $language) {
					$this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
				}
	            $this->form_validation->set_rules('order', 'order', 'trim');
	            $this->form_validation->set_rules('number', 'number', 'trim');


				foreach($usedMetadata as $used) {
					$this->form_validation->set_rules('usedmeta_'.$used->key, $used->key, 'trim');
				}

	            if($this->form_validation->run() == FALSE){
	                $error = "Some fields are missing.";
	            } else {
					if($this->form_validation->run() == FALSE){
						$error = __("Some fields are missing.");
					} else {
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/artistimages/".$id)){
							mkdir($this->config->item('imagespath') . "upload/artistimages/".$id, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/artistimages/'.$id;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('imageurl')) {
							$image = $artist->imageurl; //No image uploaded!
							$error = $this->upload->display_errors();
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/artistimages/'. $id . '/' . $returndata['file_name'];
						}

						$data = array(
								"name"				=> set_value('name'),
								"description"		=> set_value('description_'. $app->defaultlanguage),
								"imageurl"			=> $image,
								"order"				=> set_value('order'),
								"number"			=> set_value('number')
							);

						if($this->artist_model->edit($id, $data)){
							// *** SAVE TAGS *** //
							// $this->db->where('artistid', $id);
							// if($this->db->delete('tag')){
							// 	if($app->id != 393) {
							// 			if ($this->input->post('mytagsulselect') != '' && count($this->input->post('mytagsulselect')) > 0) {
							// 				foreach ($this->input->post('mytagsulselect') as $tag) {
							// 					$tags = array(
							// 							'appid' 	=> $app->id,
							// 							'tag' 		=> $tag,
							// 							'artistid' => $id
							// 						);
							// 					$this->db->insert('tag', $tags);
							// 				}
							// 			}

							// 	} else {
							// 		$tagdata = array(
							// 				'appid' 	=> $app->id,
							// 				'tag' 		=> str_replace('&','&amp;',$this->input->post('tagdelbar')),
							// 				'artistid' => $id
							// 			);
							// 		$this->db->insert('tag',$tagdata);
							// 	}
							// }
							// *** //

							foreach($usedMetadata as $used) {
								if($this->input->post('usedmeta_'.$used->key) != null && $this->input->post('usedmeta_').$used->key != '') {
									$useddata = array(
											'appid'	=> $appid,
											'`table`'	=> 'artist',
											'identifier'	=> $id,
											'`key`'	=> $used->key,
											'value'	=> $this->input->post('usedmeta_'.$used->key)
										);
									$this->general_model->insert_or_update('metadata', $useddata, 'value');
								}
							}

							//add translated fields to translation table
							foreach($languages as $language) {
								$descrSuccess = $this->language_model->updateTranslation('artist', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
								if(!$descrSuccess) {
									$this->language_model->addTranslation('artist', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
								}
							}

							$this->session->set_flashdata('artist_feedback', __('The artist has successfully been updated'));
		                    _updateTimeStamp($event->id);

							redirect('artists/event/'.$event->id);
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
	            }
	        }

	        $cdata['content'] 		= $this->load->view('c_artist_edit', array('artist' => $artist,'error' => $error, 'metadata' => $metadata, 'languages' => $languages, 'app' => $app, 'tags' => $tags, 'usedMetadata' => $usedMetadata, 'apptags' => $apptags), TRUE);
	        $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('artist' => $artist), TRUE);
	        $cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id,__("Artists") => "artists/event/".$event->id, __("Edit: ") . $artist->name => "artists/edit/".$artist->id.'/event');
	        $cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'artists');
	        $this->load->view('master', $cdata);
		} elseif($type == 'venue') {
	        $artist = $this->artist_model->getArtistByID($id);
	        if($artist == FALSE) redirect('events');
			$venue = $this->venue_model->getbyid($artist->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$appid = $app->id;
			$languages = $this->language_model->getLanguagesOfApp($app->id);
	        $this->load->library('form_validation');
	        $error = "";

	        $this->iframeurl = "artists/view/" . $id;

	        // $metadata = $this->general_model->get_meta_data('artist', $id);
	        // $usedMetadata = $this->general_model->getUsedMetadata($appid,'artist');

			// TAGS
			$tags = $this->db->query("SELECT tag FROM tag WHERE artistid = $id");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag->tag;
				}
				$tags = $tagz;
			}

			$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND artistid <> 0 GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}

			$launcher = $this->module_mdl->getLauncher(18, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, 'artist', $artist->id, $app);

	        if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				foreach($languages as $language) {
					$this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
				}
	            $this->form_validation->set_rules('order', 'order', 'trim');
	            $this->form_validation->set_rules('number', 'number', 'trim');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}

				foreach($usedMetadata as $used) {
					$this->form_validation->set_rules('usedmeta_'.$used->key, $used->key, 'trim');
				}

	            if($this->form_validation->run() == FALSE){
	                $error = "Some fields are missing.";
	            } else {
					if($this->form_validation->run() == FALSE){
						$error = "Some fields are missing.";
					} else {
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/artistimages/".$id)){
							mkdir($this->config->item('imagespath') . "upload/artistimages/".$id, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/artistimages/'.$id;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('imageurl')) {
							$image = $artist->imageurl; //No image uploaded!
							$error = $this->upload->display_errors();
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/artistimages/'. $id . '/' . $returndata['file_name'];
						}

						$data = array(
								"name"				=> set_value('name'),
								"description"		=> set_value('description_'. $app->defaultlanguage),
								"imageurl"			=> $image,
								"order"				=> set_value('order'),
								"number"			=> set_value('number')
							);

						if($this->artist_model->edit($id, $data)){
							// *** SAVE TAGS *** //
							// $this->db->where('artistid', $id);
							// if($this->db->delete('tag')){
							// 	if($app->id != 393) {
							// 			if ($this->input->post('mytagsulselect') != '' && count($this->input->post('mytagsulselect')) > 0) {
							// 				foreach ($this->input->post('mytagsulselect') as $tag) {
							// 					$tags = array(
							// 							'appid' 	=> $app->id,
							// 							'tag' 		=> $tag,
							// 							'artistid' => $id
							// 						);
							// 					$this->db->insert('tag', $tags);
							// 				}
							// 			}

							// 	} else {
							// 		$tagdata = array(
							// 				'appid' 	=> $app->id,
							// 				'tag' 		=> str_replace('&','&amp;',$this->input->post('tagdelbar')),
							// 				'artistid' => $id
							// 			);
							// 		$this->db->insert('tag',$tagdata);
							// 	}
							// }
							// *** //

							// foreach($usedMetadata as $used) {
							// 	if($this->input->post('usedmeta_'.$used->key) != null && $this->input->post('usedmeta_').$used->key != '') {
							// 		$useddata = array(
							// 				'appid'	=> $appid,
							// 				'`table`'	=> 'artist',
							// 				'identifier'	=> $id,
							// 				'`key`'	=> $used->key,
							// 				'value'	=> $this->input->post('usedmeta_'.$used->key)
							// 			);
							// 		$this->general_model->insert_or_update('metadata', $useddata, 'value');
							// 	}
							// }

							//add translated fields to translation table
							foreach($languages as $language) {
								$descrSuccess = $this->language_model->updateTranslation('artist', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
								if(!$descrSuccess) {
									$this->language_model->addTranslation('artist', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
								}
							}

							foreach($metadata as $m) {
								$postfield = _getPostedField($m, $_POST, $_FILES);
								if($postfield !== false) {
									if(_validateInputField($m, $postfield)) {
										_saveInputField($m, $postfield, 'artist', $id, $app->id);
									}
								}
							}

							$this->session->set_flashdata('artist_feedback', __('The artist has successfully been updated'));
		                    _updateVenueTimeStamp($venue->id);

							redirect('artists/venue/'.$venue->id);
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
	            }
	        }

	        $cdata['content'] 		= $this->load->view('c_artist_edit', array('artist' => $artist,'error' => $error, 'metadata' => $metadata, 'languages' => $languages, 'app' => $app, 'tags' => $tags, 'usedMetadata' => $usedMetadata, 'apptags' => $apptags, 'metadata' => $metadata), TRUE);
	        $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('artist' => $artist), TRUE);
	        $cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "artists/venue/".$venue->id, __("Edit: ") . $artist->name => "artists/edit/".$artist->id.'/venue'));
	        $cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'artists');
	        $this->load->view('master', $cdata);
		}

	}

	function delete($id, $type='', $appid = '') {
		if($type == '' || $type == 'app') {
	        $artist = $this->artist_model->getArtistByID($id);
	        if($artist == FALSE) redirect('artists');
			if($artist->appid != $appid) redirect('apps');
			$app = $this->app_model->get($artist->appid);
			_actionAllowed($app, 'app');
	        if($this->general_model->remove('artist', $id)){
	        	$this->general_model->update('app', $appid, array('timestamp' => time()));
	            //quickfix delbar
	            if($appid == 393) {
	            	_updateVenueTimeStamp(1042);
	            }
	            $this->session->set_flashdata('artist_feedback', __('The artist has successfully been deleted'));
	            redirect('artists/app/'. $appid);
	        } else {
	            $this->session->set_flashdata('artist_error', __('Oops, something went wrong. Please try again.'));
	            redirect('artists/app/'. $appid);
	        }
		} elseif($type == 'event') {
	        $artist = $this->artist_model->getArtistByID($id);
	        if($artist == FALSE) redirect('artists');
			$event = $this->event_model->getbyid($artist->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
	        if($this->general_model->remove('artist', $id)){
	        	_updateTimeStamp($event->id);
	            $this->session->set_flashdata('artist_feedback', __('The artist has successfully been deleted'));
	            redirect('artists/event/'. $event->id);
	        } else {
	            $this->session->set_flashdata('artist_error', __('Oops, something went wrong. Please try again.'));
	            redirect('artists/event/'. $event->id);
	        }
		} elseif($type == 'venue') {
	        $artist = $this->artist_model->getArtistByID($id);
	        if($artist == FALSE) redirect('artists');
			$venue = $this->venue_model->getbyid($artist->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($app->organizerid != _currentUser()->organizerId) redirect('venues');
	        if($this->general_model->remove('artist', $id)){
	        	_updateVenueTimeStamp($venue->id);
	            $this->session->set_flashdata('artist_feedback', __('The artist has successfully been deleted'));
	            redirect('artists/venue/'. $venue->id);
	        } else {
	            $this->session->set_flashdata('artist_error', __('Oops, something went wrong. Please try again.'));
	            redirect('artists/venue/'. $venue->id);
	        }
		}

	}

	function deleteimage($imageurl) {
		// unlink file itself
		unlink($imageurl);
		// unlink cache-files
		$info = pathinfo($imageurl);
		$file_name =  basename($imageurl,'.'.$info['extension']);
		array_map("unlink", glob("../cache/*".$file_name));
	}

	function sort($type = '') {
		if($type == '' || $type == FALSE) redirect('events');

		switch($type){
			case "sessions":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('session', $data);
					$this->db->last_query();
				}
				break;
			case "groups":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('sessiongroup', $data);
					$this->db->last_query();
				}
				break;
			default:
				break;
		}
	}

	function autocomplete($field) {
		//In controller
		$keyword = $this->input->post('term');

		$data['response'] = 'false'; //Set default response

		$query = _autocomplete('session', $field, $keyword);

		if(count($query) > 0){
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach($query as $row){
				$data['message'][] = array('label'=> $row->{$field}, 'value'=> $row->{$field}); //Add a row to array
			}
		}
		echo json_encode($data);
	}

	function autocompletetags() {
		//In controller
		$keyword = $this->input->post('term');

		$data['response'] = 'false'; //Set default response

		$query = _autocompletetags(_currentApp()->id, $keyword);
		$keys = array();
		if(count($query) > 0){
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach($query as $row){
				if(!in_array($row->tag,$keys)) {
					$data['message'][] = array('label'=> $row->tag, 'value'=> $row->tag); //Add a row to array
					$keys[] = $row->tag;
				}
			}
		}
		echo json_encode($data);
	}

	function removeimage($id, $appid) {
		$artist = $this->artist_model->getArtistByID($id);
		$app = $this->app_model->get($appid);
        if($artist == FALSE) redirect('artists');
		if($artist->appid != $app->id) redirect('apps');
		_actionAllowed($app, 'app');

		if(!file_exists($this->config->item('imagespath') . $artist->imageurl)) redirect('venues');

		// Delete image + generated thumbs

		unlink($this->config->item('imagespath') . $artist->imageurl);
		$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

		$data = array('imageurl' => '');
		$this->general_model->update('artist', $id, $data);

		redirect('artists/app/'.$appid);
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'artist');
    }   

}