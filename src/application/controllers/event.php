<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Event extends CI_Controller {

        //php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
			$this->load->model('schedule_model');
	}

	function index() {
		$this->session->set_userdata('eventid', '');
		redirect('events');
	}

	function event($id) {
		redirect('event/view/'.$id);
	}

	function add($launcherid = '') {
		$internal_error = FALSE;
		$this->load->library('form_validation');
		$error = "";
		$app = _currentApp();
		$this->load->model('venue_model');

		if(isset($app) && $app->familyid == 3) {
			if(isset($tagParam) && $tagParam != '') {
				$this->iframeurl = "events/tag/".$app->id."/".$tagParam;
			} else {
				$this->iframeurl = "events/app/".$app->id;
			}
		} else {
	        if($app->subdomain != "") {
	            $this->iframeurl = "events/app/".$app->id;
	        }
		}

		if($launcherid != '') {
			$this->iframeurl ='events/launcher/'.$app->id.'/'.$launcherid;
		}

		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$venues = array();
		// if($app->apptypeid == 2 || $app->apptypeid == 4) {
		// 	$venues = $this->app_model->getVenuesByApp($app->id);
		// }

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND eventid <> 0 GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}

		if($this->input->post('postback') == 'postback') {
			// $this->form_validation->set_rules('w_eventtypeid', 'event type', 'trim|max_length[1]|required');
			$this->form_validation->set_rules('w_datefrom', 'date from', 'trim|required');
			$this->form_validation->set_rules('w_dateto', 'date to', 'trim|required');
			$this->form_validation->set_rules('w_venuename', 'w_venuename', 'trim');
			$this->form_validation->set_rules('w_venuestreet', 'w_venuestreet', 'trim');
			$this->form_validation->set_rules('w_venuenumber', 'w_venuenumber', 'trim');
			$this->form_validation->set_rules('w_venuezip', 'w_venuezip', 'trim');
			$this->form_validation->set_rules('w_venuetown', 'w_venuetown', 'trim');
			$this->form_validation->set_rules('w_venuecountry', 'w_venuecountry', 'trim');
            $this->form_validation->set_rules('phonenr', 'phonenr', 'trim');
            $this->form_validation->set_rules('website', 'website', 'trim');
			$this->form_validation->set_rules('email', 'email', 'trim');
			$this->form_validation->set_rules('order', 'order', 'trim');
            $this->form_validation->set_rules('ticketlink', 'ticketlink', 'trim');

            $this->form_validation->set_rules('w_logo', 'logo', 'trim|callback_image_rules');
			// if($app->apptypeid == 2 || $app->apptypeid == 4) {
			// 	$this->form_validation->set_rules('selvenue', 'venue', 'trim|required');
			// }

            foreach($languages as $language) {
                $this->form_validation->set_rules('w_name_'.$language->key, 'name', 'trim|required');
                $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
            }

			if($this->form_validation->run() == FALSE ){
				$error = validation_errors();
				$datefrom = date('Y-m-d', strtotime($this->input->post('w_datefrom')));
				$dateto =  date('Y-m-d', strtotime($this->input->post('w_dateto')));
				if($datefrom > $dateto) {
					$error .= __("<p>The end date can't be before the start date.</p>");
				}
			} else {
				$datefrom = date('Y-m-d', strtotime($this->input->post('w_datefrom')));
				$dateto =  date('Y-m-d', strtotime($this->input->post('w_dateto')));
				if($datefrom > $dateto) {
					$error .= __("<p>The end date can't be before the start date.</p>");
				}
				if($error =='') {
					//Database model
					$this->load->model('app_model');
					$this->load->model('map_model');
					$this->load->model('venue_model');

					$venueaddress = set_value('w_venuestreet')." ".set_value('w_venuenumber').", ".set_value('w_venuezip')." ".set_value('w_venuetown');
					if(trim($venueaddress) == ',') {
						$venueaddress = '';
					}
					if(set_value('w_venuecountry') != '') {
						$venueaddress .= ', ' . set_value('w_venuecountry');
					}
					//Coordinaten opzoeken via GOOGLE API!
					$latlon = $this->event_model->getLocation($venueaddress);

					//1st submit venue-data in DB and get ID to submit in Event
					// if($app->apptypeid == 2 || $app->apptypeid == 4) {
					// 	$venueid = $this->input->post('selvenue');
					// } else {
						$venueid = $this->venue_model->add(set_value('w_venuename'), $venueaddress, $latlon);
					// }

					//2nd submit event-data+venuedata in DB
					$eventtypeid = 3;
					//$eventlogo = (isset($returndata) ? "upload/eventlogos/".$returndata['file_name'] : "");
					$active = 1; //Do not standard activate an event in the webservice!
					$description = $this->input->post('description_'.  $app->defaultlanguage);
	                $name = $this->input->post('w_name_'.  $app->defaultlanguage);
	                $phonenr = $this->input->post('phonenr');
	                $website = $this->input->post('website');
					$email = $this->input->post('email');
					$order = $this->input->post('order');
	                $ticketlink = $this->input->post('ticketlink');
					$eventlogo = "";

					if($this->input->post('permanent') == 'checked') {
						$datefrom = '1970-01-01';
						$dateto = '1970-01-01';
					}
					$website = checkhttp($website);

					#
					# Get application organizerid (used instead of current users organizerId)
					#
					$organizerId = $app->organizerid; //_currentUser()->organizerId;

					$eventid = $this->event_model->add($name, $organizerId, $venueid, $eventtypeid, $datefrom, $dateto, $active, $eventlogo, $description, $phonenr, $website, $ticketlink, $email,$order);
					$this->app_model->addEventToApp($app->id, $eventid);
					if(_currentUser()->organizerId == 1) {
						//$this->app_model->addEventToApp(1, $eventid); //Add to XpoPlus App!
					} else {
						$this->app_model->addEventToApp(2, $eventid); //Add to TAPCROWD App!
					}

					//insert info launcher
					$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE id = 3")->row();
					$this->general_model->insert('module', array('event' => $eventid, 'moduletype' => 21));

					$launcherdata = array(
						'moduletypeid'	=> 21,
						'eventid'		=> $eventid,
						'module'		=> $defaultlauncher->module,
						'title'			=> $defaultlauncher->title,
						'icon'			=> $defaultlauncher->icon,
						'order'			=> $defaultlauncher->order,
						'active'		=> 1
						);

					$this->db->insert('launcher',$launcherdata);

					//EventData successfully added to DB..get EventID and add the map:
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/eventimages/".$eventid)){
						mkdir($this->config->item('imagespath') . "upload/eventimages/".$eventid, 0755,true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/eventimages/'.$eventid;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('w_logo')) {
						$image = ""; //No image uploaded!
						if($_FILES['w_logo']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/eventimages/'. $eventid . '/' . $returndata['file_name'];
					}

	                //add translated fields to translation table
	                foreach($languages as $language) {
	                    $this->language_model->addTranslation('event', $eventid, 'name', $language->key, $this->input->post('w_name_'.$language->key));
	                    $this->language_model->addTranslation('event', $eventid, 'description', $language->key, $this->input->post('description_'.$language->key));
	                }

	                //save main categorie and brands group for exhibitors
	                $groupdata = array(
	            			'appid'	=> $app->id,
	            			'eventid'	=> $eventid,
	            			'name'	=> 'exhibitorcategories',
	            			'parentid'	=> 0
	            		);
	                $categoriegroupid = $this->general_model->insert('group', $groupdata);
	              //   $groupdata2 = array(
	            		// 	'appid'	=> $app->id,
	            		// 	'eventid'	=> $eventid,
	            		// 	'name'	=> 'exhibitorbrands',
	            		// 	'parentid'	=> 0
	            		// );
	              //   $brandgroupid =$this->general_model->insert('group', $groupdata2);

	                // *** SAVE TAGS *** //
					if($this->input->post('mytagsulselect') != null) {
						foreach ($this->input->post('mytagsulselect') as $tag) {
							$tags = array(
									'appid' 	=> $app->id,
									'tag' 		=> $tag,
									'eventid' => $eventid
								);
							$this->db->insert('tag', $tags);
						}
					}
	                // *** //

					//link event to launcher (city)
	                if($launcherid != '') {
	                	$launcher = $this->db->query("SELECT * FROM launcher WHERE id = ? LIMIT 1", $launcherid)->row();
	                	$this->general_model->insert('tag', array(
	                		'appid' => $app->id,
	                		'tag'	=> $launcher->tag,
	                		'eventid'	=> $eventid,
	                		'active'	=> 1
	                		));
	                }

	                //custom launcher icons kortrijk xpo
	                if($app->id == 1) {
	                	$launcher = $this->event_model->insertKXpoLaunchers($eventid, $categoriegroupid, $brandgroupid);
	                }

	                if($app->apptypeid == 10) {
	                	$this->load->model('module_mdl');
	                	// $this->module_mdl->insertFestivalLaunchers($eventid);
	                }

					if(!$internal_error) {
						if($app->apptypeid == 6) {
							// IF FOOTBALL FLAVOR => CREATE SESSIONGROUPS
							$this->db->insert('sessiongroup', array("eventid" => $eventid, "order" => "0", "name" => $app->name));
							$this->db->insert('sessiongroup', array("eventid" => $eventid, "order" => "1", "name" => $name));
							$this->db->insert("module", array("moduletype" => '10', "event" => $eventid));
						}
						// Succes
						
						_updateTimeStamp($eventid);
						if($error == '') {
							$this->general_model->update('event', $eventid, array('eventlogo' => $image));
							if(isset($_GET['newapp'])) {
								redirect('apps/theme/'.$app->id.'?newapp');
							}
							$this->session->set_flashdata('event_feedback', __('Your event has successfully been added.'));
							redirect('event/view/'.$eventid.'/'.$launcherid);
						} else {
							//image error
							redirect('event/edit/'.$eventid.'?error=image');
						}
					}
				}
			}
		}

		$this->general_model->update('app', $app->id, array('timestamp' => time()));

		$data['error']			= $error;
		$data['eventtypes'] 	= $this->event_model->getEventtypes();
        $data['languages']      = $languages;
        $data['app']            = $app;
		$data['venues']			= $venues;
		$data['launcherid']		= $launcherid;
		$data['apptags']		= $apptags;

		if(isset($_GET['newapp'])) {
			$cdata['content'] 		= $this->load->view('c_event_add', $data, TRUE);
			$this->load->view('master_wizard', $cdata);
		} else {
			$cdata['content'] 		= $this->load->view('c_event_add', $data, TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			if($launcherid == '') {
				if($app->familyid != 1) {
					$cdata['crumb']			= array(__("Events") => "events", __("New") => "event/add/");
					if($app->familyid == 2) {
						$crumbapp = array($app->name => 'apps/view/'.$app->id);
						$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
					}
				} else {
					$cdata['crumb']			= array(__("New") => "event/add/");
				}
			} else {
				$this->load->model('module_mdl');
				$launcher = $this->module_mdl->getLauncherById($launcherid);
				$cdata['crumb']			= array($launcher->title => "events/launcher/".$launcherid, __('New') => 'event/add/'.$launcherid);
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			}
			$this->load->view('master', $cdata);
		}

	}
	function image_rules($str){
		$filename = 'w_logo';
		return image_check($filename, $_FILES);
	}

	function edit($eventid, $launcherid = '') {
		$internal_error = FALSE;
		$this->load->model('venue_model');
		$this->load->model('map_model');
		$this->load->library('form_validation');
		$error = "";

		if($eventid == FALSE || $eventid == 0) redirect('events');
        $this->iframeurl = "event/info/" . $eventid;

		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		$this->session->set_userdata('eventid', $eventid);
		$this->session->set_userdata('appid', $app->id);

        if($app->apptypeid == 10 && $app->id >= 965) {
			redirect('event/info/'.$eventid);
        }

		$maps = $this->map_model->getMapsByEvent($event->id);
		if($maps != FALSE) { $mapid = $maps[0]->id; } else { $mapid = 0; }
		$data['map'] = $this->map_model->getMapsById($mapid);

        $languages = $this->language_model->getLanguagesOfApp($app->id);

		$venues = array();
		// if($app->apptypeid == 2 || $app->apptypeid == 4) {
		// 	$venues = $this->app_model->getVenuesByApp($app->id);
		// }

        // TAGS
        $tags = $this->db->query("SELECT * FROM tag WHERE eventid = $eventid");
        if($tags->num_rows() == 0) {
            $tags = array();
        } else {
            $tagz = array();
            foreach ($tags->result() as $tag) {
                $tagz[] = $tag;
            }
            $tags = $tagz;
        }

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND eventid <> 0 GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}

		//schedule
		$schedule = $this->schedule_model->getScheduleByEvent($eventid);
		$headers = array(__('Day') => 'key', __('Date') => 'date', 'From-till' => 'caption');

		$launcher = $this->module_mdl->getLauncher(21, 'event', $event->id);
		$metadata = $this->metadata_model->getMetadata($launcher, 'event', $event->id, $app);

		if($this->input->post('postback') == "postback") {
			// $this->form_validation->set_rules('w_eventtypeid', 'event type', 'trim|required|max_length[1]');
			$this->form_validation->set_rules('w_datefrom', 'date from', 'trim|required');
			$this->form_validation->set_rules('w_dateto', 'date to', 'trim|required');
			$this->form_validation->set_rules('w_venuename', 'w_venuename', 'trim');
			$this->form_validation->set_rules('w_address', 'w_venuestreet', 'trim');
            $this->form_validation->set_rules('phonenr', 'phonenr', 'trim');
            $this->form_validation->set_rules('website', 'website', 'trim');
			$this->form_validation->set_rules('email', 'email', 'trim');
			$this->form_validation->set_rules('order', 'order', 'trim');
            $this->form_validation->set_rules('ticketlink', 'ticketlink', 'trim');
            $this->form_validation->set_rules('w_logo', 'logo', 'trim|callback_image_rules');

            foreach($languages as $language) {
                $this->form_validation->set_rules('w_name_'.$language->key, 'name', 'trim|required');
                $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}
            }
			//Uploads Images
			if(!is_dir($this->config->item('imagespath') . "upload/eventimages/".$eventid)){
				mkdir($this->config->item('imagespath') . "upload/eventimages/".$eventid, 0755, true);
			}

			$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/eventimages/'.$eventid;
			$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
			$configexlogo['max_width']  = '2000';
			$configexlogo['max_height']  = '2000';
			$this->load->library('upload', $configexlogo);
			$this->upload->initialize($configexlogo);
			if (!$this->upload->do_upload('w_logo')) {
				$image = $event->eventlogo; //No image uploaded!
				if($_FILES['w_logo']['name'] != '') {
					$error .= $this->upload->display_errors();
				}
			} else {
				//successfully uploaded
				$returndata = $this->upload->data();
				$image = 'upload/eventimages/'. $eventid . '/' . $returndata['file_name'];
			}
			$eventlogo = $image;

			if($this->form_validation->run() == FALSE || $error != ''){
				$error .= validation_errors();
				$datefrom = date('Y-m-d', strtotime($this->input->post('w_datefrom')));
				$dateto =  date('Y-m-d', strtotime($this->input->post('w_dateto')));
				if($datefrom > $dateto) {
					$error .= __("<p>The end date can't be before the start date.</p>");
				}
			} else {
				$datefrom = date('Y-m-d', strtotime($this->input->post('w_datefrom')));
				$dateto =  date('Y-m-d', strtotime($this->input->post('w_dateto')));
				if($datefrom > $dateto) {
					$error .= __("<p>The end date can't be before the start date.</p>");
				}

				if($error =='') {
					//Database model
					$this->load->model('map_model');
					$this->load->model('venue_model');

					//Coordinaten opzoeken via GOOGLE API!
					$latlon = $this->event_model->getLocation(set_value('w_address'));

					//1st submit venue-data in DB and get ID to submit in Event
					// if($app->apptypeid == 2 || $app->apptypeid == 4) {
					// 	$venueid = $this->input->post('selvenue');
					// } else {
						$venueid = $this->venue_model->edit($event->venueid, set_value('w_venuename'), set_value('w_address'), $latlon);
					// }
					//2nd submit event-data+venuedata in DB
					$eventtypeid = 3;

					//$eventlogo = (isset($returndata) ? "upload/eventlogos/".$returndata['file_name'] : "");
					$active = $event->active; //Do not standard activate an event in the webservice!
					$description = $this->input->post('description_'.  $app->defaultlanguage);
	                $name = $this->input->post('w_name_'.  $app->defaultlanguage);
	                $phonenr = $this->input->post('phonenr');
	                $website = checkhttp($this->input->post('website'));
					$order = $this->input->post('order');
	                $ticketlink = $this->input->post('ticketlink');
					$email = $this->input->post('email');

					if($this->input->post('permanent') == 'checked') {
						$datefrom = '1970-01-01';
						$dateto = '1970-01-01';
					}


					#
					# Get application organizerid (used instead of current users organizerId)
					#
					$organizerId = $app->organizerid; //_currentUser()->organizerId;

					$this->event_model->edit($event->id, $name, $organizerId, $venueid, $eventtypeid, $datefrom, $dateto, $active, $eventlogo, $description, $phonenr, $website, $ticketlink, $email, $order);

	                //add translated fields to translation table
	                foreach($languages as $language) {
						$nameSuccess = $this->language_model->updateTranslation('event', $eventid, 'name', $language->key, $this->input->post('w_name_'.$language->key));
						if(!$nameSuccess) {
							$this->language_model->addTranslation('event', $eventid, 'name', $language->key, $this->input->post('w_name_'.$language->key));
						}
						$descrSuccess = $this->language_model->updateTranslation('event', $eventid, 'description', $language->key, $this->input->post('description_'.$language->key));
						if(!$descrSuccess) {
							$this->language_model->addTranslation('event', $eventid, 'description', $language->key, $this->input->post('description_'.$language->key));
						}

						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								$postfield = _getPostedField($m, $_POST, $_FILES, $language);
								if($postfield !== false) {
									if(_validateInputField($m, $postfield)) {
										_saveInputField($m, $postfield, 'event', $event->id, $app, $language);
									}
								}
							}
						}
	                }

					if(!$internal_error) {
						// Succes
						$this->session->set_flashdata('event_feedback', __('Your changes have successfully been saved.'));
						_updateTimeStamp($eventid);
						redirect('event/view/'.$eventid.'/'.$launcherid);
					}
				}
			}
		}

		$this->general_model->update('app', $app->id, array('timestamp' => time()));

		$data['error']			= $error;
		$data['eventtypes'] 	= $this->event_model->getEventtypes();
		$data['event']			= $event;
		$data['venue']			= $this->venue_model->getById($event->venueid);
        $data['languages']      = $languages;
        $data['app']            = $app;
        $data['tags']           = $tags;
		$data['venues']			= $venues;
		$data['apptags']		= $apptags;
		$data['schedule']		= $schedule;
		$data['headers']		= $headers;
		$data['metadata']		= $metadata;

		$cdata['content'] 		= $this->load->view('c_event_edit', $data, TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("event" => $event), TRUE);
		if($launcherid == '') {
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Edit") => "event/edit/".$event->id);
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, __("Edit") => "event/edit/".$event->id);
			}
		} else {
			$this->load->model('module_mdl');
			$launcher = $this->module_mdl->getLauncherById($launcherid);
			$cdata['crumb']			= array($launcher->title => "events/launcher/".$launcherid, $event->name => "event/view/".$event->id.'/'.$launcherid, __('Edit') => 'event/edit/'.$event->id.'/'.$launcherid);
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'about');
		$this->load->view('master', $cdata);
	}

	function remove($eventid) {
		if($eventid == FALSE || $eventid == 0) redirect('events');
		$this->load->model('event_model');
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		$this->event_model->setActive($eventid, 0);
		$this->general_model->update('event', $eventid, array('deleted' => 1));
		redirect('events');
		die('remove Event');

		$this->general_model->update('app', $app->id, array('timestamp' => time()));
		// TODO remove event function
		// Get all data from the model to delete all coupled data!
		if($this->session->userdata('logged_in')) {
			//Secure this event before displaying!
			$organizerid = $this->session->userdata('id');

			$data = array('event' => $event);
			if($event->organizerid != $organizerid) {
			    redirect('/organizer/login/3/', 'location',301); //Redirect met foutmelding!
			}

			// end security
			$this->load->model('event_model');
			$current = $this->event_model->getbyid($eventid);
			if($current) {
				$this->load->model('venue_model');
				$this->load->model('map_model');
				$this->load->model('newsitem_model');
				$this->load->model('schedule_model');
				$this->load->model('exhibrand_model');
				$this->load->model('exhicat_model');
				$this->load->model('exhibitor_model');
				$this->load->model('poi_model');
				$this->load->model('exhibitorcategory_model');
				$this->load->model('exhibitorbrand_model');
				$this->load->model('appevent_model');
				//call appropriate methods -> DO NOT CHANGE ORDER!!!
				$this->venue_model->remove($current->venueid);                  //fixed!
				$this->map_model->removeByEventId($eventid);                    //fixed!
				$this->newsitem_model->removeByEventId($eventid);               //fixed!
				$this->schedule_model->removeByEventId($eventid);               //fixed!
				$currentexhibitors = $this->exhibitor_model->getbyeventid($eventid);
				if($currentexhibitors) {
					foreach($currentexhibitors as $cexhi) {
						$this->exhibrand_model->deleteExhibitorBrands($cexhi->id);
						$this->exhicat_model->removeExhibitorCats($cexhi->id);
					}
				}
				$this->exhibitor_model->removeByEventId($eventid);              //fixed!
				$this->poi_model->removeByEventId($eventid);                    //fixed!
				$this->exhibitorbrand_model->removeByEventId($eventid);         //fixed!
				$this->exhibitorcategory_model->removeByEventId($eventid);      //fixed!
				$this->appevent_model->removeByEventId($eventid);               //fixed!
				$this->event_model->remove($eventid);                           //fixed!
				redirect('/organizer/', 'location',301);
			}
		} else {
			// Not Logged in!
			$this->session->set_userdata('login_redirect', '/organizer/');
			redirect('/organizer/login', 'location',301);
		}
	}

	function removemap($mapid = 0, $eventid = 0) {
		if($mapid == FALSE || $mapid == 0 || $eventid == FALSE || $eventid == 0) redirect('events');
		$this->load->model('event_model');
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->load->model('map_model');
		$map = $this->map_model->getMapsById($mapid);
		if($map == FALSE) redirect('events');
		if(!file_exists($this->config->item('imagespath') . $map->imageurl)) redirect('events');
		// Delete image + generated thumbs

		if($this->db->delete('map', array('id' => $map->id))){
			//remove items
			$this->map_model->removeItemsOfMap($map->id, 'event', $eventid);

			unlink($this->config->item('imagespath') . $map->imageurl);
			array_map("unlink", glob("cache/*".end(explode('/', $map->imageurl))));
			array_map("unlink", glob("cache/maps/*".end(explode('/', $map->imageurl))));
			$this->session->set_flashdata('event_feedback', __('Floorplan has successfully been removed.'));
		} else {
			$this->session->set_flashdata('event_feedback', __('Failed remove floorplan.'));
		}

		_updateTimeStamp($eventid);
		redirect('event/view/'.$eventid);

	}

	function view($id, $launcherid = '') {
		if($id == FALSE || $id == 0) redirect('events');
		$this->iframeurl = "event/index/" . $id;

		$event = $this->event_model->getbyid($id);

		$app = _actionAllowed($event, 'event','returnApp');

		// Modules ophalen voor eventtype
		$this->load->model('module_mdl');
		$eventmodules = $this->module_mdl->getModulesByType($event->eventtypeid, $event->id, $app->apptypeid);

		$launchers = $this->module_mdl->getAllLaunchersOfEvent($id);//  echo '<pre>'; print_r($launchers); exit;

		$dynamicModules = $this->module_mdl->getDynamicModules($id, 'event');

		$this->session->set_userdata('eventid', $id);
		$this->session->set_userdata('appid', $app->id);

		$subflavors = $this->module_mdl->getSubflavorsOfFlavor($app->apptypeid);

		$eventlaunchers = array();
		if($app->familyid == 3) {
			$eventlaunchers = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND moduletypeid = 30")->result();
		}

		$cdata['content'] 		= $this->load->view('c_event', array('event' => $event, 'eventmodules' => $eventmodules, 'launchers' => $launchers, 'app' => $app, 'dynamicModules' => $dynamicModules, 'subflavors' => $subflavors, 'eventlaunchers' => $eventlaunchers, 'launcherid' => $launcherid), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("event" => $event), TRUE);
		if($launcherid == '') {
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id);
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id);
			}
		} else {
			$this->load->model('module_mdl');
			$launcher = $this->module_mdl->getLauncherById($launcherid);
			$cdata['crumb']			= array($launcher->title => "events/launcher/".$launcherid, $event->name => 'event/view/'.$event->id.'/'.$launcherid);
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		}

		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, '');
		$this->load->view('master', $cdata);
	}

	function active($id, $status) {
		$this->load->model('event_model');
		$event = $this->event_model->getbyid($id);

		# Validate access
		// if($event->organizerid != _currentUser()->organizerId) redirect('events');
		_actionAllowed($event, 'event');

		if($status == 'on' && $id > 0) {
			$this->event_model->setActive($id, 1);
			_updateTimeStamp($id);
		} else if($status == 'off' && $id > 0) {
			$this->event_model->setActive($id, 0);
			_updateTimeStamp($id);
		} else {
			//show_404('page');
		}

		if ($this->session->userdata('prev_page') != '') {
			header(__('Location:').$this->session->userdata('prev_page'));
		} else {
			redirect('event/view/'.$id);
		}
	}

	function module($trigger= '', $eventid, $moduleid) {
		$this->load->model('event_model');
		$event = $this->event_model->getbyid($eventid);

		$app = _actionAllowed($event, 'event', 'returnApp');

		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.manage', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to manage Applications', 'error');
			redirect('apps/view/'.$app->id);
		}

		switch($trigger){
			case 'activate':
				$groupid = 0;
				if($moduleid == 54) {
					$groupid = $this->module_mdl->checkPlacesCats($eventid, 'event', $app);
				} elseif($moduleid == 14) {
					$groupid = $this->module_mdl->checkAttendeesCats($eventid, 'event', $app);
				}

				if($moduleid == 10) {
					//activate speakers with sessions
					if($app->apptypeid == 10) {
						$this->module_mdl->insertFestivalLaunchers($eventid);
					}
					
					$groupid = $this->module_mdl->checkSpeakersCats($eventid, 'event', $app);
					$this->module_mdl->addModuleToEvent($eventid, 40, $groupid);
				}

				// Add module to event
				if($moduleid == '18' && $app->apptypeid == 6) {
					$app->module_mdl->addModuleToApp($app->id,$moduleid);
				} else{
					$this->module_mdl->addModuleToEvent($eventid, $moduleid, $groupid);
				}

				if($moduleid == 69) {
					$this->load->model('quiz_model');
					$this->quiz_model->checkModule($eventid, 'event', $app);
				}

				if($moduleid == 49) {
					$this->module_mdl->checkUserModuleSettings($app->id);
				}
				
				// if($moduleid == '20') {
				// 	$this->load->library('email');

				// 	$this->email->from('apps@tapcrowd.com', 'TapCrowd');
				// 	$this->email->to('info@tapcrowd.com');

				// 	$this->email->subject('Push notification certificate');
				// 	$this->email->message('Push notification module geactiveerd voor app ' . $app->name . ' met id: ' . $app->id . '.
				// 							De module werd geactiveerd via eventid:' . $eventid . '. Controleer of reeds een certificaat bestaat en maak indien nodig één aan voor deze app.
				// 							Het emailadres van de user is: '. _currentUser()->email);

				// 	$this->email->send();
				// }
				break;
			case 'deactivate':
				if($moduleid == 10) {
					//activate speakers with sessions
					$this->module_mdl->removeModuleFromEvent($eventid, 40);
				}
				// TODO Remove module from event
				if($moduleid == '18'&& $app->apptypeid == 6) {
					$this->module_mdl->removeModuleFromApp($app->id,$moduleid);
				} else{
					$this->module_mdl->removeModuleFromEvent($eventid, $moduleid);
				}
				if($moduleid == 28) {
					$this->load->model('social_model');
					$this->social_model->removeFromEvent($eventid);
				}

				break;
			default:
				break;
		}
		_updateTimeStamp($eventid);
		redirect('event/view/'.$eventid);
	}

	function travelinfo($id) {
		if($id == FALSE || $id == 0) redirect('events');
		$this->iframeurl = "event/info/" . $id;

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$venue = $this->venue_model->getById($event->venueid);
		if($venue == FALSE) redirect('events');

		$error = '';
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$this->load->library('form_validation');
		if($this->input->post('postback') == "postback") {
			if($languages != null) {
			foreach($languages as $language) {
				$this->form_validation->set_rules('txt_travelinfo_'.$language->key, 'txt_travelinfo_'.$language->key, 'trim');
			}
			}

			if($this->form_validation->run() == FALSE){
				$error = "Some fields are missing.";
			} else {
				// Save exhibitor
				$res = $this->general_model->update('venue', $event->venueid, array("travelinfo" => $this->input->post('txt_travelinfo_'.$app->defaultlanguage)));
				if($res != FALSE){
					if($languages != null) {
						foreach($languages as $language) {
							$travelSuccess = $this->language_model->updateTranslation('venue', $event->venueid, 'travelinfo', $language->key, $this->input->post('txt_travelinfo_'.$language->key));
							if(!$travelSuccess) {
								$this->language_model->addTranslation('venue', $event->venueid, 'travelinfo', $language->key, $this->input->post('txt_travelinfo_'.$language->key));
							}
						}
					}
					$this->session->set_flashdata('event_feedback', __('Travel info has successfully been set!'));
					redirect('event/view/'.$id.'/'.$launcherid);
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}

			}
		}

		$cdata['content'] 		= $this->load->view('c_venue_travel', array('event' => $event, 'venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("event" => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Travel info") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, __("Travel info") => $this->uri->uri_string());
		}
		$this->load->view('master', $cdata);
	}

	function autocompletetags() {
		//In controller
		$keyword = $this->input->post('term');

		$data['response'] = 'false'; //Set default response

		$query = _autocompletetags(_currentApp()->id, $keyword);
		$keys = array();
		if(count($query) > 0){
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach($query as $row){
				if(!in_array($row->tag,$keys)) {
					$data['message'][] = array('label'=> $row->tag, 'value'=> $row->tag); //Add a row to array
					$keys[] = $row->tag;
				}
			}
		}
		echo json_encode($data);
	}

	function removeimage($eventid = 0) {
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		if(!file_exists($this->config->item('imagespath') . $event->eventlogo)) redirect('events');

		// Delete image + generated thumbs

		unlink($this->config->item('imagespath') . $event->eventlogo);
		$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

		$data = array('eventlogo' => '');
		$this->general_model->update('event', $eventid, $data);

		_updateTimeStamp($eventid);
		redirect('event/view/'.$eventid);
	}

	function addtag() {
		$eventid = $this->input->post('eventid');

		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		//launcheritem
		$tag = $this->input->post('tag');
		$launcherexists = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND (moduletypeid = 31 OR moduletypeid = 30) AND (title = ? OR tag = ?)", array($tag, $tag));
		if($launcherexists->num_rows() > 0) {
			$data = array(
				'appid'		=> $app->id,
				'eventid'	=> $eventid,
				'tag'		=> $launcherexists->row()->tag,
				'active'	=> 1
			);
			$this->general_model->insert('tag', $data);
		} else {
			$data = array(
				'appid'		=> $app->id,
				'eventid'	=> $eventid,
				'tag'		=> $tag,
				'active'	=> 1
			);
			$this->general_model->insert('tag', $data);

			$launcherdata = array(
				'appid'=>$app->id,
				'moduletypeid'=>30,
				'module'=>'eventTags',
				'title'=>$tag,
				'tag'=>$tag,
				'icon'=>'l_events',
				'active'=>1
			);
			if($app->id != 55) {
				$this->general_model->insert('launcher', $launcherdata);
			}
		}

		_updateTimeStamp($eventid);

		redirect('apps/view/'.$app->id);
	}

	function info($eventid) {
		$this->load->model('metadata_model');
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		if($app->apptypeid == 10) {
			redirect('festivalinfo/event/'.$event->id);
		}

		$metadata = $this->metadata_model->get($app->id, 'event', $eventid);

		$cdata['content'] 		= $this->load->view('c_festival_info', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'metadata' => $metadata), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("event" => $event), TRUE);

		if($app->familyid != 1) {
			$cdata['crumb']			= array("Events" => "events", $event->name => "event/view/".$event->id, "Info" => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, "Info" => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'about');
		$this->load->view('master', $cdata);
	}

	function changelauncher($eventid, $oldlauncherid, $launcherid) {
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->event_model->changelauncher($eventid, $oldlauncherid, $launcherid, $app->id);
	}

	function delete($eventid) {
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->event_model->remove($eventid);
		redirect('apps/view/'.$app->id);
	}

}
