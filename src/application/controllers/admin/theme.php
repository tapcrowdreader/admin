<?php if(!defined('BASEPATH')) exit('No direct script access');

class Theme extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(!_isAdmin()) { redirect('auth/logout'); }
		$this->load->model('theme_model');
		$this->load->model('appearance_model');
	}

	function index() {
		$themes = $this->theme_model->getAll();
		$icons = $this->theme_model->getAllIcons();
		foreach($themes as $t) {
			$t->countIcons = count($this->theme_model->getThemeIcons($t->id));
		}

		# Load header
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', array('pagetitle' => __('Themes')), true);

		$cdata['content'] 		= $this->load->view('admin/c_themes', array('themes' => $themes, 'countIcons' => count($icons)), TRUE);
		$this->load->view('master', $cdata);
	}

	function add() {
		$error = "";
		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('screenshot', 'screenshot', 'trim|required');

			$id = $this->general_model->insert('theme', array(
					"name" => $this->input->post("name"),
					"sortorder" => $this->input->post("sortorder")
				));

			$image = "";
			if(!is_dir($this->config->item('imagespath') . "upload/themeimages/".$id)){
				mkdir($this->config->item('imagespath') . "upload/themeimages/".$id, 0775,true);
			}
			$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/themeimages/'.$id;
			$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
			$configexlogo['max_width']  = '2000';
			$configexlogo['max_height']  = '2000';
			$this->load->library('upload', $configexlogo);
			$this->upload->initialize($configexlogo);
			if(!empty($_FILES['screenshot']['name'])) {
				if (!$this->upload->do_upload('screenshot')) {
					$error = $this->upload->display_errors();
					$image = '';
				} else {
					$returndata = $this->upload->data();
					$image = 'upload/themeimages/'. $id . '/' . $returndata['file_name'];
				}
				$this->general_model->update("theme", $id, array("screenshot" => $image));	
			}


			if($error == "") {
				redirect("admin/theme");
			}
		}

		# Load header
		$params = array('pagetitle' => $theme->name, 'crumbs' => array('/admin/theme' => __('Themes')));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

		$cdata['content'] 		= $this->load->view('admin/c_theme_add', array('controls' => $controls, 'error' => $error), TRUE);
		$this->load->view('master', $cdata);
	}

	function edit($id) {
		$theme = $this->theme_model->getById($id);

		$controls = $this->appearance_model->getAllControls();
		$appearance = $this->theme_model->getAppearance($id);

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('screenshot', 'screenshot', 'trim|required');

			$islive = 0;
			if($this->input->post('islive') != false) {
				$islive = 1;
			}

			$this->general_model->update('theme', $id, array(
					"name" => $this->input->post("name"),
					"sortorder" => $this->input->post("sortorder"),
					"islive" => $islive
				));

			$image = $theme->screenshot;
			if(!is_dir($this->config->item('imagespath') . "upload/themeimages/".$id)){
				mkdir($this->config->item('imagespath') . "upload/themeimages/".$id, 0775,true);
			}
			$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/themeimages/'.$id;
			$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
			$configexlogo['max_width']  = '2000';
			$configexlogo['max_height']  = '2000';
			$this->load->library('upload', $configexlogo);
			$this->upload->initialize($configexlogo);
			if(!empty($_FILES['screenshot']['name'])) {
				if (!$this->upload->do_upload('screenshot')) {
					$error = $this->upload->display_errors();
					$image = $theme->screenshot;
				} else {
					$returndata = $this->upload->data();
					$image = 'upload/themeimages/'. $id . '/' . $returndata['file_name'];
				}
				$this->general_model->update("theme", $id, array("screenshot" => $image));	
			}


			$this->theme_model->removeValuesOfTheme($id);
			foreach($controls as $control) {
				if($control->appearancetype == 2) {
					$this->form_validation->set_rules($control->id.'_image', $control->id.'_image', 'trim');

					if(!is_dir($this->config->item('imagespath') . "upload/themeimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/themeimages/".$id, 0775,true);
					}

					if(isset($appearance[$control->id])) {
						$image = $appearance[$control->id]->value;
					} else {
						$image = '';
					}

					// $this->form_validation->set_rules($control->id.'_image', $control->id.'_image', 'trim');
					//if($this->input->post($control->controlid.'_image') != '') {
					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/themeimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$configexlogo['raw_name']  = $control->image_name . '@2x';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload($control->id.'_image')) {
						$error = $this->upload->display_errors();
					} else {
						$returndata = $this->upload->data();
						$image = 'upload/themeimages/'. $id . '/' . $returndata['file_name'];
					}
					$data = array(
						"themeid"	=> $id,
						"controlid"	=> $control->id,
						"value"		=> $image
					);

					$this->general_model->insert('themeappearance', $data);
				}
				if($control->appearancetype == 3) {
					if($this->input->post($control->id)) {
						$this->general_model->insert('themeappearance', array('themeid' => $id, 'controlid' => $control->id, 'value' => $this->input->post($control->id)));
					}
				}
			}

			$controls = $this->appearance_model->getAllControls();
			$appearance = $this->theme_model->getAppearance($id);
		}

		$themecontrols = $this->appearance_model->getControlsOfTheme($id);

		$themecontrolsArray = array();
		foreach($themecontrols as $c) {
			$themecontrolsArray[$c->controlid] = $c;
		}

		foreach($controls as $c) {
			if(isset($themecontrolsArray[$c->id])) {
				if($c->appearancetype == 3) {
					$c->defaultcolor = $themecontrolsArray[$c->id]->value;
				} elseif($c->appearancetype == 2) {
					$c->image_name = $this->config->item('publicupload') . $themecontrolsArray[$c->id]->value;
				}
			}
		}
		$theme = $this->theme_model->getById($id);

		# Load header
		$params = array('pagetitle' => $theme->name, 'crumbs' => array('/admin/theme' => __('Themes')));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

		$cdata['content'] 		= $this->load->view('admin/c_theme_edit', array('theme' => $theme, 'controls' => $controls, 'appearance' => $appearance), TRUE);
		$this->load->view('master', $cdata);
	}

	function icons($id) {
		$theme = $this->theme_model->getById($id);
		$icons = $this->theme_model->getAllIcons();
		$themeicons = $this->theme_model->getThemeIcons($id);
		$themeiconsArray = array();
		foreach($themeicons as $c) {
			$themeiconsArray[$c->moduletypeid] = $c;
		}

		if($this->input->post('postback') == "postback") {
			$this->theme_model->deleteIcons($id);
			foreach($icons as $i) {
				if(isset($themeiconsArray[$i->moduletypeid])) {
					$image = $themeiconsArray[$i->moduletypeid]->icon;
				} else {
					$image = '';
				}

				if(!empty($_FILES[$i->moduletypeid]['name'])) {
					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/themeimages/'.$id;
					$configexlogo['allowed_types'] = 'png';
					$configexlogo['max_width']  = '140';
					$configexlogo['max_height']  = '140';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if($this->upload->do_upload($i->moduletypeid)) {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/themeimages/'. $id . '/' . $returndata['file_name'];
					}
				}
				$data = array(
					"themeid"		=> $id,
					"moduletypeid"	=> $i->moduletypeid,
					"icon"		=> $image
				);
				if(!empty($image)) {
					$this->general_model->insert('themeicon', $data);
				}
			}
			$themeicons = $this->theme_model->getThemeIcons($id);
		}


		$themeiconsArray = array();
		foreach($themeicons as $c) {
			$themeiconsArray[$c->moduletypeid] = $c;
		}

		foreach($icons as $c) {
			if(isset($themeiconsArray[$c->moduletypeid])) {
				$c->icon = $this->config->item('publicupload') . $themeiconsArray[$c->moduletypeid]->icon;
			} else {
				$c->icon = 'http://m.tap.cr/public_html/themes/default/images/navigation/icons/all/' . $c->icon;
			}
		}

		# Load header
		$params = array('pagetitle' => __('Theme Icons'), 'crumbs' => array('/admin/theme' => __('Themes'), '/admin/theme/edit/'.$theme->id => $theme->name));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

		$cdata['content'] 		= $this->load->view('admin/c_theme_icons', array('theme' => $theme, 'icons' => $icons), TRUE);
		$this->load->view('master', $cdata);
	}

	function remove($id) {
		$this->theme_model->remove($id);
		redirect('admin/theme');
	}

	function removeimage($themeid, $controlid) {
		$themeappearance = $this->theme_model->getAppearanceOfThemeControl($themeid, $controlid);

		if(file_exists($this->config->item('imagespath') . $themeappearance->value)) {
			unlink($this->config->item('imagespath') . $themeappearance->value);
			if(file_exists($this->config->item('imagespath') . 'cache/'. $themeappearance->value)) {
				unlink($this->config->item('imagespath') . 'cache/'. $themeappearance->value);
			}
		} 

		$this->general_model->remove('themeappearance', $themeappearance->id);

		redirect('admin/theme/edit/'.$themeid);
	}

	function removeicon($themeid, $moduletypeid) {
		$themeappearance = $this->theme_model->getAppearanceIconOfThemeControl($themeid, $moduletypeid);

		if(file_exists($this->config->item('imagespath') . $themeappearance->icon)) {
			unlink($this->config->item('imagespath') . $themeappearance->icon);
			if(file_exists($this->config->item('imagespath') . 'cache/'. $themeappearance->icon)) {
				unlink($this->config->item('imagespath') . 'cache/'. $themeappearance->icon);
			}
		} 

		$this->general_model->remove('themeicon', $themeappearance->id);

		redirect('admin/theme/icons/'.$themeid);
	}
}