<?php if(!defined('BASEPATH')) exit('No direct script access');

use \Tapcrowd\Model\Account;
use \Tapcrowd\API;
use \App_model;

class Apps extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if(!_isAdmin()) redirect();
	}

	public function index($channelid = 0)
	{
		$this->_renderOutput( __('Administration'), null, $data);
	}

	public function browse()
	{
		# Get apps
		$channel = \Tapcrowd\API::getCurrentChannel();

		$param = '';
		$key = '';
		if(isset($_GET['q']) && isset($_GET['filter'])) {
			$param = filter_var($_GET['q'], FILTER_SANITIZE_STRING);
			$key = filter_var($_GET['filter'], FILTER_SANITIZE_STRING);
			if($key == 'name') {
				$key = 'app.name';
			} elseif ($key == 'accountname') {
				$key = 'tc_accounts.fullname';
			} elseif ($key == 'email') {
				$key = 'tc_accounts.email';
			}
		}

		$data = array(
			'channel' => $channel,
			'analytics_url' => \Tapcrowd\API::getLocation('actions?appid=%d&h=%s&frame=false','analytics'),
			'apps' => \Tapcrowd\API::listChannelApplications($channel, null, 0, 100, array($key => $param))
		);

		# Load & Display views
// 		$params = array('pagetitle' => __('Applications'), 'crumbs' => array());
// 		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);
// 		$cdata['content'] = $this->load->view('admin/c_admin_apps.php', $data, true);
// 		$this->load->view('master', $cdata);

		$this->_renderOutput( __('Applications'), 'admin/c_admin_apps.php', $data);
	}

	public function analytics( $appId = null )
	{
		if(empty($appId)) redirect('/admin');

		try {
			$app =  \Tapcrowd\API::getApplication( $appId );
			$path = sprintf('actions?appid=%d&h=%s&frame=true', $app->id, md5('TCAnalytics' . $app->id));
			$analytics_url = \Tapcrowd\API::getLocation($path,'analytics');
			$analytics_url = str_replace('tom', 'staging', $analytics_url);
			$content = '<iframe style="margin-left: -7px; width: 1020px; min-height: 770px;" src="'.$analytics_url.'"></iframe>';
		} catch(\Exception $e) {
			$content = 'No app found';
		}

		# Load & Display views
		$params = array('pagetitle' => __('Analytics'), 'crumbs' => array());
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);
		$cdata['content'] = $content;
		$this->load->view('master', $cdata);
	}

	/**
	 * Displays app permissions
	 *
	 * @param int $appId
	 * @return void
	 */
	public function permissions( $appId )
	{
		# Get app
		try {
			$app = \Tapcrowd\API::getApplication( (int)$appId );
			$perms = $this->app_model->getPermissions($app);
			$account_model = Tapcrowd\Model\Account::getInstance();
		} catch(\Exception $e) {
			$this->_renderOutput( __('Error'), null, array('error' => $e));
			return;
		}

		$request_method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');

		# POST
		if($request_method == 'POST') {

			$accountId = filter_input(INPUT_POST, 'account_id');
			$entity = filter_input(INPUT_POST, 'entity');
			$entityId = filter_input(INPUT_POST, 'entity_id');
			$action = filter_input(INPUT_POST, 'action');

			# Get account
			try {
				$account = $account_model->getAccount($accountId);
				if($action == 'add') {
					$this->app_model->addPermissions($app, $account);
				} else {
					$this->app_model->removePermissions($app, $account);
				}
			} catch(\Exception $e) {
				$this->_renderOutput( __('Error'), null, array('error' => $e));
			}

			header('HTTP/1.1 303 See Other');
			header('Location: ' . site_url('/admin/apps/permissions/' . $appId));
			exit;
		}

		# GET
		elseif($request_method == 'GET') {

			# Get App permissions
			$data = array('app' => $app, 'perms' => $perms);
			try { # Create data-source string for the account list
				$accounts = $account_model->getAccountsForChannel( $data['app']->channel );
				$data['data_source'] = htmlentities(json_encode(array_map(function($n)
				{
					return $n->accountId.'|'.$n->fullname . ' #' . $n->accountId;
				},$accounts)));
			} catch(\Exception $e) {
				$data['error'] = $e;
			}
			$this->_renderOutput( __('Permissions'), 'admin/c_app_permissions.php', $data);
		}

		else {
			header('HTTP/1.1 405 Method Not Allowed');
			echo '<h1>405 Method Not Allowed</h1>';
			exit;
		}
	}

	/**
	 * Render the output (protected) supports: frame loading (no header etc...)
	 * json encoded output (only $params input is returned).
	 *
	 * @param string $pagetitle
	 * @param string $view
	 * @param array $params
	 * @return void
	 */
	protected function _renderOutput( $pagetitle, $view = null, $params = null, $crumbs = null )
	{
		$content = $view? $this->load->view($view, $params, true) : '';
		$accept = filter_input(INPUT_SERVER, 'Accept');
		$frame = filter_input(INPUT_GET, 'frame');

		if(!is_array($crumbs)) $crumbs = array();
		$header = array('pagetitle' => $pagetitle, 'crumbs' => $crumbs);
		if(isset($params['error'])) $header['error'] = $params['error'];

		if($accept == 'application\json') {
			header('Content-Type: application\json');
			echo json_encode($params);
			exit;
		}
		elseif($frame !== null) echo $content;
		else {
			$pageheader = $this->load->view('admin/c_admin_header', $header, true);
			$this->load->view('master', array('pageheader' => $pageheader, 'content' => $content));
		}
	}
}
