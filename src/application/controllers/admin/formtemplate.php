<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Formtemplate extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		if(!_isAdmin()) redirect('apps');
        $this->load->model('admin/form_template');
        $this->load->model('forms_model');
        $this->load->model('general_model');
        $this->load->model('theme_model');
	}

	//php 4 constructor
	function Formtemplate() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('admin/form_template');
		$this->load->model('forms_model');
		$this->load->model('general_model');
	}

	function index(){
    	redirect('admin/formtemplate/forms');
    }

	function forms() {
		// Get Form Templates
    	$forms = $this->form_template->getFormTemplates();
        $headers = array(__('Title') => 'title', 'Order' => 'order');

        // if single screen form - get default screen id and set redirecturl to controls screen
        foreach($forms as $key => $value):
        	$this->db->select('id');
        	$this->db->where('formid', $forms[$key]->id);
            $q = $this->db->get('templateformscreen');
            $row = $q->result();
            $redirecturl    = 'admin/templatescreens/form/'.$forms[$key]->id;
            if($forms[$key]->singlescreen)
            {
            	$redirecturl    = 'admin/templatefields/formscreen/'.$row[0]->id;
                $ask_multiple   = 0;
            }
            else if($row[0]->id)
            {
            	$redirecturl    = 'admin/templatescreens/form/'.$forms[$key]->id;
                $ask_multiple   = 0;
			}
			$forms[$key]->redirecturl = $redirecturl;
		endforeach;

		$childcon = "admin/templatescreens";
		$confunc = "form";

		# Load header
		$params = array('pagetitle' => __('Form Templates'), 'crumbs' => array());
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

		$cdata['content'] 		= $this->load->view('admin/c_form_template_listview', array('data' => $forms, 'headers' => $headers, 'childcon' => $childcon, 'confunc' => $confunc, 'single_scr_redirect' => $redirecturl, 'ask_multiple' => $ask_multiple), TRUE);
// 		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= array(__("Form Templates") => "admin/formtemplate/");

		$this->load->view('master', $cdata);

	}
	
	function add() {
	
	$apptypes = $this->form_template->getAppTypes();
	$totalTypes = count($apptypes);
	
	for($i=0; $i<$totalTypes; $i++):
		$subflavors = $this->form_template->getSubFlavorsByAppTypeId($apptypes[$i]->id);
        $apptypes[$i]->subflavor = $subflavors;
	endfor;

	$error = "";
	$singlescreen = 1;
	if($this->input->post('postback') == "postback") {
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('submissionbuttonlabel', 'Submit Button Label', 'trim|required');
		$this->form_validation->set_rules('submissionconfirmationtext', 'Submission Confirmation Text', 'trim');
		$this->form_validation->set_rules('order', 'order', 'trim|numeric');
		$this->form_validation->set_rules('apptype', 'App Type', 'callback_arrayCountCheck');

		$singlescreen = $this->input->post('multiscreen');

		if($this->form_validation->run() == FALSE){
			$error = validation_errors();
		} else {
			if($error == '') {
				$maxId = 0;
				
				$this->db->select_max('id');
				$res = $this->db->get('moduletype');
				if( $res->num_rows() > 0 ) $maxId = $res->row()->id + 1;
				
				// Adding New moduletype
				$mData = array();
				$mData['id']			= $maxId;
				$mData['name']			= trim($this->input->post('title'));
				$mData['controller']	= 'forms';
				$mData['apicall']		= 'getForms';
				$mData['component']		= str_replace(' ', '', $mData['name']);

				$this->general_model->insert('moduletype', $mData);
				$moduleTypeId = $maxId;

				$data = array();
				$data['title']                      = trim($this->input->post('title'));
				$data['order']                      = ( $this->input->post('order') == '' )? 0 : $this->input->post('order');;
				$data['submissionbuttonlabel']      = trim($this->input->post('submissionbuttonlabel'));;
				$data['submissionconfirmationtext'] = trim($this->input->post('submissionconfirmationtext'));;
				$data['emailsendresult']            = ( $this->input->post('emailsendresult') == 'no' ) ? 'no' : 'yes';
				$data['allowmutliplesubmissions']   = ( $this->input->post('allowmutliplesubmissions') == 'no' ) ? 'no' : 'yes';
				$data['getgeolocation']             = ( $this->input->post('geolocation') == '' ||  $this->input->post('geolocation') != 1 )? 0 : $this->input->post('geolocation');
				$data['moduletypeid']				= $moduleTypeId;
				$data['singlescreen']				= $singlescreen;

				$formId = $this->general_model->insert('templateform', $data);

				if($formId){

					// Adding Default Launcher
					$this->general_model->insert('defaultlauncher', array(
						'moduletypeid'		=> $data['moduletypeid'],
						'module'			=> 'forms',
						'title'				=> $data['title'],
						'icon'				=> 'icon-checklist',
						'displaytype'		=> '',
						'order'				=> 0,
						'url'				=> '',
						'tag'				=> '',
						'topurlevent'		=> 'forms/event/--eventid--/--launcherid--',
						'mobileurlevent'	=> 'forms/event/--eventid--/--launcherid--',
						'topurlvenue'		=> 'forms/venue/--venueid--/--launcherid--',
						'mobileurlvenue'	=> 'forms/venue/--venueid--/--launcherid--',
						'mobileurlapp'		=> '',
						'topurlapp'			=> '',
						'groupid'			=> 0
					));

					//Add theme icons
					$themes = $this->theme_model->getAll();
					foreach($themes as $t) {
						$formicon = $this->theme_model->getThemeFormIcon($t->id);
						if(!empty($formicon)) {
							$this->general_model->insert('themeicon', array(
								'themeid' => $t->id,
								'moduletypeid' => $data['moduletypeid'],
								'icon' => $formicon->icon
								));
						}
					}
					
					// --------- Adding into eventtypemoduletype					
					$appFamilyIds = array();
					$appInClz = implode(',', $this->input->post('apptype'));
					$qry = 'SELECT id, familyid FROM apptype WHERE id IN ( ' . $appInClz . ' )';
					$res = $this->db->query($qry);
					if( $res->num_rows() )
						$appFamilyIds = $res->result();
					
					$ETMTdata['moduletype'] = $moduleTypeId;
					
					$qry = 'SELECT id FROM eventtypemoduletype WHERE moduletype = ' . $moduleTypeId . ' AND ';
					
					foreach( $appFamilyIds as $familyRow ){
						$whr = '';
						if( $familyRow->familyid == 1 ){ $ETMTdata['eventtype'] = 3; $ETMTdata['venueid'] = 0; $ETMTdata['appid'] = 0; $whr = 'eventtype = 3'; }
						elseif( $familyRow->familyid == 2 ) { $ETMTdata['venueid'] = 1; $ETMTdata['eventtype'] = 0; $ETMTdata['appid'] = 0; $whr = 'venueid = 1'; }
						else { $ETMTdata['appid'] = 1; $ETMTdata['venueid'] =0; $ETMTdata['eventtype'] = 0; $whr = 'appid = 1'; }
						
						$ETMTid = $this->db->query( $qry . $whr )->row()->id;
					
						if(!$ETMTid){
							$this->general_model->insert('eventtypemoduletype', $ETMTdata);
						}
					
					}
					
					// -----------------------
					
					foreach($this->input->post('apptype') as $type):
						$frmApp = array();
						$frmApp['templateformid']   = $formId;
						$frmApp['apptypeid']        = $type;
						$frmApp['subflavorid']      = $this->input->post('appsubflavor_'.$type);

						$formAppId = $this->general_model->insert('templateformapp', $frmApp);
						
						// Linking new moduletype with subflavors
						$this->general_model->insert('moduletype_subflavor', array('moduletypeid' => $moduleTypeId, 'subflavorid' => $this->input->post('appsubflavor_'.$type)));

					endforeach;

					// ---------- Add Screen -----------------//
					if($singlescreen == 1):
						$this->general_model->insert('templateformscreen', array(
						'formid'	=> $formId,
						'title'		=> 'Default Screen',
						'order'		=> 0
						));

					endif;

					$this->session->set_flashdata('event_feedback', __('The form template has successfully been add!'));
					redirect('admin/formtemplate/forms');

				}else{
					$error = __("Oops, something went wrong. Please try again.");
				}
			}
		}
	}

		# Load header
		$params = array('pagetitle' => __('Add Template'), 'crumbs' => array('/admin/formtemplate/forms' => __('Form Templates')));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

	$cdata['content'] 		= $this->load->view('admin/c_form_template_add', array('error' => $error, 'apptypes' => $apptypes, 'singlescreen' => $singlescreen), TRUE);
// 	$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
	$cdata['crumb']		= array(__("Form Templates") => "admin/formtemplate/forms", __("Add new") => $this->uri->uri_string());
	$this->load->view('master', $cdata);

	}

	function edit($id) {
		$forms      = $this->form_template->getFormTemplates($id);
		$formApps   = $this->form_template->getFormTemplateApps($id);
		$singlescreen = $forms[0]->singlescreen;

		$moduleTypeId = $forms[0]->moduletypeid;
		$apptypes = $this->form_template->getAppTypes();

		$alreadyExistsSubFlavors = array();

		$totalTypes = count($apptypes);
		for($i=0; $i<$totalTypes; $i++):
			$subflavors = $this->form_template->getSubFlavorsByAppTypeId($apptypes[$i]->id);
			$apptypes[$i]->subflavor = $subflavors;
			if(array_key_exists($apptypes[$i]->id, $formApps))
			{
				$apptypes[$i]->checked = 1;
				$apptypes[$i]->subflavor['selected'] = $formApps[$apptypes[$i]->id];
				$alreadyExistsSubFlavors[] = $apptypes[$i]->subflavor['selected'];
			}
		endfor;

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Title', 'trim|required');
			$this->form_validation->set_rules('submissionbuttonlabel', 'Submit Button Label', 'trim|required');
			$this->form_validation->set_rules('submissionconfirmationtext', 'Submission Confirmation Text', 'trim');
			$this->form_validation->set_rules('order', 'order', 'trim|numeric');
			//$this->form_validation->set_rules('apptype', 'App Type', 'callback_arrayCountCheck');

			$singlescreen = $this->input->post('multiscreen');

			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {

				if($error == '') {

					$data = array();
					$data['title']                      = trim($this->input->post('title'));
					$data['order']                      = ( $this->input->post('order') == '' )? 0 : $this->input->post('order');;
					$data['submissionbuttonlabel']      = trim($this->input->post('submissionbuttonlabel'));;
					$data['submissionconfirmationtext'] = trim($this->input->post('submissionconfirmationtext'));;
					$data['emailsendresult']            = ( $this->input->post('emailsendresult') == 'no' ) ? 'no' : 'yes';
					$data['allowmutliplesubmissions']   = ( $this->input->post('allowmutliplesubmissions') == 'no' ) ? 'no' : 'yes';
					$data['getgeolocation']             = ( $this->input->post('geolocation') == '' ||  $this->input->post('geolocation') != 1 )? 0 : $this->input->post('geolocation');
					$data['singlescreen']				= $singlescreen;


					if($this->forms_model->edit_table($id, $data,'templateform')){
						
						//------- Updating moduletype table 
						$this->db->where('id', $moduleTypeId);
						$this->db->update('moduletype', array('name' => $data['title']));
						
						//------- Updating defaultlauncher table 
						$this->db->where('moduletypeid', $moduleTypeId);
						$this->db->update('defaultlauncher', array('title' => $data['title']));
						
						// --------- Adding into eventtypemoduletype					
						$appFamilyIds = array();
						$appInClz = implode(',', $this->input->post('apptype'));
						if( $appInClz ) {
							$qry = 'SELECT id, familyid FROM apptype WHERE id IN ( ' . $appInClz . ' )';
							$res = $this->db->query($qry);
							if( $res->num_rows() )
								$appFamilyIds = $res->result();
							
							$ETMTdata['moduletype'] = $moduleTypeId;
							
							$qry = 'SELECT id FROM eventtypemoduletype WHERE moduletype = ' . $moduleTypeId . ' AND ';
							
							foreach( $appFamilyIds as $familyRow ){
								$whr = '';
								if( $familyRow->familyid == 1 ){ $ETMTdata['eventtype'] = 3; $ETMTdata['venueid'] = 0; $ETMTdata['appid'] = 0; $whr = 'eventtype = 3'; }
								elseif( $familyRow->familyid == 2 ) { $ETMTdata['venueid'] = 1; $ETMTdata['eventtype'] = 0; $ETMTdata['appid'] = 0; $whr = 'venueid = 1'; }
								else { $ETMTdata['appid'] = 1; $ETMTdata['venueid'] =0; $ETMTdata['eventtype'] = 0; $whr = 'appid = 1'; }
								
								$ETMTid = $this->db->query( $qry . $whr )->row()->id;
							
								if(!$ETMTid){
									$this->general_model->insert('eventtypemoduletype', $ETMTdata);
								}
							
							}
						}
						// -----------------------
						
						
						
						
						foreach($this->input->post('apptype') as $type):
							if(!in_array($this->input->post('appsubflavor_'.$type), $alreadyExistsSubFlavors)){
								$frmApp = array();
								$frmApp['templateformid']   = $id;
								$frmApp['apptypeid']        = $type;
								$frmApp['subflavorid']      = $this->input->post('appsubflavor_'.$type);

								$formAppId = $this->general_model->insert('templateformapp', $frmApp);
								$this->general_model->insert('moduletype_subflavor', array('moduletypeid' => $moduleTypeId, 'subflavorid' => $this->input->post('appsubflavor_'.$type)));
							}
						endforeach;

						$this->session->set_flashdata('event_feedback', __('The form template has successfully been update!'));
						redirect('admin/formtemplate/forms');

					}else{
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

		}

		# Load header
		$params = array('pagetitle' => __('Edit Form Template'), 'crumbs' => array('/admin/formtemplate/forms' => __('Form Templates')));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

		$cdata['content'] 		= $this->load->view('admin/c_form_template_edit', array('form' => $forms, 'error' => $error, 'apptypes' => $apptypes, 'singlescreen' => $singlescreen), TRUE);
// 		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']		= array(__("Form Templates") => "admin/formtemplate/forms", __("Edit ".$forms[0]->title) => $this->uri->uri_string());
		$this->load->view('master', $cdata);

	}

	function arrayCountCheck($str){

		if(count($str) == 0)
		{
			$this->form_validation->set_message('arrayCountCheck', 'Please select atleast one %s.');
			return false;
		}

		return true;
	}

	function remove($id) {
		$this->form_template->removeFormTemplate($id);
		redirect('admin/formtemplate/forms');
	}

}
?>