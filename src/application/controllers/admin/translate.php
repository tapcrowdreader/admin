<?php if(!defined('BASEPATH')) exit('No direct script access');

/**
 * Localisation Administration controller
 */
class Translate extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if(!_isAdmin()) { redirect('auth/logout'); }

		# Load the tranlation model
		$this->load->model('translation_model');
	}

	public function index()
	{
		$this->browse();
	}

	/**
	 * Listing of the translation data; With optional filters
	 */
	public function browse()
	{
		$languages = $this->translation_model->getLanguages();
		$sources = $this->translation_model->getSourceTypes();
		$error = null;

		# Catch Import requests
		if( $this->input->post('postback') == "importlocalization" ) {
			if ( $this->translation_model->import('langfile') ){
				redirect('admin/translate');
			}else{
				$error = new \RuntimeException(__('File not found'));
			}
		}

		# Source filter
		$source = filter_input(INPUT_GET, 'source');
		if(!in_array($source, $sources)) $source = $sources[0];

		# Languages filter
		$filteredLanguages = array();
		foreach($languages as $l) {
			if(filter_input(INPUT_GET, $l->language)) {
				array_push($filteredLanguages, $l);
			}
		}
		if(empty($filteredLanguages)) $filteredLanguages =& $languages;

		# Get data
		try {
			$data = $this->translation_model->getLocalization($filteredLanguages, $source);
		} catch(\Exception $error) {
			//
		}

		# Render output
		$headers = array('Key' => 'k');
		foreach($filteredLanguages as $l) {
			$headers[$l->language] = $l->language;
		}
		$headers['count'] = 'lineCount';
		$headers['edit'] = 'edit';

		# Render
		$params = array(
			'headers' => $headers,
			'data' => $data,
			'languages' => $languages,
			'sources' => $sources,
			'filteredLanguages' => $filteredLanguages,
			'selectedSource' => $source,
			'error' => $error
		);
		$this->_renderOutput( __('Translations'), 'admin/c_translate_overview', $params);
	}

	/**
	 * Edit interface
	 *
	 * @param int $key The checksum
	 */
	public function edit( $source, $key )
	{
		# Get languages, sources
		$languages = $this->translation_model->getLanguages();
		$sources = $this->translation_model->getSourceTypes();

		# Validate, get translation or output errors
		try {
			if(empty($key) || !is_numeric($key)) throw new \InvalidArgumentException(__('Invalid key: %s', $key));
			if(!in_array($source, $sources)) throw new \InvalidArgumentException(__('Invalid source: %s', $source));

			$translations = $this->translation_model->getLocalization( $languages, $source, array($key));
			$translation = $translations[0];
		} catch(\Exception $error) {
			$this->_renderOutput( 'Error', 'admin/c_translate_edit', array('error'=>$error));
			return;
		}

		$request_method = filter_input(INPUT_SERVER,'REQUEST_METHOD');

		# POST : Save translations
		if($request_method == 'POST') {

			$translations = array();
			foreach($languages as $lang) {
				$translations[$lang->language] = filter_input(
					INPUT_POST, 'translation_' . $lang->language,
					FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW
				);
			}

			try {
				$this->translation_model->saveTranslation($source, $key, $translations);
				redirect('admin/translate?source='.$source);
			} catch(\Exception $error) {
				$this->_renderOutput( 'Error', 'admin/c_translate_edit', array('error'=>$error));
				return;
			}
		}

		# GET : Render form
		elseif($request_method == 'GET') {
			$params = array('key' => $key, 'translation' => $translation, 'languages' => $languages);
			$this->_renderOutput( $key, 'admin/c_translate_edit', $params);
		}
		else {
			header('HTTP/1.1 405 Method Not Allowed');
			exit;
		}
	}

	/**
	 * Render the output (protected)
	 *
	 * @param string $pagetitle
	 * @param string $view
	 * @param array $params
	 * @return void
	 */
	protected function _renderOutput( $pagetitle, $view, $params )
	{
		$content = $this->load->view($view, $params, true);
		if(filter_input(INPUT_GET, 'frame')) echo $content;
		else {
			$params = array('pagetitle' => $pagetitle, 'crumbs' => array('/admin/translate' => __('Translations')));
			$pageheader = $this->load->view('admin/c_admin_header', $params, true);
			$this->load->view('master', array('pageheader' => $pageheader, 'content' => $content));
		}
	}

	function add() {
		$languages = $this->translation_model->getLanguages();
		if ($this->input->post('postback') == "postback" && $this->input->post('translationKey')) {
			$mykey = $this->input->post('translationKey');
			foreach ($languages as $lang) {
				if ($this->input->post('translation' . $lang->language)) {
					$sql = 'INSERT INTO localization (k, language, v, country, channel,id) VALUES (?, ?, ?, ?, ?, ?)';
					$query = $this->db->query($sql, array($mykey, $lang->language, trim($this->input->post('translation' . $lang->language)), '',0, crc32($mykey)));
				}
			}
		} else {
			$params = array('pagetitle' => __('Add Text'), 'crumbs' => array('/admin/translate' => __('Translations')));
			$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);
			$cdata['content']		= $this->load->view('admin/c_translate_new', array('languages' => $languages), TRUE);

			if(filter_input(INPUT_GET, 'frame')) echo $cdata['content'];
			else $this->load->view('master', $cdata);
		}
	}

	function export($source){
		$source = ( trim($source) == 'ios' ) ? 'ios' : 'android';


		$translations = $this->translation_model->getTranslations( $source );
		$translations = $translations->getIterator();
// 		$translations = $this->translation_model->exportToZip($source);

		$zip = new ZipArchive();
		$zip_name = "export_$source" . date('Y-m-d') .".zip";
		$zip_file_path = '/tmp/'.$zip_name;
		try {
			unlink($zip_file_path);
		} catch(\Exception $e) {
			//
		}

		if( $source == 'android' ){
			$dom = new DOMDocument("1.0", "utf-8");

			$resources_en  = $dom->createElement('resources');
			$resources_en->setAttribute('xmlns:tools', 'http://schemas.android.com/tools');
			$resources_nl  = $dom->createElement('resources');
			$resources_nl->setAttribute('xmlns:tools', 'http://schemas.android.com/tools');
			$resources_fr  = $dom->createElement('resources');
			$resources_fr->setAttribute('xmlns:tools', 'http://schemas.android.com/tools');
			$resources_pt  = $dom->createElement('resources');
			$resources_pt->setAttribute('xmlns:tools', 'http://schemas.android.com/tools');
			$resources_tr  = $dom->createElement('resources');
			$resources_tr->setAttribute('xmlns:tools', 'http://schemas.android.com/tools');

			foreach($translations as $translation){
				$stringNode = $dom->createElement('string', $translation->en);
				$stringNode->setAttribute('name', $translation->k);
				$resources_en->appendChild( $stringNode );

				$stringNode = $dom->createElement('string', $translation->nl);
				$stringNode->setAttribute('name', $translation->k);
				$resources_nl->appendChild( $stringNode );

				$stringNode = $dom->createElement('string', $translation->fr);
				$stringNode->setAttribute('name', $translation->k);
				$resources_fr->appendChild( $stringNode );

				$stringNode = $dom->createElement('string', $translation->pt);
				$stringNode->setAttribute('name', $translation->k);
				$resources_pt->appendChild( $stringNode );

				$stringNode = $dom->createElement('string', $translation->tr);
				$stringNode->setAttribute('name', $translation->k);
				$resources_tr->appendChild( $stringNode );
			}

			if( $zip->open($zip_file_path, ZIPARCHIVE::OVERWRITE) === true ){
				$dom->appendChild( $resources_en );
				$zip->addFromString("strings_en.xml", $dom->saveXML());

				$dom->removeChild( $resources_en );
				$dom->appendChild( $resources_nl );
				$zip->addFromString("strings_nl.xml", $dom->saveXML());

				$dom->removeChild( $resources_nl );
				$dom->appendChild( $resources_fr );
				$zip->addFromString("strings_fr.xml", $dom->saveXML());

				$dom->removeChild( $resources_fr );
				$dom->appendChild( $resources_pt );
				$zip->addFromString("strings_pt.xml", $dom->saveXML());

				$dom->removeChild( $resources_pt );
				$dom->appendChild( $resources_tr );
				$zip->addFromString("strings_tr.xml", $dom->saveXML());

				$zip->close();
			} else {
				var_dump('error'); exit;
			}
		}else{
			$comment_start = "/*Localizable.strings\n\tTapCrowd V2\n\n\tGenerated by TapCrowd Localization System on " . date('d/m/y');
			$comment_end = "\n\tCopyright " . date('Y') . " TapCrowd. All rights reserved.\n*/";
			//English Nederlands Français Português Türkçe
			$en_string = $comment_start . "\n\tLanguage: en" . $comment_end . "\n\n\n" . '"APPNAME" = "TapCrowd";' . "\n\n" . '"lang" = "en";';
			$nl_string = $comment_start . "\n\tLanguage: nl" . $comment_end . "\n\n\n" . '"APPNAME" = "TapCrowd";' . "\n\n" . '"lang" = "nl";';
			$fr_string = $comment_start . "\n\tLanguage: fr" . $comment_end . "\n\n\n" . '"APPNAME" = "TapCrowd";' . "\n\n" . '"lang" = "fr";';
			$pt_string = $comment_start . "\n\tLanguage: pt" . $comment_end . "\n\n\n" . '"APPNAME" = "TapCrowd";' . "\n\n" . '"lang" = "pt";';
			$tr_string = $comment_start . "\n\tLanguage: tr" . $comment_end . "\n\n\n" . '"APPNAME" = "TapCrowd";' . "\n\n" . '"lang" = "tr";';

			foreach($translations as $translation){
				$en_string .=  "\n\n". '"' . $translation->k . '" = "' . $translation->en . '";';
				$nl_string .=  "\n\n". '"' . $translation->k . '" = "' . $translation->nl . '";';
				$fr_string .=  "\n\n". '"' . $translation->k . '" = "' . $translation->fr . '";';
				$pt_string .=  "\n\n". '"' . $translation->k . '" = "' . $translation->pt . '";';
				$tr_string .=  "\n\n". '"' . $translation->k . '" = "' . $translation->tr . '";';
			}
			if( $zip->open($zip_file_path, ZIPARCHIVE::OVERWRITE) === true ){
				$zip->addFromString("en.lproj/localizable.strings", $en_string);
				$zip->addFromString("nl.lproj/localizable.strings", $nl_string);
				$zip->addFromString("fr.lproj/localizable.strings", $fr_string);
				$zip->addFromString("pt.lproj/localizable.strings", $pt_string);
				$zip->addFromString("tr.lproj/localizable.strings", $tr_string);

				$zip->close();
			} else {
				var_dump('error'); exit;
			}
		}

		if(file_exists($zip_file_path)){
			// push to download the zip
			header('Content-type: application/zip');
			header('Content-Disposition: attachment; filename="'.basename($zip_file_path).'"');
			header('Content-Transfer-Encoding: binary');
			readfile($zip_file_path);
			//remove zip file is exists in temp path
			unlink($zip_file_path);
		}
	}

}
