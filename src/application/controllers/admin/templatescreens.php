<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Templatescreens extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
        if(_authed()) { }
        $this->load->model('admin/form_template');
        $this->load->model('forms_model');
        $this->load->model('general_model');
    }

	//php 4 constructor
    function Templatescreens() {
        parent::__construct();
        if(_authed()) { }
        $this->load->model('admin/form_template');
        $this->load->model('forms_model');
        $this->load->model('general_model');
    }

    function index() {
    		//$this->form();
    }

    function form($id) {
      if($id == FALSE || $id == 0) redirect('admin/formtemplate/forms');

      $form = $this->form_template->getFormByID($id);
      $formscreens = $this->form_template->getFormScreensByFormID($id);

      $headers = array('Title' => 'title', 'Order' => 'order');

		# Load header
		$params = array('pagetitle' => $form->title, 'crumbs' => array(
			'/admin/formtemplate/forms' => __('Form Templates')
		));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

      $cdata['content'] 		= $this->load->view('admin/c_template_screens_listview', array('form' => $form, 'data' => $formscreens, 'headers' => $headers, 'childcon' => $childcon, 'confunc' => $confunc, 'typeapp' => 'app'), TRUE);
//       $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
//       $cdata['crumb']			= array(__("Form Templates") => "admin/formtemplate/forms", $form->title => "admin/templatescreens/form/".$form->id,__("Screens") => $this->uri->uri_string());

      $this->load->view('master', $cdata);
    }

    function add($id, $step = 0) {
        if($id == FALSE || $id == 0) redirect('admin/formtemplate/forms');

        // If user don't want multiple screen , Insert one screen automatically and redirect user to adding controls view
        if($step == 1)
        {
            $updData = array(
                'singlescreen' => 1,
                );

            $this->db->where('id', $id);
            $this->db->update('templateform', $updData);

            $data = array(
                "formid" => $id,
                "title" => 'Default Screen',
                "order"	=> 0
                );

            if($newid = $this->general_model->insert('templateformscreen', $data)){
                redirect('admin/templatefields/formscreen/'.$newid);
            }else {
                $error = "Oops, something went wrong. Please try again.";
            }
        }

        // End

        $form = $this->form_template->getFormByID($id);

        $this->load->library('form_validation');
        $error = "";

        if($this->input->post('postback') == "postback") {

            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('order', 'order', 'trim|numeric');

            if($this->form_validation->run() == FALSE){
                $error = validation_errors();
            } else {
                if($error == '') {
                    $data = array(
                        "formid"    => $id,
                        "title"     => set_value('title'),
                        "order"     => set_value('order')
                        );

                    if($newid = $this->general_model->insert('templateformscreen', $data)){
                        $this->session->set_flashdata('event_feedback', __('The screen has successfully been add!'));

                        if($error == '') {
                            redirect('admin/templatescreens/form/'.$form->id);
                        } else {
                            $error = __("Oops, something went wrong. Please try again.");
                        }
                    }
                }

            }
        }


		# Load header
		$params = array('pagetitle' => __('Add Screen'), 'crumbs' => array(
			'/admin/formtemplate/forms' => __('Form Templates'),
			'/admin/templatefields/formscreen/'.$form->id => $form->title
		));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);


        $cdata['content'] 		= $this->load->view('admin/c_templatescreen_add', array('error' => $error), TRUE);
//         $cdata['crumb']			= array(__("Form Templates") => "admin/formtemplate/forms", $form->title => "admin/templatescreens/form/".$form->id,__("Add Screen") => $this->uri->uri_string());
//         $cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);

        $this->load->view('master', $cdata);
    }

    function edit($id) {
        if($id == FALSE || $id == 0) redirect('admin/formtemplate/forms');

        $formscreen = $this->form_template->getFormScreenByID($id);
        $form = $this->form_template->getFormByID($formscreen->formid);

        $this->load->library('form_validation');
        $error = "";

        if($this->input->post('postback') == "postback") {

            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('order', 'order', 'trim|numeric');

            if($this->form_validation->run() == FALSE){
                $error = validation_errors();
            } else {
                if($error == '') {
                    $data = array(
                        "title"     => set_value('title'),
                        "order"    => set_value('order')
                        );

                    if($this->forms_model->edit_table($id, $data, 'templateformscreen')){

                        $this->session->set_flashdata('event_feedback', __('The screen has successfully been updated'));
                        redirect('admin/templatescreens/form/'.$form->id);
                    } else {
                        $error = __("Oops, something went wrong. Please try again.");
                    }
                }
            }
        }

		# Load header
		$params = array('pagetitle' => __('Edit Screen'), 'crumbs' => array(
			'/admin/formtemplate/forms' => __('Form Templates'),
			'/admin/templatefields/formscreen/'.$form->id => $form->title
		));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

        $cdata['content']   = $this->load->view('admin/c_templatescreen_edit', array('form' => $form, 'formscreen' => $formscreen, 'error' => $error), TRUE);
//         $cdata['crumb']     = array(__("Form Templates") => "admin/formtemplate/forms", $form->title => "admin/templatescreens/form/".$form->id,__("Edit: ") . $formscreen->title => $this->uri->uri_string());
//         $cdata['sidebar']   = $this->load->view('c_sidebar', array('event' => $event), TRUE);

        $this->load->view('master', $cdata);
    }

    function delete($id) {
        if($id == FALSE || $id == 0) redirect('admin/formtemplate/forms');
        $formscreen = $this->form_template->getFormScreenByID($id);
        $form = $this->form_template->getFormByID($formscreen->formid);

        if($this->general_model->remove('templateformscreen', $id)){
            // Delete All fields by screen
            $this->general_model->removeByParentId('formscreenid', $id, 'templateformfield');

            $this->session->set_flashdata('event_feedback', __('The screen has successfully been deleted'));
            redirect('admin/templatescreens/form/'.$form->id);
        } else {
            $this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
            redirect('admin/templatescreens/form/'.$form->id);
        }
    }

    function sort($type = '') {
        if($type == '' || $type == FALSE) redirect('events');

        switch($type){
         case "formscreen":
         $orderdata = $this->input->post('records');
         foreach ($orderdata as $val) {
           $val_split = explode("=", $val);

           $data['order'] = $val_split[1];
           $this->db->where('id', $val_split[0]);
           $this->db->update('templateformscreen', $data);
           $this->db->last_query();
        }
        break;
        default:
        break;
        }
    }
}