<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Templatefields extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
                $this->load->model('admin/form_template');
		$this->load->model('forms_model');
	}

	//php 4 constructor
	function Templatefields() {
		parent::__construct();
		if(_authed()) { }
                $this->load->model('admin/form_template');
		$this->load->model('forms_model');
	}

	function index() {
		$this->form();
	}

	function formscreen($id) {

		if($id == FALSE || $id == 0) redirect('admin/formtemplate/forms');

		$formscreen = $this->form_template->getFormScreenByID($id);
        if($formscreen == FALSE) redirect('admin/templatescreens/form/'.$id);

		$form = $this->form_template->getFormByID($formscreen->formid);

		// Get Form Fields
		$formfields = $this->form_template->getFormFieldsByFormScreenID($id);

                $headers = array('Label' => 'label', 'Order' => 'order');

		# Load header
		$params = array('pagetitle' => $formscreen->title, 'crumbs' => array(
			'/admin/formtemplate/forms' => __('Form Templates'),
			'/admin/templatescreens/form/'.$form->id => $form->title
		));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

		$cdata['content'] 		= $this->load->view('admin/c_template_fields_listview', array('formscreen' => $formscreen, 'data' => $formfields, 'headers' => $headers), TRUE);
// 		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__("Form Templates") => "admin/formtemplate/forms", $form->title => 'admin/templatescreens/form/'.$form->id ,$formscreen->title =>"admin/templatefields/formscreen/".$formscreen->id ,__("Fields") => $this->uri->uri_string());

		if($form->singlescreen):
			unset($cdata['crumb'][$formscreen->title]);
			$cdata['crumb'][$form->title] = "admin/templatefields/formscreen/".$formscreen->id;
		endif;

		$this->load->view('master', $cdata);
	}

	function add($id) {

            $this->load->library('form_validation');
            $error = "";

            $formscreen = $this->form_template->getFormScreenByID($id);
            if($formscreen == FALSE) redirect('admin/templatescreens/form/'.$id);

            $formscreenorder = $this->form_template->getFormFieldsMaxOrderByFormScreenID($id);
            $formscreen->max_order = $formscreenorder->order + 1;

            $form = $this->form_template->getFormByID($formscreen->formid);

            //Get all field Types
            $fieldtypes = $this->forms_model->getFormsFieldTypes();

            if($this->input->post('postback') == "postback") {

                $this->form_validation->set_rules('label', 'Label', 'trim|required');
		$this->form_validation->set_rules('possiblevalues', 'possiblevalues', 'trim');
		$this->form_validation->set_rules('defaultvalue', 'defaultvalue', 'trim');

		$this->form_validation->set_rules('formfieldtypeid', 'Field Type', 'trim|numeric');
		$this->form_validation->set_rules('order', 'order', 'trim|numeric');
		$this->form_validation->set_rules('xpos', 'xpos', 'trim|numeric');
		$this->form_validation->set_rules('ypos', 'ypos', 'trim|numeric');
		$this->form_validation->set_rules('width', 'width', 'trim|numeric');
		$this->form_validation->set_rules('height', 'height', 'trim|numeric');

		if($this->form_validation->run() == FALSE){
                    $error = validation_errors();
		} else {
                    if($error == '') {
                        $required = $this->input->post('required');
			if($required == '')
                            $required = "no";

			$order = $this->input->post('order');
                        if($order == ''){
                            $order = $formscreen->max_order;
			}

			$data = array(
                            "formscreenid"      => $formscreen->id,
                            "formfieldtypeid"   => set_value('formfieldtypeid'),
                            "label"             => set_value('label'),
                            "possiblevalues"    => set_value('possiblevalues'),
                            "defaultvalue"      => set_value('defaultvalue'),
                            "xpos"              => set_value('xpos'),
                            "ypos"              => set_value('ypos'),
                            "width"             => set_value('width'),
                            "height"            => set_value('height'),
                            "required"          => $required,
                            "order"             => $order
			);

			if($newid = $this->general_model->insert('templateformfield', $data)){

                            $this->session->set_flashdata('event_feedback', __('The field has successfully been add!'));

                            if($error == '') {
                                redirect('admin/templatefields/formscreen/'.$formscreen->id);
                            }
			} else {
                            $error = __("Oops, something went wrong. Please try again.");
			}
                }

            }
        }

		# Load header
		$params = array('pagetitle' => 'Add Field', 'crumbs' => array(
			'/admin/formtemplate/forms' => __('Form Templates'),
			'/admin/formtemplate/form/'.$form->id => $form->title,
			'/admin/templatefields/formscreen/'.$formscreen->id => $formscreen->title
		));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

	$cdata['content'] 		= $this->load->view('admin/c_templatefield_add', array('formscreen' => $formscreen, 'fieldtypes'=>$fieldtypes, 'error' => $error), TRUE);
	$cdata['crumb']			= array(__("Form Templates") => "admin/formtemplate/forms", __($form->title) => "admin/templatescreens/form/".$form->id, __($formscreen->title) => "admin/templatefields/formscreen/".$formscreen->id, __("Add Field") => $this->uri->uri_string());
// 	$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);

	$this->load->view('master', $cdata);
    }

    function edit($id) {
        $this->load->library('form_validation');
	$error = "";

	// EDIT Form Field
	$formfield = $this->form_template->getFormFieldByID($id);
	if($formfield == FALSE) redirect('admin/formtemplate/forms');

	$formscreen = $this->form_template->getFormScreenByID($formfield->formscreenid);
	if($formscreen == FALSE) redirect('admin/formtemplate/forms');

	$formscreenorder = $this->form_template->getFormFieldsMaxOrderByFormScreenID($formfield->formscreenid);
	$formscreen->max_order = $formscreenorder->order + 1;

	$form = $this->forms_model->getFormByID($formscreen->formid);

	//Get all field Types
	$fieldtypes = $this->forms_model->getFormsFieldTypes();

	if($this->input->post('postback') == "postback") {
            $this->form_validation->set_rules('label', 'Label', 'trim|required');
		$this->form_validation->set_rules('possiblevalues', 'possiblevalues', 'trim');
		$this->form_validation->set_rules('defaultvalue', 'defaultvalue', 'trim');

		$this->form_validation->set_rules('formfieldtypeid', 'Field Type', 'trim|numeric');
		$this->form_validation->set_rules('order', 'order', 'trim|numeric');
		$this->form_validation->set_rules('xpos', 'xpos', 'trim|numeric');
		$this->form_validation->set_rules('ypos', 'ypos', 'trim|numeric');
		$this->form_validation->set_rules('width', 'width', 'trim|numeric');
		$this->form_validation->set_rules('height', 'height', 'trim|numeric');

		if($this->form_validation->run() == FALSE){
                    $error = validation_errors();
		} else {
                    if($error == '') {
                        $required = $this->input->post('required');
			if($required == '')
                            $required = "no";

			$order = $this->input->post('order');
                        if($order == ''){
                            $order = $formscreen->max_order;
			}

			$data = array(
                            "formscreenid"      => $formscreen->id,
                            "formfieldtypeid"   => set_value('formfieldtypeid'),
                            "label"             => set_value('label'),
                            "possiblevalues"    => set_value('possiblevalues'),
                            "defaultvalue"      => set_value('defaultvalue'),
                            "xpos"              => set_value('xpos'),
                            "ypos"              => set_value('ypos'),
                            "width"             => set_value('width'),
                            "height"            => set_value('height'),
                            "required"          => $required,
                            "order"             => $order
			);

                        if($this->forms_model->edit_table($id, $data, 'templateformfield')){

                            $this->session->set_flashdata('event_feedback', __('The field has successfully been updated'));
                            redirect('admin/templatefields/formscreen/'.$formscreen->id);
			} else {
                            $error = __("Oops, something went wrong. Please try again.");
			}
                    }
                }
        }

		$form = $this->form_template->getFormByID($formscreen->formid);

		# Load header
		$params = array('pagetitle' => $formfield->label, 'crumbs' => array(
			'/admin/formtemplate/forms' => __('Form Templates'),
			'/admin/templatescreens/form/'.$form->id => $form->title,
			'/admin/templatefields/formscreen/'.$formscreen->id => $formscreen->title
		));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);


        $cdata['content'] 		= $this->load->view('admin/c_templatefield_edit', array('formfield' => $formfield, 'formscreen' => $formscreen, 'fieldtypes'=>$fieldtypes, 'error' => $error), TRUE);
//         $cdata['crumb']			= array(__("Form Templates") => "admin/formtemplate/forms", __($form->title) => "admin/templatescreens/form/".$form->id, __($formscreen->title) => "admin/templatefields/formscreen/".$formscreen->id, __("Add Field") => $this->uri->uri_string());
//         $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);

	$this->load->view('master', $cdata);
    }

    function delete($id) {

        // Delete Form Field
	$formfield = $this->form_template->getFormFieldByID($id);
	if($formfield == FALSE) redirect('admin/formtemplate/forms');

	$formscreen = $this->form_template->getFormScreenByID($formfield->formscreenid);
	if($formscreen == FALSE) redirect('admin/formtemplate/forms');

	$form = $this->forms_model->getFormByID($formscreen->formid);

        if($this->general_model->remove('templateformfield', $id)){
            $this->session->set_flashdata('event_feedback', __('The field has successfully been deleted'));
            redirect('admin/templatefields/formscreen/'.$formscreen->id);
	} else {
            $this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
            redirect('admin/templatefields/formscreen/'.$formscreen->id);
	}
    }

	function sort($type = '') {
		if($type == '' || $type == FALSE) redirect('events');

		switch($type){
			case "formfield":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('templateformfield', $data);
					$this->db->last_query();
				}
				break;
			default:
				break;
		}
	}

}