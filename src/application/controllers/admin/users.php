<?php
/**
 *
 */

class Users extends CI_Controller
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		if(!_isAdmin()) {
			header('HTTP/1.1 403 Forbidden');
			exit;
		}
	}

	/**
	 * Default action
	 */
	public function index()
	{
		$this->browse();
	}

	/**
	 * List users
	 */
	public function browse()
	{
		$account_model = \Tapcrowd\Model\Account::getInstance();
		$channel = \Tapcrowd\Model\Channel::getInstance()->getCurrentChannel();
		$params = array(
			'accounts' => $account_model->getAccountsForChannel($channel)
		);
		$this->_renderOutput( __('User Accounts'), 'admin/c_admin_accounts', $params );
	}

	/**
	 * Edit user
	 * @param int $accountId
	 */
	public function edit( $accountId )
	{
		try {
			if(!is_numeric($accountId)) {
				throw new InvalidArgumentException();
			}
			$account_model = \Tapcrowd\Model\Account::getInstance();
			$account = $account_model->getAccount($accountId);

			$params = array(
				'account' => $account,
				'user_roles' => $account_model->getAccountRoles(),
				'user_statuses' => $account_model->getAccountStatuses(),
				'applications' => $account_model->getApplications($account),
			);
			$this->_renderOutput( __('Edit Account'), 'admin/c_admin_accountform', $params);
		} catch(\Exception $e) {
			$this->_renderOutput( __('Error'), '', array('error' => $e));
		}
	}

	/**
	 * Save user
	 * @param int $accountId (optional)
	 */
	public function save( $accountId = null )
	{
		try {

			# Load models
			$account_model = \Tapcrowd\Model\Account::getInstance();
			$session_model = \Tapcrowd\Model\Session::getInstance();

			# If accountId is set as a POST variable
			if(isset($_POST['accountId'])) {
				if(!is_numeric($_POST['accountId'])) {
					throw new \InvalidArgumentException('Invalid AccountId');
				} else {
					$accountId = $_POST['accountId'];
				}
			}

			# Get account
			if(is_numeric($accountId)) {
				$account_old = $account_model->getAccount($accountId);
				$account = clone $account_old;
			} else {
				$account_old = new \Tapcrowd\Account;
				$account =& $account_old;
			}

			# Collect data
			foreach(get_object_vars($account) as $param => $value) {
				if(in_array( $param, array('accountId', 'id'))) {
					continue;
				}

				if(!empty($_POST['account_'.$param]) || ($param == 'ispartner' && $_POST['account_'.$param] == 0)) {
					$account->$param = filter_var($_POST['account_'.$param], FILTER_SANITIZE_STRING);
				}
			}

			# We cannot save an Account with missing login
			if(empty($account->login)) {
				throw new \InvalidArgumentException('Parameter "login" is missing');
			}

			# Update login
			if($account->login != $account_old->login) {
				$session_model->renameLogin( $account_old->login, $account->login);
			}

			# Update/set password
			if(!empty($_POST['account_pass']) && !empty($_POST['account_pass2'])) {
				if($_POST['account_pass'] != $_POST['account_pass2']) {
					throw new \InvalidArgumentException('Passwords do not match');
				} else {
					$password = $_POST['account_pass'];
				}
			}

			# Gather email/fullname
			if(($account_old->email != $account->email) || ($account_old->fullname != $account->fullname) || ($account_old->ispartner != $account->ispartner)) {
				$email = $account->email;
				$fullname = $account->fullname;
				$ispartner = $account->ispartner;
			} else {
				$email = $fullname = $ispartner = null;
			}

			# Update / Create Login + Account
			if($password !== null || $fullname !== null || $email !== null || $ispartner !== null) {
				$session_model->saveLogin( $account->login, $password, $fullname, $email, $ispartner );
			}

			# Update account role
			if($account_old->role != $account->role) {
				$account_model->setAccountRole( $accountId, $account->role );
			}

			# Update account status
			if($account_old->status != $account->status) {
				$account_model->setAccountStatus( $accountId, $account->status );
			}

			header('HTTP/1.1 303 See Other');
			header('Location: /admin/users');

		} catch(\Exception $e) {
			$this->_renderOutput( __('Error'), '', array('error' => $e));
		}
	}

	/**
	 * Remove user
	 * @param int $accountId
	 */
	public function delete( $accountId )
	{
		try {
			if(!is_numeric($accountId)) {
				throw new InvalidArgumentException();
			}
			$account_model = \Tapcrowd\Model\Account::getInstance();
			$account_model->removeAccount($accountId);
		} catch(\Exception $e) {
			$this->_renderOutput( __('Error'), '', array('error' => $e));
		}

		header('HTTP/1.1 303 See Other');
		header('Location: /admin/users');
	}

	/**
	 * Render the output (protected) supports: frame loading (no header etc...)
	 * json encoded output (only $params input is returned).
	 *
	 * @param string $pagetitle
	 * @param string $view
	 * @param array $params
	 * @return void
	 */
	protected function _renderOutput( $pagetitle, $view = null, $params = null, $crumbs = null )
	{
		$content = $view? $this->load->view($view, $params, true) : '';
		$accept = filter_input(INPUT_SERVER, 'Accept');
		$frame = filter_input(INPUT_GET, 'frame');

		if(!is_array($crumbs)) $crumbs = array('admin/users' => 'Accounts');
		$header = array('pagetitle' => $pagetitle, 'crumbs' => $crumbs);
		if(isset($params['error'])) $header['error'] = $params['error'];

		if($accept == 'application\json') {
			header('Content-Type: application\json');
			echo json_encode($params);
			exit;
		}
		elseif($frame !== null) echo $content;
		else {
			$pageheader = $this->load->view('admin/c_admin_header', $header, true);
			$this->load->view('master', array('pageheader' => $pageheader, 'content' => $content));
		}
	}
}
