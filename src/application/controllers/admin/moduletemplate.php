<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Moduletemplate extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		if(!_isAdmin()) redirect('apps');
        $this->load->model('admin/module_template');
        $this->load->model('admin/form_template');
	}

	function index(){
		// Get Form Templates
    	$modules = $this->module_template->getModuleTemplates();
        $headers = array(__('Title') => 'title');

		# Load header
		$params = array('pagetitle' => __('Module Templates'), 'crumbs' => array());
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

		$cdata['content'] 		= $this->load->view('admin/c_module_template_listview', array('data' => $modules, 'headers' => $headers), TRUE);
		$cdata['crumb']			= array(__("Form Templates") => "admin/moduletemplate/");

		$this->load->view('master', $cdata);
    }
	
	function add() {
	$channels = $this->module_template->getChannels();
	$apptypes = $this->form_template->getAppTypes();
	$totalTypes = count($apptypes);
	
	for($i=0; $i<$totalTypes; $i++):
		$subflavors = $this->form_template->getSubFlavorsByAppTypeId($apptypes[$i]->id);
        $apptypes[$i]->subflavor = $subflavors;
	endfor;

	$error = "";
	if($this->input->post('postback') == "postback") {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('url', 'Url', 'trim');
		$this->form_validation->set_rules('order', 'order', 'trim|numeric');
		$this->form_validation->set_rules('apptype', 'App Type', 'callback_arrayCountCheck');

		if($this->form_validation->run() == FALSE){
			$error = validation_errors();
		} else {
			if($error == '') {
				$maxId = 0;
				
				$this->db->select_max('id');
				$res = $this->db->get('moduletype');
				if( $res->num_rows() > 0 ) $maxId = $res->row()->id + 1;
				
				// Adding New moduletype
				$mData = array();
				$mData['id']			= $maxId;
				$mData['name']			= trim($this->input->post('title'));
				$mData['controller']	= 'webmodule';
				$mData['template']		= 1;

				$this->general_model->insert('moduletype', $mData);
				$moduleTypeId = $maxId;

				// Adding Default Launcher
				$this->general_model->insert('defaultlauncher', array(
					'moduletypeid'		=> $moduleTypeId,
					'module'			=> 'webmodule',
					'title'				=> $mData['name'],
					'icon'				=> 'l_catalog',
					'order'				=> $this->input->post('order'),
					'url'				=> $this->input->post('url')
				));
				
				// --------- Adding into eventtypemoduletype					
				$appFamilyIds = array();
				$appInClz = implode(',', $this->input->post('apptype'));
				$qry = 'SELECT id, familyid FROM apptype WHERE id IN ( ' . $appInClz . ' )';
				$res = $this->db->query($qry);
				if( $res->num_rows() )
					$appFamilyIds = $res->result();
				
				$ETMTdata['moduletype'] = $moduleTypeId;
				
				$qry = 'SELECT id FROM eventtypemoduletype WHERE moduletype = ' . $moduleTypeId . ' AND ';
				
				foreach( $appFamilyIds as $familyRow ){
					$whr = '';
					if( $familyRow->familyid == 1 ){ $ETMTdata['eventtype'] = 3; $ETMTdata['venueid'] = 0; $ETMTdata['appid'] = 0; $whr = 'eventtype = 3'; }
					elseif( $familyRow->familyid == 2 ) { $ETMTdata['venueid'] = 1; $ETMTdata['eventtype'] = 0; $ETMTdata['appid'] = 0; $whr = 'venueid = 1'; }
					else { $ETMTdata['appid'] = 1; $ETMTdata['venueid'] =0; $ETMTdata['eventtype'] = 0; $whr = 'appid = 1'; }
					
					$ETMTid = $this->db->query( $qry . $whr )->row()->id;
				
					if(!$ETMTid){
						$this->general_model->insert('eventtypemoduletype', $ETMTdata);
					}
				
				}
				
				// -----------------------

				if(isset($_POST['channelid']) && !empty($_POST['channelid'])) {
					$this->module_template->addModuleToChannels($_POST['channelid'], $moduleTypeId);
				}
				
				foreach($this->input->post('apptype') as $type):					
					// Linking new moduletype with subflavors
					$this->general_model->insert('moduletype_subflavor', array('moduletypeid' => $moduleTypeId, 'subflavorid' => $this->input->post('appsubflavor_'.$type)));

				endforeach;

				$this->session->set_flashdata('event_feedback', __('The module template has successfully been add!'));
				redirect('admin/moduletemplate');
			}
		}
	}

		# Load header
		$params = array('pagetitle' => __('Add Template'), 'crumbs' => array('/admin/moduletemplate' => __('Module Templates')));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

	$cdata['content'] 		= $this->load->view('admin/c_module_template_add', array('error' => $error, 'apptypes' => $apptypes, 'channels' => $channels), TRUE);
	$cdata['crumb']		= array(__("Module Templates") => "admin/moduletemplate", __("Add new") => $this->uri->uri_string());
	$this->load->view('master', $cdata);

	}

	function edit($id) {
		$channels = $this->module_template->getChannels();
		$currentChannelids = $this->module_template->getChannelidsOfModule($id);
		$module = $this->module_template->getModule($id);
		$moduleApps   = $this->module_template->getModuleTemplateApps($id);

		$moduleTypeId = $id;
		$apptypes = $this->form_template->getAppTypes();

		$alreadyExistsSubFlavors = array();

		$totalTypes = count($apptypes);
		for($i=0; $i<$totalTypes; $i++):
			$subflavors = $this->form_template->getSubFlavorsByAppTypeId($apptypes[$i]->id);
			$apptypes[$i]->subflavor = $subflavors;
			if(array_key_exists($apptypes[$i]->id, $moduleApps))
			{
				$apptypes[$i]->checked = 1;
				$apptypes[$i]->subflavor['selected'] = $moduleApps[$apptypes[$i]->id];
				$alreadyExistsSubFlavors[] = $apptypes[$i]->subflavor['selected'];
			}
		endfor;

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Title', 'trim|required');
			$this->form_validation->set_rules('order', 'order', 'trim|numeric');
			$this->form_validation->set_rules('url', 'Url', 'trim');

			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {
				if($error == '') {
					$data = array();
					$data['title']                      = trim($this->input->post('title'));
					$data['order']                      = ( $this->input->post('order') == '' )? 0 : $this->input->post('order');
					$this->db->where('moduletypeid', $moduleTypeId);
					$this->db->update('defaultlauncher', array('title' => $data['title'], 'order' => $data['order'], 'url' => $this->input->post('url')));

					//------- Updating moduletype table 
					$this->db->where('id', $moduleTypeId);
					$this->db->update('moduletype', array('name' => $data['title']));
					
					// --------- Adding into eventtypemoduletype					
					$appFamilyIds = array();
					$appInClz = implode(',', $this->input->post('apptype'));
					if( $appInClz ) {
						$qry = 'SELECT id, familyid FROM apptype WHERE id IN ( ' . $appInClz . ' )';
						$res = $this->db->query($qry);
						if( $res->num_rows() )
							$appFamilyIds = $res->result();
						
						$ETMTdata['moduletype'] = $moduleTypeId;
						
						$qry = 'SELECT id FROM eventtypemoduletype WHERE moduletype = ' . $moduleTypeId . ' AND ';
						
						foreach( $appFamilyIds as $familyRow ){
							$whr = '';
							if( $familyRow->familyid == 1 ){ $ETMTdata['eventtype'] = 3; $ETMTdata['venueid'] = 0; $ETMTdata['appid'] = 0; $whr = 'eventtype = 3'; }
							elseif( $familyRow->familyid == 2 ) { $ETMTdata['venueid'] = 1; $ETMTdata['eventtype'] = 0; $ETMTdata['appid'] = 0; $whr = 'venueid = 1'; }
							else { $ETMTdata['appid'] = 1; $ETMTdata['venueid'] =0; $ETMTdata['eventtype'] = 0; $whr = 'appid = 1'; }
							
							$ETMTid = $this->db->query( $qry . $whr )->row()->id;
						
							if(!$ETMTid){
								$this->general_model->insert('eventtypemoduletype', $ETMTdata);
							}
						
						}
						
						foreach($this->input->post('apptype') as $type):
							if(!in_array($this->input->post('appsubflavor_'.$type), $alreadyExistsSubFlavors)){
								$this->general_model->insert('moduletype_subflavor', array('moduletypeid' => $moduleTypeId, 'subflavorid' => $this->input->post('appsubflavor_'.$type)));
							}
						endforeach;

						if(isset($_POST['channelid']) && !empty($_POST['channelid'])) {
							$this->module_template->addModuleToChannels($_POST['channelid'], $moduleTypeId);
						}

						$this->session->set_flashdata('event_feedback', __('The module template has successfully been update!'));
						redirect('admin/moduletemplate');

					}else{
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

		}

		# Load header
		$params = array('pagetitle' => __('Edit Module Template'), 'crumbs' => array('/admin/moduletemplate' => __('Module Templates')));
		$cdata['pageheader'] = $this->load->view('admin/c_admin_header', $params, true);

		$cdata['content'] 		= $this->load->view('admin/c_module_template_edit', array('module' => $module, 'error' => $error, 'apptypes' => $apptypes, 'channels' => $channels, 'currentChannelids' => $currentChannelids), TRUE);
		$cdata['crumb']		= array(__("Form Templates") => "admin/moduletemplate", __("Edit ".$module->title) => $this->uri->uri_string());
		$this->load->view('master', $cdata);

	}

	function arrayCountCheck($str){

		if(count($str) == 0)
		{
			$this->form_validation->set_message('arrayCountCheck', 'Please select atleast one %s.');
			return false;
		}

		return true;
	}

	function remove($id) {
		$this->module_template->removeModuleTemplate($id);
		redirect('admin/moduletemplate');
	}

}
?>