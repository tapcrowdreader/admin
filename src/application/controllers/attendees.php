<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Attendees extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('attendee_model');
		$this->load->model('confbag_model');
		$this->load->model('premium_model');
		$this->load->model('metadata_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Attendee',
			'plural' => 'Attendees',
			'title' => __('Attendees'),
			'parentType' => 'event',
			'browse_url' => 'attendees/--parentType--/--parentId--',
			'add_url' => 'attendees/add/--parentId--/--parentType--',
			'edit_url' => 'attendees/edit/--id--',
			'module_url' => 'module/editByController/attendees/--parentType--/--parentId--/0',
			'import_url' => 'import/attendees/--parentId--/--parentType--',
			'headers' => array(
				__('Name') => 'name',
				__('Firstname') => 'firstname',
				__('Company') => 'company'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'attendees/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'attendees/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'checkboxes' => false,
			'deletecheckedurl' => 'attendees/removemany/--parentId--/--parentType--'
		);
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = 'attendees/event/'.$id;

		$attendees = $this->attendee_model->getAttendeesByEventID($id, 'eventid');

		$maingroupid = $this->attendee_model->getMainCategorieGroup($id);
		#
		# Check for API call
		#
		$this->_handleAPIRequest($attendees, $error);

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# Module import url
		$import_url =& $this->_module_settings->import_url;
		$import_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $import_url);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		if($maingroupid) {
			$btnCategories = (object)array(
							'title' => __('Edit Categories'),
							'href' => 'groups/view/'.$maingroupid.'/event/'.$id.'/attendees',
							'icon_class' => 'icon-pencil',
							'btn_class' => 'edit btn',
							);
			$this->_module_settings->extrabuttons['categories'] = $btnCategories;
		}

		$launcher = $this->module_mdl->getLauncher(14, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $attendees), true);

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'attendees');
		$this->load->view('master', $cdata);
	}

	function add($id, $type = 'event', $object = 'attendees', $groupid = '', $basegroupid = '') {
		$this->load->library('form_validation');

		$error = "";

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		$this->iframeurl = 'attendees/event/'.$id;

		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$launcher = $this->module_mdl->getLauncher(14, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;
		$metadata = $this->metadata_model->getMetadata($launcher, 'event', $event->id);

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'attendees' GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}
		//validation
		if($this->input->post('mytagsulselect') != null) {
			$postedtags = $this->input->post('mytagsulselect');
		} else {
			$postedtags = array();
		}

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('fname', 'fname', 'trim|required');
			$this->form_validation->set_rules('external_id', 'external_id', 'trim');
			$this->form_validation->set_rules('company', 'company', 'trim');
			$this->form_validation->set_rules('email', 'email', 'trim|valid_email');
			$this->form_validation->set_rules('linkedin', 'linkedin', 'trim');
			$this->form_validation->set_rules('phone', 'phone', 'trim');
			$this->form_validation->set_rules('country', 'country', 'trim');
			$this->form_validation->set_rules('premium', 'premium', 'trim');
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
            foreach($languages as $language) {
                $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
                $this->form_validation->set_rules('function_'.$language->key, 'function_'.$language->key, 'trim');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}
            }
            $this->form_validation->set_rules('confbagcontent', 'confbagcontent', 'trim');

            $external_id = '';
            if($this->input->post('external_id')) {
            	$external_id = $this->input->post('external_id');
            }

			if($this->form_validation->run() == FALSE){
				$error = __("Some fields are missing.");
			} else {
				// Save exhibitor
				$order = 0;
				if(set_value('order') != '') {
					$order = set_value('order');
				}
				$data = array(
						'external_id' 	=> $external_id,
						'eventid'		=> $id,
						'name'			=> trim(set_value('name')),
						'firstname'		=> trim(set_value('fname')),
						'company'		=> set_value('company'),
						'function'		=> set_value('function_'.  $app->defaultlanguage),
						'email'			=> set_value('email'),
						'linkedin'		=> set_value('linkedin'),
						'phonenr'		=> set_value('phone'),
						"description"	=> set_value('description_'.  $app->defaultlanguage),
						'country'		=> set_value('country'),
						'order'			=> $order
					);
				$res = $this->general_model->insert('attendees', $data);

				if(!is_dir($this->config->item('imagespath') . "upload/attendeeimages/".$res)){
					mkdir($this->config->item('imagespath') . "upload/attendeeimages/".$res, 0775, true);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/attendeeimages/'.$res;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);

				$this->upload->initialize($configexlogo);
				if ($this->upload->do_upload('imageurl')) {
					$returndata = $this->upload->data();
					$image1 = 'upload/attendeeimages/'.$res.'/'.$returndata['file_name'];
					$this->general_model->update('attendees',$res,array('imageurl' => $image1));
				} else {
					if($_FILES['imageurl']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				}

				if(!is_dir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id)){
					mkdir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id, 0755, TRUE);
				}
				$configconfbag['upload_path'] = $this->config->item('imagespath') .'upload/confbagfiles/event/'.$event->id;
				$configconfbag['allowed_types'] = 'JPG|jpg|jpeg|png|pdf|xls|xlsx|txt|doc|docx|ppt|pptx';
				$configconfbag['max_size'] = '50000';
				$this->load->library('upload', $configconfbag);

				$this->upload->initialize($configconfbag);
				if ($this->upload->do_upload('confbagcontent')) {
					$returndata = $this->upload->data();
					$confbagcontent = 'upload/confbagfiles/event/'.$event->id.'/'.$returndata['file_name'];
					$this->general_model->insert('confbag', array('eventid' => $event->id, 'itemtable' => 'attendees', 'tableid' => $res, 'documentlink' => $confbagcontent));
				} else {
					if($_FILES['confbagcontent']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				}

				if($res != FALSE){
					//metadata
					$this->general_model->insert('metadata', array('appid' => $app->id, 'table' => 'attendees', 'identifier' => $res, 'key' => 'facebook', 'value' => $this->input->post('facebook')));
					$this->general_model->insert('metadata', array('appid' => $app->id, 'table' => 'attendees', 'identifier' => $res, 'key' => 'twitter', 'value' => $this->input->post('twitter')));

					//save premium
					$this->premium_model->save($this->input->post('premium'), 'attendees', $res, $this->input->post('extraline'), false, 'event', $event->id, $this->input->post('premiumorder'));

                    //add translated fields to translation table
                    foreach($languages as $language) {
                        $this->language_model->addTranslation('attendees', $res, 'description', $language->key, $this->input->post('description_'.$language->key));
                        $this->language_model->addTranslation('attendees', $res, 'function', $language->key, $this->input->post('function_'.$language->key));
						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								$postfield = _getPostedField($m, $_POST, $_FILES, $language);
								if($postfield !== false) {
									if(_validateInputField($m, $postfield)) {
										_saveInputField($m, $postfield, 'attendees', $res, $app, $language);
									}
								}
							}
						}
                    }

					if(!empty($groupid)) {
						$group = $this->group_model->getById($groupid);
						$groupitemdata = array(
								'appid'	=> $app->id,
								'eventid'	=> $event->id,
								'groupid'	=> $group->id,
								'itemtable'	=> 'attendees',
								'itemid'	=> $res,
								'displaytype' => $group->displaytype
							);
						$this->general_model->insert('groupitem', $groupitemdata);
					} 

					// *** SAVE TAGS *** //
					$tags = $this->input->post('mytagsulselect');
					foreach ($tags as $tag) {
						$this->general_model->insert('tc_tag', array(
							'appid' => $app->id,
							'itemtype' => 'attendees',
							'itemid' => $res,
							'tag' => $tag
						));
					}

					$this->session->set_flashdata('event_feedback', __('The attendee has successfully been added'));
					_updateTimeStamp($event->id);
					#
					# Check for API call
					#
					$this->_handleAPIRequest($id, $error);
					if($error == '') {
						if($groupid != '') {
							redirect('groups/view/'.$groupid.'/event/'.$event->id.'/attendees/'.$basegroupid);
						}

						redirect('attendees/event/'.$event->id);
					} else {
						//image error
						redirect('attendees/edit/'.$id.'/event?error=image');
					}
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}

			}
		}

		//confbag
		$confbagactive = $this->confbag_model->checkActive($event->id);

		$cdata['content'] 		= $this->load->view('c_attendee_add', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'confbagactive' => $confbagactive, 'metadata' => $metadata, 'catshtml'	=> $catshtml, 'maincat' => $maincat, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);

		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "attendees/event/".$event->id, __("Add new") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "attendees/event/".$event->id, __("Add new") => $this->uri->uri_string());
		}
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'attendees');
		$this->load->view('master', $cdata);
	}

	function edit($id, $type = 'event', $object = 'attendees', $groupid = '', $basegroupid = '') {
		$this->load->library('form_validation');
		$error = "";

		// EDIT EXHIBITOR
		$attendee = $this->attendee_model->getAttendeeByID($id);
		if($attendee == FALSE) redirect('events');

		$event = $this->event_model->getbyid($attendee->eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$this->iframeurl = 'attendees/view/'.$id;

		$premium = $this->premium_model->get('attendees', $attendee->id);

		$metadata = $this->metadata_model->get($app->id, 'attendees', $id);
		foreach($metadata as $m) {
			$attendee->{$m->key} = $m->value;
		}

		$launcher = $this->module_mdl->getLauncher(14, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;
		$metadata = $this->metadata_model->getMetadata($launcher, 'attendees', $attendee->id, $app);

		$maincat = $this->attendee_model->getMainCategorieGroup($event->id);
		if(empty($basegroupid) && empty($groupid)) {
			$basegroupid = $maincat;
			$groupid = $maincat;
		}

		$catshtml = '';
		$categories = array();
		$currentCats = '';
		if($maincat != false) {
			$this->attendee_model->htmlarray = array();
			$catshtml = $this->attendee_model->display_children($basegroupid,0,$attendee->eventid, $attendee->id);
			$currentCats = $this->attendee_model->groupids;
			$this->attendee_model->groupids = '';
		}

		if(!empty($groupid)) {
			$group = $this->group_model->getById($groupid);
			$parents = $this->group_model->getParents($groupid);
		}

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'attendees' GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}
		$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'attendees'");
		if($tags->num_rows() == 0) {
			$tags = array();
		} else {
			$tagz = array();
			foreach ($tags->result() as $tag) {
				$tagz[] = $tag;
			}
			$tags = $tagz;
		}
		//validation
		if($this->input->post('mytagsulselect') != null) {
			$postedtags = $this->input->post('mytagsulselect');
		} else {
			$postedtags = array();
		}

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('fname', 'fname', 'trim|required');
			$this->form_validation->set_rules('company', 'company', 'trim');
			$this->form_validation->set_rules('external_id', 'external_id', 'trim');
			$this->form_validation->set_rules('email', 'email', 'trim|valid_email');
			$this->form_validation->set_rules('linkedin', 'linkedin', 'trim');
			$this->form_validation->set_rules('phone', 'phone', 'trim');
			$this->form_validation->set_rules('country', 'country', 'trim');
			$this->form_validation->set_rules('premium', 'premium', 'trim');
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
            foreach($languages as $language) {
                $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
                $this->form_validation->set_rules('function_'.$language->key, 'function_'.$language->key, 'trim');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}
            }
            $this->form_validation->set_rules('confbagcontent', 'confbagcontent', 'trim');

			if($this->form_validation->run() == FALSE){
				$error = __("Some fields are missing.");
			} else {
	            $external_id = $attendee->external_id;
	            if($this->input->post('external_id')) {
	            	$external_id = $this->input->post('external_id');
	            }
				$data = array(
						'external_id' 	=> $external_id,
						'name'			=> trim(set_value('name')),
						'firstname'		=> trim(set_value('fname')),
						'company'		=> set_value('company'),
						'function'		=> set_value('function_'. $app->defaultlanguage),
						'email'			=> set_value('email'),
						'linkedin'		=> set_value('linkedin'),
						'phonenr'		=> set_value('phone'),
						"description"   => set_value('description_'. $app->defaultlanguage),
						'country'		=> set_value('country'),
						'order'			=> set_value('order')
					);

				if(!is_dir($this->config->item('imagespath') . "upload/attendeeimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/attendeeimages/".$id, 0775, true);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/attendeeimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);

				$this->upload->initialize($configexlogo);
				if ($this->upload->do_upload('imageurl')) {
					$returndata = $this->upload->data();
					$image1 = 'upload/attendeeimages/'.$id.'/'.$returndata['file_name'];
					$data['imageurl'] = $image1;
				} else {
					if($_FILES['imageurl']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				}

				if($this->general_model->update('attendees', $id, $data)){
					if(!is_dir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id)){
						mkdir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id, 0755, TRUE);
					}
					$configconfbag['upload_path'] = $this->config->item('imagespath') .'upload/confbagfiles/event/'.$event->id;
					$configconfbag['allowed_types'] = 'JPG|jpg|jpeg|png|pdf|xls|xlsx|txt|doc|docx|ppt|pptx';
					$configconfbag['max_size'] = '50000';
					$this->load->library('upload', $configconfbag);

					$this->upload->initialize($configconfbag);
					if ($this->upload->do_upload('confbagcontent')) {
						$returndata = $this->upload->data();
						$confbagcontent = 'upload/confbagfiles/event/'.$event->id.'/'.$returndata['file_name'];
						$this->general_model->insert('confbag', array('eventid' => $event->id, 'itemtable' => 'attendees', 'tableid' => $id, 'documentlink' => $confbagcontent));
					} else {
						if($_FILES['confbagcontent']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					}

					//metadata
					$this->metadata_model->removeKey('facebook', 'attendees', $id);
					$this->general_model->insert('metadata', array('appid' => $app->id, 'table' => 'attendees', 'identifier' => $id, 'key' => 'facebook', 'value' => $this->input->post('facebook')));
					$this->metadata_model->removeKey('twitter', 'attendees', $id);
					$this->general_model->insert('metadata', array('appid' => $app->id, 'table' => 'attendees', 'identifier' => $id, 'key' => 'twitter', 'value' => $this->input->post('twitter')));

					//save premium
					$this->premium_model->save($this->input->post('premium'), 'attendees', $id, $this->input->post('extraline'), $premium, 'event', $event->id, $this->input->post('premiumorder'));

					$this->db->where('itemtype', 'attendees');
					$this->db->where('itemid', $id);
					$this->db->delete('tc_tag');
					$tags = $this->input->post('mytagsulselect');
					foreach ($tags as $tag) {
						$this->general_model->insert('tc_tag', array(
							'appid' => $app->id,
							'itemtype' => 'attendees',
							'itemid' => $id,
							'tag' => $tag
						));
					}

                    //add translated fields to translation table
                    foreach($languages as $language) {
						$descrSuccess = $this->language_model->updateTranslation('attendees', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
						if($descrSuccess == false || $descrSuccess == null) {
							$this->language_model->addTranslation('attendees', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
						}
						$functionSuccess = $this->language_model->updateTranslation('attendees', $id, 'function', $language->key, $this->input->post('function_'.$language->key));
						if($functionSuccess == false || $functionSuccess == null) {
							$this->language_model->addTranslation('attendees', $id, 'function', $language->key, $this->input->post('function_'.$language->key));
						}

						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								$postfield = _getPostedField($m, $_POST, $_FILES, $language);
								if($postfield !== false) {
									if(_validateInputField($m, $postfield)) {
										_saveInputField($m, $postfield, 'attendees', $id, $app, $language);
									}
								}
							}
						}
					}
					
					if((isset($groupid) && $groupid != 0) || $maincat != false) {
						//groupitem
						$this->db->query("DELETE FROM groupitem WHERE itemid = $attendee->id AND itemtable = 'attendees'");
						if($this->input->post('groups') != null && $this->input->post('groups') != false) {
							$groups = $this->input->post('groups');
							foreach($groups as $group) {
								if($group != '') {
									$group2 = $this->group_model->getById($group);
									$groupitemdata = array(
											'appid'	=> $app->id,
											'eventid'	=> $attendee->eventid,
											'groupid'	=> $group2->id,
											'itemtable'	=> 'attendees',
											'itemid'	=> $attendee->id,
											'displaytype' => $group2->displaytype
										);
									$this->general_model->insert('groupitem', $groupitemdata);
								}
							}
						}
					}
					$this->session->set_flashdata('event_feedback', __('The attendee has successfully been updated'));
					_updateTimeStamp($event->id);
					#
					# Check for API call
					#
					$this->_handleAPIRequest($attendee->id, $error);
					if($error == '') {
						if($groupid != '') {
							redirect('groups/view/'.$groupid.'/event/'.$event->id.'/attendees/'.$basegroupid);
						}
						redirect('attendees/event/'.$event->id);
					}
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
			}
		}

		#
		# Check for API call
		#
		$this->_handleAPIRequest($attendee, $error);
		//confbag
		$confbagactive = $this->confbag_model->checkActive($event->id);
		$attendeeconfbag = false;
		if($confbagactive) {
			$attendeeconfbag = $this->confbag_model->getFromObject('attendees',$id);
		}

		if(!empty($groupid)) {
			$crumbarray = array($event->name => 'event/view/'.$event->id);
			$currentCats = explode('/', $currentCats);
			foreach($parents as $p) {
				$p->name = str_replace('attendeecategories', 'Attendees', $p->name);
				if(strtolower($p->name) != 'attendeecategories' && strtolower($p->name != 'attendees')) {
					$crumbarray[$p->name] = 'groups/view/'.$p->id.'/event/'.$event->id.'/attendees/'.$basegroupid;
				}
			}
			$group->name = str_replace('attendeecategories', 'Attendees', $group->name);
			$crumbarray[$group->name] = 'groups/view/'.$group->id.'/event/'.$event->id.'/attendees/'.$basegroupid;
			$crumbarray[__("Edit: ") . $attendee->firstname . ' ' . $attendee->name] = $this->uri->uri_string();
			$cdata['crumb']			= $crumbarray;
		} else {
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "attendees/event/".$event->id, __("Edit: ") . $attendee->firstname . ' ' . $attendee->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "attendees/event/".$event->id, __("Edit: ") . $attendee->firstname . ' ' . $attendee->name => $this->uri->uri_string());
			}
		}

		$cdata['content'] 		= $this->load->view('c_attendee_edit', array('event' => $event, 'attendee' => $attendee, 'error' => $error, 'languages' => $languages, 'app' => $app, 'confbagactive' => $confbagactive, 'attendeeconfbag' => $attendeeconfbag, 'premium' => $premium, 'metadata' => $metadata, 'maincat' => $maincat, 'catshtml' => $catshtml, 'currentCats' => $currentCats, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'attendees');
		$this->load->view('master', $cdata);
	}

	function delete($id) {
		$attendee = $this->attendee_model->getAttendeeByID($id);
		$event = $this->event_model->getbyid($attendee->eventid);
		$app = _actionAllowed($event, 'event','returnApp');
        $this->language_model->removeTranslations('attendees', $id);
		if($this->general_model->remove('attendees', $id)){
			$this->attendee_model->removePremium($id);
			$this->metadata_model->removeFromObject('attendees', $id);
			$this->session->set_flashdata('event_feedback', __('The attendee has successfully been deleted'));
			_updateTimeStamp($attendee->eventid);
			redirect('attendees/event/'.$attendee->eventid);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('attendees/event/'.$attendee->eventid);
		}
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'attendees');
    } 

	/**
	 * Handles API requests
	 *
	 * @param array $exhibitors
	 */
	protected function _handleAPIRequest( $data, $errors = null )
	{
		# Verify Request Accept header
		if($_SERVER['HTTP_ACCEPT'] != 'application/json') return;

		# Parse errors
		if(!empty($errors)) {
			echo json_encode(array('error' => strip_tags($errors)));
			exit;
		}

		# Cleanup the $exhibitors data
		$trash = array('username','password','mapid','y1','x1','x2','y2');

		# Verify empty image params && Trash bloated params
		$cleanup = function( &$exh ) use($trash)
		{

		};
		if(is_array($data)) array_walk($data, $cleanup); else $cleanup($data);

		# Output json encoded data
		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($data);
		exit;
	} 

	function removeimage($id, $type = 'event') {
		$item = $this->attendee_model->getAttendeeByID($id);
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($item->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->imageurl)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('attendees', $id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('attendees/venue/'.$venue->id);
		} else {
			$event = $this->event_model->getbyid($item->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->imageurl)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('attendees', $id, $data);

			_updateTimeStamp($event->id);
			redirect('attendees/event/'.$event->id);
		}
	}

}