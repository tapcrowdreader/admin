<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Download extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Download() {
		parent::__construct();
	}
	
	function index() {
		$this->app();
	}
	
	function app($name) {
		if($name == '' || $name == FALSE) header("Location:http://www.tapcrowd.com");
		
		$res  = $this->db->query("SELECT * FROM appstores WHERE name = '$name'");
		if($res->num_rows() == 0) header("Location:http://www.tapcrowd.com");
		$app = $res->row();
		
		$this->load->library('uagent_info');
		if($this->uagent_info->DetectAndroid()){
			//echo "ANDROID";
			header("Location:$app->androidmarket");
			//redirect($app->androidmarket, 'location', 301);
		} else if($this->uagent_info->DetectIphoneOrIpod()){
			//echo "IPHONE";
			header("Location:$app->appleappstore");
			//redirect($app->appleappstore, 'location', 301);
		} else if($this->uagent_info->DetectSmartphone()){
			//echo "SMARTPHONE";
			header("Location:$app->mobwebsite");
			//redirect($app->appleappstore, 'location', 301);
		} else if($this->uagent_info->DetectIpad()){
			//echo "IPAD";
			header("Location:$app->mobwebsite");
			//redirect($app->appleappstore, 'location', 301);
		} else {
			//echo "NO APP";
			$this->load->view('download/overview', array('app' => $app));
		}
		
	}

}