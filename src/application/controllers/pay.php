<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Pay extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('price_model', 'price');
		$this->load->model('order_model', 'order');
	}

	function cancel($id) {
		$app = $this->app_model->get($id);
		if($app == FALSE) redirect('apps');

		_actionAllowed($app, 'app');

		$balance = $this->price->getBalanceForApp($id);
		$cdata['content'] 		= $this->load->view('c_pay_cancel', array('app' => $app, 'balance' => $balance, 'flavor' => $flavor, 'options' => $options, 'priceApp' => $priceApp), TRUE);

		$cdata['crumb']			= array($app->name => 'apps/view/'.$app->id);
		$this->load->view('master', $cdata);
	}

	function ok($id) {
		$app = $this->app_model->get($id);
		if($app == FALSE) redirect('apps');

		_actionAllowed($app, 'app');
		$cdata['content'] 		= $this->load->view('c_pay_ok', array('app' => $app), TRUE);
		$cdata['crumb']			= array($app->name => 'apps/view/'.$app->id);
		$this->load->view('master', $cdata);
	}

	function order($id) {
		$order = $this->order->get_entry($id);		
		$app = $this->app_model->get($order->appid);
		if($app == FALSE) redirect('apps');
		_actionAllowed($app, 'app');

		$this->load->model('price_model');
		$allowpayment = $this->price_model->allowPayment();

		$cdata['content'] 		= $this->load->view('c_pay_order', array('app' => $app, 'order' => $order, 'flavor' => $flavor, 'options' => $options, 'priceApp' => $priceApp, 'allowpayment' => $allowpayment), TRUE);

		$cdata['crumb']			= array($app->name => 'apps/view/'.$app->id);
		$this->load->view('master', $cdata);
	}

	function success($id) {
		$app = $this->app_model->get($id);
		if($app == FALSE) redirect('apps');

		_actionAllowed($app, 'app');

		$balance = $this->price->getBalanceForApp($id);
		$cdata['content'] 		= $this->load->view('c_pay_confirm', array('app' => $app, 'balance' => $balance), TRUE);

		$cdata['crumb']			= array($app->name => 'apps/view/'.$app->id);
		$this->load->view('master', $cdata);
	}

	function app($id) {
		$app = $this->app_model->get($id);
		if($app == FALSE) redirect('apps');

		_actionAllowed($app, 'app');

		$balance = $this->price->getBalanceForApp($id);
		$cdata['content'] 		= $this->load->view('c_pay', array('app' => $app, 'balance' => $balance, 'flavor' => $flavor, 'options' => $options, 'priceApp' => $priceApp), TRUE);

		$cdata['crumb']			= array($app->name => 'apps/view/'.$app->id);
		$this->load->view('master', $cdata);
	}	

	function confirm($id) {
		$app = $this->app_model->get($id);
		if($app == FALSE) redirect('apps');

		_actionAllowed($app, 'app');

		$balance = $this->price->getBalanceForApp($id);
		$cdata['content'] 		= $this->load->view('c_pay_confirm', array('app' => $app), TRUE);

		$cdata['crumb']			= array($app->name => 'apps/view/'.$app->id);
		$this->load->view('master', $cdata);
	}
}