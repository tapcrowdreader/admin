<?php

class Webservice extends CI_Controller {
	//General Section
	//Methods made for the xml webservice
	function Webservice() {
		parent::__construct();
	}
	
	function index() {
		header ("content-type: text/xml");
		echo'<?xml version="1.0" encoding="UTF-8"?>';
		echo '<expoplus version="1.0">';
		echo '<error id="404">'.__('404 page not found').'</error>';
		echo '<errortext>'.__('page not found.').'</errortext>';
		echo '</expoplus>';
	}

	/*
	 * Login Section
	 */
	//Login pageview
	function getEventDetails($eventid = 0, $token = 0) {
		$url = 'http://api.tabcrowd.com/webservice/getEventDetails/'.$eventid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/xml');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}
	
	function getEventList($appid = 0, $token = 0) {
		$url = 'http://api.tabcrowd.com/webservice/getEventList/'.$appid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/xml');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}
	
	function getToken($appid) {
		$this->load->model("app_model");
		$test = $this->app_model->get($appid);
		print_r($test);
	}
	
    function getTCList($token) {
		$url = 'http://api.tabcrowd.com/webservice/getTCList/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/xml');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
    }

	function getNokiaXML($eventid = 0, $token = 0) {
		$url = 'http://api.tabcrowd.com/webservice/getNokiaXML/'.$eventid.'/'.$token;
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		
		if($result === false) {
			echo __('Gateway error: ') . curl_error($ch);
		} else {
			// Operation completed without any errors
			header('Content-type: application/xml');
			echo $result;
			// echo $result;
		}
		
		curl_close($ch);
	}

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */