<?php
/**
 * Tapcrowd Controllers
 */
if(!defined('BASEPATH')) exit(__('No direct script access'));

use \Tapcrowd\Models\Map as MapModel;
// use \Tapcrowd\Structs\Map as MapObject;
// use \Tapcrowd\Structs\Marker as MarkerObject;

class Maps extends CI_Controller
{
	protected $map_model;

	protected $_module_settings;

	protected $_app;

	public function __construct()
	{
		parent::__construct();
		$this->map_model = MapModel::getInstance();

		$this->_module_settings = array();

		$this->_app = (object)array('type' => 'event', 'id' => 4740, 'appId' => $this->app);
	}

	public function index()
	{
		//redirect();
	}

	public function launcher( $launcherId )
	{
		$this->browse('launcher', (int)$launcherId);
	}

	public function app( $appId )
	{
		$this->browse('app', (int)$appId);
	}

	public function browse( $parentType, $parentId )
	{
		# Get maps
		$maps = $this->map_model->getMaps( $parentType, $parentId);

		$accept_header = $_SERVER['HTTP_ACCEPT'];
		if(strpos($accept_header, 'application/json') === false) {

			# Api needs separated arrays
			$data = array('maps' => array(), 'markers' => array());
			foreach($maps as &$map) {
				$data['maps'][] = $map;
				$data['markers'] = array_merge($data['markers'], $map->markers);
				unset($map->markers);
			}
			header('Content-Type: application/json');
			echo json_encode($data);
			exit;
		}

		# Render maps
		$html_maps = '';
		foreach($maps as $map) {
			$html_maps .= $this->load->view('tc_mapview', array('map'=>$map), true);
		}

		# Output
		$this->_loadView($html_maps);
	}

	public function addDemoMap()
	{
		$appId = 1426;
		$parentType = 'launcher';
		$parentId = 8933;
		$url = 'https://maps.google.com/?z=8&t=t&ll=50.753887,4.269936';
		$title = 'ArtBrussels City Map';
		$mapType = 'geographic';

		# Create new Map and save it
		$map = $this->map_model->createMap( $parentType, $parentId, $url, $title, $mapType);
		$this->map_model->saveMap($map);

		# Add Markers to the map
// 		$this->map_model->createMarker( \Tapcrowd\Structs\Map $map, $parentType, $parentId);

	}

	/**
	 * View shorthand
	 */
	protected function _loadView( $content_view )
	{
		$cdata = array(
			'content' => $content_view,
			'sidebar' => $this->load->view('c_sidebar', array($this->_app->type => $this->_app->id), true),
			'crumb' => array(),
			'modules' => $this->module_mdl->getModulesForMenu($this->_app->type, $this->_app->id, $this->_app->appId, 'maps'),
		);
		$this->load->view('master', $cdata);
	}
}
