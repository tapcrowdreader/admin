<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Sponsorgroups extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('sponsor_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Sponsorgroup',
			'plural' => 'Sponsorgroups',
			'parentType' => 'event',
			'browse_url' => 'sponsorgroups/--parentType--/--parentId--',
			'add_url' => 'sponsorgroups/add/--parentId--/--parentType--',
			'edit_url' => 'sponsorgroups/edit/--id--',
			'module_url' => 'module/editByController/sponsors/--parentType--/--parentId--/0',
			'import_url' => '',
			'headers' => array(
				__('Name') => 'name',
				__('Order') => 'order'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'sponsorgroups/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'sponsorgroups/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(
				'sponsors' => array()
			)
		);
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		$this->iframeurl = "sponsors/event/" . $id;

		$groups = $this->sponsor_model->getSponsorgroupsByEventID($id);

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);

		$btnSponsors = (object)array(
						'title' => __('Edit Sponsors'),
						'href' => 'sponsors/event/'.$id,
						'icon_class' => 'icon-pencil',
						'btn_class' => 'edit btn',
						);
		$this->_module_settings->extrabuttons['sponsors'] = $btnSponsors;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $groups), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$id, __('Sponsors') => 'sponsors/event/'.$id, __("Sponsor Groups") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']	= array($event->name => "event/view/".$id, __('Sponsors') => 'sponsors/event/'.$id, __("Sponsor Groups") => $this->uri->uri_string());
		}

		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->iframeurl = "sponsors/venue/" . $id;

		$this->_module_settings->parentType = 'venue';
		$groups = $this->sponsor_model->getSponsorgroupsByVenueID($id);
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'venue', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'venue', $delete_href);

		$btnSponsors = (object)array(
						'title' => __('Edit Sponsors'),
						'href' => 'sponsors/venue/'.$id,
						'icon_class' => 'icon-pencil',
						'btn_class' => 'edit btn',
						);
		$this->_module_settings->extrabuttons['sponsors'] = $btnSponsors;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $groups), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Sponsor Groups") => $this->uri->uri_string()));
		$this->load->view('master', $cdata);
	}

	function add($id, $type = 'event') {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');

			$this->iframeurl = "sponsors/venue/" . $id;

			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
					$this->form_validation->set_rules('name_'.$language->key, 'name', 'trim|required');
				}
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('venues', 'venues', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array(
						"name" => set_value('name_'.$app->defaultlanguage),
						//"venueid" => set_value('venues'),
						"venueid" => $venue->id,
						"order" => set_value('order')
					);

					if($newid = $this->general_model->insert('sponsorgroup', $data)){
						foreach($languages as $language) {
							$this->language_model->addTranslation('sponsorgroup', $newid, 'name', $language->key, $this->input->post('name_'.$language->key));
						}
						$this->session->set_flashdata('event_feedback', __('The group has successfully been added!'));
						_updateVenueTimeStamp($venue->id);
						redirect('sponsorgroups/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_sponsorgroup_add', array('venue' => $venue, 'venues' => $venues, 'error' => $error, 'languages' => $languages), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Sponsors") => "sponsors/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');

			$events = $this->event_model->allfromorganizer(_currentUser()->organizerId);
			$this->iframeurl = "sponsors/event/" . $id;

			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
					$this->form_validation->set_rules('name_'.$language->key, 'name', 'trim|required');
				}
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('events', 'events', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					$data = array(
						"name" => set_value('name_'.$app->defaultlanguage),
						//"eventid" => set_value('events'),
						"eventid" => $event->id,
						"order" => set_value('order')
					);

					if($newid = $this->general_model->insert('sponsorgroup', $data)){
						foreach($languages as $language) {
							$this->language_model->addTranslation('sponsorgroup', $newid, 'name', $language->key, $this->input->post('name_'.$language->key));
						}
						$this->session->set_flashdata('event_feedback', __('The sessiongroup has successfully been added!'));
						_updateTimeStamp($event->id);
						redirect('sponsorgroups/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_sponsorgroup_add', array('event' => $event, 'events' => $events, 'error' => $error, 'languages' => $languages), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Sponsors") => "sponsors/event/".$event->id, __("Sponsor Groups") => 'sponsorgroups/event/'.$event->id, __("Add new") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, __("Sponsors") => "sponsors/event/".$event->id, __("Sponsor Groups") => 'sponsorgroups/event/'.$event->id, __("Add new") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function edit($id, $type = 'event') {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			// EDIT SESSIONGROUP
			$group = $this->sponsor_model->getSponsorgroupByID($id);
			if($group == FALSE) redirect('venues');

			$venue = $this->venue_model->getById($group->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$venues = $this->venue_model->allfromorganizer(_currentUser()->organizerId);
			$this->iframeurl = "sponsors/venue/" . $venue->id;

			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
					$this->form_validation->set_rules('name_'.$language->key, 'name', 'trim|required');
				}
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('venues', 'venues', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					$data = array(
						"name" => set_value('name_'.$app->defaultlanguage),
						//"venueid" => set_value('venues'),
						"venueid" => $venue->id,
						"order" => set_value('order')
					);

					if($this->sponsor_model->edit_sponsorgroup($id, $data)){
						foreach($languages as $language) {
							$this->language_model->addTranslation('sponsorgroup', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
						}
						$this->session->set_flashdata('event_feedback', __('The group has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						redirect('sponsorgroups/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_sponsorgroup_edit', array('venue' => $venue, 'sponsorgroup' => $group, 'venues' => $venues, 'error' => $error, 'languages' => $languages), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Sponsors") => "sponsors/venue/".$venue->id, __("Edit: ") . $group->name => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			// EDIT SESSIONGROUP
			$group = $this->sponsor_model->getSponsorgroupByID($id);
			if($group == FALSE) redirect('events');

			$event = $this->event_model->getbyid($group->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			$events = $this->event_model->allfromorganizer(_currentUser()->organizerId);
			$this->iframeurl = "sponsors/event/" . $event->id;

			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
					$this->form_validation->set_rules('name_'.$language->key, 'name', 'trim|required');
				}
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('events', 'events', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array(
						"name" => set_value('name_'.$app->defaultlanguage),
						//"eventid" => set_value('events'),
						"eventid" => $event->id,
						"order" => set_value('order')
					);

					if($this->sponsor_model->edit_sponsorgroup($id, $data)){
						foreach($languages as $language) {
							$nameSuccess = $this->language_model->updateTranslation('sponsorgroup', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
							if(!$nameSuccess) {
								$this->language_model->addTranslation('sponsorgroup', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
							}
						}
						$this->session->set_flashdata('event_feedback', __('The group has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('sponsorgroups/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_sponsorgroup_edit', array('event' => $event, 'sponsorgroup' => $group, 'events' => $events, 'error' => $error, 'languages' => $languages), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Sponsors") => "sponsors/event/".$event->id, __("Sponsor Groups") => 'sponsorgroups/event/'.$event->id, __("Edit: ") . $group->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, __("Sponsors") => "sponsors/event/".$event->id, __("Sponsor Groups") => 'sponsorgroups/event/'.$event->id, __("Edit: ") . $group->name => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function delete($id, $type = 'event') {
		if($type == 'venue') {
			$group = $this->sponsor_model->getSponsorgroupByID($id);
			$venue = $this->venue_model->getbyid($group->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($this->general_model->remove('sponsorgroup', $id)){
				$this->session->set_flashdata('event_feedback', __('The group has successfully been deleted'));
				_updateVenueTimeStamp($group->venueid);
				redirect('sponsorgroups/venue/'.$group->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('sponsorgroups/venue/'.$group->venueid);
			}
		} else {
			$group = $this->sponsor_model->getSponsorgroupByID($id);
			$event = $this->event_model->getbyid($group->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			if($this->general_model->remove('sponsorgroup', $id)){
				$this->session->set_flashdata('event_feedback', __('The group has successfully been deleted'));
				_updateTimeStamp($group->eventid);
				redirect('sponsorgroups/event/'.$group->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('sponsorgroups/event/'.$group->eventid);
			}
		}
	}

	function sort($type = '') {
		if($type == '' || $type == FALSE) redirect('events');

		switch($type){
			case "sponsors":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('sponsor', $data);
					$this->db->last_query();
				}
				break;
			case "groups":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('sponsorgroup', $data);
					$this->db->last_query();
				}
				break;
			default:
				break;
		}
	}

}