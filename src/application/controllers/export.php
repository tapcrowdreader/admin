<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Export extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	//php 4 constructor
	function Export() {
		parent::__construct();
		if(_authed()) { }
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		if($event == FALSE) redirect('events');
		if($event->organizerid != _currentUser()->organizerId) redirect('events');

		$cdata['content'] 		= $this->load->view('c_excelimport', array('event' => $event), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Import") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	/*function session($id) {

		if($id == FALSE || $id == 0) redirect('sessions');

		$this->load->model('general_model');
		$this->load->model('sessions_model');
		$this->load->model('venue_model');
		$this->load->model('event_model');

		$session = $this->sessions_model->getSessionByID($id);
		//echo("<pre>");
		//print_r($session);
		//echo("</pre>");

		$sessiongroupid = $session->sessiongroupid;
		$eventid = $session->eventid;

		$event = $this->event_model->getbyid($eventid);

		if($event == FALSE) redirect('events'); //If event is not found
		if($event->organizerid != _currentUser()->organizerId) redirect('events'); //event organized and current user doesn't match.

		//echo("<pre>");
		//print_r($event);
		//echo("</pre>");

		$sessiongroup = $this->sessions_model->getSessiongroupByID($sessiongroupid);
		$venueid = $sessiongroup->venueid;

		//echo("<pre>");
		//print_r($sessiongroup);
		//echo("</pre>");

		$venue = $this->venue_model->getById($venueid);

		//echo("<pre>");
		//print_r($venue);
		//echo("</pre>");

		$tab = "\t";
		$endln = "\r\n";

    $headers =  "sessiongroup name ".$tab.
				"id ".$tab.
				"name ".$tab.
				"description ".$tab.
				"starttime ".$tab.
				"endtime ".$tab.
				"speaker ".$tab.
				"location ".$tab.
				"url ".$endln;

	$values .=  $sessiongroup->name.$tab.
			    $session->id.$tab.
			    $session->name.$tab.
				$session->description.$tab.
				$session->starttime.$tab.
				$session->endtime.$tab.
				$session->speaker.$tab.
				$session->location.$tab.
				$session->url.$endln;

	$contents = $headers.$values;

	$filename ="sessionexport.xls";
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename='.$filename);
	echo $contents;

	exit;

	}*/

	function sessions($eventid) {

		if($eventid == FALSE || $eventid == 0) redirect('events');

		$this->load->model('general_model');
		$this->load->model('sessions_model');
		$this->load->model('venue_model');
		$this->load->model('event_model');


		$event = $this->event_model->getbyid($eventid);

		if($event == FALSE) redirect('events'); //If event is not found
		if($event->organizerid != _currentUser()->organizerId) redirect('events'); //event organized and current user doesn't match.

		$eventsessions = $this->sessions_model->getSessionByEventID($eventid);

		$tab = "\t";
		$endln = "\r\n";

		if($eventsessions)
		{
			$values = '';

			//echo("<pre>");
			//print_r($eventsessions);
			//echo("</pre>");

			$headers =  "sessiongroup name ".$tab.
						"id ".$tab.
						"name ".$tab.
						"description ".$tab.
						"starttime ".$tab.
						"endtime ".$tab.
						"speaker ".$tab.
						"location ".$tab.
						"url ".$endln;



			foreach ($eventsessions as $key=>$session)
			{
				//$session = $this->sessions_model->getSessionByID($id);
				$sessiongroupid = $session->sessiongroupid;
				$sessiongroup = $this->sessions_model->getSessiongroupByID($sessiongroupid);

				$values .= $sessiongroup->name.$tab.
						  $session->id.$tab.
						  $session->name.$tab.
						  $session->description.$tab.
						  $session->starttime.$tab.
						  $session->endtime.$tab.
						  $session->speaker.$tab.
						  $session->location.$tab.
						  $session->url.$endln;

			}

			$contents = $headers.$values;
		}
		else
		{
			$contents = __('No session found for this event');
		}

		$filename ="eventsessionexport.xls";
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='.$filename);
		echo $contents;

		exit;

	}

}