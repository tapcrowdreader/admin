<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Contactus extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	/**
	 * Displays the contactus page
	 */
	public function index()
	{
		$channel_model = \Tapcrowd\Model\Channel::getInstance();
		$channel = $channel_model->getCurrentChannel();

		# Handle 'ContactUs' Email
		if($this->input->post('postback') == "postback") {
			if($this->_sendContactMail()) redirect('apps');
		}

		# Output
		$this->load->view('master_wizard', array(
			'content' => $this->load->view('c_contactus', array('channel' => $channel), true),
		));
	}

	/**
	 * Sends the input from the form to the channel support email.
	 * (the email is also forwarded to info[at]tapcrowd[dot]com).
	 * @return bool
	 */
	protected function _sendContactMail()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
		$this->form_validation->set_rules('message', 'message', 'trim|required');
		if($this->form_validation->run()) {

			$channel_model = \Tapcrowd\Model\Channel::getInstance();
			$channel = $channel_model->getCurrentChannel();

			# Send mail
			$title = __('ContactUs Form from %s Backend', $channel->name);
			$from_email = $channel->support->email;
			$bcc = 'info@tapcrowd.com';
			$headers = "From: $from_email\r\nBcc: $bcc";
			$headers.= "\r\nContent-type: text/html; charset=utf-8\r\n";
			$message = __('New message from %s.<br />Email: %s<br /><br />Message: %s',
				$this->input->post('name'),
				$this->input->post('email'),
				nl2br($this->input->post('message'))
			);
			mail($from_email, $title, $message, $headers);

			\Notifications::getInstance()->alert('Your message has been sent.', 'success');
			return true;
		} else {
			\Notifications::getInstance()->alert('Your message has NOT been sent.', 'error');
			return false;
		}
	}
}
