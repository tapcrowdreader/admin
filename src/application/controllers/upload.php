<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Upload extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Upload() {
		parent::__construct();
	}
	
	function index() {
		die('Not supposed to be here');
	}
	
	function delete() {
		unlink($this->input->post('file'));
	}
	
	// Uploads app submition
	function apphomescreen() {
		if ($this->input->post('postback') == 'postback') {
			$config['upload_path'] 		= './upload/homescreens/';
			$config['allowed_types'] 	= 'JPG|gif|jpeg|jpg|png';
			$config['max_width'] 		= '640';
			$config['max_height'] 		= '960';
			$config['max_size'] 		= '4096';
			$config['file_name'] 		= date("Ymd", time()) . '_' . $this->_generateName();
			
			$this->load->library('upload', $config);
			
			$obj->name = "";
			$obj->type = "";
			$obj->size = "";
			$obj->path = "";
			
			if (!$this->upload->do_upload('file')) {
				// Error, fileupload failed
				echo json_encode($obj);
			} else {
				//successfully uploaded
				$file = $this->upload->data();
				
				$obj->name = $file['file_name'];
				$obj->type = $file['file_type'];
				$obj->size = $file['file_size'];
				$obj->path = $config['upload_path'].$file['file_name'];
				
				echo json_encode($obj);
			}
		} else {
			die('Failed');
		}
	}
	
	function appicon() {
		if ($this->input->post('postback') == 'postback') {
			$config['upload_path'] 		= './upload/appicons/';
			$config['allowed_types'] 	= 'JPG|gif|jpeg|jpg|png';
			$config['max_width'] 		= '512';
			$config['max_height'] 		= '512';
			$config['max_size'] 		= '4096';
			$config['file_name'] 		= date("Ymd", time()) . '_' . $this->_generateName();
			
			$this->load->library('upload', $config);
			
			$obj->name = "";
			$obj->type = "";
			$obj->size = "";
			$obj->path = "";
			
			if (!$this->upload->do_upload('file')) {
				// Error, fileupload failed
				echo json_encode($obj);
			} else {
				//successfully uploaded
				$file = $this->upload->data();
				
				$obj->name = $file['file_name'];
				$obj->type = $file['file_type'];
				$obj->size = $file['file_size'];
				$obj->path = $config['upload_path'].$file['file_name'];
				
				echo json_encode($obj);
			}
		} else {
			die('Failed');
		}
	}
	
	function _generateName($length = 12) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$string = '';    
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters)-1)];
		}
		return $string;
	}
	
}