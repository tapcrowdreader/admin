<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Ad extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('ad_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Ad',
			'plural' => 'Ads',
			'title' => __('Ads'),
			'parentType' => 'event',
			'browse_url' => 'ad/--parentType--/--parentId--',
			'add_url' => 'ad/add/--parentId--/--parentType--',
			'edit_url' => 'ad/edit/--id--',
			'module_url' => 'module/editByController/ad/--parentType--/--parentId--/0',
			'headers' => array(
				__('Title') => 'name',
				__('Order') => 'order'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'ad/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'ad/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'checkboxes' => false,
			'deletecheckedurl' => 'ad/removemany/--parentId--/--parentType--'
		);
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = "event/index/" . $id;

		$ads = $this->ad_model->getAdsByEventID($id);
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		$launcher = $this->module_mdl->getLauncher(29, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($ads)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'ad');
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->iframeurl = "venue/index/" . $id;

		$ads = $this->ad_model->getAdsByVenueID($id);

		$this->_module_settings->parentType = 'venue';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'venue', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'venue', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $deletecheckedurl);

		$launcher = $this->module_mdl->getLauncher(29, 'venue', $venue->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($ads)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $this->_module_settings->title => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'ad');
		$this->load->view('master', $cdata);
	}

	function app($id) {
		if($id == FALSE || $id == 0) redirect('apps');
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app','');

		$this->iframeurl = "city/index/" . $id;

		$ads = $this->ad_model->getAdsByAppID($id);
		$this->_module_settings->parentType = 'app';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('app', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('app', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'app', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'app', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('app', $id), $deletecheckedurl);

		$launcher = $this->module_mdl->getLauncher(29, 'app', $app->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($ads)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);

		$cdata['crumb']			= array($this->_module_settings->title => $this->uri->uri_string());
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app, 'ad');
		$this->load->view('master', $cdata);
	}

    function add($id, $type = 'event') {
		if($type == 'venue'){
			$this->load->library('form_validation');
			$error = "";

			$this->iframeurl = "venue/index/" . $id;

			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');

			$launcher = $this->module_mdl->getLauncher(29, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('image', 'image', 'trim');
                $this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('time', 'time', 'trim|required');
				$this->form_validation->set_rules('website', 'website', 'trim');

				if($this->form_validation->run() == FALSE){
					$error .= validation_errors();
				} else {
					$website = checkhttp($this->input->post('website'));
					$data = array(
							"name"      => set_value('name'),
							"venueid" 	=> $venue->id,
							"order"		=> set_value('order'),
							"time"		=> set_value('time'),
							"website"	=> $website
						);

					if($adid = $this->general_model->insert('ad', $data)){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/adimages/".$adid)){
							mkdir($this->config->item('imagespath') . "upload/adimages/".$adid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$adid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = ""; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/adimages/'. $adid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('ad', $adid, array('image' => $image));

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$adid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image_ipad')) {
							$image_ipad = ""; //No image uploaded!
							if($_FILES['image_ipad']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image_ipad = 'upload/adimages/'. $adid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('ad', $adid, array('image_ipad' => $image_ipad));

						$this->session->set_flashdata('event_feedback', __('The ad has successfully been added!'));
						_updateVenueTimeStamp($venue->id);
						if($error == '') {
							redirect('ad/venue/'.$venue->id);
						} else {
							redirect('ad/edit/'.$adid.'/venue?error=image');
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_ad_add', array('venue' => $venue, 'error' => $error, 'app' => $app), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "ad/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'ad');
			$this->load->view('master', $cdata);
		} elseif($type == 'app'){
			$this->load->library('form_validation');
			$error = "";

			$this->iframeurl = "city/index/" . $id;

			$app = $this->app_model->get($id);
			_actionAllowed($app, 'app');

			$launcher = $this->module_mdl->getLauncher(29, 'app', $app->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('image', 'image', 'trim');
                $this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('time', 'time', 'trim|required');
				$this->form_validation->set_rules('website', 'website', 'trim');

				if($this->form_validation->run() == FALSE){
					$error .= validation_errors();
				} else {
					$website = checkhttp($this->input->post('website'));
					$data = array(
							"name"      => set_value('name'),
							"appid" 	=> $app->id,
							"order"		=> set_value('order'),
							"time"		=> set_value('time'),
							"website"	=> $website
						);

					if($adid = $this->general_model->insert('ad', $data)){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/adimages/".$adid)){
							mkdir($this->config->item('imagespath') . "upload/adimages/".$adid, 0775, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$adid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = ""; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/adimages/'. $adid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('ad', $adid, array('image' => $image));

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$adid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image_ipad')) {
							$image_ipad = ""; //No image uploaded!
							if($_FILES['image_ipad']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image_ipad = 'upload/adimages/'. $adid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('ad', $adid, array('image_ipad' => $image_ipad));

						$this->session->set_flashdata('event_feedback', __('The ad has successfully been added!'));
						_updateVenueTimeStamp($app->id);
						if($error == '') {
							redirect('ad/app/'.$app->id);
						} else {
							redirect('ad/edit/'.$adid.'/app?error=image');
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_ad_add', array('app' => $app, 'error' => $error, 'app' => $app), TRUE);
			$cdata['crumb']			= array($this->_module_settings->title => "ad/app/".$app->id, __("Add new") => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app, 'ad');
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');

			$this->iframeurl = "event/index/" . $id;

			$launcher = $this->module_mdl->getLauncher(29, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('image', 'image', 'trim');
                $this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('time', 'time', 'trim|required');
				$this->form_validation->set_rules('website', 'website', 'trim');

				if($this->form_validation->run() == FALSE){
					$error .= validation_errors();
				} else {
					$website = checkhttp($this->input->post('website'));
					$data = array(
							"name"      => set_value('name'),
							"eventid" 	=> $event->id,
							"order"		=> set_value('order'),
							"time"		=> set_value('time'),
							"website"	=> $website
						);

					if($adid = $this->general_model->insert('ad', $data)){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/adimages/".$adid)){
							mkdir($this->config->item('imagespath') . "upload/adimages/".$adid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$adid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = ""; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/adimages/'. $adid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('ad', $adid, array('image' => $image));

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$adid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image_ipad')) {
							$image_ipad = ""; //No image uploaded!
							if($_FILES['image_ipad']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image_ipad = 'upload/adimages/'. $adid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('ad', $adid, array('image_ipad' => $image_ipad));

						$this->session->set_flashdata('event_feedback', __('The ad has successfully been added!'));
						_updateTimeStamp($event->id);
						if($error == '') {
							redirect('ad/event/'.$event->id);
						} else {
							redirect('ad/edit/'.$adid.'/event?error=image');
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_ad_add', array('event' => $event, 'error' => $error, 'app' => $app), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "sponsors/event/".$event->id, __("Add new") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "sponsors/event/".$event->id, __("Add new") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'ad');
			$this->load->view('master', $cdata);
		}
	}

    function edit($id, $type) {
		if($type == 'venue'){
			$this->load->library('form_validation');
			$error = "";

			// EDIT SPONSOR
			$ad = $this->ad_model->getAdById($id);
			if($ad == FALSE) redirect('venues');
			$venue = $this->venue_model->getbyid($ad->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			$venue = $this->venue_model->getById($ad->venueid);

			$this->iframeurl = "venue/index/" . $ad->venueid;

			$launcher = $this->module_mdl->getLauncher(29, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('image', 'image', 'trim');
                $this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('time', 'time', 'trim|required');
				$this->form_validation->set_rules('website', 'website', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/adimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/adimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = $ad->image; //No image uploaded!
						if($_FILES['image']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/adimages/'. $id . '/' . $returndata['file_name'];
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image_ipad')) {
						$image_ipad = $ad->image_ipad; //No image uploaded!
						if($_FILES['image_ipad']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image_ipad = 'upload/adimages/'. $id . '/' . $returndata['file_name'];
					}

					$website = checkhttp($this->input->post('website'));
					$data = array(
							"name" => set_value('name'),
							"image"	=> $image,
							"image_ipad"	=> $image_ipad,
							"order"	=> set_value('order'),
							"time"	=> set_value('time'),
							"website"	=> $website
						);

					if($this->general_model->update('ad', $id, $data)){
						$this->session->set_flashdata('event_feedback', __('The ad has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						if($error == '') {
							redirect('ad/venue/'.$venue->id);
						} else {
							redirect('ad/edit/'.$id.'/venue?error=image');
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_ad_edit', array('venue' => $venue, 'ad' => $ad, 'error' => $error, 'app' => $app), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "ad/venue/".$venue->id, __("Edit: ") . $ad->name => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'ad');
			$this->load->view('master', $cdata);
		} elseif($type == 'app'){
			$this->load->library('form_validation');
			$error = "";

			// EDIT SPONSOR
			$ad = $this->ad_model->getAdById($id);
			if($ad == FALSE) redirect('apps');
			$app = $this->app_model->get($ad->appid);
			_actionAllowed($app, 'app');

			$this->iframeurl = "city/index/" . $ad->appid;

			$launcher = $this->module_mdl->getLauncher(29, 'app', $app->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('image', 'image', 'trim');
                $this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('time', 'time', 'trim|required');
				$this->form_validation->set_rules('website', 'website', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/adimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/adimages/".$id, 0775, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = $ad->image; //No image uploaded!
						if($_FILES['image']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/adimages/'. $id . '/' . $returndata['file_name'];
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image_ipad')) {
						$image_ipad = $ad->image_ipad; //No image uploaded!
						if($_FILES['image_ipad']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image_ipad = 'upload/adimages/'. $id . '/' . $returndata['file_name'];
					}

					$website = checkhttp($this->input->post('website'));
					$data = array(
							"name" => set_value('name'),
							"image"	=> $image,
							"image_ipad"	=> $image_ipad,
							"order"	=> set_value('order'),
							"time"	=> set_value('time'),
							"website"	=> $website
						);

					if($this->general_model->update('ad', $id, $data)){
						$this->session->set_flashdata('event_feedback', __('The ad has successfully been updated'));
						$this->general_model->update('app', $app->id, array('timestamp' => time()));
						if($error == '') {
							redirect('ad/app/'.$app->id);
						} else {
							redirect('ad/edit/'.$id.'/app?error=image');
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_ad_edit', array('ad' => $ad, 'error' => $error, 'app' => $app), TRUE);
			$cdata['crumb']			= array($this->_module_settings->title => "ad/app/".$app->id, __("Edit: ") . $ad->name => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app, 'ad');
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$ad = $this->ad_model->getAdById($id);
			if($ad == FALSE) redirect('events');

			$event = $this->event_model->getbyid($ad->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			$this->iframeurl = "event/index/" . $ad->eventid;

			$launcher = $this->module_mdl->getLauncher(29, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('image', 'image', 'trim');
                $this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('time', 'time', 'trim|required');
				$this->form_validation->set_rules('website', 'website', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/adimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/adimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = $ad->image; //No image uploaded!
						if($_FILES['image']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/adimages/'. $id . '/' . $returndata['file_name'];
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/adimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image_ipad')) {
						$image_ipad = $ad->image_ipad; //No image uploaded!
						if($_FILES['image_ipad']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image_ipad = 'upload/adimages/'. $id . '/' . $returndata['file_name'];
					}

					$website = checkhttp($this->input->post('website'));
					$data = array(
							"name" => set_value('name'),
							"image"	=> $image,
							"image_ipad"	=> $image_ipad,
							"order"	=> set_value('order'),
							"time"	=> set_value('time'),
							"website"	=> $website
						);

					if($this->general_model->update('ad', $id, $data)){
						//add translated fields to translation table
						$this->session->set_flashdata('event_feedback', __('The ad has successfully been updated'));
						_updateTimeStamp($event->id);
						if($error == '') {
							redirect('ad/event/'.$event->id);
						} else {
							redirect('ad/edit/'.$id.'/event?error=image');
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_ad_edit', array('event' => $event, 'ad' => $ad, 'error' => $error, 'app' => $app), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "ad/event/".$event->id, __("Edit: ") . $ad->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "ad/event/".$event->id, __("Edit: ") . $ad->name => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'ad');
			$this->load->view('master', $cdata);
		}
	}

	function delete($id, $type) {
		if($type == 'venue'){
			$ad = $this->ad_model->getAdById($id);
			$venue = $this->venue_model->getbyid($ad->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($this->general_model->remove('ad', $id)){
				$this->session->set_flashdata('event_feedback', __('The ad has successfully been deleted'));
				_updateVenueTimeStamp($ad->venueid);
				redirect('ad/venue/'.$ad->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('ad/venue/'.$ad->venueid);
			}
		} elseif($type == 'app'){
			$ad = $this->ad_model->getAdById($id);
			$app = $this->app_model->get($ad->appid);
			_actionAllowed($app, 'app');
			if($this->general_model->remove('ad', $id)){
				$this->session->set_flashdata('event_feedback', __('The ad has successfully been deleted'));
				$this->general_model->update('app', $app->id, array('timestamp' => time()));
				redirect('ad/app/'.$ad->appid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('ad/app/'.$ad->appid);
			}
		} else {
			$ad = $this->ad_model->getAdById($id);
			$event = $this->event_model->getbyid($ad->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			if($this->general_model->remove('ad', $id)){
				$this->session->set_flashdata('event_feedback', __('The ad has successfully been deleted'));
				_updateTimeStamp($ad->eventid);
				redirect('ad/event/'.$ad->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('ad/event/'.$ad->eventid);
			}
		}
	}

	function removeimage($id, $type = 'event') {
		$ad = $this->ad_model->getAdById($id);
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($ad->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $ad->image)) redirect('venues');

			// Delete image + generated thumbs

			unlink($this->config->item('imagespath') . $ad->image);
			unlink($this->config->item('imagespath') . $ad->image_ipad);
			$this->session->set_flashdata('event_feedback', 'Image has successfully been removed.');

			$data = array('image' => '');
			$this->general_model->update('ad', $id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('ad/venue/'.$venue->id);
		} else {
			$event = $this->event_model->getbyid($ad->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $ad->image)) redirect('events');

			// Delete image + generated thumbs

			unlink($this->config->item('imagespath') . $ad->image);
			unlink($this->config->item('imagespath') . $ad->image_ipad);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('image' => '');
			$this->general_model->update('ad', $id, $data);

			_updateTimeStamp($event->id);
			redirect('ad/event/'.$event->id);
		}
	}

    function crop($id, $type = 'event') {
        $error = '';
        $ad = $this->ad_model->getAdById($id);
        if($ad == FALSE) redirect('events');
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($ad->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
		} else{
            $event = $this->event_model->getbyid($ad->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
		}

        $adpicwidth = 640;
        $adpicheight = 100;

        if($this->input->post('postback') == "postback") {
           $w = $this->input->post('w');
           $h = $this->input->post('h');
           $x = $this->input->post('x');
           $y = $this->input->post('y');

		   $newname = 'upload/adimages/'.$ad->id.'/cropped_'.time().'.jpg';
           $config['image_library']  = 'gd2';
           $config['source_image']  = $this->config->item('imagespath') . $ad->image;
           $config['create_thumb']  = FALSE;
           $config['maintain_ratio']  = FALSE;
           $config['width']    = $w;
           $config['height']   = $h;
           $config['x_axis']   = $x;
           $config['y_axis']   = $y;
           $config['dynamic_output']  = FALSE;
           $config['new_image'] = $this->config->item('imagespath') . $newname;

           $this->load->library('image_lib', $config);
           $this->image_lib->initialize($config);

           if (!$this->image_lib->crop())
           {
			   $newname = $ad->image;
			   echo $this->image_lib->display_errors();
           }

			$data = array(
					"image"	=> $ad->image
				);

			$this->general_model->update('ad', $ad->id, $data);
			$ad->image = $newname;
        }

        if($type == 'venue') {
            $venue = $this->venue_model->getbyid($ad->venueid);
            $cdata['content']  	= $this->load->view('c_ad_crop', array('venue' => $venue, 'ad' => $ad, 'error' => $error, 'adpicheight' => $adpicheight, 'adpicwidth' => $adpicwidth), TRUE);
            $cdata['crumb']		= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$event->id, __("Ads") => "ad/venue/".$venue->id, __("Edit Picture: ") . $ad->name => $this->uri->uri_string()));
            $cdata['sidebar'] 	= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
            $cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'ad');
            $this->load->view('master', $cdata);
        } else {
            $event = $this->event_model->getbyid($ad->eventid);
            $cdata['content'] 		= $this->load->view('c_ad_crop', array('event' => $event, 'ad' => $ad, 'error' => $error, 'adpicheight' => $adpicheight, 'adpicwidth' => $adpicwidth), TRUE);
            if($app->familyid != 1) {
	            $cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Ads") => "ad/event/".$event->id, __("Edit Picture: ") . $ad->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
	        } else {
	            $cdata['crumb']			= array($event->name => "event/view/".$event->id, __("Ads") => "ad/event/".$event->id, __("Edit Picture: ") . $ad->name => $this->uri->uri_string());
	        }
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
            $cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'ad');
            $this->load->view('master', $cdata);
        }
    }

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'ad');
    }   
}
