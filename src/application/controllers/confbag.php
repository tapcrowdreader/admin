<?php if(!defined('BASEPATH')) exit('No direct script access');

class Confbag extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('confbag_model');
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$launcher = $this->module_mdl->getLauncher(42, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$title = $launcher->title;
		
		$cdata['content'] 		= $this->load->view('c_confbag_event', array('event' => $event, 'title' => $title, 'app' => $app), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array("Events" => "events", $event->name => "event/view/".$id, $title => $this->uri->uri_string());
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'confbag');
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$launcher = $this->module_mdl->getLauncher(42, 'venue', $venue->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$title = $launcher->title;
		
		$cdata['content'] 		= $this->load->view('c_confbag_venue', array('venue' => $venue, 'title' => $title, 'app' => $app), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $title => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'confbag');
		$this->load->view('master', $cdata);
	}
	
	function removeitem($id, $type = 'event', $controller) {
		$item = $this->confbag_model->getById($id);
		if($type == 'venue') {
			$venue = $this->venue_model->getById($item->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->documentlink)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->documentlink);
			$this->session->set_flashdata('event_feedback', 'Item has successfully been removed.');

			$this->general_model->remove('confbag', $id);

			_updateVenueTimeStamp($venue->id);
			redirect($controller.'/edit/'.$item->tableid);
		} else {
			$event = $this->event_model->getbyid($item->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->documentlink)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->documentlink);
			$this->session->set_flashdata('event_feedback', 'Item has successfully been removed.');

			$this->general_model->remove('confbag', $id);

			_updateTimeStamp($event->id);
			redirect($controller.'/edit/'.$item->tableid);
		}
	}

	function sendconfbagmail($eventid) {
		// if($eventid == FALSE || $eventid == 0) redirect('events');

		// $event = $this->event_model->getbyid($eventid);
		// $app = _actionAllowed($event, 'event','returnApp');

		// //mail settings
		// $subject = $event->name . ' - Your conference bag.';
		// $headers = "From: interact@iab.emsecure.net\r\nReply-To: interact@iab.emsecure.net";
		// $headers .= "\r\nBcc: jens.verstuyft@tapcrowd.com";
		// $headers .=  "\r\n".'Content-type: text/html; charset=utf-8' . "\r\n"; 

		// $data = $this->confbag_model->getConfbagdata($eventid);
		// $current_email = $data[0]->email;
		// $current_data = array();
		// $startdate = '';

		// if($event->id == 1318) {
		// 	$event->eventlogo = 'upload/appimages/850/navbar-logo2.png';
		// }
		// foreach($data as $row) {
		// 	if(trim(strtolower($row->email)) != trim(strtolower($current_email))) {
		// 		$datei = 0;
		// 		$ibg = 0;
		// 		//send mail to current_email with current_data and clear both
		// 		$to = $current_email;
		// 		$message = '<div id="container" style="font-family:helvetica, arial, sans-serif;color:#000 !important; line-height:20px;font-size:12px;margin:0 auto; width:600px;">
		// 						<div id="logo" style="text-align:center;padding:10px 0px;background-color:#fff;margin-bottom:25px;">
		// 							<img style="max-height:100px;" src="'.($event->eventlogo == '' ? 'http://tapcrowd.com/sites/all/themes/tapcrowd/logo.png' : 'http://upload.tapcrowd.com/'.$event->eventlogo) .'" alt="">
		// 						</div>
		// 						<div id="content" style="background-color:#ddd;padding:20px;">
		// 							Hi, <br/>
		// 							Thank you for visiting <strong>'.$event->name.'</strong>. We hope you enjoyed using our app. Below you will find a list of everything you subscribed for. If there is content available you can find the download link next to it.<br/>';

		// 		$current_object = '';
		// 		foreach($current_data as $current_row) {
		// 			//group objects
		// 			if($current_object != $current_row->object) {
		// 				if($current_row->object == 'attendee' || $current_row->object == 'session' || $current_row->object == 'session') {
		// 					$datei = 0;
		// 					$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."s</h2>";
		// 					$message .= '<div style="margin-left:8px;">';
		// 				} else {
		// 					$datei = 0;
		// 					$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."</h2>";
		// 					$message .= '<div style="margin-left:8px;">';
		// 				}
						
		// 				$current_object = $current_row->object;
		// 			}

		// 			//session times
		// 			if($current_row->starttime != '' && $startdate != substr($current_row->starttime,0,10)) {
		// 				$startdate = substr($current_row->starttime,0,10);
		// 				$message .= '<h3 style="margin-bottom:0px;padding-top:10px;color:#26A8E0;margin-left:-8px;'.($datei == 0 ? 'margin-top:0px;padding-top:0px;' : 'padding-top:0px;') .'">'.$startdate."</h3>";
		// 			} else {
		// 				$startdate != substr($current_row->starttime,0,10);
		// 			}
		// 			$datei++;
					
		// 			if($current_object == 'attendee') {
		// 				$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. $current_row->firstname . ($current_row->company != '' ? ' (' . $current_row->company . ($current_row->function != '' ? ' - ' . $current_row->function : '') . ')' : '') . '<br/>' . 
		// 							'<a href="'.$row->email.'" style="color:#26A8E0;">'.$row->email.'</a>' . ($row->linkedin != '' ? ' - <a href="'.$row->linkedin.'" style="color:#26A8E0;">LinkedIn</a>' : ' ');
		// 			} else {
		// 				$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. $current_row->firstname;
		// 			}

		// 			//document links
		// 			$documentlinks = explode(',', $current_row->documentlink);
		// 			if($current_row->documentlink != '' && $current_object == 'attendee') {
		// 				$message .= '<br/>';
		// 			}
		// 			$i = 1;
		// 			foreach($documentlinks as $documentlink) {
		// 				if(substr($documentlink, 0, 7) != 'http://' && substr($documentlink, 0, 8) != 'https://' && $documentlink != '') {
		// 					$documentlink = utf8_encode('http://upload.tapcrowd.com/'.$documentlink);
		// 				}
		// 				if($documentlink != '') {
		// 					if($current_object == 'attendee') {
		// 						$message .=	'<a href="'.$documentlink.'" style="color:#26A8E0;">Document'.$i.'</a> ';
		// 					} elseif($current_object == 'other') {
		// 						$message .= '<a href="'.$documentlink.'" style="color:#26A8E0;">'.$documentlink.'</a><br/>';
		// 					} else {
		// 						$message .= ' - ' . '<a href="'.$documentlink.'" style="color:#26A8E0;">Document'.$i.'</a> ';
		// 					}
		// 				} 
		// 				$i++;
		// 			}

		// 			$message .= '<br clear="both"/>';
		// 			$ibg++;						
		// 		}
				
		// 		$message .= '<br/>If you have any question, please contact us.<br/>
		// 						Thank you,<br/>
		// 						<a href="mailto:events@iabeurope.eu" style="color:#26A8E0">The Interact team</a></div></div>';
		// 		echo $message;
		// 		// $mail_sent = @mail( $to, $subject, $message, $headers );
		// 		$current_data = array();
		// 		$current_email = $row->email;
		// 	}
		// 	$current_data[] = $row;
		// }
	}

	function mail($eventid, $type = 'event') {
		if($type == 'venue') {
			if($eventid == FALSE || $eventid == 0) redirect('events');

			$venue = $this->venue_model->getById($eventid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			$launcher = $this->module_mdl->getLauncher(42, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				//mail settings
				$ownmail = $this->input->post('email');
				$subject = $venue->name . ' - Your personal '. $launcher->title;
				ini_set('sendmail_from', $ownmail);
				$headers = "From: ".$ownmail;
				// $headers .= "\r\nBcc: jens.verstuyft@tapcrowd.com";
				$headers .=  "\r\n".'Content-type: text/html; charset=utf-8' . "\r\n"; 

				$data = $this->confbag_model->getConfbagdata($venue->id, 'venue');
				$current_email = $data[0]->email;
				$current_data = array();
				$startdate = '';

				$appicon = '';
				$appiconres = $this->db->query("SELECT * FROM appearance WHERE controlid = 5 AND appid = $app->id LIMIT 1");
				if($appiconres->num_rows() > 0) {
					$appicon = $appiconres->row()->value;
				}

				$mailheader = false;
				$mailfooter = false;
				$mailheaderRes = $this->db->query("SELECT * FROM confbagmail WHERE appid = $app->id LIMIT 1");
				if($mailheaderRes->num_rows() > 0) {
					$mailheader = $mailheaderRes->row()->header;
					$mailfooter = $mailheaderRes->row()->footer;
					if(!empty($mailheaderRes->row()->subject)) {
						$subject = $mailheaderRes->row()->subject;
					}
				}

				$iEmails = 0;
				$ibg = 0;
				$data[] = (object) array('email' => 'zzzzzzzzzzzzz@tapcrowd.com');
				foreach($data as $row) {
					if(trim(strtolower($row->email)) != trim(strtolower($current_email)) || $iEmails == count($data) - 1) {
						$datei = 0;
						$ibg = 0;
						//send mail to current_email with current_data and clear both
						$to = $current_email;
						if($mailheader !== false) {
							$message = $mailheader;
						} else {
							$message = '<div id="container" style="font-family:helvetica, arial, sans-serif;color:#000 !important; line-height:20px;font-size:12px;margin:0 auto; width:600px;">
										<div id="logo" style="text-align:center;padding:10px 0px;background-color:#fff;margin-bottom:25px;">
											<img style="max-height:100px;" src="'.($appicon == '' ? 'http://tapcrowd.com/sites/all/themes/tapcrowd/logo.png' : 'http://upload.tapcrowd.com/'.$appicon) .'" alt="">
										</div>
										<div id="content" style="background-color:#ddd;padding:20px;">
											'. __("Dear Sir/ Madam,"). '<br/>
											' . __("Thank you for visiting <strong>%s</strong>. We hope you enjoyed using our app. Below you will find an overview of all items you subscribed for. If there is content available you can find the download link next to it. <br/>", $venue->name);
						}

						$current_object = '';
						foreach($current_data as $current_row) {
							//group objects
							if($current_object != $current_row->object) {
								if($current_row->object == 'catalog') {
									$launcher = $this->db->query("SELECT title FROM launcher WHERE venueid = $venueid AND moduletypeid = 15 LIMIT 1");
									if($launcher->num_rows() > 0) {
										$current_row->title = $launcher->row()->title;
									}
									$datei = 0;
									if(isset($current_row->title)) {
										$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->title)."</h2>";
									} else {
										$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."s</h2>";
									}
									$message .= '<div style="margin-left:8px;">';
								} else {
									$datei = 0;
									$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."</h2>";
									$message .= '<div style="margin-left:8px;">';
								}
								
								$current_object = $current_row->object;
							}

							$message .= '<span style="margin-left:-8px;">* </span>' . $current_row->name;


							//document links
							$documentlinks = explode(',', $current_row->documentlink);
							$i = 1;
							foreach($documentlinks as $documentlink) {
								if(substr($documentlink, 0, 7) != 'http://' && substr($documentlink, 0, 8) != 'https://' && $documentlink != '') {
									$documentlink = utf8_encode('http://upload.tapcrowd.com/'.$documentlink);
								}
								if($documentlink != '') {
									$message .= ' - ' . '<a href="'.$documentlink.'" style="color:#26A8E0;">Document'.$i.'</a> ';
								} 
								$i++;
							}

							if($current_row->object == 'catalog' && !empty($current_row->pdf)) {
								$message .= ' - <a href="http://upload.tapcrowd.com/'. $current_row->pdf.'">PDF</a> ';
							}
							if($current_row->object == 'catalog' && !empty($current_row->url)) {
								$message .= ' - <a href="'.$current_row->url.'">'.$current_row->urltitle.'</a> ';
							}

							$message .= '<br clear="both"/>';
							$ibg++;						
						}
						
						if($mailfooter !== false) {
							$message .= $mailfooter;
						} else {
							$message .= '<br/>'.__("We hope to welcome you again.<br/>
										Kind regards,<br/>").
										'<a href="mailto:'.$ownmail.'" style="color:#26A8E0">'.$app->name . ' ' . __('Team') . '</a></div></div>';
						}
						$mail_sent = mail( $to, $subject, $message, $headers );
						$current_data = array();
						$current_email = $row->email;
					}
					$current_data[] = $row;
					$iEmails++;
				}

				$this->session->set_flashdata('event_feedback', __('The mails have been sent'));
				redirect('confbag/venue/'.$venue->id);
			}

			$cdata['content'] 		= $this->load->view('c_confbag_venue_mail', array('venue' => $venue, 'title' => $title), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $title => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'confbag');
			$this->load->view('master', $cdata);
		} else {
			if($eventid == FALSE || $eventid == 0) redirect('events');

			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			$launcher = $this->module_mdl->getLauncher(42, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				//mail settings
				$ownmail = $this->input->post('email');
				// if($app->id == 2187) {
				// 	//euroclear specific mail
				// 	redirect('dummy/euroclearconfbag/'.$app->id.'/'.$event->id.'/'.$launcher->id.'?email='.$ownmail);
				// }
				$subject = $event->name . ' - Your personal '. $launcher->title;
				ini_set('sendmail_from', $ownmail);
				$headers = "From: ".$ownmail;
				// $headers .= "\r\nBcc: jens.verstuyft@tapcrowd.com";
				$headers .=  "\r\n"."Content-type: text/html; charset=utf-8\r\nContent-Transfer-Encoding: base64\r\n";

				$data = $this->confbag_model->getConfbagdata($eventid);
				$current_email = $data[0]->email;
				$current_data = array();
				$startdate = '';

				$appicon = '';
				$appiconres = $this->db->query("SELECT * FROM appearance WHERE controlid = 5 AND appid = $app->id LIMIT 1");
				if($appiconres->num_rows() > 0) {
					$appicon = $appiconres->row()->value;
				}

				$mailheader = false;
				$mailfooter = false;
				$viewtype = '';
				$mailheaderRes = $this->db->query("SELECT * FROM confbagmail WHERE appid = $app->id LIMIT 1");
				if($mailheaderRes->num_rows() > 0) {
					$mailheader = $mailheaderRes->row()->header;
					$mailfooter = $mailheaderRes->row()->footer;
					if(!empty($mailheaderRes->row()->subject)) {
						$subject = $mailheaderRes->row()->subject;
					}
					$viewtype = $mailheaderRes->row()->viewtype;
				}

				$iEmails = 0;
				$data[] = (object) array('email' => 'zzzzzzzzzzzzz@tapcrowd.com');
				foreach($data as $row) {
					if(trim(strtolower($row->email)) != trim(strtolower($current_email)) || $iEmails == count($data) - 1) {
						$datei = 0;
						$ibg = 0;
						//send mail to current_email with current_data and clear both
						$to = $current_email;
						if($mailheader !== false) {
							$message = $mailheader;
						} else {
							$message = '<div id="container" style="font-family:helvetica, arial, sans-serif;color:#000 !important; line-height:20px;font-size:12px;margin:0 auto; width:600px;">
										<div id="logo" style="text-align:center;padding:10px 0px;background-color:#fff;margin-bottom:25px;">
											<img style="max-height:100px;" src="'.($appicon == '' ? 'http://tapcrowd.com/sites/all/themes/tapcrowd/logo.png' : 'http://upload.tapcrowd.com/'.$appicon) .'" alt="">
										</div>
										<div id="content" style="background-color:#ddd;padding:20px;">
											'. __("Dear Sir/ Madam,"). '<br/>
											' . __("Thank you for visiting <strong>%s</strong>. We hope you enjoyed using our app. Below you will find an overview of all items you subscribed for. If there is content available you can find the download link next to it. <br/>", $event->name);
						}

						$current_object = '';
						foreach($current_data as $current_row) {
							//group objects
							if($current_object != $current_row->object) {
								if($current_row->object == 'attendee' || $current_row->object == 'session' || $current_row->object == 'exhibitor') {
									if($current_row->object == 'attendee') {
										$launcher = $this->db->query("SELECT title FROM launcher WHERE eventid = $eventid AND moduletypeid = 14 LIMIT 1");
										if($launcher->num_rows() > 0) {
											$current_row->title = $launcher->row()->title;
										}
									} elseif($current_row->object == 'session') {
										$launcher = $this->db->query("SELECT title FROM launcher WHERE eventid = $eventid AND moduletypeid = 10 LIMIT 1");
										if($launcher->num_rows() > 0) {
											$current_row->title = $launcher->row()->title;
										}
									} elseif($current_row->object == 'exhibitor') {
										$launcher = $this->db->query("SELECT title FROM launcher WHERE eventid = $eventid AND moduletypeid = 2 LIMIT 1");
										if($launcher->num_rows() > 0) {
											$current_row->title = $launcher->row()->title;
										}
									} elseif($current_row->object == 'speaker') {
										$current_row->title = "Speakers";
									}
									$datei = 0;
									if(empty($viewtype)) {
										if(isset($current_row->title)) {
											$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->title)."</h2>";
										} else {
											$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."s</h2>";
										}
										$message .= '<div style="margin-left:8px;">';	
									} elseif($viewtype == 'tables') {
										if(isset($current_row->title)) {
											$message .= ($ibg > 0 ?'</table>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->title)."</h2>";
										} else {
											$message .= ($ibg > 0 ?'</table>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."s</h2>";
										}
										$message .= '<table style="margin-left:8px;">';	
									}
								} else {
									$datei = 0;
									if(empty($viewtype)) {
										$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."</h2>";
										$message .= '<div style="margin-left:8px;">';
									} elseif($viewtype == 'tables') {
										$message .= ($ibg > 0 ?'</table>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."</h2>";
										$message .= '<table style="margin-left:8px;">';
									}
								}

								if($viewtype == 'tables' && $current_row->object == 'attendee') {
									$message .= '<tr><th>Name</th><th>Firstname</th><th>Company</th><th>Function</th><th>Email</th><th>Linkedin</th><th>Phone</th><th>Country</th><th>Image</th><th>Documents</th></tr>';
								} elseif($viewtype == 'tables' && $current_row->object == 'exhibitor') {
									$message .= '<tr><th>Name</th><th>Booth</th><th>Image</th><th>Description</th><th>Phone</th><th>Website</th><th>Email</th><th>Address</th><th>Documents</th></tr>';
								} elseif($viewtype == 'tables' && $current_row->object == 'session') {
									$message .= '<tr><th>Name</th><th>Description</th><th>Location</th><th>Start time</th><th>End time</th><th>Image</th><th>Documents</th></tr>';
								} elseif($viewtype == 'tables' && $current_row->object == 'speaker') {
									$message .= '<tr><th>Name</th><th>Company</th><th>Function</th><th>Description</th><th>Image</th><th>Documents</th></tr>';
								}
								
								$current_object = $current_row->object;
							}

							//session times
							if($current_row->starttime != '' && $startdate != substr($current_row->starttime,0,10)) {
								$startdate = substr($current_row->starttime,0,10);
								$message .= '<h3 style="margin-bottom:0px;padding-top:10px;color:#26A8E0;margin-left:-8px;'.($datei == 0 ? 'margin-top:0px;padding-top:0px;' : 'padding-top:0px;') .'">'.$startdate."</h3>";
							} else {
								$startdate != substr($current_row->starttime,0,10);
							}
							$datei++;
							if(empty($viewtype)) {
								// default view
								if($current_row->object == 'attendee') {
									$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. $current_row->firstname . ($current_row->company != '' ? ' (' . $current_row->company . ($current_row->function != '' ? ' - ' . $current_row->function : '') . ')' : '') . '<br/>';
									if($current_row->emailatt != '') {
										$message .= '<a href="mailto:'.$current_row->emailatt.'" style="color:#26A8E0;">'.$current_row->emailatt.'</a>';
									} 
									if($current_row->linkedin != '') {
										$message .= ' - <a href="'.$current_row->linkedin.'" style="color:#26A8E0;">LinkedIn</a>';
									}
								} elseif($current_row->object == 'speaker') {
									$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. ($current_row->company != '' ? ' (' . $current_row->company . ($current_row->function != '' ? ' - ' . $current_row->function : '') . ')' : '');
								} elseif($current_row->object == 'session') {
									$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' <a href="http://'.$app->subdomain.'.m.tap.cr/sessions/view/'.$current_row->id.'" target="_blank">(View)</a>' ;
								} elseif($current_row->object != 'other') {
									$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. $current_row->firstname;
									if(isset($current_row->website) && !empty($current_row->website)) {
										$message .= ' - <a href="'.$current_row->website.'">Website</a>';
									}
								}

								//document links
								$documentlinks = explode(',', $current_row->documentlink);
								if($current_row->documentlink != '' && $current_object == 'attendee') {
									$message .= '<br/>';
								}
								$i = 1;
								foreach($documentlinks as $documentlink) {
									if(substr($documentlink, 0, 7) != 'http://' && substr($documentlink, 0, 8) != 'https://' && $documentlink != '') {
										$documentlink = utf8_encode('http://upload.tapcrowd.com/'.$documentlink);
									}
									if($documentlink != '') {
										if($current_object == 'attendee') {
											$message .=	'<a href="'.$documentlink.'" style="color:#26A8E0;">Document'.$i.'</a> ';
										} elseif($current_object == 'other') {
											$message .= '* <a href="'.$documentlink.'" style="color:#26A8E0;">'.$documentlink.'</a><br/>';
										} else {
											$message .= ' - ' . '<a href="'.$documentlink.'" style="color:#26A8E0;">Document'.$i.'</a> ';
										}
									} 
									$i++;
								}

								$message .= '<br clear="both"/>';
							} elseif($viewtype == 'tables') {
								if(substr($current_row->imageurl, 0, 7) == 'upload/') {
									$current_row->imageurl = 'http://upload.tapcrowd.com/'.$current_row->imageurl;
								}
								if($current_row->object == 'attendee') {
									$message .= '<tr><td>'.$current_row->name.'</td><td>'.$current_row->firstname.'</td><td>'.$current_row->company.'</td><td>'.$current_row->function.'</td><td>'.$current_row->emailatt.'</td><td>'.$current_row->linkedin.'</td><td>'.$current_row->phone.'</td><td>'.$current_row->country.'</td><td>'.$current_row->imageurl.'</td>';
								} elseif($current_row->object == 'exhibitor') {
									$message .= '<tr><td>'.$current_row->name.'</td><td>'.$current_row->booth.'</td><td>'.$current_row->imageurl.'</td><td>'.$current_row->description.'</td><td>'.$current_row->phone.'</td><td>'.$current_row->website.'</td><td>'.$current_row->emailatt.'</td><td>'.$current_row->address.'</td>';
								} elseif($current_row->object == 'session') {
									$message .= '<tr><td>'.$current_row->name.'</td><td>'.$current_row->description.'</td><td>'.$current_row->location.'</td><td>'.$current_row->starttime.'</td><td>'.$current_row->endtime.'</td><td>'.$current_row->imageurl.'</td>';
								} elseif($current_row->object == 'speaker') {
									$message .= '<tr><td>'.$current_row->name.'</td><td>'.$current_row->company.'</td><td>'.$current_row->function.'</td><td>'.$current_row->description.'</td><td>'.$current_row->imageurl.'</td>';
								}

								$message .= '<td>';
								$documentlinks = explode(',', $current_row->documentlink);
								$i = 1;
								foreach($documentlinks as $documentlink) {
									if(substr($documentlink, 0, 7) != 'http://' && substr($documentlink, 0, 8) != 'https://' && $documentlink != '') {
										$documentlink = utf8_encode('http://upload.tapcrowd.com/'.$documentlink);
									}
									if($documentlink != '') {
										$message .= '<a href="'.$documentlink.'" style="color:#26A8E0;">'.$documentlink.'</a> , ';
									} 
									$i++;
								}

								$message .= '</td></tr>';
							}
							$ibg++;						
						}
						
						if($mailfooter !== false) {
							$message .= $mailfooter;
						} else {
							$message .= '<br/>'.__("We hope to welcome you again at our next edition.<br/>
										Kind regards,<br/>").
										'<a href="mailto:'.$ownmail.'" style="color:#26A8E0">'.$app->name . ' ' . __('Team') . '</a></div></div>';
						}

						$message = rtrim(chunk_split(base64_encode($message)));
						$mail_sent = mail( $to, $subject, $message, $headers );
						$current_data = array();
						$current_email = $row->email;
					}
					$current_data[] = $row;
					$iEmails++;
				}

				$this->session->set_flashdata('event_feedback', __('The mails have been sent'));
				redirect('confbag/event/'.$eventid);
			}

			$cdata['content'] 		= $this->load->view('c_confbag_event_mail', array('event' => $event, 'title' => $title), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, $title => $this->uri->uri_string());
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'confbag');
			$this->load->view('master', $cdata);
		}
	}

	function maillayout($typeid, $type) {
		if($type == 'venue') {
			$venue = $this->venue_model->getById($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			$confbagmail = $this->confbag_model->getMailData($app->id);

			if($this->input->post('postback') == "postback") {
				$this->load->library('form_validation');
				foreach($languages as $language) {
					$this->form_validation->set_rules('subject_'.$language->key, 'subject ('.$language->name.')', 'trim|required');
					$this->form_validation->set_rules('header_'.$language->key, 'header', 'trim');
					$this->form_validation->set_rules('footer_'.$language->key, 'footer', 'trim');
				}
				$data = array(
					'appid' => $app->id,
					'subject' => $this->input->post('subject_'.$app->defaultlanguage),
					'header' => $this->input->post('header_'.$app->defaultlanguage),
					'footer' => $this->input->post('footer_'.$app->defaultlanguage),
					);
				$id = $this->general_model->insert_unique('confbagmail', $data, 'appid', $app->id);

				foreach($languages as $language) {
					$subjectSuccess = $this->language_model->updateTranslation('confbagmail', $id, 'subject', $language->key, $this->input->post('subject_'.$language->key));
					if(!$subjectSuccess) {
						$this->language_model->addTranslation('confbagmail', $id, 'subject', $language->key, $this->input->post('subject_'.$language->key));
					}
					$headerSuccess = $this->language_model->updateTranslation('confbagmail', $id, 'header', $language->key, $this->input->post('header_'.$language->key));
					if(!$headerSuccess) {
						$this->language_model->addTranslation('confbagmail', $id, 'header', $language->key, $this->input->post('header_'.$language->key));
					}
					$footerSucces = $this->language_model->updateTranslation('confbagmail', $id, 'footer', $language->key, $this->input->post('footer_'.$language->key));
					if(!$footerSucces) {
						$this->language_model->addTranslation('confbagmail', $id, 'footer', $language->key, $this->input->post('footer_'.$language->key));
					}
				}

				redirect('venue/view/'.$venue->id);
			}

			$cdata['content'] 		= $this->load->view('c_confbag_maillayout', array('venue' => $venue, 'languages' => $languages, 'data' => $confbagmail), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $title => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'confbag');
			$this->load->view('master', $cdata);
		} else {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			$confbagmail = $this->confbag_model->getMailData($app->id);

			if($this->input->post('postback') == "postback") {
				$this->load->library('form_validation');
				foreach($languages as $language) {
					$this->form_validation->set_rules('subject_'.$language->key, 'subject ('.$language->name.')', 'trim|required');
					$this->form_validation->set_rules('header_'.$language->key, 'header', 'trim');
					$this->form_validation->set_rules('footer_'.$language->key, 'footer', 'trim');
				}
				$this->form_validation->set_rules('viewtype', 'viewtype', 'trim');
				$data = array(
					'appid' => $app->id,
					'subject' => $this->input->post('subject_'.$app->defaultlanguage),
					'header' => $this->input->post('header_'.$app->defaultlanguage),
					'footer' => $this->input->post('footer_'.$app->defaultlanguage),
					'viewtype' => $this->input->post('viewtype')
					);
				$id = $this->general_model->insert_unique('confbagmail', $data, 'appid', $app->id);

				foreach($languages as $language) {
					$subjectSuccess = $this->language_model->updateTranslation('confbagmail', $id, 'subject', $language->key, $this->input->post('subject_'.$language->key));
					if(!$subjectSuccess) {
						$this->language_model->addTranslation('confbagmail', $id, 'subject', $language->key, $this->input->post('subject_'.$language->key));
					}
					$headerSuccess = $this->language_model->updateTranslation('confbagmail', $id, 'header', $language->key, $this->input->post('header_'.$language->key));
					if(!$headerSuccess) {
						$this->language_model->addTranslation('confbagmail', $id, 'header', $language->key, $this->input->post('header_'.$language->key));
					}
					$footerSucces = $this->language_model->updateTranslation('confbagmail', $id, 'footer', $language->key, $this->input->post('footer_'.$language->key));
					if(!$footerSucces) {
						$this->language_model->addTranslation('confbagmail', $id, 'footer', $language->key, $this->input->post('footer_'.$language->key));
					}
				}

				redirect('event/view/'.$event->id);
			}

			$cdata['content'] 		= $this->load->view('c_confbag_maillayout', array('venue' => $venue, 'languages' => $languages, 'data' => $confbagmail), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, 'Conference bag mail' => $this->uri->uri_string());
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'confbag');
			$this->load->view('master', $cdata);
		}
	}

	function testmail($eventid, $type = 'event') {
		if($type == 'venue') {
			if($eventid == FALSE || $eventid == 0) redirect('events');

			$venue = $this->venue_model->getById($eventid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			$launcher = $this->module_mdl->getLauncher(42, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				//mail settings
				$ownmail = $this->input->post('email');
				$to = $this->input->post('emailto');
				$subject = $venue->name . ' - Your personal '. $launcher->title;
				ini_set('sendmail_from', $ownmail);
				$headers = "From: ".$ownmail;
				// $headers .= "\r\nBcc: jens.verstuyft@tapcrowd.com";
				$headers .=  "\r\n".'Content-type: text/html; charset=utf-8' . "\r\n"; 

				$data = $this->confbag_model->getConfbagdata($venue->id, 'venue');
				$current_email = $data[0]->email;
				$current_data = array();
				$startdate = '';

				$appicon = '';
				$appiconres = $this->db->query("SELECT * FROM appearance WHERE controlid = 5 AND appid = $app->id LIMIT 1");
				if($appiconres->num_rows() > 0) {
					$appicon = $appiconres->row()->value;
				}

				$mailheader = false;
				$mailfooter = false;
				$mailheaderRes = $this->db->query("SELECT * FROM confbagmail WHERE appid = $app->id LIMIT 1");
				if($mailheaderRes->num_rows() > 0) {
					$mailheader = $mailheaderRes->row()->header;
					$mailfooter = $mailheaderRes->row()->footer;
					if(!empty($mailheaderRes->row()->subject)) {
						$subject = $mailheaderRes->row()->subject;
					}
				}

				$iEmails = 0;
				$ibg = 0;
				$data[] = (object) array('email' => 'zzzzzzzzzzzzz@tapcrowd.com');
				foreach($data as $row) {
					if(trim(strtolower($row->email)) != trim(strtolower($current_email)) || $iEmails == count($data) - 1) {
						$datei = 0;
						$ibg = 0;
						//send mail to current_email with current_data and clear both
						// $to = $current_email;
						if($mailheader !== false) {
							$message = $mailheader;
						} else {
							$message = '<div id="container" style="font-family:helvetica, arial, sans-serif;color:#000 !important; line-height:20px;font-size:12px;margin:0 auto; width:600px;">
										<div id="logo" style="text-align:center;padding:10px 0px;background-color:#fff;margin-bottom:25px;">
											<img style="max-height:100px;" src="'.($appicon == '' ? 'http://tapcrowd.com/sites/all/themes/tapcrowd/logo.png' : 'http://upload.tapcrowd.com/'.$appicon) .'" alt="">
										</div>
										<div id="content" style="background-color:#ddd;padding:20px;">
											'. __("Dear Sir/ Madam,"). '<br/>
											' . __("Thank you for visiting <strong>%s</strong>. We hope you enjoyed using our app. Below you will find an overview of all items you subscribed for. If there is content available you can find the download link next to it. <br/>", $venue->name);
						}

						$current_object = '';
						foreach($current_data as $current_row) {
							//group objects
							if($current_object != $current_row->object) {
								if($current_row->object == 'catalog') {
									$launcher = $this->db->query("SELECT title FROM launcher WHERE venueid = $venueid AND moduletypeid = 15 LIMIT 1");
									if($launcher->num_rows() > 0) {
										$current_row->title = $launcher->row()->title;
									}
									$datei = 0;
									if(isset($current_row->title)) {
										$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->title)."</h2>";
									} else {
										$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."s</h2>";
									}
									$message .= '<div style="margin-left:8px;">';
								} else {
									$datei = 0;
									$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."</h2>";
									$message .= '<div style="margin-left:8px;">';
								}
								
								$current_object = $current_row->object;
							}

							$message .= '<span style="margin-left:-8px;">* </span>' . $current_row->name;


							//document links
							$documentlinks = explode(',', $current_row->documentlink);
							$i = 1;
							foreach($documentlinks as $documentlink) {
								if(substr($documentlink, 0, 7) != 'http://' && substr($documentlink, 0, 8) != 'https://' && $documentlink != '') {
									$documentlink = utf8_encode('http://upload.tapcrowd.com/'.$documentlink);
								}
								if($documentlink != '') {
									$message .= ' - ' . '<a href="'.$documentlink.'" style="color:#26A8E0;">Document'.$i.'</a> ';
								} 
								$i++;
							}

							if($current_row->object == 'catalog' && !empty($current_row->pdf)) {
								$message .= ' - <a href="http://upload.tapcrowd.com/'. $current_row->pdf.'">PDF</a> ';
							}
							if($current_row->object == 'catalog' && !empty($current_row->url)) {
								$message .= ' - <a href="'.$current_row->url.'">'.$current_row->urltitle.'</a> ';
							}

							$message .= '<br clear="both"/>';
							$ibg++;						
						}
						
						if($mailfooter !== false) {
							$message .= $mailfooter;
						} else {
							$message .= '<br/>'.__("We hope to welcome you again.<br/>
										Kind regards,<br/>").
										'<a href="mailto:'.$ownmail.'" style="color:#26A8E0">'.$app->name . ' ' . __('Team') . '</a></div></div>';
						}
						$mail_sent = mail( $to, $subject, $message, $headers );
						break;
						$current_data = array();
						$current_email = $row->email;
					}
					$current_data[] = $row;
					$iEmails++;
				}

				$this->session->set_flashdata('event_feedback', __('The mails have been sent'));
				redirect('confbag/venue/'.$venue->id);
			}

			$cdata['content'] 		= $this->load->view('c_confbag_venue_testmail', array('venue' => $venue, 'title' => $title), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $title => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'confbag');
			$this->load->view('master', $cdata);
		} else {
			if($eventid == FALSE || $eventid == 0) redirect('events');

			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			$launcher = $this->module_mdl->getLauncher(42, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$title = $launcher->title;

			if($this->input->post('postback') == "postback") {
				//mail settings
				$ownmail = $this->input->post('email');
				$to = $this->input->post('emailto');
				// if($app->id == 2187) {
				// 	//euroclear specific mail
				// 	redirect('dummy/euroclearconfbag/'.$app->id.'/'.$event->id.'/'.$launcher->id.'?email='.$ownmail);
				// }
				$subject = $event->name . ' - Your personal '. $launcher->title;
				ini_set('sendmail_from', $ownmail);
				$headers = "From: ".$ownmail;
				// $headers .= "\r\nBcc: jens.verstuyft@tapcrowd.com";
				$headers .=  "\r\n"."Content-type: text/html; charset=utf-8\r\nContent-Transfer-Encoding: base64\r\n";

				$data = $this->confbag_model->getConfbagTestData($ownmail, $eventid, 'event');
				$current_email = $data[0]->email;
				$current_data = array();
				$startdate = '';

				$appicon = '';
				$appiconres = $this->db->query("SELECT * FROM appearance WHERE controlid = 5 AND appid = $app->id LIMIT 1");
				if($appiconres->num_rows() > 0) {
					$appicon = $appiconres->row()->value;
				}

				$mailheader = false;
				$mailfooter = false;
				$viewtype = '';
				$mailheaderRes = $this->db->query("SELECT * FROM confbagmail WHERE appid = $app->id LIMIT 1");
				if($mailheaderRes->num_rows() > 0) {
					$mailheader = $mailheaderRes->row()->header;
					$mailfooter = $mailheaderRes->row()->footer;
					if(!empty($mailheaderRes->row()->subject)) {
						$subject = $mailheaderRes->row()->subject;
					}
					$viewtype = $mailheaderRes->row()->viewtype;
				}

				$iEmails = 0;
				$data[] = (object) array('email' => 'zzzzzzzzzzzzz@tapcrowd.com');
				foreach($data as $row) {
					if(trim(strtolower($row->email)) != trim(strtolower($current_email)) || $iEmails == count($data) - 1) {
						$datei = 0;
						$ibg = 0;
						//send mail to current_email with current_data and clear both
						// $to = $current_email;
						if($mailheader !== false) {
							$message = $mailheader;
						} else {
							$message = '<div id="container" style="font-family:helvetica, arial, sans-serif;color:#000 !important; line-height:20px;font-size:12px;margin:0 auto; width:600px;">
										<div id="logo" style="text-align:center;padding:10px 0px;background-color:#fff;margin-bottom:25px;">
											<img style="max-height:100px;" src="'.($appicon == '' ? 'http://tapcrowd.com/sites/all/themes/tapcrowd/logo.png' : 'http://upload.tapcrowd.com/'.$appicon) .'" alt="">
										</div>
										<div id="content" style="background-color:#ddd;padding:20px;">
											'. __("Dear Sir/ Madam,"). '<br/>
											' . __("Thank you for visiting <strong>%s</strong>. We hope you enjoyed using our app. Below you will find an overview of all items you subscribed for. If there is content available you can find the download link next to it. <br/>", $event->name);
						}

						$current_object = '';
						foreach($current_data as $current_row) {
							//group objects
							if($current_object != $current_row->object) {
								if($current_row->object == 'attendee' || $current_row->object == 'session' || $current_row->object == 'exhibitor') {
									if($current_row->object == 'attendee') {
										$launcher = $this->db->query("SELECT title FROM launcher WHERE eventid = $eventid AND moduletypeid = 14 LIMIT 1");
										if($launcher->num_rows() > 0) {
											$current_row->title = $launcher->row()->title;
										}
									} elseif($current_row->object == 'session') {
										$launcher = $this->db->query("SELECT title FROM launcher WHERE eventid = $eventid AND moduletypeid = 10 LIMIT 1");
										if($launcher->num_rows() > 0) {
											$current_row->title = $launcher->row()->title;
										}
									} elseif($current_row->object == 'exhibitor') {
										$launcher = $this->db->query("SELECT title FROM launcher WHERE eventid = $eventid AND moduletypeid = 2 LIMIT 1");
										if($launcher->num_rows() > 0) {
											$current_row->title = $launcher->row()->title;
										}
									} elseif($current_row->object == 'speaker') {
										$current_row->title = "Speakers";
									}
									$datei = 0;
									if(empty($viewtype)) {
										if(isset($current_row->title)) {
											$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->title)."</h2>";
										} else {
											$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."s</h2>";
										}
										$message .= '<div style="margin-left:8px;">';	
									} elseif($viewtype == 'tables') {
										if(isset($current_row->title)) {
											$message .= ($ibg > 0 ?'</table>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->title)."</h2>";
										} else {
											$message .= ($ibg > 0 ?'</table>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."s</h2>";
										}
										$message .= '<table style="margin-left:8px;">';	
									}	
								} else {
									$datei = 0;
									if(empty($viewtype)) {
										$message .= ($ibg > 0 ?'</div>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."</h2>";
										$message .= '<div style="margin-left:8px;">';
									} elseif($viewtype == 'tables') {
										$message .= ($ibg > 0 ?'</table>' : '') . '<h2 style="margin-bottom:5px;padding-top:10px;color:#26A8E0;">'.ucfirst($current_row->object)."</h2>";
										$message .= '<table style="margin-left:8px;">';
									}
								}

								if($viewtype == 'tables' && $current_row->object == 'attendee') {
									$message .= '<tr><th>Name</th><th>Firstname</th><th>Company</th><th>Function</th><th>Email</th><th>Linkedin</th><th>Phone</th><th>Country</th><th>Image</th><th>Documents</th></tr>';
								} elseif($viewtype == 'tables' && $current_row->object == 'exhibitor') {
									$message .= '<tr><th>Name</th><th>Booth</th><th>Image</th><th>Description</th><th>Phone</th><th>Website</th><th>Email</th><th>Address</th><th>Documents</th></tr>';
								} elseif($viewtype == 'tables' && $current_row->object == 'session') {
									$message .= '<tr><th>Name</th><th>Description</th><th>Location</th><th>Start time</th><th>End time</th><th>Image</th><th>Documents</th></tr>';
								} elseif($viewtype == 'tables' && $current_row->object == 'speaker') {
									$message .= '<tr><th>Name</th><th>Company</th><th>Function</th><th>Description</th><th>Image</th><th>Documents</th></tr>';
								}
								
								$current_object = $current_row->object;
							}

							if(empty($viewtype)) {
								// default view
								if($current_row->object == 'attendee') {
									$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. $current_row->firstname . ($current_row->company != '' ? ' (' . $current_row->company . ($current_row->function != '' ? ' - ' . $current_row->function : '') . ')' : '') . '<br/>';
									if($current_row->emailatt != '') {
										$message .= '<a href="mailto:'.$current_row->emailatt.'" style="color:#26A8E0;">'.$current_row->emailatt.'</a>';
									} 
									if($current_row->linkedin != '') {
										$message .= ' - <a href="'.$current_row->linkedin.'" style="color:#26A8E0;">LinkedIn</a>';
									}
								} elseif($current_row->object == 'speaker') {
									$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. ($current_row->company != '' ? ' (' . $current_row->company . ($current_row->function != '' ? ' - ' . $current_row->function : '') . ')' : '');
								} elseif($current_row->object == 'session') {
									$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' <a href="http://'.$app->subdomain.'.m.tap.cr/sessions/view/'.$current_row->id.'" target="_blank">(View)</a>' ;
								} elseif($current_row->object != 'other') {
									$message .= '<span style="margin-left:-8px">* </span>' . $current_row->name . ' '. $current_row->firstname;
									if(isset($current_row->website) && !empty($current_row->website)) {
										$message .= ' - <a href="'.$current_row->website.'">Website</a>';
									}
								}

								//document links
								$documentlinks = explode(',', $current_row->documentlink);
								if($current_row->documentlink != '' && $current_object == 'attendee') {
									$message .= '<br/>';
								}
								$i = 1;
								foreach($documentlinks as $documentlink) {
									if(substr($documentlink, 0, 7) != 'http://' && substr($documentlink, 0, 8) != 'https://' && $documentlink != '') {
										$documentlink = utf8_encode('http://upload.tapcrowd.com/'.$documentlink);
									}
									if($documentlink != '') {
										if($current_object == 'attendee') {
											$message .=	'<a href="'.$documentlink.'" style="color:#26A8E0;">Document'.$i.'</a> ';
										} elseif($current_object == 'other') {
											$message .= '* <a href="'.$documentlink.'" style="color:#26A8E0;">'.$documentlink.'</a><br/>';
										} else {
											$message .= ' - ' . '<a href="'.$documentlink.'" style="color:#26A8E0;">Document'.$i.'</a> ';
										}
									} 
									$i++;
								}

								$message .= '<br clear="both"/>';
							} elseif($viewtype == 'tables') {
								if(substr($current_row->imageurl, 0, 7) == 'upload/') {
									$current_row->imageurl = 'http://upload.tapcrowd.com/'.$current_row->imageurl;
								}
								if($current_row->object == 'attendee') {
									$message .= '<tr><td>'.$current_row->name.'</td><td>'.$current_row->firstname.'</td><td>'.$current_row->company.'</td><td>'.$current_row->function.'</td><td>'.$current_row->emailatt.'</td><td>'.$current_row->linkedin.'</td><td>'.$current_row->phone.'</td><td>'.$current_row->country.'</td><td>'.$current_row->imageurl.'</td>';
								} elseif($current_row->object == 'exhibitor') {
									$message .= '<tr><td>'.$current_row->name.'</td><td>'.$current_row->booth.'</td><td>'.$current_row->imageurl.'</td><td>'.$current_row->description.'</td><td>'.$current_row->phone.'</td><td>'.$current_row->website.'</td><td>'.$current_row->emailatt.'</td><td>'.$current_row->address.'</td>';
								} elseif($current_row->object == 'session') {
									$message .= '<tr><td>'.$current_row->name.'</td><td>'.$current_row->description.'</td><td>'.$current_row->location.'</td><td>'.$current_row->starttime.'</td><td>'.$current_row->endtime.'</td><td>'.$current_row->imageurl.'</td>';
								} elseif($current_row->object == 'speaker') {
									$message .= '<tr><td>'.$current_row->name.'</td><td>'.$current_row->company.'</td><td>'.$current_row->function.'</td><td>'.$current_row->description.'</td><td>'.$current_row->imageurl.'</td>';
								}

								$message .= '<td>';
								$documentlinks = explode(',', $current_row->documentlink);
								$i = 1;
								foreach($documentlinks as $documentlink) {
									if(substr($documentlink, 0, 7) != 'http://' && substr($documentlink, 0, 8) != 'https://' && $documentlink != '') {
										$documentlink = utf8_encode('http://upload.tapcrowd.com/'.$documentlink);
									}
									if($documentlink != '') {
										$message .= '<a href="'.$documentlink.'" style="color:#26A8E0;">'.$documentlink.'</a> , ';
									} 
									$i++;
								}

								$message .= '</td></tr>';
							}
							$ibg++;						
						}
						
						if($mailfooter !== false) {
							$message .= $mailfooter;
						} else {
							$message .= '<br/>'.__("We hope to welcome you again at our next edition.<br/>
										Kind regards,<br/>").
										'<a href="mailto:'.$ownmail.'" style="color:#26A8E0">'.$app->name . ' ' . __('Team') . '</a></div></div>';
						}

						$message = rtrim(chunk_split(base64_encode($message)));
						$mail_sent = mail( $to, $subject, $message, $headers );
						break;
						$current_data = array();
						$current_email = $row->email;
					}
					$current_data[] = $row;
					$iEmails++;
				}

				$this->session->set_flashdata('event_feedback', __('The mails have been sent'));
				redirect('confbag/event/'.$eventid);
			}

			$cdata['content'] 		= $this->load->view('c_confbag_event_testmail', array('event' => $event, 'title' => $title), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, $title => $this->uri->uri_string());
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'confbag');
			$this->load->view('master', $cdata);
		}
	}
}