<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Notes extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('news_model');
		$this->load->model('notes_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Note',
			'plural' => 'Notes',
			'title' => __('Notes'),
			'parentType' => 'event',
			'browse_url' => '',
			'add_url' => '',
			'edit_url' => '',
			'module_url' => 'module/editByController/notes/--parentType--/--parentId--/0',
			'import_url' => '',
			'rss_url' => '',
			'headers' => array(),
			'actions' => array(
				'edit' => (object)array(),
				'duplicate' => (object)array(),
				'delete' => (object)array(),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'c_notes_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false
		);
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		// $this->iframeurl = 'notes/event/'.$id;

		$activeModules = $this->module_mdl->getActiveLaunchers($event, 'event', 'notes');

		$notemodules = $this->notes_model->getActiveNoteModules($app, 'event', $event->id);

		if($this->input->post('postback') == 'postback') {
			$this->notes_model->saveNoteModules($app, $this->input->post('notemodules'), 'event', $event->id);
			redirect('event/view/'.$event->id);
		}

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);

		$launcher = $this->module_mdl->getLauncher(35, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = ucfirst($launcher->title);

		$listview = $this->_module_settings->views['edit'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'modules' => $activeModules, 'activemodules' => $notemodules), true);

		// $cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => $news, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'notes');
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		// $this->iframeurl = 'notes/venue/'.$id;

		$activeModules = $this->module_mdl->getActiveLaunchers($venue, 'venue', 'notes');

		$notemodules = $this->notes_model->getActiveNoteModules($app, 'venue', $venue->id);

		if($this->input->post('postback') == 'postback') {
			$this->notes_model->saveNoteModules($app, $this->input->post('notemodules'), 'venue', $venue->id);
			redirect('venue/view/'.$venue->id);
		}

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $module_url);

		$launcher = $this->module_mdl->getLauncher(35, 'venue', $venue->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = ucfirst($launcher->title);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array()), true);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array()), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $this->_module_settings->title => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'notes');
		$this->load->view('master', $cdata);
	}   
}