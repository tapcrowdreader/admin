<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Events extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	function index($tag = '') {
		//if($this->venue_model->isVenueApp(_currentApp()->id)){ redirect('venues'); }
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');
		$this->session->set_userdata('artistid', '');
		$this->myEvents($tag);
	}

	function venue($venueid) {
		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$launcher = $this->db->query("SELECT * FROM launcher WHERE venueid = ? AND moduletypeid = 30", $venueid);
		if($launcher->num_rows() != 0) {
			$launcher = $launcher->row();
			$tag = $launcher->tag;
			$this->myEvents($tag, $launcher->id);
		}
	}

	function launcher($launcherid) {
		$app = _currentApp();
		$launcher = $this->db->query("SELECT * FROM launcher WHERE id = ?", $launcherid);
		if($launcher->num_rows() != 0) {
			$launcher = $launcher->row();
			$tag = $launcher->tag;
			$this->myEvents($tag, $launcherid);
		}
	}

	function chooselauncher($appid) {
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');
		$launchers = $this->module_mdl->getEventsCityLaunchers($app);
		if(count($launchers) == 1) {
			redirect('events/launcher/'.$launchers[0]->id);
		}
		$cdata['content'] 		= $this->load->view('c_events_chooselauncher', array('launchers' => $launchers, 'app' => $app), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array();
		if($app->familyid == 2) {
			$crumbapp = array($app->name => 'apps/view/'.$app->id);
			$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app);
		$this->load->view('master', $cdata);
	}

	function myEvents($tagParam = '', $launcherid = '') {
		$app = _currentApp();
		if(isset($app) && $app != null) {
 			if($app->subdomain != "") {
	            $this->iframeurl = "events/app/".$app->id;
	        }

			$events = $this->event_model->allFromApp($app->id);
			if($app->apptypeid != 4 ) {
				if(isset($events) && $events != false && count($events) != 0) {
					$event = $events[0];
					if(count($events) == 1 && $app->apptypeid != 5 && $app->apptypeid != 8) {
						redirect('event/view/' . $event->id);
					}
				}
			}

			if($app->apptypeid == 5 && $tagparam == '' && $launcherid == '') {
				redirect('events/chooselauncher/'.$app->id);
			}

			if((empty($events) || $events == false) && $app->familyid == 1) {
				if(!empty($launcherid)) {
					redirect('event/add/'.$launcherid);
				}
				redirect('event/add');
			}


			if($app->familyid == 3) {
				if(isset($tagParam) && $tagParam != '') {
					$this->iframeurl = "events/tag/".$app->id."/".$tagParam;
				} else {
					$this->iframeurl = "events/app/".$app->id;
				}
			}

			if($launcherid != '') {
				$this->iframeurl ='events/launcher/'.$app->id.'/'.$launcherid;
			}

			if($app->apptypeid == 1 && $events != false) {
				$this->iframeurl = 'app/expo/'.$app->id.'/events';
			}

			if($tagParam != '') {
				$eventsTags = array();
				if($events!=false && $events != null) {
					foreach($events as $event) {
						// TAGS
						$tagNames = array();
						$tags = $this->db->query("SELECT * FROM tag WHERE eventid = $event->id");
						if($tags->num_rows() == 0) {
							$tags = array();
						} else {
							$tagz = array();
							foreach ($tags->result() as $tag) {
								$tagz[] = $tag;
								$tagNames[] = $tag->tag;
							}
							$tags = $tagz;
						}

						$event->tags = $tags;

						if($tagParam != '') {
							if(in_array($tagParam,$tagNames)) {
								$eventsTags[] = $event;
							}
						}
					}
				}

				if($tagParam != '') {
					$events = $eventsTags;
				}
			}


			$api_events = array();
			switch ($this->channel->id) {
				case '3':
					$api_events = $this->getMyEventsAPI('http://www.mijnevent.be', _currentUser()->external_id);
					break;
				case '4':
					$api_events = $this->getMyEventsAPI('http://www.mijnevenement.nl', _currentUser()->external_id);
					break;
				case '6':
					$api_events = $this->getMyEventsAPI('http://www.myupcoming.com', _currentUser()->external_id);
					break;
				default:
					$api_events = FALSE;
					//$api_events = $this->getMyEventsAPI('http://www.mijnevent.be', '2');
					break;
			}


		//Metadata curstomfields
		$showButtonMetadata = false;
		// $eventslaunchers = $this->module_mdl->getLauncheridsOfObjects('event', $events);
		// $metadata = $this->metadata_model->getMetadataNamesOfLaunchers($eventslaunchers);
		// if(!empty($metadata)) {
		// 	$showButtonMetadata = true;
		// }

		// $count = 0;
		// $prev = '';
		// foreach($metadata as $m) {
		// 	$countCurrent = count($m);
		// 	if($countCurrent != $count && $count != 0 && $countCurrent != 0) {
		// 		$showButtonMetadata = false;
		// 		break;
		// 	}

		// 	$prev = $m;

		// 	$count = $countCurrent;
		// }

		// if($showButtonMetadata) {
		// 	$namesPrev = array();
		// 	$first = array_shift($metadata);
		// 	foreach($first as $f) {
		// 		$namesPrev[$f->name] = $f->name;
		// 	}
		// 	foreach($metadata as $m) {
		// 		foreach($m as $m2) {
		// 			if(!isset($namesPrev[$m2->name])) {
		// 				$showButtonMetadata = false;
		// 				break;
		// 			}
		// 		}
		// 	}
		// }
			
			$cdata['content'] 		= $this->load->view('c_events', array('events' => $events, 'api_events' => $api_events, 'app' => $app, 'launcherid' => $launcherid, 'showButtonMetadata' => $showButtonMetadata), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			if($launcherid == '') {
				if($app->familyid != 1) {
					$cdata['crumb']			= array(__("Events") => "events");
					if($app->familyid == 2) {
						$crumbapp = array($app->name => 'apps/view/'.$app->id);
						$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
					}
				}
			} else {
				$this->load->model('module_mdl');
				$launcher = $this->module_mdl->getLauncherById($launcherid);
				$cdata['crumb']			= array($launcher->title => "events/launcher/".$launcherid);
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app);
			$this->load->view('master', $cdata);
		} else {
			redirect('error/index');
		}
	}


	function getMyEventsAPI($domain = 'http://www.mijnevent.be', $external_id = '') {
		// change settings here
		$email = 'info@tapcrowd.com';
		$password = 'alleskanbeter';
		$apiId = '78678';
		$apiKey = 'JK65DL4OV5JF59';
		$eventId = 'your event id here'; // check the URL for your event to find its id

		// login
		$loginUrl = $domain . '/api/login';
		$ch = curl_init($loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array(
			'email' => $email,
			'pwd' => $password,
			'app-id' => $apiId,
			'fmt' => 'json',
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// get session id and store
		$result = curl_exec($ch);
		curl_close($ch);
		$json = json_decode($result);
		$sessionId = $json->result->sessionId;

		// get event info
		$apiCallUrl = $domain . "/api/organizer-events?session-id=$sessionId&org-id=$external_id&fmt=json";
		$signature = md5($apiCallUrl . $apiKey);
		$apiCallUrl .= '&sign=' . $signature;
		$json = json_decode(file_get_contents($apiCallUrl));
		// var_dump($json);
		return $json->result;

	}

	// IMPORT EVENT FROM THIRD PARTY WEBSITE
	function import($importid = '') {
		if($importid == '') redirect('events');

		// GET EVENT DATA FROM EXTERNAL API
		$api_events = array();
		switch ($this->channel->id) {
			case '3':
				$api_events = $this->getMyEventsAPI('http://www.mijnevent.be', _currentUser()->external_id);
				break;
			case '4':
				$api_events = $this->getMyEventsAPI('http://www.mijnevenement.nl', _currentUser()->external_id);
				break;
			case '6':
				$api_events = $this->getMyEventsAPI('http://www.myupcoming.com', _currentUser()->external_id);
				break;
			default:
				//$api_events = $this->getMyEventsAPI('http://www.mijnevent.be', '2');
				break;
		}

		//print_r($api_events);die();

		foreach ($api_events as $event) {
			if ($event->id == $importid) {

				/*
				{
				(
				    [id] => 597
				    [isDeleted] => 0
				    [isFree] =>
				    [isPrivate] => 0
				    [language] => nl
				    [currency] => EUR
				    [saleOpen] => 0
				    [title] => in het buitenland
				    [descritption] =>
				    [logo] => http://dnwa3gu2jm4p2.cloudfront.net/images/custom//0x0/original/b7212cc125345659d821dbb2a2184cc5
				    [performers] =>
				    [venue] =>
				    [street] => Ajuinlei 15J
				    [sublocation] => 0
				    [location] => Famagusta
				    [zip] =>
				    [state] => Famagusta
				    [country] => Cyprus
				    [organizer] => Rekall
				    [email] => bert@rekalldesign.com
				    [tel] => 0486/12.34.56
				    [website] =>
				    [ticketingWebsite] =>
				    [start] => 2010-01-25 14:33:00
				    [end] => 2010-01-25 14:33:00
				    [dates] => Array
				        (
				            [0] => stdClass Object
				                (
				                    [start] => 2010-01-25 14:33:00
				                    [end] => 2010-01-25 14:33:00
				                )

				        )

				    [numTickets] => 0
				    [numOnlineTickets] => 0
				    [numTicketsSold] => 0
				    [numOnlineTicketsSold] => 0
				    [numTicketsAvailable] => 0
				    [numOnlineTicketsAvailable] => 0
				    [numValidatedTickets] => 0
				    [numTicketsReserved] => 0
				    [totalOrganizerRevenue] => 0
				)
				}
				*/


				//Database model
				$this->load->model('app_model');
				$this->load->model('map_model');
				$this->load->model('venue_model');

				$eventlogo = "";

				$venueaddress = $event->street . ", " . $event->zip . " " . $event->town;
				// GET LATLON-LOCATION FOR VENUE
				$latlon = $this->event_model->getLocation($venueaddress);

				// INSERT VENUE AND RETURN ID
				$venueid = $this->venue_model->add($event->venue, $venueaddress, $latlon);

				// TODO : event verder importeren

				// INSERT EVENT + VENUEID
				$eventtypeid = 3;
				$datefrom = substr($event->start, 0, 10);
				$dateto = substr($event->end, 0, 10);
				//$eventlogo = (isset($returndata) ? "upload/eventlogos/".$returndata['file_name'] : "");
				$active = 1; //Do not standard activate an event in the webservice!
				$qrshare = 0;
				$description = $event->descritption;
				$eventid = $this->event_model->add(''.$event->title, _currentUser()->organizerId, $venueid, $eventtypeid, $datefrom, $dateto, $active, $eventlogo, $qrshare, ''.$description);
				$this->app_model->addEventToApp(_currentApp()->id, $eventid);
				if(_currentUser()->organizerId != 1) {
					$this->app_model->addEventToApp(2, $eventid); //Add to TAPCROWD App!
				}

				$this->session->set_flashdata('event_feedback', __('Your event has successfully been imported.'));
				_updateTimeStamp($eventid);
				//redirect('event/view/'.$eventid);
				redirect('events');
			}
		}
	}

	function sort() {
		$ids = $this->input->post('ids');
		$i = 0;
		foreach($ids as $id) {
			$data = array('order' => $i);
			$this->general_model->update('event', $id, $data);
			$i++;
			_updateTimeStamp($id);
		}
	}

	function removelauncher($launcherid) {
		$launcher = $this->module_mdl->getLauncherById($launcherid);
		$app = $this->app_model->get($launcher->appid);
		_actionAllowed($app, 'app');

		$this->module_mdl->removeLauncher($launcher);

		redirect('apps/view/'.$launcher->appid);
	}
}