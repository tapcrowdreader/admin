<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Poi extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('map_model');
		$this->load->model('poi_model');
	}
	
	//php 4 constructor
	function Poi() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('map_model');
		$this->load->model('poi_model');
	}
	
	function index() {
		$this->event();
	}
	
	function event($eventid, $mapid = 0) {
		if($eventid == FALSE || $eventid == 0) redirect('events');
		
		$event = $this->event_model->getbyid($eventid);
		$app = _actionAllowed($event, 'event','returnApp');
        
        $this->iframeurl = "map/by-event/" . $eventid;
		
		$maps = $this->map_model->getMapsByEvent($event->id);
		if($maps != FALSE && $mapid == 0) { $mapid = $maps[0]->id; }
		$map = $this->map_model->getMapsById($mapid);
		$markers = $this->poi_model->getPoisByMapId($mapid);
		
		// Exhibitors meegeven voor koppeling aan locaties op map (indien juiste flavor)
		$poitypes = $this->poi_model->getPoiTypes();
		
		$cdata['content'] 		= $this->load->view('c_poi', array('event' => $event, 'maps' => $maps, 'map' => $map, 'poitypes' => $poitypes, 'markers' => $markers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("event" => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __('POI') => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']	= array($event->name => "event/view/".$event->id, __('POI') => $this->uri->uri_string());
		}
		$this->load->view('master', $cdata);
	}
	
	function venue($venueid, $mapid = 0) {
		if($venueid == FALSE || $venueid == 0) redirect('venues');
		
		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');
        
        $this->iframeurl = "map/by-venue/" . $venueid;
		
		$maps = $this->map_model->getMapsByVenue($venue->id);
		if($maps != FALSE && $mapid == 0) { $mapid = $maps[0]->id; }
		$map = $this->map_model->getMapsById($mapid);
		$markers = $this->poi_model->getPoisByMapIdForVenue($mapid);
		
		// Exhibitors meegeven voor koppeling aan locaties op map (indien juiste flavor)
		$poitypes = $this->poi_model->getPoiTypes();
		
		$cdata['content'] 		= $this->load->view('c_poi_venue', array('venue' => $venue, 'maps' => $maps, 'map' => $map, 'poitypes' => $poitypes, 'markers' => $markers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("venue" => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __('POI') => $this->uri->uri_string()));
		$this->load->view('master', $cdata);
	}

	function addPoi() {
		if(stristr($_SERVER['HTTP_REFERER'], base_url()) != FALSE) {
			if($this->input->post('postback') == 'postback'){
				
				$data = array(
					'eventid'	=> $this->input->post('eventid'),
					'venueid'	=> $this->input->post('venueid'),
					'name'		=> $this->input->post('name'),
					'poitypeid' => $this->input->post('sel_koppel'),
					'x1' 		=> $this->input->post('posx'), 
					'y1' 		=> $this->input->post('posy'), 
					'x2' 		=> 0, 
					'y2' 		=> 0
				);
				
				if($this->general_model->insert('poi', $data)) {
					_updateTimeStamp($this->input->post('eventid'));
					echo TRUE;
				} else {
					echo FALSE;
				}
			} else {
				echo FALSE;
			}
		} else {
			echo "False, intruder!";
		}
	}
	
	function removepoi($eventid, $mapid, $id) {
		$this->general_model->remove('poi', $id);
		redirect('poi/event/'.$eventid.'/'.$mapid.'/');
	}
	
	function removepoiVenue($venueid, $mapid, $id) {
		$this->general_model->remove('poi', $id);
		redirect('poi/venue/'.$venueid.'/'.$mapid.'/');
	}

}