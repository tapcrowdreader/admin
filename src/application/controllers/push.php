<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Push extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('push_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Push Notification',
			'plural' => 'Push Notifications',
			'parentType' => 'event',
			'browse_url' => 'push/--parentType--/--parentId--',
			'add_url' => 'push/add/--parentId--/--parentType--',
			'headers' => array(
				__('Message') => 'message', 
				__('Sent') => 'sent'
			),
			'actions' => array(
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			)
		);
	}
	
	function index() {
		redirect('events');
	}
	
	function app($id) {
		if($id == FALSE || $id == 0) redirect('events');
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');
		
		$error = '';		
		$push = $this->push_model->getSentByAppId($app->id);
		
		// if($app->certificate == null || $app->certificate == '') {
		// 	$cdata['content'] = $this->load->view('c_push', array('error' => $error, 'app' => $app), TRUE);
		// } else {
			$this->_module_settings->parentType = 'app';
			# Module add url
			$add_url =& $this->_module_settings->add_url;
			$add_url = str_replace(array('--parentType--','--parentId--'), array('app', $id), $add_url);

			$listview = $this->_module_settings->views['list'];
			$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $push), true);
		// }
		
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__("Push notifications") => "push/app/".$id);
		$this->load->view('master', $cdata);
	}
	
	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');
		
		$event = $this->event_model->getbyid($id);
		if($event == FALSE) redirect('events');
		$app = _actionAllowed($event, 'event','returnApp');
		
		$error = '';		
		$push = $this->push_model->getSentByAppId($app->id);
		
		// if($app->certificate == null || $app->certificate == '') {
		// 	$cdata['content'] = $this->load->view('c_push', array('error' => $error, 'app' => _currentApp()), TRUE);
		// } else {
			# Module add url
			$add_url =& $this->_module_settings->add_url;
			$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);

			$listview = $this->_module_settings->views['list'];
			$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $push), true);
		// }
		
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array( $event->name => "event/view/".$id,__("Push notifications") => "push/event/".$id);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'push');
		$this->load->view('master', $cdata);
	}
	
	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		
		$error = '';
		
		$push = $this->push_model->getSentByAppId($app->id);
		
		// if($app->certificate == null || $app->certificate == '') {
		// 	$cdata['content'] = $this->load->view('c_push', array('error' => $error, 'app' => _currentApp()), TRUE);
		// } else {
			$this->_module_settings->parentType = 'venue';
			# Module add url
			$add_url =& $this->_module_settings->add_url;
			$add_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $add_url);

			$listview = $this->_module_settings->views['list'];
			$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $push), true);
		// }
		
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= array( $venue->name => "venue/view/".$id,__("Push notifications") => "push/venue/".$id);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'push');
		$this->load->view('master', $cdata);
	}
	
	function add($id, $type = '') {
		if($type == 'event') {
			$error = '';
			
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');

			if ($this->input->post('postback') == 'postback') {
				$this->load->library('form_validation');
				$this->form_validation->set_rules('pushtext', 'pushtext', 'trim|required|max_length[160]|min_length[10]');

				if($this->form_validation->run() == FALSE){
					$error = __("Oops, your notification might be too long or too short.<br />Maximum 160 characters and minimum 10 characters!");
				} else {
					$pushtype = 'push';
					$data = array(
							'appid'			=> $app->id,
							'message'		=> $this->input->post('pushtext'),
							'certificate'	=> $app->certificate,
							'type'          => $pushtype
						);


					$res = $this->general_model->insert('pushcue', $data);

					//iphone
					$ch = curl_init("http://clients.tapcrowd.com/push/ios/push2.php");  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_exec($ch); 
					curl_close($ch); 

					//android
					$ch = curl_init("http://clients.tapcrowd.com/push/android/sendpush.php");  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_exec($ch); 
					curl_close($ch); 

					_updateTimeStamp($id);
					redirect('push/event/'.$id);
				}
			}
			$cdata['content'] 		= $this->load->view('c_push_add', array('error' => $error), TRUE);
			
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, __("Push notifications") => "push/event/".$event->id);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'push');
			$this->load->view('master', $cdata);
		} elseif($type == 'venue') {
			$error = '';
			$venue = $this->venue_model->getbyid($id);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if ($this->input->post('postback') == 'postback') {
				$this->load->library('form_validation');
				$this->form_validation->set_rules('pushtext', 'pushtext', 'trim|required|max_length[160]|min_length[10]');

				if($this->form_validation->run() == FALSE){
					$error = __("Oops, your notification might be too long or too short.<br />Maximum 160 characters and minimum 10 characters!");
				} else {
					$pushtype = 'push';
					$data = array(
							'appid'			=> $app->id,
							'message'		=> $this->input->post('pushtext'),
							'certificate'	=> $app->certificate,
							'type'          => $pushtype
						);


					$res = $this->general_model->insert('pushcue', $data);


					$ch = curl_init("http://clients.tapcrowd.com/push/ios/push2.php");  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_exec($ch); 
					curl_close($ch); 

					//android
					$ch = curl_init("http://clients.tapcrowd.com/push/android/sendpush.php");  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_exec($ch); 
					curl_close($ch); 

					_updateVenueTimeStamp($id);
					redirect('push/venue/'.$id);
				}
			}
			$cdata['content'] 		= $this->load->view('c_push_add', array('error' => $error), TRUE);
			
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= array($venue->name => "venue/view/".$venue->id, __("Push notifications") => "push/venue/".$venue->id);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'push');
			$this->load->view('master', $cdata);
		} elseif($type == 'app') {
			$error = '';
			
			$app = $this->app_model->get($id);
			_actionAllowed($app, 'app');

			if ($this->input->post('postback') == 'postback') {
				$this->load->library('form_validation');
				$this->form_validation->set_rules('pushtext', 'pushtext', 'trim|required|max_length[160]|min_length[10]');

				if($this->form_validation->run() == FALSE){
					$error = __("Oops, your notification might be too long or too short.<br />Maximum 160 characters and minimum 10 characters!");
				} else {
					$pushtype = 'push';
					$data = array(
							'appid'			=> $app->id,
							'message'		=> $this->input->post('pushtext'),
							'certificate'	=> $app->certificate,
							'type'          => $pushtype
						);


					$res = $this->general_model->insert('pushcue', $data);


					$ch = curl_init("http://clients.tapcrowd.com/push/ios/push2.php");  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_exec($ch); 
					curl_close($ch); 

					//android
					$ch = curl_init("http://clients.tapcrowd.com/push/android/sendpush.php");  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_exec($ch); 
					curl_close($ch); 

					$this->general_model->update('app', $app->id, array('timestamp' => time()));
					redirect('push/app/'.$id);
				}
			}
			$cdata['content'] 		= $this->load->view('c_push_add', array('error' => $error), TRUE);
		
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['crumb']			= array(__("Push notifications") => "push/app/".$app->id, __('Add') => $this->uri->uri_string());
			$this->load->view('master', $cdata);
		}
	}
	
	function sendtest() {
		$this->load->library('pushnotification');
		$this->pushnotification->testje();
	}
	
}