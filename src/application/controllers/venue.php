<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Venue extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('schedule_model');
	}

	//php 4 constructor
	function Venue() {
		parent::__construct();
		if(_authed()) { }
	}

	function index() {
		$this->session->set_userdata('venueid', '');
		redirect('venues');
	}

	function image_rules($str){
		$filename = 'w_logo';
		return image_check($filename, $_FILES);
	}

	function add($launcherid = '') {
		$this->load->library('form_validation');
		$error = "";
		$app = _currentApp();
        $languages = $this->language_model->getLanguagesOfApp($app->id);

        $venues = $this->venue_model->getAppVenue($app->id);

        if($app->subdomain != "" && $venues != false) {
            $this->iframeurl = "venues/app/".$app->id;
        }

        if(!empty($launcherid)) {
        	$this->iframeurl = "venues/launcher/".$app->id.'/'.$launcherid;
        }

		$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND venueid <> 0 GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}

		if($this->input->post('postback') == 'postback') {
			$this->form_validation->set_rules('w_address', 'w_address', 'trim');
			$this->form_validation->set_rules('w_lat', 'w_lat', 'trim');
			$this->form_validation->set_rules('w_lon', 'w_lon', 'trim');
			$this->form_validation->set_rules('w_tel', 'w_tel', 'trim');
			$this->form_validation->set_rules('w_fax', 'w_fax', 'trim');
			$this->form_validation->set_rules('website', 'website', 'trim');
			$this->form_validation->set_rules('w_email', 'w_email', 'trim|valid_email');
			$this->form_validation->set_rules('w_opening', 'w_opening', 'trim');
			$this->form_validation->set_rules('facebookurl', 'facebookurl', 'trim');
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('imageurl1', 'venue image 1', 'trim|callback_image_rules');
			$this->form_validation->set_rules('imageurl2', 'venue image 2', 'trim|callback_image_rules');
			$this->form_validation->set_rules('imageurl3', 'venue image 3', 'trim|callback_image_rules');
			$this->form_validation->set_rules('imageurl4', 'venue image 4', 'trim|callback_image_rules');
			$this->form_validation->set_rules('imageurl5', 'venue image 5', 'trim|callback_image_rules');
			if($languages != null) {
				foreach($languages as $language) {
					$this->form_validation->set_rules('w_name_'.$language->key, 'w_name_'.$language->key, 'trim|required');
					$this->form_validation->set_rules('w_info_'.$language->key, 'w_info_'.$language->key, 'trim');
				}
			}

			if($this->form_validation->run() == FALSE){
				$error = __("Some fields are missing.");
			} else {
				$website = checkhttp($this->input->post('website'));
				$user_data = array(
						'name'			=> $this->input->post('w_name_'.$app->defaultlanguage),
						'address'		=> $this->input->post('w_address'),
						'lat'			=> $this->input->post('w_lat'),
						'lon'			=> $this->input->post('w_lon'),
						'telephone'		=> $this->input->post('w_tel'),
						'fax'			=> $this->input->post('w_fax'),
						'email'			=> $this->input->post('w_email'),
						'website'		=> $website,
						'facebookurl'	=> $this->input->post('facebookurl'),
						'openinghours'	=> $this->input->post('w_opening'),
						'info'			=> $this->input->post('w_info_'.$app->defaultlanguage),
						'order'			=> $this->input->post('order'),
						'timestamp'		=> time()
					);

				$newid = $this->general_model->insert('venue', $user_data);


				if($newid){
					//catalogcategorie main group
	                $groupdata = array(
	            			'appid'	=> $app->id,
	            			'venueid'	=> $newid,
	            			'name'	=> 'catalogcategories',
	            			'parentid'	=> 0
	            		);
	                $cataloggroupid = $this->general_model->insert('group', $groupdata);

					//insert info launcher
					$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE id = 3")->row();
					$this->general_model->insert('module', array('venue' => $newid, 'moduletype' => 21));
					if($app->apptypeid != 7 && $app->apptypeid != 8) {
						$this->general_model->insert('launcher', array(
								'venueid' => $newid,
								'moduletypeid' => 15,
								'groupid' => $cataloggroupid,
								'active' => 0,
								'module' => 'catalog',
								'icon' => 'l_catalog',
								'order' => 7,
								'title' => 'Catalog'
							));
					}

					$launcherdata = array(
						'moduletypeid'	=> 21,
						'venueid'		=> $newid,
						'module'		=> $defaultlauncher->module,
						'title'			=> $defaultlauncher->title,
						'icon'			=> $defaultlauncher->icon,
						'order'			=> $defaultlauncher->order,
						'active'		=> 1
						);

					$this->db->insert('launcher',$launcherdata);

                    //add translated fields to translation table
					if($languages != null) {
                    foreach($languages as $language) {
                        $this->language_model->addTranslation('venue', $newid, 'name', $language->key, $this->input->post('w_name_'.$language->key));
                        $this->language_model->addTranslation('venue', $newid, 'info', $language->key, $this->input->post('w_info_'.$language->key));
                    }
					}
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/venueimages/".$newid)){
						mkdir($this->config->item('imagespath') . "upload/venueimages/".$newid, 0755, true);
					}

					$configpics['upload_path'] = $this->config->item('imagespath') . 'upload/venueimages/'.$newid;
					$configpics['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configpics['max_width']  = '2000';
					$configpics['max_height']  = '2000';
					$configpics['max_size']  = '1024';
					$this->load->library('upload', $configpics);
					$this->upload->initialize($configpics);

					$images = array();
					for($i = 1; $i <= 5; $i++){
						if ($this->upload->do_upload('imageurl'.$i)) {
							// Succesfully uploaded
							$returndata = $this->upload->data();
							$images['image'.$i] = 'upload/venueimages/'.$newid.'/'.$returndata['file_name'];
						} else {
							if(isset($_FILES['imageurl'.$i]['name']) && $_FILES['imageurl'.$i]['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						}
					}
					//update item with images
					if(count($images) > 0){
						$this->general_model->update('venue', $newid, $images);
					}

					// *** SAVE TAGS *** //
					if($newid != 0) {
						$this->db->where('venueid', $newid);
						$this->db->where('appid', $app->id);
						if($this->db->delete('tag')){
							if ($this->input->post('mytagsulselect') != '' && count($this->input->post('mytagsulselect')) > 0) {
								foreach ($this->input->post('mytagsulselect') as $tag) {
									$tags = array(
											'appid' 	=> $app->id,
											'tag' 		=> $tag,
											'venueid' => $newid
										);
									$this->db->insert('tag', $tags);
								}
							}
						}
					}

					// *** //

					//link venue to launcher (city)
	                if($launcherid != '') {
	                	$launcher = $this->db->query("SELECT * FROM launcher WHERE id = ? LIMIT 1", $launcherid)->row();
	                	$this->general_model->insert('tag', array(
	                		'appid' => $app->id,
	                		'tag'	=> $launcher->tag,
	                		'venueid'	=> $newid,
	                		'active'	=> 1
	                		));
	                }

					//about module for business apps and car dealer app
					if($app->apptypeid == 4 || $app->apptypeid == 11) {
						$this->db->insert("module", array("moduletype" => '21', "venue" => $newid));
						$this->db->insert("module", array("moduletype" => '22', "venue" => $newid));
						$dl = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = 22 LIMIT 1")->row();
						$this->db->insert("launcher", array("venueid" => $newid, "moduletypeid" => $dl->moduletypeid, "module" => $dl->module, "title" => $dl->title, "icon" => $dl->icon, "`order`" => $dl->order, "active" => 1));
						$this->db->insert("module", array("moduletype" => '23', "venue" => $newid));
						$dl = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = 23 LIMIT 1")->row();
						$this->db->insert("launcher", array("venueid" => $newid, "moduletypeid" => $dl->moduletypeid, "module" => $dl->module, "title" => $dl->title, "icon" => $dl->icon, "`order`" => $dl->order, "active" => 1));
					}

					//modules for resto app
					if($app->apptypeid == 8) {
						// $this->general_model->insert("module", array("moduletype" => '15', "venue" => $newid));
						// $this->general_model->insert("module", array("moduletype" => '41', "venue" => $newid));
						$this->venue_model->insertRestoLaunchers($app->id, $newid);
					}

					//modules for shop app
					if($app->apptypeid == 7) {
						// $this->general_model->insert("module", array("moduletype" => '15', "venue" => $newid));
						// $this->general_model->insert("module", array("moduletype" => '41', "venue" => $newid));
						$this->venue_model->insertShopLaunchers($app->id, $newid);
					}

					$this->general_model->insert('appvenue', array('venueid' => $newid, "appid" => $app->id));
					$this->general_model->insert('appvenue', array('venueid' => $newid, "appid" => 2));
					$this->session->set_flashdata('event_feedback', __('Your venue has succesfully been added.'));
					if($error == '') {
						if(isset($_GET['newapp'])) {
							redirect('apps/theme/'.$app->id.'?newapp');
						}

						redirect('venue/view/'.$newid.'/'.$launcherid);
					} else {
						//image error
						redirect('venue/edit/'.$newid.'/'.$launcherid.'?error=image');
					}
				} else {
					$error = __("Oops, something went wrong, please try again.");
				}
			}
		}

		$this->general_model->update('app', $app->id, array('timestamp' => time()));

		$data['error']			= $error;
        $data['languages']      = $languages;
        $data['app']            = $app;
        $data['launcherid']		= $launcherid;
        $data['apptags']		= $apptags;
		if(isset($_GET['newapp'])) {
			$cdata['content'] 		= $this->load->view('c_venue_add', $data, TRUE);
			$this->load->view('master_wizard', $cdata);
		} else {
			$cdata['content'] 		= $this->load->view('c_venue_add', $data, TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			if($launcherid == '') {
				$cdata['crumb']			= checkBreadcrumbsVenue($app, array(__("New") => "venue/add/"));
			} else {
				$this->load->model('module_mdl');
				$launcher = $this->module_mdl->getLauncherById($launcherid);
				$cdata['crumb']			= array($launcher->title => "venues/launcher/".$launcherid, 'New' => 'venue/add/'.$launcherid);
			}
			$this->load->view('master', $cdata);
		}
	}

	function edit($venueid, $launcherid = '') {
		$this->load->model('venue_model');
		$this->load->library('form_validation');
		$error = "";

		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->session->set_userdata('venueid', $venueid);
		$app = $this->app_model->getRealAppOfVenue($venueid);
		$this->session->set_userdata('appid', $app->appid);
		$app = $this->app_model->get($app->appid);
		$languages = $this->language_model->getLanguagesOfApp($app->id);

        $this->iframeurl = "venue/info/" . $venueid;
		if($app->apptypeid == 8  || $app->apptypeid == 7) {
			$this->iframeurl = "resto/info/" . $venue->id;
		}

        // TAGS
        $tags = $this->db->query("SELECT * FROM tag WHERE venueid = $venueid");
        if($tags->num_rows() == 0) {
            $tags = array();
        } else {
            $tagz = array();
            foreach ($tags->result() as $tag) {
                $tagz[] = $tag;
            }
            $tags = $tagz;
        }

		$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND venueid <> 0 GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}

		$schedule = $this->schedule_model->getScheduleByVenue($venueid);
		$headers = array(__('Day') => 'key', __('Date') => 'date', 'From-till' => 'caption');

		$launcher = $this->module_mdl->getLauncher(21, 'venue', $venue->id);
		if(!empty($launcherid) && $app->familyid == 3) {
			$launcher = $this->module_mdl->getLauncherById($launcherid);
		}
		$metadata = $this->metadata_model->getMetadata($launcher, 'venue', $venue->id, $app);

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('w_address', 'w_address', 'trim');
			$this->form_validation->set_rules('w_lat', 'w_lat', 'trim');
			$this->form_validation->set_rules('w_lon', 'w_lon', 'trim');
			$this->form_validation->set_rules('w_tel', 'w_tel', 'trim');
			$this->form_validation->set_rules('w_fax', 'w_fax', 'trim');
			$this->form_validation->set_rules('website', 'website', 'trim');
			$this->form_validation->set_rules('w_email', 'w_email', 'trim|valid_email');
			$this->form_validation->set_rules('w_opening', 'w_opening', 'trim');
			$this->form_validation->set_rules('facebookurl', 'facebookurl', 'trim');
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('imageurl1', 'venue image 1', 'trim|callback_image_rules');
			$this->form_validation->set_rules('imageurl2', 'venue image 2', 'trim|callback_image_rules');
			$this->form_validation->set_rules('imageurl3', 'venue image 3', 'trim|callback_image_rules');
			$this->form_validation->set_rules('imageurl4', 'venue image 4', 'trim|callback_image_rules');
			$this->form_validation->set_rules('imageurl5', 'venue image 5', 'trim|callback_image_rules');
			if($languages != null) {
            foreach($languages as $language) {
                $this->form_validation->set_rules('w_name_'.$language->key, 'w_name_'.$language->key, 'trim|required');
                $this->form_validation->set_rules('w_info_'.$language->key, 'w_info_'.$language->key, 'trim');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}
            }
			}

			//Uploads Images
			if(!is_dir($this->config->item('imagespath') . "upload/venueimages/".$venueid)){
				mkdir($this->config->item('imagespath') . "upload/venueimages/".$venueid, 0755, true);
			}

			$configpics['upload_path'] = $this->config->item('imagespath') . 'upload/venueimages/'.$venueid;
			$configpics['allowed_types'] = 'JPG|jpg|jpeg|png';
			$configpics['max_width']  = '2000';
			$configpics['max_height']  = '2000';
			$configpics['max_size']  = '1024';
			$this->load->library('upload', $configpics);

			$images = array();
			for($i = 1; $i <= 5; $i++){
				$this->upload->initialize($configpics);
				if ($this->upload->do_upload('imageurl'.$i)) {
					// Succesfully uploaded
					$returndata = $this->upload->data();
					$images['image'.$i] = 'upload/venueimages/'.$venueid.'/'.$returndata['file_name'];
				} else {
					$imagenumber = 'image'.$i;
					$images['image'.$i] = $venue->{$imagenumber};
					if(isset($_FILES['imageurl'.$i]['name']) && $_FILES['imageurl'.$i]['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				}
			}

			if($this->form_validation->run() == FALSE || $error != ''){
				$error .= validation_errors();
			} else {
				$website = checkhttp($this->input->post('website'));
				$user_data = array(
						'name'			=> $this->input->post('w_name_'.  $app->defaultlanguage),
						'address'		=> $this->input->post('w_address'),
						'lat'			=> $this->input->post('w_lat'),
						'lon'			=> $this->input->post('w_lon'),
						'telephone'		=> $this->input->post('w_tel'),
						'fax'			=> $this->input->post('w_fax'),
						'email'			=> $this->input->post('w_email'),
						'website'		=> $website,
						'facebookurl'	=> $this->input->post('facebookurl'),
						'openinghours'	=> $this->input->post('w_opening'),
						'order'			=> $this->input->post('order'),
						'info'			=> $this->input->post('w_info_'.  $app->defaultlanguage)
					);


				if($this->general_model->update('venue', $venueid, $user_data)){
					//update item with images
					if(count($images) > 0){
						$this->general_model->update('venue', $venueid, $images);
					}

					// *** SAVE TAGS *** //
					// $this->db->where('venueid', $venueid);
					// if($this->db->delete('tag')){
					// 	if ($this->input->post('mytagsulselect') != '' && count($this->input->post('mytagsulselect')) > 0) {
					// 		foreach ($this->input->post('mytagsulselect') as $tag) {
					// 			$tags = array(
					// 					'appid' 	=> $app->id,
					// 					'tag' 		=> $tag,
					// 					'venueid' => $venueid
					// 				);
					// 			$this->db->insert('tag', $tags);
					// 		}
					// 	}
					// }
					// *** //

                        //add translated fields to translation table
						if($languages != null) {
                        foreach($languages as $language) {
                            $nameSuccess = $this->language_model->updateTranslation('venue', $venueid, 'name', $language->key, $this->input->post('w_name_'.$language->key));
                            if(!$nameSuccess) {
								$this->language_model->addTranslation('venue', $venueid, 'name', $language->key, $this->input->post('w_name_'.$language->key));
							}
							$descrSuccess = $this->language_model->updateTranslation('venue', $venueid, 'info', $language->key, $this->input->post('w_info_'.$language->key));
							if(!$descrSuccess) {
								$this->language_model->addTranslation('venue', $venueid, 'info', $language->key, $this->input->post('w_info_'.$language->key));
							}
							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
									$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'venue', $venueid, $app, $language);
										}
									}
								}
							}
                        }
						}

					$this->session->set_flashdata('event_feedback', __('Your changes have successfully been saved.'));
					_updateVenueTimeStamp($venueid);
					redirect('venue/view/'.$venueid.'/'.$launcherid);
				} else {
					$error = __("Oops, something went wrong, please try again.");
				}
			}
		}

		$this->general_model->update('app', $app->id, array('timestamp' => time()));

		$data['error']			= $error;
		$data['venue']			= $venue;
        $data['languages']      = $languages;
        $data['app']            = $app;
		$data['tags']			= $tags;
		$data['apptags']		= $apptags;
		$data['schedule']		= $schedule;
		$data['headers']		= $headers;
		$data['metadata']		= $metadata;
		$data['launcherid'] 	= $launcherid;

		$cdata['content'] 		= $this->load->view('c_venue_edit', $data, TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("venue" => $venue), TRUE);
		if($launcherid == '') {
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Edit") => "venue/edit/".$venue->id));
		} else {
			$this->load->model('module_mdl');
			$launcher = $this->module_mdl->getLauncherById($launcherid);
			$cdata['crumb']			= array($launcher->title => "venues/launcher/".$launcherid, $venue->name => "venue/view/".$venue->id.'/'.$launcherid, __('Edit') => 'venue/edit/'.$venue->id.'/'.$launcherid);
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'about');
		$this->load->view('master', $cdata);
	}

	function remove($venueid) {
		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->general_model->update('app', $app->id, array('timestamp' => time()));
		$this->venue_model->setActive($venueid, 0);
		$this->general_model->update('venue', $venueid, array('deleted' => 1));
		redirect('venues');
	}

	function view($id, $launcherid = '') {
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->session->set_userdata('venueid', $id);
		$app = $this->app_model->getRealAppOfVenue($id);
		$this->session->set_userdata('appid', $app->appid);
		$app_id = $app->appid;
		$app = $this->app_model->get($app->appid);
                
		$this->iframeurl = "venue/index/" . $venue->id;
		if($app->apptypeid == 8  || $app->apptypeid == 7) {
			$this->iframeurl = "resto/index/" . $venue->id;
		}

		// Modules ophalen voor eventtype
		$this->load->model('module_mdl');
		$eventmodules = $this->module_mdl->getVenueModules($venue->id, $app->apptypeid);
		/*
		// Getting Form Templates          
        $this->load->model('formtemplate_model');
		$templateforms = $this->formtemplate_model->getFormTemplateIdByAppId($app->apptypeid, $app_id, $id, 'venue');
		if(count($templateforms)>0){
		    $eventmodules[] = (object)array('template' => $templateforms);
		}
		*/
		$artistLauncher = '';
		$artistsActive = false;
		$subflavor = '';
		if($app!=null) {
		$subflavor = $this->module_mdl->getSubflavor($app->id);
		}
		// if($app->apptypeid == 4 || $app->apptypeid == 11) {
		// 	$artistLauncher = $this->module_mdl->getArtistLauncher($app->id, $app->apptypeid);
		// 	$artistsActive = $this->module_mdl->artistsActive($app->id);
		// }

        // TAGS
        $tags = $this->db->query("SELECT * FROM tag WHERE venueid = $id");
        if($tags->num_rows() == 0) {
            $tags = array();
        } else {
            $tagz = array();
            foreach ($tags->result() as $tag) {
                $tagz[] = $tag;
            }
            $tags = $tagz;
        }

		$venue->tags = $tags;

		$launchers = $this->module_mdl->getAllLaunchersOfVenue($id);
		$dynamicModules = $this->module_mdl->getDynamicModules($id, 'venue');
		$groupModules = $this->module_mdl->getGroupModules($id, 'venue');

		$subflavors = $this->module_mdl->getSubflavorsOfFlavor($app->apptypeid);

		$venuelaunchers = array();
		if($app->familyid == 3) {
			$venuelaunchers = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND moduletypeid = 31")->result();
		}
		
		if($app->apptypeid == 8) {
			$cdata['content'] 		= $this->load->view('c_venue_resto', array('venue' => $venue, 'eventmodules' => $eventmodules, 'launchers' => $launchers, 'subflavor' => $subflavor, 'artistsActive' => $artistsActive, 'app' => $app, 'artistLauncher' => $artistLauncher, 'dynamicModules' => $dynamicModules, 'groupModules'	=> $groupModules, 'subflavors' => $subflavors), TRUE);
		} elseif($app->apptypeid == 7) {
			$cdata['content'] 		= $this->load->view('c_venue_shop', array('venue' => $venue, 'eventmodules' => $eventmodules, 'launchers' => $launchers, 'subflavor' => $subflavor, 'artistsActive' => $artistsActive, 'app' => $app, 'artistLauncher' => $artistLauncher, 'dynamicModules' => $dynamicModules, 'groupModules'	=> $groupModules, 'subflavors' => $subflavors), TRUE);
		} else {
			$cdata['content'] 		= $this->load->view('c_venue', array('venue' => $venue, 'eventmodules' => $eventmodules, 'launchers' => $launchers, 'subflavor' => $subflavor, 'artistsActive' => $artistsActive, 'app' => $app, 'artistLauncher' => $artistLauncher, 'dynamicModules' => $dynamicModules, 'groupModules'	=> $groupModules, 'subflavors' => $subflavors, 'venuelaunchers' => $venuelaunchers, 'launcherid' => $launcherid), TRUE);
		}
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("venue" => $venue), TRUE);
		if($launcherid == '') {
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id));
		} else {
			$this->load->model('module_mdl');
			$launcher = $this->module_mdl->getLauncherById($launcherid);
			$cdata['crumb']			= array($launcher->title => "venues/launcher/".$launcherid, $venue->name => 'venue/view/'.$venue->id.'/'.$launcherid);
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, '');
		$this->load->view('master', $cdata);
	}

	function travelinfo($id) {
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

        $this->iframeurl = "venue/info/" . $venue->id;

		$error = '';
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		$this->load->library('form_validation');
		if($this->input->post('postback') == "postback") {
			foreach($languages as $language) {
				$this->form_validation->set_rules('txt_travelinfo_'.$language->key, 'txt_travelinfo_'.$language->key, 'trim');
			}


			if($this->form_validation->run() == FALSE){
				$error = __("Some fields are missing.");
			} else {
				// Save exhibitor
				$res = $this->general_model->update('venue', $id, array("travelinfo" => set_value('txt_travelinfo_'.$app->defaultlanguage)));

				if($res != FALSE){
					if($languages != null) {
						foreach($languages as $language) {
							$travelSuccess = $this->language_model->updateTranslation('venue', $id, 'travelinfo', $language->key, $this->input->post('txt_travelinfo_'.$language->key));
							if(!$travelSuccess) {
								$this->language_model->addTranslation('venue', $id, 'travelinfo', $language->key, $this->input->post('txt_travelinfo_'.$language->key));
							}
						}
					}
					$this->session->set_flashdata('event_feedback', __('Travel info has succesfully been set!'));
					redirect('venue/view/'.$id.'/'.$launcherid);
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}

			}
		}

		$cdata['content'] 		= $this->load->view('c_venue_travel', array('venue' => $venue, 'error' => $error, 'languages' => $languages), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("venue" => $venue), TRUE);
		$cdata['crumb']			= array(($app->familyid == 3) ? __("POI's") : __("Venues") => "venues", $venue->name => "venue/view/".$venue->id, __("Travel Info") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function active($id, $status, $launcherid = '') {
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		if($status =='on' && $id > 0) {
			$this->venue_model->setActive($id, 1);
			_updateVenueTimeStamp($id);
			//echo('This venue is currently <strong>active</strong>');
		} else if($status =='off' && $id > 0) {
			$this->venue_model->setActive($id, 0);
			_updateVenueTimeStamp($id);
			//echo('This venue is currently <strong>not active</strong>');
		} else {
			//show_404('page');
		}

		if ($this->session->userdata('prev_page') != '') {
			header('Location:'.$this->session->userdata('prev_page'));
		} else {
			redirect('venue/view/'.$id.'/'.$launcherid);
		}
	}

	function module($trigger= '', $venueid, $moduleid) {
		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');

		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.manage', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to manage Applications', 'error');
			redirect('apps/view/'.$app->id);
		}

		$this->load->model('module_mdl');
		switch($trigger){
			case 'activate':
				// Add module to event
				$groupid = '';
				if($moduleid == 54) {
					$groupid = $this->module_mdl->checkPlacesCats($venueid, 'venue', $app);
				}
				if($moduleid == 52) {
					$groupid = $this->module_mdl->checkTeamCats($venueid, 'venue', $app);
				}
				if($moduleid == 15 && $app->apptypeid == 4) {
					$groupid = $this->module_mdl->checkCatalogCats($venueid, 'venue', $app);
				}

				if($moduleid == 49) {
					$this->module_mdl->checkUserModuleSettings($app->id);
				}

				$this->module_mdl->addModuleToVenue($venueid, $moduleid, $app, $groupid);
				// if($moduleid == '20') {
				// 	$this->load->library('email');

				// 	$this->email->from('apps@tapcrowd.com', 'MobileJuice');
				// 	$this->email->to('info@mobilejuice.be');

				// 	$this->email->subject('Push notification certificate');
				// 	$this->email->message('Push notification module geactiveerd voor app ' . $app->name . ' met id: ' . $app->id . '.
				// 							De module werd geactiveerd via venueid:' . $venueid . '. Controleer of reeds een certificaat bestaat en maak indien nodig één aan voor deze app.
				// 							Het emailadres van de user is: '. _currentUser()->email);

				// 	$this->email->send();
				// }

				if($moduleid == '48')
				{
					if($this->db->query("SELECT * FROM basket WHERE venueid=$venueid")->num_rows() == 0)
					{
						$data = array('venueid' => $venueid, 'active' => 1);
						$this->db->insert('basket', $data);
					}
					else
					{
						$data = array('active' => 1);
						$this->db->where('venueid', $venueid);
						$this->db->update('basket', $data);
					}
				}
				break;
			case 'deactivate':
				// TODO Remove module from event
				$this->module_mdl->removeModuleFromVenue($venueid, $moduleid);

				if($moduleid == 28) {
					$this->load->model('social_model');
					$this->social_model->removeFromVenue($venueid);
				}
				
				if($moduleid == '48')
				{
					$data = array('active' => 0);
					$this->db->where('venueid', $venueid);
					$this->db->update('basket', $data);
				}
				break;
			default:
				break;
		}
		_updateVenueTimeStamp($venueid);

		$eventmodules = $this->module_mdl->getVenueModules($venue->id, $app->apptypeid);

		$artistLauncher = '';
		$artistsActive = false;
		$subflavor = '';
		if($app!=null) {
		$subflavor = $this->module_mdl->getSubflavor($app->id);
		}

		if(($app->apptypeid == 4 || $app->apptypeid == 11) && $moduleid == 18) {
			$artistLauncher = $this->module_mdl->getArtistLauncher($app->id, $app->apptypeid);
			$artistsActive = $this->module_mdl->artistsActive($app->id);
		}

        // TAGS
        $tags = $this->db->query("SELECT * FROM tag WHERE venueid = $venue->id");
        if($tags->num_rows() == 0) {
            $tags = array();
        } else {
            $tagz = array();
            foreach ($tags->result() as $tag) {
                $tagz[] = $tag;
            }
            $tags = $tagz;
        }

		$venue->tags = $tags;

		$launchers = $this->module_mdl->getAllLaunchersOfVenue($venue->id);
		$dynamicModules = $this->module_mdl->getDynamicModules($venue->id, 'venue');
		$groupModules = $this->module_mdl->getGroupModules($venue->id, 'venue');

		$subflavors = $this->module_mdl->getSubflavorsOfFlavor($app->apptypeid);

		if($app->apptypeid == 8) {
			$cdata['content'] 		= $this->load->view('c_venue_resto', array('venue' => $venue, 'eventmodules' => $eventmodules, 'launchers' => $launchers, 'subflavor' => $subflavor, 'artistsActive' => $artistsActive, 'app' => $app, 'artistLauncher' => $artistLauncher, 'dynamicModules' => $dynamicModules, 'groupModules'	=> $groupModules, 'subflavors' => $subflavors), TRUE);
		} elseif($app->apptypeid == 7) {
			$cdata['content'] 		= $this->load->view('c_venue_shop', array('venue' => $venue, 'eventmodules' => $eventmodules, 'launchers' => $launchers, 'subflavor' => $subflavor, 'artistsActive' => $artistsActive, 'app' => $app, 'artistLauncher' => $artistLauncher, 'dynamicModules' => $dynamicModules, 'groupModules'	=> $groupModules, 'subflavors' => $subflavors), TRUE);
		} else {
			$cdata['content'] 		= $this->load->view('c_venue', array('venue' => $venue, 'eventmodules' => $eventmodules, 'launchers' => $launchers, 'subflavor' => $subflavor, 'artistsActive' => $artistsActive, 'app' => $app, 'artistLauncher' => $artistLauncher, 'dynamicModules' => $dynamicModules, 'groupModules'	=> $groupModules, 'subflavors' => $subflavors), TRUE);
		}
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array("venue" => $venue), TRUE);
		$cdata['crumb']			= array(($app->familyid == 3) ? __("POI's") : __("Venues") => "venues", $venue->name => "venue/view/".$venue->id);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, '');
		$this->load->view('master', $cdata);
	}

	function removemap($mapid = 0, $venueid = 0) {
		if($mapid == FALSE || $mapid == 0 || $venueid == FALSE || $venueid == 0) redirect('venues');
		$this->load->model('venue_model');
		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->load->model('map_model');
		$map = $this->map_model->getMapsById($mapid);
		if($map == FALSE) redirect('venues');
		if(!file_exists($this->config->item('imagespath') . $map->imageurl)) redirect('venues');
		// Delete image + generated thumbs

		if($this->db->delete('map', array('id' => $map->id))){
			//remove items
			$this->map_model->removeItemsOfMap($map->id, 'venue', $venueid);

			unlink($this->config->item('imagespath') . $map->imageurl);
			array_map("unlink", glob("cache/*".end(explode('/', $map->imageurl))));
			array_map("unlink", glob("cache/maps/*".end(explode('/', $map->imageurl))));
			$this->session->set_flashdata('event_feedback', __('Floorplan has successfully been removed.'));
		} else {
			$this->session->set_flashdata('event_feedback', __('Failed remove floorplan.'));
		}

		_updateVenueTimeStamp($venueid);
		redirect('venue/view/'.$venueid);

	}

	function removeimage($venueid = 0, $imagenumber) {
		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');

		if(!file_exists($this->config->item('imagespath') . $venue->{'image'.$imagenumber})) redirect('venues');

		// Delete image + generated thumbs

		unlink($this->config->item('imagespath') . $venue->{'image'.$imagenumber});
		$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

		$data = array('image'.$imagenumber => '');
		$this->general_model->update('venue', $venueid, $data);

		_updateVenueTimeStamp($venueid);
		redirect('venue/view/'.$venueid);
	}

	function addtag() {
		$venueid = $this->input->post('venueid');

		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');

		//launcheritem
		$tag = $this->input->post('tag');
		$launcherexists = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND (moduletypeid = 31 OR moduletypeid = 30) AND (title = ? OR tag = ?)", array($tag, $tag));
		if($launcherexists->num_rows() > 0) {
			$data = array(
				'appid'		=> $app->id,
				'venueid'	=> $venueid,
				'tag'		=> $launcherexists->row()->tag,
				'active'	=> 1
			);
			$this->general_model->insert('tag', $data);
		} else {
			$data = array(
				'appid'		=> $app->id,
				'venueid'	=> $venueid,
				'tag'		=> $tag,
				'active'	=> 1
			);
			$this->general_model->insert('tag', $data);

			$launcherdata = array(
				'appid'=>$app->id,
				'moduletypeid'=>31,
				'module'=>'venueTags',
				'title'=>$tag,
				'tag'=>$tag,
				'icon'=>'l_venues',
				'active'=>1
			);
			if($app->id != 55) {
				$this->general_model->insert('launcher', $launcherdata);
			}
		}
		_updateVenueTimeStamp($venueid);


		redirect('apps/view/'.$app->id);
	}

	function changelauncher($venueid, $oldlauncherid, $launcherid) {
		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->venue_model->changelauncher($venueid, $oldlauncherid, $launcherid, $app->id);
	}
}
