<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Sponsors extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('sponsor_model');
		$this->load->model('premium_model');

		# Load settings http://admin.shahid.tapcrowd.com/import/exhibitors/5044/event
		$this->_module_settings = (object)array(
			'singular' => 'Sponsor',
			'plural' => 'Sponsors',
			'title' => __('Sponsors'),
			'parentType' => 'event',
			'browse_url' => 'sponsors/--parentType--/--parentId--',
			'add_url' => 'sponsors/add/--parentId--/--parentType--',
			'edit_url' => 'sponsors/edit/--id--',
			'module_url' => 'module/editByController/sponsors/--parentType--/--parentId--/0',
			'import_url' => 'import/sponsors/--id--/--parentType--',
			'headers' => array(
				__('Name') => 'name',
				__('Order') => 'order'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'sponsors/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'sponsors/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'filter' => array(
				'id' => 'sponsorgroup',
				'class' => 'groupselect',
				'name' => 'sponsorgroup',
				'label' => __('All sponsorgroups'),
				'selected' => '',
				'data' => array(),
				'url' => 'sponsors/--parentType--/--parentId--'
			),
			'filtermanage' => array(
				'class' => 'edit btn',
				'title' => __('Edit Sponsorgroups'),
				'url' => 'sponsorgroups/--parentType--/--parentId--'
			),
			'checkboxes' => false,
			'deletecheckedurl' => 'sponsors/removemany/--parentId--/--parentType--'
		);
	}

	function index() {
		$this->event();
	}

	function event($id, $groupid = '') {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		$this->iframeurl = "sponsors/event/" . $id;

		if($groupid != ''){
			$sponsors = $this->sponsor_model->getSponsorByGroups($groupid);
		} else {
			$sponsors = $this->sponsor_model->getSponsorsByEventID($id);
		}

		$sponsorgroups = $this->sponsor_model->getSponsorgroupsByEventID($id);
		
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# filter sponsorgroups
		$filter =& $this->_module_settings->filter;
		$filter['data'] = $sponsorgroups;
		$filter['url'] = str_replace(array('--parentType--','--parentId--'), array('event', $id), $filter['url']);
		$filter['selected'] = $groupid;
		$filtermanage =& $this->_module_settings->filtermanage;
		$filtermanage['url'] = str_replace(array('--parentType--','--parentId--'), array('event', $id), $filtermanage['url']);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);
		# Module import url
		$import_url =& $this->_module_settings->import_url;
		$import_url = str_replace(array('--parentType--','--id--'), array('event', $id), $import_url);
		
		$launcher = $this->module_mdl->getLauncher(19, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($sponsors)), true);

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']	= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'sponsors');
		$this->load->view('master', $cdata);
	}

	function venue($id, $groupid = '') {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->iframeurl = "sponsors/venue/" . $id;

		if($groupid != ''){
			$sponsors = $this->sponsor_model->getSponsorByGroups($groupid);
		} else {
			$sponsors = $this->sponsor_model->getSponsorsByVenueID($id);
		}

		$sponsorgroups = $this->sponsor_model->getSponsorgroupsByVenueID($id);

		$this->_module_settings->parentType = 'venue';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'venue', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'venue', $delete_href);
		# filter sponsorgroups
		$filter =& $this->_module_settings->filter;
		$filter['data'] = $sponsorgroups;
		$filter['url'] = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $filter['url']);
		$filter['selected'] = $groupid;
		$filtermanage =& $this->_module_settings->filtermanage;
		$filtermanage['url'] = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $filtermanage['url']);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $deletecheckedurl);
		# Module import url
		$import_url =& $this->_module_settings->import_url;
		$import_url = str_replace(array('--parentType--','--id--'), array('venue', $id), $import_url);

		$launcher = $this->module_mdl->getLauncher(19, 'venue', $venue->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($sponsors)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $this->_module_settings->title => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'sponsors');
		$this->load->view('master', $cdata);
	}

    function add($id, $type = 'event') {
		if($type == 'venue'){
			$this->load->library('form_validation');
			$error = "";

			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = "sponsors/venue/" . $id;

			$launcher = $this->module_mdl->getLauncher(19, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, '', '', $app);

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'sponsor' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('sponsorgroup', 'sponsorgroup', 'trim');
				$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
               	$this->form_validation->set_rules('website', 'Website', 'trim|prep_url|valid_url|xss_clean');
               	// $this->form_validation->set_rules('premium', 'premium', 'trim');
                if($languages != null) {
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
                }

				$this->form_validation->set_rules('order', 'order', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {
					$data = array(
							"sponsorgroupid"	=> set_value('sponsorgroup'),
							"name"      => set_value('name_'.$app->defaultlanguage),
							"venueid" 	=> $venue->id,
							"description" => set_value('description_'.$app->defaultlanguage),
                            "website"   => set_value('website'),
							"order"		=> set_value('order')
						);

					if($sponsorid = $this->general_model->insert('sponsor', $data)){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/sponsorimages/".$sponsorid)){
							mkdir($this->config->item('imagespath') . "upload/sponsorimages/".$sponsorid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/sponsorimages/'.$sponsorid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = ""; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/sponsorimages/'. $sponsorid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('sponsor', $sponsorid, array('image' => $image));

						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$this->general_model->insert('tc_tag', array(
								'appid' => $app->id,
								'itemtype' => 'sponsor',
								'itemid' => $sponsorid,
								'tag' => $tag
							));
						}

						//save premium
						// $this->premium_model->save($this->input->post('premium'), 'sponsor', $sponsorid, $this->input->post('extraline'), false, 'venue', $venue->id);

                        //add translated fields to translation table
                        if($languages != null) {
                        foreach($languages as $language) {
                            $this->language_model->addTranslation('sponsor', $sponsorid, 'name', $language->key, $this->input->post('name_'.$language->key));
                            $this->language_model->addTranslation('sponsor', $sponsorid, 'description', $language->key, $this->input->post('description_'.$language->key));
							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
									$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'sponsor', $sponsorid, $app, $language);
										}
									}
								}
							}
                        }
                    	}
						$this->session->set_flashdata('event_feedback', __('The sponsor has successfully been added!'));
						_updateVenueTimeStamp($venue->id);

						if($error == '') {
							redirect('sponsors/venue/'.$venue->id);
						}else {
							//image error
							redirect('sponsors/edit/'.$sponsorid.'/venue?error=image');
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			$groups = $this->sponsor_model->getSponsorGroupsByVenue($venue->id);

			$cdata['content'] 		= $this->load->view('c_sponsors_add', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'sponsorgroups' => $groups, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "sponsors/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'sponsors');
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = "sponsors/event/" . $id;

			$launcher = $this->module_mdl->getLauncher(19, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, '', '', $app);

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'sponsor' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('sponsorgroup', 'sponsorgroup', 'trim');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
				$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
                $this->form_validation->set_rules('website', 'Website', 'trim|prep_url|valid_url|xss_clean');
				$this->form_validation->set_rules('premium', 'premium', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {
					$data = array(
							"sponsorgroupid"	=> set_value('sponsorgroup'),
							"name"      => set_value('name_'.$app->defaultlanguage),
							"eventid" 	=> $event->id,
							"description" => set_value('description_'.$app->defaultlanguage),
                            "website"   => set_value('website'),
							"order"		=> set_value('order')
						);

					if($sponsorid = $this->general_model->insert('sponsor', $data)){
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/sponsorimages/".$sponsorid)){
							mkdir($this->config->item('imagespath') . "upload/sponsorimages/".$sponsorid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/sponsorimages/'.$sponsorid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = ""; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/sponsorimages/'. $sponsorid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('sponsor', $sponsorid, array('image' => $image));

						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$this->general_model->insert('tc_tag', array(
								'appid' => $app->id,
								'itemtype' => 'sponsor',
								'itemid' => $sponsorid,
								'tag' => $tag
							));
						}

						//save premium
						// $this->premium_model->save($this->input->post('premium'), 'sponsor', $sponsorid, $this->input->post('extraline'), false, 'event', $event->id);

                        //add translated fields to translation table
                        foreach($languages as $language) {
                            $this->language_model->addTranslation('sponsor', $sponsorid, 'name', $language->key, $this->input->post('name_'.$language->key));
                            $this->language_model->addTranslation('sponsor', $sponsorid, 'description', $language->key, $this->input->post('description_'.$language->key));
							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
									$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'sponsor', $sponsorid, $app, $language);
										}
									}
								}
							}
                        }
						$this->session->set_flashdata('event_feedback', __('The sponsor has successfully been added!'));
						_updateTimeStamp($event->id);
						if($error == '') {
							redirect('sponsors/event/'.$event->id);
						}else {
							//image error
							redirect('sponsors/edit/'.$sponsorid.'/event?error=image');
						}
					} else {
						$error = "Oops, something went wrong. Please try again.";
					}
				}
			}

			$groups = $this->sponsor_model->getSponsorGroupsByEvent($event->id);

			$cdata['content'] 		= $this->load->view('c_sponsors_add', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'sponsorgroups' => $groups, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "sponsors/event/".$event->id, __("Add new") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "sponsors/event/".$event->id, __("Add new") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'sponsors');
			$this->load->view('master', $cdata);
		}
	}

	function image_rules($str){
		$filename = 'image';
		return image_check($filename, $_FILES);
	}

    function edit($id, $type) {
		if($type == 'venue'){
			$this->load->library('form_validation');
			$error = "";
			// EDIT SPONSOR
			$sponsor = $this->sponsor_model->getSponsorById($id);
			if($sponsor == FALSE) redirect('venues');

			$venue = $this->venue_model->getById($sponsor->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = "sponsors/view/" . $id;

			// $premium = $this->premium_model->get('sponsor', $sponsor->id);
			$launcher = $this->module_mdl->getLauncher(19, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, 'sponsor', $sponsor->id, $app);

			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'sponsor' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'sponsor'");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag;
				}
				$tags = $tagz;
			}

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('sponsorgroup', 'sponsorgroup', 'trim');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
				$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
                $this->form_validation->set_rules('website', 'Website', 'trim|prep_url|valid_url|xss_clean');
				$this->form_validation->set_rules('premium', 'premium', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');
				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/sponsorimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/sponsorimages/".$id, 0755, true);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/sponsorimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('image')) {
					$image = $sponsor->image; //No image uploaded!
					if($_FILES['image']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/sponsorimages/'. $id . '/' . $returndata['file_name'];
				}

				if($this->form_validation->run() == FALSE || $error != ''){
					$error .= validation_errors();
				} else {
					$data = array(
							"sponsorgroupid"	=> set_value('sponsorgroup'),
							"name" => set_value('name_'. $app->defaultlanguage),
							"description" 	=> set_value('description_'. $app->defaultlanguage),
							"image"	=> $image,
                            "website"   => set_value('website'),
							"order"		=> set_value('order')
						);

					if($this->general_model->update('sponsor', $id, $data)){
						//save premium
						// $this->premium_model->save($this->input->post('premium'), 'sponsor', $id, $this->input->post('extraline'), $premium, 'venue', $venue->id);

						$this->db->where('itemtype', 'sponsor');
						$this->db->where('itemid', $id);
						$this->db->delete('tc_tag');
						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$this->general_model->insert('tc_tag', array(
								'appid' => $app->id,
								'itemtype' => 'sponsor',
								'itemid' => $id,
								'tag' => $tag
							));
						}

                        //add translated fields to translation table
                        foreach($languages as $language) {
                            $nameSuccess = $this->language_model->updateTranslation('sponsor', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
                            if(!$nameSuccess) {
								$this->language_model->addTranslation('sponsor', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
							}
							$descrSuccess = $this->language_model->updateTranslation('sponsor', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							if(!$descrSuccess) {
								$this->language_model->addTranslation('sponsor', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							}
							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
									$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'sponsor', $id, $app, $language);
										}
									}
								}
							}
                        }
						$this->session->set_flashdata('event_feedback', __('The sponsor has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						redirect('sponsors/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$groups = $this->sponsor_model->getSponsorGroupsByVenue($venue->id);

			$cdata['content'] 		= $this->load->view('c_sponsors_edit', array('venue' => $venue, 'sponsor' => $sponsor, 'error' => $error, 'languages' => $languages, 'app' => $app, 'sponsorgroups' => $groups, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);
			// $cdata['content'] 		= $this->load->view('c_sponsors_edit', array('venue' => $venue, 'sponsor' => $sponsor, 'error' => $error, 'languages' => $languages, 'app' => $app, 'sponsorgroups' => $groups, 'premium' => $premium), TRUE);
			$cdata['crumb']			= array("Venues" => "venues", $venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "sponsors/venue/".$venue->id, "Edit: " . $sponsor->name => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'sponsors');
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$sponsor = $this->sponsor_model->getSponsorById($id);
			if($sponsor == FALSE) redirect('events');

			$event = $this->event_model->getbyid($sponsor->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = "sponsors/view/" . $id;

			// $premium = $this->premium_model->get('sponsor', $sponsor->id);
			// $premium = $this->premium_model->get('sponsor', $sponsor->id);
			$launcher = $this->module_mdl->getLauncher(19, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, 'sponsor', $sponsor->id, $app);

			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'sponsor' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'sponsor'");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag;
				}
				$tags = $tagz;
			}

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('sponsorgroup', 'sponsorgroup', 'trim');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
				$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
                $this->form_validation->set_rules('website', 'Website', 'trim|prep_url|valid_url|xss_clean');
				// $this->form_validation->set_rules('premium', 'premium', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');

				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/sponsorimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/sponsorimages/".$id, 0755, true);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/sponsorimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('image')) {
					$image = $sponsor->image; //No image uploaded!
					if($_FILES['image']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/sponsorimages/'. $id . '/' . $returndata['file_name'];
				}

				if($this->form_validation->run() == FALSE || $error != ''){
					$error .= validation_errors();
				} else {
					$data = array(
							"sponsorgroupid"	=> set_value('sponsorgroup'),
							"name" => set_value('name_'. $app->defaultlanguage),
							"description" 	=> set_value('description_'. $app->defaultlanguage),
							"image"	=> $image,
                            "website"   => set_value('website'),
							"order"		=> set_value('order')
						);

					if($this->general_model->update('sponsor', $id, $data)){
						//save premium
						// $this->premium_model->save($this->input->post('premium'), 'sponsor', $id, $this->input->post('extraline'), $premium, 'event', $event->id);

						$this->db->where('itemtype', 'sponsor');
						$this->db->where('itemid', $id);
						$this->db->delete('tc_tag');
						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$this->general_model->insert('tc_tag', array(
								'appid' => $app->id,
								'itemtype' => 'sponsor',
								'itemid' => $id,
								'tag' => $tag
							));
						}

                        //add translated fields to translation table
                        foreach($languages as $language) {
                            $nameSuccess = $this->language_model->updateTranslation('sponsor', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
                            if(!$nameSuccess) {
								$this->language_model->addTranslation('sponsor', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
							}
							$descrSuccess = $this->language_model->updateTranslation('sponsor', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							if(!$descrSuccess) {
								$this->language_model->addTranslation('sponsor', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							}
							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
									$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'sponsor', $id, $app, $language);
										}
									}
								}
							}
                        }
						$this->session->set_flashdata('event_feedback', __('The sponsor has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('sponsors/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$groups = $this->sponsor_model->getSponsorGroupsByEvent($event->id);

			$cdata['content'] 		= $this->load->view('c_sponsors_edit', array('event' => $event, 'sponsor' => $sponsor, 'error' => $error, 'languages' => $languages, 'app' => $app, 'sponsorgroups' => $groups, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);
			// $cdata['content'] 		= $this->load->view('c_sponsors_edit', array('event' => $event, 'sponsor' => $sponsor, 'error' => $error, 'languages' => $languages, 'app' => $app, 'sponsorgroups' => $groups, 'premium' => $premium), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "sponsors/event/".$event->id, __("Edit: ") . $sponsor->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "sponsors/event/".$event->id, __("Edit: ") . $sponsor->name => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'sponsors');
			$this->load->view('master', $cdata);
		}
	}

	function delete($id, $type) {
		if($type == 'venue'){
			$sponsor = $this->sponsor_model->getSponsorById($id);
			$venue = $this->venue_model->getbyid($sponsor->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
            //delete translations
            $this->language_model->removeTranslations('sponsor', $id);
			if($this->general_model->remove('sponsor', $id)){
				$this->sponsor_model->removePremium($id);
				$this->session->set_flashdata('event_feedback', __('The sponsor has successfully been deleted'));
				_updateVenueTimeStamp($sponsor->venueid);
				redirect('sponsors/venue/'.$sponsor->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('sponsors/venue/'.$sponsor->venueid);
			}
		} else {
			$sponsor = $this->sponsor_model->getSponsorById($id);
			$event = $this->event_model->getbyid($sponsor->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
            $this->language_model->removeTranslations('sponsor', $id);
			if($this->general_model->remove('sponsor', $id)){
				$this->sponsor_model->removePremium($id);
				$this->session->set_flashdata('event_feedback', __('The sponsor has successfully been deleted'));
				_updateTimeStamp($sponsor->eventid);
				redirect('sponsors/event/'.$sponsor->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('sponsors/event/'.$sponsor->eventid);
			}
		}
	}

	function removeimage($id, $type = 'event') {
		$item = $this->sponsor_model->getSponsorById($id);
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($item->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->image)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->image);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('image' => '');
			$this->general_model->update('sponsor', $id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('sponsors/venue/'.$venue->id);
		} else {
			$event = $this->event_model->getbyid($item->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->image)) redirect('events');
			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->image);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('image' => '');
			$this->general_model->update('sponsor', $id, $data);

			_updateTimeStamp($event->id);
			redirect('sponsors/event/'.$event->id);
		}
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'sponsor');
    }   
}
