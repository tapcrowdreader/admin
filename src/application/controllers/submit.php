<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Submit extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		//if(_authed()) { }
		$this->load->model('app_model');
		$this->load->model('appearance_model');
	}
	
	//php 4 constructor
	function Submit() {
		parent::__construct();
		//if(_authed()) { }
		$this->load->model('app_model');
	}
	
	function index() {
		$this->app(0);
	}
	
	function app($id) {
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');
		
		$this->load->library('form_validation');
		
		if($id == FALSE || $id == 0) {
			$this->session->set_flashdata('tc_error', __('Invalid ApplicationID!'));
			redirect('apps');
		}
		
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');
		if($app->status != 'inactive') {
			$this->session->set_flashdata('event_feedback', __('Application has already been submitted!'));
			// redirect('pay/app/'.$app->id);
		}


		# Validate account permissions
		$account = \Tapcrowd\Model\Session::getInstance()->getCurrentAccount();
		if(\Tapcrowd\Model\Account::getInstance()->hasPermission('app.submit', $account) === false) {
			\Notifications::getInstance()->alert('No Permission to Submit Application', 'error');
			redirect('apps/view/'.$app->id);
		}

		$error = "";
		$nocredits = "";

		$appearance = $this->appearance_model->getControlsForApp($app);
		
		if($this->input->post('postback') == 'postback') {

			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('remarks', 'remarks', 'trim');
			$this->form_validation->set_rules('category[]', 'category', 'trim|required');
			
			$this->form_validation->set_rules('appicon', 'appicon', 'trim');
			$this->form_validation->set_rules('homescreen', 'homescreen', 'trim');
			
			$this->form_validation->set_rules('chk_appstore', 'chk_appstore', 'trim');
			$this->form_validation->set_rules('chk_androidm', 'chk_androidm', 'trim');
			
			$this->form_validation->set_rules('chk_worldwide', 'chk_worldwide', 'trim');
			$this->form_validation->set_rules('chk_europe', 'chk_europe', 'trim');
			$this->form_validation->set_rules('chk_us', 'chk_us', 'trim');
			
			if($this->form_validation->run() == FALSE){
				$error = "Some fields are missing.";
			} else {
				$appicon = '';
				if($_FILES['appicon']['size'] > 0) {
					
					$configicon['upload_path'] = 'upload/appicons/';
					$configicon['allowed_types'] = 'png';
					$configicon['max_width']  = '1024';
					$configicon['max_height']  = '1024';
					$this->load->library('upload', $configicon);
					$this->upload->initialize($configicon);
					
					if ($this->upload->do_upload('appicon')) {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/appicons/' . $returndata['file_name'];

						//icon specific
						$newname = 'upload/appimages/'.$id.'/icon1024.png';
						$config['image_library'] = 'gd2';
						$config['source_image'] = $this->config->item('imagespath') . $image;
						$config['width'] = $control->image_width;
						$config['height'] = $control->image_height;
						$config['maintain_ratio'] = false;
						$config['new_image'] = $this->config->item('imagespath') . $newname;

						$this->image_lib->initialize($config);

						if (!$this->image_lib->resize())
						{
							echo $this->image_lib->display_errors();
						}
						$appicon = $newname;
						$newname = 'upload/appimages/'.$id.'/icon512.png';
						$config['image_library'] = 'gd2';
						$config['source_image'] = $this->config->item('imagespath') . $image;
						$config['width'] = 512;
						$config['height'] = 512;
						$config['maintain_ratio'] = false;
						$config['new_image'] = $this->config->item('imagespath') . $newname;

						$this->image_lib->initialize($config);

						if (!$this->image_lib->resize())
						{
							echo $this->image_lib->display_errors();
						}
						$newname = 'upload/appimages/'.$id.'/icon@2x.png';
						$config['image_library'] = 'gd2';
						$config['source_image'] = $this->config->item('imagespath') . $image;
						$config['width'] = 114;
						$config['height'] = 114;
						$config['maintain_ratio'] = false;
						$config['new_image'] = $this->config->item('imagespath') . $newname;

						$this->image_lib->initialize($config);

						if (!$this->image_lib->resize())
						{
							echo $this->image_lib->display_errors();
						}
						$newname = 'upload/appimages/'.$id.'/icon.png';
						$config['image_library'] = 'gd2';
						$config['source_image'] = $this->config->item('imagespath') . $image;
						$config['width'] = 57;
						$config['height'] = 57;
						$config['maintain_ratio'] = false;
						$config['new_image'] = $this->config->item('imagespath') . $newname;

						$this->image_lib->initialize($config);

						if (!$this->image_lib->resize())
						{
							echo $this->image_lib->display_errors();
						}
					}

				}
				
				$homescreen = '';
				if($_FILES['homescreen']['size'] > 0) {
					
					$configicon['upload_path'] = 'upload/homescreens/';
					$configicon['allowed_types'] = 'png';
					$configicon['max_width']  = '640';
					$configicon['max_height']  = '960';
					$this->load->library('upload', $configicon);
					$this->upload->initialize($configicon);
					
					if (!$this->upload->do_upload('homescreen')) {
						$homescreen = ''; //No image uploaded!
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$homescreen = 'upload/homescreens/' . $returndata['file_name'];
					}
				} else {
					$homescreen = $app->homescreen;
				}
				$categories = $this->input->post('category');
				$categories = implode(';', $categories);

				if($this->input->post('mytagsulselect') != null) {
					$searchterms = implode(';',$this->input->post('mytagsulselect'));
				} else {
					$searchterms = '';
				}

				if(!is_dir($this->config->item('imagespath') . "upload/appscreens/".$app->id)){
					mkdir($this->config->item('imagespath') . "upload/appscreens/".$app->id, 0775,true);
				}
				$configicon['upload_path'] = $this->config->item('imagespath') .'upload/appscreens/'.$app->id;
				$configicon['allowed_types'] = 'JPG|jpg|jpeg|png';
				$this->load->library('upload', $configicon);
				$this->upload->initialize($configicon);
				$i = 1;
				while($i <= 5) {
					if($_FILES['iphone_'.$i]['size'] > 0) {
						if($this->upload->do_upload('iphone_'.$i)) {
							$returndata = $this->upload->data();
							$image = 'upload/appscreens/'.$app->id .'/' . $returndata['file_name'];
							$this->general_model->insert('appscreenshots', array(
									'appid' => $app->id,
									'type' => 'iphone',
									'value' => $image
								)); 
						} else {
							$error .= $this->upload->display_errors();
						}
					}
					if($_FILES['iphone5_'.$i]['size'] > 0) {
						if($this->upload->do_upload('iphone5_'.$i)) {
							$returndata = $this->upload->data();
							$image = 'upload/appscreens/'.$app->id .'/' . $returndata['file_name'];
							$this->general_model->insert('appscreenshots', array(
									'appid' => $app->id,
									'type' => 'iphone5',
									'value' => $image
								)); 
						}
					}
					if($_FILES['android_'.$i]['size'] > 0) {
						if($this->upload->do_upload('android_'.$i)) {
							$returndata = $this->upload->data();
							$image = 'upload/appscreens/'.$app->id .'/' . $returndata['file_name'];
							$this->general_model->insert('appscreenshots', array(
									'appid' => $app->id,
									'type' => 'android',
									'value' => $image
								)); 	
						}
					}

					$i++;
				}

				$this->db->where('id', $id);
				$this->db->update('app', array(
						'submit_description' 	=> set_value('remarks'),
						'category' 				=> $categories,
						'app_icon' 				=> $appicon,
						'homescreen' 			=> $homescreen,
						'searchterms'			=> $searchterms
					));
				$app = $this->app_model->get($id);
				
				// if(_currentUser()->credits > 500) {
					// *** SEND MAIL TO SUBMITTER *** //
					//process app submition 
					$submitdata = array(
							"remarks" 		=> set_value('remarks'),
							"appstore" 		=> set_value('chk_appstore'),
							"androidm" 		=> set_value('chk_androidm'),
							"worldwide"		=> set_value('chk_worldwide'),
							"europeonly"	=> set_value('chk_europe'),
							"usonly"		=> set_value('chk_us'),
							"icon"			=> $appicon,
							"homescreen"	=> $homescreen,
							"searchterms" 	=> $searchterms
						);
					
					$this->load->library('email');
					
					$config['charset'] = 'utf-8';
					$config['wordwrap'] = TRUE;
					$config['mailtype'] = "html";
					
					$this->email->initialize($config);
					
					$this->email->from($this->config->item('no-reply'), 'TapCrowd');
					$this->email->to('apps@tapcrowd.com'); 
					//$this->email->to($this->config->item('webmaster'));
					// $this->email->bcc('jens.verstuyft@tapcrowd.com'); 
					$this->email->subject("TapCrowd App Submit");
					
					$mailcontent = $this->load->view('mails/e_appsubmit', array("app" => $app, "submitdata" => $submitdata), TRUE);
					$this->email->message($this->load->view('mails/e_master', array('title' => __("App Submit"), 'content' => $mailcontent), TRUE));
					
					if($this->email->send()){
						// set status "Pending" for application
						$this->general_model->update('app', $id, array('status' => 'pending'));
						
						$this->session->set_flashdata('event_feedback', __('Your app has been submitted, we will alert you when your app is available!'));
						redirect('pay/app/'.$app->id);
					} else {
						$error = __("Error while submitting, please try again of contact support!"); //$this->email->print_debugger();
					}
					// *** //
				// } else {
				// 	$nocredits = $this->channel->htmlPurchaseCredits;
				// }
			}
		}

		$appscreens = $this->appearance_model->getScreens($app->id);
		
		$cdata['content'] 		= $this->load->view('c_submit', array('app' => $app, 'error' => $error, 'nocredits' => $nocredits, 'appearance' => $appearance, 'appscreens' => $appscreens), TRUE);
		// $cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__("Apps") => "apps", $app->name => "apps/manage/".$app->id);
		$this->load->view('master', $cdata);
	}

}
