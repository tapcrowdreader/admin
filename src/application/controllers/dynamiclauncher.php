<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Dynamiclauncher extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('module_mdl');
	}
	function add($id, $type = 'event') {
		$error = '';
		$this->load->library('form_validation');
		$languages = array();
		$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = 0 LIMIT 1")->row();
		if($type == 'event') {
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');

			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
	                $this->form_validation->set_rules('url_'.$language->key, 'url_'.$language->key, 'trim');
	            }
				$this->form_validation->set_rules('icon', 'icon', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					$url = $this->input->post('url_'.$app->defaultlanguage);
					if(substr($url,0,7) != 'http://' && substr($url,0,8) != 'https://' && $url != '') {
						$url = 'http://'.$url;
					}

					$extraparams = $this->input->post('extraparams');
					if($extraparams == 'on') {
						$extragetparams = 1;
						if(strpos($url, 'appid') === false) {
							$querystring = strpos($url, '?');
							if($querystring === false) {
								$url = $url . '?appid='.$app->id;
							} else {
								$url = $url . '&appid='.$app->id;
							}
						}
					} else {
						$extragetparams = 0;
					}

					if($app->themeid != 0) {
						//use theme icon
						$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = 0 LIMIT 1");
						if($icon->num_rows() != 0) {
							$icon = $icon->row();
							$icon = $icon->icon;
							$image = $this->config->item('imagespath') . $icon;
							$newpath = 'upload/appimages/'.$app->id .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
							if(file_exists($image)) {
								if(copy($image, $this->config->item('imagespath') . $newpath)) {
									$defaultlauncher->icon = $newpath;
								}
							}
						}
					}

					$order = $defaultlauncher->order;
					if($this->input->post('order') != 0) {
						$order = $this->input->post('order');
					}

					$data = array(
						'eventid'	=> $id,
						'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
						'module'	=> 'dynamic',
						'url'		=> $url,
						'order'		=> $order,
						'active'	=> 1,
						'extragetparams' => $extragetparams,
						'icon' => $defaultlauncher->icon
					);

					$launcherid = $this->general_model->insert('launcher', $data);
	                foreach($languages as $language) {
	                	$urllang = checkhttp($this->input->post('url_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $launcherid, 'url', $language->key, $urllang);
	                }

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
						mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0755,true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
					$configexlogo['allowed_types'] = 'png';
					$configexlogo['max_width']  = '140';
					$configexlogo['max_height']  = '140';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('icon')) {
						$icon = ""; //No image uploaded!
						if($_FILES['icon']['name'] != '') {
							$error = $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$icon = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
						$this->general_model->update('launcher', $launcherid, array('icon' => $icon));
					}
					
					_updateTimeStamp($id);
					if($error == '') {
						redirect('event/view/'.$id);
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_dynamicmodule_add', array('event' => $event, 'error' => $error, 'languages'=>$languages), TRUE);
			$cdata['crumb']			= array(__('Events') => 'events', $event->name => 'event/view/'.$event->id, __('Dynamic Module') => 'dynamiclauncher/add/'.$id.'/event');
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, '');
			$this->load->view('master', $cdata);
		} elseif($type == 'venue') {
			$venue = $this->venue_model->getbyid($id);
			if($venue == false) redirect('venues');
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
	                $this->form_validation->set_rules('url_'.$language->key, 'url_'.$language->key, 'trim');
	            }
				$this->form_validation->set_rules('icon', 'icon', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$url = $this->input->post('url_'.$app->defaultlanguage);

					if(substr($url,0,7) != 'http://' && substr($url,0,8) != 'https://') {
						$url = 'http://'.$url;
					}


					$extraparams = $this->input->post('extraparams');
					if($extraparams == 'on') {
						$extragetparams = 1;
						if(strpos($url, 'appid') === false) {
							$querystring = strpos($url, '?');
							if($querystring === false) {
								$url = $url . '?appid='.$app->id;
							} else {
								$url = $url . '&appid='.$app->id;
							}
						}
					} else {
						$extragetparams = 0;
					}

					$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = 0 LIMIT 1")->row();

					if($app->themeid != 0) {
						//use theme icon
						$icon = $this->db->query("SELECT icon FROM themeicon WHERE themeid = $app->themeid AND moduletypeid = 0 LIMIT 1");
						if($icon->num_rows() != 0) {
							$icon = $icon->row();
							$icon = $icon->icon;
							$image = $this->config->item('imagespath') . $icon;
							$newpath = 'upload/appimages/'.$app->id .'/themeicon_'. substr($icon, strrpos($icon , "/") + 1);
							if(file_exists($image)) {
								if(copy($image, $this->config->item('imagespath') . $newpath)) {
									$defaultlauncher->icon = $newpath;
								}
							}
						}
					}

					$order = $defaultlauncher->order;
					if($this->input->post('order') != 0) {
						$order = $this->input->post('order');
					}

					$data = array(
						'venueid'	=> $id,
						'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
						'module'	=> 'dynamic',
						'url'		=> $url,
						'order'		=> $order,
						'active'	=> 1,
						'extragetparams' => $extragetparams,
						'icon'	=> $defaultlauncher->icon
					);

					$launcherid = $this->general_model->insert('launcher', $data);
	                foreach($languages as $language) {
	                	$urllang = checkhttp($this->input->post('url_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $launcherid, 'url', $language->key, $urllang);
	                }

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
						mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0755,true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
					$configexlogo['allowed_types'] = 'png';
					$configexlogo['max_width']  = '140';
					$configexlogo['max_height']  = '140';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('icon')) {
						$icon = ""; //No image uploaded!
						if($_FILES['icon']['name'] != '') {
							$error = $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$icon = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
						$this->general_model->update('launcher', $launcherid, array('icon' => $icon));
					}
					
					_updateVenueTimeStamp($id);
					if($error == '') {
						redirect('venue/view/'.$id);
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_dynamicmodule_add', array('venue' => $venue, 'error' => $error, 'languages'=>$languages, 'defaultlauncher' => $defaultlauncher), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => 'venue/view/'.$venue->id, __('Dynamic Module') => 'dynamiclauncher/add/'.$id.'/venue'));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, '');
			$this->load->view('master', $cdata);
		}
	}

	function edit($id, $type, $typeid) {
		$error = '';
		$this->load->library('form_validation');
		$languages = array();
		if($type == 'event') {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$launcher = $this->module_mdl->getLauncherById($id);

			if($launcher->eventid != $typeid) redirect('events');

			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
	                $this->form_validation->set_rules('url_'.$language->key, 'url_'.$language->key, 'trim');
	            }
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('icon', 'icon', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$url = $this->input->post('url_'.$app->defaultlanguage);
					$extraparams = $this->input->post('extraparams');
					if($extraparams == 'on') {
						$extragetparams = 1;
						if(strpos($url, 'appid') === false) {
							$querystring = strpos($url, '?');
							if($querystring === false) {
								$url = $url . '?appid='.$app->id;
							} else {
								$url = $url . '&appid='.$app->id;
							}
						}
					} else {
						$extragetparams = 0;
					}

					if(substr($url,0,7) != 'http://' && substr($url,0,8) != 'https://') {
						$url = 'http://'.$url;
					}


					$data = array(
						'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
						'order'		=> set_value('order'),
						'url'		=> $url,
						'extragetparams' => $extragetparams
					);

					$this->general_model->update('launcher', $id, $data);
	                foreach($languages as $language) {
	                	$urllang = checkhttp($this->input->post('url_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $id, 'url', $language->key, $urllang);
	                }

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/moduleimages/".$id, 0755,true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$id;
					$configexlogo['allowed_types'] = 'png';
					$configexlogo['max_width']  = '150';
					$configexlogo['max_height']  = '150';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('icon')) {
						$icon = ""; //No image uploaded!
						if($_FILES['icon']['name'] != '') {
							$error = $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$icon = 'upload/moduleimages/'. $id . '/' . $returndata['file_name'];
						if($icon == '') {
							$icon = "l_catalog";
						}
						$this->general_model->update('launcher', $id, array('icon' => $icon));
					}
					_updateTimeStamp($typeid);
					if($error == '') {
						redirect('event/view/'.$typeid);
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_dynamicmodule_edit', array('event' => $event, 'error' => $error, 'launcher' => $launcher, 'languages'=>$languages), TRUE);
			$cdata['crumb']			= array(__('Events') => 'events', $event->name => 'event/view/'.$event->id, $launcher->title => 'dynamiclancher/edit/'.$id.'/event/'.$typeid);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'dynamiclauncher', $launcher->id);
			$this->load->view('master', $cdata);
		} elseif($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			$launcher = $this->module_mdl->getLauncherById($id);

			if($launcher->venueid != $typeid) redirect('venues');

			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
	                $this->form_validation->set_rules('url_'.$language->key, 'url_'.$language->key, 'trim');
	            }
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('icon', 'icon', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					$url = $this->input->post('url_'.$app->defaultlanguage);
					$extraparams = $this->input->post('extraparams');
					if($extraparams == 'on') {
						$extragetparams = 1;
						if(strpos($url, 'appid') === false) {
							$querystring = strpos($url, '?');
							if($querystring === false) {
								$url = $url . '?appid='.$app->id;
							} else {
								$url = $url . '&appid='.$app->id;
							}
						}
					} else {
						$extragetparams = 0;
					}

					if(substr($url,0,7) != 'http://' && substr($url,0,8) != 'https://') {
						$url = 'http://'.$url;
					}

					$data = array(
						'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
						'order'		=> set_value('order'),
						'url'		=> $url,
						'extragetparams' => $extragetparams
					);

					$this->general_model->update('launcher', $id, $data);
	                foreach($languages as $language) {
	                	$urllang = checkhttp($this->input->post('url_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $id, 'url', $language->key, $urllang);
	                }

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/moduleimages/".$id, 0755,true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$id;
					$configexlogo['allowed_types'] = 'png';
					$configexlogo['max_width']  = '150';
					$configexlogo['max_height']  = '150';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('icon')) {
						$icon = ""; //No image uploaded!
						if($_FILES['icon']['name'] != '') {
							$error = $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$icon = 'upload/moduleimages/'. $id . '/' . $returndata['file_name'];
						if($icon == '') {
							$icon = "l_catalog";
						}
						$this->general_model->update('launcher', $id, array('icon' => $icon));
					}
					_updateVenueTimeStamp($typeid);
					if($error == '') {
						redirect('venue/view/'.$typeid);
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_dynamicmodule_edit', array('venue' => $venue, 'error' => $error, 'launcher' => $launcher, 'languages'=>$languages), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => 'venue/view/'.$venue->id, $launcher->title => 'dynamiclancher/edit/'.$id.'/venue/'.$typeid));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'dynamiclauncher', $launcher->id);
			$this->load->view('master', $cdata);
		} elseif($type == 'app') {
			$app = $this->app_model->get($typeid);
			_actionAllowed($app, 'app');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$launcher = $this->module_mdl->getLauncherById($id);

			if($launcher->appid != $typeid) redirect('apps');

			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
	                $this->form_validation->set_rules('url_'.$language->key, 'url_'.$language->key, 'trim');
	            }
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('icon', 'icon', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					$url = $this->input->post('url_'.$app->defaultlanguage);
					$extraparams = $this->input->post('extraparams');
					if($extraparams == 'on') {
						$extragetparams = 1;
						if(strpos($url, 'appid') === false) {
							$querystring = strpos($url, '?');
							if($querystring === false) {
								$url = $url . '?appid='.$app->id;
							} else {
								$url = $url . '&appid='.$app->id;
							}
						}
					} else {
						$extragetparams = 0;
					}

					if(substr($url,0,7) != 'http://' && substr($url,0,8) != 'https://') {
						$url = 'http://'.$url;
					}


					$data = array(
						'title'		=> $this->input->post('title_'.  $app->defaultlanguage),
						'order'		=> set_value('order'),
						'url'		=> $url,
						'extragetparams' => $extragetparams
					);

					$this->general_model->update('launcher', $id, $data);
	                foreach($languages as $language) {
	                	$urllang = checkhttp($this->input->post('url_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
	                    $this->language_model->addTranslation('launcher', $id, 'url', $language->key, $urllang);
	                }

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/moduleimages/".$id, 0755,true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$id;
					$configexlogo['allowed_types'] = 'png';
					$configexlogo['max_width']  = '150';
					$configexlogo['max_height']  = '150';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('icon')) {
						$icon = ""; //No image uploaded!
						if($_FILES['icon']['name'] != '') {
							$error = $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$icon = 'upload/moduleimages/'. $id . '/' . $returndata['file_name'];
						if($icon == '') {
							$icon = "l_catalog";
						}
						$this->general_model->update('launcher', $id, array('icon' => $icon));
					}
					$this->general_model->update('app', $typeid, array('timestamp' => time()));
					if($error == '') {
						redirect('apps/view/'.$typeid);
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_dynamicmodule_edit', array('app' => $app, 'error' => $error, 'launcher' => $launcher, 'languages'=>$languages), TRUE);
			$cdata['crumb']			= array(__('Apps') => 'apps', $app->name => 'apps/view/'.$app->id, $launcher->title => 'dynamiclancher/edit/'.$id.'/app/'.$typeid);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function delete($id,$typeid,$type) {
		if($type == 'event') {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
			$launcher = $this->module_mdl->getLauncherById($id);
			if($launcher->eventid == $typeid) {
				$this->general_model->remove('launcher', $id);
			}
			_updateTimeStamp($typeid);
			redirect('event/view/'.$typeid);
		} elseif($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$launcher = $this->module_mdl->getLauncherById($id);
			if($launcher->venueid == $typeid) {
				$this->general_model->remove('launcher', $id);
			}
			_updateVenueTimeStamp($typeid);
			redirect('venue/view/'.$typeid);
		} elseif($type == 'app') {
			$app = $this->app_model->get($typeid);
			_actionAllowed($app, 'app','');
			$launcher = $this->module_mdl->getLauncherById($id);
			if($launcher->appid == $typeid) {
				$this->general_model->remove('launcher', $id);
			}
			$this->general_model->update('app', $app->id, array('timestamp' => time()));
			redirect('apps/view/'.$typeid);
		}
	}

	function activate($typeid,$type,$id) {
		if($type == 'event') {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
			$data = array(
				'active'	=> 1
			);

			$this->general_model->update('launcher', $id, $data);
			_updateTimeStamp($typeid);
			redirect('event/view/'.$typeid);
		}elseif($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$data = array(
				'active'	=> 1
			);

			$this->general_model->update('launcher', $id, $data);
			_updateVenueTimeStamp($typeid);
			redirect('venue/view/'.$typeid);
		}
	}

	function deactivate($typeid,$type,$id) {
		if($type == 'event') {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
			$data = array(
				'active'	=> 0
			);

			$this->general_model->update('launcher', $id, $data);
			_updateTimeStamp($typeid);
			redirect('event/view/'.$typeid);
		} elseif($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$data = array(
				'active'	=> 0
			);
			_updateVenueTimeStamp($typeid);
			$this->general_model->update('launcher', $id, $data);
			redirect('venue/view/'.$typeid);
		}
	}

	function removeimage($id, $type = 'event', $typeid) {
		if($type == 'venue') {
			$item = $this->module_mdl->getLauncherById($id);
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->icon)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->icon);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('icon' => '');
			$this->general_model->update('launcher', $item->id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('venue/view/'.$venue->id);
		} elseif($type == 'event') {
			$item = $this->module_mdl->getLauncherById($id);
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->icon)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->icon);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('icon' => '');
			$this->general_model->update('launcher', $item->id, $data);

			_updateTimeStamp($event->id);
			redirect('event/view/'.$event->id);
		} elseif($type == 'app') {
			$item = $this->module_mdl->getLauncherById($id);

			if(!file_exists($this->config->item('imagespath') . $item->icon)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->icon);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('icon' => '');
			$this->general_model->update('launcher', $item->id, $data);

			redirect('app/view/'.$typeid);
		}
	}
}