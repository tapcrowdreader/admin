<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Duplicate extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}
	
	function index($id, $object, $type = 'event', $groupobject = '') {
		if($object == 'sessions') {
			$object = 'session'; 
		}
		if($object == 'session') {
			$this->load->model('sessions_model');
			$session = $this->sessions_model->getSessionByID($id);
			unset($session->venueid);
			unset($session->sessiongroupname);
			$this->duplicate('session', $session, $type, 'sessions', '', $id);
		} elseif($object == 'news') {
			$this->load->model('news_model');
			$news = $this->news_model->getNewsById($id);
			$this->duplicate('newsitem', $news, $type, 'news', '', $id);
		} elseif($object == 'exhibitors') {
			$this->load->model('exhibitor_model');
			$ex = $this->exhibitor_model->getExhibitorByID($id);
			unset($ex->brands);
			unset($ex->categories);
			$this->duplicate('exhibitor', $ex, $type, 'exhibitors', $groupobject, $id);
		} elseif($object == 'sponsors') {
			$this->load->model('sponsor_model');
			$sponsor = $this->sponsor_model->getSponsorById($id);
			$this->duplicate('sponsor', $sponsor, $type, 'sponsors', '', $id);
		} elseif($object == 'attendees') {
			$this->load->model('attendee_model');
			$att = $this->attendee_model->getAttendeeByID($id);
			$this->duplicate('attendees', $att, $type, 'attendees', '', $id);
		} elseif($object == 'ad' || $object == 'ads') {
			$this->load->model('ad_model');
			$ad = $this->ad_model->getAdById($id);
			$this->duplicate('ad', $ad, $type, 'ad', '', $id);
		} elseif($object == 'groups') {
			$this->load->model('group_model');
			$group = $this->group_model->getById($id);
			$this->duplicate('group', $group, $type, 'groups', $groupobject, $id);
		} elseif($object == 'artists') {
			$this->load->model('artist_model');
			$artist = $this->artist_model->getArtistByID($id);
			$this->duplicate('artist', $artist, $type, 'artists', '', $id);
		} elseif($object == 'catalog') {
			$this->load->model('catalog_model');
			$cat = $this->catalog_model->getCatalogByID($id);
			unset($cat->brands);
			unset($cat->categories);
			$this->duplicate('catalog', $cat, $type, 'catalog', $groupobject, $id);
		} elseif($object == 'schedule') {
			$this->load->model('schedule_model');
			$s = $this->schedule_model->getById($id);
			$this->duplicate('schedule', $s, $type, 'schedule', '', $id);
		} elseif($object == 'speaker') {
			$this->load->model('speaker_model');
			$s = $this->speaker_model->getById($id);
			$this->duplicate('speaker', $s, $type, 'speakers', '', $id);
		} elseif($object == 'sessiongroups') {
			$this->load->model('sessions_model');
			$s = $this->sessions_model->getSessiongroupByID($id);
			$this->duplicate('sessiongroup', $s, $type, 'sessiongroups', '', $id);
		} elseif($object == 'festivalinfo') {
			$this->load->model('metadata_model');
			$s = $this->metadata_model->getById($id);
			$this->duplicate('metadata', $s, $type, 'festivalinfo', '', $id);
		} elseif($object == 'formfields') {
			$this->load->model('forms_model');
			$s = $this->forms_model->getFormFieldByID($id);
			$this->duplicate('formfield', $s, $type, 'formfield', '', $id);
		}
	}

	private function duplicate($table, $object, $type, $controller, $groupobject = '', $oldid = '') {
		$object->id = '';
		if($insertedId = $this->general_model->insert($table, (array)$object)) {
			//metadata has no tags
			if($table != 'metadata' && $table != 'group' && $table != 'groupitem' && $table != 'schedule' && $table != 'formfield' && $table != 'speaker' && $table != 'attendees' && $table != 'ad') {
				$this->load->model('tag_model');
				$tagids = $this->tag_model->getTagIdsOfObject($table, $oldid);
				foreach($tagids as $tagid) {
					$tag = $this->tag_model->getById($tagid);
					$tag->id = '';
					$tag->{$table.'id'} = $insertedId;
					$this->general_model->insert('tag', (array) $tag);
				}
			}
			$this->session->set_flashdata('event_feedback', __('The object has been duplicated!'));				
		} else {
			$this->session->set_flashdata('event_feedback', __('There was an error when duplicating.'));
		}

		//group redirects
		if($table == 'group' && $type == 'event') {
			redirect('groups/view/'.$object->parentid.'/event/'.$object->eventid.'/'.$groupobject);
		} elseif($table == 'group' && $type == 'venue') {
			redirect('groups/view/'.$object->parentid.'/venue/'.$object->venueid.'/'.$groupobject);
		}

		//catalog
		if($table == 'catalog' && is_numeric($groupobject)) {
			$this->load->model('groupitem_model');
			$groupitem = $this->groupitem_model->getByItemIdAndTypeAndGroup($oldid, 'catalog', $groupobject);
			$groupitem->id = '';
			$groupitem->itemid = $insertedId;
			$this->general_model->insert('groupitem', (array) $groupitem);

			redirect('groups/view/'.$groupobject.'/venue/'.$object->venueid.'/catalog');
		} elseif($table == 'catalog') {
			redirect('catalog/venue/'.$object->venueid.'/0/'.$object->type);
		}

		//exhibitor
		if($table == 'exhibitor' && is_numeric($groupobject)) {
			$this->load->model('groupitem_model');
			$groupitem = $this->groupitem_model->getByItemIdAndTypeAndGroup($oldid, 'exhibitor', $groupobject);
			$groupitem->id = '';
			$groupitem->itemid = $insertedId;
			$this->general_model->insert('groupitem', (array) $groupitem);

			redirect('groups/view/'.$groupobject.'/event/'.$object->eventid.'/exhibitor');
		} elseif($table == 'catalog') {
			redirect('exhibitors/event/'.$object->eventid);
		}

		if($table == 'metadata') {
			$object->eventid = $object->identifier;
		}

		//formfields
		if($table == 'formfield') {
			redirect('formfields/formscreen/'.$object->formscreenid);
		}

		if($type == 'event') {
			redirect($controller. '/'.$type.'/'.$object->eventid);
		} elseif($type == 'venue') {
			redirect($controller. '/'.$type.'/'.$object->venueid);
		} elseif($type == 'app') {
			redirect($controller. '/'.$type.'/'.$object->appid);
		}
	}
}