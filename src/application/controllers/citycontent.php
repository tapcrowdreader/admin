<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Citycontent extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('citycontent_model');
	}

	//php 4 constructor
	function Citycontent() {
		parent::__construct();
		if(_authed()) { }
	}

	function index() {
		$this->app(_currentApp()->id);
	}

	function app($id, $tagParam = '') {
		$this->session->set_userdata('eventid', '');
		$this->session->set_userdata('venueid', '');
        $this->session->set_userdata('artistid', '');
		if($id == FALSE || $id == 0) redirect('apps');

		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		if($app->familyid == 3) {
			if($tagParam != '') {
				$tagParam2 = str_replace(' ', '___', $tagParam);
				$this->iframeurl = "citycontent/tag/".$app->id."/".$tagParam2;
			} else {
				$this->iframeurl = "citycontent/app/".$app->id;
			}
		}

        $citycontent = $this->citycontent_model->allFromApp($id);

        if($tagParam != '') {
        	$filtered = array();
        	foreach($citycontent as $cc) {
				// TAGS
				$tagNames = array();
				$tags = $this->db->query("SELECT * FROM tag WHERE citycontentid = $cc->id AND tag = '$tagParam'");
				if($tags->num_rows() == 0) {

				} else {
					$filtered[] = $cc;
				}

        	}
        	$citycontent = $filtered;
        }

		$headers = array(__('Title') => 'title');

		$cdata['content'] 		= $this->load->view('c_listview', array('app' => $app, 'data' => $citycontent, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$cdata['crumb']			= array(__("Content") => "citycontent");
		$this->load->view('master', $cdata);
	}

	function add() {
		$internal_error = FALSE;
		$this->load->library('form_validation');
		$error = "";
		$app = _currentApp();
		_actionAllowed($app, 'app');


		if($app->familyid == 3) {
			if(isset($tagParam) && $tagParam != '') {
				$this->iframeurl = "citycontent/tag/".$app->id."/".$tagParam;
			} else {
				$this->iframeurl = "citycontent/app/".$app->id;
			}
		}

		$languages = $this->language_model->getLanguagesOfApp($app->id);

		if($this->input->post('postback') == 'postback') {
            $this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title', 'trim|required');
                $this->form_validation->set_rules('text_'.$language->key, 'text', 'trim');
            }

			if($this->form_validation->run() == FALSE ){
				$error = validation_errors();
			} else {
                $data = array(
                        "appid"     => $app->id,
						"title"		=> set_value('title_'.  $app->defaultlanguage),
						"text"		=> set_value('text_'.  $app->defaultlanguage)
                    );

                if($newid = $this->general_model->insert('citycontent', $data)){
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/citycontentimages/".$newid)){
						mkdir($this->config->item('imagespath') . "upload/citycontentimages/".$newid, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/citycontentimages/'.$newid;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = ""; //No image uploaded!
						$error = $this->upload->display_errors();
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/citycontentimages/'. $newid . '/' . $returndata['file_name'];
					}

					$this->general_model->update('citycontent', $newid, array('image' => $image));

				   //add translated fields to translation table
					foreach($languages as $language) {
						//description
						$this->language_model->addTranslation('citycontent', $newid, 'title', $language->key, $this->input->post('title_'.$language->key));
						$this->language_model->addTranslation('citycontent', $newid, 'text', $language->key, $this->input->post('text_'.$language->key));
					}

					redirect('citycontent/app/'.$app->id);
                }
			}
		}

        $cdata['content'] 		= $this->load->view('c_citycontent_add', array('error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
        $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
        $cdata['crumb']			= array(__("Content") => "citycontent", __("New") => "citycontent/add/");
        $this->load->view('master', $cdata);

	}
	function image_rules($str){
		$filename = 'image';
		return image_check($filename, $_FILES);
	}

	function edit($id) {
		$appid = _currentApp()->id;
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');
		$languages = $this->language_model->getLanguagesOfApp($app->id);
        $this->load->library('form_validation');
        $error = "";

        if($app->familyid == 3) {
			$this->iframeurl = "citycontent/view/" . $id;
        }

        // EDIT SESSION
        $citycontent = $this->citycontent_model->getById($id);
        if($citycontent == FALSE) redirect('citycontent');
		if($citycontent->appid != $app->id) redirect('apps');

		// TAGS
		$tags = $this->db->query("SELECT tag FROM tag WHERE citycontentid = $id");
		if($tags->num_rows() == 0) {
			$tags = array();
		} else {
			$tagz = array();
			foreach ($tags->result() as $tag) {
				$tagz[] = $tag->tag;
			}
			$tags = $tagz;
		}


        if($this->input->post('postback') == "postback") {
            $this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title', 'trim|required');
                $this->form_validation->set_rules('text_'.$language->key, 'text', 'trim');
            }

			if($this->form_validation->run() == FALSE){
				$error .= validation_errors();
			} else {
				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/citycontentimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/citycontentimages/".$id, 0755, true);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/citycontentimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('image')) {
					$image = $citycontent->image; //No image uploaded!
					$error = $this->upload->display_errors();
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/citycontentimages/'. $id . '/' . $returndata['file_name'];
				}

				$data = array(
						"appid"             => $app->id,
						"title"				=> set_value('title_'. $app->defaultlanguage),
						"text"				=> set_value('text_'. $app->defaultlanguage),
						"image"				=> $image
					);

				if($this->general_model->update('citycontent',$id, $data)){
					//add translated fields to translation table
					foreach($languages as $language) {
						$titleSuccess = $this->language_model->updateTranslation('citycontent', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
						if(!$titleSuccess) {
							$this->language_model->addTranslation('citycontent', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
						}
						$descrSuccess = $this->language_model->updateTranslation('citycontent', $id, 'text', $language->key, $this->input->post('text_'.$language->key));
						if(!$descrSuccess) {
							$this->language_model->addTranslation('citycontent', $id, 'text', $language->key, $this->input->post('text_'.$language->key));
						}
					}

					$this->session->set_flashdata('citycontent_feedback', __('The content has successfully been updated'));
					$this->general_model->update('app', $app->id, array('timestamp' => time()));
					redirect('citycontent/app/'.$app->id);
				} else {
					$error = "Oops, something went wrong. Please try again.";
				}
			}
        }

        $cdata['content'] 		= $this->load->view('c_citycontent_edit', array('citycontent' => $citycontent,'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
        $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('citycontent' => $citycontent), TRUE);
        $cdata['crumb']			= array(__("Content") => "citycontent", __("Edit: ") . $citycontent->title => "citycontent/edit/".$citycontent->id);
        $this->load->view('master', $cdata);
	}

	function delete($id, $type = '') {

        $citycontent = $this->citycontent_model->getById($id);
        $appid = $citycontent->appid;
        if($citycontent == FALSE) redirect('citycontent');
		if($citycontent->appid != $appid) redirect('apps');
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');
        if($this->general_model->remove('citycontent', $id)){
        	$this->general_model->update('app', $appid, array('timestamp' => time()));
            $this->session->set_flashdata('citycontent_feedback', __('The citycontent has successfully been deleted'));
            redirect('citycontent/app/'. $appid);
        } else {
            $this->session->set_flashdata('citycontent_error', __('Oops, something went wrong. Please try again.'));
            redirect('citycontent/app/'. $appid);
        }
	}

	function deleteimage($image) {
		// unlink file itself
		unlink($image);
		// unlink cache-files
		$info = pathinfo($image);
		$file_name =  basename($imageurl,'.'.$info['extension']);
		array_map("unlink", glob("../cache/*".$file_name));
	}

	function addtag() {
		$citycontentid = $this->input->post('citycontentid');
		$tag = $this->input->post('tag');

		$citycontent = $this->citycontent_model->getbyid($citycontentid);
		$appid = $citycontent->appid;
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');

		$data = array(
			'appid'		=> $appid,
			'citycontentid'	=> $citycontentid,
			'tag'		=> $tag,
			'active'	=> 1
		);
		$this->general_model->insert('tag', $data);

		//launcheritem
		$launcherexists = $this->db->query("SELECT * FROM launcher WHERE appid = $appid AND moduletypeid = 32 AND title = '$tag'");
		$launcherdata = array(
				'appid'=>$app->id,
				'moduletypeid'=>32,
				'module'=>'citycontentTags',
				'title'=>$tag,
				'tag'=>$tag,
				'icon'=>'l_venues',
				'active'=>1
			);

		if($launcherexists->num_rows() == 0) {
			$this->general_model->insert('launcher', $launcherdata);
		}

		// if($launcherexists->num_rows() == 0) {
		// 	$this->general_model->insert('launcher', $launcherdata);
		// } else {
		// 	$launcherexists = $launcherexists->row();
		// 	$this->general_model->update('launcher',$launcherexists->id, $launcherdata);
		// }

		$this->general_model->update('app', $appid, array('timestamp' => time()));

		redirect('apps/view/'.$appid);
	}
}