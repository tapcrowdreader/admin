<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Favourites extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('favourites_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Favourite',
			'plural' => 'Favourites',
			'parentType' => 'event',
			'browse_url' => 'favourites/--parentType--/--parentId--',
			'edit_url' => 'favourites/edit/--id--',
			'module_url' => 'module/editByController/favourites/--parentType--/--parentId--/0',
			'headers' => array(
				__('Email') => 'useremail', 
				__('Session') => 'sname'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'favourites/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'favourites/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			)
		);
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$favourites = $this->favourites_model->getFavouritesByEvent($id);
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $favourites), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Favourites") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function edit($id) {
		$this->load->library('form_validation');
		$error = "";
		// EDIT FAVOURITE
		$favourite = $this->favourites_model->getFavouriteById($id);
		if($favourite == FALSE) redirect('events');

		$event = $this->event_model->getbyid($favourite->eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->load->model('exhibitor_model');
		$exhibitors = $this->exhibitor_model->getExhibitorsByEventID($event->id);

		$this->load->model('sessions_model');
		$sessions = $this->sessions_model->getSessionPerGroupByEventID($event->id);

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('email', 'email', 'trim|required');
			$this->form_validation->set_rules('sel_session', 'sel_session', 'trim');
			$this->form_validation->set_rules('sel_exhibitor', 'sel_exhibitor', 'trim');

			if($this->form_validation->run() == FALSE){
				$error = __("Some fields are missing.");
			} else {
				$data = array(
						'useremail'		=> set_value('email'),
						'eventid'		=> $event->id,
						'exhibitorid'	=> set_value('sel_exhibitor'),
						'sessionid'		=> set_value('sel_session')
					);

				if($this->general_model->update('favorites', $id, $data)){
					$this->session->set_flashdata('event_feedback', __('The favourite has successfully been updated'));
					_updateTimeStamp($event->id);
					redirect('favourites/event/'.$event->id);
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_favourite_edit', array('event' => $event, 'favourite' => $favourite, 'exhibitors' => $exhibitors, 'sessions' => $sessions, 'error' => $error), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Favourites") => "favourites/event/".$event->id, __("Edit: " ). $favourite->useremail => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$this->load->view('master', $cdata);
	}

	function delete($id) {
		$favourite = $this->general_model->one('favorites', $id);
		$event = $this->event_model->getbyid($favourite->eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		if($this->general_model->remove('favorites', $id)){
			$this->session->set_flashdata('event_feedback', __('The favourite has successfully been deleted'));
			_updateTimeStamp($favourite->eventid);
			redirect('favourites/event/'.$favourite->eventid);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('favourites/event/'.$favourite->eventid);
		}
	}

	function notify($id) {
		$this->load->library('form_validation');
		$error = '';

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		// Notify users with mail
		$stats = $this->favourites_model->getEventFavouriteStats($id);
		$usersfavs = $this->favourites_model->getUsersFavourites($id);

		if($this->input->post('postback') == 'postback') {
			$this->form_validation->set_rules('title', 'title', 'trim|required');
			$this->form_validation->set_rules('text', 'text', 'trim|required');

			if($this->form_validation->run() == FALSE){
				$error = __("Some fields are missing.");
			} else {
				$log = "-- START SEND -- <br />";

				$favorites = array();
				$prev = FALSE;
				$prev->useremail = '';
				$count = 1;
				$sendmail = FALSE;

				// Send mail
				$this->load->library('email');

				$config['charset'] = 'utf-8';
				//$config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				$this->email->initialize($config);

				foreach ($usersfavs as $user) {
					if($user->useremail != $prev->useremail) {
						$sendmail = TRUE;
						array_push($favorites, $user);
					} else {
						if($count == count($usersfavs)) $sendmail = TRUE;
						array_push($favorites, $user);
					}

					if($sendmail){
						$subject = set_value('title'); //'My Conference bag';
						$mailcontent = $this->load->view('mails/e_conferencebag', array('title' => $subject, 'message' => set_value('text'), 'favorites' => $favorites), TRUE);
						$message = $this->load->view('mails/e_master', array('logo' => $event->eventlogo, 'title' => $subject, 'content' => $mailcontent), TRUE);

						// SEND
						$this->load->library('email');
						$this->email->from($this->config->item('no-reply'), 'TapCrowd');
						//$this->email->from('eva@stichtingmarketing.be', 'Stichting Marketing');
						$this->email->to($user->useremail);
						//$this->email->to('matthijs@floatleft.be');
						$this->email->bcc('log@floatleft.be');
						$this->email->subject($subject);
						$this->email->message($message);
						// Set Alternate message
						$this->email->set_alt_message(__('This is the alternative message'));

						foreach ($favorites as $fav) {
							$this->db->where('id', $fav->id);
							$this->db->update('favorites', array('notified' => date('Y-m-d H:i:s')));
						}

						$log .= __(" - send : ") . $user->useremail . "<br />";
						$this->email->send();
						$favorites = array();
						$sendmail = FALSE;
					}

					// REinit PREVIOUS
					$prev = $user;
					$count++;
				}
				$log .= __("<br /> count : ") . $count;
				$this->session->set_flashdata('event_feedback', __('The conferencebag notification has successfully been sent!'));
				redirect('favourites/event/'.$event->id);
				//echo $log;
				//die();
			}
		}

		$cdata['content'] 		= $this->load->view('c_notifyfavourites', array('event' => $event, 'stats' => $stats, 'usersfavs' => $usersfavs, 'error' => $error), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, __("Favourites") => 'favourites/event/'.$event->id, __("Notify") => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

//	function notifyextra($id) {
//		$this->load->library('form_validation');
//		$error = '';
//
//		$event = $this->event_model->getbyid($id);
//		if($event == FALSE) redirect('events');
//		if($event->organizerid != _currentUser()->organizerId) redirect('events');
//
//		// Notify users with mail
//		$stats = $this->favourites_model->getEventFavouriteStats($id);
//		$usersfavs = $this->favourites_model->getUsersFavourites($id);
//
//		if($this->input->post('postback') == 'postback') {
//			$this->form_validation->set_rules('title', 'title', 'trim|required');
//			$this->form_validation->set_rules('text', 'text', 'trim|required');
//
//			if($this->form_validation->run() == FALSE){
//				$error = "Some fields are missing.";
//			} else {
//				$log = "-- START SEND -- <br />";
//
//				$favorites = array();
//				$prev = FALSE;
//				$prev->useremail = 'b.wylin@televic-education.com';
//				$count = 1;
//				$sendmail = FALSE;
//
//				// Send mail
//				$this->load->library('email');
//
//				$config['charset'] = 'utf-8';
//				//$config['wordwrap'] = TRUE;
//				$config['mailtype'] = "html";
//				$this->email->initialize($config);
//
//				$emails = array('b.wylin@televic-education.com', 'bart.hendrickx@questionmark.eu', 'bart@vanorshoven.be', 'hanne.vankerckhoven@teamprosource.eu', 'info@systo.be', 'iris@prana.be', 'jan.cumps@ideasatwork.be', 'jo@white-rabbit.be', 'johan@vierkant.be', 'karen.leclair@gmail.com', 'karl.meysmans@gmail.com', 'Kathleen.HEIREMAN@Blcc.be', 'Kristin.adriaensen@blcc.be', 'matvugt@hotmail.com', 'mdk@teamprosource.be', 'Peter.van.beeck@scotwork.com', 'Philippe.lorrez@globalservices.be', 'studio@cprojects.be', 'w.valbracht@synaps.be', '');
//				foreach ($emails as $email) {
//					//for testing
////					if($prev->useremail == 'brunob@but.be') {
//						if($email != $prev->useremail) {
//							$sendmail = TRUE;
//						} else {
//
//						}
//
//						$favsextra = $this->favourites_model->getUsersFavouritesExtra($id, $prev->useremail);
//
//						//inserts
//						$inserts = $this->db->query("SELECT * FROM confbag WHERE eventid = $id ORDER BY `order`");
//						$inserts = $inserts->result();
//
//
//						if($sendmail){
//							$subject = set_value('title'); //'My Conference bag';
//							$mailcontent = $this->load->view('mails/e_conferencebagextra', array('title' => $subject, 'message' => set_value('text'), 'favorites' => $favorites, 'inserts' => $inserts, 'favsExtra' => $favsextra), TRUE);
//							$message = $this->load->view('mails/e_masterextra', array('logo' => $event->eventlogo, 'title' => $subject, 'content' => $mailcontent), TRUE);
//							//$message = $message . '<br/>'.$prev->useremail;
//							// SEND
//							$this->load->library('email');
//							$this->email->from($this->config->item('no-reply'), 'TapCrowd');
//							//$this->email->from('eva@stichtingmarketing.be', 'Stichting Marketing');
//							$this->email->to($prev->useremail);
//							//$this->email->to('jens_verstuyft@hotmail.com');
//							//$this->email->to('matthijs@floatleft.be');
//							$this->email->bcc('jens_verstuyft@hotmail.com');
//							$this->email->subject($subject);
//							$this->email->message($message);
//							// Set Alternate message
////							$this->email->set_alt_message('This is the alternative message');
//
//							//removed notified for testing
////							foreach ($favorites as $fav) {
////								$this->db->where('id', $fav->id);
////								$this->db->update('favorites', array('notified' => date('Y-m-d H:i:s')));
////							}
//
//							$log .= " - send : " . $prev->useremail . "<br />";
//							$this->email->send();
//							$favorites = array();
//							$sendmail = FALSE;
//						}
////					}
//					// REinit PREVIOUS
//					$prev->useremail = $email;
//					$count++;
//					}
//				$log .= "<br /> count : " . $count;
//				$this->session->set_flashdata('event_feedback', 'The conferencebag notification has successfully been sent!');
//				redirect('favourites/event/'.$event->id);
//				//echo $log;
//				//die();
//			}
//		}
//
//		$cdata['content'] 		= $this->load->view('c_notifyfavourites', array('event' => $event, 'stats' => $stats, 'usersfavs' => $usersfavs, 'error' => $error), TRUE);
//		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
//		$cdata['crumb']			= array("Events" => "events", $event->name => "event/view/".$id, "Favorites" => 'favourites/event/'.$event->id, "Notify" => $this->uri->uri_string());
//		$this->load->view('master', $cdata);
//	}
//
//	function notifyone($id) {
//		$this->load->library('form_validation');
//		$error = '';
//
//		$event = $this->event_model->getbyid($id);
//		if($event == FALSE) redirect('events');
//		if($event->organizerid != _currentUser()->organizerId) redirect('events');
//
//		// Notify users with mail
//		$stats = $this->favourites_model->getEventFavouriteStats($id);
//		$usersfavs = $this->favourites_model->getUsersFavourites($id);
//
//		if($this->input->post('postback') == 'postback') {
//			$this->form_validation->set_rules('title', 'title', 'trim|required');
//			$this->form_validation->set_rules('text', 'text', 'trim|required');
//
//			if($this->form_validation->run() == FALSE){
//				$error = "Some fields are missing.";
//			} else {
//				$log = "-- START SEND -- <br />";
//
//				$favorites = array();
//				$prev = FALSE;
//				$prev->useremail = 'brunob@but.be';
//				$count = 1;
//				$sendmail = true;
//
//				// Send mail
//				$this->load->library('email');
//
//				$config['charset'] = 'utf-8';
//				//$config['wordwrap'] = TRUE;
//				$config['mailtype'] = "html";
//				$this->email->initialize($config);
//
//
//						$favsextra = $this->favourites_model->getUsersFavouritesExtra($id, $prev->useremail);
//
//						//inserts
//						$inserts = $this->db->query("SELECT * FROM confbag WHERE eventid = $id ORDER BY `order`");
//						$inserts = $inserts->result();
//
//
//						if($sendmail){
//							$subject = set_value('title'); //'My Conference bag';
//							$mailcontent = $this->load->view('mails/e_conferencebagextra', array('title' => $subject, 'message' => set_value('text'), 'favorites' => $favorites, 'inserts' => $inserts, 'favsExtra' => $favsextra), TRUE);
//							$message = $this->load->view('mails/e_masterextra', array('logo' => $event->eventlogo, 'title' => $subject, 'content' => $mailcontent), TRUE);
//
//							// SEND
//							$this->load->library('email');
//							$this->email->from($this->config->item('no-reply'), 'TapCrowd');
//							//$this->email->from('eva@stichtingmarketing.be', 'Stichting Marketing');
//							//$this->email->to($prev->useremail);
//							$this->email->to('bruno.bogaerts@telenet.be');
//							//$this->email->to('matthijs@floatleft.be');
//							$this->email->bcc('jens@mobilejuice.be');
//							$this->email->subject($subject);
//							$this->email->message($message);
//							// Set Alternate message
////							$this->email->set_alt_message('This is the alternative message');
//
//							//removed notified for testing
////							foreach ($favorites as $fav) {
////								$this->db->where('id', $fav->id);
////								$this->db->update('favorites', array('notified' => date('Y-m-d H:i:s')));
////							}
//
//							$log .= " - send : " . $prev->useremail . "<br />";
//							$this->email->send();
//							$favorites = array();
//							$sendmail = FALSE;
//						}
//				$log .= "<br /> count : " . $count;
//				$this->session->set_flashdata('event_feedback', 'The conferencebag notification has successfully been sent!');
//				redirect('favourites/event/'.$event->id);
//				//echo $log;
//				//die();
//			}
//		}
//
//		$cdata['content'] 		= $this->load->view('c_notifyfavourites', array('event' => $event, 'stats' => $stats, 'usersfavs' => $usersfavs, 'error' => $error), TRUE);
//		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
//		$cdata['crumb']			= array("Events" => "events", $event->name => "event/view/".$id, "Favorites" => 'favourites/event/'.$event->id, "Notify" => $this->uri->uri_string());
//		$this->load->view('master', $cdata);
//	}


}