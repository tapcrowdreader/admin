<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Askaquestion extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }

		$this->load->model('askaquestion_model');
		$this->load->model('forms_model');
		$this->load->model('speaker_model');
		$this->load->model('sessions_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Question',
			'plural' => 'Questions',
			'title' => __('Questions'),
			'parentType' => 'event',
			'browse_url' => 'askaquestion/--parentType--/--parentId--',
			'add_url' => 'askaquestion/add/--parentId--/--parentType--',
			'edit_url' => 'askaquestion/edit/--id--',
			'module_url' => 'module/edit/61/--parentType--/--parentId--',
			'headers' => array(
				__('Show on screen') => 'show',
				__('Question') => 'question',
				__('From Name') => 'fromname',
				__('From Company') => 'fromcompany',
				__('Addressed to') => 'addressedto'
			),
			'views' => array(
				'list' => 'tc_questions',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'checkboxes' => false,
			'deletecheckedurl' => 'askaquestion/removemany/--parentId--/--parentType--'
		);
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = "event/index/" . $id;

		$launcher = $this->module_mdl->getLauncher(61, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$form = $this->forms_model->getFormByLauncherID($launcher->id);

		# Module edit url
		$this->_module_settings->module_url = 'forms/editlauncher/'.$form->launcherid;
		$this->_module_settings->form = $form;

		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		//questions page
		$btnResults = (object)array(
						'title' => __('Result page settings'),
						'href' => site_url('askaquestion/pagelayout/event/'.$id),
						'icon_class' => 'icon-wrench',
						'btn_class' => 'btn',
						'target' => '_self'
						);
		$this->_module_settings->extrabuttons['results'] = $btnResults;

		
		$questions = $this->askaquestion_model->getQuestionsOfEvent($event->id, $launcher->id);

		if(isset($_GET['sessionid']) && !empty($_GET['sessionid'])) {
			$sessionspeakers = $this->speaker_model->getBySessionId($_GET['sessionid']);
			$sessionspeakerids = array();
			foreach($sessionspeakers as $ss) {
				$sessionspeakerids[] = $ss->id;
			}
			
			$questions2 = array();
			foreach($questions as $k => $v) {
				$v['showForSession'] = false;
				foreach($v as $k2 => $question) {
					if(in_array($question->speakerid, $sessionspeakerids)) {
						$v['showForSession'] = true;
					}
				}

				if($v['showForSession']) {
					 $questions2[$k] = $v;
				}
			}
			$questions = $questions2;
		}

		$i = count($questions);
		foreach($questions as $q) {
				$q[0]->num = $i;
				$i--;
		}

		$formopts = explode(',', $form->opts);
		if(in_array('moderation', $formopts)) {
			$headers = array('Num' => 'num', __('Show on screen') => 'show');
		} else {
			$headers = array('Num' => 'num');
		}
		
		if(!empty($questions)) {
			foreach($questions as $q) {
				foreach($q as $object) {
					if($object->formfieldtypeid != 10) {
						if(!empty($object->label)) {
							$headers[$object->label] = $object->label;
						}
					}
				}
				break;
			}
		}

		if(!empty($headers)) {
			$headers[__('Interesting')] = 'interesting';
			$headers[__('Answered')] = 'answered';
			$this->_module_settings->headers = $headers;
		}

		$sessions = $this->sessions_model->getSessionByEventID($event->id);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($questions), 'formopts' => $formopts, 'sessions' => $sessions, 'event' => $event), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => 'forms/event/'.$event->id.'/'.$launcher->id, __('Questions') => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $this->_module_settings->title => 'forms/event/'.$event->id.'/'.$launcher->id, __('Questions') => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'askaquestion');
		$this->load->view('master', $cdata);
	}

    function add($id, $type = 'event') {
		$this->load->library('form_validation');
		$error = "";

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = "event/index/" . $id;

		$launcher = $this->module_mdl->getLauncher(61, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$form = $this->askaquestion_model->getQuestionFormOfEvent($event->id, $launcher->id);
		$screens = $this->forms_model->getFormScreensByFormID($form->id);
		$fields = $this->forms_model->getFormFieldsByFormScreenID($screens[0]->id);
		$speakers = $this->speaker_model->getByEventId($event->id);

		if($this->input->post('postback') == "postback") {
			foreach($fields as $field) {
				if($field->required == 1) {
					$this->form_validation->set_rules($field->label, $field->label, 'trim|required');
				} else {
					$this->form_validation->set_rules($field->label, $field->label, 'trim');
				}
			}

			if($this->form_validation->run() == FALSE){
				$error .= validation_errors();
			} else {
				$formopts = explode(',', $form->opts);
				$visible = 0;
				if(!in_array('moderation', $formopts)) {
					$visible = 1;
				}
				$formsubmissionid = $this->general_model->insert('formsubmission', array('formid' => $form->id, 'visible' => $visible));
				foreach($fields as $field) {
					if($field->formfieldtypeid != 10) {
						$this->general_model->insert('formsubmissionfield', array(
							'formsubmissionid' => $formsubmissionid,
							'formfieldid' => $field->id,
							'value' => $this->input->post(str_replace(' ', '', $field->label))
							));
					}
				}
				$this->session->set_flashdata('event_feedback', __('The question has successfully been added!'));
				_updateTimeStamp($event->id);
				if($error == '') {
					redirect('askaquestion/event/'.$event->id);
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_question_add', array('event' => $event, 'error' => $error, 'app' => $app, 'fields' => $fields, 'speakers' => $speakers), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => 'forms/event/'.$event->id.'/'.$launcher->id, __('Questions') => "askaquestion/event/".$event->id, __("Add new") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$event->id, $this->_module_settings->title => 'forms/event/'.$event->id.'/'.$launcher->id, __('Questions') => "askaquestion/event/".$event->id, __("Add new") => $this->uri->uri_string());
		}
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'askaquestion');
		$this->load->view('master', $cdata);
	}

	function delete($id, $type) {
		$ad = $this->ad_model->getAdById($id);
		$event = $this->event_model->getbyid($ad->eventid);
		$app = _actionAllowed($event, 'event','returnApp');
		if($this->general_model->remove('ad', $id)){
			$this->session->set_flashdata('event_feedback', __('The ad has successfully been deleted'));
			_updateTimeStamp($ad->eventid);
			redirect('ad/event/'.$ad->eventid);
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
			redirect('ad/event/'.$ad->eventid);
		}
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->askaquestion_model->removeMany($selectedids);
    }   

    function show($formsubmissionid, $onoff) {
    	$formsubmission = $this->forms_model->getFormSubmissionById($formsubmissionid);
    	$form = $this->forms_model->getFormByID($formsubmission->formid);
    	$event = $this->event_model->getbyid($form->eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		if($onoff == 'show') {
			$this->forms_model->showSubmission($formsubmissionid);
		} else {
			$this->forms_model->hideSubmission($formsubmissionid);
		}
    }

    function pagelayout($type, $typeId) {
    	if($type == 'event') {
    		$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');

			$pagelayout = $this->askaquestion_model->getPageLayout($app->id, 'askaquestion');

			$launcher = $this->module_mdl->getLauncher(61, 'event', $event->id);
			$form = $this->forms_model->getFormByLauncherID($launcher->id);

			if($this->input->post('postback') == "postback") {
				$this->load->library('form_validation');
				$this->form_validation->set_rules('bodyHtml', 'body html', 'trim');
				$this->form_validation->set_rules('headerHtml', 'header html', 'trim');
				$this->form_validation->set_rules('footerHtml', 'footer html', 'trim');
				$this->form_validation->set_rules('css', 'css', 'trim');
				$data = array(
					'appid' => $app->id,
					'purpose' => 'askaquestion',
					'body' => $this->input->post('bodyHtml'),
					'header' => $this->input->post('headerHtml'),
					'footer' => $this->input->post('footerHtml'),
					'css' => $this->input->post('css')
					);
				$id = $this->general_model->insert_unique('customhtml', $data, array('appid', 'purpose'), array($app->id, 'askaquestion'));

				redirect('event/view/'.$event->id);
			}

			$cdata['content'] 		= $this->load->view('c_question_pagelayout', array('event' => $event, 'error' => $error, 'app' => $app, 'data' => $pagelayout, 'form' => $form), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => 'forms/event/'.$event->id.'/'.$launcher->id, __('Questions') => "askaquestion/event/".$event->id, __("Page layout") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, $this->_module_settings->title => 'forms/event/'.$event->id.'/'.$launcher->id, __('Questions') => "askaquestion/event/".$event->id, __("Page layout") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'askaquestion');
			$this->load->view('master', $cdata);
    	}
    }

    function opts($formsubmissionid, $val, $key) {
		$formsubmission = $this->forms_model->getFormSubmissionById($formsubmissionid);
    	$form = $this->forms_model->getFormByID($formsubmission->formid);
    	$event = $this->event_model->getbyid($form->eventid);
		$app = _actionAllowed($event, 'event','returnApp');

		$opts = explode(',', $formsubmission->opts);
		if($val == 1 && !in_array($key, $opts)) {
			$opts[] = $key;
		} elseif($val == 0 && in_array($key, $opts)) {
			$opts = array_diff($opts, array($key));
		}

		$opts = array_diff($opts, array(''));
		$opts = implode(',', $opts);
		$this->general_model->update('formsubmission', (int)$formsubmissionid, array('opts' => $opts));
    }
}
