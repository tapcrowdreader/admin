<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Analytics extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('analytics_model');
	}

	function index() {
		$this->app();
	}

	function app($id, $filterDate = '') {
		$this->session->set_userdata('appid', $id);
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		// $analytics = $this->analytics_model->getAnalytics($id, $filterDate);

		// $filterDateArray = array();
		// $filterDateArray = $this->analytics_model->getMonths($id);

		// $cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'analytics' => $analytics, 'filterDate' => $filterDate, 'filterDateArray' => $filterDateArray), TRUE);
		$cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'controller' => 'dashboard'), TRUE);
		// $cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		// $cdata['crumb']			= array('Analytics' => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function activate($id) {
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$config['mailtype'] 	= 'html';
		$config['charset']		= 'utf-8';
		//Set Config!
		$this->load->library('email');
		$this->email->initialize($config);
		//prepare email
		$to = 'apps@tapcrowd.com'; //TO Address
		$subject = "Analytics Request"; //Subject of the message
		$from = 'info@tapcrowd.com';

		$message = 'Analytics were requested for app "'.$app->name.'" with id .'.$app->id;

		$this->email->from($from, 'TapCrowd');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();
		$cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'pending' => true), TRUE);
		$this->load->view('master', $cdata);
	}

	function usage($id, $filterDate = '') {
		$this->session->set_userdata('appid', $id);
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'controller' => 'stats'), TRUE);
		$this->load->view('master', $cdata);
	}

	function content($id, $filterDate = '') {
		$this->session->set_userdata('appid', $id);
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'controller' => 'actions'), TRUE);
		$this->load->view('master', $cdata);
	}

	function livemap($id, $filterDate = '') {
		$this->session->set_userdata('appid', $id);
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'controller' => 'livemap'), TRUE);
		$this->load->view('master', $cdata);
	}

	function replay($id, $filterDate = '') {
		$this->session->set_userdata('appid', $id);
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'controller' => 'replay'), TRUE);
		$this->load->view('master', $cdata);
	}

	function realtime($id, $filterDate = '') {
		$this->session->set_userdata('appid', $id);
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'controller' => 'realtime/segments'), TRUE);
		$this->load->view('master', $cdata);
	}

	function logs($id, $filterDate = '') {
		$this->session->set_userdata('appid', $id);
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'controller' => 'realtime/raw'), TRUE);
		$this->load->view('master', $cdata);
	}

	function report($id, $filterDate = '') {
		$this->session->set_userdata('appid', $id);
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');

		$cdata['content'] 		= $this->load->view('c_analytics', array('app' => $app, 'controller' => 'report'), TRUE);
		$this->load->view('master', $cdata);
	}

}
