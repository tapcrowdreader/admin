<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Qrlist extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	function names($eventid) {
		$this->load->model('exhibitor_model');
		$this->load->model('event_model');
		$this->load->model('app_model');
		$event = $this->event_model->getbyid($eventid);
		$exhibitors = $this->exhibitor_model->getExhibitorsByEventID($eventid);
		
		$appid = $this->app_model->getRealAppOfEvent($eventid);
		$app = $this->app_model->get($appid->appid);
		$cdata['content'] 		= $this->load->view('c_qrlist', array('items' => $exhibitors, 'event' => $event,'app' => $app), TRUE);
		$cdata['crumb']			= array();

		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}
	
	function codes($eventid) {
		
	}
}