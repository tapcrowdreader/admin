<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Basket extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		$this->load->model("basket_model");
	}

	/**
	 * @param $id, venue id
	 * @return Creates a view for the basket menu
	 */	
	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$modulecode = (object) array('name' => "code", 'title' => "Code of the Day", "status" => $this->basket_model->getModuleStatus($id, "code"));
		$moduletable = (object) array('name' => "table", 'title' => "Table Selection Options", "status" => $this->basket_model->getModuleStatus($id, "table"));

		$modules = array($modulecode, $moduletable);

		if($this->input->post('postback') == "postback") {
			$this->basket_model->optionschange($this->input->post('orderattable'), $this->input->post('ordertakeaway'), $this->input->post('orderreservation'), $id, $this->input->post('popupperorder'), $this->input->post('popupperitem'));
			$this->basket_model->apichange($this->input->post('api'), $this->input->post('externalvenueid'), $id);
			$this->session->set_flashdata('event_feedback', __('The basket information has been updated.'));
			redirect('venue/view/'.$venue->id);
		}

		$basket = $this->basket_model->getBasket($id);
		
		$cdata['content'] 		= $this->load->view('c_basket', array('venue' => $venue, 'modules' => $modules, 'basket' => $basket), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Basket") => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'basket');
		$this->load->view('master', $cdata);
		
	}
	
	/**
	 * @param $venueid, the Venue Id on the database
	 * @return Creates a view for the user to change the code of the day.
	 */
	function code($venueid)
	{
		$venue = $this->venue_model->getbyid($venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');
		
		$code = $this->basket_model->getCode($venueid)->code;
				
		$cdata['content'] 		= $this->load->view('c_basket_code', array('venue' => $venue, 'code' => $code), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app,
			 						array($venue->name => "venue/view/".$venueid,
			 						__("Basket") => "basket/venue/$venueid",
			 						__("Code of the Day") => $this->uri->uri_string())
									);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'basket');
		$this->load->view('master', $cdata);
	}
	
	/**
	 * @param $venueid, the Venue Id of the current venue
	 * @param $active, the status of the module
	 * @return Changes the status of the option from active to inactive and from inactive to active and changes the view accordingly
	 */
	function activate($venueid, $module, $status){
		//if the module is inactive put it active, if active put it inactive.
		if($status == 0)
		{
			$this->basket_model->activateModule($venueid, $module);
			
		}
		else if ($status == 1)
		{
			$this->basket_model->deactivateModule($venueid, $module);
		}

		$this->venue($venueid);
	}
	
	function table($venueid)
	{
		$venue = $this->venue_model->getbyid($venueid);
		$id = $this->basket_model->getBasketId($venueid);
		$table_options = $this->basket_model->getTableOptions($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$keys = array_keys($table_options);
		$options = array();
		
		foreach($keys as $key)
		{
			if($key != "id")
				$options[] = array($key, $table_options[$key]);
		}
		
		$cdata['content'] 		= $this->load->view('c_table_opts', array('venue' => $venue, 'options' => $options), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venueid, __("Basket") => "basket/venue/".$venueid,__("Table Options") => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'basket');
		$this->load->view('master', $cdata);
	}
	
	function table_option($venueid, $type, $status)
	{
		$id = $this->basket_model->getBasketId($venueid);
		$this->basket_model->setTableOption($id, $type, $status);
		
		$this->table($venueid);
	}
	
	/**
	 * @return Sets the new Code of the day from a post method in the code view.
	 */
	function setcode()
	{
		if(isset($_POST['venueid']))
		{
			$venueid = $_POST['venueid'];
			$code = $_POST['code'];
			
			$res = $this->basket_model->setCode($venueid, $code);
		}
		echo $res;
	}
	
	/**
	 * @param $id, the venue id
	 * @param $code, the code of the day
	 * @return creates a new window with the QR Code on it ready to be printed
	 */
	function printcode($id, $code)
	{
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->iframeurl = 'basket/venue/'.$id;
		
		$this->load->view('c_print_qrcode', array('app' => $app, 'venueid' => $id, 'code' => $code));
	}
	
	
}