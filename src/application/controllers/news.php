<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class News extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('news_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Newsitem',
			'plural' => 'News',
			'title' => __('News'),
			'parentType' => 'event',
			'browse_url' => 'news/--parentType--/--parentId--',
			'add_url' => 'news/add/--parentId--/--parentType--',
			'edit_url' => 'news/edit/--id--',
			'module_url' => 'module/editByController/news/--parentType--/--parentId--/0',
			'import_url' => '',
			'rss_url' => 'news/rsslist/--parentId--/--parentType--/',
			'headers' => array(
				__('Title') => 'title',
				__('Date') => 'datum',
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'news/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'news/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false,
			'deletecheckedurl' => 'news/removemany/--parentId--/--parentType--'
		);
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = 'news/event/'.$id;

		$news = $this->news_model->getNewsByEventID($id);

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# Module rss url
		$rss_url =& $this->_module_settings->rss_url;
		$rss_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $rss_url);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		$launcher = $this->module_mdl->getLauncher(1, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = ucfirst($launcher->title);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($news)), true);

		// $cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'data' => $news, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']			= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'news');
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		$venue = $this->venue_model->getbyid($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->iframeurl = 'news/venue/'.$id;

		$news = $this->news_model->getNewsByVenueID($id);

		$this->_module_settings->parentType = 'venue';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'venue', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'venue', $delete_href);
		# Module rss url
		$rss_url =& $this->_module_settings->rss_url;
		$rss_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $rss_url);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $deletecheckedurl);

		$launcher = $this->module_mdl->getLauncher(1, 'venue', $venue->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = ucfirst($launcher->title);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($news)), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, $this->_module_settings->title => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'news');
		$this->load->view('master', $cdata);
	}

	function app($id, $tagid = '') {
		if($id == FALSE || $id == 0) redirect('apps');
		$app = $this->app_model->get($id);
		_actionAllowed($app, 'app');
		$this->iframeurl = 'news/app/'.$id;

		if($app->apptypeid == 9) {
			$this->iframeurl = 'contentmodule/app/'.$app->id;
		}

		if($tagid == '') {
			$news = $this->news_model->getNewsByAppId($id);
		} else {
			$news = $this->news_model->getNewsByTagId($tagid);
		}

		//top block for content flavor
		$contenflavor = false;
		$newstags = array();
		if($app->apptypeid == 9) {
			$contentflavor = true;
			$newstags = $this->news_model->getTagsOfNewsByApp($app->id);
		}
		$flavor = $this->app_model->getFlavorOfId($app->apptypeid);

		$this->_module_settings->parentType = 'app';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('app', $id), $module_url);
		if($app->apptypeid == 9) {
			$module_url = '';
		}
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('app', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'app', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'app', $delete_href);
		# Module rss url
		$rss_url =& $this->_module_settings->rss_url;
		$rss_url = str_replace(array('--parentType--','--parentId--'), array('app', $id), $rss_url);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('app', $id), $deletecheckedurl);

		$launcher = $this->module_mdl->getLauncher(1, 'app', $app->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = ucfirst($launcher->title);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $news), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$cdata['crumb']			= array($this->_module_settings->title => $this->uri->uri_string());
		$this->load->view('master', $cdata);
	}

	function add($id, $type = 'event') {
		if($type == 'venue'){
			$this->load->library('form_validation');
			$error = "";
            $imageError = "";
            $venue = $this->venue_model->getById($id);
            $app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'news/venue/'.$id;

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'newsitem' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			$launcher = $this->module_mdl->getLauncher(1, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = ucfirst($launcher->title);
			$metadata = $this->metadata_model->getMetadata($launcher, '', '', $app);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
                if($languages != null) {
                foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('text_'.$language->key, 'text ('.$language->name.')', 'trim|required');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
                }

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {
					$data = array(
							"title" 	=> set_value('title_'.$app->defaultlanguage),
							"venueid" 	=> $venue->id,
							"txt" 		=> set_value('text_'.$app->defaultlanguage),
                            "datum"     => date("Y-m-d H:i:s",time())
						);

					if(($newsid = $this->general_model->insert('newsitem', $data)) && $imageError == ''){
						//push notification
						if($this->input->post('push') == 'yes') {
							if($this->input->post('pushtext') != '') {
								$data = array(
										'appid'			=> $app->id,
										'message'		=> $this->input->post('pushtext'),
										'certificate'	=> '',
										'type'          => 'text'
									);
								$res = $this->general_model->insert('pushcue', $data);
							}

							$ch = curl_init("http://clients.tapcrowd.com/push/ios/push2.php");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_exec($ch);
							curl_close($ch);

							//android
							$ch = curl_init("http://clients.tapcrowd.com/push/android/sendpush.php");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_exec($ch);
							curl_close($ch);
						}

						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$newsid)){
							mkdir($this->config->item('imagespath') . "upload/newsimages/".$newsid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$newsid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = ""; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/newsimages/'. $newsid . '/' . $returndata['file_name'];
						}

						$this->image_lib->clear();

						$config = array();

						// create thumb
						$config['image_library'] = 'GD2';
						$config['source_image'] = $uploaddata['full_path'];
						$config['new_image'] = $this->config->item('imagespath') .'upload/newsimages/'.$newsid;
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = true;
						$config['width'] = "";
						$config['height'] = "";

						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						//$this->image_lib->display_errors();

						$this->general_model->update('newsitem', $newsid, array('image' => $image));

                        //add translated fields to translation table
                        if($languages != null) {
	                        foreach($languages as $language) {
	                            $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, $this->input->post('title_'.$language->key));
	                            $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $this->input->post('text_'.$language->key));
								foreach($metadata as $m) {
									if(_checkMultilang($m, $language->key, $app)) {
										$postfield = _getPostedField($m, $_POST, $_FILES, $language);
										if($postfield !== false) {
											if(_validateInputField($m, $postfield)) {
												_saveInputField($m, $postfield, 'newsitem', $newsid, $app, $language);
											}
										}
									}
								}
	                        }
                        }


						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$this->general_model->insert('tc_tag', array(
								'appid' => $app->id,
								'itemtype' => 'newsitem',
								'itemid' => $newsid,
								'tag' => $tag
							));
						}


						$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been added!'));
						_updateVenueTimeStamp($venue->id);
						if($error == '') {
							redirect('news/venue/'.$venue->id);
						}else {
							//image error
							redirect('news/edit/'.$newsid.'/venue?error=image');
						}
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

                    if($imageError != '') {
                        $error .= '<br />' .$imageError;
                    }
				}
			}

			$cdata['content'] 		= $this->load->view('c_news_add', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app, 'apptags' => $apptags, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "news/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'news');
			$this->load->view('master', $cdata);
		} elseif($type == 'app'){
			$this->load->library('form_validation');
			$error = "";
            $imageError = "";
            $appid = $id;
			$app = $this->app_model->get($id);
			_actionAllowed($app, 'app','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'newsitem' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			$launcher = $this->module_mdl->getLauncher(1, 'app', $app->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = ucfirst($launcher->title);
			$metadata = $this->metadata_model->getMetadata($launcher, '', '', $app);

			//for contentmodule more fields are available
			if($app->apptypeid == 9) {
				$this->iframeurl = 'contentmodule/app/'.$app->id;

				if($this->input->post('postback') == "postback") {
					$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
	                if($languages != null) {
	                foreach($languages as $language) {
	                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
	                    $this->form_validation->set_rules('txt_'.$language->key, 'text ('.$language->name.')', 'trim|required');
						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								foreach(_setRules($m, $language) as $rule) {
									$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
								}
							}
						}
	                }
	                }
	                $this->form_validation->set_rules('order', 'order', 'trim');
	                $this->form_validation->set_rules('tags', 'tags', 'trim');

					if($this->form_validation->run() == FALSE){
						$error = validation_errors();
					} else {
						$url = $this->input->post('url');

						// get host name from URL
						preg_match('@^(?:http://)?([^/]+)@i',
						    $url, $matches);
						$host = $matches[1];

						// get last two segments of host name
						preg_match('/[^.]+\.[^.]+$/', $host, $matches);
						$hostUrl = $matches[0];

						// Check if its a youtube URL for a video
						if($hostUrl=="youtube.com"){
							$field = "videourl";
						}else{
							$field = "url";
						}

						if(!strstr($url,'http://') && trim($url) != '' && !strstr($url,'https://')) {
							$url = 'http://'.$url;
						}
						$data = array(
								"title" 	=> $this->input->post('title_'.$app->defaultlanguage),
								"appid" 	=> $id,
								"txt" 		=> $this->input->post('txt_'.$app->defaultlanguage),
	                            "datum"     => date("Y-m-d H:i:s",time()),
	                            "order"		=> set_value('order'),
	                            "".$field.""	=> $url
							);

						if(($newsid = $this->general_model->insert('newsitem', $data)) && $imageError == ''){
							//Uploads Images
							if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$newsid)){
								mkdir($this->config->item('imagespath') . "upload/newsimages/".$newsid, 0755, true);
							}

							$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$newsid;
							$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
							$configexlogo['max_width']  = '2000';
							$configexlogo['max_height']  = '2000';
							$this->load->library('upload', $configexlogo);
							$this->upload->initialize($configexlogo);
							if (!$this->upload->do_upload('image')) {
								$image = ""; //No image uploaded!
								if($_FILES['image']['name'] != '') {
									$error .= $this->upload->display_errors();
								}
							} else {
								//successfully uploaded
								$returndata = $this->upload->data();
								$image = 'upload/newsimages/'. $newsid . '/' . $returndata['file_name'];
							}

							$this->general_model->update('newsitem', $newsid, array('image' => $image));

	                        //add translated fields to translation table
	                        if($languages != null) {
	                        foreach($languages as $language) {
	                            $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, $this->input->post('title_'.$language->key));
	                            $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $this->input->post('txt_'.$language->key));
								foreach($metadata as $m) {
									if(_checkMultilang($m, $language->key, $app)) {
										$postfield = _getPostedField($m, $_POST, $_FILES, $language);
										if($postfield !== false) {
											if(_validateInputField($m, $postfield)) {
												_saveInputField($m, $postfield, 'newsitem', $newsid, $app, $language);
											}
										}
									}
								}
	                        }
	                        }

							// *** SAVE TAGS *** //
							$tags = $this->input->post('mytagsulselect');
							foreach ($tags as $tag) {
								$tags = array(
										'appid' 	=> $app->id,
										'tag' 		=> $tag,
										'newsitemid' => $newsid
									);
								$this->general_model->insert('tag', $tags);
							}
							if($this->input->post('tagfield') != null && trim($this->input->post('tagfield')) != '') {
								$tags = array(
										'appid' 	=> $app->id,
										'tag' 		=> $this->input->post('tagfield'),
										'newsitemid' => $newsid
									);
								$this->general_model->insert('tag', $tags);
							}

							$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been added!'));
							$this->general_model->update('app', $app->id, array('timestamp' => time()));
							if($error == '') {
								redirect('news/app/'.$app->id);
							}else {
								//image error
								redirect('news/edit/'.$newsid.'/app?error=image');
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}

	                    if($imageError != '') {
	                        $error .= '<br />' .$imageError;
	                    }
					}
				}

				$cdata['content'] 		= $this->load->view('c_news_add_content', array('app' => $app, 'error' => $error, 'languages' => $languages, 'apptags' => $apptags, 'metadata' => $metadata), TRUE);
				$cdata['crumb']			= array($this->_module_settings->title => "news/app/".$app->id, __("Add new") => $this->uri->uri_string());
				$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
				$this->load->view('master', $cdata);
			} else {
				$this->iframeurl = 'news/app/'.$id;

				// TAGS
				$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'newsitem' GROUP BY tag");
				if($apptags->num_rows() == 0) {
					$apptags = array();
				} else {
					$apptags = $apptags->result();
				}
				//validation
				if($this->input->post('mytagsulselect') != null) {
					$postedtags = $this->input->post('mytagsulselect');
				} else {
					$postedtags = array();
				}

				if($this->input->post('postback') == "postback") {
					$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
	                if($languages != null) {
	                foreach($languages as $language) {
	                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
	                    $this->form_validation->set_rules('text_'.$language->key, 'text ('.$language->name.')', 'trim|required');
						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								foreach(_setRules($m, $language) as $rule) {
									$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
								}
							}
						}
	                }
	                }

					if($this->form_validation->run() == FALSE){
						$error = validation_errors();
					} else {
						$data = array(
								"title" 	=> set_value('title_'.$app->defaultlanguage),
								"appid" 	=> $id,
								"txt" 		=> set_value('text_'.$app->defaultlanguage),
	                            "datum"     => date("Y-m-d H:i:s",time())
							);

						if(($newsid = $this->general_model->insert('newsitem', $data)) && $imageError == ''){
							//push notification
							if($this->input->post('push') == 'yes') {
								if($this->input->post('pushtext') != '') {
									$data = array(
											'appid'			=> $app->id,
											'message'		=> $this->input->post('pushtext'),
											'certificate'	=> '',
											'type'          => 'text'
										);
									$res = $this->general_model->insert('pushcue', $data);
								}

								$ch = curl_init("http://clients.tapcrowd.com/push/ios/push2.php");
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								curl_exec($ch);
								curl_close($ch);

								//android
								$ch = curl_init("http://clients.tapcrowd.com/push/android/sendpush.php");
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								curl_exec($ch);
								curl_close($ch);
							}

							//Uploads Images
							if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$newsid)){
								mkdir($this->config->item('imagespath') . "upload/newsimages/".$newsid, 0755, true);
							}

							$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$newsid;
							$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
							$configexlogo['max_width']  = '2000';
							$configexlogo['max_height']  = '2000';
							$this->load->library('upload', $configexlogo);
							$this->upload->initialize($configexlogo);
							if (!$this->upload->do_upload('image')) {
								$image = ""; //No image uploaded!
								if($_FILES['image']['name'] != '') {
									$error .= $this->upload->display_errors();
								}
							} else {
								//successfully uploaded
								$returndata = $this->upload->data();
								$image = 'upload/newsimages/'. $newsid . '/' . $returndata['file_name'];
							}

							$this->general_model->update('newsitem', $newsid, array('image' => $image));

							$tags = $this->input->post('mytagsulselect');
							foreach ($tags as $tag) {
								$this->general_model->insert('tc_tag', array(
									'appid' => $app->id,
									'itemtype' => 'newsitem',
									'itemid' => $newsid,
									'tag' => $tag
								));
							}

	                        //add translated fields to translation table
	                        if($languages != null) {
	                        foreach($languages as $language) {
	                            $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, $this->input->post('title_'.$language->key));
	                            $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $this->input->post('text_'.$language->key));
								foreach($metadata as $m) {
									if(_checkMultilang($m, $language->key, $app)) {
										$postfield = _getPostedField($m, $_POST, $_FILES, $language);
										if($postfield !== false) {
											if(_validateInputField($m, $postfield)) {
												_saveInputField($m, $postfield, 'newsitem', $newsid, $app, $language);
											}
										}
									}
								}
	                        }
	                        }

							$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been added!'));
							$this->general_model->update('app', $app->id, array('timestamp' => time()));
							if($error == '') {
								redirect('news/app/'.$app->id);
							}else {
								//image error
								redirect('news/edit/'.$newsid.'/app?error=image');
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}

	                    if($imageError != '') {
	                        $error .= '<br />' .$imageError;
	                    }
					}
				}

				$cdata['content'] 		= $this->load->view('c_news_add', array('app' => $app, 'error' => $error, 'languages' => $languages, 'apptags' => $apptags, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);
				$cdata['crumb']			= array($this->_module_settings->title => "news/app/".$app->id, __("Add new") => $this->uri->uri_string());
				$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
				$this->load->view('master', $cdata);
			}
		} else {
			$this->load->library('form_validation');
			$error = "";
            $imageError = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'news/event/'.$id;

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'newsitem' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			$launcher = $this->module_mdl->getLauncher(1, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = ucfirst($launcher->title);
			$metadata = $this->metadata_model->getMetadata($launcher, '', '', $app);

			if($this->input->post('postback') == "postback") {
                foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('text_'.$language->key, 'text ('.$language->name.')', 'trim|required');
                }
				$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {

					$data = array(
							"title" 	=> set_value('title_'.$app->defaultlanguage),
							"eventid" 	=> $event->id,
							"txt" 		=> set_value('text_'.$app->defaultlanguage),
                            "datum"     => date("Y-m-d H:i:s",time())
						);

					if(($newsid = $this->general_model->insert('newsitem', $data)) && $imageError == ''){
                        //push notification
						if($this->input->post('push') == 'yes') {
							if($this->input->post('pushtext') != '') {
								$data = array(
										'appid'			=> $app->id,
										'message'		=> $this->input->post('pushtext'),
										'certificate'	=> '',
										'type'          => 'text'
									);
								$res = $this->general_model->insert('pushcue', $data);
							}

							$ch = curl_init("http://clients.tapcrowd.com/push/ios/push2.php");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_exec($ch);
							curl_close($ch);

							//android
							$ch = curl_init("http://clients.tapcrowd.com/push/android/sendpush.php");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_exec($ch);
							curl_close($ch);
						}
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$newsid)){
							mkdir($this->config->item('imagespath') . "upload/newsimages/".$newsid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$newsid;
						$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
						$configexlogo['max_width']  = '2000';
						$configexlogo['max_height']  = '2000';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = ""; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error .= $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/newsimages/'. $newsid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('newsitem', $newsid, array('image' => $image));

                        //add translated fields to translation table
                        foreach($languages as $language) {
                            $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, $this->input->post('title_'.$language->key));
                            $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $this->input->post('text_'.$language->key));
							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
								$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'newsitem', $newsid, $app, $language);
										}
									}
								}
							}
                        }

						$tags = $this->input->post('mytagsulselect');
							foreach ($tags as $tag) {
								$this->general_model->insert('tc_tag', array(
									'appid' => $app->id,
									'itemtype' => 'newsitem',
									'itemid' => $newsid,
									'tag' => $tag
								));
							}

						$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been added!'));
						_updateTimeStamp($event->id);
						if($error == '') {
							redirect('news/event/'.$event->id);
						} else {
							//image error
							redirect('news/edit/'.$newsid.'/event?error=image');
						}

					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

                    if($imageError != '') {
                        $error .= '<br />' .$imageError;
                    }
				}
			}

			$cdata['content'] 		= $this->load->view('c_news_add', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'apptags' => $apptags, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "news/event/".$event->id, __("Add new") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "news/event/".$event->id, __("Add new") => $this->uri->uri_string());
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'news');
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function image_rules($str){
		$filename = 'image';
		return image_check($filename, $_FILES);
	}

	function edit($id, $type) {
		$tags = $this->db->query("SELECT * FROM tag WHERE newsitemid = $id");
		if($tags->num_rows() == 0) {
			$tags = array();
		} else {
			$tagz = array();
			foreach ($tags->result() as $tag) {
				$tagz[] = $tag;
			}
			$tags = $tagz;
		}
		if($type == 'venue'){
			$this->load->library('form_validation');
			$error = "";
            $imageError = "";
			// EDIT NEWSITEM
			$newsitem = $this->news_model->getNewsById($id);
			if($newsitem == FALSE) redirect('venues');
			$venue = $this->venue_model->getById($newsitem->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'news/view/'.$id;

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'newsitem' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'newsitem'");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag;
				}
				$tags = $tagz;
			}

			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			$launcher = $this->module_mdl->getLauncher(1, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = ucfirst($launcher->title);
			$metadata = $this->metadata_model->getMetadata($launcher, 'newsitem', $id, $app);

			if($this->input->post('postback') == "postback") {
                foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('text_'.$language->key, 'text ('.$language->name.')', 'trim|required');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
				$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');

				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/newsimages/".$id, 0755, true);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('image')) {
					$image = $newsitem->image; //No image uploaded!
					if($_FILES['image']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/newsimages/'. $id . '/' . $returndata['file_name'];
				}

				if($this->form_validation->run() == FALSE || $error != ''){
					$error .= validation_errors();
				} else {
					$this->db->where('itemtype', 'newsitem');
					$this->db->where('itemid', $id);
					$this->db->delete('tc_tag');
					$tags = $this->input->post('mytagsulselect');
					foreach ($tags as $tag) {
						$this->general_model->insert('tc_tag', array(
							'appid' => $app->id,
							'itemtype' => 'newsitem',
							'itemid' => $id,
							'tag' => $tag
						));
					}

					$data = array(
							"title" => set_value('title_'. $app->defaultlanguage),
							"txt" 	=> set_value('text_'. $app->defaultlanguage),
							"image"	=> $image,
                            "datum" => date("Y-m-d H:i:s",time())
						);

					if($this->general_model->update('newsitem', $id, $data) && $imageError == ''){
                        //add translated fields to translation table
                        foreach($languages as $language) {
							$nameSuccess = $this->language_model->updateTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
							if(!$nameSuccess) {
								$this->language_model->addTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
							}
							$descrSuccess = $this->language_model->updateTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('text_'.$language->key));
							if(!$descrSuccess) {
								$this->language_model->addTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('text_'.$language->key));
							}
							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
									$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'newsitem', $id, $app, $language);
										}
									}
								}
							}
                        }
						$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						redirect('news/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

                    if($imageError != '') {
                        $error .= '<br />' .$imageError;
                    }
				}
			}

			$cdata['content'] 		= $this->load->view('c_news_edit', array('venue' => $venue, 'newsitem' => $newsitem, 'error' => $error, 'languages' => $languages, 'app' => $app, 'apptags' => $apptags, 'metadata' => $metadata, 'postedtags' => $postedtags, 'tags' => $tags), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "news/venue/".$venue->id, __("Edit: ") . $newsitem->title => $this->uri->uri_string()));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'news');
			$this->load->view('master', $cdata);
		} elseif($type == 'app'){
			$this->load->library('form_validation');
			$error = "";
            $imageError = "";
			// EDIT NEWSITEM
			$newsitem = $this->news_model->getNewsById($id);
			if($newsitem == FALSE) redirect('apps');
			$appid = $newsitem->appid;
			$app = $this->app_model->get($appid);
			_actionAllowed($app, 'app');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'newsitem' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			$launcher = $this->module_mdl->getLauncher(1, 'app', $app->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = ucfirst($launcher->title);
			$metadata = $this->metadata_model->getMetadata($launcher, 'newsitem', $id, $app);

			if($app->apptypeid == 9) {
				// TAGS
				$tags = $this->db->query("SELECT * FROM tag WHERE newsitemid = $id");
				if($tags->num_rows() == 0) {
					$tags = array();
				} else {
					$tagz = array();
					foreach ($tags->result() as $tag) {
						$tagz[] = $tag;
					}
					$tags = $tagz;
				}
				$this->iframeurl = 'news/view/'.$id;

				if($this->input->post('postback') == "postback") {
	                foreach($languages as $language) {
	                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
	                    $this->form_validation->set_rules('txt_'.$language->key, 'text ('.$language->name.')', 'trim|required');
						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								foreach(_setRules($m, $language) as $rule) {
									$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
								}
							}
						}
	                }
					$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
	                $this->form_validation->set_rules('order', 'order', 'trim');
	                $this->form_validation->set_rules('tags', 'tags', 'trim');

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/newsimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = $newsitem->image; //No image uploaded!
						if($_FILES['image']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/newsimages/'. $id . '/' . $returndata['file_name'];
					}

					if($this->form_validation->run() == FALSE || $error != ''){
						$error .= validation_errors();
					} else {
						$url = $this->input->post('url');
						if(!strstr($url,'http://') && !strstr($url,'https://') && $url != '') {
							$url = 'http://'.$url;
						}

						$data = array(
								"title" => $this->input->post('title_'. $app->defaultlanguage),
								"txt" 	=> $this->input->post('txt_'. $app->defaultlanguage),
								"image"	=> $image,
	                            "datum" => date("Y-m-d H:i:s",time()),
	                            "order" => set_value('order'),
	                            'videourl'		=> $url
							);

						if($this->general_model->update('newsitem', $id, $data) && $imageError == ''){
	                        //add translated fields to translation table
	                        foreach($languages as $language) {
								$nameSuccess = $this->language_model->updateTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
								if(!$nameSuccess) {
									$this->language_model->addTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
								}
								$descrSuccess = $this->language_model->updateTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('txt_'.$language->key));
								if(!$descrSuccess) {
									$this->language_model->addTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('txt_'.$language->key));
								}
								foreach($metadata as $m) {
									if(_checkMultilang($m, $language->key, $app)) {
										$postfield = _getPostedField($m, $_POST, $_FILES, $language);
										if($postfield !== false) {
											if(_validateInputField($m, $postfield)) {
												_saveInputField($m, $postfield, 'newsitem', $id, $app, $language);
											}
										}
									}
								}
	                        }
    						// *** SAVE TAGS *** //
    						if(!empty($id)) {
								$this->db->where('newsitemid', $id);
								$this->db->where('appid', $app->id);
				                $this->db->delete('tag');
								$tags = $this->input->post('mytagsulselect');
								foreach ($tags as $tag) {
									$tags = array(
											'appid' 	=> $app->id,
											'tag' 		=> $tag,
											'newsitemid' => $id
										);
									$this->general_model->insert('tag', $tags);
								}
								if($this->input->post('tagfield') != null && trim($this->input->post('tagfield')) != '') {
									$tags = array(
											'appid' 	=> $app->id,
											'tag' 		=> $this->input->post('tagfield'),
											'newsitemid' => $id
										);
									$this->general_model->insert('tag', $tags);
								}
							}

							$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been updated'));
							$this->general_model->update('app', $app->id, array('timestamp' => time()));
							redirect('news/app/'.$app->id);
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}

	                    if($imageError != '') {
	                        $error .= '<br />' .$imageError;
	                    }
					}
				}

				$cdata['content'] 		= $this->load->view('c_news_edit_content', array('app' => $app, 'newsitem' => $newsitem, 'error' => $error, 'languages' => $languages, 'tags' => $tags, 'apptags' => $apptags, 'tags' => $tags, 'metadata' => $metadata), TRUE);
				$cdata['crumb']			= array($this->_module_settings->title => "news/app/".$app->id, __("Edit: ") . $newsitem->title => $this->uri->uri_string());
				$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
				$this->load->view('master', $cdata);
			} else {
				$this->iframeurl = 'news/view/'.$id;

				// TAGS
				$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'newsitem' GROUP BY tag");
				if($apptags->num_rows() == 0) {
					$apptags = array();
				} else {
					$apptags = $apptags->result();
				}
				$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'newsitem'");
				if($tags->num_rows() == 0) {
					$tags = array();
				} else {
					$tagz = array();
					foreach ($tags->result() as $tag) {
						$tagz[] = $tag;
					}
					$tags = $tagz;
				}

				//validation
				if($this->input->post('mytagsulselect') != null) {
					$postedtags = $this->input->post('mytagsulselect');
				} else {
					$postedtags = array();
				}

				if($this->input->post('postback') == "postback") {
	                foreach($languages as $language) {
	                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
	                    $this->form_validation->set_rules('text_'.$language->key, 'text ('.$language->name.')', 'trim|required');
						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								foreach(_setRules($m, $language) as $rule) {
									$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
								}
							}
						}
	                }
					$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');

					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/newsimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = $newsitem->image; //No image uploaded!
						if($_FILES['image']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/newsimages/'. $id . '/' . $returndata['file_name'];
					}

					if($this->form_validation->run() == FALSE || $error != ''){
						$error .= validation_errors();
					} else {
						$this->db->where('itemtype', 'newsitem');
						$this->db->where('itemid', $id);
						$this->db->delete('tc_tag');
						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$this->general_model->insert('tc_tag', array(
								'appid' => $app->id,
								'itemtype' => 'newsitem',
								'itemid' => $id,
								'tag' => $tag
							));
						}

						$data = array(
								"title" => set_value('title_'. $app->defaultlanguage),
								"txt" 	=> set_value('text_'. $app->defaultlanguage),
								"image"	=> $image,
	                            "datum" => date("Y-m-d H:i:s",time())
							);

						if($this->general_model->update('newsitem', $id, $data) && $imageError == ''){
	                        //add translated fields to translation table
	                        foreach($languages as $language) {
								$nameSuccess = $this->language_model->updateTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
								if(!$nameSuccess) {
									$this->language_model->addTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
								}
								$descrSuccess = $this->language_model->updateTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('text_'.$language->key));
								if(!$descrSuccess) {
									$this->language_model->addTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('text_'.$language->key));
								}
								foreach($metadata as $m) {
									if(_checkMultilang($m, $language->key, $app)) {
										$postfield = _getPostedField($m, $_POST, $_FILES, $language);
										if($postfield !== false) {
											if(_validateInputField($m, $postfield)) {
												_saveInputField($m, $postfield, 'newsitem', $id, $app, $language);
											}
										}
									}
								}
	                        }
							$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been updated'));
							$this->general_model->update('app', $app->id, array('timestamp' => time()));
							redirect('news/app/'.$app->id);
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}

	                    if($imageError != '') {
	                        $error .= '<br />' .$imageError;
	                    }
					}
				}

				$cdata['content'] 		= $this->load->view('c_news_edit', array('app' => $app, 'newsitem' => $newsitem, 'error' => $error, 'languages' => $languages, 'postedtags' => $postedtags, 'tags' => $tags, 'metadata' => $metadata), TRUE);
				$cdata['crumb']			= array($this->_module_settings->title => "news/app/".$app->id, __("Edit: ") . $newsitem->title => $this->uri->uri_string());
				$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
				$this->load->view('master', $cdata);
			}
		} else {
			$this->load->library('form_validation');
			$error = "";
            $imageError = "";
			// EDIT NEWSITEM
			$newsitem = $this->news_model->getNewsById($id);
			if($newsitem == FALSE) redirect('events');

			$event = $this->event_model->getbyid($newsitem->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'news/view/'.$id;

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'newsitem' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'newsitem'");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag;
				}
				$tags = $tagz;
			}

			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			$launcher = $this->module_mdl->getLauncher(1, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = ucfirst($launcher->title);
			$metadata = $this->metadata_model->getMetadata($launcher, 'newsitem', $id, $app);

			if($this->input->post('postback') == "postback") {
                foreach($languages as $language) {
                    $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('text_'.$language->key, 'text ('.$language->name.')', 'trim|required');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
				$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');


				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/newsimages/".$id, 0755, true);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('image')) {
					$image = $newsitem->image; //No image uploaded!
					if($_FILES['image']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/newsimages/'. $id . '/' . $returndata['file_name'];
				}

				if($this->form_validation->run() == FALSE || $error != ''){
					$error .= validation_errors();
				} else {
					$this->db->where('itemtype', 'newsitem');
						$this->db->where('itemid', $id);
						$this->db->delete('tc_tag');
						$tags = $this->input->post('mytagsulselect');
						foreach ($tags as $tag) {
							$this->general_model->insert('tc_tag', array(
								'appid' => $app->id,
								'itemtype' => 'newsitem',
								'itemid' => $id,
								'tag' => $tag
							));
						}

					$data = array(
							"title" => set_value('title_'. $app->defaultlanguage),
							"txt" 	=> set_value('text_'. $app->defaultlanguage),
							"image" => $image,
                            "datum" => date("Y-m-d H:i:s",time())
						);

					if($this->general_model->update('newsitem', $id, $data) && $imageError == ''){
                        //add translated fields to translation table
                        foreach($languages as $language) {
							$nameSuccess = $this->language_model->updateTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
							if(!$nameSuccess) {
								$this->language_model->addTranslation('newsitem', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
							}
							$descrSuccess = $this->language_model->updateTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('text_'.$language->key));
							if(!$descrSuccess) {
								$this->language_model->addTranslation('newsitem', $id, 'txt', $language->key, $this->input->post('text_'.$language->key));
							}
							foreach($metadata as $m) {
								if(_checkMultilang($m, $language->key, $app)) {
									$postfield = _getPostedField($m, $_POST, $_FILES, $language);
									if($postfield !== false) {
										if(_validateInputField($m, $postfield)) {
											_saveInputField($m, $postfield, 'newsitem', $id, $app, $language);
										}
									}
								}
							}
                        }
						$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('news/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}

                    if($imageError != '') {
                        $error .= '<br />' .$imageError;
                    }
				}
			}

			$cdata['content'] 		= $this->load->view('c_news_edit', array('event' => $event, 'newsitem' => $newsitem, 'error' => $error, 'languages' => $languages, 'app' => $app, 'postedtags' => $postedtags, 'tags' => $tags, 'metadata' => $metadata), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "news/event/".$event->id,__("Edit: ") . $newsitem->title => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "news/event/".$event->id,__("Edit: ") . $newsitem->title => $this->uri->uri_string());
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'news');
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function delete($id, $type) {
		if($type == 'venue'){
			$newsitem = $this->news_model->getNewsById($id);
			$venue = $this->venue_model->getbyid($newsitem->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
            //delete translations
            $this->language_model->removeTranslations('newsitem', $id);
			if($this->general_model->remove('newsitem', $id)){
				$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
				_updateVenueTimeStamp($newsitem->venueid);
				redirect('news/venue/'.$newsitem->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('news/venue/'.$newsitem->venueid);
			}
		} elseif($type == 'app'){
			$newsitem = $this->news_model->getNewsById($id);
			$appid = $newsitem->appid;
			$app = $this->app_model->get($appid);
			_actionAllowed($app, 'app');
            //delete translations
            $this->language_model->removeTranslations('newsitem', $id);
			if($this->general_model->remove('newsitem', $id)){
				$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
				$this->general_model->update('app',$appid,array('timestamp' => time()));
				redirect('news/app/'.$newsitem->appid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('news/app/'.$newsitem->appid);
			}
		} else {
			$newsitem = $this->news_model->getNewsById($id);
			$event = $this->event_model->getbyid($newsitem->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
            $this->language_model->removeTranslations('newsitem', $id);
			if($this->general_model->remove('newsitem', $id)){
				$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
				_updateTimeStamp($newsitem->eventid);
				redirect('news/event/'.$newsitem->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('news/event/'.$newsitem->eventid);
			}
		}
	}

	function rss_to_array($tag, $array, $url) {
		$doc = new DOMdocument();
		$doc->load($url);
		$rss_array = array();
		$items = array();
		foreach($doc->getElementsByTagName($tag) as $node) {
			foreach($array as $key => $value) {
				$items[$value] = $node->getElementsByTagName($value)->item(0)->nodeValue;
			}
			$items['hash'] = md5($node->getElementsByTagName('title')->item(0)->nodeValue.$node->getElementsByTagName('description')->item(0)->nodeValue.$node->getElementsByTagName('link')->item(0)->nodeValue);
			array_push($rss_array, $items);
		}
		return $rss_array;
	}

    function rss($id, $type = 'event') {
        $error = "";
		$rss_item_tag = 'item';
        if($type == 'venue'){
        	$venue = $this->venue_model->getbyid($id);
        	$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
            $currentRSSfeeds = $this->news_model->getRssOfVenue($id);

			$this->iframeurl = 'news/venue/'.$id;

			$launcher = $this->module_mdl->getLauncher(1, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

            if($this->input->post('postback') == "postback") {
                $this->load->library('form_validation');
				$this->form_validation->set_rules('url', 'url', 'trim|required');
                $this->form_validation->set_rules('refreshrate', 'Refresh rate', 'trim|numeric');
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$url = $this->input->post('url');
					$newssourceData = array(
					    "venueid"   => $venue->id,
					    "type"      => "rss",
					    "url"       => $url,
					    "timestamp" => date("Y-m-d H:i:s",time()),
					    "refreshrate" => abs($this->input->post('refreshrate'))
					);
					$sourceid = $this->general_model->insert('newssource', $newssourceData);
					$source = (object) $newssourceData;
					$source->id = $sourceid;

					$insert = '';
					$rss_url = $url;
					$rssfeed = $this->news_model->rss_to_array($rss_url);
					foreach($rssfeed as $item) {
						$hash = $item['hash'];
						$guid = $item['guid'];
						if(!empty($guid)) {
							$exists = $this->db->query("SELECT id, hash FROM newsitem WHERE (hash='$hash' OR guid = '$guid') AND venueid = $venue->id LIMIT 1");
						} else {
							$exists = $this->db->query("SELECT id, hash FROM newsitem WHERE hash='$hash' AND venueid = $venue->id LIMIT 1");
						}
						
						if($exists->num_rows() == 0) {
							$title = $item['title'];
							$txt = $item['description'];
							$url = $item['link'];
							$videourl = $item['videourl'];
							$extra = $item['extra'];
							
							if($item['pubDate']!= null && !empty($item['pubDate'])) {
								$date = date("Y-m-d H:i:s", strtotime($item['pubDate']));
							} else {
								$date = date("Y-m-d H:i:s",time());
							}
							$image = $item['image'];
							if(empty($image)) {
								$imgpos = stripos($txt, '<img');
								if($imgpos !== false) {
									// var_dump($item['description']);
									$imgend = stripos($txt, '</img>', $imgpos) + 6;
									if($imgend === false || $imgend == 6) {
										$imgend = stripos($txt, '/>', $imgpos) + 3;
									}
									if($imgend !== false && $imgpos !== false) {
										$imglength = $imgend - $imgpos;
										$image = substr($txt, $imgpos, $imglength);
										preg_match('@src="([^"]+)"@' , $image , $imagematch);
										if(isset($imagematch[1])) {
											$image = $imagematch[1];
										} else {
											$image = '';
										}
										// $txt = substr($txt, 0, $imgpos) . substr($txt, $imgend - 1);
									}
								}
								
								if(empty($image)) {
									preg_match('@enclosure url="([^"]+)"@' , $txt , $imagematch);
									if(isset($imagematch[1])) {
										$image = $imagematch[1];
									} else {
										preg_match('@media:content url="([^"]+)"@' , $txt , $imagematch);
										if(isset($imagematch[1])) {
											$image = $imagematch[1];
										} else {
											$image = '';
										}
									}
								}
							}

							$deletetime = time() - $source->deleteafterdays * 3600 * 24;
							if($source->deleteafterdays == 0 || strtotime($date) >= $deletetime) {
								$insert = 'inserted';
								$sql = "INSERT INTO newsitem (venueid, title, txt, image, url, sourceid, datum, hash, videourl, guid, extra) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
								$this->db->query($sql, array($venue->id,$title, $txt, $image, $url, $source->id, $date, $hash, $videourl, $guid, $extra));
								$newsitemid = $this->db->insert_id();
								if($newsitemid && isset($item['categories']) && !empty($item['categories'])) {
									foreach($item['categories'] as $cat) {
										$tagExists = $this->db->query("SELECT id FROM tag WHERE appid = $app->id AND `tag` = ? AND newsitemid = $newsitemid LIMIT 1", array($cat));
										if($tagExists->num_rows() == 0) {
											$this->db->query("INSERT INTO `tag` (appid, `tag`, newsitemid) VALUES ($app->id, ?, $newsitemid)", array($cat));
										}
									}
								}
								if($newsitemid && isset($source->tag) && !empty($source->tag)) {
									$tagExists = $obj->db->query("SELECT id FROM tag WHERE appid = $appid AND `tag` = ? AND newsitemid = $newsitemid LIMIT 1", array($source->tag));
									if($tagExists->num_rows() == 0) {
										$obj->db->query("INSERT INTO `tag` (appid, `tag`, newsitemid) VALUES ($appid, ?, $newsitemid)", array($source->tag));
									}
								}
							}
						}
					}

					if(!empty($insert)) {
						$this->db->query("UPDATE newssource SET hash = '$sourcehash', `timestamp` = '$today' WHERE id = $source->id");
						$update = true;
					}

                    if($error == '') {
                        $this->session->set_flashdata('event_feedback', __('The rss has successfully been imported'));
                        _updateTimeStamp($venue->id);
                        redirect('news/venue/'.$venue->id);
                    }
                }
            }
            $cdata['content'] 		= $this->load->view('c_news_rss', array('venue' => $venue, 'error' => $error, 'currentRSSfeeds' => $currentRSSfeeds), TRUE);
            $cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "news/venue/".$venue->id, __("Import RSS") => $this->uri->uri_string()));
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
            $cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'news');
            $this->load->view('master', $cdata);
        } elseif($type == 'app') {
            $app = $this->app_model->get($id);
			_actionAllowed($app, 'app','returnApp');
			$currentRSSfeeds = $this->news_model->getRssOfApp($id);
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'news/app/'.$id;

			$launcher = $this->module_mdl->getLauncher(1, 'app', $app->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

            if($this->input->post('postback') == "postback") {
                $this->load->library('form_validation');
				$this->form_validation->set_rules('url', 'url', 'trim|required');
                $this->form_validation->set_rules('refreshrate', 'Refresh rate', 'trim|numeric');
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$url = $this->input->post('url');
					$newssourceData = array(
					    "appid"   => $app->id,
					    "type"      => "rss",
					    "url"       => $url,
					    "timestamp" => date("Y-m-d H:i:s",time()),
					    "refreshrate" => abs($this->input->post('refreshrate'))
					);
					$sourceid = $this->general_model->insert('newssource', $newssourceData);
					$source = (object) $newssourceData;
					$source->id = $sourceid;

					$insert = '';
					$rss_url = $url;
					$rssfeed = $this->news_model->rss_to_array($rss_url);
					foreach($rssfeed as $item) {
						$hash = $item['hash'];
						$guid = $item['guid'];
						if(!empty($guid)) {
							$exists = $this->db->query("SELECT id, hash FROM newsitem WHERE (hash='$hash' OR guid = '$guid') AND appid = $app->id LIMIT 1");
						} else {
							$exists = $this->db->query("SELECT id, hash FROM newsitem WHERE hash='$hash' AND appid = $app->id LIMIT 1");
						}
						if($exists->num_rows() == 0) {
							$title = $item['title'];
							$txt = $item['description'];
							$url = $item['link'];
							$videourl = $item['videourl'];
							$extra = $item['extra'];
							if($item['pubDate']!= null && !empty($item['pubDate'])) {
								$date = date("Y-m-d H:i:s", strtotime($item['pubDate']));
							} else {
								$date = date("Y-m-d H:i:s",time());
							}
							$image = $item['image'];
							if(empty($image)) {
								$imgpos = stripos($txt, '<img');
								if($imgpos !== false) {
									// var_dump($item['description']);
									$imgend = stripos($txt, '</img>', $imgpos) + 6;
									if($imgend === false || $imgend == 6) {
										$imgend = stripos($txt, '/>', $imgpos) + 3;
									}
									if($imgend !== false && $imgpos !== false) {
										$imglength = $imgend - $imgpos;
										$image = substr($txt, $imgpos, $imglength);
										preg_match('@src="([^"]+)"@' , $image , $imagematch);
										if(isset($imagematch[1])) {
											$image = $imagematch[1];
										} else {
											$image = '';
										}
										// $txt = substr($txt, 0, $imgpos) . substr($txt, $imgend - 1);
									}
								}
								
								if(empty($image)) {
									preg_match('@enclosure url="([^"]+)"@' , $txt , $imagematch);
									if(isset($imagematch[1])) {
										$image = $imagematch[1];
									} else {
										preg_match('@media:content url="([^"]+)"@' , $txt , $imagematch);
										if(isset($imagematch[1])) {
											$image = $imagematch[1];
										} else {
											$image = '';
										}
									}
								}
							}

							$deletetime = time() - $source->deleteafterdays * 3600 * 24;
							if($source->deleteafterdays == 0 || strtotime($date) >= $deletetime) {
								$insert = 'inserted';
								$sql = "INSERT INTO newsitem (appid, title, txt, image, url, sourceid, datum, hash, videourl, guid, extra) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
								$this->db->query($sql, array($app->id,$title, $txt, $image, $url, $source->id, $date, $hash, $videourl, $guid, $extra));
								$newsitemid = $this->db->insert_id();
								if($newsitemid && isset($item['categories']) && !empty($item['categories'])) {
									foreach($item['categories'] as $cat) {
										$tagExists = $this->db->query("SELECT id FROM tag WHERE appid = $app->id AND `tag` = ? AND newsitemid = $newsitemid LIMIT 1", array($cat));
										if($tagExists->num_rows() == 0) {
											$this->db->query("INSERT INTO `tag` (appid, `tag`, newsitemid) VALUES ($app->id, ?, $newsitemid)", array($cat));
										}
									}
								}
								if($newsitemid && isset($source->tag) && !empty($source->tag)) {
									$tagExists = $obj->db->query("SELECT id FROM tag WHERE appid = $appid AND `tag` = ? AND newsitemid = $newsitemid LIMIT 1", array($source->tag));
									if($tagExists->num_rows() == 0) {
										$obj->db->query("INSERT INTO `tag` (appid, `tag`, newsitemid) VALUES ($appid, ?, $newsitemid)", array($source->tag));
									}
								}
							}
						}
					}

					if(!empty($insert)) {
						$this->db->query("UPDATE newssource SET hash = '$sourcehash', `timestamp` = '$today' WHERE id = $source->id");
						$update = true;
					}

                    if($error == '') {
                        $this->session->set_flashdata('event_feedback', __('The RSS has successfully been imported'));
                        $this->general_model->update('app', $app->id, array('timestamp' => time()));
                        redirect('news/app/'.$app->id);
                    }
                }
            }
            $cdata['content'] 		= $this->load->view('c_news_rss', array('app' => $app, 'error' => $error, 'currentRSSfeeds' => $currentRSSfeeds), TRUE);
            $cdata['crumb']			= array($this->_module_settings->title => "news/app/".$app->id, __("Import RSS") => $this->uri->uri_string());
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
            $this->load->view('master', $cdata);
        } else {
            $event = $this->event_model->getbyid($id);
			$currentRSSfeeds = $this->news_model->getRssOfEvent($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'news/event/'.$id;

			$launcher = $this->module_mdl->getLauncher(1, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

            if($this->input->post('postback') == "postback") {
                $this->load->library('form_validation');
				$this->form_validation->set_rules('url', 'url', 'trim|required');
                $this->form_validation->set_rules('refreshrate', 'Refresh rate', 'trim|numeric');
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$url = $this->input->post('url');
					$newssourceData = array(
					    "eventid"   => $event->id,
					    "type"      => "rss",
					    "url"       => $url,
					    "timestamp" => date("Y-m-d H:i:s",time()),
					    "refreshrate" => abs($this->input->post('refreshrate'))
					);
					$sourceid = $this->general_model->insert('newssource', $newssourceData);
					$source = (object) $newssourceData;
					$source->id = $sourceid;

					$insert = '';
					$rss_url = $url;
					$rssfeed = $this->news_model->rss_to_array($rss_url);
					foreach($rssfeed as $item) {
						$hash = $item['hash'];
						$guid = $item['guid'];
						if(!empty($guid)) {
							$exists = $this->db->query("SELECT id, hash FROM newsitem WHERE (hash='$hash' OR guid = '$guid') AND eventid = $event->id LIMIT 1");
						} else {
							$exists = $this->db->query("SELECT id, hash FROM newsitem WHERE hash='$hash' AND eventid = $event->id LIMIT 1");
						}
						if($exists->num_rows() == 0) {
							$title = $item['title'];
							$txt = $item['description'];
							$url = $item['link'];
							$videourl = $item['videourl'];
							$extra = $item['extra'];
							if($item['pubDate']!= null && !empty($item['pubDate'])) {
								$date = date("Y-m-d H:i:s", strtotime($item['pubDate']));
							} else {
								$date = date("Y-m-d H:i:s",time());
							}
							$image = $item['image'];
							if(empty($image)) {
								$imgpos = stripos($txt, '<img');
								if($imgpos !== false) {
									// var_dump($item['description']);
									$imgend = stripos($txt, '</img>', $imgpos) + 6;
									if($imgend === false || $imgend == 6) {
										$imgend = stripos($txt, '/>', $imgpos) + 3;
									}
									if($imgend !== false && $imgpos !== false) {
										$imglength = $imgend - $imgpos;
										$image = substr($txt, $imgpos, $imglength);
										preg_match('@src="([^"]+)"@' , $image , $imagematch);
										if(isset($imagematch[1])) {
											$image = $imagematch[1];
										} else {
											$image = '';
										}
										// $txt = substr($txt, 0, $imgpos) . substr($txt, $imgend - 1);
									}
								}
								
								if(empty($image)) {
									preg_match('@enclosure url="([^"]+)"@' , $txt , $imagematch);
									if(isset($imagematch[1])) {
										$image = $imagematch[1];
									} else {
										preg_match('@media:content url="([^"]+)"@' , $txt , $imagematch);
										if(isset($imagematch[1])) {
											$image = $imagematch[1];
										} else {
											$image = '';
										}
									}
								}
							}
							$deletetime = time() - $source->deleteafterdays * 3600 * 24;
							if($source->deleteafterdays == 0 || strtotime($date) >= $deletetime) {
								$insert = 'inserted';
								$sql = "INSERT INTO newsitem (eventid, title, txt, image, url, sourceid, datum, hash, videourl, guid, extra) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
								$this->db->query($sql, array($event->id,$title, $txt, $image, $url, $source->id, $date, $hash, $videourl, $guid, $extra));

								$newsitemid = $this->db->insert_id();
								if($newsitemid && isset($item['categories']) && !empty($item['categories'])) {
									foreach($item['categories'] as $cat) {
										$tagExists = $this->db->query("SELECT id FROM tag WHERE appid = $app->id AND `tag` = ? AND newsitemid = $newsitemid LIMIT 1", array($cat));
										if($tagExists->num_rows() == 0) {
											$this->db->query("INSERT INTO `tag` (appid, `tag`, newsitemid) VALUES ($app->id, ?, $newsitemid)", array($cat));
										}
									}
								}
								if($newsitemid && isset($source->tag) && !empty($source->tag)) {
									$tagExists = $obj->db->query("SELECT id FROM tag WHERE appid = $appid AND `tag` = ? AND newsitemid = $newsitemid LIMIT 1", array($source->tag));
									if($tagExists->num_rows() == 0) {
										$obj->db->query("INSERT INTO `tag` (appid, `tag`, newsitemid) VALUES ($appid, ?, $newsitemid)", array($source->tag));
									}
								}
							}
						}
					}

					if(!empty($insert)) {
						$this->db->query("UPDATE newssource SET hash = '$sourcehash', `timestamp` = '$today' WHERE id = $source->id");
						$update = true;
					}

                    if($error == '') {
                        $this->session->set_flashdata('event_feedback', __('The rss has successfully been imported'));
                        _updateTimeStamp($event->id);
                        redirect('news/event/'.$event->id);
                    }
                }
            }
            $cdata['content'] 		= $this->load->view('c_news_rss', array('event' => $event, 'error' => $error, 'currentRSSfeeds' => $currentRSSfeeds), TRUE);
            if($app->familyid != 1) {
	            $cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "news/event/".$event->id, __("Import RSS") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
	        } else {
	        	$cdata['crumb']			= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "news/event/".$event->id, __("Import RSS") => $this->uri->uri_string());
	        }
	        $cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'news');
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
            $this->load->view('master', $cdata);
        }
    }

    function rsslist($id, $type = 'event') {
        $error = "";
        if($type == 'venue'){
        	$venue = $this->venue_model->getbyid($id);
        	$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
            $currentRSSfeeds = $this->news_model->getRssOfVenue($id);

			$this->iframeurl = 'news/venue/'.$id;

			$launcher = $this->module_mdl->getLauncher(1, 'venue', $venue->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

            $cdata['content'] 		= $this->load->view('c_news_rsslist', array('venue' => $venue, 'error' => $error, 'currentRSSfeeds' => $currentRSSfeeds, 'type' => 'venue', 'typeid' => $id), TRUE);
            $cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, $this->_module_settings->title => "news/venue/".$venue->id, __("Import RSS") => $this->uri->uri_string()));
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
            $cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'news');
            $this->load->view('master', $cdata);
        } elseif($type == 'app') {
            $app = $this->app_model->get($id);
			_actionAllowed($app, 'app','returnApp');
			$currentRSSfeeds = $this->news_model->getRssOfApp($id);
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'news/app/'.$id;

			$launcher = $this->module_mdl->getLauncher(1, 'app', $app->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

            $cdata['content'] 		= $this->load->view('c_news_rsslist', array('app' => $app, 'error' => $error, 'currentRSSfeeds' => $currentRSSfeeds, 'type' => 'app', 'typeid' => $id), TRUE);
            $cdata['crumb']			= array($this->_module_settings->title => "news/app/".$app->id, __("Import RSS") => $this->uri->uri_string());
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
            $this->load->view('master', $cdata);
        } else {
            $event = $this->event_model->getbyid($id);
			$currentRSSfeeds = $this->news_model->getRssOfEvent($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'news/event/'.$id;

			$launcher = $this->module_mdl->getLauncher(1, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;

            $cdata['content'] 		= $this->load->view('c_news_rsslist', array('event' => $event, 'error' => $error, 'currentRSSfeeds' => $currentRSSfeeds, 'type' => 'event', 'typeid' => $id), TRUE);
            if($app->familyid != 1) {
	            $cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "news/event/".$event->id, __("Import RSS") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
	        } else {
	        	$cdata['crumb']			= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "news/event/".$event->id, __("Import RSS") => $this->uri->uri_string());
	        }
	        $cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'news');
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
            $this->load->view('master', $cdata);
        }
    }

    function deletesource($sourceid) {
        $source = $this->news_model->getSourceOfId($sourceid);
        $this->news_model->removeNewsOfSourceId($sourceid);

        if($this->general_model->remove('newssource', $sourceid)){
            if($source->venueid != '' && $source->venueid != '0') {
                $this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
                _updateVenueTimeStamp($source->venueid);
                redirect('news/venue/'.$source->venueid);
            } elseif($source->appid != '' && $source->appid != '0') {
                $this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
                $this->general_model->update('app', $app->id, array('timestamp' => time()));
                redirect('news/app/'.$source->appid);
            } else {
                $this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
                _updateTimeStamp($source->eventid);
                redirect('news/event/'.$source->eventid);
            }
        } else {
            if($source->venueid != ''  && $source->venueid != '0') {
                $this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
                redirect('news/venue/'.$source->venueid);
            } elseif($source->appid != '' && $source->appid != '0') {
                $this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
                redirect('news/app/'.$source->appid);
            } else {
                $this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
                redirect('news/event/'.$source->eventid);
            }
        }
    }

    function crop($id, $type = 'event') {
        $error = '';
        $newsitem = $this->news_model->getNewsById($id);
        if($newsitem == FALSE) redirect('events');
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($newsitem->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
		} elseif($type=='app'){
			$appid = $newsitem->appid;
			$app = $this->app_model->get($appid);
			_actionAllowed($app, 'app');
		}else{
            $event = $this->event_model->getbyid($newsitem->eventid);
            $app = _actionAllowed($venue, 'venue','returnApp');
		}

        $newspicwidth = $this->general_model->get_meta_data_by_key('app', $app->id, 'newspicwidth');
        $newspicheight = $this->general_model->get_meta_data_by_key('app', $app->id, 'newspicheight');
        if(isset($newspicwidth) && $newspicwidth != false) {
        	$newspicwidth = $newspicwidth->value;
        } else {
        	$newspicwidth = 0;
        }
        if(isset($newspicheight) && $newspicheight != false) {
        	$newspicheight = $newspicheight->value;
        } else {
        	$newspicheight = 0;
        }

        if($this->input->post('postback') == "postback") {
           $w = $this->input->post('w');
           $h = $this->input->post('h');
           $x = $this->input->post('x');
           $y = $this->input->post('y');

		   $newname = 'upload/newsimages/'.$newsitem->id.'/cropped_'.time().'.jpg';
           $config['image_library']  = 'gd2';
           $config['source_image']  = $this->config->item('imagespath') . $newsitem->image;
           $config['create_thumb']  = FALSE;
           $config['maintain_ratio']  = FALSE;
           $config['width']    = $w;
           $config['height']   = $h;
           $config['x_axis']   = $x;
           $config['y_axis']   = $y;
           $config['dynamic_output']  = FALSE;
           $config['new_image'] = $this->config->item('imagespath') . $newname;

           $this->load->library('image_lib', $config);
           $this->image_lib->initialize($config);

           if (!$this->image_lib->crop())
           {
			   $newname = $newsitem->image;
			   echo $this->image_lib->display_errors();
           }

			$data = array(
					"image"	=> $newname,
			        "datum" => date("Y-m-d H:i:s",time())
				);

			$this->general_model->update('newsitem', $newsitem->id, $data);
			$newsitem->image = $newname;
        }

        if($type == 'venue') {
            $venue = $this->venue_model->getbyid($newsitem->venueid);
            $cdata['content'] 		= $this->load->view('c_news_crop', array('venue' => $venue, 'newsitem' => $newsitem, 'error' => $error, 'newspicheight' => $newspicheight, 'newspicwidth' => $newspicwidth), TRUE);
            $cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("News") => "news/venue/".$venue->id, __("Edit Picture: ") . $newsitem->title => $this->uri->uri_string()));
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
            $cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'news');
            $this->load->view('master', $cdata);
        } elseif($type == 'app') {
            $cdata['content'] 		= $this->load->view('c_news_crop', array('app' => $app, 'newsitem' => $newsitem, 'error' => $error, 'newspicheight' => $newspicheight, 'newspicwidth' => $newspicwidth), TRUE);
            $cdata['crumb']			= array(__("News") => "news/app/".$app->id, __("Edit Picture: ") . $newsitem->title => $this->uri->uri_string());
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
            $this->load->view('master', $cdata);
        } else {
            $event = $this->event_model->getbyid($newsitem->eventid);
            $cdata['content'] 		= $this->load->view('c_news_crop', array('event' => $event, 'newsitem' => $newsitem, 'error' => $error, 'newspicheight' => $newspicheight, 'newspicwidth' => $newspicwidth), TRUE);
            if($app->familyid != 1) {
	            $cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("News") => "news/event/".$event->id, __("Edit Picture: ") . $newsitem->title => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
	        } else {
	        	$cdata['crumb']	= array($event->name => "event/view/".$event->id, __("News") => "news/event/".$event->id, __("Edit Picture: ") . $newsitem->title => $this->uri->uri_string());
	        }
            $cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
            $this->load->view('master', $cdata);
        }
    }

	function removeimage($id, $type = 'event') {
		$news = $this->news_model->getNewsById($id);
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($news->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $news->image)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $news->image);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('image' => '');
			$this->general_model->update('newsitem', $id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('news/venue/'.$venue->id);
		} elseif($type == 'app') {
			$app = $this->app_model->get($news->appid);
			_actionAllowed($app, 'app');

			if(!file_exists($this->config->item('imagespath') . $news->image)) redirect('apps');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $news->image);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('image' => '');
			$this->general_model->update('newsitem', $id, $data);

			$this->general_model->update('app', $app->id, array('timestamp' => time()));
			redirect('news/app/'.$app->id);
		} else {
			$event = $this->event_model->getbyid($news->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $news->image)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $news->image);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('image' => '');
			$this->general_model->update('newsitem', $id, $data);

			_updateTimeStamp($event->id);
			redirect('news/event/'.$event->id);
		}
	}
        
    public function importrss($id) {
	    //$contentmodule = $this->contentmodule_model->getById($id);
		// $currentRSSfeeds = $this->news_model->getRssOfApp($id);            
	 //    if ($this->input->post('postback') == "postback") {
	 //        $this->load->library('form_validation');
	 //        $this->form_validation->set_rules('url', 'url', 'trim|required');
	 //        $this->form_validation->set_rules('refreshrate', 'refreshrate', 'trim|numeric');
	 //        $this->form_validation->set_rules('tag', 'tag', 'trim|required');            
	 //        if ($this->form_validation->run() == FALSE) {
	 //            $error = __("Some fields are missing.");
	 //        } else {
	 //            $tag = $this->input->post('tag');
	 //            $url = $this->input->post('url');
	 //            //$url = 'http://feeds.reuters.com/reuters/Election2012';
	 //            $content = file_get_contents($url);
	 //            try {
	 //                $rss = new SimpleXmlElement($content);
	 //            } catch (Exception $e) {
	 //                $rss = null;
	 //            }
	 //            //echo '<pre>';print_r($rss->channel->cloud->attributes()->path);echo'</pre>';exit;
	 //            $urlExists = $this->news_model->urlExists($id,$url);
	            
	 //            if ($content != null && $rss != null && !empty($content) && !empty($rss)) {
	 //                $newssourceData = array(
	 //                    "appid" => $id,
	 //                    "type" => "rss",
	 //                    "url" => $url,
	 //                    "timestamp" => date("Y-m-d H:i:s", time()),
	 //                    "refreshrate" => $this->input->post('refreshrate'),
	 //                    "tags" => $tag    
	 //                );
	                
	 //                if($urlExists > 0){
	 //                    $error = __("<p>RSS Feed URL already exists.</p>");
	 //                    $this->session->set_flashdata('error', __("<p>RSS Feed URL already exists.</p>"));                        
	 //                    redirect('news/importrss/' . $id);
	 //                }else{
	 //                    if($sourceid = $this->general_model->insert('newssource', $newssourceData)){
	 //                        $tagData = array(
	 //                            "contentmoduleid" => $contentmodule->id,
	 //                            "tag" => (string) $tag                                
	 //                        );                            
	 //                        //$this->general_model->insert('tag', $tagData);                            
	 //                    }
	 //                }
	                
	 //                foreach ($rss->channel->item as $post) {
	 //                    $description = (string) $post->description;
	 //                    $namespace = $post->children('http://purl.org/rss/1.0/modules/content/');
	 //                    $content = (string) $namespace->encoded;
	 //                    if ($content != null) {
	 //                        $description = $content;
	 //                    }

		// 				// get host name from URL
		// 				preg_match('@^(?:http://)?([^/]+)@i',
		// 				    $post->link, $matches);
		// 				$host = $matches[1];

		// 				// get last two segments of host name
		// 				preg_match('/[^.]+\.[^.]+$/', $host, $matches);
		// 				$hostUrl = $matches[0];

		// 				// Check if its a youtube URL for a video
		// 				if($hostUrl=="youtube.com"){
		// 					$field = "videourl";
		// 				}else{
		// 					$field = "url";
		// 				}

	 //                    $data = array(
	 //                        "title" => (string) $post->title,
	 //                        "appid" => $id,
	 //                        "txt" => $description,
	 //                        "".$field."" => (string) $post->link,
	 //                        "sourceid" => $sourceid,
	 //                        "datum" => ((string) $post->pubDate != null && (string) $post->pubDate != '') ? date("Y-m-d H:i:s", strtotime((string) $post->pubDate)) : date("Y-m-d H:i:s", time())
	 //                    );


		// 				$dom = new domDocument;
		// 				$dom->loadHTML($data['txt']);
		// 				$dom->preserveWhiteSpace = false;
		// 				$images = $dom->getElementsByTagName('img');
						
		// 				foreach ($images as $image) {
		// 				  $desc_image = $image->getAttribute('src');
		// 				}

	 //                    $newsItemExists = $this->news_model->newsItemExists($sourceid, $post->link);
	 //                    if($newsItemExists <= 0){
	 //                        if ($newsid = $this->general_model->insert('newsitem', $data)) {

		// 					  if (!is_dir($this->config->item('imagespath').'upload/newsimages/'.$newsid)){
		// 						    mkdir($this->config->item('imagespath').'upload/newsimages/'.$newsid);
	 //                                if(isset($post->image)){
		// 		                        $file_name = $this->ShowFileName($post->image);
		// 		                        $file_ext = $this->ShowFileExtension($post->image);                                                                        	
	 //                                    copy($post->image, $this->config->item('imagespath').'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext);
	 //                                    $data = array("image" => 'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext);
	 //                                    $this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext));
	 //                                }
	                                
	 //                                if(isset($post->enclosure->attributes()->url)){
		// 		                        $file_name = $this->ShowFileName($post->enclosure->attributes()->url);
		// 		                        $file_ext = $this->ShowFileExtension($post->enclosure->attributes()->url);                                                                        	
	 //                                    copy($post->enclosure->attributes()->url, $this->config->item('imagespath').'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext);
	 //                                    $data = array("image" => 'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext);
	 //                                    $this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$file_ext));
	                                                                                                      
	 //                                }                                                                    
	                                
	 //                                if(!empty($desc_image)){
		// 		                        $file_name = $this->ShowFileName($desc_image);
		// 		                        $ext = $this->ShowFileExtension($desc_image);													                        
	 //                                	if($ext=="jpg" || $ext=="gif" || $ext=="png"){
	 //                                    	copy($desc_image, $this->config->item('imagespath').'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$ext);
	 //                                    	$this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/'.$newsid.'/'.$file_name.'.'.$ext));
	 //                                	}else{
	 //                                		$this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/no-image-placeholder.png'));
	 //                                	}
	 //                                }

	 //                                if(!isset($post->image) && !isset($desc_image) && !isset($post->enclosure->attributes()->url)){
	 //                              		$this->general_model->update('newsitem', $newsid, array("image"=>'upload/newsimages/no-image-placeholder.png'));
	 //                                }
	 //                           }

	 //                            //add translated fields to translation table
	 //                            if ($languages != null) {
	 //                                foreach ($languages as $language) {
	 //                                    $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, (string) $post->title);
	 //                                    $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $description);
	 //                                }
	 //                            }

	 //                            $mystring = 'tag1,tag2,tag3';

		// 						$pos = strrpos($mystring, ",");
		// 						if (strlen($pos) > -1) { // note: three equal signs
		// 						    echo $pos;
		// 						}
								
	 //                            $tags = "";
	 //                            if($this->input->post('tag')!=""){
	 //                               $tags.= $this->input->post('tag');
	 //                            }
	 //                            $tagsList = explode(",",$tags);
	 //                            if(isset($post->category)){
		// 							$categories = strpos($post->category, ',');

		// 							if(strlen($categories) > -1) {
		// 								$categories = explode(",",$categories);

		//                                 foreach($categories as $category){
		//                                 	$tagsList[] = $category;

		//                                 }

		// 							}else{
	 //                            	  $tagsList[] = $post->category;
	 //                            	}
	 //                            }
	 //                            foreach($tagsList as $tag){
	 //                                $tagData = array(
	 //                                    "newsitemid" => (string) $newsid,
	 //                                    "tag" => (string) $tag                                
	 //                                );
	 //                                $this->general_model->insert('tag', $tagData);
	 //                            }
	 //                        } else {
	 //                            $error = __("<p>Oops, something went wrong. Please try again.</p>");
	 //                        }
	 //                    }   
	 //                }
	 //            } else {
	 //                $error = __("<p>Oops, something went wrong. Please try again.</p>");
	 //            }

	 //            if ($error == '') {
	 //                $this->session->set_flashdata('event_feedback', __('The rss has succesfully been imported'));
	 //                _updateTimeStamp($id);
	 //                redirect('news/app/' . $id);
	 //            }
	 //        }
	 //    }

	 //    $cdata['content'] = $this->load->view('c_importrss_view', array('error' => $error, 'appid' => $id, 'currentRSSfeeds' => $currentRSSfeeds), TRUE);
	 //    $this->load->view('master', $cdata);
	}
    
    function editRss($sourceId, $appId){
        if ($this->input->post('postback') == "postback") {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('url', 'url', 'trim|required');
            $this->form_validation->set_rules('refreshrate', 'refreshrate', 'trim|numeric');
            //$this->form_validation->set_rules('tag', 'tag', 'trim|required');            
            if ($this->form_validation->run() == FALSE) {
                $error = __("Some fields are missing.");
            } else {
                $tag = $this->input->post('tag');
                $url = $this->input->post('url');
                
                    $newssourceData = array(
                        "url" => $url,
                        "timestamp" => date("Y-m-d H:i:s", time()),
                        "refreshrate" => $this->input->post('refreshrate')
                    );
                    $this->general_model->update('newssource',$sourceId, $newssourceData);                                        
            }
        }
        
	$currentRSSfeeds = $this->news_model->getRssOfApp($appId);        
        $currentSource = $this->news_model->getSourceOfId($sourceId);
        $cdata['content'] = $this->load->view('c_importrss_view', array('error' => $error, 'appid' => $appId, 'currentRSSfeeds' => $currentRSSfeeds, 'currentSource' => $currentSource), TRUE);
        $this->load->view('master', $cdata);        
    }
    //check if file exists OR not
    public function remote_file_exists($url){
    	return(bool)preg_match('~HTTP/1\.\d\s+200\s+OK~', @current(get_headers($url)));
    }

    public function ShowFileExtension($filepath)
    {
        preg_match('/[^?]*/', $filepath, $matches);
        $string = $matches[0];
     
        $pattern = preg_split('/\./', $string, -1, PREG_SPLIT_OFFSET_CAPTURE);

        # check if there is any extension
        if(count($pattern) == 1)
        {
            echo 'No File Extension Present';
            exit;
        }
       
        if(count($pattern) > 1)
        {
            $filenamepart = $pattern[count($pattern)-1][0];
            preg_match('/[^?]*/', $filenamepart, $matches);
            return $matches[0];
        }
    }
   
    public function ShowFileName($filepath)
    {
        preg_match('/[^?]*/', $filepath, $matches);
        $string = $matches[0];
        #split the string by the literal dot in the filename
        $pattern = preg_split('/\./', $string, -1, PREG_SPLIT_OFFSET_CAPTURE);
        #get the last dot position
        $lastdot = $pattern[count($pattern)-1][1];
        #now extract the filename using the basename function
        $filename = basename(substr($string, 0, $lastdot-1));
        #return the filename part
        return $filename;
    }    

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'newsitem');
    }    

    function refreshsource($sourceid) {
  //       $source = $this->news_model->getSourceOfId($sourceid);

		// if($source->venueid != '' && $source->venueid != '0') {
		// 	$venue = $this->venue_model->getbyid($source->venueid);
		// 	$app = _actionAllowed($venue, 'venue','returnApp');
		// 	$this->news_model->refreshrss($source, $venue, 'venue');
		//     _updateVenueTimeStamp($source->venueid);
		//     redirect('news/venue/'.$source->venueid);
		// } elseif($source->appid != '' && $source->appid != '0') {
		// 	$app = $this->app_model->get($source->appid);
		// 	_actionAllowed($app, 'app');
		// 	$this->news_model->refreshrss($source, $app, 'app');
		//     $this->general_model->update('app', $app->id, array('timestamp' => time()));
		//     redirect('news/app/'.$source->appid);
		// } else {
		// 	$event = $this->event_model->getbyid($source->eventid);
		// 	$app = _actionAllowed($event, 'event','returnApp');
		// 	$this->news_model->refreshrss($source, $event, 'event');
		//     _updateTimeStamp($source->eventid);
		//     redirect('news/event/'.$source->eventid);
		// }
    }

    function addtosection($sectionid) {
    	$this->load->model('contentmodule_model');
		$this->load->library('form_validation');
		$error = "";
        $imageError = "";
		$section = $this->contentmodule_model->getSectionById($sectionid);
		$contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		// TAGS
		$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'newsitem' GROUP BY tag");
		if($apptags->num_rows() == 0) {
			$apptags = array();
		} else {
			$apptags = $apptags->result();
		}
		//validation
		if($this->input->post('mytagsulselect') != null) {
			$postedtags = $this->input->post('mytagsulselect');
		} else {
			$postedtags = array($section->title);
		}

		$launcher = $this->module_mdl->getLauncher(1, 'app', $app->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = ucfirst($launcher->title);

		$this->iframeurl = 'contentmodule/app/'.$app->id;

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');
            if($languages != null) {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
                $this->form_validation->set_rules('txt_'.$language->key, 'text ('.$language->name.')', 'trim|required');
				foreach($metadata as $m) {
					if(_checkMultilang($m, $language->key, $app)) {
						foreach(_setRules($m, $language) as $rule) {
							$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
						}
					}
				}
            }
            }
            $this->form_validation->set_rules('order', 'order', 'trim');
            $this->form_validation->set_rules('tags', 'tags', 'trim');

			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {
				$url = $this->input->post('url');

				// get host name from URL
				preg_match('@^(?:http://)?([^/]+)@i',
				    $url, $matches);
				$host = $matches[1];

				// get last two segments of host name
				preg_match('/[^.]+\.[^.]+$/', $host, $matches);
				$hostUrl = $matches[0];

				// Check if its a youtube URL for a video
				if($hostUrl=="youtube.com"){
					$field = "videourl";
				}else{
					$field = "url";
				}

				if(!strstr($url,'http://') && trim($url) != '' && !strstr($url,'https://')) {
					$url = 'http://'.$url;
				}
				$data = array(
						"title" 	=> $this->input->post('title_'.$app->defaultlanguage),
						"appid" 	=> $app->id,
						"txt" 		=> $this->input->post('txt_'.$app->defaultlanguage),
                        "datum"     => date("Y-m-d H:i:s",time()),
                        "order"		=> set_value('order'),
                        $field		=> $url
					);

				if(($newsid = $this->general_model->insert('newsitem', $data)) && $imageError == ''){
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/newsimages/".$newsid)){
						mkdir($this->config->item('imagespath') . "upload/newsimages/".$newsid, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/newsimages/'.$newsid;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = ""; //No image uploaded!
						if($_FILES['image']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/newsimages/'. $newsid . '/' . $returndata['file_name'];
					}

					$this->general_model->update('newsitem', $newsid, array('image' => $image));

                    //add translated fields to translation table
                    if($languages != null) {
                    foreach($languages as $language) {
                        $this->language_model->addTranslation('newsitem', $newsid, 'title', $language->key, $this->input->post('title_'.$language->key));
                        $this->language_model->addTranslation('newsitem', $newsid, 'txt', $language->key, $this->input->post('txt_'.$language->key));
						foreach($metadata as $m) {
							if(_checkMultilang($m, $language->key, $app)) {
								$postfield = _getPostedField($m, $_POST, $_FILES, $language);
								if($postfield !== false) {
									if(_validateInputField($m, $postfield)) {
										_saveInputField($m, $postfield, 'newsitem', $newsid, $app, $language);
									}
								}
							}
						}
                    }
                    }

					// *** SAVE TAGS *** //
					$tags = $this->input->post('mytagsulselect');
					foreach ($tags as $tag) {
						$tags = array(
								'appid' 	=> $app->id,
								'tag' 		=> $tag,
								'newsitemid' => $newsid
							);
						$this->general_model->insert('tag', $tags);
					}
					if($this->input->post('tagfield') != null && trim($this->input->post('tagfield')) != '') {
						$tags = array(
								'appid' 	=> $app->id,
								'tag' 		=> $this->input->post('tagfield'),
								'newsitemid' => $newsid
							);
						$this->general_model->insert('tag', $tags);
					}

					$this->session->set_flashdata('event_feedback', __('The newsitem has successfully been added!'));
					$this->general_model->update('app', $app->id, array('timestamp' => time()));
					if($error == '') {
						redirect('section/news/'.$section->id);
					}else {
						//image error
						redirect('news/edit/'.$newsid.'/app?error=image');
					}
				} else {
					$error = __("Oops, something went wrong. Please try again.");
				}

                if($imageError != '') {
                    $error .= '<br />' .$imageError;
                }
			}
		}

		$cdata['content'] 		= $this->load->view('c_news_add_content', array('app' => $app, 'error' => $error, 'languages' => $languages, 'apptags' => $apptags, 'metadata' => $metadata, 'startingtags' => $postedtags), TRUE);
		$cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id, $section->title => 'section/news/'.$section->id, __("Add new") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$this->load->view('master', $cdata);
    }
}
