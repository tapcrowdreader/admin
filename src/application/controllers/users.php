<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Users extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		
		$this->load->model('users_model');
		$this->load->model('attendee_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'User',
			'plural' => 'Users',
			'parentType' => 'event',
			'browse_url' => 'users/--parentType--/--parentId--',
			'add_url' => 'users/add/--parentId--/--parentType--',
			'edit_url' => 'users/edit/--id--/--parentId--/--parentType--',
			'module_url' => 'module/editByController/users/--parentType--/--parentId--/0',
			'headers' => array(
				__('Name') => 'name',
				__('Login') => 'login',
				__('Email') => 'email'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'users/edit/--id--/--parentId--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'users/delete/--id--/--parentId--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_userlistview',
				'edit' => 'c_user_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			)
		);
	}
	
	function index() {
		
	}
	
	function event($id){ $this->_listUsers($id, 'event');}
	function venue($id){ $this->_listUsers($id, 'venue');}
	
	function _listUsers($id, $type = 'event'){
		//-- If attendee created from import, ask user to activate the attendee list module
		$attendee_created = ( $this->session->flashdata('user_create_attendee') != '' ) ? $this->session->flashdata('user_create_attendee') : '';
		$activate_module_url = '';

		if($type == 'venue'){
			if($id == FALSE || $id == 0) redirect('venues');
			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
		} elseif($type == 'event') {
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			if($attendee_created == 'Yes'){
				$launcherRow = $this->general_model->rowExists('launcher', array('eventid' => $id, 'moduletypeid' => 14), 1);
				if( count( $launcherRow ) == 0 || $launcherRow->active == 0 ){
					$activate_module_url = 'event/module/activate/' . $id . '/14';	
				}
			}
		} elseif($type == 'app') {
			$app = $this->app_model->get($id);
			_actionAllowed($app,'app');
		}
		
		/* Get all users of app */
		$appUsers = $this->users_model->getUserByAppId($app->id);

		$securedmodules = $this->users_model->securedmodules($app);

		#
		# Check for API call
		#
		$this->_handleAPIRequest($appUsers, $error);

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array($type, $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array($type, $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace(array('--parentType--','--parentId--'), array($type, $id), $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace(array('--parentType--','--parentId--'), array($type, $id), $delete_href);

		# Module import url
		$this->_module_settings->import_url = 'import/users/'.$type.'/'.$id;

		$listview = $this->_module_settings->views['list'];

		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $appUsers, 'activate_module_url'=>$activate_module_url, 'securedmodules' => $securedmodules, 'type' => $type, 'typeId' => $id), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		if($type == 'venue') {
			$cdata['crumb']		= checkBreadcrumbsVenue($app, array($venue->name => 'venue/view/'.$id, __('Users') => $this->uri->uri_string()));
		} elseif($type == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __('Users') => $this->uri->uri_string());
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, __('Users') => $this->uri->uri_string());
			}
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($type, $id, $app, 'users');
		
		$this->load->view('master', $cdata);	
	}
	
	function add($id, $type = 'event'){
		$attendees = array();
		$meetme = false;
		if($type == 'venue'){
			if($id == FALSE || $id == 0) redirect('venues');
			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
		} elseif($type == 'event') {
			if($id == FALSE || $id == 0) redirect('events');
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$attendees = $this->attendee_model->getAttendeesByEventID($event->id);
			$meetme = $this->users_model->CheckMeetMe($event, $id);
		} elseif($type == 'app') {
			$app = $this->app_model->get($id);
			_actionAllowed($app,'app');
		}
		
		$cancelBtnUrl = 'users/'.$type.'/'.$id;
		$error = '';

		$securedmodules = $this->users_model->securedmodules($app);
		$pincodes = $this->users_model->pincodes($type, $id);
		
		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			if($pincodes == 0) {
				$this->form_validation->set_rules('external_id', 'external_id', 'trim');
				$this->form_validation->set_rules('login', 'Login', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('repassword', 'Repassword', 'required');
			} else {
				$this->form_validation->set_rules('pincode', 'pincode', 'trim|required');
			}
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			
			if($this->form_validation->run() == FALSE) {
				$error = validation_errors();
			} elseif($pincodes == 0 && (md5($this->input->post('password')) != md5($this->input->post('repassword')))) {
				$error = __('Password mismatched.');
			} elseif(!$error){
	            $external_id = '';
	            if($this->input->post('external_id')) {
	            	$external_id = $this->input->post('external_id');
	            }
				if($pincodes == 0) {
					$userData = array(
						'external_id' 		=> $external_id,
						'name'				=> trim($this->input->post('name')),
						'login'				=> strtolower(trim($this->input->post('login'))),
						'password'			=> $this->input->post('password'),
						'email'				=> trim($this->input->post('email'))					
					);
				} else {
					$userData = array(
						'external_id' 		=> $external_id,
						'name'				=> trim($this->input->post('name')),
						'login'				=> trim($this->input->post('pincode')),
						'password'			=> $this->input->post('pincode'),
						'email'				=> trim($this->input->post('email'))					
					);
				}

				
				$modules =  '';
				if( $this->input->post('modules') )
					$modules = $this->input->post('modules');
				
				if($this->users_model->checkUser($app->id, $userData['login'], $type, $id)){
					$userId = $this->users_model->insertUser($app->id, $userData, $type, $id, $modules);
					if($userId) {
						if($this->input->post('attendee')) {
							$this->general_model->update('user', $userId, array('attendeeid' => $this->input->post('attendee')));
						}
				
						#
						# Check for API call
						#
						$this->_handleAPIRequest($userId, $error);
						
						$this->session->set_flashdata('event_feedback', __('The User has successfully been add!'));
						redirect($cancelBtnUrl);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}	
				} else {
					$error = __('Login already exists.');
				}
			}
		}
		
		$cdata['content'] 		= $this->load->view('c_user_add', array('error' => $error, 'cancelbtnurl' => $cancelBtnUrl, 'securedmodules' => $securedmodules, 'pincodes' => $pincodes, 'attendees' => $attendees, 'meetme' => $meetme), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		if($type == 'venue') {
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => 'venue/view/'.$id, __('Users') => 'users/venue/'.$id, __('Add User') => $this->uri->uri_string()));
		} elseif($type == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __('Users') => 'users/event/'.$id, __('Add User') => $this->uri->uri_string());
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, __('Users') => 'users/event/'.$id, __('Add User') => $this->uri->uri_string());
			}
		}
			
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($type, $id, $app, 'users');
		
		$this->load->view('master', $cdata);
	}
	
	function edit($userId, $id = '', $type = ''){
		$user = $this->users_model->getUserById($userId);
		if(empty($type)) {
			if($user->eventid != 0) {
				$type = 'event';
				$id = $user->eventid;
			} elseif($user->venueid != 0) {
				$type = 'venue';
				$id = $user->venueid;
			} elseif($user->appid != 0) {
				$type = 'app';
				$id = $user->appid;
			}
		}

		$pincodes = $this->users_model->pincodes($type, $id);
		
		$attendees = array();
		$meetme = false;
		if($type == 'venue'){
			if($id == FALSE || $id == 0) redirect('venues');
			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
		} elseif($type == 'event') {
			if($id == FALSE || $id == 0) redirect('events');
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$attendees = $this->attendee_model->getAttendeesByEventID($event->id);
			$meetme = $this->users_model->CheckMeetMe($event, $id);
		} elseif($type == 'app') {
			$app = $this->app_model->get($id);
			_actionAllowed($app,'app');
		}
		
		$cancelBtnUrl = 'users/'.$type.'/'.$id;
		$error = '';

		$securedmodules = $this->users_model->securedmodules($app);
		$usersecuredmoduleids = $this->users_model->getUserSecuredModuleIds($user);

		
		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('external_id', 'external_id', 'trim');
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'password', 'trim');
			if($pincodes != 0) {
				$this->form_validation->set_rules('pincode', 'pincode', 'trim|required');
			}

            $external_id = $user->external_id;
            if($this->input->post('external_id')) {
            	$external_id = $this->input->post('external_id');
            }
			
			if($this->form_validation->run() == FALSE) {
				$error = validation_errors();
			// } elseif($this->input->post('password') && ! $this->input->post('repassword')) {
			// 	$error = __('The Re. Password field is required.');
			// } elseif (($this->input->post('password') && $this->input->post('repassword')) && md5($this->input->post('password')) != md5( $this->input->post('repassword'))) {
			// 	$error = __('Password mismatched.');
			} elseif(!$error) {
				if($pincodes == 0) {
					$userData = array(
						'external_id' 		=> $external_id,
						'name'				=> trim($this->input->post('name')),
						'email'				=> trim($this->input->post('email'))				
					);
				} else {
					$userData = array(
						'external_id' 		=> $external_id,
						'name'				=> trim($this->input->post('name')),
						'login'				=> trim($this->input->post('pincode')),
						'password'			=> $this->input->post('pincode'),
						'email'				=> trim($this->input->post('email'))					
					);
				}
				
				if($this->input->post('password')) {
					$password = $this->input->post('password');
					if(!empty($password)) {
						$userData['password'] = $this->input->post('password');
					}
				}
					

				if($this->input->post('attendee')) {
					$this->general_model->update('user', $userId, array('attendeeid' => $this->input->post('attendee')));
				}
				
				$modules =  '';
				if( $this->input->post('modules') )
					$modules = $this->input->post('modules');
				
				$userId = $this->users_model->updateUser( $userId, $app->id, $userData, $type, $id, $modules );
				if($userId){
					#
					# Check for API call
					#
					$this->_handleAPIRequest($userId, $error);

					$this->session->set_flashdata('event_feedback', __('The User has successfully been update!'));
					redirect($cancelBtnUrl);
				}
				else {
					$error = __("Oops, something went wrong. Please try again.");
				}
			}
		}

		#
		# Check for API call
		#
		$this->_handleAPIRequest($user, $error);
		
		$cdata['content'] 		= $this->load->view('c_user_edit', array('error' => $error, 'cancelbtnurl' => $cancelBtnUrl, 'user' => $user, 'securedmodules' => $securedmodules, 'usersecuredmoduleids' => $usersecuredmoduleids, 'pincodes' => $pincodes, 'attendees' => $attendees, 'meetme' => $meetme), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		if($type == 'venue') {
			$cdata['crumb']		= checkBreadcrumbsVenue($app, array($venue->name => 'venue/view/'.$id, __('Users') =>'users/venue/'.$id, __('Edit User') => $this->uri->uri_string()));
		} elseif($type == 'event') {
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __('Users') => 'users/event/'.$id, __('Edit User') => $this->uri->uri_string());
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, __('Users') => 'users/event/'.$id, __('Edit User') => $this->uri->uri_string());
			}
		}
			
		$cdata['modules'] = $this->module_mdl->getModulesForMenu($type, $id, $app, 'users');
		
		$this->load->view('master', $cdata);
	
	}
	
	function delete($delId, $typeId = '', $type = ''){
		$user = $this->users_model->getUserById($delId);
		if(empty($type)) {
			if($user->eventid != 0) {
				$type = 'event';
				$typeId = $user->eventid;
			} elseif($user->venueid != 0) {
				$type = 'venue';
				$typeId = $user->venueid;
			} elseif($user->appid != 0) {
				$type = 'app';
				$typeId = $user->appid;
			}
		}

		if($type == 'venue'){
			$venue = $this->venue_model->getById($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
		} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
		} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app,'app');
		}
		
		if($this->users_model->removeUser($app->id, $delId)) {
			#
			# Check for API call
			#
			$this->_handleAPIRequest('success', $error);
			$this->session->set_flashdata('event_feedback', __('User has successfully been removed.'));
		} else {
			$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
		}
		
		redirect( 'users/' . $type . '/' . $typeId );
	}

	/**
	 * Handles API requests
	 *
	 * @param array $exhibitors
	 */
	protected function _handleAPIRequest( $data, $errors = null )
	{
		# Verify Request Accept header
		if($_SERVER['HTTP_ACCEPT'] != 'application/json') return;

		# Parse errors
		if(!empty($errors)) {
			echo json_encode(array('error' => strip_tags($errors)));
			exit;
		}

		# Cleanup the $exhibitors data
		$trash = array('mapid','y1','x1','x2','y2');

		# Verify empty image params && Trash bloated params
		$cleanup = function( &$exh ) use($trash)
		{

		};
		if(is_array($data)) array_walk($data, $cleanup); else $cleanup($data);

		# Output json encoded data
		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($data);
		exit;
	} 

    function linkmodules($type, $typeId){
        if($this->input->post('selectedids') && $this->input->post('launcherid')) {
            $launcher = $this->module_mdl->getLauncherById($this->input->post('launcherid'));
            if($type == 'venue'){
                $venue = $this->venue_model->getById($typeId);
                if($launcher->venueid != 0 && $launcher->venueid != $venue->id) {
                    return false;
                }
                $app = _actionAllowed($venue, 'venue','returnApp');
            } elseif($type == 'event') {
                $event = $this->event_model->getbyid($typeId);
                if($launcher->eventid != 0 && $launcher->eventid != $event->id) {
                    return false;
                }
                $app = _actionAllowed($event, 'event','returnApp');
            } elseif($type == 'app') {
                $app = $this->app_model->get($typeId);
                if($launcher->appid != 0 && $launcher->appid != $app->id) {
                    return false;
                }
                _actionAllowed($app,'app');
            }

            $appuserids = $this->users_model->getAppUserIds($app->id);
            
            foreach($this->input->post('selectedids') as $id) {
                if(isset($appuserids[$id])) {
                    $this->users_model->applySecuredModules($launcher->id, $id, $app->id);
                }
            }
        }
    }

    function linkmodulesall($type, $typeId){
        $launcher = $this->module_mdl->getLauncherById($this->input->post('launcherid'));
        if($type == 'venue'){
            $venue = $this->venue_model->getById($typeId);
            if($launcher->venueid != 0 && $launcher->venueid != $venue->id) {
                return false;
            }
            $app = _actionAllowed($venue, 'venue','returnApp');
        } elseif($type == 'event') {
            $event = $this->event_model->getbyid($typeId);
            if($launcher->eventid != 0 && $launcher->eventid != $event->id) {
                return false;
            }
            $app = _actionAllowed($event, 'event','returnApp');
        } elseif($type == 'app') {
            $app = $this->app_model->get($typeId);
            if($launcher->appid != 0 && $launcher->appid != $app->id) {
                return false;
            }
            _actionAllowed($app,'app');
        }

        $appuserids = $this->users_model->getAppUserIds($app->id);
        
        if(!empty($appuserids)) {
	        foreach($appuserids as $id) {
	            $this->users_model->applySecuredModules($launcher->id, $id, $app->id);
	        }
        }

    }
	
}
?>