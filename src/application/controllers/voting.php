<?php if(!defined('BASEPATH')) exit('No direct script access');

class Voting extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
	}
	
	//php 4 constructor
	function Voting() {
		parent::__construct();
	}
	
	function index() {
		$this->sign();
	}
	
	function session($sessionid = '') {
		
		// Presentatie, inhoud, overtuigingskracht
		
		$this->load->library('form_validation');
		if ($sessionid != '') {
			// INIT CONFIG
			$error = '';
			$voted = FALSE;
			$objsess = '';
				$session = $this->db->query("SELECT * FROM session WHERE id = '$sessionid' LIMIT 1");
				if ($session->num_rows() == 0) {
					$error = "Unknown session!";
				} else {
					$objsess = $session->row();
					if ($this->input->post('postback') == 'postback') {
						$this->form_validation->set_rules('speaker', 'speaker', 'trim|required');
						$this->form_validation->set_rules('inhoud', 'inhoud', 'trim|required');
						$this->form_validation->set_rules('relevance', 'relevance', 'trim|required');
						
						if($this->form_validation->run() == FALSE) {
							$error = "Please make sure all fields are filled in correctly.";
						} else {
							$votingdata = array(
								"sessionid"	=> $sessionid,
								"inhoud" 	=> set_value('inhoud'),
								"speaker" 	=> set_value('speaker'),
								"relevance" => set_value('relevance')
							);
							
							if($this->db->insert('voting', $votingdata)){
								$this->session->set_flashdata('feedback_voting','Thanks for voting!');
								redirect("voting/session/".$phoneid."/".$sessionid."/");
							} else {
								$error = "Validation failed, please try again?";
							}
						}
					}
				}
			// }
			
			$this->load->view('voting/master', array('session' => $objsess, 'error' => $error, 'voted' => $voted));
		} else {
			die("INVALID AUTHENTICATION");
		}
	}
	
}