<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Sessions extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('sessions_model');
		$this->load->model('artist_model');
		$this->load->model('speaker_model');
		$this->load->model('confbag_model');
		$this->load->model('metadata_model');
		$this->load->model('premium_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Session',
			'plural' => 'Sessions',
			'title' => __('Sessions'),
			'parentType' => 'event',
			'browse_url' => 'sessions/--parentType--/--parentId--',
			'add_url' => 'sessions/add/--parentId--/--parentType--',
			'edit_url' => 'sessions/edit/--id--',
			'module_url' => 'module/editByController/sessions/--parentType--/--parentId--/0',
			'import_url' => 'import/sessions_speakers/--parentId--',
			'headers' => array(
				__('Name') => 'name',
				__('Order') => 'order'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'sessions/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'sessions/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'filter' => array(
				'id' => 'sessgroup',
				'class' => 'groupselect',
				'name' => 'sessgroup',
				'label' => __('All sessiongroups'),
				'selected' => '',
				'data' => array(),
				'url' => 'sessions/--parentType--/--parentId--'
			),			
			'filtermanage' => array(
				'class' => 'edit btn',
				'title' => __('Edit Sessiongroups'),
				'url' => 'sessiongroups/--parentType--/--parentId--'
			),
			'tabs' => array(

			),
			'checkboxes' => false,
			'deletecheckedurl' => 'sessions/removemany/--parentId--/--parentType--',
			'premium' => 'session'
		);
	}

	function index() {
		$this->event();
	}

	function event($id, $groupid = '') {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		// Get Sessions for event per group
		if($groupid != ''){
			$sessions = $this->sessions_model->getSessionByGroups($groupid);
		} else {
			$sessions = $this->sessions_model->getSessionByEventID($id);
		}
		$sessgroups = $this->sessions_model->getSessiongroupsByEventID($id);

		$this->iframeurl = $this->sessions_model->getIFrameUrl($id, $app);

		$this->_module_settings->parentId = $id;
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# Module import url
		$import_url =& $this->_module_settings->import_url;
		$import_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $import_url);
		# filter sessiongroups
		$filter =& $this->_module_settings->filter;
		$filter['data'] = $sessgroups;
		$filter['url'] = str_replace(array('--parentType--','--parentId--'), array('event', $id), $filter['url']);
		$filter['selected'] = $groupid;
		$filtermanage =& $this->_module_settings->filtermanage;
		$filtermanage['url'] = str_replace(array('--parentType--','--parentId--'), array('event', $id), $filtermanage['url']);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		$launcher = $this->module_mdl->getLauncher(10, 'event', $event->id);
		if($launcher->newLauncherItem) {
			$launcher->title = __($launcher->title);
		}
		$this->_module_settings->title = $launcher->title;

		$tabs =& $this->_module_settings->tabs;
		$tabs[] = (object)array(
				'active' => true,
				'url' => 'sessions/event/'.$id,
				'title' => __('Sessions')
			);
		$tabs[] = (object)array(
				'active' => false,
				'url' => 'speakers/event/'.$id,
				'title' => __('Speakers')
			);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($sessions)), true);

		// $cdata['content'] 		= $this->load->view('c_listview_sessions', array('event' => $event, 'data' => $sessions, 'sessgroups' => $sessgroups, 'headers' => $headers, 'headersSpeakers' => $headers, 'speakers' => $speakers, 'app' => $app), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']	= array($event->name => "event/view/".$id, $this->_module_settings->title => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'sessions');
		$this->load->view('master', $cdata);
	}

	function venue($id, $groupid = '') {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		// Get Sessions for event per group
		if($groupid != ''){
			$sessions = $this->sessions_model->getSessionByGroups($groupid);
		} else {
			$sessions = $this->sessions_model->getSessionByVenueID($id);
		}
		$sessgroups = $this->sessions_model->getSessiongroupsByVenueID($id);

		$headers = array('Name' => 'name', 'Order' => 'order');

		$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'data' => $sessions, 'sessgroups' => $sessgroups, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Sessions") => $this->uri->uri_string()));
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'sessions');
		$this->load->view('master', $cdata);
	}

	function add($id, $type = "event") {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('sessgroup', 'sessgroup', 'trim|required');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
                }
				$this->form_validation->set_rules('startdate', 'startdate', 'trim');
				$this->form_validation->set_rules('starttime', 'starttime', 'trim');
				$this->form_validation->set_rules('enddate', 'enddate', 'trim');
				$this->form_validation->set_rules('endtime', 'endtime', 'trim');
				$this->form_validation->set_rules('speaker', 'speaker', 'trim');
                $this->form_validation->set_rules('url', 'url', 'trim');
				$this->form_validation->set_rules('location', 'location', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('host', 'host', 'trim');
				$this->form_validation->set_rules('mytagsulselect', 'mytagsulselect', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					$data = array(
							"sessiongroupid"	=> set_value('sessgroup'),
							"name"				=> set_value('name_'. $app->defaultlanguage),
							"description"		=> set_value('description_'.  $app->defaultlanguage),
							"starttime"			=> date('Y-m-d', strtotime(set_value('startdate'))) . " " . date('H:i', strtotime(set_value('starttime'))),
							"endtime"			=> date('Y-m-d', strtotime(set_value('enddate'))) . " " . date('H:i', strtotime(set_value('endtime'))),
							"speaker"			=> set_value('speaker'),
							"location"			=> set_value('location'),
							"order"				=> set_value('order'),
                            "url"               => checkhttp($this->input->post('url')),
							"organizer"			=> set_value('host')
						);

					if($newid = $this->general_model->insert('session', $data)){
						// *** SAVE TAGS *** //
						foreach ($this->input->post('mytagsulselect') as $tag) {
							$tags = array(
									'appid' 	=> $app->id,
									'tag' 		=> $tag,
									'sessionid' => $newid
								);
							$this->general_model->insert('tag', $tags);
						}
						// *** //
                        //add translated fields to translation table
                        foreach($languages as $language) {
                            $this->language_model->addTranslation('session', $newid, 'name', $language->key, $this->input->post('name_'.$language->key));
                            $this->language_model->addTranslation('session', $newid, 'description', $language->key, $this->input->post('description_'.$language->key));
                        }

                        //metadata
                        $meta = array(
                        		'twitter' => $this->input->post('twitter'),
                        		'facebook' => $this->input->post('facebook'),
                        		'youtube' => $this->input->post('youtube')
                        	);
                        $this->metadata_model->insertArray($meta, $app->id, 'session', $newid);

						$this->session->set_flashdata('event_feedback', __('The session has successfully been add!'));
						_updateVenueTimeStamp($venue->id);
						redirect('sessions/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$this->load->model('exhibitor_model');
			$exhibitors = $this->exhibitor_model->getExhibitorsByVenueID($venue->id);
			$groups = $this->sessions_model->getSessionGroupsByVenue($venue->id);

			$cdata['content'] 		= $this->load->view('c_session_add', array('venue' => $venue, 'sessgroups' => $groups, 'exhibitors' => $exhibitors, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Sessions") => "sessions/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');

			$this->iframeurl = $this->sessions_model->getIFrameUrl($id, $app);
			$languages = $this->language_model->getLanguagesOfApp($app->id);

            //for football event get artists of own team
            if($app->apptypeid == 6) {
                $artists = $this->artist_model->getArtistsByAppID($app->id);
            } else {
                $artists = array();
            }

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'session' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			if($this->input->post('myspeakersulselect') != null) {
				$postedspeakers = $this->input->post('myspeakersulselect');
			} else {
				$postedspeakers = array();
			}

			$eventspeakers = $this->speaker_model->getByEventId($event->id);

			$launcher = $this->module_mdl->getLauncher(10, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, '', '', $app);

			$premiumtitle = $this->premium_model->getTitle('session', 'event', $event->id);

			$parentsessions = $this->sessions_model->getSessionsByParentId($event->id, 0);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('sessgroup', 'sessgroup', 'trim');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description', 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
                $this->form_validation->set_rules('external_id', 'external_id', 'trim');
				$this->form_validation->set_rules('startdate', 'startdate', 'trim');
				$this->form_validation->set_rules('starttime', 'starttime', 'trim');
				$this->form_validation->set_rules('enddate', 'enddate', 'trim');
				$this->form_validation->set_rules('endtime', 'endtime', 'trim');
				$this->form_validation->set_rules('speaker', 'speaker', 'trim');
                $this->form_validation->set_rules('url', 'url', 'trim');
				$this->form_validation->set_rules('location', 'location', 'trim');
				if($app->apptypeid == 10) {
					$this->form_validation->set_rules('twitter', 'twitter', 'trim');
					$this->form_validation->set_rules('facebook', 'facebook', 'trim');
					$this->form_validation->set_rules('youtube', 'youtube', 'trim');
				}
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('host', 'host', 'trim');
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
				$this->form_validation->set_rules('nameselect', 'nameselect', 'trim');
				$this->form_validation->set_rules('mytagsulselect[]', 'mytagsulselect[]', 'trim');
				$this->form_validation->set_rules('confbagcontent', 'confbagcontent', 'trim');
                $this->form_validation->set_rules('premium', 'premium', 'trim');
                $this->form_validation->set_rules('order', 'order', 'trim');
                $this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
                $this->form_validation->set_rules('premiumtitle', 'premiumtitle', 'trim');
                $this->form_validation->set_rules('parentid', 'parent session', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
					$startdate = strtotime($this->input->post('startdate'));
					$enddate = strtotime($this->input->post('enddate'));
					$starttime = strtotime($this->input->post('starttime'));
					$endtime = strtotime($this->input->post('endtime'));

					if($enddate < $startdate) {
						$error .= __("<p>End date must be greater than start date.</p>");
					} elseif($enddate == $startdate) {
						if($endtime < $starttime) {
							$error .= __("<p>End time must be greater than start time.</p>");
						}
					}
				} else {
					$startdate = strtotime($this->input->post('startdate'));
					$enddate = strtotime($this->input->post('enddate'));
					$starttime = strtotime($this->input->post('starttime'));
					$endtime = strtotime($this->input->post('endtime'));

					if($enddate < $startdate) {
						$error .= __('<p>End date must be greater than start date.</p>');
					} elseif($enddate == $startdate) {
						if($endtime < $starttime) {
							$error .= __('<p>End time must be greater than start time.</p>');
						}
					}

		            $external_id = '';
		            if($this->input->post('external_id')) {
		            	$external_id = $this->input->post('external_id');
		            }

					if($error == '') {
						$sessgroup = $this->sessions_model->getSessiongroupByName($event->id, $this->input->post('sessgroup'));

						if($sessgroup != '') {
	                        if($app->apptypeid == 6) {
	                            if($this->input->post('nameselect') != null && $this->input->post('nameselect') != '') {
	                            $artist = $this->artist_model->getArtistByID($this->input->post('nameselect'));
	                            }
	                        }

	                        if($this->input->post('sessionsameasartist')) {
	                        	if($this->input->post('nameselect') != null && $this->input->post('nameselect') != '') {
	                            $artist = $this->artist_model->getArtistByID($this->input->post('nameselect'));
	                            $name = $artist->name;
	                            } else {
	                            	$name = set_value('name_'. $app->defaultlanguage);
	                            }
	                        } else {
	                            $name = set_value('name_'. $app->defaultlanguage);
	                        }

							$startdateSQL = '0000-00-00 ';
							if($this->input->post('startdate')) {
								$startdateSQL = date('Y-m-d', strtotime($this->input->post('startdate')));
							} 
							$enddateSQL = '0000-00-00 ';
							if($this->input->post('enddate')) {
								$enddateSQL = date('Y-m-d', strtotime($this->input->post('enddate')));
							}
							$starttimeSQL = '00:00:00';
							if($this->input->post('starttime')) {
								$starttimeSQL = date('H:i', strtotime($this->input->post('starttime')));
							} 
							$endtimeSQL = '00:00:00';
							if($this->input->post('endtime')) {
								$endtimeSQL = date('H:i', strtotime($this->input->post('endtime')));
							}

							$data = array(
									'external_id' 		=> $external_id,
									"sessiongroupid"	=> $sessgroup,
									"eventid"			=> $id,
									"name"              => ($app->apptypeid == 6 && $this->input->post('nameselect') != null && $this->input->post('nameselect') != '' ) ? $artist->name : $name,
									"description"		=> set_value('description_'.  $app->defaultlanguage),
									"starttime"			=> $startdateSQL . " " . $starttimeSQL,
									"endtime"			=> $enddateSQL . " " . $endtimeSQL,
									"speaker"			=> set_value('speaker'),
	                                "url"               => checkhttp($this->input->post('url')),
									"location"			=> set_value('location'),
									"order"				=> set_value('order'),
									"organizer"			=> set_value('host'),
									"parentid"			=> $this->input->post('parentid')
								);

							if($newid = $this->general_model->insert('session', $data)){
								//Uploads Images
								if(!is_dir($this->config->item('imagespath') . "upload/sessionimages/".$newid)){
									mkdir($this->config->item('imagespath') . "upload/sessionimages/".$newid, 0755, true);
								}

								$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/sessionimages/'.$newid;
								$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
								$configexlogo['max_width']  = '2000';
								$configexlogo['max_height']  = '2000';
								$this->load->library('upload', $configexlogo);
								$this->upload->initialize($configexlogo);
								if (!$this->upload->do_upload('imageurl')) {
									$image = ""; //No image uploaded!
									if($_FILES['imageurl']['name'] != '') {
										$error .= $this->upload->display_errors();
									}
								} else {
									//successfully uploaded
									$returndata = $this->upload->data();
									$image = 'upload/sessionimages/'. $newid . '/' . $returndata['file_name'];
								}

								$this->general_model->update('session', $newid, array('imageurl' => $image));

								if(!is_dir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id)){
									mkdir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id, 0755, TRUE);
								}
								$configconfbag['upload_path'] = $this->config->item('imagespath') .'upload/confbagfiles/event/'.$event->id;
								$configconfbag['allowed_types'] = 'JPG|jpg|jpeg|png|pdf|xls|xlsx|txt|doc|docx|ppt|pptx';
								$configconfbag['max_size'] = '50000';
								$this->load->library('upload', $configconfbag);

								$this->upload->initialize($configconfbag);
								if ($this->upload->do_upload('confbagcontent')) {
									$returndata = $this->upload->data();
									$confbagcontent = 'upload/confbagfiles/event/'.$event->id.'/'.$returndata['file_name'];
									$this->general_model->insert('confbag', array('eventid' => $event->id, 'itemtable' => 'session', 'tableid' => $newid, 'documentlink' => $confbagcontent));
								} else {
									if($_FILES['confbagcontent']['name'] != '') {
										$error .= $this->upload->display_errors();
									}
								}

								//for football event insert artist session record
	                            if($app->apptypeid == 6 || $this->input->post('nameselect') != null && $this->input->post('nameselect') != 0 ) {
	                                $artistsession = array(
	                                        'artistid' 	=> $this->input->post('nameselect'),
	                                        'sessionid' => $newid
	                                    );
	                                $this->db->insert('artistsessions', $artistsession);
	                            }
								// *** SAVE TAGS *** //
								$tags = $this->input->post('mytagsulselect');
								foreach ($tags as $tag) {
									$this->general_model->insert('tc_tag', array(
										'appid' => $app->id,
										'itemtype' => 'session',
										'itemid' => $newid,
										'tag' => $tag
									));
								}

								//save speakers
								$speakers = $this->input->post('myspeakersulselect');
								$this->speaker_model->insertSpeakers($speakers, $event->id, $newid);

								//save premium
								$this->premium_model->save($this->input->post('premium'), 'session', $newid, $this->input->post('extraline'), false, 'event', $event->id, $this->input->post('premiumorder'), $this->input->post('premiumtitle'));

	                            //add translated fields to translation table
	                            foreach($languages as $language) {
	                                //description
									if($this->input->post('sessionsameasartist')) {
										if($this->input->post('nameselect') != null && $this->input->post('nameselect') != '') {
										$artist = $this->artist_model->getArtistByID($this->input->post('nameselect'));
										$name = $artist->name;
										} else {
											$name = $this->input->post('name_'.$language->key);
										}
									} else {
										$name = $this->input->post('name_'.$language->key);
									}
									$this->language_model->addTranslation('session', $newid, 'name', $language->key, $name);
	                                $this->language_model->addTranslation('session', $newid, 'description', $language->key, $this->input->post('description_'.$language->key));
									foreach($metadata as $m) {
										if(_checkMultilang($m, $language->key, $app)) {
											$postfield = _getPostedField($m, $_POST, $_FILES, $language);
											if($postfield !== false) {
												if(_validateInputField($m, $postfield)) {
													_saveInputField($m, $postfield, 'session', $newid, $app, $language);
												}
											}
										}
									}
	                            }

		                        //metadata
		                       	if($app->apptypeid == 10) {
			                        $meta = array(
			                        		'twitter' => $this->input->post('twitter'),
			                        		'facebook' => $this->input->post('facebook'),
			                        		'youtube' => $this->input->post('youtube')
			                        	);
			                        $this->metadata_model->insertArray($meta, $app->id, 'session', $newid);
			                    }

								$this->session->set_flashdata('event_feedback', __('The session has successfully been add!'));
								_updateTimeStamp($event->id);
								if($error == '') {
									redirect('sessions/event/'.$event->id);
								} else {
									//image error
									redirect('sessions/edit/'.$newid.'/event?error=image');
								}
							} else {
								$error = __("Oops, something went wrong. Please try again.");
							}
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}

				}
			}

			$this->load->model('exhibitor_model');
			$exhibitors = $this->exhibitor_model->getExhibitorsByEventID($event->id);
			$groups = $this->sessions_model->getSessionGroupsByEvent($event->id);
			$artists = $this->artist_model->getArtistsByAppID($app->id);

			//confbag
			$confbagactive = $this->confbag_model->checkActive($event->id);

			$cdata['content'] 		= $this->load->view('c_session_add', array('event' => $event, 'sessgroups' => $groups, 'exhibitors' => $exhibitors, 'error' => $error, 'languages' => $languages, 'app' => $app, 'artists' => $artists, 'apptags' => $apptags, 'eventspeakers' => $eventspeakers, 'postedtags' => $postedtags, 'postedspeakers' => $postedspeakers, 'confbagactive' => $confbagactive, 'metadata' => $metadata, 'premiumtitle' => $premiumtitle, 'parentsessions' => $parentsessions), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "sessions/event/".$event->id, __("Add new") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "sessions/event/".$event->id, __("Add new") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'sessions');
			$this->load->view('master', $cdata);
		}
	}

	function image_rules($str){
		$filename = 'image';
		return image_check($filename, $_FILES);
	}

	function edit($id, $type = "event") {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			// EDIT SESSION
			$session = $this->sessions_model->getSessionByID($id);
			if($session == FALSE) redirect('venues');

			$venue = $this->venue_model->getById($session->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tag WHERE appid = $app->id AND sessionid <> 0 GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT tag FROM tag WHERE sessionid = $id");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag->tag;
				}
				$tags = $tagz;
			}		

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('sessgroup', 'sessgroup', 'trim');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
                }
                $this->form_validation->set_rules('external_id', 'external_id', 'trim');
				$this->form_validation->set_rules('startdate', 'startdate', 'trim');
				$this->form_validation->set_rules('starttime', 'starttime', 'trim');
				$this->form_validation->set_rules('enddate', 'enddate', 'trim');
				$this->form_validation->set_rules('endtime', 'endtime', 'trim');
				$this->form_validation->set_rules('speaker', 'speaker', 'trim');
                $this->form_validation->set_rules('url', 'url', 'trim');
				$this->form_validation->set_rules('location', 'location', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('host', 'host', 'trim');


				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array(
							"sessiongroupid"	=> set_value('sessgroup'),
							"name"				=> set_value('name_'.  $app->defaultlanguage),
							"description"		=> set_value('description_'.  $app->defaultlanguage),
							"starttime"			=> date('Y-m-d', strtotime(set_value('startdate'))) . " " . date('H:i', strtotime(set_value('starttime'))),
							"endtime"			=> date('Y-m-d', strtotime(set_value('enddate'))) . " " . date('H:i', strtotime(set_value('endtime'))),
							"speaker"			=> set_value('speaker'),
                            "url"               => checkhttp($this->input->post('url')),
							"location"			=> set_value('location'),
							"order"				=> set_value('order'),
							"organizer"			=> set_value('host')
						);

					if($this->sessions_model->edit_session($id, $data)){
						// *** SAVE TAGS *** //
						if(!empty($id)) {
							$this->db->where('sessionid', $id);
							$this->db->where('appid', $app->id);
							if($this->db->delete('tag')){
								if ($this->input->post('mytagsulselect') != '' && count($this->input->post('mytagsulselect')) > 0) {
									foreach ($this->input->post('mytagsulselect') as $tag) {
										$tags = array(
												'appid' 	=> $app->id,
												'tag' 		=> $tag,
												'sessionid' => $id
											);
										$this->db->insert('tag', $tags);
									}
								}
							}
						}
						// *** //

                        //add translated fields to translation table
                        foreach($languages as $language) {
                            $nameSuccess = $this->language_model->updateTranslation('session', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
                            if(!$nameSuccess) {
								$this->language_model->addTranslation('session', $id, 'name', $language->key, $this->input->post('name_'.$language->key));
							}
							$descrSuccess = $this->language_model->updateTranslation('session', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							if(!$descrSuccess) {
								$this->language_model->addTranslation('session', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
							}
						}

						$this->session->set_flashdata('event_feedback', __('The session has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						redirect('sessions/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$this->load->model('exhibitor_model');
			$exhibitors = $this->exhibitor_model->getExhibitorsByVenueID($venue->id);
			$groups = $this->sessions_model->getSessionGroupsByVenue($venue->id);

			$cdata['content'] 		= $this->load->view('c_session_edit', array('venue' => $venue, 'session' => $session, 'sessgroups' => $groups, 'exhibitors' => $exhibitors, 'tags' => $tags, 'error' => $error, 'languages' => $languages, 'app' => $app, 'apptags' => $apptags), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Sessions") => "sessions/venue/".$venue->id, __("Edit: ") . $session->name => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			// EDIT SESSION
			$session = $this->sessions_model->getSessionByID($id);
			if($session == FALSE) redirect('events');

			$event = $this->event_model->getbyid($session->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			$premium = $this->premium_model->get('session', $session->id);
			$premiumtitle = $this->premium_model->getTitle('session', 'event', $event->id);

			// TAGS
			$apptags = $this->db->query("SELECT tag FROM tc_tag WHERE appid = $app->id AND itemtype = 'session' GROUP BY tag");
			if($apptags->num_rows() == 0) {
				$apptags = array();
			} else {
				$apptags = $apptags->result();
			}
			$tags = $this->db->query("SELECT * FROM tc_tag WHERE itemid = $id AND itemtype = 'session'");
			if($tags->num_rows() == 0) {
				$tags = array();
			} else {
				$tagz = array();
				foreach ($tags->result() as $tag) {
					$tagz[] = $tag;
				}
				$tags = $tagz;
			}
			//validation
			if($this->input->post('mytagsulselect') != null) {
				$postedtags = $this->input->post('mytagsulselect');
			} else {
				$postedtags = array();
			}

			if($this->input->post('myspeakersulselect') != null) {
				$postedspeakers = $this->input->post('myspeakersulselect');
			} else {
				$postedspeakers = array();
			}
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$eventspeakers = $this->speaker_model->getByEventId($event->id);
			$speakers = $this->speaker_model->getBySessionId($id);

			$this->iframeurl = 'sessions/view/'.$id.'?r=1&width=247';

			$launcher = $this->module_mdl->getLauncher(10, 'event', $event->id);
			if($launcher->newLauncherItem) {
				$launcher->title = __($launcher->title);
			}
			$this->_module_settings->title = $launcher->title;
			$metadata = $this->metadata_model->getMetadata($launcher, 'session', $session->id, $app);

			if($app->apptypeid == 10) {
				$sessionmeta = $this->metadata_model->fetchAssoc($app->id, 'session', $session->id);
				foreach($sessionmeta as $key => $value) {
					$session->{$key} = $value->value;
				}
			}

			$parentsessions = $this->sessions_model->getSessionsByParentId($event->id, 0);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('sessgroup', 'sessgroup', 'trim');
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                    $this->form_validation->set_rules('description_'.$language->key, 'description_'.$language->key, 'trim');
					foreach($metadata as $m) {
						if(_checkMultilang($m, $language->key, $app)) {
							foreach(_setRules($m, $language) as $rule) {
								$this->form_validation->set_rules($rule->qname, $rule->name, $rule->validation);
							}
						}
					}
                }
				$this->form_validation->set_rules('startdate', 'startdate', 'trim');
				$this->form_validation->set_rules('starttime', 'starttime', 'trim');
				$this->form_validation->set_rules('enddate', 'enddate', 'trim');
				$this->form_validation->set_rules('endtime', 'endtime', 'trim');
				$this->form_validation->set_rules('speaker', 'speaker', 'trim');
                $this->form_validation->set_rules('url', 'url', 'trim');
				$this->form_validation->set_rules('location', 'location', 'trim');
				if($app->apptypeid == 10) {
					$this->form_validation->set_rules('twitter', 'twitter', 'trim');
					$this->form_validation->set_rules('facebook', 'facebook', 'trim');
					$this->form_validation->set_rules('youtube', 'youtube', 'trim');
				}
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('host', 'host', 'trim');
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');
				$this->form_validation->set_rules('confbagcontent', 'confbagcontent', 'trim');
				$this->form_validation->set_rules('premium', 'premium', 'trim');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('premiumorder', 'premiumorder', 'trim');
				$this->form_validation->set_rules('premiumtitle', 'premiumtitle', 'trim');
				$this->form_validation->set_rules('parentid', 'parent session', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
					$startdate = strtotime($this->input->post('startdate'));
					$enddate = strtotime($this->input->post('enddate'));
					$starttime = strtotime($this->input->post('starttime'));
					$endtime = strtotime($this->input->post('endtime'));

					if($enddate < $startdate) {
						$error .= __("<p>End date must be greater than start date.</p>");
					} elseif($enddate == $startdate) {
						if($endtime < $starttime) {
							$error .= __("<p>End time must be greater than start time.</p>");
						}
					}
				} else {
					$startdate = strtotime($this->input->post('startdate'));
					$enddate = strtotime($this->input->post('enddate'));
					$starttime = strtotime($this->input->post('starttime'));
					$endtime = strtotime($this->input->post('endtime'));

					if($enddate < $startdate) {
						$error .= __('<p>End date must be greater than start date.</p>');
					} elseif($enddate == $startdate) {
						if($endtime < $starttime) {
							$error .= __('<p>End time must be greater than start time.</p>');
						}
					}

					if($error == '') {
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/sessionimages/".$id)){
						mkdir($this->config->item('imagespath') . "upload/sessionimages/".$id, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/sessionimages/'.$id;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('imageurl')) {
						$image = $session->imageurl; //No image uploaded!
						if($_FILES['imageurl']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/sessionimages/'. $id . '/' . $returndata['file_name'];
					}

					if(!is_dir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id)){
						mkdir($this->config->item('imagespath') . "upload/confbagfiles/event/".$event->id, 0755, TRUE);
					}

					$configconfbag['upload_path'] = $this->config->item('imagespath') .'upload/confbagfiles/event/'.$event->id;
					$configconfbag['allowed_types'] = 'docx|jpg|jpeg|png|pdf|xls|xlsx|txt|doc|ppt|pptx';
					$configconfbag['max_size'] = '50000';
					$this->load->library('upload', $configconfbag);

					$this->upload->initialize($configconfbag);
					if ($this->upload->do_upload('confbagcontent')) {
						$returndata = $this->upload->data();
						$confbagcontent = 'upload/confbagfiles/event/'.$event->id.'/'.$returndata['file_name'];
						$this->general_model->insert('confbag', array('eventid' => $event->id, 'itemtable' => 'session', 'tableid' => $id, 'documentlink' => $confbagcontent));
					} else {
						if($_FILES['confbagcontent']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					}

					if($this->form_validation->run() == FALSE || $error != ''){
						$error .= validation_errors();
					}else {
						if($this->input->post('sessionsameasartist')) {
							if($this->input->post('nameselect') != null && $this->input->post('nameselect') != '') {
							$artist = $this->artist_model->getArtistByID($this->input->post('nameselect'));
							$name = $artist->name;
							} else {
								$name = set_value('name_'. $app->defaultlanguage);
							}
						} else {
							$name = set_value('name_'. $app->defaultlanguage);
						}

						$startdateSQL = '0000-00-00';
						if($this->input->post('startdate')) {
							$startdateSQL = date('Y-m-d', strtotime($this->input->post('startdate')));
						} 
						$enddateSQL = '0000-00-00';
						if($this->input->post('enddate')) {
							$enddateSQL = date('Y-m-d', strtotime($this->input->post('enddate')));
						}
						$starttimeSQL = '00:00:00';
						if($this->input->post('starttime')) {
							$starttimeSQL = $this->input->post('starttime') . ':00';
						} 
						$endtimeSQL = '00:00:00';
						if($this->input->post('endtime')) {
							$endtimeSQL = $this->input->post('endtime') . ':00';
						}

			            $external_id = $session->external_id;
			            if($this->input->post('external_id')) {
			            	$external_id = $this->input->post('external_id');
			            }
						$sessgroup = $this->sessions_model->getSessiongroupByName($event->id, $this->input->post('sessgroup'));
						$data = array(
								'external_id' 		=> $external_id,
								"sessiongroupid"	=> $sessgroup,
								"eventid"			=> $event->id,
								"name"				=> $name,
								"description"		=> set_value('description_'.  $app->defaultlanguage),
								"starttime"			=> $startdateSQL . " " . $starttimeSQL,
								"endtime"			=> $enddateSQL . " " . $endtimeSQL,
								"speaker"			=> set_value('speaker'),
	                            "url"               => checkhttp($this->input->post('url')),
								"location"			=> set_value('location'),
								"order"				=> set_value('order'),
								"imageurl" 			=> $image,
								"organizer"			=> set_value('host'),
								'parentid'			=> $this->input->post('parentid')
							);

						if($this->sessions_model->edit_session($id, $data)){
							// *** SAVE TAGS *** //
							$this->db->where('itemtype', 'session');
							$this->db->where('itemid', $id);
							$this->db->delete('tc_tag');
							$tags = $this->input->post('mytagsulselect');
							foreach ($tags as $tag) {
								$this->general_model->insert('tc_tag', array(
									'appid' => $app->id,
									'itemtype' => 'session',
									'itemid' => $id,
									'tag' => $tag
								));
							}
							//save speakers
							$speakers = $this->input->post('myspeakersulselect');
							$this->speaker_model->insertSpeakers($speakers, $event->id, $id);

							//save premium
							$this->premium_model->save($this->input->post('premium'), 'session', $session->id, $this->input->post('extraline'), $premium, 'event', $event->id, $this->input->post('premiumorder'), $this->input->post('premiumtitle'));

	                        //add translated fields to translation table
	                        foreach($languages as $language) {
	                            //description
								if($this->input->post('sessionsameasartist')) {
									if($this->input->post('nameselect') != null && $this->input->post('nameselect') != '') {
										$artist = $this->artist_model->getArtistByID($this->input->post('nameselect'));
										$name = $artist->name;
									} else {
										$name = $this->input->post('name_'.$language->key);
									}
								} else {
									$name = $this->input->post('name_'.$language->key);
								}
	                            $nameSuccess = $this->language_model->updateTranslation('session', $id, 'name', $language->key, $name);
								if($nameSuccess == false || $nameSuccess == null) {
									$this->language_model->addTranslation('session', $id, 'name', $language->key, $name);
								}
								$descrSuccess = $this->language_model->updateTranslation('session', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
								if($descrSuccess == false || $descrSuccess == null) {
									$this->language_model->addTranslation('session', $id, 'description', $language->key, $this->input->post('description_'.$language->key));
								}
								foreach($metadata as $m) {
									if(_checkMultilang($m, $language->key, $app)) {
										$postfield = _getPostedField($m, $_POST, $_FILES, $language);
										if($postfield !== false) {
											if(_validateInputField($m, $postfield)) {
												_saveInputField($m, $postfield, 'session', $id, $app, $language);
											}
										}
									}
								}
							}

	                        //metadata
	                        if($app->apptypeid == 10) {
		                        $meta = array(
		                        		'twitter' => $this->input->post('twitter'),
		                        		'facebook' => $this->input->post('facebook'),
		                        		'youtube' => $this->input->post('youtube')
		                        	);
		                        $this->metadata_model->insertArray($meta, $app->id, 'session', $id);
		                    }

							$this->session->set_flashdata('event_feedback', __('The session has successfully been updated'));
							_updateTimeStamp($event->id);
							redirect('sessions/event/'.$event->id);
						} else {
							$error = __("Oops, something went wrong. Please try again.");
						}
					}
				}
				}
			}
			$this->load->model('exhibitor_model');
			$exhibitors = $this->exhibitor_model->getExhibitorsByEventID($event->id);
			$groups = $this->sessions_model->getSessionGroupsByEvent($event->id);
			$artists = $this->artist_model->getArtistsByAppID($app->id);

			//confbag
			$confbagactive = $this->confbag_model->checkActive($event->id);
			$sessionconfbag = false;
			if($confbagactive) {
				$sessionconfbag = $this->confbag_model->getFromObject('session',$session->id);
			}

			$cdata['content'] 		= $this->load->view('c_session_edit', array('event' => $event, 'session' => $session, 'sessgroups' => $groups, 'exhibitors' => $exhibitors, 'tags' => $tags, 'error' => $error, 'languages' => $languages, 'app' => $app, 'artists' => $artists, 'apptags' => $apptags, 'eventspeakers' => $eventspeakers, 'speakers' => $speakers, 'postedtags' => $postedtags, 'postedspeakers' => $postedspeakers, 'confbagactive' => $confbagactive, 'sessionconfbag' => $sessionconfbag, 'premium' => $premium, 'metadata' => $metadata, 'premiumtitle' => $premiumtitle, 'parentsessions' => $parentsessions), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, $this->_module_settings->title => "sessions/event/".$event->id, __("Edit: ") . $session->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, $this->_module_settings->title => "sessions/event/".$event->id, __("Edit: ") . $session->name => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'sessions');
			$this->load->view('master', $cdata);
		}
	}

	function delete($id, $type = "event") {
		if($type == "venue") {
			$session = $this->sessions_model->getSessionByID($id);
            $this->language_model->removeTranslations('session', $id);
			if($this->general_model->remove('session', $id)){
				$this->session->set_flashdata('event_feedback', __('The session has successfully been deleted'));
				_updateVenueTimeStamp($session->venueid);
				redirect('sessions/venue/'.$session->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('sessions/venue/'.$session->venueid);
			}
		} else {
			$session = $this->sessions_model->getSessionByID($id);
			$event = $this->event_model->getbyid($session->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
            $this->language_model->removeTranslations('session', $id);
			if($this->general_model->remove('session', $id)){
				if($session->imageurl != '') {
					$this->deleteimage($session->imageurl);
				}
				$this->sessions_model->removePremium($id);

				$this->session->set_flashdata('event_feedback', __('The session has successfully been deleted'));
				_updateTimeStamp($session->eventid);
				redirect('sessions/event/'.$session->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('sessions/event/'.$session->eventid);
			}
		}
	}

	function deleteimage($imageurl) {
		// unlink file itself
		unlink($imageurl);
		// unlink cache-files
		$info = pathinfo($imageurl);
		$file_name =  basename($imageurl,'.'.$info['extension']);
		array_map("unlink", glob("../cache/*".$file_name));
	}

	function sort($type = '') {
		if($type == '' || $type == FALSE) redirect('events');

		switch($type){
			case "sessions":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('session', $data);
					$this->db->last_query();
				}
				break;
			case "groups":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('sessiongroup', $data);
					$this->db->last_query();
				}
				break;
			default:
				break;
		}
	}

	function autocomplete($field) {
		//In controller
		$keyword = $this->input->post('term');

		$data['response'] = 'false'; //Set default response

		$query = _autocomplete('session', $field, $keyword);

		if(count($query) > 0){
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach($query as $row){
				$data['message'][] = array('label'=> $row->{$field}, 'value'=> $row->{$field}); //Add a row to array
			}
		}
		echo json_encode($data);
	}

	function autocompletetags() {
		//In controller
		$keyword = $this->input->post('term');

		$data['response'] = 'false'; //Set default response

		$query = _autocompletetags(_currentApp()->id, $keyword);
		$keys = array();
		if(count($query) > 0){
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach($query as $row){
				if(!in_array($row->tag,$keys)) {
					$data['message'][] = array('label'=> $row->tag, 'value'=> $row->tag); //Add a row to array
					$keys[] = $row->tag;
				}
			}
		}

		echo json_encode($data);
	}

	function removeimage($id, $type = 'event') {
		$item = $this->sessions_model->getSessionByID($id);
		if($type == 'venue') {
			$venue = $this->venue_model->getbyid($item->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->imageurl)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('session', $id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('sessions/venue/'.$venue->id);
		} else {
			$event = $this->event_model->getbyid($item->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->imageurl)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->imageurl);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('imageurl' => '');
			$this->general_model->update('session', $id, $data);

			_updateTimeStamp($event->id);
			redirect('sessions/event/'.$event->id);
		}
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'session');
    }   
}