<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Module extends CI_Controller {

	protected $app;
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('module_mdl');
		$this->load->model('metadata_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Custom field',
			'plural' => 'Custom fields',
			'browse_url' => 'module/fields/--launcherid--/--parentType--/--parentId--',
			'add_url' => 'module/fieldadd/--launcherid--/--parentType--/--parentId--',
			'edit_url' => 'module/fieldedit/--id--',
			'module_url' => 'module/edit/--moduletypeid--/--parentType--/--parentId--',
			'headers' => array(
				__('Name') => 'name',
				__('Order') => 'sortorder',
				__('Type') => 'type',
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'module/fieldedit/--id--/--parentType--/--parentId--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'module/fielddelete/--id--/--parentType--/--parentId--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_customfield_edit',
				'read' => 'tc_displayview',
				'add' => 'tc_customfield_edit'
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'launcher' => null,
			'checkboxes' => false,
			'deletecheckedurl' => 'module/removemanyfields/--launcherid--/--parentId--/--parentType--'
		);
	}

	function editByController($controller, $type, $typeid, $launcherid = 0) {
		if($launcherid != 0 && ($controller == 'forms' || $controller == 'formscreens' || $controller == 'formfields' || $controller == 'groups')) {
			redirect('module/editlauncher/'.$launcherid.'/'.$type.'/'.$typeid);
		}

		if($controller == 'groups' && $launcherid == 0) {
			if($type == 'venue') {
				redirect('module/edit/15/'.$type.'/'.$typeid);
			} elseif($type == 'event') {
				redirect('module/edit/2/'.$type.'/'.$typeid);
			}
		}

		$moduletypes = $this->module_mdl->getModuletypes();
		foreach($moduletypes as $m) {
			if($m->controller == $controller) {
				redirect('module/edit/'.$m->id.'/'.$type.'/'.$typeid);
			}
		}
	}

	function edit($id, $type, $typeid) {
		$module = $this->module_mdl->getById($id);
		$error = '';
		$this->load->library('form_validation');
		if($type == 'venue') {
			$this->load->model('venue_model');
			$venue = $this->venue_model->getById($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
		} elseif($type == 'event') {
			$this->load->model('event_model');
			$event = $this->event_model->getById($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
		} elseif($type == 'app') {
			$app = $this->app_model->get($typeid);
			_actionAllowed($app, 'app');
		}
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$stayloggedin = '';
		$securedmodules = array();
		$activeModules = array();
		if($id == 49) {
			$this->load->model('users_model');
			$stayloggedin = $this->users_model->stayloggedin($type, $typeid);
			$securedmodules = $this->users_model->securedmodules($app);
			$pincodes = $this->users_model->pincodes($type, $typeid);
		}

		if($id == 69) {
			$this->load->model('quiz_model');
			$this->load->model('forms_model');
			$launcher = $this->module_mdl->getLauncher($id, $type, $typeid);
			$quiz = $this->quiz_model->getByLauncherId($launcher->id);
			$quizforms = $this->forms_model->getFormsByAppId($app->id, false);
		}

		if($type == 'venue') {
			$launcher = $this->module_mdl->getLauncher($id, 'venue', $typeid);

			$activeModules = $this->module_mdl->getActiveLaunchers($venue, 'venue');

			if($this->input->post('postback') == "postback") {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('order', 'order', 'trim|required');

				$module = $this->module_mdl->getById($id);

				$this->form_validation->set_rules('image', 'image', 'trim');

				if($launcher->moduletype == 24 || $launcher->moduletypeid == 25 || $launcher->moduletypeid == 27) {
					$this->form_validation->set_rules('displaytype', 'displaytype', 'trim');
				}

				$displaytype = $launcher->displaytype;
				if($this->input->post('displaytype') != null && $this->input->post('displaytype') != '') {
					$displaytype = $this->input->post('displaytype');
				}

				$data = array(
					'venueid'		=> $typeid,
					'moduletypeid'	=> $id,
					'module'		=> $module->name,
					'title'			=> $this->input->post('title_'.  $app->defaultlanguage),
					'order'			=> $this->input->post('order'),
					'displaytype'	=> $displaytype
				);

				if($id == 49) {
					$this->form_validation->set_rules('stayloggedin', 'stay logged in', 'trim');
					$this->users_model->saveStayLoggedIn($app->id, $this->input->post('stayloggedin'));
					$this->users_model->saveUsermodule($app, $this->input->post('securedmodules'));
					$this->users_model->savePincodes($app->id, $this->input->post('pincodes'));
				}

				if($id == 69) {
					$this->form_validation->set_rules('numberofquestionspergame', 'number of questions', 'trim');
					$this->form_validation->set_rules('facebooksharescore', 'facebook share', 'trim');
					$this->form_validation->set_rules('defaultscorepercorrectanswer', 'default score per answer', 'trim');
					$this->form_validation->set_rules('sendresultsbyemail_formid', 'result form', 'trim');
					$this->quiz_model->saveQuizData($app->id, $launcher->id, $quiz, $this->input->post('numberofquestionspergame'), $this->input->post('facebooksharescore'), $this->input->post('defaultscorepercorrectanswer'), $this->input->post('sendresultsbyemail_formid'));
				}

				if($launcher->newLauncherItem == true) {
					if($newid = $this->general_model->insert('launcher', $data)) {
		                foreach($languages as $language) {
		                    $this->language_model->addTranslation('launcher', $newid, 'title', $language->key, $this->input->post('title_'.$language->key));
		                }
					//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$newid)){
							mkdir($this->config->item('imagespath') . "upload/moduleimages/".$newid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$newid;
						$configexlogo['allowed_types'] = 'png';
						$configexlogo['max_width']  = '200';
						$configexlogo['max_height']  = '200';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = $launcher->icon; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error = $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/moduleimages/'. $newid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('launcher', $newid, array('icon' => $image));
						$this->session->set_flashdata('event_feedback', __('The module settings have successfully been changed!'));
						_updateVenueTimeStamp($venue->id);
						if($error == '') {
							redirect('venue/view/'.$venue->id);
						}
					}else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				} else {
					if($this->general_model->update('launcher', $launcher->id, $data)){
		                foreach($languages as $language) {
		                    $this->language_model->addTranslation('launcher', $launcher->id, 'title', $language->key, $this->input->post('title_'.$language->key));
		                }
					//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcher->id)){
							mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcher->id, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcher->id;
						$configexlogo['allowed_types'] = 'png';
						$configexlogo['max_width']  = '200';
						$configexlogo['max_height']  = '200';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = $launcher->icon; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error = $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/moduleimages/'. $launcher->id . '/' . $returndata['file_name'];
						}

						$this->general_model->update('launcher', $launcher->id, array('icon' => $image));
						$this->session->set_flashdata('event_feedback', __('The module settings have successfully been changed!'));
						_updateVenueTimeStamp($venue->id);
						if($error == '') {
							redirect('venue/view/'.$venue->id);
						}
					}else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_module_edit', array('app' => $app, 'venue' => $venue, 'error' => $error, 'launcher' => $launcher, 'languages'=>$languages, 'parentType' => 'venue', 'parentId' => $venue->id, 'stayloggedin' => $stayloggedin, 'securedmodules' => $securedmodules, 'activeModules' => $activeModules, 'quiz' => $quiz, 'quizforms' => $quizforms, 'pincodes' => $pincodes), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Module") => "module/edit/".$module->id.'/venue/'.$venue->id));
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, $launcher->controller);
			$this->load->view('master', $cdata);
		} elseif($type == 'event') {
			$launcher = $this->module_mdl->getLauncher($id, 'event', $typeid);

			$activeModules = $this->module_mdl->getActiveLaunchers($event, 'event');

			if($this->input->post('postback') == "postback") {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('order', 'order', 'trim|required');
				if($launcher->moduletype == 10) {
					$this->form_validation->set_rules('displaytype', 'displaytype', 'trim');
				}

				$module = $this->module_mdl->getById($id);

				$this->form_validation->set_rules('image', 'image', 'trim');

				$displaytype = $launcher->displaytype;
				if($this->input->post('displaytype') != null && $this->input->post('displaytype') != '') {
					$displaytype = $this->input->post('displaytype');
				}

				$data = array(
					'eventid'		=> $typeid,
					'moduletypeid'	=> $id,
					'module'		=> $module->name,
					'title'			=> $this->input->post('title_'. $app->defaultlanguage),
					'order'			=> $this->input->post('order'),
					'displaytype'	=> $displaytype
				);

				if($id == 49) {
					$this->form_validation->set_rules('stayloggedin', 'stay logged in', 'trim');
					$this->users_model->saveStayLoggedIn($app->id, $this->input->post('stayloggedin'));
					$this->users_model->saveUsermodule($app, $this->input->post('securedmodules'));
					$this->users_model->savePincodes($app->id, $this->input->post('pincodes'));
				}

				if($id == 69) {
					$this->form_validation->set_rules('numberofquestionspergame', 'number of questions', 'trim');
					$this->form_validation->set_rules('facebooksharescore', 'facebook share', 'trim');
					$this->form_validation->set_rules('defaultscorepercorrectanswer', 'default score per answer', 'trim');
					$this->form_validation->set_rules('sendresultsbyemail_formid', 'result form', 'trim');
					$this->quiz_model->saveQuizData($app->id, $launcher->id, $quiz, $this->input->post('numberofquestionspergame'), $this->input->post('facebooksharescore'), $this->input->post('defaultscorepercorrectanswer'), $this->input->post('sendresultsbyemail_formid'));
				}

				//watermark for fb photos festival
				if($id == 39) {
					if(!is_dir($this->config->item('imagespath') . "upload/appimages/".$app->id)){
						mkdir($this->config->item('imagespath') . "upload/appimages/".$app->id, 0774);
					}

					$configexwatermark['upload_path'] = $this->config->item('imagespath') .'upload/appimages/'.$app->id;
					$configexwatermark['allowed_types'] = 'png';
					$configexwatermark['max_width']  = '612';
					$configexwatermark['max_height']  = '612';
					$configexwatermark['file_name']  = 'watermark.png';
					$this->load->library('upload', $configexwatermark);

					if (!$this->upload->do_upload('watermark')) {
						$watermark = '';
						if($_FILES['watermark']['name'] != '') {
							$error = $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$watermark = 'upload/appimages/'. $app->id . '/' . $returndata['file_name'];

						$watermarkdata = array(
							"appid"		=> $app->id,
							"controlid"	=> 54,
							"value"		=> $watermark
						);
						$watermarkexists = $this->db->query("SELECT id FROM appearance WHERE appid = $app->id AND controlid = 54");
						if($watermarkexists->num_rows() == 0) {
							$this->general_model->insert('appearance', $watermarkdata);
						} else {
							$this->general_model->update('appearance', $watermarkexists->row()->id, $watermarkdata);
						}

					}
				}

				if($launcher->newLauncherItem == true) {
					if($newid = $this->general_model->insert('launcher', $data)) {
		                foreach($languages as $language) {
		                    $this->language_model->addTranslation('launcher', $newid, 'title', $language->key, $this->input->post('title_'.$language->key));
		                }
						//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$newid)){
							mkdir($this->config->item('imagespath') . "upload/moduleimages/".$newid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$newid;
						$configexlogo['allowed_types'] = 'png';
						$configexlogo['max_width']  = '200';
						$configexlogo['max_height']  = '200';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = $launcher->icon;//No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error = $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/moduleimages/'. $newid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('launcher', $newid, array('icon' => $image));
						$this->session->set_flashdata('event_feedback', __('The module settings have successfully been changed!'));
						_updateTimeStamp($event->id);
						if($error == '') {
							redirect('event/view/'.$event->id);
						}
					}else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				} else {
					if($this->general_model->update('launcher', $launcher->id, $data)){
		                foreach($languages as $language) {
		                    $this->language_model->addTranslation('launcher', $launcher->id, 'title', $language->key, $this->input->post('title_'.$language->key));
		                }
					//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcher->id)){
							mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcher->id, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcher->id;
						$configexlogo['allowed_types'] = 'png';
						$configexlogo['max_width']  = '200';
						$configexlogo['max_height']  = '200';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = $launcher->icon; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error = $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/moduleimages/'. $launcher->id . '/' . $returndata['file_name'];
						}

						$this->general_model->update('launcher', $launcher->id, array('icon' => $image));
						$this->session->set_flashdata('event_feedback', __('The module settings have successfully been changed!'));
						_updateTimeStamp($event->id);
						if($error == '') {
							redirect('event/view/'.$event->id);
						}
					}else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_module_edit', array('app' => $app, 'event' => $event, 'error' => $error, 'launcher' => $launcher, 'languages'=>$languages, 'parentType' => 'event', 'parentId' => $event->id, 'stayloggedin' => $stayloggedin, 'securedmodules' => $securedmodules, 'activeModules' => $activeModules, 'quiz' => $quiz, 'quizforms' => $quizforms, 'pincodes' => $pincodes), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']			= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Module") => "module/edit/".$event->id.'/event/'.$event->id);
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']			= array($event->name => "event/view/".$event->id, __("Module") => "module/edit/".$event->id.'/event/'.$event->id);
			}
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, $launcher->controller);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		} elseif($type == 'app') {
			$launcher = $this->module_mdl->getLauncher($id, 'app', $typeid);
			$activeModules = $this->module_mdl->getActiveLaunchers($app, 'app');

			if($this->input->post('postback') == "postback") {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('order', 'order', 'trim|required');

				$module = $this->module_mdl->getById($id);

				$this->form_validation->set_rules('image', 'image', 'trim');

				$data = array(
					'appid'			=> $typeid,
					'moduletypeid'	=> $id,
					'module'		=> $module->name,
					'title'			=> $this->input->post('title_'.  $app->defaultlanguage),
					'order'			=> $this->input->post('order'),
				);

				if($id == 49) {
					$this->form_validation->set_rules('stayloggedin', 'stay logged in', 'trim');
					$this->users_model->saveStayLoggedIn($app->id, $this->input->post('stayloggedin'));
					$this->users_model->saveUsermodule($app, $this->input->post('securedmodules'));
					$this->users_model->savePincodes($app->id, $this->input->post('pincodes'));
				}

				if($id == 69) {
					$this->form_validation->set_rules('numberofquestionspergame', 'number of questions', 'trim');
					$this->form_validation->set_rules('facebooksharescore', 'facebook share', 'trim');
					$this->form_validation->set_rules('defaultscorepercorrectanswer', 'default score per answer', 'trim');
					$this->form_validation->set_rules('sendresultsbyemail_formid', 'result form', 'trim');
					$this->quiz_model->saveQuizData($app->id, $launcher->id, $quiz, $this->input->post('numberofquestionspergame'), $this->input->post('facebooksharescore'), $this->input->post('defaultscorepercorrectanswer'), $this->input->post('sendresultsbyemail_formid'));
				}

				if($launcher->newLauncherItem == true) {
					if($newid = $this->general_model->insert('launcher', $data)) {
		                foreach($languages as $language) {
		                    $this->language_model->addTranslation('launcher', $newid, 'title', $language->key, $this->input->post('title_'.$language->key));
		                }
					//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$newid)){
							mkdir($this->config->item('imagespath') . "upload/moduleimages/".$newid, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$newid;
						$configexlogo['allowed_types'] = 'png';
						$configexlogo['max_width']  = '200';
						$configexlogo['max_height']  = '200';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = $launcher->icon; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error = $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/moduleimages/'. $newid . '/' . $returndata['file_name'];
						}

						$this->general_model->update('launcher', $newid, array('icon' => $image));
						$this->session->set_flashdata('event_feedback', __('The module settings have successfully been changed!'));
						if($error == '') {
							redirect('apps/view/'.$typeid);
						}
					}else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				} else {
					if($this->general_model->update('launcher', $launcher->id, $data)){
		                foreach($languages as $language) {
		                    $this->language_model->addTranslation('launcher', $launcher->id, 'title', $language->key, $this->input->post('title_'.$language->key));
		                }
					//Uploads Images
						if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcher->id)){
							mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcher->id, 0755, true);
						}

						$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcher->id;
						$configexlogo['allowed_types'] = 'png';
						$configexlogo['max_width']  = '200';
						$configexlogo['max_height']  = '200';
						$this->load->library('upload', $configexlogo);
						$this->upload->initialize($configexlogo);
						if (!$this->upload->do_upload('image')) {
							$image = $launcher->icon; //No image uploaded!
							if($_FILES['image']['name'] != '') {
								$error = $this->upload->display_errors();
							}
						} else {
							//successfully uploaded
							$returndata = $this->upload->data();
							$image = 'upload/moduleimages/'. $launcher->id . '/' . $returndata['file_name'];
						}

						$this->general_model->update('launcher', $launcher->id, array('icon' => $image));
						$this->session->set_flashdata('event_feedback', __('The module settings have successfully been changed!'));
						if($error == '') {
							redirect('apps/view/'.$typeid);
						}
					}else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_module_edit', array('app' => $app, 'error' => $error, 'launcher' => $launcher, 'languages'=>$languages, 'parentType' => 'app', 'parentId' => $app->id, 'stayloggedin' => $stayloggedin, 'securedmodules' => $securedmodules, 'activeModules' => $activeModules, 'quiz' => $quiz, 'quizforms' => $quizforms, 'pincodes' => $pincodes), TRUE);
			$cdata['crumb']			= array(_currentApp()->name => "apps/view/".$typeid, __("Module") => "module/edit/".$module->id.'/app/'.$typeid);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => _currentApp()), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('app', $app->id, $app, $launcher->controller);
			$this->load->view('master', $cdata);
		}
	}

    function deletesource($sourceid) {
        $source = $this->news_model->getSourceOfId($sourceid);
        $this->news_model->removeNewsOfSourceId($sourceid);

        if($this->general_model->remove('newssource', $sourceid)){
            if($source->venueid != '' && $source->venueid != '0') {
                $this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
                _updateVenueTimeStamp($source->venueid);
                redirect('news/venue/'.$source->venueid);
            } else {
                $this->session->set_flashdata('event_feedback', __('The newsitem has successfully been deleted'));
                _updateTimeStamp($source->eventid);
                redirect('news/event/'.$source->eventid);
            }
        } else {
            if($source->venueid != ''  && $source->venueid != '0') {
                $this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
                redirect('news/venue/'.$source->venueid);
            }
            else {
                $this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
                redirect('news/event/'.$source->eventid);
            }
        }
    }

	function removeimage($id, $type = 'event', $typeid) {
		if($type == 'venue') {
			$item = $this->module_mdl->getLauncher($id, 'venue', $typeid);
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->icon)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->icon);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('icon' => '');
			$this->general_model->update('launcher', $item->id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('venue/view/'.$venue->id);
		} elseif($type == 'event') {
			$item = $this->module_mdl->getLauncher($id, 'event', $typeid);
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->icon)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->icon);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('icon' => '');
			$this->general_model->update('launcher', $item->id, $data);

			_updateTimeStamp($event->id);
			redirect('event/view/'.$event->id);
		} elseif($type == 'app') {
			$item = $this->module_mdl->getLauncher($id, 'app', $typeid);

			if(!file_exists($this->config->item('imagespath') . $item->icon)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->icon);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('icon' => '');
			$this->general_model->update('launcher', $item->id, $data);

			redirect('apps/view/'.$typeid);
		}
	}

	function removelauncherimage($id, $type = 'event', $typeid) {
		if($type == 'venue') {
			$item = $this->module_mdl->getLauncherById($id);
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->icon)) redirect('venues');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->icon);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('icon' => '');
			$this->general_model->update('launcher', $item->id, $data);

			_updateVenueTimeStamp($venue->id);
			redirect('venue/view/'.$venue->id);
		} elseif($type == 'event') {
			$item = $this->module_mdl->getLauncherById($id);
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');

			if(!file_exists($this->config->item('imagespath') . $item->icon)) redirect('events');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->icon);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('icon' => '');
			$this->general_model->update('launcher', $item->id, $data);

			_updateTimeStamp($event->id);
			redirect('event/view/'.$event->id);
		} elseif($type == 'app') {
			$item = $this->module_mdl->getLauncherById($id);

			if(!file_exists($this->config->item('imagespath') . $item->icon)) redirect('apps');

			// Delete image + generated thumbs
			unlink($this->config->item('imagespath') . $item->icon);
			$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

			$data = array('icon' => '');
			$this->general_model->update('launcher', $item->id, $data);

			redirect('apps/view/'.$typeid);
		}
	}

	function removewatermark($id, $type = 'event', $typeid) {
		$event = $this->event_model->getbyid($typeid);
		$app = _actionAllowed($event, 'event','returnApp');

		if(!file_exists($this->config->item('imagespath') .'upload/appimages/'.$app->id.'/watermark.png')) redirect('events');

		// Delete image + generated thumbs
		unlink($this->config->item('imagespath') .'upload/appimages/'.$app->id.'/watermark.png');
		$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

		_updateTimeStamp($event->id);
		redirect('event/view/'.$event->id);
	}

	function reset($id, $type, $typeid) {
		$launcher = $this->module_mdl->getLauncherById($id);
		if($launcher) {
			$moduletypeid = $launcher->moduletypeid;
			if($type == 'venue') {
				if($launcher->venueid != $typeid) redirect('apps');
				$this->load->model('venue_model');
				$venueid = $launcher->venueid;
				$venue = $this->venue_model->getById($venueid);
				$app = _actionAllowed($venue, 'venue','returnApp');

				$this->general_model->remove('launcher', $id);
			} elseif($type == 'event') {
				if($launcher->eventid != $typeid) redirect('apps');
				$this->load->model('event_model');
				$event = $this->event_model->getbyid($launcher->eventid);
				$app = _actionAllowed($event, 'event','returnApp');

				$this->general_model->remove('launcher', $id);
			} elseif($type == 'app') {
				if($launcher->appid != $typeid) redirect('apps');
				$app = $this->app_model->get($launcher->appid);
				_actionAllowed($app, 'app');

				$this->general_model->remove('launcher', $id);
			}
			redirect('module/edit/'.$moduletypeid.'/'.$type.'/'.$typeid);
		} else {
			redirect('apps');
		}
	}

	function activatelauncher($moduletypeid, $type = 'event', $typeid) {
		if($type == 'app') {
			$moduleactive = $this->db->query("SELECT * FROM module WHERE app = $typeid AND moduletype = $moduletypeid LIMIT 1");
			if($moduleactive->num_rows() == 0) {
				$moduledata = array(
						'moduletype' => $moduletypeid,
						'app'	=> $typeid
					);
				$this->general_model->insert('module', $moduledata);
			}
			$launcherexists = $this->db->query("SELECT * FROM launcher WHERE appid = $typeid AND moduletypeid = $moduletypeid LIMIT 1");
			if($launcherexists->num_rows() == 0) {
				$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = $moduletypeid LIMIT 1")->row();

				$launcherdata = array(
					'moduletypeid'	=> $moduletypeid,
					'appid'			=> $typeid,
					'module'		=> $defaultlauncher->module,
					'title'			=> $defaultlauncher->title,
					'icon'			=> $defaultlauncher->icon,
					'order'			=> $defaultlauncher->order,
					'active'		=> 1
					);

				$this->general_model->insert('launcher',$launcherdata);
				redirect('apps/view/'.$typeid);
			} else {
				$launcher = $launcherexists->row();

				$data = array(
						'active'	=> 1
					);

				$this->general_model->update('launcher', $launcher->id, $data);
				redirect('apps/view/'.$typeid);
			}
		}
	}

	function deactivatelauncher($moduletypeid, $type = 'event', $typeid) {
		if($type == 'app') {
			$this->db->query("DELETE FROM module WHERE app = $typeid AND moduletype = $moduletypeid");

			$launcherexists = $this->db->query("SELECT * FROM launcher WHERE appid = $typeid AND moduletypeid = $moduletypeid LIMIT 1");
			if($launcherexists->num_rows() == 0) {
				$defaultlauncher = $this->db->query("SELECT * FROM defaultlauncher WHERE moduletypeid = $moduletypeid LIMIT 1")->row();

				$launcherdata = array(
					'moduletypeid'	=> $moduletypeid,
					'appid'			=> $typeid,
					'module'		=> $defaultlauncher->module,
					'title'			=> $defaultlauncher->title,
					'icon'			=> $defaultlauncher->icon,
					'order'			=> $defaultlauncher->order,
					'active'		=> 0
					);

				$this->general_model->insert('launcher',$launcherdata);
				redirect('apps/view/'.$typeid);
			} else {
			$launcher = $launcherexists->row();

			$data = array(
					'active'	=> 0
				);

			$this->general_model->update('launcher', $launcher->id, $data);
			$this->general_model->update('app', $typeid, array('timestamp' => time()));
			redirect('apps/view/'.$typeid);
			}
		}
	}

	function deactivatelauncherbyid($id, $type = 'event', $typeid) {
		if($type == 'app') {
			$launcherexists = $this->db->query("SELECT * FROM launcher WHERE id = $id LIMIT 1");
			if($launcherexists->num_rows() == 0) {
				redirect('apps/view/'.$typeid);
			} else {
			$launcher = $launcherexists->row();

			$data = array(
					'active'	=> 0
				);

			$this->general_model->update('launcher', $launcher->id, $data);

			if($launcher->moduletypeid == 1 || $launcher->moduletypeid == 29 || $launcher->moduletypeid == 28) {
				$this->db->query("DELETE FROM module WHERE app = $typeid AND moduletype = $launcher->moduletypeid");
			}
			$this->general_model->update('app', $typeid, array('timestamp' => time()));
			redirect('apps/view/'.$typeid);
			}
		}
		redirect('apps/view/'.$typeid);
	}

	function activatelauncherById($id, $type = 'event', $typeid) {
		$launcherexists = $this->db->query("SELECT * FROM launcher WHERE id = $id LIMIT 1");
		if($launcherexists->num_rows() != 0) {
			$launcher = $launcherexists->row();

			$data = array(
					'active'	=> 1
				);

			$this->general_model->update('launcher', $id, $data);
		}

		if($launcher->moduletypeid == 1 || $launcher->moduletypeid == 29 || $launcher->moduletypeid == 28) {
			$this->general_model->insert('module', array('app' => $typeid, 'moduletype' => $launcher->moduletypeid));
		}
		$this->general_model->update('app', $typeid, array('timestamp' => time()));
		redirect('apps/view/'.$typeid);
	}

	function editLauncher($launcherid) {
		$launcher = $this->module_mdl->getLauncherById($launcherid);
		$error = '';
		$this->load->library('form_validation');
		$app = _currentApp();
		$languages = $this->language_model->getLanguagesOfApp($app->id);

		if($launcher->venueid != 0) {
			$parentType = 'venue';
			$parentId = $launcher->venueid;
		} elseif($launcher->eventid != 0) {
			$parentType = 'event';
			$parentId = $launcher->eventid;
		} elseif($launcher->appid != 0) {
			$parentType = 'app';
			$parentId = $launcher->appid;
		}

		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
            }
			$this->form_validation->set_rules('order', 'order', 'trim|required');
			$this->form_validation->set_rules('image', 'image', 'trim');
			$this->form_validation->set_rules('url', 'url', 'trim');

			$displaytype = $launcher->displaytype;
			if($this->input->post('displaytype') != null && $this->input->post('displaytype') != '') {
				$displaytype = $this->input->post('displaytype');
			}

			$title = $this->input->post('title_'.  $app->defaultlanguage);
			$data = array(
				'title'			=> $title,
				'order'			=> $this->input->post('order'),
				'displaytype' 	=> $displaytype,
				'tag' 			=> $title,
				'url'			=> $this->input->post('url')
			);

			$tags = $this->db->query("UPDATE tag SET tag = ? WHERE tag = ? AND appid = $app->id", array($title, $launcher->tag));


			$this->general_model->update('launcher', $launcherid, $data);

			if($launcher->groupid != 0) {
				$this->general_model->update('group', $launcher->groupid, array('displaytype' => $displaytype));
			}

            foreach($languages as $language) {
                $this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
            }
			//Uploads Images
			if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
				mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0775, true);
			}

			$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
			$configexlogo['allowed_types'] = 'png';
			$configexlogo['max_width']  = '200';
			$configexlogo['max_height']  = '200';
			$this->load->library('upload', $configexlogo);
			$this->upload->initialize($configexlogo);
			if (!$this->upload->do_upload('image')) {
				$image = $launcher->icon; //No image uploaded!
				$error = $this->upload->display_errors();
			} else {
				//successfully uploaded
				$returndata = $this->upload->data();
				$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
			}
			$this->general_model->update('app', $app->id, array('timestamp' => time()));
			$this->general_model->update('launcher', $launcherid, array('icon' => $image));
			$this->session->set_flashdata('event_feedback', 'The module settings have successfully been changed!');
			if($launcher->venueid != 0) {
				redirect('venue/view/'.$launcher->venueid);
			}
			redirect('apps/view/'.$app->id);
		}

		$cdata['content'] 		= $this->load->view('c_module_edit', array('app' => $app, 'error' => $error, 'launcher' => $launcher, 'languages'=>$languages, 'parentType' => $parentType, 'parentId' => $parentId), TRUE);
		$cdata['crumb']			= array($app->name => "apps/view/".$app->id, $launcher->title => "module/editLauncher/".$launcherid);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
		$this->load->view('master', $cdata);
	}

	function addlauncher($type, $typeid) {
		$error = '';
		$this->load->library('form_validation');
		if($type == 'app') {
			$app = $this->app_model->get($typeid);
			_actionAllowed($app, 'app');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			if($this->input->post('postback') == "postback") {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('title_'.$language->key, 'title_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('order', 'order', 'trim|required');
				$this->form_validation->set_rules('image', 'image', 'trim');

				$icon = '';
				$url = '';
				if($app->familyid == 3) {
					$extragetparams = 0;
					if($this->input->post('eventsorvenues') == 'events') {
						$moduletypeid = 30;
						$module = 'eventTags';
						$icon = 'l_events';
					} elseif($this->input->post('eventsorvenues') == 'venues') {
						$moduletypeid = 31;
						$module = 'venueTags';
						$icon = 'l_venues';
					} elseif($this->input->post('eventsorvenues') == 'website') {
						$moduletypeid = 0;
						$module = 'dynamic';
						$icon = 'l_catalog';
						if($this->input->post('extraparams') == 'on') {
							$extragetparams = 1;
						}
						$url = $this->input->post('url');
						if(substr($url,0,7) != 'http://' && substr($url,0,8) != 'https://') {
							$url = 'http://'.$url;
						}
					}
				}

				$data = array(
					'appid'			=> $app->id,
					'title'			=> $this->input->post('title_'.  $app->defaultlanguage),
					'order'			=> $this->input->post('order'),
					'moduletypeid'	=> $moduletypeid,
					'module'		=> $module,
					'icon'			=> $icon,
					'url'			=> $url,
					'tag'			=> str_replace(' ', '',$this->input->post('title_'.  $app->defaultlanguage)),
					'active'		=> 1,
					'extragetparams' => $extragetparams
				);

				$launcherid = $this->general_model->insert('launcher', $data);
				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid)){
					mkdir($this->config->item('imagespath') . "upload/moduleimages/".$launcherid, 0755, true);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/moduleimages/'.$launcherid;
				$configexlogo['allowed_types'] = 'png';
				$configexlogo['max_width']  = '200';
				$configexlogo['max_height']  = '200';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('image')) {
					$image = $icon; //No image uploaded!
					if($_FILES['image']['name'] != '') {
						$error = $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/moduleimages/'. $launcherid . '/' . $returndata['file_name'];
				}

				$this->general_model->update('launcher', $launcherid, array('icon' => $image));

	            foreach($languages as $language) {
	                $this->language_model->addTranslation('launcher', $launcherid, 'title', $language->key, $this->input->post('title_'.$language->key));
	            }
	            $this->general_model->update('app', $app->id, array('timestamp' => time()));
	            if($error =='') {
					$this->session->set_flashdata('event_feedback', __('The module settings have successfully been changed!'));
					redirect('apps/view/'.$app->id);
	            }
	        }

			$cdata['content'] 		= $this->load->view('c_launcher_add', array('app' => $app, 'error' => $error, 'languages'=>$languages), TRUE);
			$cdata['crumb']			= array(__('New Module') => $this->uri->uri_string());
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('app' => $app), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function fields($launcherid, $parentType, $parentId) {
		$parent = $this->_getParentObject( $parentType, $parentId );
		$launcher = $this->module_mdl->getLauncherById($launcherid);

		$listview = $this->_module_settings->views['list'];

		$fields = $this->metadata_model->getFieldsByParent('launcher', $launcher->id);

		$this->_module_settings->add_url = 'module/fieldadd/'.$launcher->id.'/'.$parentType.'/'.$parentId;
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--moduletypeid--','--parentType--','--parentId--'), array($launcher->moduletypeid, $parentType, $parentId), $module_url);

		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace(array('--parentType--','--parentId--'), array($parentType, $parentId), $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace(array('--parentType--','--parentId--'), array($parentType, $parentId), $delete_href);

		if($parentType == 'app') {
			$crumb = array($launcher->title => 'module/edit/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Custom fields') => $this->uri->uri_string());
		} else {
			$crumb = array($parent->name => "$parentType/view/".$parentId, $launcher->title => 'module/edit/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Custom fields') => $this->uri->uri_string());
		}
		$cdata = array(
			'content' => $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => array_reverse($fields)), true),
			'sidebar' => $this->load->view('c_sidebar', array($parentType => $parent), true),
			'crumb' => $crumb,
			'modules' => $this->module_mdl->getModulesForMenu($parentType, $parentId, $this->app, ''),
		);
		if($parent->name == 'app') {
			$cdata['crumb'] = array($launcher->title => 'module/edit/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Custom fields') => 'module/fields/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Add new') => $this->uri->uri_string());
		}
		$this->load->view('master', $cdata);
	}

	function fieldadd($launcherid, $parentType, $parentId) {
		$parent = $this->_getParentObject( $parentType, $parentId );
		$launcher = $this->module_mdl->getLauncherById($launcherid);
		$error = '';
		$types = $this->metadata_model->getAllTypes();
		$languages = $this->language_model->getLanguagesOfApp($this->app->id);

		$modules = array();
		if($launcher->moduletypeid == 31) {
			$modules = $this->module_mdl->getAvailableCityVenueModules(array(0, 44));
		} elseif($launcher->moduletypeid == 30 || $launcher->moduletypeid == 26) {
			$modules = $this->module_mdl->getAvailableCityEventModules(array(0, 44));
		}

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('type', 'type', 'trim|required');
			$this->form_validation->set_rules('order', 'order', 'trim');
			$moduletype = $this->module_mdl->getById($launcher->moduletypeid);
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {
				$opts = 0;
				if($this->input->post('type') == 'markericon') {
					$opts = 2;	# This marks this meta as hidden
				}
				$name = $this->input->post('name');

				$moduletypeid = $launcher->moduletypeid;
				if($this->input->post('moduletype')) {
					$moduletypeid = $this->input->post('moduletype');
				}
				$res = $this->general_model->insert('tc_metadata', array(
					'appId' => $this->app->id,
					'parentType' => 'launcher',
					'parentId' => $launcher->id,
					'moduletypeid' => $moduletypeid,
					'sortorder' => $this->input->post('order'),
					'type' => $this->input->post('type'),
					'qname' => 'meta_'.str_replace(' ','',$moduletype->controller).'_'.strtolower(str_replace(array(' ', '.'), array('_', '_'), $name)),
					'name' => $name,
					'opts' => $opts,
				));
			}

			if(empty($error)) {
				redirect('module/fields/'.$launcher->id.'/'.$parentType.'/'.$parentId);
			}
		}

		if($parentType == 'app') {
			$crumb = array($launcher->title => 'module/edit/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Custom fields') => 'module/fields/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Add new') => $this->uri->uri_string());
		} else {
			$crumb = array($parent->name => "$parentType/view/".$parentId, $launcher->title => 'module/edit/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Custom fields') => 'module/fields/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Add new') => $this->uri->uri_string());
		}
		$cdata = array(
			'content' => $this->load->view('c_customfield_add', array('settings' => $this->_module_settings, 'types' => $types, 'languages' => $languages, 'error' => $error, 'modules' => $modules), true),
			'sidebar' => $this->load->view('c_sidebar', array($parentType => $parent), true),
			'crumb' => $crumb,
			'modules' => $this->module_mdl->getModulesForMenu($parentType, $parentId, $this->app, ''),
		);
		if($parent->name == 'app') {
			$cdata['crumb'] = array($launcher->title => 'module/edit/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Custom fields') => 'module/fields/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Add new') => $this->uri->uri_string());
		}
		$this->load->view('master', $cdata);
	}

	function fieldedit($fieldid, $parentType, $parentId) {
		$parent = $this->_getParentObject( $parentType, $parentId );
		$field = $this->metadata_model->getFieldById($fieldid);
		$launcher = $this->module_mdl->getLauncherById($field->parentId);
		$error = '';
		$types = $this->metadata_model->getAllTypes();
		$languages = $this->language_model->getLanguagesOfApp($this->app->id);

		$modules = array();
		if($launcher->moduletypeid == 31) {
			$modules = $this->module_mdl->getAvailableCityVenueModules(array(0, 44));
		} elseif($launcher->moduletypeid == 30 || $launcher->moduletypeid == 26) {
			$modules = $this->module_mdl->getAvailableCityEventModules(array(0, 44));
		}

		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('type', 'type', 'trim|required');
			$this->form_validation->set_rules('order', 'order', 'trim');
			$moduletype = $this->module_mdl->getById($launcher->moduletypeid);
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {
				$opts = 0;
				if($this->input->post('type') == 'markericon') {
					$opts = 2;	# This marks this meta as hidden
				}

				$moduletypeid = $field->moduletypeid;
				if($this->input->post('moduletype')) {
					$moduletypeid = $this->input->post('moduletype');
				}
				$this->general_model->update('tc_metadata', $fieldid, array(
					'appId' => $this->app->id,
					'parentType' => 'launcher',
					'parentId' => $launcher->id,
					'moduletypeid' => $moduletypeid,
					'sortorder' => $this->input->post('order'),
					'type' => $this->input->post('type'),
					'qname' => 'meta_'.str_replace(' ','',$moduletype->controller).'_'.strtolower(str_replace(array(' ', '.'), array('_', '_'), $this->input->post('name'))),
					'name' => $this->input->post('name'),
					'opts' => $opts,
				));

				// foreach($languages as $language) {
				// 	$nameSuccess = $this->language_model->updateTranslation('tc_metadata', $fieldid, 'name', $language->key, $this->input->post('name_'.$language->key));
				// 	if(!$nameSuccess) {
				// 		$this->language_model->addTranslation('tc_metadata', $fieldid, 'name', $language->key, $this->input->post('name_'.$language->key));
				// 	}
				// }
			}

			if(empty($error)) {
				redirect('module/fields/'.$launcher->id.'/'.$parentType.'/'.$parentId);
			}
		}

		if($parentType == 'app') {
			$crumb = array($launcher->title => 'module/edit/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Custom fields') => 'module/fields/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Edit %s', $field->name) => $this->uri->uri_string());
		} else {
			$crumb = array($parent->name => "$parentType/view/".$parentId, $launcher->title => 'module/edit/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Custom fields') => 'module/fields/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Edit %s', $field->name) => $this->uri->uri_string());
		}
		$cdata = array(
			'content' => $this->load->view('c_customfield_add', array('settings' => $this->_module_settings, 'types' => $types, 'languages' => $languages, 'error' => $error, 'field' => $field, 'modules' => $modules), true),
			'sidebar' => $this->load->view('c_sidebar', array($parentType => $parent), true),
			'crumb' => $crumb,
			'modules' => $this->module_mdl->getModulesForMenu($parentType, $parentId, $this->app, ''),
		);
		if($parent->name == 'app') {
			$cdata['crumb'] = array($launcher->title => 'module/edit/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Custom fields') => 'module/fields/'.$launcher->moduletypeid.'/'.$parentType.'/'.$parentId, __('Add new') => $this->uri->uri_string());
		}
		$this->load->view('master', $cdata);
	}

	function fielddelete($id, $parentType, $parentId) {
		$parent = $this->_getParentObject( $parentType, $parentId );
		$field = $this->metadata_model->getFieldById($id);
		$this->language_model->removeTranslations('tc_metadata', $id);
		$this->general_model->remove('tc_metadata', $id);

		$this->session->set_flashdata('event_feedback', __('The field has successfully been deleted'));
		if($parentType == 'venue') {
			_updateVenueTimeStamp($parentId);
		} elseif($parentType == 'event') {
			_updateTimeStamp($parentId);
		}

		redirect('module/fields/'.$field->parentId.'/'.$parentType.'/'.$parentId);
	}

	/**
	 * Validate and return parent object
	 *
	 * @param string $parentType The parent type
	 * @param string $parentId The parent identifier
	 * @return mixed the parent object
	 */
	protected function _getParentObject( $parentType, $parentId )
	{
		$parent_model = $parentType.'_model';
		if(!isset($this->$parent_model) || method_exists('getbyid', $this->$parent_model)) {
			throw new RuntimeException(__('Unknown parenttype %s', $parentType));
		}
		$parent = $this->$parent_model->getbyid($parentId);
		$this->app = _actionAllowed($parent,$parentType, 'returnApp');
		return $parent;
	}
}