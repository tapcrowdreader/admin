<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Webmodule extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	function index() {
		$this->event();
	}

	function event($id, $launcherid) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');
		redirect('module/editLauncher/'.$launcherid);

		$this->iframeurl = "event/index/" . $id;
	}
}
