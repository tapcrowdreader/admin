<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Coupons extends CI_Controller {
	
	//php 5 constructor
	function __construct()
	{
		parent::__construct();
		if(_authed()) { }
		$this->load->model('coupons_model');
		$this->load->model('venue_model');
		$this->load->library('PKPass/PKPass');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Coupon',
			'plural' => 'Coupons',
			'parentType' => 'event',
			'browse_url' => 'coupons/--parentType--/--parentId--',
			'add_url' => 'coupons/add/--parentId--/--parentType--',
			'edit_url' => 'coupons/edit/--id--',
			'module_url' => 'module/editByController/coupons/--parentType--/--parentId--/0',
			'import_url' => '',
			'headers' => array(
				__('Title') => 'title'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'coupons/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'coupons/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(

			),
			'checkboxes' => false,
			'deletecheckedurl' => 'sessiongroups/removemany/--parentId--/--parentType--'
		);
	}
	
	function index(){
		$this->venue();
	}
	
	/**
	 * @param $id, the venue id of the current venue
	 * @return Creates the view for editing/adding coupons
	 */
	function venue($id) {
		$error = "";

		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');
		$this->iframeurl = 'coupons/venue/'.$id;

		$languages = $this->language_model->getLanguagesOfApp($app->id);
		
		$res = $this->db->query("SELECT * FROM coupons WHERE venueid = {$venue->id}");
		
		$coupons = $res->result();

		$this->_module_settings->parentType = 'venue';
		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'venue', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'venue', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $deletecheckedurl);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $coupons), true);
		$cdata['crumb']			= array($venue->name => "venue/view/".$venue->id, __("Coupons") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'coupons');
		$this->load->view('master', $cdata);
	}
	
	/**
	 * @param $id, The venue id
	 * @return Creates a view to add a new coupon to the venue
	 */
	function add($id){
		$error = "";
        $imageError = "";
		
		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->iframeurl = 'coupons/venue/'.$id;
		
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		
		if($this->input->post('postback') == "postback") {
			$this->load->library('form_validation');
         	$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');

			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {

				$data = array(
						'venueid' 	=> $venue->id,
						'title' 	=> $this->input->post('title_'.$app->defaultlanguage),
						'content' 	=> $this->input->post('text_'.$app->defaultlanguage),
                        'uses'      => $this->input->post('group1')
					);
				
				if($_FILES['image']['name'] != ''){
					
					$couponid = $this->general_model->insert('coupons', $data);
					//Uploads Images
					if(!is_dir($this->config->item('imagespath') . "upload/couponimages/".$couponid)){
						mkdir($this->config->item('imagespath') . "upload/couponimages/".$couponid, 0755, true);
					}

					$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/couponimages/'.$couponid;
					$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
					$configexlogo['max_width']  = '2000';
					$configexlogo['max_height']  = '2000';
					$this->load->library('upload', $configexlogo);
					$this->upload->initialize($configexlogo);
					if (!$this->upload->do_upload('image')) {
						$image = ""; //No image uploaded!
						if($_FILES['image']['name'] != '') {
							$error .= $this->upload->display_errors();
						}
					} else {
						//successfully uploaded
						$returndata = $this->upload->data();
						$image = 'upload/couponimages/'. $couponid . '/' . $returndata['file_name'];
					}

					$this->general_model->update('coupons', $couponid, array('image' => $image));

                    //add translated fields to translation table
                    foreach($languages as $language) {
                        $this->language_model->addTranslation('coupons', $couponid, 'title', $language->key, $this->input->post('title_'.$language->key));
                        $this->language_model->addTranslation('coupons', $couponid, 'content', $language->key, $this->input->post('text_'.$language->key));
                    }
					
					$this->session->set_flashdata('event_feedback', __('The coupon has successfully been added!'));
					_updateTimeStamp($venue->id);
					if($error == '') {
						redirect('coupons/venue/'.$venue->id);
					} else {
						//image error
						redirect('coupons/edit/'.$couponid.'/event?error=image');
					}
					
				} else {
					$error = __("<p>You need to upload a picture</p>");
				}

                if($imageError != '') {
                    $error .= '<br />' .$imageError;
                }
			}
		}

		$cdata['content'] 		= $this->load->view('c_coupons_add', array('venue' => $venue, 'error' => $error, 'languages' => $languages, 'app' => $app), TRUE);
		$cdata['crumb']			= array($venue->name => "venue/view/".$venue->id, __("Coupons") => "coupons/venue/".$venue->id, __("Add new") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'coupons');
		$this->load->view('master', $cdata);
	}
	
	/**
	 * @param $id, the Coupon Id of the coupon to be edited
	 * @return Creates a view to edit the coupon with the coupon id $id
	 */
	function edit($id){
		$this->load->library('form_validation');
		
		$error = "";
        $imageError = "";
		
		$coupon = $this->coupons_model->getById($id);
		
		$venue = $this->venue_model->getById($coupon->venueid);
		$app = _actionAllowed($venue, 'venue','returnApp');

		$this->iframeurl = 'coupons/coupon/'.$id;
		
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		
		if($this->input->post('postback') == "postback") {			
			$this->form_validation->set_rules('image', 'image', 'trim|callback_image_rules');

			if($this->form_validation->run() == FALSE){	
				$error = validation_errors();
				
			} else {
				
				$data = array(
						'venueid' 	=> $venue->id,
						'title' 	=> $this->input->post('title_'.$app->defaultlanguage),
						'content' 	=> $this->input->post('text_'.$app->defaultlanguage),
                        'uses'      => $this->input->post('group1')
					);
				
				$title = $data['title'];
				$content = $data['content'];
				$uses = $data['uses'];
				
				$couponid = $this->db->query("UPDATE coupons SET title='$title', content='$content', uses=$uses WHERE id=$id");
					
				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/couponimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/couponimages/".$id, 0775, true);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/couponimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				
				if ($this->upload->do_upload('image')) {
					$returndata = $this->upload->data();
					$image1 = 'upload/couponimages/'.$id.'/'.$returndata['file_name'];
					$this->general_model->update('coupons',$id,array('image' => $image1));
				} else {
					if($_FILES['image']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				}

                //add translated fields to translation table
                foreach($languages as $language) {
                    $this->language_model->updateTranslation('coupons', $id, 'title', $language->key, $this->input->post('title_'.$language->key));
                    $this->language_model->updateTranslation('coupons', $id, 'content', $language->key, $this->input->post('text_'.$language->key));
                }
				
				$this->session->set_flashdata('event_feedback', __('The coupon was successfully updated!'));
				_updateTimeStamp($venue->id);
				if($error == '') {
					redirect('coupons/venue/'.$venue->id);
				} else {
					//image error
					redirect('coupons/edit/'.$id.'/event?error=image');
				}

                if($imageError != '') {
                    $error .= '<br />' .$imageError;
                }
			}
		}
		
		$cdata['content'] 		= $this->load->view('c_coupons_edit', array('event' => $event, 'error' => $error, 'languages' => $languages, 'app' => $app, 'coupon' => $coupon), TRUE);
		$cdata['crumb']			= array($venue->name => "venue/view/".$venue->id, __("Coupons") => "coupons/venue/".$venue->id, __("Edit Coupon") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'coupons');
		$this->load->view('master', $cdata);
	}
	
	/**
	 * @param $id, the coupon id
	 * @return redirects to the coupons main page after deleting the coupon
	 */
	function delete($id){
		$res = $this->db->query("SELECT * FROM coupons WHERE id={$id}")->row();
		
		$path = $this->config->item('imagespath') . $res->image;
		unlink($path);
		
		$this->db->query("DELETE FROM coupons WHERE id={$id}");
		
		redirect("coupons/venue/".$res->venueid);
	}

	/**
	 * @param $venueid, The current Venue Id
	 * @param $couponid, the id of the coupon to create the passbook coupon.
	 * @return creates a passbook coupon file to check how it will be displayed for the user.
	 */
	function createPass($venueid,$couponid)
	{
		$venue = $this->venue_model->getById($venueid);
		$coupon = $this->coupons_model->getById($couponid);
		
		$pass = new PKPass();

		$pass->setCertificate('_certificates/Certificates.p12'); // Set the path to your Pass Certificate (.p12 file)
		$pass->setCertificatePassword(''); // Set password for certificate
		$pass->setWWDRcertPath('_certificates/AppleWWDRCA.pem');
		
		$pass->setJSON('{ 
		    "passTypeIdentifier": "pass.com.tapcrowd.coupons",
		    "formatVersion": 1,
		    "organizationName": "TapCrowd",
		    "serialNumber": "'.$venue->id.$coupon->id.'",
		    "teamIdentifier": "GWJL7Q5Q2K",
		    "backgroundColor": "rgb(107,156,196)",
		    "logoText": "'.$coupon->title.'",
		    "description": "'.$venue->name.' '.$coupon->id.'",
			"coupon": {
		        "primaryFields":
	            [{
	                "key" : "coupon",
	            	"label" : "Discount",
	            	"value" : "'.$coupon->title.'"
	            }],
		        "backFields":
	        	[{
	                "key" : "venue",
	            	"label" : "Venue",
	            	"value" : "'.$venue->name.'"
	            },
	            {
	                "key" : "coupondesc",
	            	"label" : "Description",
	            	"value" : "'.$coupon->content.'"
	            }]
		    }
		}');
		
		// add files to the PKPass package
		$pass->addFile('img/coupons/icon.png');
		$pass->addFile('img/coupons/icon@2x.png');
		$pass->addFile('img/coupons/logo.png');
		
		$file = $pass->create(true); // Create and output the PKPass
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'coupons');
    } 
}

	