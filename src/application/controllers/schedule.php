<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Schedule extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('schedule_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Schedule',
			'plural' => 'Schedules',
			'parentType' => 'event',
			'browse_url' => 'schedule/--parentType--/--parentId--',
			'add_url' => 'schedule/add/--parentId--/--parentType--',
			'edit_url' => 'schedule/edit/--id--',
			'headers' => array(
				__('Day') => 'key',
				__('From-till') => 'caption'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'schedule/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'schedule/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'checkboxes' => false,
			'deletecheckedurl' => 'schedule/removemany/--parentId--/--parentType--'
		);
	}
	
	function index() {
		$this->event();
	}
	
	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');
		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = 'event/info/'.$id;
		
		$schedule = $this->schedule_model->getScheduleByEvent($id);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $schedule), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$id, __("Schedule") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']	= array($event->name => "event/view/".$id, __("Schedule") => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'about');
		$this->load->view('master', $cdata);
	}
	
	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');
		
		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		if($app->apptypeid == 7 || $app->apptypeid == 8) {
			$this->iframeurl = 'resto/info/'.$id;
		} else {
			$this->iframeurl = 'venue/info/'.$id;
		}
		

		//delbar
		if($id == 1042) {
			$openingdelbar_nl = $this->db->query("SELECT * FROM metadata WHERE appid = 393 AND `table` = 'venue' AND identifier = 1042 AND `key` = 'openinghours_nl' LIMIT 1 ")->row()->value;
			$openingdelbar_fr = $this->db->query("SELECT * FROM metadata WHERE appid = 393 AND `table` = 'venue' AND identifier = 1042 AND `key` = 'openinghours_fr' LIMIT 1 ")->row()->value;
			$error = '';
			$this->load->library('form_validation');
			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('text_nl', 'Nederlands', 'trim|required');
				$this->form_validation->set_rules('text_fr', 'Frans', 'trim|required');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data_nl = array('value'=>htmlspecialchars_decode(html_entity_decode(set_value('text_nl'))));
					$data_fr = array('value'=>htmlspecialchars_decode(html_entity_decode(set_value('text_fr'))));
					$this->general_model->update_meta_data('venue',1042,'openinghours_nl',$data_nl);
					$this->general_model->update_meta_data('venue',1042,'openinghours_fr',$data_fr);

					redirect('venue/view/1042');
				}
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Opening hours") => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'about');
			$this->load->view('master', $cdata);
		} else {
			$schedule = $this->schedule_model->getScheduleByVenue($id);
			$this->_module_settings->parentType = 'venue';
			# Module add url
			$add_url =& $this->_module_settings->add_url;
			$add_url = str_replace(array('--parentType--','--parentId--'), array('venue', $id), $add_url);
			# Edit href
			$edit_href =& $this->_module_settings->actions['edit']->href;
			$edit_href = str_replace('--parentType--', 'venue', $edit_href);
			# Delete href
			$delete_href =& $this->_module_settings->actions['delete']->href;
			$delete_href = str_replace('--parentType--', 'venue', $delete_href);

			$listview = $this->_module_settings->views['list'];
			$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $schedule), true);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Schedule") => $this->uri->uri_string()));
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'about');
			$this->load->view('master', $cdata);
		}
	}
	
	function add($eventid, $type = 'event') {
		if ($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
			
			$venue = $this->venue_model->getById($eventid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'venue/info/'.$eventid;
			
			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
					$this->form_validation->set_rules('when_'.$language->key, 'When ('.$language->name.')', 'trim|required');
					$this->form_validation->set_rules('fromtill', 'fromtill', 'trim');
				}
				$this->form_validation->set_rules('sortorder', 'sortorder', 'trim');
				
				$this->form_validation->set_rules('w_date', 'w_date', 'trim');
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array( 
							"venueid"	=> $venue->id,
							"key" 		=> set_value('when_'.  $app->defaultlanguage),
							"caption" 	=> set_value('fromtill_'.  $app->defaultlanguage),
							"date" 		=> date('Y-m-d', strtotime($this->input->post('when_'.$app->defaultlanguage))),
							'sortorder' => $this->input->post('sortorder')
						);
					
					if($res = $this->general_model->insert('schedule', $data)){
						foreach($languages as $language) {
							$this->language_model->addTranslation('schedule', $res, 'when', $language->key, $this->input->post('when_'.$language->key));
							$this->language_model->addTranslation('schedule', $res, 'caption', $language->key, $this->input->post('fromtill_'.$language->key));
						}
						$this->session->set_flashdata('event_feedback', __('The schedule has successfully been added!'));
						_updateVenueTimeStamp($venue->id);
						redirect('schedule/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_schedule_add', array('venue' => $venue, 'error' => $error, 'languages' => $languages), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'about');
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Schedule") => "schedule/venue/".$venue->id, __("Add schedule") => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

            $dateMonthYearArr = array();
            $fromDateTS = strtotime($event->datefrom);
            $toDateTS = strtotime($event->dateto);

            for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
            $currentDateStr = date("Y-m-d",$currentDateTS);
            $dateMonthYearArr[] = $currentDateStr;
            }

			$this->iframeurl = 'event/info/'.$eventid;

			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
					$this->form_validation->set_rules('when_'.$language->key, 'When ('.$language->name.')', 'trim|required');
					$this->form_validation->set_rules('fromtill_'.$language->key, 'fromtill ('.$language->name.')', 'trim');
				}
				$this->form_validation->set_rules('sortorder', 'sortorder', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
                    $data = array( 
						"eventid"	=> $event->id,
						"key" 		=> set_value('when_'.  $app->defaultlanguage),
						"caption" 	=> set_value('fromtill_'.  $app->defaultlanguage),
						"date" 		=> date('Y-m-d', strtotime($this->input->post('when_'.$app->defaultlanguage))),
						'sortorder' => $this->input->post('sortorder')
					);

					if($res = $this->general_model->insert('schedule', $data)){
						foreach($languages as $language) {
							$this->language_model->addTranslation('schedule', $res, 'when', $language->key, $this->input->post('when_'.$language->key));
							$this->language_model->addTranslation('schedule', $res, 'caption', $language->key, $this->input->post('fromtill_'.$language->key));
						}
						$this->session->set_flashdata('event_feedback', __('The schedule has successfully been added!'));
						_updateTimeStamp($event->id);
						redirect('schedule/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_schedule_add_event', array('event' => $event, 'error' => $error, 'dates' => $dateMonthYearArr, 'languages' => $languages), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Schedule") => "schedule/event/".$event->id, __("Add schedule") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, __("Schedule") => "schedule/event/".$event->id, __("Add schedule") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'about');
			$this->load->view('master', $cdata);
		}
	}
	
	function edit($id, $type = 'event') {
		if ($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";
			// EDIT 
			$schedule = $this->general_model->one('schedule', $id);
			if($schedule == FALSE) redirect('venues');
			
			$venue = $this->venue_model->getById($schedule->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'venue/info/'.$schedule->venueid;
			
			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
					$this->form_validation->set_rules('when_'.$language->key, 'When ('.$language->name.')', 'trim|required');
					$this->form_validation->set_rules('fromtill_'.$language->key, 'fromtill ('.$language->name.')', 'trim');
				}
				$this->form_validation->set_rules('sortorder', 'sortorder', 'trim');
				
				$this->form_validation->set_rules('w_date', 'w_date', 'trim');
				
				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array( 
							"venueid"	=> $venue->id,
							"key" 		=> set_value('when_'.  $app->defaultlanguage),
							"caption" 	=> set_value('fromtill_'.  $app->defaultlanguage),
							"date" 		=> date('Y-m-d', strtotime($this->input->post('when_'.$app->defaultlanguage))),
							'sortorder' => $this->input->post('sortorder')
						);
					
					if($this->general_model->update('schedule', $id, $data)){
                        foreach($languages as $language) {
							$whenSuccess = $this->language_model->updateTranslation('schedule', $id, 'when', $language->key, $this->input->post('when_'.$language->key));
							if(!$whenSuccess) {
								$this->language_model->addTranslation('schedule', $id, 'when', $language->key, $this->input->post('when_'.$language->key));
							}
							$captionSuccess = $this->language_model->updateTranslation('schedule', $id, 'caption', $language->key, $this->input->post('fromtill_'.$language->key));
							if(!$captionSuccess) {
								$this->language_model->addTranslation('schedule', $id, 'caption', $language->key, $this->input->post('fromtill_'.$language->key));
							}
                        }
						$this->session->set_flashdata('event_feedback', __('The schedule has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						redirect('schedule/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}
			
			$cdata['content'] 		= $this->load->view('c_schedule_edit', array('venue' => $venue, 'schedule' => $schedule, 'error' => $error, 'languages' => $languages), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('venue', $venue->id, $app, 'about');
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Schedule") => "schedule/venue/".$venue->id, __("Edit: ") . $schedule->key => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";
			// EDIT
			$schedule = $this->general_model->one('schedule', $id);
			if($schedule == FALSE) redirect('events');

			$event = $this->event_model->getbyid($schedule->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$this->iframeurl = 'event/info/'.$schedule->eventid;
            
            $dateMonthYearArr = array();
            $fromDateTS = strtotime($event->datefrom);
            $toDateTS = strtotime($event->dateto);

            for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
            $currentDateStr = date("Y-m-d",$currentDateTS);
            $dateMonthYearArr[] = $currentDateStr;
            }

			if($this->input->post('postback') == "postback") {
				foreach($languages as $language) {
					$this->form_validation->set_rules('when_'.$language->key, 'When ('.$language->name.')', 'trim|required');
					$this->form_validation->set_rules('fromtill_'.$language->key, 'fromtill ('.$language->name.')', 'trim');
				}
				$this->form_validation->set_rules('sortorder', 'sortorder', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
                    $data = array( 
						"eventid"	=> $event->id,
						"key" 		=> set_value('when_'.  $app->defaultlanguage),
						"caption" 	=> set_value('fromtill_'.  $app->defaultlanguage),
						"date" 		=> date('Y-m-d', strtotime($this->input->post('when_'.$app->defaultlanguage))),
						'sortorder' => $this->input->post('sortorder')
					);

					if($res = $this->general_model->update('schedule', $id, $data)){
                        foreach($languages as $language) {
							$whenSuccess = $this->language_model->updateTranslation('schedule', $id, 'when', $language->key, $this->input->post('when_'.$language->key));
							if(!$whenSuccess) {
								$this->language_model->addTranslation('schedule', $id, 'when', $language->key, $this->input->post('when_'.$language->key));
							}
							$captionSuccess = $this->language_model->updateTranslation('schedule', $id, 'caption', $language->key, $this->input->post('fromtill_'.$language->key));
							if(!$captionSuccess) {
								$this->language_model->addTranslation('schedule', $id, 'caption', $language->key, $this->input->post('fromtill_'.$language->key));
							}
                        }
						$this->session->set_flashdata('event_feedback', __('The schedule has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('schedule/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_schedule_edit_event', array('event' => $event, 'schedule' => $schedule, 'error' => $error, 'dates' => $dateMonthYearArr, 'languages' => $languages), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Schedule") => "schedule/event/".$event->id, __("Edit: ") . $schedule->key => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, __("Schedule") => "schedule/event/".$event->id, __("Edit: ") . $schedule->key => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'about');
			$this->load->view('master', $cdata);
		}
	}
	
	function delete($id, $type = 'event') {
		if ($type == "venue") {
			$schedule = $this->general_model->one('schedule', $id);
			$venue = $this->venue_model->getbyid($schedule->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($this->general_model->remove('schedule', $id)){
				$this->session->set_flashdata('event_feedback', __('The schedule has successfully been deleted'));
				_updateVenueTimeStamp($schedule->venueid);
				redirect('schedule/venue/'.$schedule->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('schedule/venue/'.$schedule->venueid);
			}
		} else {
			$schedule = $this->general_model->one('schedule', $id);
			$event = $this->event_model->getbyid($schedule->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			if($this->general_model->remove('schedule', $id)){
				$this->session->set_flashdata('event_feedback', __('The schedule has successfully been deleted'));
				_updateTimeStamp($schedule->eventid);
				redirect('schedule/event/'.$schedule->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('schedule/event/'.$schedule->eventid);
			}
		}
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		$this->general_model->removeMany($selectedids, 'schedule');
    }   
	
}