<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Sessiongroups extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('sessions_model');

		# Load settings
		$this->_module_settings = (object)array(
			'singular' => 'Sessiongroup',
			'plural' => 'Sessiongroups',
			'parentType' => 'event',
			'browse_url' => 'sessiongroups/--parentType--/--parentId--',
			'add_url' => 'sessiongroups/add/--parentId--/--parentType--',
			'edit_url' => 'sessiongroups/edit/--id--',
			'module_url' => 'module/editByController/sessions/--parentType--/--parentId--/0',
			'import_url' => '',
			'headers' => array(
				__('Name') => 'name',
				__('Order') => 'order'
			),
			'actions' => array(
				'edit' => (object)array(
					'title' => __('Edit'),
					'href' => 'sessiongroups/edit/--id--/--parentType--',
					'icon_class' => 'icon-pencil',
					'btn_class' => 'btn-warning',
				),
				'duplicate' => (object)array(
					'title' => __('Duplicate'),
					'href' => 'duplicate/index/--id--/--plural--/--parentType--',
					'icon_class' => 'icon-tags',
					'btn_class' => 'btn-inverse',
				),
				'delete' => (object)array(
					'title' => __('Delete'),
					'href' => 'sessiongroups/delete/--id--/--parentType--',
					'icon_class' => 'icon-remove',
					'btn_class' => 'btn-danger',
				),
			),
			'views' => array(
				'list' => 'tc_listview',
				'edit' => 'tc_place_edit',
				'read' => 'tc_displayview',
			),
			'statuses' => array('created','pending','inactive','active','locked','deleted','published'),
			'metadata' => array(
			),
			'launcher' => null,
			'extrabuttons' => array(
				'sessions' => array()
			),
			'checkboxes' => false,
			'deletecheckedurl' => 'sessiongroups/removemany/--parentId--/--parentType--'
		);
	}

	//php 4 constructor
	function Sessiongroups() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('sessions_model');
	}

	function index() {
		$this->event();
	}

	function event($id) {
		if($id == FALSE || $id == 0) redirect('events');

		$event = $this->event_model->getbyid($id);
		$app = _actionAllowed($event, 'event','returnApp');

		$this->iframeurl = "sessions/event/" . $id;

		// Get Sessions for event per group
		$sessiongroups = $this->sessions_model->getSessiongroupsByEventID($id);

		# Module edit url
		$module_url =& $this->_module_settings->module_url;
		$module_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $module_url);
		# Module add url
		$add_url =& $this->_module_settings->add_url;
		$add_url = str_replace(array('--parentType--','--parentId--'), array('event', $id), $add_url);
		# Edit href
		$edit_href =& $this->_module_settings->actions['edit']->href;
		$edit_href = str_replace('--parentType--', 'event', $edit_href);
		# Delete href
		$delete_href =& $this->_module_settings->actions['delete']->href;
		$delete_href = str_replace('--parentType--', 'event', $delete_href);
		# delete many
		$this->_module_settings->checkboxes = true;
		$deletecheckedurl =& $this->_module_settings->deletecheckedurl;
		$deletecheckedurl = str_replace(array('--parentType--','--parentId--'), array('event', $id), $deletecheckedurl);

		$btnSessions = (object)array(
						'title' => __('Edit Sessions'),
						'href' => 'sessions/event/'.$id,
						'icon_class' => 'icon-pencil',
						'btn_class' => 'edit btn',
						);
		$this->_module_settings->extrabuttons['sessions'] = $btnSessions;

		$listview = $this->_module_settings->views['list'];
		$cdata['content'] 		= $this->load->view($listview, array('settings' => $this->_module_settings, 'data' => $sessiongroups), true);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
		if($app->familyid != 1) {
			$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$id, __("Sessions") => 'sessions/event/'.$id, __("Session Groups") => $this->uri->uri_string());
			if($app->familyid == 2) {
				$crumbapp = array($app->name => 'apps/view/'.$app->id);
				$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
			}
		} else {
			$cdata['crumb']	= array($event->name => "event/view/".$id, __("Sessions") => 'sessions/event/'.$id, __("Session Groups") => $this->uri->uri_string());
		}
		$cdata['modules'] = $this->module_mdl->getModulesForMenu('event', $event->id, $app, 'sessions');
		$this->load->view('master', $cdata);
	}

	function venue($id) {
		if($id == FALSE || $id == 0) redirect('venues');

		$venue = $this->venue_model->getById($id);
		$app = _actionAllowed($venue, 'venue','returnApp');

		// Get Sessions for event per group
		$sessiongroups = $this->sessions_model->getSessiongroupsByVenueID($id);
		$headers = array(__('Name') => 'name', __('Order') => 'order');

		$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'data' => $sessiongroups, 'headers' => $headers), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
		$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$id, __("Session Groups") => $this->uri->uri_string()));
		$this->load->view('master', $cdata);
	}

	function add($id, $type = 'event') {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			$venue = $this->venue_model->getById($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			$venues = $this->venue_model->allfromorganizer(_currentUser()->organizerId);

			if($this->input->post('postback') == "postback") {
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                }
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('venues', 'venues', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array(
						"name" => set_value('name'),
						//"venueid" => set_value('venues'),
						"venueid" => $venue->id,
						"order" => set_value('order')
					);

					if($newid = $this->general_model->insert('sessiongroup', $data)){
                        //add translated fields to translation table
                        foreach($languages as $language) {
                            $this->language_model->addTranslation('sessiongroup', $newid, 'name', $language->key, $this->input->post('name_'.$language->key));
                        }
						$this->session->set_flashdata('event_feedback', __('The sessiongroup has successfully been added!'));
						_updateVenueTimeStamp($venue->id);
						redirect('sessiongroups/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_sessiongroup_add', array('venue' => $venue, 'venues' => $venues, 'error' => $error), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Sessions") => "sessions/venue/".$venue->id, __("Add new") => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$this->iframeurl = "sessions/event/" . $id;
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$events = $this->event_model->allfromorganizer(_currentUser()->organizerId);

			if($this->input->post('postback') == "postback") {
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                }
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('events', 'events', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = "Some fields are missing.";
				} else {
					$data = array(
						"name" => set_value('name_'. $app->defaultlanguage),
						//"eventid" => set_value('events'),
						"eventid" => $event->id,
						"order" => set_value('order')
					);

					if($newid = $this->general_model->insert('sessiongroup', $data)){
                        //add translated fields to translation table
                        foreach($languages as $language) {
                            $this->language_model->addTranslation('sessiongroup', $newid, 'name', $language->key, $this->input->post('name_'.$language->key));
                        }
						$this->session->set_flashdata('event_feedback', __('The sessiongroup has successfully been added!'));
						_updateTimeStamp($event->id);
						redirect('sessiongroups/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_sessiongroup_add', array('event' => $event, 'events' => $events, 'error' => $error, 'languages' => $languages), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Sessions") => 'sessions/event/'.$id, __("Session Groups") => 'sessiongroups/event/'.$event->id, __("Add new") => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, __("Sessions") => 'sessions/event/'.$id, __("Session Groups") => 'sessiongroups/event/'.$event->id, __("Add new") => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function edit($id, $type = 'event') {
		if($type == 'venue') {
			$this->load->library('form_validation');
			$error = "";

			// EDIT SESSIONGROUP
			$sessiongroup = $this->sessions_model->getSessiongroupByID($id);
			if($sessiongroup == FALSE) redirect('venues');

			$venue = $this->venue_model->getById($sessiongroup->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$venues = $this->venue_model->allfromorganizer(_currentUser()->organizerId);

			if($this->input->post('postback') == "postback") {
				$this->form_validation->set_rules('name', 'name', 'trim|required');
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('venues', 'venues', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$data = array(
						"name" => set_value('name'),
						//"venueid" => set_value('venues'),
						"venueid" => $venue->id,
						"order" => set_value('order')
					);

					if($this->sessions_model->edit_sessiongroup($id, $data)){
						$this->session->set_flashdata('event_feedback', __('The sessiongroup has successfully been updated'));
						_updateVenueTimeStamp($venue->id);
						redirect('sessiongroups/venue/'.$venue->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_sessiongroup_edit', array('venue' => $venue, 'sessiongroup' => $sessiongroup, 'venues' => $venues, 'error' => $error), TRUE);
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('venue' => $venue), TRUE);
			$cdata['crumb']			= checkBreadcrumbsVenue($app, array($venue->name => "venue/view/".$venue->id, __("Sessions") => "sessions/venue/".$venue->id, __("Edit: ") . $sessiongroup->name => $this->uri->uri_string()));
			$this->load->view('master', $cdata);
		} else {
			$this->load->library('form_validation');
			$error = "";

			// EDIT SESSIONGROUP
			$sessiongroup = $this->sessions_model->getSessiongroupByID($id);
			if($sessiongroup == FALSE) redirect('events');

			$event = $this->event_model->getbyid($sessiongroup->eventid);
			$app = _actionAllowed($event, 'event','returnApp');

			$this->iframeurl = "sessions/group/" . $id;
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$events = $this->event_model->allfromorganizer(_currentUser()->organizerId);

			if($this->input->post('postback') == "postback") {
                foreach($languages as $language) {
                    $this->form_validation->set_rules('name_'.$language->key, 'name ('.$language->name.')', 'trim|required');
                }
				$this->form_validation->set_rules('order', 'order', 'trim');
				$this->form_validation->set_rules('events', 'events', 'trim');

				if($this->form_validation->run() == FALSE){
					$error = __("Some fields are missing.");
				} else {
					$name = set_value('name_'. $app->defaultlanguage);
					$data = array(
						"name" => $name,
						//"eventid" => set_value('events'),
						"eventid" => $event->id,
						"order" => set_value('order')
					);

					if($this->sessions_model->edit_sessiongroup($id, $data)){
						foreach($languages as $language) {
							$name = $this->input->post('name_'.$language->key);
	                        $nameSuccess = $this->language_model->updateTranslation('sessiongroup', $id, 'name', $language->key, $name);
							if($nameSuccess == false || $nameSuccess == null) {
								$this->language_model->addTranslation('sessiongroup', $id, 'name', $language->key, $name);
							}
						}
						$this->session->set_flashdata('event_feedback', __('The sessiongroup has successfully been updated'));
						_updateTimeStamp($event->id);
						redirect('sessiongroups/event/'.$event->id);
					} else {
						$error = __("Oops, something went wrong. Please try again.");
					}
				}
			}

			$cdata['content'] 		= $this->load->view('c_sessiongroup_edit', array('event' => $event, 'sessiongroup' => $sessiongroup, 'events' => $events, 'error' => $error, 'languages' => $languages), TRUE);
			if($app->familyid != 1) {
				$cdata['crumb']	= array(__("Events") => "events", $event->name => "event/view/".$event->id, __("Sessions") => 'sessions/event/'.$id, __("Session Groups") => 'sessiongroups/event/'.$event->id, __("Edit: ") . $sessiongroup->name => $this->uri->uri_string());
				if($app->familyid == 2) {
					$crumbapp = array($app->name => 'apps/view/'.$app->id);
					$cdata['crumb'] = array_merge($crumbapp, $cdata['crumb']);
				}
			} else {
				$cdata['crumb']	= array($event->name => "event/view/".$event->id, __("Sessions") => 'sessions/event/'.$id, __("Session Groups") => 'sessiongroups/event/'.$event->id, __("Edit: ") . $sessiongroup->name => $this->uri->uri_string());
			}
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array('event' => $event), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function delete($id, $type = 'event') {
		if($type == 'venue') {
			$sessiongroup = $this->sessions_model->getSessiongroupByID($id);
			$venue = $this->venue_model->getbyid($sessiongroup->venueid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			if($this->general_model->remove('sessiongroup', $id)){
				$this->language_model->removeTranslations('sessiongroup', $id);
				$this->session->set_flashdata('event_feedback', __('The sessiongroup has successfully been deleted'));
				_updateVenueTimeStamp($sessiongroup->venueid);
				redirect('sessiongroups/venue/'.$sessiongroup->venueid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('sessiongroups/venue/'.$sessiongroup->venueid);
			}
		} else {
			$sessiongroup = $this->sessions_model->getSessiongroupByID($id);
			$event = $this->event_model->getbyid($sessiongroup->eventid);
			$app = _actionAllowed($event, 'event','returnApp');
			if($this->general_model->remove('sessiongroup', $id)){
				$this->sessions_model->removeSessionsFromGroup($id);
				$this->language_model->removeTranslations('sessiongroup', $id);
				$this->session->set_flashdata('event_feedback', __('The sessiongroup has successfully been deleted'));
				_updateTimeStamp($sessiongroup->eventid);
				redirect('sessiongroups/event/'.$sessiongroup->eventid);
			} else {
				$this->session->set_flashdata('event_error', __('Oops, something went wrong. Please try again.'));
				redirect('sessiongroups/event/'.$sessiongroup->eventid);
			}
		}
	}

	function sort($type = '') {
		if($type == '' || $type == FALSE) redirect('events');

		switch($type){
			case "sessions":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('session', $data);
					$this->db->last_query();
				}
				break;
			case "groups":
				$orderdata = $this->input->post('records');
				foreach ($orderdata as $val) {
					$val_split = explode("=", $val);

					$data['order'] = $val_split[1];
					$this->db->where('id', $val_split[0]);
					$this->db->update('sessiongroup', $data);
					$this->db->last_query();
				}
				break;
			default:
				break;
		}
	}

    function removemany($typeId, $type) {
    	if($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeId);
			$app = _actionAllowed($venue, 'venue','returnApp');
    	} elseif($type == 'event') {
			$event = $this->event_model->getbyid($typeId);
			$app = _actionAllowed($event, 'event','returnApp');
    	} elseif($type == 'app') {
			$app = $this->app_model->get($typeId);
			_actionAllowed($app, 'app');
    	}
		$selectedids = $this->input->post('selectedids');
		foreach($selectedids as $id) {
			$this->sessions_model->removeSessionsFromGroup($id);
		}
		$this->general_model->removeMany($selectedids, 'sessiongroup');
    }  

}