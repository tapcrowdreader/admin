<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Tag extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('tag_model');
	}

	function edit($tagname, $appid) {
		$error = '';
		$this->load->library('form_validation');

		$app = $this->app_model->get($appid);
		$oldimage = $this->tag_model->getOldImage($appid, $tagname);
		$launcher = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND title='$tagname'");
		if($launcher->num_rows() != 0) {
			$order = $launcher->row()->order;
		} else {
			$order = 0;
		}

		if($this->input->post('postback') == "postback") {
			$this->form_validation->set_rules('tagname', 'tagname', 'trim|required');
			$this->form_validation->set_rules('image', 'image', 'trim');
			$this->form_validation->set_rules('order', 'order', 'trim');

			if(!is_dir($this->config->item('imagespath') . "upload/tagimages/".$appid)){
				mkdir($this->config->item('imagespath') . "upload/tagimages/".$appid, 0755, true);
			}

			$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/tagimages/'.$appid;
			$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
			$configexlogo['max_width']  = '2000';
			$configexlogo['max_height']  = '2000';
			$this->load->library('upload', $configexlogo);
			$this->upload->initialize($configexlogo);
			if (!$this->upload->do_upload('image')) {
				$image = $oldimage; //No image uploaded!
				$error = $this->upload->display_errors();
			} else {
				//successfully uploaded
				$returndata = $this->upload->data();
				$image = 'upload/tagimages/'. $appid . '/' . $returndata['file_name'];
			}

			$data = array(
				'tag'			=> $this->input->post('tagname'),
				'image'			=> $image
			);

			//update
			$tagidsArray = $this->tag_model->getTagIds($appid, $tagname);

			$this->general_model->updateMany('tag', $tagidsArray, $data);

			//launcheritem
			$launcherexists = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND title='$tagname'");
			if($launcherexists->num_rows() != 0) {
				$launcherdata = array(
						'appid'=>$app->id,
						'title'=>$this->input->post('tagname'),
						'tag'=>$this->input->post('tagname'),
						'icon'=>$image,
						'order'=>$this->input->post('order')
					);

				$launcherexists = $launcherexists->row();
				$this->general_model->update('launcher',$launcherexists->id, $launcherdata);
			}


			$this->session->set_flashdata('event_feedback', __('The tag settings have successfully been changed!'));
			redirect('apps/view/'.$appid);
		}
		$cdata['content'] 		= $this->load->view('c_tag_edit', array('app' => $app, 'error' => $error, 'tagname' => $tagname, 'image' => $oldimage, 'order'=>$order), TRUE);
		$cdata['crumb']			= array("Tag: ".$tagname => "tag/edit/".$tagname.'/'.$appid);
		$this->load->view('master', $cdata);
	}

	function removeimage($tagname, $appid) {
		$oldimage = $this->tag_model->getOldImage($appid, $tagname);;

		if(!file_exists($this->config->item('imagespath') . $oldimage)) redirect('apps/view/'.$appid);

		// Delete image + generated thumbs

		unlink($this->config->item('imagespath') . $oldimage);
		$this->session->set_flashdata('event_feedback', __('Image has successfully been removed.'));

		$tagidsArray = $this->tag_model->getTagIds($appid, $tagname);
		$data = array('image' => '');
		$this->general_model->updateMany('tag', $tagidsArray, $data);

		redirect('apps/view/'.$appid);
	}

	function active($tagname, $appid, $trigger) {
		$app = $this->app_model->get($appid);
		_actionAllowed($app, 'app');
		switch($trigger){
			case 'on':
				$data = array(
					'active' => 1
				);
				$this->db->where('appid', $appid);
				$this->db->where('tag', $tagname);
				$this->db->update('tag', $data);

				$launcherexists = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND tag='$tagname'");
				if($launcherexists->num_rows() != 0) {
					$launcherdata = array('active'=>1);

					$launcherexists = $launcherexists->row();
					$this->general_model->update('launcher',$launcherexists->id, $launcherdata);
				}
				break;
			case 'off':
				$data = array(
					'active' => 0
				);
				$this->db->where('appid', $appid);
				$this->db->where('tag', $tagname);
				$this->db->update('tag', $data);

				$launcherexists = $this->db->query("SELECT * FROM launcher WHERE appid = $app->id AND tag='$tagname'");
				if($launcherexists->num_rows() != 0) {
					$launcherdata = array('active'=>0);

					$launcherexists = $launcherexists->row();
					$this->general_model->update('launcher',$launcherexists->id, $launcherdata);
				}
				break;
			default:
				break;
		}
		$this->general_model->update('app', $appid, array('timestamp' => time()));
		redirect('apps/view/'.$appid);
	}
}