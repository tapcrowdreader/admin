<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Groupitems extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('module_mdl');
		$this->load->model('group_model');
		$this->load->model('groupitem_model');
	}	
	function add($id, $type = 'event', $groupid) {
		$error = '';
		$this->load->library('form_validation');
		$languages = array();
		if($type == 'event') {
			$event = $this->event_model->getbyid($id);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('name_'.$language->key, 'name_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {		
					
					//insert groupitem			
					$data = array(
						'appid'		=> $app->id,
						'eventid'	=> $id,
						'groupid'	=> $groupid
					);

					$group = $this->group_model->getById($groupid);
					$data['displaytype'] = $group->displaytype;
					
					$groupitemid = $this->general_model->insert('groupitem', $data);

					_updateTimeStamp($id);
					if($error == '') {
						redirect('groupitems/view/'.$groupitemid.'/event');
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_groupitems_add', array('event' => $event, 'error' => $error, 'languages'=>$languages, 'app' => $app), TRUE);
			$cdata['crumb']			= array(__('Events') => 'events', $event->name => 'event/view/'.$event->id, __('Item') => 'groupitems/add/'.$id.'/event');
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$this->load->view('master', $cdata);
		} elseif($type == 'venue') {
			$venue = $this->venue_model->getbyid($id);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);
			
			if($this->input->post('postback') == 'postback') {
	            foreach($languages as $language) {
	                $this->form_validation->set_rules('name_'.$language->key, 'name_'.$language->key, 'trim|required');
	            }
				$this->form_validation->set_rules('imageurl', 'image', 'trim|callback_image_rules');

				if($this->form_validation->run() == FALSE){
					$error = validation_errors();
				} else {		
					
					//insert groupitem			
					$data = array(
						'appid'		=> $app->id,
						'venueid'	=> $id,
						'groupid'	=> $groupid
					);

					$group = $this->group_model->getById($groupid);
					$data['displaytype'] = $group->displaytype;
					
					$groupitemid = $this->general_model->insert('groupitem', $data);

					_updateVenueTimeStamp($id);
					if($error == '') {
						redirect('groupitems/view/'.$groupitemid.'/venue');
					}
				}
			}
			$cdata['content'] 		= $this->load->view('c_groupitems_add', array('venue' => $venue, 'error' => $error, 'languages'=>$languages, 'app' => $app), TRUE);
			$cdata['crumb']			= array(__('Venues') => 'venues', $venue->name => 'venue/view/'.$venue->id, __('Item') => 'groupitemss/add/'.$id.'/venue');
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$this->load->view('master', $cdata);
		}
	}

	function getObjectsByType($typeid, $type, $object) {
		//alles appid geven of kijken op venue of event of appid?
		if($type == 'venue') {
			$res = $this->db->query("SELECT * FROM $type WHERE venueid = $typeid");
			if($res->num_rows() == 0) {
				return array();
			} else {
				return $res->result();
			}
		} elseif($type == 'event') {
			$res = $this->db->query("SELECT * FROM $type WHERE eventid = $typeid");
			if($res->num_rows() == 0) {
				return array();
			} else {
				return $res->result();
			}
		}
	}

	function delete($id, $type, $typeid, $object) {
		if($type == 'event') {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
		} elseif($type =='venue') {
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
		}
		
		$groupitem = $this->groupitem_model->getById($id);
		$this->general_model->remove('groupitem', $id);
		redirect('groups/view/'.$groupitem->groupid.'/'.$type.'/'.$typeid.'/'.$object);
	}

	function deleteall($id, $type, $typeid, $object) {
		if($type == 'event') {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
		} elseif($type =='venue') {
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
		}
		
		$groupitem = $this->groupitem_model->getById($id);
		$this->groupitem_model->removeGroupitemsOfObject($groupitem->itemtable, $groupitem->itemid);
		$this->general_model->remove($groupitem->itemtable, $groupitem->itemid);
		redirect('groups/view/'.$groupitem->groupid.'/'.$type.'/'.$typeid.'/'.$object);
	}

	function view($id, $type = 'event', $typeid) {
		$error = '';
		if($type == 'event') {
			$event = $this->event_model->getbyid($typeid);
			$app = _actionAllowed($event, 'event','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$groupitem = $this->groupitem_model->getById($id);

			$headers = array(__('Title') => 'title');

			$cdata['content'] 		= $this->load->view('c_listview', array('event' => $event, 'type'=>'event', 'typeid'=>$typeid, 'parents' => $parents, 'group' => $group, 'data' => $children, 'headers' => $headers), TRUE);
			$cdata['crumb']			= array(__('Events') => 'events', $event->name => 'event/view/'.$event->id, 'Groups' => 'groups/view/'.$id.'/'.$typeid.'/event');
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$this->load->view('master', $cdata);
		} elseif($type == 'venue') {
			$venue = $this->venue_model->getbyid($typeid);
			$app = _actionAllowed($venue, 'venue','returnApp');
			$languages = $this->language_model->getLanguagesOfApp($app->id);

			$groupitem = $this->groupitem_model->getById($id);

			$headers = array('Title' => 'title');

			$cdata['content'] 		= $this->load->view('c_listview', array('venue' => $venue, 'type'=>'venue', 'typeid'=>$typeid, 'parents' => $parents, 'group' => $group, 'data' => $children, 'headers' => $headers), TRUE);
			$cdata['crumb']			= array('Venues' => 'venues', $venue->name => 'venue/view/'.$venue->id, __('Groups') => 'groups/view/'.$id.'/'.$typeid.'/venue');
			$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
			$this->load->view('master', $cdata);
		}
	}
}