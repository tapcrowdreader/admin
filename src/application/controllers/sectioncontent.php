<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Sectioncontent extends CI_Controller {
	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
		$this->load->model('contentmodule_model');
	}
	
	function add($sectionid) {
		$section = $this->contentmodule_model->getSectionById($sectionid);
		$contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$error = '';
		$this->load->library('form_validation');
		$contentmodules = $this->contentmodule_model->getContentModules($app->id);
		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
            }
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('contentmodule', 'contentmodule', 'trim');
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {	
				$data = array(
						'sectionid' => $sectionid,
						'title'		=> $this->input->post('title_'.$app->defaultlanguage),
						'order'		=> $this->input->post('order'),
						'contentmoduleid' => set_value('contentmodule')
					);
				$id = $this->general_model->insert('sectioncontent', $data);

				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/sectioncontentimages/".$id)){
					mkdir($this->config->item('imagespath') . "upload/sectioncontentimages/".$id, 0755, TRUE);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/sectioncontentimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('imageurl')) {
					$image = ""; //No image uploaded!
					if($_FILES['imageurl']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/sectioncontentimages/'. $id . '/' . $returndata['file_name'];
				}
				
				$this->general_model->update('sectioncontent', $id, array('icon' => $image));
				$this->general_model->update('app', $app->id, array('timestamp' => time()));
				if($error == '') {
					redirect('section/view/'.$sectionid);
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_sectioncontent_add', array('app' => $app, 'error' => $error, 'languages' => $languages, 'contentmodules' => $contentmodules), TRUE);
		$cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id, $section->title => "section/view/".$section->id, __("Add Button") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}

	function edit($sectioncontentid) {
		$sectioncontent = $this->contentmodule_model->getSectioncontentById($sectioncontentid);
		$section = $this->contentmodule_model->getSectionById($sectioncontent->sectionid);
		$contentmodule = $this->contentmodule_model->getById($section->contentmoduleid);
		$app = $this->app_model->get($contentmodule->appid);
		_actionAllowed($app, 'app','returnApp');
		$languages = $this->language_model->getLanguagesOfApp($app->id);
		$error = '';
		$this->load->library('form_validation');
		$contentmodules = $this->contentmodule_model->getContentModules($app->id);

		if($this->input->post('postback') == "postback") {
            foreach($languages as $language) {
                $this->form_validation->set_rules('title_'.$language->key, 'title ('.$language->name.')', 'trim|required');
            }
			$this->form_validation->set_rules('order', 'order', 'trim');
			$this->form_validation->set_rules('contentmodule', 'contentmodule', 'trim');
			
			if($this->form_validation->run() == FALSE){
				$error = validation_errors();
			} else {	
				$data = array(
						'title'		=> $this->input->post('title_'.$app->defaultlanguage),
						'order'		=> $this->input->post('order'),
						'contentmoduleid' => set_value('contentmodule')
					);
				$sectioncontentid = $this->general_model->update('sectioncontent', $sectioncontentid, $data);
				//Uploads Images
				if(!is_dir($this->config->item('imagespath') . "upload/sectioncontentimages/".$sectioncontentid)){
					mkdir($this->config->item('imagespath') . "upload/sectioncontentimages/".$sectioncontentid, 0755, TRUE);
				}

				$configexlogo['upload_path'] = $this->config->item('imagespath') .'upload/sectioncontentimages/'.$id;
				$configexlogo['allowed_types'] = 'JPG|jpg|jpeg|png';
				$configexlogo['max_width']  = '2000';
				$configexlogo['max_height']  = '2000';
				$this->load->library('upload', $configexlogo);
				$this->upload->initialize($configexlogo);
				if (!$this->upload->do_upload('imageurl')) {
					$image = ""; //No image uploaded!
					if($_FILES['imageurl']['name'] != '') {
						$error .= $this->upload->display_errors();
					}
				} else {
					//successfully uploaded
					$returndata = $this->upload->data();
					$image = 'upload/sectioncontentimages/'. $sectioncontentid . '/' . $returndata['file_name'];
				}
				
				$this->general_model->update('sectioncontent', $sectioncontentid, array('icon' => $image));
				$this->general_model->update('app', $app->id, array('timestamp' => time()));
				if($error == '') {
					redirect('section/view/'.$section->id);
				}
			}
		}

		$cdata['content'] 		= $this->load->view('c_sectioncontent_edit', array('app' => $app, 'error' => $error, 'languages' => $languages, 'sectioncontent' => $sectioncontent, 'contentmodules' => $contentmodules), TRUE);
		$cdata['crumb']			= array($contentmodule->title => "contentmodule/view/".$contentmodule->id, $section->title => "section/view/".$section->id,__("Add Section") => $this->uri->uri_string());
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$this->load->view('master', $cdata);
	}
}