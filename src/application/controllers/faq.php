<?php if(!defined('BASEPATH')) exit(__('No direct script access'));

class Faq extends CI_Controller {

	//php 5 constructor
	function __construct() {
		parent::__construct();
		if(_authed()) { }
	}

	function index() {
		$cdata['content'] 		= $this->load->view('c_faq', array(), TRUE);
		$cdata['sidebar'] 		= $this->load->view('c_sidebar', array(), TRUE);
		$cdata['crumb']			= array(__("FAQ") => "faq");
		$this->load->view('master', $cdata);
	}
}
