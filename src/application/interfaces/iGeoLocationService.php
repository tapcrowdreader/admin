<?php
namespace Tapcrowd\Interfaces;

/**
 * GeoLocationService Interface
 */
interface iGeoLocationService
{
	/**
	 * Return the coordinates for the given address query
	 * @param array $locations Array with string locationd queries
	 * @return array [[latitute: 0.00000, longitude: 0.00000],[latitute: ...], [] ... ]
	 * @throws \InvalidArgumentException The given address/query is not a valid string
	 * @throws \RuntimeException The service(s) returned failure
	 */
	public function getCoordinatesForLocations( Array &$locations, $addressCol, $latCol, $lngCol, $swap );
}