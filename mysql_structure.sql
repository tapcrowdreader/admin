--
-- Tapcrowd Admin SQL Query Log : All database structure updates.
--

--
-- Update 2012-09-27 @tomvdp
-- * Added accountId column to the app table: maps to tc_accounts.accountId
-- * Update all apps to point to respective accounts
--
ALTER TABLE app ADD COLUMN accountId SMALLINT UNSIGNED NOT NULL DEFAULT 0 AFTER organizerid;
UPDATE app a INNER JOIN tc_accounts b ON b.clientId=a.organizerid SET a.accountId = b.accountId;


--
-- Update ? by Jens?
-- * apptype_channel table + data
-- * theme, themeappearance, appearanceflavormodule tables + data
-- * team table
-- * apptype makeover (replacement structure+data)
--
CREATE TABLE IF NOT EXISTS `apptype_channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `apptypeid` int(11) NOT NULL,
  `channelid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42;

INSERT INTO `apptype_channel` (`id`, `apptypeid`, `channelid`) VALUES(1, 1, 1),(2, 3, 1),(3, 4, 1),(4, 5, 1),(5, 7, 1),(6, 8, 1),(7, 9, 1),
(8, 10, 1),(9, 1, 2),(10, 3, 3),(11, 10, 3),(12, 3, 4),(13, 10, 4),(14, 3, 6),(15, 10, 6),(16, 4, 7),(17, 1, 8),(18, 3, 8),(19, 4, 8),(20, 5, 8),
(21, 7, 8),(22, 8, 8),(23, 9, 8),(24, 10, 8),(25, 1, 9),(26, 3, 9),(27, 4, 9),(28, 5, 9),(29, 7, 9),(30, 8, 9),(31, 9, 9),(32, 10, 9),(33, 1, 10),
(34, 3, 10),(35, 4, 10),(36, 5, 10),(37, 7, 10),(38, 8, 10),(39, 9, 10),(40, 10, 10),(41, 12, 1);

CREATE TABLE IF NOT EXISTS `theme` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `screenshot` varchar(255) NOT NULL DEFAULT '',
  `sortorder` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7;

INSERT INTO `theme` (`id`, `name`, `screenshot`, `sortorder`) VALUES(1, 'Theme 1', 'img/themes/theme-1.png', 1),(2, 'Theme 2', 'img/themes/theme-2.png', 2),
(3, 'Theme 3', 'img/themes/theme-3.png', 3),(4, 'Theme 4', 'img/themes/theme-4.png', 4),(5, 'Theme 5', 'img/themes/theme-5.png', 5),(6, 'Theme 6', 'img/themes/theme-6.png', 6);

CREATE TABLE IF NOT EXISTS `themeappearance` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `themeid` int(11) NOT NULL,
  `controlid` int(11) NOT NULL,
  `val` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `appearanceflavormodule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appearancecontrolid` int(11) NOT NULL,
  `flavorid` int(11) NOT NULL,
  `moduletypeid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55;

INSERT INTO `appearanceflavormodule` (`id`, `appearancecontrolid`, `flavorid`, `moduletypeid`) VALUES(1, 1, 0, 0),(2, 2, 0, 0),(3, 3, 0, 0),
(4, 4, 0, 0),(5, 5, 0, 0),(6, 6, 0, 0),(7, 7, 0, 0),(8, 8, 0, 0),(9, 9, 0, 0),(10, 10, 0, 0),(11, 11, 0, 0),(12, 12, 0, 0),(13, 13, 0, 0),
(15, 15, 0, 0),(16, 16, 0, 0),(17, 17, 0, 0),(18, 18, 0, 0),(20, 20, 10, 10),(21, 21, 10, 10),(22, 22, 10, 10),(23, 23, 0, 0),(24, 24, 0, 0),
(25, 25, 0, 0),(27, 27, 0, 0),(28, 28, 0, 10),(29, 29, 0, 10),(30, 30, 0, 10),(31, 31, 0, 10),(32, 32, 0, 0),(33, 33, 0, 0),(34, 34, 0, 0),
(35, 35, 10, 10),(36, 36, 10, 10),(37, 37, 0, 0),(38, 38, 0, 0),(39, 39, 0, 0),(40, 40, 0, 0),(41, 41, 0, 37),(42, 42, 0, 0),(43, 43, 0, 10),
(44, 44, 0, 37),(45, 45, 0, 37),(46, 46, 0, 0),(47, 47, 0, 0),(48, 48, 0, 0),(49, 49, 0, 0),(50, 50, 0, 10),(51, 51, 0, 10),(52, 52, 0, 10),
(53, 53, 0, 10),(54, 54, 10, 0);

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `eventid` int(11) NOT NULL,
  `venueid` int(11) NOT NULL,
  `appid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(500) NOT NULL DEFAULT '',
  `tel` varchar(20) NOT NULL DEFAULT '',
  `function` varchar(255) NOT NULL DEFAULT '',
  `company` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `imageurl` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS apptype;
CREATE TABLE IF NOT EXISTS `apptype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL DEFAULT '',
  `image_phone` varchar(255) NOT NULL DEFAULT '',
  `formname` varchar(255) NOT NULL DEFAULT '',
  `available` int(11) NOT NULL,
  `sortorder` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13;

INSERT INTO `apptype` (`id`, `name`, `title`, `description`, `image_phone`, `formname`, `available`, `sortorder`) VALUES
(1, 'Expo Flavor', 'Venue Flavor', 'For expo halls, museums, theatres...', 'img/02_expo.png', 'formExpo', 1, 50),
(2, 'Venue Flavor', '', 'For expo halls, museums, theatres...', '', '', 0, 0),
(3, 'Event Flavor', 'Pro Event Flavor', 'For conferences, tradeshows, workshops, exhibitions and other professional events.', 'img/03_event.png', 'formEvent', 1, 40),
(4, 'Business Flavor', 'Business Flavor', 'For companies, brands and products.', 'img/01_business.png', 'formBusiness', 1, 60),
(5, 'City Flavor', 'City Flavor', 'For cities and municipalities and tourist boards.', 'img/06_city.png', 'formCity', 1, 10),
(6, 'Football Flavor', '', '', '', '', 0, 0),
(7, 'Shopping flavor', 'Shopping Flavor', 'For shops and shopping malls.', 'img/05_shopping.png', 'formShop', 1, 30),
(8, 'Restaurant Flavor', 'Resto Flavor', 'For restaurants, bars, hotels and clubs.', 'img/07_resto.png', 'formRestaurant', 1, 20),
(9, 'Content Flavor', 'Content Flavor', 'For content & media publishers.', 'img/contentflavoriphone.png', 'formContent', 1, 70),
(10, 'Festival Flavor', 'Leisure Event Flavor', 'For festivals, concerts, sports events, parties and other leisure events.', 'img/iphone-festival.png', 'formFestival', 1, 80),
(11, 'Car Dealer Flavor', '', '', '', '', 0, 0),
(12, 'Blank Flavor', 'Blank Flavor', 'Create a native app for your mobile web app.', 'img/01_blanco.png', 'formBlank', 1, 90);


--
-- Update 2012-10-01 by @jens
-- * Changed sortorder col in premium table
--
ALTER TABLE `premium` ADD COLUMN sortorder INT UNSIGNED NOT NULL;


--
-- Update 2012-10-02 by @tom
-- * Update channelId in tc_accounts to correct values from organizer table
--
UPDATE tc_accounts b INNER JOIN organizer a ON b.clientId=a.id SET b.channelId=a.channelid;

--
-- Update 2012-10-02 by @tom
-- * Added price, basket tables (created by tiago)
--
CREATE TABLE IF NOT EXISTS `prices` (
  `idprices` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`idprices`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `basket` (
  `idbasket` int(11) NOT NULL AUTO_INCREMENT,
  `externalvenueid` int(11) DEFAULT NULL,
  `idvenue` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `codeactive` tinyint(1) DEFAULT '0',
  `code` int(11) DEFAULT '0',
  PRIMARY KEY (`idbasket`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;


--
-- Update 2012-10-02 by @Jens
-- * Added apptypefamily table + updates to apptype table
--
CREATE TABLE `apptypefamily` (id INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255) NOT NULL);
INSERT INTO `apptypefamily` (`id`, `name`)
VALUES
  (1, 'single_event'),
  (2, 'single_venue'),
  (3, 'multiple_event_multiple_venue'),
  (4, 'content');

ALTER TABLE `apptype` ADD `familyid` INT  NOT NULL  AFTER `sortorder`;
UPDATE `apptype` SET `familyid` = '2' WHERE `id` = '1';
UPDATE `apptype` SET `familyid` = '2' WHERE `id` = '2';
UPDATE `apptype` SET `familyid` = '1' WHERE `id` = '3';
UPDATE `apptype` SET `familyid` = '2' WHERE `id` = '4';
UPDATE `apptype` SET `familyid` = '3' WHERE `id` = '5';
UPDATE `apptype` SET `familyid` = '2' WHERE `id` = '7';
UPDATE `apptype` SET `familyid` = '2' WHERE `id` = '8';
UPDATE `apptype` SET `familyid` = '4' WHERE `id` = '9';
UPDATE `apptype` SET `familyid` = '1' WHERE `id` = '10';
UPDATE `apptype` SET `familyid` = '2' WHERE `id` = '11';
UPDATE `apptype` SET `familyid` = '4' WHERE `id` = '12';

--
-- Update 2012-10-11 @tomvdp
-- * Added indexes to group,groupitem table (for ferdau)
--
ALTER TABLE `group` ADD INDEX parent_id (parentid);
ALTER TABLE `group` ADD INDEX app_id (appid);
ALTER TABLE `groupitem` ADD INDEX app_table (appid, itemtable);
ALTER TABLE `groupitem` ADD INDEX groupid (groupid);


--
-- Update 2012-10-11 @tomvdp
-- * Added some missing templateform columns
--
ALTER TABLE templateform ADD COLUMN getgeolocation TINYINT NOT NULL DEFAULT 0 AFTER singlescreen;
ALTER TABLE templateform ADD COLUMN moduletypeid INT NOT NULL DEFAULT 0 AFTER getgeolocation;


--
-- Update 2012-10-19 @tomvdp
-- * Created app_cname table
-- * Update cname data using data from app table
--
CREATE TABLE IF NOT EXISTS app_cname (
  appId INT NOT NULL,
  cname VARCHAR(32) NOT NULL,
  UNIQUE INDEX (cname),
  UNIQUE INDEX cname_appid (cname, appId)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO app_cname (appId,cname) VALUES
-- (235, 'm.forestfestival.be'),
-- (88, 'm.thefuturesummit.be'),
-- (184, 'm.rockternat.be'),
-- (219, 'm.marktrock.com'),
-- (236, 'm.houza-palooza.be'),
-- (245, 'm.supersonicfestival.be'),
-- (267, 'm.cavalor.com'),
-- (283, 'm.boekenbeurs.be'),
-- (301, 'm.facts.be'),
-- (465, 'm.resultbrazil.com'),
(657, 'm.groezrock.be'),
(250, 'm.medtechforum.eu'),
(1538, 'belsecurail.be'),
(1538, 'appelezsecurail.be');

INSERT INTO app_cname (appId,cname) VALUES (1538, 'www.belsecurail.be'),(1538, 'www.appelezsecurail.be');

--
-- Update 2012-10-19 @jens
-- * Added tables for pricing
--
CREATE TABLE `price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channelid` int(11) NOT NULL,
  `apptypeid` int(11) NOT NULL,
  `subflavorid` int(11) NOT NULL,
  `appid` int(11) NOT NULL,
  `moduleid` int(11) NOT NULL,
  `priceoptionid` int(11) NOT NULL,
  `price_setup` int(11) NOT NULL,
  `price_permonth` int(11) NOT NULL,
  `minimumcontractduration` int(11) NOT NULL,
  `billingperiod` varchar(255) NOT NULL DEFAULT '',
  `currencyid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

CREATE TABLE `priceoption` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `currency` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


--
-- Update 2012-10-26 @jens
-- * Added extra tables for pricing
--
CREATE TABLE `appprice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `priceid` int(11) NOT NULL,
  `appstatusid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `appstatus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Update 2012-10-26 @jens
-- * screenshots for submitting
--
CREATE TABLE `appscreenshots` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Update 2012-11-05 @matthias
-- * Added database model for Orders and payments
--

CREATE  TABLE IF NOT EXISTS `tapcrowd`.`order` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `appid` INT(11) NULL DEFAULT NULL ,
  `subflavorid` INT(11) NULL DEFAULT NULL ,
  `userid` INT(11) NULL DEFAULT NULL ,
  `totalamount` FLOAT(11) NULL DEFAULT NULL ,
  `amountexcludingVAT` FLOAT(11) NULL DEFAULT NULL ,
  `amountVAT` FLOAT(11) NULL DEFAULT NULL ,
  `amountincludingVAT` FLOAT(11) NULL DEFAULT NULL ,
  `currencyid` INT(11) NULL DEFAULT NULL ,
  `VATpercentage` FLOAT(11) NULL DEFAULT NULL ,
  `startdate` DATE NULL DEFAULT NULL ,
  `enddate` DATE NULL DEFAULT NULL ,
  `contractduration` INT(11) NULL DEFAULT NULL COMMENT 'Month' ,
  `billingperiod` INT(11) NULL DEFAULT NULL COMMENT '/Month' ,
  `timestamp` DATETIME NULL DEFAULT NULL ,
  `ordertype` VARCHAR(50) NULL DEFAULT NULL ,
  `orderstatus` VARCHAR(50) NULL DEFAULT NULL ,
  `allreadypaid` FLOAT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = MyISAM
AUTO_INCREMENT = 53
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE  TABLE IF NOT EXISTS `tapcrowd`.`orderprice` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `orderid` INT(11) NULL DEFAULT NULL ,
  `priceid` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = MyISAM
AUTO_INCREMENT = 87
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE  TABLE IF NOT EXISTS `tapcrowd`.`payment` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `orderid` INT(11) NULL DEFAULT NULL ,
  `paymentmethodid` INT(11) NULL DEFAULT NULL ,
  `totalamount` INT(11) NULL DEFAULT NULL ,
  `currencyid` INT(11) NULL DEFAULT NULL ,
  `status` INT(11) NULL DEFAULT NULL ,
  `timestamp` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE  TABLE IF NOT EXISTS `tapcrowd`.`paymentmethod` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;



/* Alter app table */
ALTER TABLE `tapcrowd`.`app` ADD COLUMN `subflavorid` INT(11) NULL DEFAULT '1'  AFTER `timestamp` , ADD COLUMN `subflavortype` INT(11) NULL DEFAULT NULL  AFTER `subflavorid` , ADD COLUMN `subflavorpaidtype` INT(11) NULL DEFAULT NULL  AFTER `subflavortype`;

/* Set all current apps to 9999 paid for all */
UPDATE `app` SET `subflavorpaidtype` = '9999';

/* Add Subflavortype column */
ALTER TABLE `tapcrowd`.`subflavor` ADD COLUMN `subflavortype` INT(11) NULL DEFAULT NULL  AFTER `name`;

/* Add the values */
UPDATE `subflavor` SET `subflavortype` = '1' WHERE `Name` = 'Basic';
UPDATE `subflavor` SET `subflavortype` = '2' WHERE `Name` = 'Plus';
UPDATE `subflavor` SET `subflavortype` = '3' WHERE `Name` = 'Pro';

/* Add subflavortypetable */
CREATE  TABLE IF NOT EXISTS `tapcrowd`.`subflavortype` (
  `id` INT(11) UNSIGNED NOT NULL ,
  `Name` VARCHAR(50) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;



ALTER TABLE `app` ADD `show_external_ids_in_cms` INT  NOT NULL  AFTER `subflavorpaidtype`;




--
-- Update 2012-11-09 @jens
-- * Conversion of restogroup modules to real modules
--
INSERT INTO `moduletype` (`id`, `name`, `price`, `controller`, `apicall`, `component`, `ios`, `android`, `webapp`) VALUES  (50, 'Menu', 0, 'groups', 'getGroups', '', 0, 0, 0),  (51, 'Gallery', 0, 'groups', 'getGroups', '', 0, 0, 0),   (52, 'Team', 0, 'groups', 'getGroups', '', 0, 0, 0);

INSERT INTO `eventtypemoduletype` (`moduletype`, `eventtype`, `appid`, `venueid`) VALUES   (50, 0, 0, 1),  (51, 0, 0, 1),  (52, 0, 0, 1);

INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '51', '20');

INSERT INTO `defaultlauncher` (`moduletypeid`, `module`, `title`, `icon`, `displaytype`, `order`, `url`, `tag`, `topurlevent`, `mobileurlevent`, `topurlvenue`, `mobileurlvenue`, `mobileurlapp`, `topurlapp`, `groupid`) VALUES   (50, 'Menu', 'Menu', 'l_catalog', '', 0, '', '', '', '', 'groups/view/--groupid--/venue/--venueid--/catalog', 'catalogs/resto/--venueid--/--groupid--', '', '', 0),   (51, 'Gallery', 'Gallery', 'l_catalog', '', 0, '', '', '', '', 'groups/view/--groupid--/venue/--venueid--/catalog', 'catalogs/resto/--venueid--/--groupid--', '', '', 0),   (52, 'Team', 'Team', 'l_catalog', '', 0, '', '', '', '', 'groups/view/--groupid--/venue/--venueid--/catalog', 'catalogs/resto/--venueid--/--groupid--', '', '', 0);




--
-- Update 2012-11-12 @tomvdp
-- * Added translation table changes
--
ALTER TABLE translation ENGINE = InnoDB;
ALTER TABLE translation MODIFY `language` enum('de','en','eng','es','fr','it','nl','pt','tr','ts') NOT NULL DEFAULT 'en';
ALTER TABLE translation MODIFY `table` ENUM('','app','artist','attendees','catalog','citycontent','coupons',
'event','exhibitor','exhibitorcategory','form','formfield','formscreen','group','launcher',
'loyalty','metadata','newsitem','session','sessiongroup','speaker','sponsor','sponsorgroup','tag','venue') NOT NULL;
ALTER TABLE translation MODIFY `translation` text NOT NULL;
ALTER TABLE translation ADD INDEX `table_id` (`table`,`tableid`);




--
-- Update 2012-11-15 @tomvdp
-- * Added places module
-- * Added places, resources, metadata tables
--
INSERT INTO moduletype (name,controller) VALUES ('Places','places');
SET @moduletypeid := (SELECT id FROM moduletype WHERE controller='places');
INSERT INTO moduletype_subflavor (moduletypeid,subflavorid) VALUES (@moduletypeid,1);
INSERT INTO defaultlauncher (moduletypeid,module,title,icon) VALUES (@moduletypeid,'places','Places','l_location');
INSERT INTO eventtypemoduletype (moduletype,venueid) VALUES (@moduletypeid,1);

CREATE TABLE tc_places (
  id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  appId INT UNSIGNED NOT NULL,
  parentType ENUM('venue','event','place') NOT NULL,
  parentId INT UNSIGNED NOT NULL,
  sortorder SMALLINT UNSIGNED NOT NULL,
  status ENUM('created','pending','inactive','active','locked','deleted','published') NOT NULL DEFAULT 'inactive',

  lat DECIMAL(10,6) NOT NULL,
  lng DECIMAL(10,6) NOT NULL,
  addr VARCHAR(255) NOT NULL,
  title VARCHAR(255) NOT NULL,
  info TEXT NOT NULL,

  PRIMARY KEY (id),
  INDEX idx_parent (appId, parentType, parentId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tc_resources (
  id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  appId INT UNSIGNED NOT NULL,
  parentType ENUM('venue','event','place') NOT NULL,
  parentId INT UNSIGNED NOT NULL,
  sortorder SMALLINT UNSIGNED NOT NULL,
  status ENUM('created','pending','inactive','active','locked','deleted','published') NOT NULL DEFAULT 'inactive',

  path VARCHAR(255) NOT NULL,
  title VARCHAR(255) NOT NULL,
  info TEXT NOT NULL,

  PRIMARY KEY (id),
  INDEX idx_parent (appId, parentType, parentId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tc_metadata (
  id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  appId INT UNSIGNED NOT NULL,
  parentType ENUM('venue','event','place') NOT NULL,
  parentId INT UNSIGNED NOT NULL,
  sortorder SMALLINT UNSIGNED NOT NULL,
  status ENUM('created','pending','inactive','active','locked','deleted','published') NOT NULL DEFAULT 'inactive',

  type ENUM('text','email','phone','datetime') NOT NULL,
  opts INT UNSIGNED NOT NULL DEFAULT 0,
  qname VARCHAR(32) NOT NULL,
  name VARCHAR(255) NOT NULL,
  value VARCHAR(255) NOT NULL,

  PRIMARY KEY (id),
  INDEX idx_parent (appId, parentType, parentId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS tc_metavalues;
CREATE TABLE tc_metavalues (
  metaId SMALLINT UNSIGNED NOT NULL,
  parentType ENUM('venue','event','place') NOT NULL,
  parentId INT UNSIGNED NOT NULL,

  name VARCHAR(255) NOT NULL,
  type ENUM('text','email','phone','datetime') NOT NULL,
  value TEXT NOT NULL,

  UNIQUE INDEX idx_parent (metaId, parentType, parentId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


UPDATE launcher SET moduletypeid = 50, module = 'Menu' WHERE moduletypeid = 41 AND title = 'Menu';
UPDATE launcher SET moduletypeid = 51, module = 'Gallery' WHERE moduletypeid = 41 AND (title = 'Gallery' OR title = 'Pictures');
UPDATE launcher SET moduletypeid = 52, module = 'Team' WHERE moduletypeid = 41 AND title = 'Team';


--
-- Update 2012-11-15 @tomvdp
-- * addded image meta type
--
ALTER TABLE tc_metavalues MODIFY COLUMN type ENUM('text','email','phone','datetime','image') NOT NULL;
ALTER TABLE tc_metadata MODIFY COLUMN type ENUM('text','email','phone','datetime','image') NOT NULL;


--
-- Update 2012-11-28 @tomvdp
-- * added to source column localization, _queue
-- * updated unique index for table localization, _queue
-- * updated channel cols to point to default tapcrowd channel
--
ALTER TABLE localization ADD COLUMN source ENUM('cms','ios','android') NOT NULL DEFAULT 'cms' AFTER channel;
ALTER TABLE localization DROP INDEX id;
ALTER TABLE localization ADD UNIQUE INDEX idx_unique (id,language,country,channel,source);
UPDATE localization SET channel=1 where channel=0;

ALTER TABLE localization_queue ADD COLUMN source ENUM('cms','ios','android') NOT NULL DEFAULT 'cms' AFTER channel;
ALTER TABLE localization_queue DROP INDEX id;
ALTER TABLE localization_queue ADD UNIQUE INDEX idx_unique (id,language,country,channel,source);
UPDATE localization_queue SET channel=1 where channel=0;


--
-- Update 2012-12-05 @tomvdp
-- * added tc_tokens table (session/account tokens)
-- * created anonymous acount (accountId == 3)
--
CREATE TABLE tc_tokens (
  token char(40) NOT NULL,
  timer INT UNSIGNED NOT NULL,
  accountId int(10) UNSIGNED NOT NULL,

  UNIQUE INDEX idx_token (token)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tc_accounts VALUES(3,1,2,1,'user',0,0,'anonymous','anonymous@tapcrowd.com');

--
-- Update 2012-12-04 @jens
-- * added columns to buildqueue and catalog
--
ALTER TABLE buildqueue ADD COLUMN notification_lang VARCHAR(2) NOT NULL AFTER buildservice;
ALTER TABLE catalog ADD COLUMN urltitle VARCHAR(255) NOT NULL AFTER url;

--
-- Update 2012-12-05 @jens
-- * added columns to basket
--
ALTER TABLE `basket` ADD `orderattable` TINYINT  NOT NULL  AFTER `send_order_to_api`;
ALTER TABLE `basket` ADD `ordertakeaway` TINYINT  NOT NULL  AFTER `orderattable`;
ALTER TABLE `basket` ADD `orderreservation` TINYINT  NOT NULL  AFTER `ordertakeaway`;

--
-- Update 2012-12-05 @tomvdp
-- * added status column to tc_accounts
-- * set status of existing accounts to active
-- * update sys admin email address
--
alter table tc_accounts add column status enum('pending','active','disabled') not null default 'pending' after role;
update tc_accounts set status='active';
UPDATE tc_accounts SET email = 'sysadmin@tapcrowd.com' where accountId=1 OR accountId=2

--
-- Update 2012-12-05 @tomvdp
-- * added 'fax' to metadata.type, tc_metavalues.type
-- * updates metavalues with metavalues.name == 'Fax' to reflect new type
--
ALTER TABLE tc_metavalues MODIFY COLUMN type ENUM('text','email','phone','datetime','image','fax') NOT NULL;
ALTER TABLE tc_metadata MODIFY COLUMN type ENUM('text','email','phone','datetime','image','fax') NOT NULL;
update tc_metavalues set type='fax' where name='Fax';

--
-- Update 2012-12-13 @tomvdp
-- * Added column tc_tokens.sessId
--
ALTER TABLE tc_tokens ADD COLUMN sessId INT UNSIGNED NOT NULL AFTER accountId;



--
-- Update 2012-12-18 @tomvdp
-- * Modified column tc_account.status
--
ALTER TABLE tc_accounts MODIFY COLUMN status enum('pending','active','disabled','deleted') NOT NULL DEFAULT 'pending';

--
-- Update 2012-12-21 (Probably last update ever made thanks to the Mayans) @jens
-- * Added column app.launcherview
--
ALTER TABLE app ADD COLUMN launcherview VARCHAR(255) NOT NULL AFTER show_external_ids_in_cms;


--
-- Update 2012-12-28 @jens
-- * Modified enum metadata
--
ALTER TABLE `tc_metadata` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url') NOT NULL  DEFAULT 'text';
ALTER TABLE `tc_metavalues` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url')  NOT NULL  DEFAULT 'text';

--
-- Update 2013-01-08 @ferdau
-- * Added app.responsive column
--
ALTER TABLE app ADD COLUMN responsive TINYINT UNSIGNED DEFAULT 0;

--
-- Update 2012-12-28 @jens
-- * Added place to translation table
--
ALTER TABLE `translation` CHANGE `table` `table` ENUM('','app','artist','attendees','catalog','citycontent','coupons','event','exhibitor','exhibitorcategory','form','formfield','formscreen','group','launcher','loyalty','metadata','newsitem','session','sessiongroup','speaker','sponsor','sponsorgroup','tag','venue','place')  NOT NULL  DEFAULT '';
ALTER TABLE `tc_places` ADD `imageurl` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `info`;

--
-- Update 2013-01-10 @jens
-- * Added title to premium table
--
ALTER TABLE `premium` ADD `title` VARCHAR(255) NOT NULL AFTER `sortorder`;

--
-- Update 2013-01-21 @tomvdp
-- * Added test, staging and dev channels urls
--
INSERT INTO channelurl (channelid, url) VALUES
(1, 'admin.test.tapcrowd.com'),
(1, 'admin.staging.tapcrowd.com'),
(1, 'admin.live.tapcrowd.com'),
(1, 'admin.jens.tapcrowd.com'),
(1, 'admin.tom.tapcrowd.com'),
(1, 'admin.ferdau.tapcrowd.com'),
(1, 'admin.shahid.tapcrowd.com'),
(1, 'admin.benji.tapcrowd.com'),
(1, 'admin.mustfeez.tapcrowd.com');

--
-- Update 2013-01-24 @tom
-- * Modified enum metadata
--
ALTER TABLE `tc_metadata` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url','location','header','map','pdf') NOT NULL  DEFAULT 'text';
ALTER TABLE `tc_metavalues` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url','location','header','map','pdf')  NOT NULL  DEFAULT 'text';

-- Update 2013-01-21 @jens
-- * Added fields for user module
--
ALTER TABLE `appuser` ADD `parentType` ENUM('app','event','venue','moduletype')  NOT NULL  DEFAULT 'app' AFTER userid;
ALTER TABLE `appuser` ADD `parentId` INT(11) NOT NULL AFTER `parentType`;


--
-- Update 2013-01-25 @jens
-- * added table usermodule
--
CREATE TABLE `usermodule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `stayloggedin` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Update 2012-12-28 @jens
-- * Modified enum metadata
--
ALTER TABLE `tc_metadata` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url','location','pdf') NOT NULL  DEFAULT 'text';
ALTER TABLE `tc_metavalues` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url','location','pdf')  NOT NULL  DEFAULT 'text';

--
-- Update 2012-12-28 @jens
-- * hash field in newsitem table for cronjob
--
ALTER TABLE `newsitem` ADD `hash` VARCHAR(255) NOT NULL DEFAULT '' AFTER `order`;
UPDATE newsitem SET hash = md5(CONCAT(title, txt, url)) WHERE hash = '';

--
-- Update 2013-01-30 @shahid
-- * external_id field in tc_places table for excel import
--
ALTER TABLE `tc_places` ADD `external_id` VARCHAR( 255 ) NOT NULL AFTER `id`

--
-- Update 2013-02-04 @tom
-- * Added foster account (adopts parentless elements === no account linked)
--
INSERT INTO tc_accounts VALUES(4,1,2,1,'user','active',0,0,'Foster Parent','fosterparent@tapcrowd.com');
UPDATE app SET accountId=4 WHERE accountId=0;

--
-- Update 2012-02-05 @jens
-- * deleteafterdays field in newsitem table for cronjob
--
ALTER TABLE `newssource` ADD `deleteafterdays` INT  NOT NULL  AFTER `hash`;

--
-- Update 2012-02-06 @jens
-- * visible field in formsubmission
-- * venueid field in personal
--
ALTER TABLE `formsubmission` ADD `visible` SMALLINT  NOT NULL  AFTER `deviceid`;
ALTER TABLE `personal` ADD `venueid` INT  NOT NULL  AFTER `eventid`;
ALTER TABLE `formscreen` ADD `visible` SMALLINT  NOT NULL  AFTER `order`;
UPDATE formscreen SET visible = 1;

--
-- Update 2012-02-18 @jens
-- * emailconfirmation field in form table
--
ALTER TABLE `form` ADD `emailconfirmation` SMALLINT  NOT NULL  AFTER `getgeolocation`;


--
-- Update 2012-02-19 @jens
-- * images of flavors
--
UPDATE apptype SET image_phone = 'img/venue-flavor.png' WHERE id = 1;
UPDATE apptype SET image_phone = 'img/pro-event-flavor.png' WHERE id = 3;
UPDATE apptype SET image_phone = 'img/business-flavor.png' WHERE id = 4;
UPDATE apptype SET image_phone = 'img/city-flavor.png' WHERE id = 5;
UPDATE apptype SET image_phone = 'img/shop-flavor.png' WHERE id = 7;
UPDATE apptype SET image_phone = 'img/resto-flavor.png' WHERE id = 8;
UPDATE apptype SET image_phone = 'img/content-flavor.png' WHERE id = 9;
UPDATE apptype SET image_phone = 'img/festival-flavor.png' WHERE id = 10;
UPDATE apptype SET image_phone = 'img/blank-flavor.png' WHERE id = 12;
UPDATE apptype SET image_phone = 'img/taptarget-flavor.png' WHERE id = 13;

--
-- Update 2012-02-20 @jens
-- * session start and endtime fields to datetime fields
--
ALTER TABLE `session` CHANGE `starttime` `starttime` DATETIME  NOT NULL;
ALTER TABLE `session` CHANGE `endtime` `endtime` DATETIME  NOT NULL;

--
-- Update 2012-02-20 @jens
-- * confbagmail layout
--
CREATE TABLE `confbagmail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `header` text NOT NULL,
  `footer` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
ALTER TABLE `translation` CHANGE `table` `table` ENUM('global','ad','app','artist','attendees','catalog','citycontent','coupons','event','exhibitor','exhibitorcategory','form','formfield','formscreen','group','launcher','loyalty','metadata','newsitem','session','sessiongroup','speaker','sponsor','sponsorgroup','tag','venue','place','confbagmail')  CHARACTER SET utf8  NOT NULL  DEFAULT 'global';


--
-- Update 2012-02-21 @jens
-- * sortorder field schedules
--
ALTER TABLE `schedule` ADD `sortorder` INT  NOT NULL  AFTER `caption`;

--
-- Update 2012-02-26 @jens
-- * customhtml table for result pages of voting and question modules
--
CREATE TABLE `customhtml` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `purpose` enum('voting','askaquestion','') NOT NULL DEFAULT '',
  `header` text NOT NULL,
  `body` text NOT NULL,
  `footer` text NOT NULL,
  `css` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Update 2012-02-27 @jens
-- * metadata language
--
ALTER TABLE `tc_metavalues` ADD `language` ENUM('de','en','eng','es','fr','it','nl','pt','tr','ts')  NOT NULL  DEFAULT 'en'  AFTER `value`;
ALTER TABLE `tapcrowd`.`tc_metavalues` DROP INDEX `idx_parent` ,ADD UNIQUE `idx_parent` ( `metaId` , `parentType` , `parentId` , `language` );
ALTER TABLE `tc_metavalues` CHANGE `parentType` `parentType` ENUM('venue','event','place','launcher','exhibitor','newsitem','session','speaker','attendees','sponsor','artist','catalog','tc_metadata','tc_metavalues')  CHARACTER SET utf8  NOT NULL  DEFAULT 'event';


--
-- Update 2013-03-04 @tom
-- * Added markers table
-- * Altered map table to include markermaps data
-- * Altered meta tables to support markerIcon type
--
ALTER TABLE `map` ADD COLUMN launcherid INT NOT NULL DEFAULT 0 AFTER venueid;
ALTER TABLE `map` ADD COLUMN mapType ENUM('cartesian','geographic') DEFAULT 'cartesian';
ALTER TABLE `map` ADD COLUMN appid INT UNSIGNED NOT NULL DEFAULT 0 AFTER id;

CREATE TABLE tc_markers
(
  id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  mapId SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  parentType ENUM('event','venue','place','exhibitor'),
  parentId SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  xpos SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  ypos SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  status ENUM('created','pending','inactive','active','deleted') DEFAULT 'active',
  mdate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  cdate timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',

  PRIMARY KEY (id),
  INDEX idx_parent (parentType, parentId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tc_metadata` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url','location','pdf','markericon') NOT NULL  DEFAULT 'text';
ALTER TABLE `tc_metavalues` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url','location','pdf','markericon')  NOT NULL  DEFAULT 'text';

--
-- Update 2013-03-05 @jens
-- * metadata
--
ALTER TABLE `tc_metadata` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url','location','header','map','pdf','markericon')  CHARACTER SET utf8  NOT NULL  DEFAULT 'text';
ALTER TABLE `tc_metavalues` CHANGE `type` `type` ENUM('text','email','phone','datetime','image','fax','url','location','header','map','pdf','markericon')  CHARACTER SET utf8  NOT NULL  DEFAULT 'text';

--
-- Update 2013-03-08 @jens
-- * customhtml
--
ALTER TABLE `customhtml` ADD `colors` VARCHAR(255) NOT NULL DEFAULT '' AFTER `css`;

--
-- Update 2013-03-08 @jens
-- * opts formsubmission
--
ALTER TABLE `formsubmission` ADD `opts` SET('interesting','answered')  NOT NULL  DEFAULT ''  AFTER `visible`;

--
-- Update 2013-03-12 @jens
-- * metavalues app parenttype
--
ALTER TABLE `tc_metavalues` CHANGE `parentType` `parentType` ENUM('venue','event','place','launcher','exhibitor','newsitem','session','speaker','attendees','sponsor','artist','catalog','tc_metadata','tc_metavalues','app')  CHARACTER SET utf8  NOT NULL  DEFAULT 'event';

--
-- Update 2013-03-12 @jens
-- * team module business flavor
--
UPDATE `moduletype_subflavor` SET `moduletypeid` = '52' WHERE `moduletypeid` = '18' AND `subflavorid` = '1';

--
-- Update 2012-03-12 @jens
-- * apppaymentinfo table for pricing
-- * securedmodules table for user module
-- * appuser launcher in enum
--

CREATE TABLE `apppaymentinfo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `country` varchar(255) NOT NULL DEFAULT '',
  `vat` smallint(6) NOT NULL,
  `vatnumber` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `securedmodules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `launcherid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `appuser` CHANGE `parentType` `parentType` ENUM('app','event','venue','launcher')  CHARACTER SET latin1  COLLATE latin1_swedish_ci  NOT NULL  DEFAULT 'app';

--
-- Update 2013-03-14 @tom
-- * ALTER tables to use utf-8
--
ALTER TABLE analytics DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE appearanceflavormodule DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE apptypefamily DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE apptype_channel DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE appuser DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE contentmodule DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE fb_friend DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE fb_user DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE form DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE formfield DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE formfieldtype DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE formscreen DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE formsubmission DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE formsubmissionfield DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE formtemplate DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE formtemplatefield DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE image DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE moduletype_subflavor DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `order` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE orderprice DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE payment DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE paymentmethod DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE personal DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE section DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE sectiontype DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE tc_accounts DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE tc_entityrights DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE tc_logins DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE tc_sessions DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE templateform DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE templateformfield DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE templateformscreen DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE theme DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE themeappearance DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE themeicon DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE currency DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE team DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE push_android DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


--
-- Update 2013-03-19 @jens
-- * form table opts
--
ALTER TABLE `form` ADD `opts` SET('moderation')  NOT NULL  DEFAULT ''  AFTER `emailconfirmation`;


--
-- Update 2013-03-26 @jens
-- * form table opts
--
ALTER TABLE `tc_metadata` ADD `moduletypeid` INT  UNSIGNED  NOT NULL  AFTER `parentId`;


--
-- Update 2013-04-04 @jens
-- * meetme module
--
ALTER TABLE `user` ADD `attendeeid` INT  UNSIGNED  NOT NULL  AFTER `mode`;

CREATE TABLE `message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `senderuserid` int(10) unsigned NOT NULL,
  `recipientuserid` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `timestampcreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `timestampsent` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `moduletype`(`id`, `name`) VALUES (64, 'MeetMe');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '64', '3', '0', '0');
INSERT INTO `defaultlauncher` (`moduletypeid`, `module`, `title`, `icon`, `displaytype`, `order`, `url`, `tag`, `topurlevent`, `mobileurlevent`, `topurlvenue`, `mobileurlvenue`, `mobileurlapp`, `topurlapp`, `groupid`)
VALUES
  (64, 'meetme', 'Meet Me', '', '', 0, '', '', '', '', '', '', '', '', 0);
INSERT INTO `moduletype_subflavor` (`moduletypeid`, `subflavorid`)
VALUES
  (64, 12);

-- Update 2013-03-14 @tom
-- * ALTER postpicture table
--
ALTER TABLE postpicture.picture_new ADD ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

--
-- Update 2013-04-04 @tom
-- * Added tapcrowd.appevent, tapcrowd.appvenue indexes
--
ALTER TABLE tapcrowd.appevent ADD INDEX (eventid, appid);
ALTER TABLE tapcrowd.appvenue ADD INDEX (venueid, appid);

--
-- Update 2013-04-05 @tom (via @jens)
-- * Enable user module
--
INSERT INTO eventtypemoduletype (moduletype,eventtype) VALUES(49, 3);
INSERT INTO moduletype_subflavor (moduletypeid,subflavorid) VALUES(49, 24), (49, 12);

--
-- Update 2013-04-09 @jens
-- * ALTER message table
--
ALTER TABLE `message` ADD `appid` INT  NOT NULL  AFTER `id`;


--
-- Update 2013-04-15 @jens
-- * ALTER attendees table
--
ALTER TABLE `attendees` ADD `facebookid` VARCHAR(255)  NOT NULL  DEFAULT ''  AFTER `linkedin`;

--
-- Update 2013-04-19 @jens
-- * ALTER user table
---
ALTER TABLE `user` ADD `external_id` VARCHAR(255)  NOT NULL  DEFAULT ''  AFTER `id`;

--
-- Update 2013-04-25 @jens
-- * Gamification module
---
INSERT INTO `moduletype` (`id`, `name`, `price`, `controller`, `apicall`, `component`, `android`, `ios`, `webapp`, `responsive`) VALUES ('66', 'Gamification', '0', 'gamification', '', '', '1', '1', '0', '1');

INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '66', '12');

INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '66', '3', '0', '0');

INSERT INTO `defaultlauncher` (`id`, `moduletypeid`, `module`, `title`, `icon`, `displaytype`, `order`, `url`, `tag`, `topurlevent`, `mobileurlevent`, `topurlvenue`, `mobileurlvenue`, `mobileurlapp`, `topurlapp`, `groupid`) VALUES (NULL, '66', 'gamification', 'Gamification', 'game-icon-2', '', '100', '', '', '', '', '', '', '', '', '0');


--
-- Update 2013-04-23 @tom
-- * Added indexes on tag, appvenue and translation
---
ALTER TABLE `tapcrowd`.`tag` ADD INDEX `tag_venue` ( `tag` , `venueid` );
ALTER TABLE `tapcrowd`.`appvenue` ADD INDEX ( `appid` , `venueid` );
ALTER TABLE `tapcrowd`.`translation` ADD INDEX ( `language` , `table` , `fieldname` )


--
-- Update 2013-05-02 @jens
-- * notes table
---
CREATE TABLE `notes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `eventid` int(11) NOT NULL,
  `venueid` int(11) NOT NULL,
  `launcherid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Update 2013-05-06 @shahid
-- * translation table
---
ALTER TABLE `translation` CHANGE `table` `table` ENUM( 'global', 'ad', 'app', 'artist', 'attendees', 'catalog', 'citycontent', 'coupons', 'event', 'exhibitor', 'exhibitorcategory', 'form', 'formfield', 'formscreen', 'group', 'launcher', 'loyalty', 'metadata', 'newsitem', 'session', 'sessiongroup', 'speaker', 'sponsor', 'sponsorgroup', 'tag', 'venue', 'place', 'confbagmail', 'formfieldoption' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'global'


--
-- Update 2013-05-06 @jens
-- * tag v2 table
---
CREATE TABLE `tc_tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `itemtype` varchar(255) NOT NULL DEFAULT '',
  `itemid` int(11) NOT NULL,
  `tag` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_tags` (`appid`,`tag`,`itemtype`,`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- Update 2013-05-06 @tom
-- * Added country columns to venue/tc_places for geocoding
---
ALTER TABLE venue ADD COLUMN country CHAR(2) DEFAULT 'BE' COLLATE 'utf8_general_ci' AFTER address;
ALTER TABLE tc_places ADD COLUMN country CHAR(2) DEFAULT 'BE' COLLATE 'utf8_general_ci' AFTER addr;
ALTER TABLE venue ADD COLUMN isgeocoded TINYINT UNSIGNED NOT NULL DEFAULT 0;
ALTER TABLE tc_places ADD COLUMN isgeocoded TINYINT UNSIGNED NOT NULL DEFAULT 0;

--
-- Update 2013-05-13 @jens
-- * in app purchase
---
CREATE TABLE `inapppurchase` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `productid` varchar(255) NOT NULL DEFAULT '',
  `productvalue` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `userinapppurchase` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `udid` varchar(255) NOT NULL DEFAULT '',
  `inapppurchaseid` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Update 2013-05-14 @jens
-- * in app purchase + session parentid
---
ALTER TABLE `userinapppurchase` ADD UNIQUE INDEX `unique` (`udid`, `inapppurchaseid`);
ALTER TABLE `session` ADD `parentid` INT  NOT NULL  AFTER `url`;

--
-- Update 2013-05-15 @jens
-- * search module
---
INSERT INTO `moduletype` (`id`, `name`, `price`, `controller`, `apicall`, `component`, `android`, `ios`, `webapp`) VALUES ('67', 'Search', '0', 'search', '', '', '0', '0', '0');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '67', '3', '0', '0');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '67', '0', '0', '1');
INSERT INTO `defaultlauncher` (`id`, `moduletypeid`, `module`, `title`, `icon`, `displaytype`, `order`, `url`, `tag`, `topurlevent`, `mobileurlevent`, `topurlvenue`, `mobileurlvenue`, `mobileurlapp`, `topurlapp`, `groupid`) VALUES (NULL, '67', 'search', 'Search', '', '', '110', '', '', '', '', '', '', '', '', '0');
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '67', '3');
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '67', '12');

--
-- Update 2013-05-15 @jens
-- * confbag module business flavor
---
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '42', '3');

--
-- Update 2013-05-15 @jens
-- * Social share module
---
INSERT INTO `moduletype` (`id`, `name`, `price`, `controller`, `apicall`, `component`, `android`, `ios`, `webapp`) VALUES ('68', 'Social Share', '0', 'socialshare', '', '', '0', '0', '0');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '68', '3', '0', '0');
INSERT INTO `defaultlauncher` (`id`, `moduletypeid`, `module`, `title`, `icon`, `displaytype`, `order`, `url`, `tag`, `topurlevent`, `mobileurlevent`, `topurlvenue`, `mobileurlvenue`, `mobileurlapp`, `topurlapp`, `groupid`) VALUES (NULL, '68', 'socialshare', 'Social sharing', '', '', '120', '', '', '', '', '', '', '', '', '0');
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '68', '12');
CREATE TABLE `socialshare` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `eventid` int(11) NOT NULL,
  `venueid` int(11) NOT NULL,
  `launcherid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Update 2013-05-22 @shahid
-- * Alter table formfieldoption
---

ALTER TABLE `formfieldoption` CHANGE `formscreenid` `formflow_nextscreenid` INT( 11 ) NOT NULL 


--
-- Update 2013-05-21 @jens
-- * Quiz module
---
CREATE TABLE `quiz` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `launcherid` int(11) NOT NULL,
  `numberofquestionspergame` int(11) NOT NULL,
  `defaultscorepercorrectanswer` double NOT NULL,
  `facebooksharescore` tinyint(1) NOT NULL,
  `facebooksharescore_defaulttext` varchar(255) NOT NULL DEFAULT '',
  `sendresultsbyemail_formid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `quizquestion` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `quizid` int(11) NOT NULL,
  `quizquestiontypeid` int(11) NOT NULL,
  `questiontext` varchar(255) NOT NULL DEFAULT '',
  `imageurl` varchar(255) NOT NULL DEFAULT '',
  `videourl` varchar(255) NOT NULL DEFAULT '',
  `tags` varchar(255) NOT NULL DEFAULT '',
  `correctanswer` varchar(255) NOT NULL DEFAULT '',
  `correctanswer_quizquestionoptionid` int(11) NOT NULL,
  `correctanswer_score` double NOT NULL,
  `explanationtext` text NOT NULL,
  `explanationimage` varchar(255) NOT NULL DEFAULT '',
  `explanationvideo` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `quizquestiontype` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `quizquestiontype` (`id`, `name`)
VALUES
  (1, 'Free text'),
  (2, 'Multiple choice');


CREATE TABLE `quizquestionoption` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `quizquestionid` int(11) NOT NULL,
  `optiontext` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `quizreview` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `quizid` int(11) NOT NULL,
  `minimum_score` double NOT NULL,
  `maximum_score` double NOT NULL,
  `reviewtext` text NOT NULL,
  `reviewimage` varchar(255) NOT NULL DEFAULT '',
  `reviewvideo` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `quizsubmission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `quizid` int(11) NOT NULL,
  `deviceid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `score` double NOT NULL,
  `submissiontimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `quizsubmissionquestion` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `quizsubmissionid` int(11) NOT NULL,
  `quizquestionid` int(11) NOT NULL,
  `quizquestionanswer` varchar(255) NOT NULL DEFAULT '',
  `quizquestionoptionid` int(11) NOT NULL,
  `answeriscorrect` tinyint(4) NOT NULL,
  `score` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `moduletype` (`id`, `name`, `price`, `controller`, `apicall`, `component`, `android`, `ios`, `webapp`) VALUES ('69', 'Quiz', '0', 'quiz', '', '', '0', '0', '0');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '69', '3', '0', '0');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '69', '0', '0', '1');
INSERT INTO `defaultlauncher` (`id`, `moduletypeid`, `module`, `title`, `icon`, `displaytype`, `order`, `url`, `tag`, `topurlevent`, `mobileurlevent`, `topurlvenue`, `mobileurlvenue`, `mobileurlapp`, `topurlapp`, `groupid`) VALUES (NULL, '69', 'quiz', 'Quiz', '', '', '130', '', '', '', '', '', '', '', '', '0');
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '69', '12');

ALTER TABLE `quizquestion` CHANGE `quizquestiontypeid` `quizquestiontypeid` INT(11)  NOT NULL  DEFAULT '1';
ALTER TABLE `quizquestionoption` ADD `quizid` INT  NOT NULL  AFTER `id`;



--
-- Update 2013-05-24 @jens
-- * push android table
---
ALTER TABLE `pushandroid` CHANGE `deviceid` `deviceid` VARCHAR(36)  CHARACTER SET utf8  NOT NULL  DEFAULT '';

--
-- Update 2013-05-24 @jens
-- * rating module festival flavor
---
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '60', '27');

--
-- Update 2013-05-30 @jens
-- * event module resto flavor
---
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '30', '24');

-- Update 2013-06-04 @tom
-- * Added bridge mobile channel
---
ALTER TABLE channel ADD logo VARCHAR(255) NOT NULL DEFAULT '/img/header_logo.png';
INSERT INTO channel (name, templatefolder, logo) VALUES ('Bridge Mobile', 'bridgemobile', '/img/logo.png');
SET @channelId := LAST_INSERT_ID();
INSERT INTO channelurl (channelid, url) VALUES ( @channelId, 'admin.bm.tapcrowd.com' );
INSERT INTO apptype_channel (apptypeid, channelid) VALUES ( 5, @channelId );


--
-- Update 2013-06-20 @shahid
-- queries for forms_options_v1.1 branch
---
CREATE TABLE `formfieldoption` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `formfieldid` int(11) NOT NULL,  
  `value` varchar(255) DEFAULT '',
  `isdefault` tinyint(1),
  `formflow_nextscreenid` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `translation` CHANGE `table` `table` ENUM( 'global', 'ad', 'app', 'artist', 'attendees', 'catalog', 'citycontent', 'coupons', 'event', 'exhibitor', 'exhibitorcategory', 'form', 'formfield', 'formscreen', 'group', 'launcher', 'loyalty', 'metadata', 'newsitem', 'session', 'sessiongroup', 'speaker', 'sponsor', 'sponsorgroup', 'tag', 'venue', 'place', 'confbagmail', 'formfieldoption' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'global';

ALTER TABLE `form` ADD `customurl` VARCHAR(255)  NOT NULL  DEFAULT ''  AFTER `opts`;
ALTER TABLE `form` ADD `useflows` TINYINT(1)  NOT NULL DEFAULT 0 AFTER `customurl`;
ALTER TABLE `formfield` ADD `sticky` TINYINT( 1 ) NOT NULL

--
-- Update 2013-07- @jens
-- sponsor module shop flavor and basket property
---
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '19', '21');
ALTER TABLE `basket` ADD `popupperitem` TINYINT(4)  NOT NULL  AFTER `popupperorder`;


--
-- Update 2013-07-15 @jens
-- photosharing module
---
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '39', '21');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '39', '0', '0', '1');

-- Update 2013-07-08 @tom
-- - added index to user.attendeeid
-- - altered user to innodb
--
ALTER TABLE `user` ADD INDEX user_attendeeid (attendeeid);
ALTER TABLE `user` ENGINE = InnoDB;

-- Update 2013-07-09 @tom
ALTER TABLE `contentmodule`
CHANGE `title` `title` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
CHANGE `icon` `icon` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- Update 2013-07-10 @tom
-- - Altered table user; Added index on login for checkUser calls
ALTER TABLE tapcrowd.user ADD INDEX login (login);

-- Update 2013-07-15 @tom
-- - Added (test) index for venue-tags queries
ALTER TABLE tag ADD INDEX venue_tagid (venueid,id);



--
-- Update 2013-07-17
-- extra field quizsubmission table
---
ALTER TABLE `quizsubmission` ADD `extra` TEXT  NOT NULL  AFTER `submissiontimestamp`;

--
-- Update 2013-07-19
-- extra field allow payment
ALTER TABLE `channel` ADD `allowpayment` TINYINT  NOT NULL  AFTER `logo`;

--
-- Update 2013-07-10 @tom
-- Added channel_theme table
--
CREATE TABLE tc_channel_theme (
  channelId SMALLINT UNSIGNED NOT NULL,
  themeId SMALLINT UNSIGNED NOT NULL,
  sortOrder SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  UNIQUE INDEX channel_theme (channelId, themeId)
) ENGINE=InnoDB, DEFAULT CHARSET=utf8;

-- insert into theme (name,screenshot,sortorder,islive) values ('AXA 1','upload/themeimages/24/theme-cactus.png',0,1);
-- insert into tc_channel_theme values(15,last_insert_id(),0);
-- insert into theme (name,screenshot,sortorder,islive) values ('AXA 2','upload/themeimages/24/theme-black.png',0,1);
-- insert into tc_channel_theme values(15,last_insert_id(),0);


--
-- Update 2013-07-19
-- pincodes usermodule 
---
ALTER TABLE `usermodule` ADD `pincodes` TINYINT  NOT NULL  AFTER `stayloggedin`;

--
-- Update 2013-07-23
-- pincodes usermodule 
---
ALTER TABLE `personal` ADD `userid` INT  NOT NULL  AFTER `type`;
ALTER TABLE `personal` CHANGE `userid` `userid` INT(11)  UNSIGNED  NOT NULL;
ALTER TABLE `personal` ADD `userexternalid` VARCHAR(255)  NOT NULL  DEFAULT ''  AFTER `userid`;


--
-- Update 2013-07-29 @jens
-- RSS
--
ALTER TABLE `newsitem` ADD `guid` VARCHAR(255)  NOT NULL  DEFAULT ''  AFTER `hash`;
ALTER TABLE `newsitem` ADD `extra` TEXT  NOT NULL  AFTER `guid`;

--
-- Update 2013-08-02 @jens
-- myprogram
--
ALTER TABLE `personal` ADD `externaltableid` VARCHAR(255)  NOT NULL  DEFAULT ''  AFTER `externaluserid`;


--
-- Update 2013-08-05 @jens
-- customproperty formfields (TC-7945)
--
ALTER TABLE `formfield` ADD `customproperty` TEXT  NOT NULL  AFTER `sticky`;



--
-- Update 2013-08-06 @tom
-- Added accounts roles
--
ALTER TABLE tc_accounts MODIFY `role` enum('admin','user','appadmin','editor') NOT NULL DEFAULT 'user';

--
-- Update 2013-08-08 jens
-- facebook json field mediumtext
--
ALTER TABLE `facebook` CHANGE `json` `json` MEDIUMTEXT  CHARACTER SET utf8  NOT NULL;

CREATE TABLE tc_channel_settings (
  channelId SMALLINT UNSIGNED NOT NULL,
  n VARCHAR(32) NOT NULL,
  v VARCHAR(64) NOT NULL DEFAULT '',
  UNIQUE INDEX channel_setting (channelId, n)
) ENGINE=InnoDB, DEFAULT CHARSET=utf8;

INSERT INTO tc_channel_settings (channelId, n, v) VALUES
(1, 'account.default_role', 'user'),
(15, 'account.default_role', 'editor'),
(1, 'dummy', 'dumbo');

--
-- Update 2013-08-08 @tom
-- Added test channel data
--
INSERT INTO channel (id,name,support,templatefolder,logo) VALUES(16,'Test Channel','tom@tapcrowd.com','testchannel','/img/header_logo.png');

--

-- Update 2013-08-14 @jens
-- Mobilesitedownloads table to log native installs via mobile site
--
CREATE TABLE `mobilesitedownloads` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `android` int(11) NOT NULL,
  `ios` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Update 2013-08-13 @tom
-- Added unique key on channel/flavor
--
ALTER TABLE apptype_channel ADD UNIQUE INDEX channel_flavors (channelid,apptypeid);


-- Update 2013-08-22 @jens
-- ispartner field for accounts
--
ALTER TABLE `tc_accounts` ADD `ispartner` TINYINT  NOT NULL  AFTER `email`;

-- Update 2013-08-28 @tom
-- Added unique key on profiling.log
--
ALTER TABLE `log` ADD UNIQUE INDEX idx_logid (`id`);
ALTER TABLE `log` DROP PRIMARY KEY;
ALTER TABLE `log` ADD uniqId INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;


-- Update 2013-09-09 @jens
-- Viewtype for confbag mails (list, tables)
--
ALTER TABLE `confbagmail` ADD `viewtype` VARCHAR(255)  NOT NULL  DEFAULT ''  AFTER `footer`;


-- Update 2013-09-11 @jens
-- new modules business flavor
--
INSERT INTO `moduletype` (`id`, `name`, `price`, `controller`, `apicall`, `component`, `android`, `ios`, `webapp`) VALUES ('71', 'Find my car', '0', 'findmycar', '', 'findmycar', '0', '0', '0');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '38', '0', '0', '1');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '71', '0', '0', '1');
INSERT INTO `defaultlauncher` (`id`, `moduletypeid`, `module`, `title`, `icon`, `displaytype`, `order`, `url`, `tag`, `topurlevent`, `mobileurlevent`, `topurlvenue`, `mobileurlvenue`, `mobileurlapp`, `topurlapp`, `groupid`) VALUES (NULL, '71', 'findmycar', 'Find my car', 'l_catalog', '', '8', '', '', '', '', '', '', '', '', '0');
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '38', '3');
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '39', '3');
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '71', '3');

-- Update 2013-09-11 @jens
-- attendees qr code
--
ALTER TABLE `attendees` ADD `qrcode` VARCHAR(255)  NOT NULL  DEFAULT ''  AFTER `order`;

-- Update 2013-09-13 @jens
-- module temlates
--
ALTER TABLE `moduletype` ADD `template` TINYINT  NOT NULL  AFTER `webapp`;
CREATE TABLE `moduletype_channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `moduletypeid` int(11) NOT NULL,
  `channelid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Update 2013-09-23 @jens
-- company in account table
--
ALTER TABLE `tc_accounts` ADD `company` VARCHAR(255)  NOT NULL  DEFAULT ''  AFTER `ispartner`;

-- Update 2013-09-24 @ferdau
-- Added custom css table
CREATE TABLE `customcss` (
  `appid` int(11) NOT NULL,
  `css` text,
  PRIMARY KEY (`appid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Update 2013-09-30 @ferdau
-- Added social to translate table
ALTER TABLE translation MODIFY `table` enum('global','ad','app','artist','attendees','catalog','citycontent','coupons','event','exhibitor','exhibitorcategory','form','formfield','formscreen','group','launcher','loyalty','metadata','newsitem','session','sessiongroup','speaker','sponsor','sponsorgroup','tag','venue','place','confbagmail','formfieldoption','social');


-- Update 2013-10-08 @jens
-- catalog description field
ALTER TABLE `catalog` CHANGE `description` `description` TEXT  CHARACTER SET utf8  NOT NULL;


-- Update 2013-11-04 @jens
-- paths field in ad table
ALTER TABLE `ad` ADD `paths` TEXT  NOT NULL  AFTER `appid`;

-- Update 2013-11-04 @jens
-- default value for ints
ALTER TABLE `module` CHANGE `moduletype` `moduletype` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `module` CHANGE `event` `event` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `module` CHANGE `timestamp` `timestamp` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `module` CHANGE `app` `app` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `module` CHANGE `timestamp` `timestamp` INT(11)  NOT NULL  DEFAULT '0';

ALTER TABLE `launcher` CHANGE `eventid` `eventid` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `launcher` CHANGE `venueid` `venueid` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `launcher` CHANGE `appid` `appid` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `launcher` CHANGE `moduletypeid` `moduletypeid` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `launcher` CHANGE `order` `order` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `launcher` CHANGE `groupid` `groupid` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `launcher` CHANGE `confbagserver` `confbagserver` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `launcher` CHANGE `extragetparams` `extragetparams` INT(11)  NOT NULL  DEFAULT '0';
ALTER TABLE `launcher` CHANGE `templateformid` `templateformid` INT(11)  NOT NULL  DEFAULT '0';


-- Update 2013-11-06 @jens
-- default value for ints
CREATE TABLE `map_zoomlevel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mapid` int(11) NOT NULL,
  `imageurl` varchar(255) NOT NULL DEFAULT '',
  `zoomlevel` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `map_routingpath` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mapid` int(11) NOT NULL,
  `map_routingpointid_start` int(11) NOT NULL,
  `map_routingpointid_end` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `map_routingpoint` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mapid` int(11) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `moduletype` (`id`, `name`, `price`, `controller`, `apicall`, `component`, `android`, `ios`, `webapp`) VALUES ('72', 'MapV2', '0', 'mapv2', 'getMapV2', 'mapv2', '0', '0', '0');
INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '72', '12');
INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '72', '3', '0', '0');
INSERT INTO `defaultlauncher` (`id`, `moduletypeid`, `module`, `title`, `icon`, `displaytype`, `order`, `url`, `tag`, `topurlevent`, `mobileurlevent`, `topurlvenue`, `mobileurlvenue`, `mobileurlapp`, `topurlapp`, `groupid`)
VALUES
  (78, 72, 'mapV2', 'Improved Floorplan', 'l_map', '', 3, '', '', 'mapv2/event/--eventid--', 'map/event/--eventid--', 'mapv2/venue/--venueid--', 'map/venue/--venueid--', '', '', 0);


-- INSERT INTO `moduletype` (`id`, `name`, `price`, `controller`, `apicall`, `component`, `android`, `ios`, `webapp`) VALUES ('73', 'Indoor Routing', '0', 'indoorrouting', 'getIndoorRouting', '', '0', '0', '0');
-- INSERT INTO `moduletype_subflavor` (`id`, `moduletypeid`, `subflavorid`) VALUES (NULL, '73', '12');
-- INSERT INTO `eventtypemoduletype` (`id`, `moduletype`, `eventtype`, `appid`, `venueid`) VALUES (NULL, '73', '3', '0', '0');
-- INSERT INTO `defaultlauncher` (`id`, `moduletypeid`, `module`, `title`, `icon`, `displaytype`, `order`, `url`, `tag`, `topurlevent`, `mobileurlevent`, `topurlvenue`, `mobileurlvenue`, `mobileurlapp`, `topurlapp`, `groupid`)
-- VALUES
--   (79, 73, 'indoorrouting', 'Indoor Routing', 'l_map', '', 3, '', '', '', '', '', '', '', '', 0);


-- Update 2013-11-06 @ferdau
-- fixed format of device id in formsubmission table
ALTER TABLE formsubmission MODIFY deviceid VARCHAR (36) NOT NULL DEFAULT '';



-- Update 2013-12-04 @jens
-- analyticsgrant table
CREATE TABLE `analyticsgrant` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
